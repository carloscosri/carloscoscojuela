<?php
require_once 'lib/View.php';

class LogoutView extends View
{
    function __construct()
    {
        parent::__construct();
    }

    public function render($template='logout.tpl')
    {  
        $this->smarty->display($template);
    }

}
