<?php
require_once 'lib/View.php';

class ProductView extends View
{
    function __construct()
    {
        parent::__construct();
    }

    public function render($rows, $template='product.tpl')
    {   
        $js[] = 'ajaxProduct.js';
        $this->smarty->assign('js', $js);
        $this->smarty->assign('rows', $rows);
        $this->smarty->display($template);
    }

    public function add()
    {
        $template='productFormAdd.tpl';
        $this->smarty->display($template);
    }

    public function edit($row, $error="")
    {
        $template='productFormEdit.tpl';
        $this->smarty->assign('row', $row);
        $this->smarty->assign('error', $error);
        $this->smarty->display($template);
    }
    /*
    public function order($template='order.tpl')
    {
        $this->smarty->assign('rows', $_SESSION["orderList"]);
        $this->smarty->display($template);
    }    
    */
}