<?php
require_once 'lib/Model.php';

class ProductModel extends Model{
    
    function __construct()
    {
        parent::__construct();
    }

    public function delete($id)
    {
        $this->_sql = "DELETE FROM producto WHERE id=$id";
        $this->executeQuery();
    }

    public function get($id)
    {
        $this->_sql = "SELECT * FROM producto"
                . " WHERE id = $id ORDER BY id";
        $this->executeSelect();
        return $this->_rows[0];
    }

        public function getAll()
    {
        $this->_sql = "SELECT * FROM producto";
        $this->executeSelect();
        return $this->_rows;
    }

    public function insert($fila)
    {
        $this->_sql = "INSERT INTO producto(codigo, nombre, precio, existencia) "
                . "VALUES ('$fila[codigo]', '$fila[nombre]', '$fila[precio]', '$fila[existencia]')";

        $this->executeQuery();
    }

    public function update($row)
    {
        $this->_sql = "UPDATE producto SET "
                . " codigo='$row[codigo]', "
                . " nombre='$row[nombre]',"
                . " precio=$row[precio],"
                . " existencia='$row[existencia]'"
                . " WHERE id = $row[id]";
        return $this->executeQuery();
    }

    public function getPage($pageNumber)
    {
        $this->_sql = "SELECT * FROM producto"
                . " LIMIT " . $this::PAGE_SIZE * ($pageNumber - 1)
                . ', ' . $this::PAGE_SIZE;
        $this->executeSelect();
        return $this->_rows;
    }
    
    public function getNumPages()
    {
        $this->_sql = "SELECT * FROM producto";
        $this->executeSelect();
        return ceil(count($this->_rows)/$this::PAGE_SIZE);
    }
    
    public function delReg($num)
    {
        $this->_sql = "DELETE FROM producto WHERE id=$num";
        return $this->executeQuery();
    }
    
    public function insReg($fila)
    {        
        $this->_sql = "INSERT INTO producto(codigo, nombre, precio, existencia) "
        . "VALUES ('$fila[codigo]', '$fila[nombre]', '$fila[precio]', '$fila[existencia]')";
        return  $this->executeQuery();
    }
    
    public function findName($filter)
    {
        $this->_sql = "SELECT id, codigo, nombre, precio, existencia"
                . " FROM producto WHERE nombre LIKE '%" . $filter . "%'";
        $this->executeSelect();
        return $this->_rows;
    }
    
    public function updateProduct($idProd, $valor, $campo)
    {       
        $this->_sql = "UPDATE producto SET ". $campo ."='". $valor ."' WHERE id= $idProd";
        return $this->executeQuery();
    }
}