<?php

require_once 'lib/Model.php';

class OrderModel extends Model {

    function __construct() {

        parent::__construct();
    }

    protected function delete($numero) {
        
    }

    protected function get($numero) {
        
    }

    protected function getAll() {
        
    }

    protected function insert($fila) {
        
    }

    protected function update($fila) {
        
    }
    
    public function addOrder()
    {
        $idUser = $_SESSION['idUsuario'];
        $this->_sql = "INSERT INTO pedido(fechaServido, estado, idUsuario) "
                . "VALUES (0, 0,'" . $idUser . "')";
        $id = $this->executeInsert();
        return $id;
        //Tengo que devolver el id(pedido) que se acaba de insertar en pedido
    }
    
    public function showOrdersUser()
    {
        $idUser = $_SESSION['idUsuario'];
        
        if($_SESSION['role']>=3){
            $this->_sql = "SELECT * FROM pedido";
        }
        else{
            $this->_sql = "SELECT * FROM pedido WHERE idUsuario ='$idUser'";
        }
        $this->executeSelect();
        return $this->_rows;
    }
    public function selectDetailsOrder($idPedido) 
    {
        $this->_sql = "SELECT * FROM detallepedido WHERE idPedido ='$idPedido'";
        $this->executeSelect();
        return $this->_rows;
    }

    public function addOrderProducts($idPedido)
    {
        $arrayProd = $_SESSION['listaPedido'];
        $contador = 0;
        foreach ($arrayProd as $producto) {
            $contador++;    
            $idP = $producto['id'];
            $precioP = $producto['precio'];
            $cantidadP = $producto['cantidad'];
            $this->_sql = "INSERT INTO detallepedido (idPedido, linea, idProducto, cantidad, precio)"
                    . " VALUES ('" . $idPedido."','". $contador ."', '".$idP."', '".$cantidadP."', '".$precioP."')";
            $this->executeQuery();
        }
        unset($_SESSION['listaPedido']);
        return true;
    }
    
    public function selectIdUsuario($nombreUsuario)
    {
        $this->_sql = "SELECT id FROM usuario WHERE nombre ='$nombreUsuario'";
        $this->executeSelect();
        return $this->_rows;
        }
    }
