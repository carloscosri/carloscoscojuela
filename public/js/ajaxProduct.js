$(document).ready(function (){
    var loadDoc = function (numero) {
        $(document).attr("pagina", numero);
        var url = 'http://localhost/proyectotema7/es/product/ajaxPageData/' + numero;
        $.get(url, '', function(data) {
            $("#tbodyList").empty();            
            for(var i = 0; i<data.length; i++){                
                newRows = "";
                newRows = '<tr id="row'+data[i].id+'"><td class="cell">' + data[i].id + '</td>';
                newRows = newRows +'<td><input class="cell update" name="codigo" type="text" value="' + data[i].codigo + '" /></td>';
                newRows = newRows +'<td><input class="cell update" name="nombre" type="text" value="' + data[i].nombre + '" /></td>';
                newRows = newRows +'<td><input class="cell update" name="precio" type="text" value="' + data[i].precio + '" /></td>';
                newRows = newRows +'<td><input class="cell update" name="existencia" type="text" value="' + data[i].existencia + '" /></td>';                
                newRows = newRows + '<td><button class="pedido" value='+data[i].id+'\
                co="' + data[i].codigo + '"\n\
                no="' + data[i].nombre + '"\n\
                pr="' + data[i].precio + '"\n\
                >comprar</button>';
                newRows = newRows + '<button class="del" value='+data[i].id+'>borrar</button></td></tr>';
                $("#tbodyList").append(newRows);
                $(document).on("focusout", '.update', function () {
                    var idProd = $(this).parent().parent().attr('id');
                    idProd = idProd.substring(3);
                    var valor = this.value;
                    var campo = this.name;
                    updateProducto(idProd, campo, valor);
                });
            }
        }, 'json' );
    };
    
    var updateProducto = function (idProd, campo, valor) {
        $.post('http://localhost/proyectotema7/es/product/ajaxUpdateProduct', {idProd: idProd, campo: campo, valor: valor}, function (data) {
        }, 'json');
    };
    
    var numeroBotones = function () {
        var url = 'http://localhost/proyectotema7/es/product/ajaxNumPages/';
        $.get(url, '', function(data) {
            $("#buttons").empty();
            var calcButtons = data;
            var newButtons = "";
            for(var i = 1; i<=calcButtons; i++){
                newButtons = '<button class="push" value='+i+'>'+i+'</button>';
                $("#buttons").append(newButtons);
            }
        } );
    };

    $(document).on('click','.push',(function () {
        var num = $(this).val();
        loadDoc(num);
    }));

    numeroBotones();
    loadDoc(1);

    $(document).on('click','.del',(function () {
        var num = $(this).val();
        ajaxDelete(num);
    }));

    var ajaxDelete = function (numero) {
        var url = 'http://localhost/proyectotema7/es/product/ajaxDelete/'+ numero;
        $.get(url, '', function(data) {
            pagina = $(document).attr("pagina");
            loadDoc(pagina);
            numeroBotones();
            if(data){
                $("#row" + numero).empty();
            }
            else{
                alert("Borrado Fallido");
            }
        }, 'json' );
    };

    $(document).on('click','.ins',(function () {
        var codigo = $('#codigo').val();
        var nombre = $('#nombre').val();
        var precio = $('#precio').val();
        var existencia = $('#existencia').val();
        ajaxInsert(codigo, nombre, precio, existencia);
    }));

        var limpiarFormulario = function (){
            $('#codigo').val("");
            $('#nombre').val("");
            $('#precio').val("");
            $('#existencia').val("");
        };

        var ajaxInsert = function (codigo, nombre, precio, existencia) {
        var url = 'http://localhost/proyectotema7/es/product/ajaxInsert/';
        $.post(url, 
            {
                codigo: codigo,
                nombre: nombre,
                precio: precio,
                existencia: existencia
            }
            , function(data) {
            pagina = $(document).attr("pagina");
            loadDoc(pagina);
            numeroBotones();
            if(data){
                limpiarFormulario();
            }
            else{
                alert("Inserción Fallida");
            }
        }, 'json' );
    };  
          
    $("#filter").keyup(function () {
        filterTable($(this).val());
    });
    
    var filterTable = function (text) {
        url = 'http://localhost/proyectotema7/es/product/ajaxGetDataFilter/';
        $.get(url, {filter: text}, function (data) {
            rows = data;
            $("#tbodyList").empty();
            for (var i = 0; i < rows.length; i++) {

                newRows = '<tr class="cell" id="row' + rows[i].id + '" idRow=' + rows[i].id + ' >';
                newRows = newRows + '<td > ' + rows[i].id + '</td>';
                newRows = newRows + '<td > ' + rows[i].codigo + '</td>';
                newRows = newRows + '<td > ' + rows[i].nombre + '</td>';
                newRows = newRows + '<td > ' + rows[i].precio + '</td>';
                newRows = newRows + '<td > ' + rows[i].existencia + '</td>';
                newRows = newRows + '<td><button class="buy" value='+data[i].id+'\
                co="' + data[i].codigo + '"\n\
                no="' + data[i].nombre + '"\n\
                pr="' + data[i].precio + '"\n\
                >comprar</button>';
                newRows = newRows + '<button class="del" value='+data[i].id+'>borrar</button></td></tr>';
                $("#tbodyList").append(newRows);
            }
            $(document).on('click', '.deleteRecord', function () {
                idRow = this.id;
                idRow = idRow.substring(6);
                deleteRecord(idRow);
            });
        }, 'json');       
    }; 
        
    $(document).on("click", '.pedido', function (e) {
        e.preventDefault();
        var idProd = $(this).attr("value");
        var precio = $(this).attr("pr");
        var nombre = $(this).attr("no");
        nuevoPedido(idProd, precio, nombre);
    });
    
    var nuevoPedido = function (idProd, precioProd, nombreProd) {
        $.post('http://localhost/proyectotema7/es/product/ajaxNuevoPedido',
                {id: idProd, nombre: nombreProd, precio: precioProd, cantidad: "1"}, function (data) {
            alert("Producto añadido al carro");
        }, 'json');
    };

    $(document).on("click", '#botonPedido', function () {
        alert("Pedido realizado con exito");
        addPedido();
    });
    
    var addPedido = function () {
        $.post('http://localhost/proyectotema7/es/order/ajaxAddOrder', "", function (data) {
            if (data) {
                $(".listadoPedidos").empty();
                alert("Pedido realizado con exito! \n Para ver el estado de su pedido, pulse arriba");
            }
        }, 'json');

    };

    var deletePedido = function (idProd) {
        $.post('http://localhost/proyectotema7/es/order/ajaxDeleteOrder', {id: idProd}, function (data) {
            if (data) {
                $("#ped" + idProd).empty();
            } else {
                alert("Borrado no realizado con exito");
            }
        }, 'json');
    };
    
    var editarUnidades = function (idProd, cantidadItem) {
        $.post('http://localhost/examen2alb/es/order/updateList',
                {id: idProd, cantidad: cantidadItem}, function (data) {
        }, 'json');
    };
    
    $(document).on("focusout", '.cantidad', function () {
        var idProd = $(this).attr('ide');
        var precio = $(this).attr('precio');
        var info = this.value;
        editarUnidades(idProd, info);
        $("#total" + idProd).html('');
        $('#total' + idProd).append(info * precio + " €");
    });
    
    $(document).on("click", '.eliminarPedido', function () {
        var idProd = $(this).attr("ide");
        deletePedido(idProd);
    });
    
    $(document).on("click", '.listaPedidosRealizados', function () {
        var idPedido = $(this).attr("ide");
        seleccionarPedidoRealizado(idPedido);
    });
});