<?php
//Llamamos a la funcion que nos devuelve el nombre del script en ejecucion, para hacer el enlace a los idiomas correcto
	$pagina_actual=nombre_script_en_ejecucion();

//Los nombres de las subsecciones deberan de ir precedidos del nombre de la seccion separado por un guion bajo (empresa_descripcion.php). De esta forma podemos automatizar el cambio de foto de la cabecera. Como hay una fotografia por seccion esta se llamara igual que la seccion

//En el caso de exisitir apartados dentro de una subseccion emplearemos la misma nomenclatura empresa_situacion_mapa.php
//cabecera
	$nombre_seccion=explode("_",$pagina_actual);

//Esta variable almacenara el nombre de la subseccion en la que nos encontramos, la suarmeos para hacer enlaces o no los componentes del menu.
	$nombre_subseccion=explode(".",$nombre_seccion[1]);
	$nombre_apartado=explode(".",$nombre_seccion[2]);


//Se mostrara un menu u otro dependiendo de la seccion activa
	switch($nombre_seccion[0])
	{
//**********************************************************************EMPRESA******************************************************************
//***********************************************************************************************************************************************


		default:
//Se incluye el archivo de idiomas necesario, dependiendo del idioma que se encuentre activo
			include("../idiomas/".$_SESSION["idioma"]."/oficina_submenu.php");
?>
			<!--<div class="posicion_icono_submenu"><img src="../img/oficina/icono_oficina_mini.gif" alt="" /></div>-->
  			<!--<div class="submenu_titulo"><?php echo $menu4; ?></div>-->
            <div class="limpiar"></div>
            <div class="submenu_filete_superior"></div>


<?php         	?><!--<div class="submenu_filete_intermenu"></div>--><?php
			if($nombre_subseccion[0]!="datos")
			{
?>
   				<div class="bolo_submenu"><i class="fa fa-user" aria-hidden="true"></i>&nbsp;</div>
               	<div class="posicion_submenu_texto separacion_submenu"><a href="oficina_datos.php?id=<?=$_SESSION["idioma"]?>" class="submenu"><?=$oficina_submenu1?></a></div>

<?php
			}//if(nombre_subseccion[0]!="datos")
			else
			{
?>
				<div class="bolo_submenu"><i class="fa fa-user" aria-hidden="true"></i>&nbsp;</div>
               	<div class="posicion_submenu_texto separacion_submenu submenu_seleccionado"><?=$oficina_submenu1?></div>
<?php
			}//else(nombre_subseccion[0]!="datos")
			?>
			<!--
			<div class="submenu_filete_intermenu"></div><?php
			//CASO PARTICULAR: Como hay dos secciones que contienne la palabra informacion como la primera del nombre, para esta entrada del menu se tendran
			//en cuenta las dos primeras palabras, para que solo se marque cuando le corresponda
			if($nombre_subseccion[0]!="infofactura")
			{
?>
   				<div class="bolo_submenu"><i class="fa fa-chevron-right" aria-hidden="true"></i></div>
               	<div class="posicion_submenu_texto separacion_submenu"><a href="oficina_infofactura.php?id=<?=$_SESSION["idioma"]?>" class="submenu"><?=$oficina_submenu2?></a></div>

<?php
			}//if($nombre_subseccion[0]!="infofactura")
			else
			{
?>
				<div class="bolo_submenu"><i class="fa fa-chevron-right" aria-hidden="true"></i></div>
               	<div class="posicion_submenu_texto separacion_submenu submenu_seleccionado"><?=$oficina_submenu2?></div>
<?php
			}//else($nombre_subseccion[0]!="infofactura")
			?>
			-->
			<div class="submenu_filete_intermenu"></div><?php

			if($nombre_subseccion[0]!="modificacion")
			{
?>
   					<div class="bolo_submenu"><i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;</div>
        	       	<div class="posicion_submenu_texto separacion_submenu"><a href="oficina_modificacion_datostitular.php?id=<?=$_SESSION["idioma"]?>" class="submenu"><?=$oficina_submenu4?></a></div>

<?php
			}//if($nombre_subseccion[0]!="modificacion")

				else
				{
?>
					<div class="bolo_submenu"><i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;</div>
           			<div class="posicion_submenu_texto separacion_submenu submenu_seleccionado"><?=$oficina_submenu4?></div>
<?php
				}//else($nombre_subseccion[0]!="cambios")


//Ahora comprobamos en que apartado estamos
				if($nombre_apartado[0]!="datostitular")
				{
?>                   <div class="bolo_submenu2"></div>
					<div class="posicion_submenu_subseccion separacion_submenu_subseccion"><a href="oficina_modificacion_datostitular.php?id=<?=$_SESSION["idioma"]?>" class="submenu_subseccion"><?=$oficina_submenu4_1?></a></div>
<?php
                }//if($nombre_apartado[0]!="titular")
                else
                {
?>				<div class="bolo_submenu2"><img src="../img/comun/bolo_submenu2.gif" alt=""/></div>
					<div class="posicion_submenu_subseccion separacion_submenu_subseccion submenu_subseccion_seleccionado"><?=$oficina_submenu4_1?></div>
<?php
                }//else($nombre_apartado[0]!="titular")

//Ahora comprobamos en que apartado estamos
				if($nombre_apartado[0]!="datosbancarios")
				{
?>                   <div class="bolo_submenu2"></div>
					<div class="posicion_submenu_subseccion separacion_submenu_subseccion"><a href="oficina_modificacion_datosbancarios.php?id=<?=$_SESSION["idioma"]?>" class="submenu_subseccion"><?=$oficina_submenu4_2?></a></div>
<?php
                }//if($nombre_apartado[0]!="datosbancarios")
                else
                {
?>				<div class="bolo_submenu2"><img src="../img/comun/bolo_submenu2.gif" alt=""/></div>
					<div class="posicion_submenu_subseccion separacion_submenu_subseccion submenu_subseccion_seleccionado"><?=$oficina_submenu4_2?></div>
<?php
                }//else($nombre_apartado[0]!="datosbancarios")



				if($nombre_apartado[0]!="datossuministro")
				{
?>                  <div class="bolo_submenu2"></div>
					<div class="posicion_submenu_subseccion separacion_submenu_subseccion"><a href="oficina_modificacion_datossuministro.php?id=<?=$_SESSION["idioma"]?>" class="submenu_subseccion"><?=$oficina_submenu4_3?></a></div>
<?php
                }//if($nombre_apartado[0]!="datossuministro")
                else
                {
?>				<div class="bolo_submenu2"><img src="../img/comun/bolo_submenu2.gif" alt=""/></div>
					<div class="posicion_submenu_subseccion separacion_submenu_subseccion submenu_subseccion_seleccionado"><?=$oficina_submenu4_3?></div>
<?php
                }//else($nombre_apartado[0]!="datossuministro")




				if($nombre_apartado[0]!="visualizar")
				{
?>                  <div class="bolo_submenu2"></div>
					<div class="posicion_submenu_subseccion separacion_submenu_subseccion"><a href="oficina_modificacion_visualizar.php?id=<?=$_SESSION["idioma"]?>" class="submenu_subseccion"><?=$oficina_submenu4_4?></a></div>
<?php
                }//if($nombre_apartado[0]!="datossuministro")
                else
                {
?>				<div class="bolo_submenu2"><img src="../img/comun/bolo_submenu2.gif" alt=""/></div>
					<div class="posicion_submenu_subseccion separacion_submenu_subseccion submenu_subseccion_seleccionado"><?=$oficina_submenu4_4?></div>
<?php
                }//else($nombre_apartado[0]!="datossuministro")





//Ahora comprobamos en que apartado estamos



				?><div class="submenu_filete_intermenu"></div><?php

				if($nombre_subseccion[0]!="informacion")
				{
?>
   					<div class="bolo_submenu"><i class="fa fa-info-circle" aria-hidden="true"></i>&nbsp;</div>
        	       	<div class="posicion_submenu_texto separacion_submenu"><a href="oficina_informacion_contratos.php?id=<?=$_SESSION["idioma"]?>" class="submenu"><?=$oficina_submenu5?></a></div>

<?php
				}//if($nombre_subseccion[0]!="informacion")
				else
				{
?>
					<div class="bolo_submenu"><i class="fa fa-info-circle" aria-hidden="true"></i>&nbsp;</div>
               		<div class="posicion_submenu_texto separacion_submenu submenu_seleccionado"><?=$oficina_submenu5?></div>
<?php
				}//else($nombre_subseccion[0]!="informacion")

//Ahora comprobamos en que apartado estamos
					if($nombre_apartado[0]!="contratos")
					{
?>                      <div class="bolo_submenu2"></div>
						<div class="posicion_submenu_subseccion separacion_submenu_subseccion"><a href="oficina_informacion_contratos.php?id=<?=$_SESSION["idioma"]?>" class="submenu_subseccion"><?=$oficina_submenu5_1?></a></div>
<?php
	                }//if($nombre_apartado[0]!="contratos")
        	        else
    	            {
?>				<div class="bolo_submenu2"><img src="../img/comun/bolo_submenu2.gif" alt=""/></div>
						<div class="posicion_submenu_subseccion separacion_submenu_subseccion submenu_subseccion_seleccionado"><?=$oficina_submenu5_1?></div>
<?php
                	}//else($nombre_apartado[0]!="contratos")

//facturas.php -> informacion_facturas.php

//Ahora comprobamos en que apartado estamos
					if($nombre_apartado[0]!="facturas")
					{
?>                       <div class="bolo_submenu2"></div>
						<div class="posicion_submenu_subseccion separacion_submenu_subseccion"><a href="oficina_informacion_facturas.php?id=<?=$_SESSION["idioma"]?>" class="submenu_subseccion"><?=$oficina_submenu5_2?></a></div>
<?php
	                }//if($nombre_apartado[0]!="facturas")
        	        else
    	            {
?>
						<div class="bolo_submenu2"><img src="../img/comun/bolo_submenu2.gif" alt=""/></div>
                        <div class="posicion_submenu_subseccion separacion_submenu_subseccion submenu_subseccion_seleccionado"><?=$oficina_submenu5_2?></div>
<?php
                	}//else($nombre_apartado[0]!="facturas")

//Ahora comprobamos en que apartado estamos
					if($nombre_apartado[0]!="certificadofacturas")
					{
?>
						<div class="bolo_submenu2"></div>
						<div class="posicion_submenu_subseccion separacion_submenu_subseccion"><a href="oficina_informacion_certificadofacturas.php?id=<?=$_SESSION["idioma"]?>" class="submenu_subseccion"><?=$oficina_submenu5_3?></a></div>
<?php
	                }//if($nombre_apartado[0]!="certificadofacturas")
        	        else
    	            {
?>				<div class="bolo_submenu2"><img src="../img/comun/bolo_submenu2.gif" alt=""/></div>
						<div class="posicion_submenu_subseccion separacion_submenu_subseccion submenu_subseccion_seleccionado"><?=$oficina_submenu5_3?></div>
<?php
                	}//else($nombre_apartado[0]!="certificadofacturas")

//certificado_consumos.php -> informacion_certificadoconsumos.php

//Ahora comprobamos en que apartado estamos
					if($nombre_apartado[0]!="certificadoconsumos")
					{
?>
						<div class="bolo_submenu2"></div>
						<div class="posicion_submenu_subseccion separacion_submenu_subseccion"><a href="oficina_informacion_certificadoconsumos.php?id=<?=$_SESSION["idioma"]?>" class="submenu_subseccion"><?=$oficina_submenu5_4?></a></div>
<?php
	                }//if($nombre_apartado[0]!="certificadoconsumos")
        	        else
    	            {
?>				<div class="bolo_submenu2"><img src="../img/comun/bolo_submenu2.gif" alt=""/></div>
						<div class="posicion_submenu_subseccion separacion_submenu_subseccion submenu_subseccion_seleccionado"><?=$oficina_submenu5_4?></div>
<?php
                	}//else($nombre_apartado[0]!="certificadoconsumos")

				?><div class="submenu_filete_intermenu"></div><?php

						if($nombre_subseccion[0]!="mediosdepago")
				{
?>
   					<div class="bolo_submenu"><i class="fa fa-credit-card" aria-hidden="true"></i>&nbsp;</div>
        	       	<div class="posicion_submenu_texto separacion_submenu"><a href="oficina_mediosdepago_pagoelectronico.php?id=<?=$_SESSION["idioma"]?>" class="submenu">Medios de Pago</a></div>

<?php
				}//if($nombre_subseccion[0]!="mediosdepago")
				else
				{
?>
					<div class="bolo_submenu"><i class="fa fa-credit-card" aria-hidden="true"></i>&nbsp;</div>
               		<div class="posicion_submenu_texto separacion_submenu submenu_seleccionado">Medios de Pago</div>
<?php
				}//else($nombre_subseccion[0]!="mediosdepago")






				if($nombre_apartado[0]!="consultadeuda")
					{
?>
						<div class="bolo_submenu2"></div>
						<div class="posicion_submenu_subseccion separacion_submenu_subseccion"><a href="oficina_mediosdepago_consultadeuda.php?id=<?=$_SESSION["idioma"]?>" class="submenu_subseccion">Consulta de Deuda</a></div>
<?php
	                }//if($nombre_apartado[0]!="consultadedeuda")
        	        else
    	            {
?>				<div class="bolo_submenu2"><img src="../img/comun/bolo_submenu2.gif" alt=""/></div>
						<div class="posicion_submenu_subseccion separacion_submenu_subseccion submenu_subseccion_seleccionado">Consulta de Deuda</div>
<?php
                	}//else($nombre_apartado[0]!="consultadedeuda")








				if($nombre_apartado[0]!="pagoelectronico")
					{
?>
						<div class="bolo_submenu2"></div>
						<div class="posicion_submenu_subseccion separacion_submenu_subseccion"><a href="oficina_mediosdepago_pagoelectronico.php?id=<?=$_SESSION["idioma"]?>" class="submenu_subseccion">Pago Electr&oacute;nico</a></div>
<?php
	                }//if($nombre_apartado[0]!="pagoelectronico")
        	        else
    	            {
?>				<div class="bolo_submenu2"><img src="../img/comun/bolo_submenu2.gif" alt=""/></div>
						<div class="posicion_submenu_subseccion separacion_submenu_subseccion submenu_subseccion_seleccionado">Pago Electr&oacute;nico</div>
<?php
                	}//else($nombre_apartado[0]!="pagoelectronico")










		?><?php


/*

				if($nombre_subseccion[0]!="lecturas")
				{
?>
   					<div class="bolo_submenu"><i class="fa fa-chevron-right" aria-hidden="true"></i></div>
        	       	<div class="posicion_submenu_texto separacion_submenu"><a href="oficina_lecturas.php?id=<?=$_SESSION["idioma"]?>" class="submenu"><?=$oficina_submenu6?></a></div>

<?php
				}//if($nombre_subseccion[0]!="lecturas")
				else
				{
?>
					<div class="bolo_submenu"><i class="fa fa-chevron-right" aria-hidden="true"></i></div>
               		<div class="posicion_submenu_texto separacion_submenu submenu_seleccionado"><?=$oficina_submenu6?></div>
<?php
				}//else($nombre_subseccion[0]!="lecturas")

?><div class="submenu_filete_intermenu"></div><?php

				if($nombre_subseccion[0]!="avisos")
			{
?>
   				<div class="bolo_submenu"><i class="fa fa-chevron-right" aria-hidden="true"></i></div>
               	<div class="posicion_submenu_texto separacion_submenu"><a href="oficina_avisos_cortes.php?id=<?=$_SESSION["idioma"]?>" class="submenu"><?=$oficina_submenu3?></a></div>

<?php
			}
			else
			{
?>
				<div class="bolo_submenu"><i class="fa fa-chevron-right" aria-hidden="true"></i></div>
               	<div class="posicion_submenu_texto separacion_submenu submenu_seleccionado"><?=$oficina_submenu3?></div>
<?php
			}


					if($nombre_apartado[0]!="noticias")
				{
?>                   <div class="bolo_submenu2"><img src="../img/comun/bolo_submenu2b.gif" alt=""/></div>                     <div class="posicion_submenu_subseccion separacion_submenu_subseccion"><a href="oficina_avisos_noticias.php?id=<?=$_SESSION["idioma"]?>" class="submenu_subseccion"><?=$descripcion_noticias_titulo?></a></div>
<?php
                }
                else
                {
?>				<div class="bolo_submenu2"><img src="../img/comun/bolo_submenu2.gif" alt=""/></div>
					<div class="posicion_submenu_subseccion separacion_submenu_subseccion submenu_subseccion_seleccionado"><?=$descripcion_noticias_titulo?></div>
<?php
                }



					if($nombre_apartado[0]!="cortes")
				{
?>                   <div class="bolo_submenu2"><img src="../img/comun/bolo_submenu2b.gif" alt=""/></div>
					<div class="posicion_submenu_subseccion separacion_submenu_subseccion"><a href="oficina_avisos_cortes.php?id=<?=$_SESSION["idioma"]?>" class="submenu_subseccion"><?=$descripcion_avisos_titulo?></a></div>
<?php
                }//if($nombre_apartado[0]!="titular")
                else
                {
?>				<div class="bolo_submenu2"><img src="../img/comun/bolo_submenu2.gif" alt=""/></div>
					<div class="posicion_submenu_subseccion separacion_submenu_subseccion submenu_subseccion_seleccionado"><?=$descripcion_avisos_titulo?></div>
<?php
                }//else($nombre_apartado[0]!="titular")

*/




?><?php
//avisos_incidencias.php -> incidencias.php
				if($nombre_subseccion[0]!="incidencias")
				{
?>
   					<div class="bolo_submenu"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>&nbsp;</div>
        	       	<div class="posicion_submenu_texto separacion_submenu"><a href="oficina_incidencias.php?id=<?=$_SESSION["idioma"]?>" class="submenu"><?=$oficina_submenu7?></a></div>

<?php
				}//if($nombre_subseccion[0]!="incidencias")
				else
				{
?>
					<div class="bolo_submenu"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>&nbsp;</div>
               		<div class="posicion_submenu_texto separacion_submenu submenu_seleccionado"><?=$oficina_submenu7?></div>
<?php
				}//else($nombre_subseccion[0]!="incidencias")
?><div class="submenu_filete_intermenu"></div><?php
				if($nombre_subseccion[0]!="sugerencias")
				{
?>
   					<div class="bolo_submenu"><i class="fa fa-comments-o" aria-hidden="true"></i>&nbsp;</div>
        	       	<div class="posicion_submenu_texto separacion_submenu"><a href="oficina_sugerencias.php?id=<?=$_SESSION["idioma"]?>" class="submenu"><?=$oficina_submenu8?></a></div>

<?php
				}//if($nombre_subseccion[0]!="sugerencias")
				else
				{
?>
					<div class="bolo_submenu"><i class="fa fa-comments-o" aria-hidden="true"></i>&nbsp;</div>
               		<div class="posicion_submenu_texto separacion_submenu submenu_seleccionado"><?=$oficina_submenu8?></div>
<?php
				}//else($nombre_subseccion[0]!="sugerencias")

?><div class="submenu_filete_intermenu"></div>

			    <div class="bolo_submenu"><i class="fa fa-bar-chart" aria-hidden="true"></i>&nbsp;</div>
               	<div class="posicion_submenu_texto separacion_submenu"><a href="../oficina_virtual/graficos_curvas.php?id=<?=$_SESSION["idioma"]?>" class="submenu">Gr&aacute;ficos</a></div>                
                <div class="submenu_filete_intermenu"></div><?php
//CASO PARTICULAR: Como hay dos secciones que contienne la palabra informacion como la primera del nombre, para esta entrada del menu se tendran 
//en cuenta las dos primeras palabras, para que solo se marque cuando le corresponda
?>
			    <div class="bolo_submenu2"></div>
               	<div class="posicion_submenu_subseccion separacion_submenu_subseccion"><a href="../oficina_virtual/graficos_curvas.php?id=<?=$_SESSION["idioma"]?>" class="submenu_subseccion">Curvas de Carga</a></div>                
                <div class="submenu_filete_intermenu"></div>
				
				
   				<div class="bolo_submenu2"></div>
               	<div class="posicion_submenu_subseccion separacion_submenu_subseccion"><a href="../oficina_virtual/graficos_consumos.php?id=<?=$_SESSION["idioma"]?>" class="submenu_subseccion">Consumos</a></div>
                <div class="submenu_filete_intermenu"></div>
                
                
   				<div class="bolo_submenu2"></div>
        	    <div class="posicion_submenu_subseccion separacion_submenu_subseccion"><a href="../oficina_virtual/graficos_importes.php?id=<?=$_SESSION["idioma"]?>" class="submenu_subseccion">Importes</a></div>                
                <div class="submenu_filete_intermenu"></div>
                
                <div class="bolo_submenu2"></div>
        	    <div class="posicion_submenu_subseccion separacion_submenu_subseccion"><a href="../oficina_virtual/graficos_kwh.php?id=<?=$_SESSION["idioma"]?>" class="submenu_subseccion">Consumos/Costes</a></div>                
                <div class="submenu_filete_intermenu"></div> 
<?php


		break;
	}//switch($nombre_seccion)


//Los elementos que aparecen a continuacion son fijos para todas las secciones, por lo tanto tambien los incluimos en el menu
?>
<!--Se crea una capa a modo de filete por la incompatibilidad de explorer 7 con la etiqueta hr-->
<div class="submenu_filete_inferior"></div>

<div class="banner_averias">
	<div class="texto_banner_averias">
	   <?=$tlf_averias?>
    </div>
</div><!--<div class="banner_averias">	-->

<div class="limpiar"></div>

<div class="banner_sigeenergia">

    <div class="banner_sigeenergia_imagen">
<?php
//La ruta de la imagen si se accede desde la oficina virtual
		if($_SESSION['oficina_virtual']!=1)
		{
?>
			<!-- <a href="http://www.audinforsystem.es/" ><img src="img/comun/banner_sigeenergia.jpg" border="0" alt="<?=$menu5?>" title="<?=$menu5?>" /></a> -->				
			<a href="http://www.audinforsystem.es/" target="_blank"><button class="btn_volver" alt="<?=$menu5?>" title="<?=$menu5?>">volver a la web corporativa</button></a>
<?php
		}//if($_SESSION['oficina_virtual']!=1)
		else
		{
?>
			<!-- <a href="http://www.audinforsystem.es/" ><img src="../img/comun/banner_sigeenergia.jpg" border="0" alt="<?=$menu5?>" title="<?=$menu5?>" /></a> -->
			<a href="http://www.audinforsystem.es/" target="_blank"><button class="btn_volver" alt="<?=$menu5?>" title="<?=$menu5?>">volver a la web corporativa</button></a>
			
<?php
		}//else($_SESSION['oficina_virtual']!=1)
?>
	</div><!--<div class="banner_sigeenergia_imagen">-->

	<div>
    	<a href="http://www.audinforsystem.es/" class="banner_sigeenergia_texto"></a>
	</div>

</div><!--<div class="banner_sigeenergia">-->

<div class="limpiar"></div>



<div class="pie_logo">
<?php
//La ruta de la imagen si se accede desde la oficina virtual
		if($_SESSION['oficina_virtual']!=1)
		{
?>
			<a href="http://www.audinforsystem.es" target="_blank"><img src="img/comun/logo_audinfor_pequeno.gif" border="0" alt="<?=$pie_imagen;?>" title="<?=$pie_imagen;?>" /></a>

<?php
		}//if($_SESSION['oficina_virtual']!=1)
		else
		{
?>
			<a href="http://www.audinforsystem.es" target="_blank"><img src="../img/comun/logo_audinfor_pequeno.gif" border="0" alt="<?=$pie_imagen;?>" title="<?=$pie_imagen;?>" /></a>
<?php
		}//else($_SESSION['oficina_virtual']!=1)
?>

</div><!--<div class="pie_logo">-->

<div class="posicion_realizado_audinfor_seccion">
	<a href="http://www.audinforsystem.es" target="_blank" class="realizado_audinfor"><?=$pie?></a>

</div>
<?php /*?><div class="posicion_exploradores_recomendados">
	<a href="http://windows.microsoft.com/es-ES/internet-explorer/products/ie/home" align="left" target="_blank" class="realizado_audinfor"><strong>Web Optimizada para:<br />
- Internet Explorer 8 o Superior<br />
- Firefox 7 o Superior</strong></a>
</div><!--<div class="posicion_realizado_audinfor_seccion">-->
<?php */?>
<div class="limpiar"></div>





