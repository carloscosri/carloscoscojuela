<?php
//Llamamos a la funcion que nos devuelve el nombre del script en ejecucion, para hacer el enlace a los idiomas correcto
	$pagina_actual=nombre_script_en_ejecucion();

	//Los nombres de las subsecciones deberan de ir precedidos del nombre de la seccion separado por un guion bajo (empresa_descripcion.php). De esta forma podemos automatizar el cambio de foto de la cabecera. Como hay una fotografia por seccion esta se llamara igual que la seccion
//cabecera
	$nombre_seccion=explode("_",$pagina_actual);

//Dependiendo de la seccion activa marcaremos una seccion u otra del menu


	if($nombre_seccion[0]!="oficina")
	{
//Los enlaces del menu variaran dependiendo de si se pulsa desde la oficina virtual
		if($_SESSION['oficina_virtual']!=1)
		{

//Este enlace es condicional, si el usuario no esta identificado en la oficina virtual se accedera a la seccion de login y si si lo esta a la seccion de presentacion
			if(isset($_SESSION["usuario"]))
			{
?>
                <?php /*?><div class="posicion_componente_menu espacio_componente_menu"><a href="oficina_virtual/oficina_presentacion.php?id=<?=$_SESSION["idioma"]?>" class="menu"><?=$menu4?></a></div>	<?php */?>
<?php
			} //if(isset($_SESSION["usuario"]))

			else
			{
?>
                <?php /*?><div class="posicion_componente_menu espacio_componente_menu"><a href="oficina_virtual/oficina_index.php?id=<?=$_SESSION["idioma"]?>" class="menu"><?=$menu4?></a></div>	<?php */?>
<?php
			}//else(isset($_SESSION["usuario"]))
		}//if($_SESSION['oficina_virtual']!=1)
		else
		{

			if(isset($_SESSION["usuario"]))
			{
?>
               <?php /*?> <div class="posicion_componente_menu espacio_componente_menu"><a href="../oficina_virtual/oficina_presentacion.php?id=<?=$_SESSION["idioma"]?>" class="menu"><?=$menu4?></a></div>	<?php */?>
<?php
			}//if(isset($_SESSION["usuario"]))

			else
			{
?>
              <?php /*?>  <div class="posicion_componente_menu espacio_componente_menu"><a href="../oficina_virtual/oficina_index.php?id=<?=$_SESSION["idioma"]?>" class="menu"><?=$menu4?></a></div><?php */?>
<?php
			}//else(isset($_SESSION["usuario"]))
		}//else($_SESSION['oficina_virtual']!=1)
	}//if($nombre_seccion[0]!="oficina")

	else
	{
?>
		<?php /*?><div class="posicion_componente_menu_seleccionado espacio_componente_menu menu_seleccionado"><?=$menu4?></div>		<?php */?>
<?php
	}//else($nombre_seccion[0]!="oficina")
?>

<!--En cualquier caso acontinuacion siempre ira el menu de idiomas, limpiaremos los alementos flotantes para no afectar al resto del contenido y
por ultimo se a�de el filete-->
<div class="posicion_menu_idiomas">
<?php
//La ruta del archivo de idiomas, cambiara dependiendo de si nos encontramos o no en la oficina virtual
	if($_SESSION['oficina_virtual']!=1)
	{
			include($_SESSION["directorio_raiz"]."includes/menu_idiomas.php");
	}//if($_SESSION['oficina_virtual']!=1)
	else
	{
		include("../includes/menu_idiomas.php");
	}//else($_SESSION['oficina_virtual']!=1)
?>
</div>

<div class="limpiar"></div>

<!--Creamos una capa que haga de filete, por la incompatibilidad de explorer 7 con el correcto visionado de esa etiqueta-->
