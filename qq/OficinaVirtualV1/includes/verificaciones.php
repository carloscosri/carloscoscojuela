<?php
/**********************************************************VERIFICACIONES Y MENSAJES DE ERROR*******************************************************/
/***************************************************************************************************************************************************/

//Verifica que un campo solo tenga caracteres numericos 0 si todo numeros 1 si hay otro caracter
function solo_numero($campo)
{
	$Error=0;
	$x=0;
	//En campo2 se queda la cadena solo con los caracteres alfanumericos
//Se puede probar con la cadena $cadena="abcABC123!!ºª\@#·$~%&¬/()=?'¿¡€`^[]*+´¨{}ç,;.:-_456abcABC";
    $campo2 = preg_replace("[^A-Za-z0-9]","", $campo);
	if ($campo==$campo2)
	{
		While ($x<strlen($campo))
		{
			if (!is_numeric($campo[$x])){
				$Error=1;
				break;
			}//if is_numeric 
			
//Si se encuentra un error sale de las comprobaciones
			else{$x++;}//if else error
		}//While strlen
	}//if $campo==$campo2
	else
	{
		$Error=1;
	}//else $campo==$campo2
		return $Error;
}//function solo_numero($campo)

//Verifica que un campo solo tenga caracteres no numericos 0->si todo numeros. 1-> si hay otro caracter
function solo_letra($campo)
{
	$Error=0;
	$x=0;
//En campo2 se queda la cadena solo con los caracteres alfanumericos Y ESPACIOS
//Se puede probar con la cadena $cadena="abcABC123!!ºª\@#·$~%&¬/()=?'¿¡€`^[]*+´¨{}ç,;.:-_456abcABC";
$campo2 = preg_replace("[^A-Za-z0-9 ñÑáéíóúÁÉÍÓÚäëïöüÄËÏÖÜ]","", $campo);	if ($campo==$campo2)
	{
		While ($x<strlen($campo))
		{
			if (is_numeric($campo[$x])) {$Error=1;}//if is_numeric 
			
			//Si se encuentra un error sale de las comprobaciones
			if ($Error==1){break;}//if error
			else{$x++;}//if else error
		}//While strlen
	}//if $campo==$campo2
	else
	{
		$Error=1;
	}//else $campo==$campo2
		return $Error;
}//function solo_letra($campo)

//Verifica si un email a sido escrito correctamente
function comprobar_email($email)
{	
    $mail_correcto = 0;

//Longitud minimo de 6,que solo tenga 1 @,que el 1º caracter no sea la @, que el penultimo caracter tampoco sea @
    if ((strlen($email) >= 6) && (substr_count($email,"@") == 1) && (substr($email,0,1) != "@") && (substr($email,strlen($email)-1,1) != "@")){

//Que no tenga comillas,ni barras,ni doble barra, ni barra $,ni espacios

       if ((!strstr($email,"'")) && (!strstr($email,"\"")) && (!strstr($email,"\\")) && (!strstr($email,"\$")) && (!strstr($email," "))) {
          //Tiene que tener un solo punto
          if (substr_count($email,".")>= 1){
//Obtengo desde el ultimo punto la terminacion del dominio
             $term_dom = substr(strrchr ($email, '.'),1);
             
//Se comprueba que la terminacion del dominio tenga entre 1 y 4 caracteres y que no contenga @
             if (strlen($term_dom)>1 && strlen($term_dom)<5 && (!strstr($term_dom,"@")) ){
                
//Compruebo que lo de antes del dominio sea correcto
		
		$antes_dom = substr($email,0,strlen($email) - strlen($term_dom) - 1);
                $caracter_ult = substr($antes_dom,strlen($antes_dom)-1,1);

//Si el ultimo caracter de lo de antes del dominio no es @ ni . el mail es correcto
                if ($caracter_ult != "@" && $caracter_ult != ".")
				{
                   $mail_correcto = 1;
                }//if carcater_ult
             }//if comprobacion del dominio
          }//if comprobacion de si tiene un punto
       }//if comprobacion de carcteres prohibidos
    }//if comprobaciones iniciales

    if ($mail_correcto)
       return 1;
    else
       return 0;
}//function comprobar_email($email) 

//Esta funcion cambia una fecha normal, en formato SQL para guardarla en la base de datos
function fecha_mysql($fecha)
{
	$dia=substr($fecha,0,2);
	$mes=substr($fecha,3,2);
	$año=substr($fecha,6,4);
	$lafecha=$año."-".$mes."-".$dia;
    return $lafecha;
}//function fecha_mysql($fecha)

//Esta funcion devuelve los dias del mes que tiene el numero de mes que recibe como parametro
function obtener_dias_mes($num_mes,$ano)
{	
	switch($num_mes)
	{
		case 1:return 31;//enero
//En este caso no siempre sera el mismo numero de dias, ya que en los años bisiestos tendra 29 dias.		
		case 2://febrero

//Esta variable almacenara el numero de dias del año		
			$dias_ano=obtener_dias_ano($ano);
			
//Si el año es bisiesto , significa que febrero tendra 29 dias
			if($dias_ano==366)
			{
				return 29;						
			}//if($dias_ano==366)
//Si el año no es bisiesto febrero tendra 28 dias
			else
			{
				return 28;			
			}//else($dias_ano==366)
		break;
		
		case 3:return 31;//marzo
		case 4:return 30;//abril
		case 5:return 31;//mayo
		case 6:return 30;//junio
		case 7:return 31;//julio
		case 8:return 31;//agosto
		case 9:return 30;//septiembre
		case 10:return 31;//ocubre
		case 11:return 30;//noviembre
		case 12:return 31;//diciembre
	}//switch($num_mes)
}//function obtener_dias_mes($num_mes)

//Esta funcion devuelve los dias del año que recibe como parametro teniendo en cuenta si el año es o no es bisiesto
function obtener_dias_ano($ano)
{	
//Si un año es divisible por 4, puede que sea bisiesto, pero son necesarias mas comprobaciones
	if($ano%4==0)
	{
//Si es multiplo de 4 y no es multiplo de 100 ,no hace falta mas comprobaciones sera bisiesto
		if($ano%100!=0)
		{
			return 366;													
		}//if($ano%100!=0)
		
//Si el año es mulriplo de 4 y termina en 00 (es multiplo de 100) 
		else
		{
//El año sera bisiesto si es divisible por 400		
			if($ano%400==0)
			{
				return 366;
			}//if($ano%400==0)			
//Si el año es multiplo de 4 y no es multiplo de 400
			else
			{
				return 365;										
			}//else($ano%400==0)
		}//if($ano%100==0)
		
	}//if($ano%4==0)
	
//Si el año no es multiplo de 4 no hace falta realizar mas comprobaciones, el año NO sera bisiesto
	else
	{
		return 365;			
	}//else($ano%4==0 and $ano%100!=0)						
}//function obtener_dias_mes($num_mes,$ano)

//Esta funcion devuelve el numero de dias trascurridos entre las dos fechas recibidas como parametro. Hace uso de la funcion obtener_dias_mes($num_mes) y  obtener_dias_ano($ano). Tiene en cuanta años bisiestos          
function dias_trascurridos($fecha_inicial,$fecha_final)
{
//Subdividimos ambas fechas en dia, mes y año.    
	list($dia_inicial,$mes_inicial,$ano_inicial) = explode("-",$fecha_inicial);
    list($dia_final,$mes_final,$ano_final) = explode("-",$fecha_final);
	
//Se recorren los meses del año que han pasado hasya el mes incial, y se van sumando los dias que tiene cada uno			                
    for($x=1;$x<=$mes_inicial ;$x++)
	{
		$dias_pasados_inicial += obtener_dias_mes($x,$ano_inicial);						
	}//for($x=1;$x <=$mes_inicial ;$x++)
			
//Ahora la variabke "dias_pasados_inicial" contiene el numero de dias hasta el mes inicial, para hacerlo exacto, hay que restarle el numero de dias trascurridos en el mes inicial	
	$dias_pasados_inicial-=(obtener_dias_mes($mes_inicial,$ano_inicial)-$dia_inicial);
			
//Ahora se repite el proceso con el mes de la fecha final.
	for($x=1;$x <=$mes_final ;$x++)
	{
		$dias_pasados_final += obtener_dias_mes($x,$ano_final);
	}//for($x=1;$x <=$mes_final ;$x++)
				
//Ahora la variabke "dias_pasados_final" contiene el numero de dias hasta el mes final, para hacerlo exacto, hay que restarle el numero de dias trascurridos en el mes final
	$dias_pasados_final-=(obtener_dias_mes($mes_final,$ano_final)-$dia_final);
	
//Esta variable almacenara la diferencia de años que hay entre las dos fechas.
	$total_anos=$ano_final-$ano_inicial;
	
//Si el año inicial y el final no es el mismo
    if($total_anos!= 0)
	{	
//Si hay mas de un año de diferencia entre las fechas calcularemos los dias trascurridos en los años de diferencia		
		if(abs($total_anos)!=1)
		{
//Si la diferencia de años es negativa (año final menor que el año inicial) para calcular correctamente la diferencia de dias entre las fechas habra que sustituir el valor de la diferencia de años por su valor absoluto, de otro modo no realizaria correctamente el calculo
			if($total_anos<0)
			{
				$total_anos=abs($total_anos);
			}//if($total_anos>0)
			
//En primer lugar calcularemos el numero total de dias trascurridos en los años de diferencia entre las dos fechas
			for($x=$ano_inicial;$x<$ano_final ;$x++)
			{
				$total_dias_anos += obtener_dias_ano($x);
			}//for($x=$ano_inicial;$x<$ano_final ;$x++)
						
		}//if(abs($total_anos)!=1)
		
//Si solo hay un año de diferencia entre las fechas, podemos calcular los dias trascurridos simplemente llamando a la funcion que calcula los dias de un año, pasandole como parametro el año de la fecha final
		else
		{
			$total_dias_anos += obtener_dias_ano($ano_final);		
		}//else(abs($total_anos)!=1)
				
//Una vez calculado el numero de dias trascurridos, si la diferencia entre los años era negativa, el resultado del calculo se debera de hacer tambien negativo, si no las cuentas saldran falseadas
		if($ano_final-$ano_inicial<0)
		{
			$total_dias_anos=$total_dias_anos*-1;
		}//if($total_anos<0)
		
//EL NUMERO DE DIAS TRASCURRIDOS EN LOS AÑOS DE DIFERENCIA ENTRE LAS FECHAS, restandole el numero de dias que han pasado desde que comenzo el año de la fecha inicial y sumandole el numero de dias que han trascurrido desde la fecha final        		                        
		$total_dias= $total_dias_anos-$dias_pasados_inicial + $dias_pasados_final;
		
	}//if(($total_anos=$ano_final-$ano_inicial)!= 0)
			
//Si el año de la fecha inicial y la final coinciden 
	else
	{	
//Si el mes inicial es menor que el mes final, es decir si la diferencia de fechas va a ser positiva haremos el calculo de un modo
		if($mes_inicial<$mes_final)
		{				
//Realizamos el proceso de los dias trascurridos durante el año, pero en esta ocasion lo acumularemos directamente en el total de dias	
			for($x = $mes_inicial; $x<$mes_final;$x++)
			{
				$total_dias += obtener_dias_mes($x,$ano_inicial);
			}//for($x = $mes_inicial; $x<=$mes_final;$x++)		

//En este caso para calcular el total de dias trascurridos, al numero de dias que tiene el año actual se le resta el total de dias trascurridos entre la fecha inicial y la final. Tambien habrá que restar los dias trascurridos del mes inicial y sumar los dias transcurridos desde el final
			$total_dias = $total_dias-$dia_inicial + $dia_final;     			
												
		}//if($mes_inicial<$mes_final)
		
//Si la fecha final es mayor que la inicial (siendo ambas pertenecientes al mismo año) haremos el calculo de otra forma. Teniendo en cuenta los meses trascurridos en cada una de las fechas, al igual que se ha hecho para el calculo si las fechas son de distinto año
		else
		{
//En este caso el total de dias de diferencia sera los dias del año que han trascurrido desde la fecha final menos los que han trascurrido desde la fecha inicial. No hay que introducir nada extra para tener en cuenta los años bisiestos, ya que el calculo de los dias que tiene cada mes ya lo tiene en cuenta
			$total_dias= -$dias_pasados_inicial + $dias_pasados_final;
		}//else($mes_inicial<$mes_final)
			
	}//else(($total_anos!= 0)
				            
	return $total_dias;                        
	
}//function dias_trascurridos($fecha_inicial,$fecha_final)

//Esta funcion comprueba si una fecha esta escrita correctamente.
function comprobar_fecha($fecha)
{
	$Error=0;

//Subdividimos la fecha usando el guion como caracter separadoe	
	$partes_fecha=explode("-",$fecha);
	
	if (count($partes_fecha)!=3 or $partes_fecha[0]=="" or $partes_fecha[1]=="" or $partes_fecha[2]=="" or solo_numero($partes_fecha[0]) or solo_numero($partes_fecha[1]) or solo_numero($partes_fecha[2]))
	{
		$Error=1;
	}//if (count($partes_fecha)!=3 or $partes_fecha[0]=="" or $partes_fecha[1]=="" or $partes_fecha[2]=="" or solo_numero($partes_fecha[0]) or solo_numero($partes_fecha[1]) or solo_numero($partes_fecha[2]))
		
//Si las partes de las fechas son correctas, pasaremos a comprobar el contendido de las mismas
		else
		{
			if (!checkdate($partes_fecha[1],$partes_fecha[0],$partes_fecha[2]))
			{
				$Error=1;		
			}//if (!checkdate($partes_fecha[1],$partes_fecha[0],$partes_fecha[2]))
		}//else (count($partes_fecha)!=3 or $partes_fecha[0]=="" or $partes_fecha[1]=="" or $partes_fecha[2]=="" or solo_numero($partes_fecha[0]) or solo_numero($partes_fecha[1]) or solo_numero($partes_fecha[2]))
		
	return $Error;	
}//function comprobar_fecha($fecha)

//Esta funcion compara 2 fechas y comprueba:

//Que ninguna de las 2 este vacia
//Que las fechas esten escritas correctamente
//Que las fechas no sean posteriores a la actual
//Que la fecha de inicio no sea superior a la de fin
function comparar_fechas($fecha_inicio,$fecha_fin)
{
//Esta variable controlara si se produce algun error en las verificaciones		
	$error=0;
			
//Comprobamos que se han escrito todos los campos obligatorios
	if($fecha_inicio=="" or $fecha_fin=="")
	{
		$error=1;
	}//if($fecha_inicio=="" or $fecha_fin=="")

//Se comprueba qur las fechas esten escritas correctamente
	if($error==0 and (comprobar_fecha($fecha_inicio) or comprobar_fecha($fecha_fin)))
	{
		$error=2;
	}//if($error==0 and (comprobar_fecha($fecha_inicio) or comprobar_fecha($fecha_fin)))

//Si no se ha producido un error anteriormente pasaremos a comprobar las fechas
	if ($error==0)
	{
//Esta variable capturara la fecha actual del sistema, la usaremos para comparar con las fechas escritas por el usuario	
		$fecha_actual=date("d-m-Y");
				
//Estas variables almacenaran los dias trascurridos desde la fecha de inicio y la fecha de fin a la fecha actual, respectivamente
		$diferencia_fecha_inicio=dias_trascurridos($fecha_inicio,$fecha_actual);									
		$diferencia_fecha_fin=dias_trascurridos($fecha_fin,$fecha_actual);			
		
//Comprobamos que las fechas introducidas por el usuario no sean posteriores a la fecha actual
		if($diferencia_fecha_inicio<0 or $diferencia_fecha_fin<0)
		{
			$error=3;			
		}//if($diferencia_fecha_inicio<0 or $diferencia_fecha_fin<0)

//Si las fechas estan escritas correctamente y son anteriores a la actual		
		else
		{
			$diferencia_fechas=dias_trascurridos($fecha_inicio,$fecha_fin);			
						
//Comprobamos que la fecha de inicio no sea mayor que la fecha de final
			if($diferencia_fechas<0)
			{
				$error=4;			
			}//if($diferencia_fecha_inicio<0 or $diferencia_fecha_fin<0)
		}//else($diferencia_fecha_inicio<0 or $diferencia_fecha_fin<0)		
	}//if ($error==0)
	
	return $error;
}//function comparar_fechas($fecha_inicio,$fecha_fin)


//Que un numero decimal estes escrito con el formato adecuado Devuelve: 0->Correcto. 1-> Incorrecto. Hay que pasarle los numeros decimales con la coma como caracter separador
function comprobar_decimal($numero,$long_parte_entera,$long_parte_decimal)
{
//Con esta variable comprobaremos si se produce un error o no
	$error=0;
					
//Ahora subdividimos el numero de contador en la parte entera y la parte decimal
	$partes_numero=explode(",",$numero);
		
//El numero decimal debe cumplir los siguientes requisitos:
//No puede haber mas de un caracter separador (una sola coma). 
	if(count($partes_numero)>2)
	{
		$error=1;
	}//if(count($partes_numero)>2)					
	else
	{	
//En el caso de que el numero sea decimal, es decir que se haya encontrado una coma
		if(count($partes_numero)!=1)
		{
//Compobamnos que la coma no se encuentre ni en la primera ni en la ultima posicion		
			if(strrpos($numero, ",")==0 or strrpos($numero, ",")==strlen($numero)-1)
			{
				$error=1;							
			}//if(strrpos($numero, ",")==0 or strrpos($numero, ",")==strlen($numero)-1))	
						
//Si la coma esta en la posicion adecuada, continuaremos las comprobaciones
			else
			{														
//Las partes del numero deben ser completamente numericas y ademas la parte entera debe tener un maximo de la longitud de la parte entera recibida como parametro y lo mismo sucede con la parte decimal 
				if(solo_numero($partes_numero[0]) or solo_numero($partes_numero[1]) or strlen($partes_numero[0])>$long_parte_entera or strlen($partes_numero[1])>$long_parte_decimal)
				{
					$error=1;			
				}//if(solo_numero($partes_numero[0]) or solo_numero($partes_numero[1]) or strlen($partes_numero[0])>$long_parte_entera or strlen($partes_numero[1])>$long_parte_decimal)
			}//if(strrpos($numero, ",")==0 or strrpos($numero, ",")==strlen($numero)-1))		
		}//if(count($partes_numero)!=1)

//Si el numero es entero (no tiene coma) debe ser completamente numerico y tener el maximo numero de caracteres de la parte entera
		else
		{						
			if(solo_numero($partes_numero[0]) or strlen($partes_numero[0])>$long_parte_entera)
			{
				$error=1;						
			}//if(solo_numero($partes_numero[0]) or strlen($partes_numero[0])>$long_parte_entera)
		}//else(count($partes_numero)!=1)
	}//else(count($partes_numero)>2)
		
	return $error;
}//function comprobar_decimal($long_parte_entera,$long_parte_decimal)

//Esta funcion comprueba si un DNI ha sido escrito correctamente
function comprobar_dni($dni)
{	
//Con esta variable comprobaremos si se produce un error o no
	$error=0;
	
//Primero comprobamos si el numero de caracteres es 9
	if(strlen($dni)<9)
	{
		$error=1;
	}//if(strlen($dni)<9)
	else
	{		
//Si la longitud del DNI es de 9 caracteres haremos el resto de las comprobaciones, si no lo daremos por bueno
		if(strlen($dni)==9)
		{		
//Sudividimos el DNI en la parte numerica y la letra		
			$numero=substr($dni,0,8);
			$letra=substr($dni,8,9);
								
//Comprobamos si el numero solo contiene caracteres numericos y la letra es realmente una letra			
			if(solo_numero($numero) or solo_letra($letra))
			{
				$error=1;
			}//if($solo_numero($numero) or solo_letra($letra))

//Si las comprobaciones han sido exitosas la ultima comprobacion es si la letra es correcta
			else
			{				
				$letra_correspondiente=letraNIF($numero);
				
				if($letra_correspondiente!=strtoupper($letra))
				{
					$error=1;
				}//if($letra_correspondiente!=strtoupper($letra))
			}//else($solo_numero($numero) or solo_letra($letra))											
		}//if(strlen($dni)==9)
	}//else(strlen($dni)<9)
	
	return $error;	
}//function comprobar_dni($dni)

//Esta funcion calcula la letra de un NIF dandole solo el numero, devuelve la letra correspondiente
function letraNIF($dni) 
{
    $valor= (int) ($dni / 23);
    $valor *= 23;
    $valor= $dni - $valor;
    $letras="TRWAGMYFPDXBNJZSQVHLCKEO";
    $letraNif= substr ($letras, $valor, 1);
    return $letraNif;
}//function letraNIF($dni) 

function comprobacion_digito_control($numero_cuenta)
{
	$valores = array(1, 2, 4, 8, 5, 10, 9, 7, 3, 6);
	
	$controlCS=0;
	$controlCC=0;

//Multiplicamos cada numero de la cuenta bancaria por los numeros del array de valores y los vamos sumando				
	for ($i=0; $i<=7; $i++)
	{
		$controlCS += intval(substr($numero_cuenta,$i,1)) * $valores[$i+2];
	}//for (i=0; i<=7; i++)

//Le restamos a 11 el resto de la division entre 11 y el numero obtenido en la operacion anterior 	
	$controlCS = 11 - ($controlCS % 11);

//Si el numero obtenido es 11, el digito de control sera 0 y es 10 sera 1, para los demas valores no se modificara	
	if ($controlCS == 11) $controlCS = 0;
	else if ($controlCS == 10) $controlCS = 1;
	
	for ($i=10; $i<=19; $i++)
	{
		$controlCC += intval(substr($numero_cuenta,$i,1)) * $valores[$i-10];
	}//for ($i=10; $i<=19; $i++)
	
	$controlCC = 11 - ($controlCC % 11);
	
	if ($controlCC == 11) $controlCC = 0;
	else if ($controlCC == 10) $controlCC = 1;
			
	if (substr($numero_cuenta,8,1)==$controlCS && substr($numero_cuenta,9,1)==$controlCC )
	{
		return true;
	}//if ($numero_cuenta.charAt(8)==$controlCS && $numero_cuenta.charAt(9)==$controlCC )
	
	else
	{
		return false;
	}//else ($numero_cuenta.charAt(8)==$controlCS && $numero_cuenta.charAt(9)==$controlCC )
	
}//function esCorrecto($numero_cuenta) 

//Esta funcion comprueba si lo que recibe como parametro está únicamente formado por numeros y letras
function comprobar_alfanumerico($cadena)
{
//Esta variable controlara si se ha producido un error o no en la verificacion de la cadena alfanumerica
	$error=0;
	
//Esta variable almacenara el numero de caracteres permitidos
   $caracteres_permitidos = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
   
//Recorremos la cadena recibida y comparamos si el caracter se encuentra dentro del conjunto de caracteres permitidos.
	for ($i=0; $i<strlen($cadena); $i++)
	{
		if (strpos($caracteres_permitidos, substr($cadena,$i,1))===false)
		{
			$error=1;
			return $error;
		}//if (strpos($caracteres_permitidos, substr($cadena,$i,1))===false)
	}//for ($i=0; $i<strlen($cadena); $i++)
	
	return $error;
}//function comprobar_alfanumerico($cadena)

//Esta funcion comprueba si un CUPS se ha escrito correctamente
function comprobar_cups($cups)
{
//Esta variable controlara si se ha producido un error o no en la verificacion del CUPS
	$error=0;
		
	if(strlen($cups)!=20 and strlen($cups)!=22)
	{
		$error=1;
	}//if(strlen($cups)!=20 or strlen($cups)!=22)	
	else
	{
//Estas variables almacenaran las diferentes partes del CUPS		
		$letras_principio=substr($cups,0,2);
		$numeros_intermedios=substr($cups,2,16);
		$letras_final=substr($cups,18,2);
				
//Si el cups es de 22 caacteres habrá que comprobar tambien los dos ultimos caracteres
		if(strlen($cups)==22)
		{
			$caracteres_extra=substr($cups,20,2);
		}//if(strlen($cups)==22)

		if(solo_letra($letras_principio) or solo_numero($numeros_intermedios) or solo_letra($letras_final) or comprobar_alfanumerico($caracteres_extra))
		{
			$error=1;	
		}//if(solo_letra($letras_principio) or solo_numero($numeros_intermedios) or solo_letra($letras_final))
					
	}//else(strlen($cups)!=20 or strlen($cups)!=22)
		
	return $error;
}//function comprobar_cups($cups)

//******************************************************(FIN)VERIFICACIONES FORMULARIOS**************************************---*******************
//*************************************************************************************************************************************************
?>
