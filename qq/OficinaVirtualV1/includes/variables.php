<?php
//Esta variable almacena la razon social de la empresa completo, lo usaremos en distintas ubicaciones
$nombre_empresa_completo="<strong>AUDINFOR SYSTEM, SL</strong>";
$direccion_empresa_completa="C/ PANAMA 2 LOCAL 2";

//Esta variable definira el nombre de la empresa. La usaremos para el envio de correos y otros fines
$nombre_empresa="AUDINFOR SYSTEM";

//Esta variable definira el mail de contacto de la empresa
$mail_empresa="info@audinforsystem.es";

//Esta variable definira el idioma con el que se abrira la web por defecto
$idioma_defecto="es";

//No es estandar introducir "&" directamente en la URL, por lo tanto definimos esta variable que contiene la sintaxis del simbolo en html, cada vez que queramos escribirlo en una URL, se hara referencia a esta variable
$ampersand="&amp;";

//Esta variable alamacenara los carcateres HTML para el espacio. Se utilizara para espaciar elementos peque�as distancias cuando no merezca la
//pena hacer capas u otro tipo de maquetacion
$espacio="&nbsp;";
?>