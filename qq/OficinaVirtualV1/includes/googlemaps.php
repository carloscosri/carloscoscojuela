<?php	
//Definimos la variable para sustituir al caracter "&" y asi valide en la W3C
	$ampersand="&amp;";

//PARA DOMINO AUDINFOR!!!!
//Esta variable almacenara la clave que nos facilite googlemaps cuando se pida para el dominio correspondiente
//	$clave_googlemaps="ABQIAAAAqJha7SPclKWLYiRMfHKUeRTHvsWfMCmNPKcCkVMu6gYW6KLajxRcev6aRiGMxEGUBMeLnm0AvXaL7Q";

//PARA DOMINO CLIENTE!!!!
	$clave_googlemaps="ABQIAAAAcAYAcrLH-hq6hl5DpKN5ZxTmowcvFJaGho5C_hz2Kb5C_wdhKRRFB7oG6dzMrKrYiB58gN2jLcNKMg";

	
	$ruta_api_google_con_clave ="http://maps.google.com/maps?file=api".$ampersand."v=2".$ampersand."key=".$clave_googlemaps;
		
//Ahora generaremos el codigo de javascript que llama a la API de googlemaps	
	echo("<script src='".$ruta_api_google_con_clave."' type='text/javascript'></script>")
?>

<script type="text/javascript">
/*
IMPORTANTE!!!!!!!!!!!!!!!!! PARA QUE VALIDE HAY QUE COMENTAR EL CODIGO. SIGUE FUNCIONANDO PERO NO DA LOS ERRORES EN EL W3C VALIDATOR. LOS ERRORES SE GENERAN PORQUE HAY CODIGO HTML INTRODUCIDO Y COMO EL CODIGO SE EJECUTA ANTES DE LA ETIQUETA BODY, EL INTERPRETA QUE NO PUEDE HABER
HTML ANTES DEL BODY, POR LO TANTO DA UN ERROR EN CADA ETIQUETA HTML QUE SE ENCUENTAR ENBEBIDA EN JAVASCRIPT
*/
<!--
//Función para cargar un mapa de Google.
//Esta función se llama cuando la página se ha terminado de cargar. Evento onload
function cargar_googlemaps() 
{	
//CAMBIO!!!!!
//Esta variable almacenara el titulo que se mostará en el bocadillo al hacer clic en la chincheta mostrada
	var titulo_bocadillo="Electra Avellana S.L.";
			
//Esta variable almacenara los datos de la empresa que se van a mostrar en el bocadillo. Si deseamos introducir un salto de linea NO PUEDE HACERSE
//simplemente con la etiqueta HTML de salto de linea, ya que nop valida. Por ello se ha creado un array en el que se introducira en cada posicion una sola linea de texto. Luego las posiciones se mostraran en el bocadillo en capas diferentes consecutivas 
	var datos_empresa = new Array()
	datos_empresa[0]="C/ del Terri, 29";	
	datos_empresa[1]="Cornellà del Terri, GIRONA 17844";
				
//Esta variable almacenara la ruta donde se encuentra la imagen del logo de la empresa
	var ruta_logo_empresa="http://www.audinforsystem.es/clientes/plantilla/img/empresa/icono_empresa_mini.gif";
	
//Estas variables almacenaran el tamaño del logo pequeño de la empresa 	
	var ancho_logo_empresa=28;
	var alto_logo_empresa=31;

/*AVERIGUAR COORDENADAS GPS:

http://www.multimap.com/

http://itouchmap.com/latlong.html -> MAS PRECISO, DA MAS DECIMALES

O DESDE LA URL DE GOOGLEMAPS SON LOS NUMEROS A CONTINUACION DEL PARAMETRO LL=
DESDE LA URL NO SE DAN LOS DECIMALES SUFICIENTES PARA TENER UNA PRECISION DEL 100%, POR LO TANTO SERA NECESARIO HACER PEQUEÑOS AJUSTES A MANO
*/

//Estas variables almacenaran las coordenadas GPS donde se centrara el mapa y se colocara la chincheta. Las unidades son numeros decimales no grados minutos y segundos
//Cuanto mayor mas arriba, cuanto menor mas abajo
	var coordenada_gps_latitud=42.0891524;
//Cuanto mayor mas a la derecha, cuanto menor mas a la izquierda	
	var coordenada_gps_longitud=2.8161389;
						
//Esta variable controlara el nivel de zoom con el que se muestra el mapa	
	var nivel_zoom=12;
	
//CAMBIO!!!!!	
//Comprobamos si el navegador es compatible con los mapas de google
	if (GBrowserIsCompatible()) 
	{		
//Instanciamos un mapa con GMap, pasándole una referencia a la capa donde queremos mostrar el mapa
         var mapa = new GMap2(document.getElementById("mapa_contacto"));   
		 		
//Definimos las coordenadas de latitud y longitud
		var coordenadas = new GLatLng(coordenada_gps_latitud,coordenada_gps_longitud);
//Centramos el mapa en una latitud y longitud deseadas y le asignamos el zoom        		
		 mapa.setCenter(coordenadas, nivel_zoom);   
		 
//Añadimos controles al mapa, para interacción con el usuario
         mapa.addControl(new GLargeMapControl());
         mapa.addControl(new GMapTypeControl());
         mapa.addControl(new GOverviewMapControl()); ;
		
//Activamos el zoom con la rueda del ratón		 
		mapa.enableScrollWheelZoom();
//Activamos que se pueda manejar con el teclado
		new GKeyboardHandler(mapa);
			
//Establecemos el tipo de mapa normal		 
		 mapa.setMapType(G_NORMAL_MAP);
 
//Para poner el mapa en vista satelite:		 
//         mapa.setMapType(G_SATELLITE_MAP); 

//COMPROBAR LAS COORDENADAS DEL CENTRO DEL MAPA
/*		 
	     var prueba = mapa.getCenter();
 	     var latitud = prueba.lat(); 
	     var longitud = prueba.lng();		 
		 alert("latitud: "+latitud+" longitud: "+longitud);
*/
		 	  
//Definimos el icono de la empresa
	     var icono_chincheta = new GIcon(G_DEFAULT_ICON);
      	 icono_chincheta.image = ruta_logo_empresa;
		 
//Definimos el tamaño del icono de la chincheta
      	 var tamano_icono = new GSize(ancho_logo_empresa,alto_logo_empresa);
      	 icono_chincheta.iconSize = tamano_icono;
		 		
//Funcion que crea un marcador nuevo		 
		 function crear_punto(coordenadas,titulo,datos) 
		{      
/*			
La clase GMarker recibe como parámetro un punto: ya sea relativo al mapa (GPoint) o una Latitud-Longitud (GlatLng). En nuestro caso nos hemos decantado por la segunda opción y hemos pasado como parámetro de la clase GMarker una clase GlatLng, ya que de la otra forma habría que 
ir ajustando a mano el punto, ya que la medida de referencia es el pixel, y de esta forma podemos poner las coordenadas que ya hemos averiguado
*/

//Para crear un punto con el icono por defecto
			var icono_chincheta = new GMarker(coordenadas, G_DEFAULT_ICON);

//CREAR CHINCHETA CON ICONO PEROSNALIZADO
//			var icono_chincheta = new GMarker(coordenadas, icono_chincheta);

		
//Añadimos un comportamiento al punto establecido en el mapa. Cuando se haga clic se abrirá el bocadillo         
			GEvent.addListener(icono_chincheta, 'click', function() 
			{			
//Aqui se puede personalizar el bocadillo que se abre al hacer clic

				icono_chincheta.openInfoWindowHtml("<div style='float:left; font-size: 14pt;'><img src='img/empresa/icono_empresa_mini.gif' alt='"+titulo_bocadillo+"' title='"+titulo_bocadillo+"'/></div><div style='font-size: 14pt; font-family: Verdana, Arial, Helvetica, sans-serif; margin-left:40px; margin-top:8px;'>" + titulo + "</div><div style='font-size: 11pt; font-family: Verdana, Arial, Helvetica, sans-serif; margin-left:40px; margin-top:8px;'><div>"+ datos[0] +"</div><div>"+ datos[1] +"</div></div><div style='clear:both;'></div>");
									
			});//GEvent.addListener(icono_chincheta, 'click', function() 
	
        	 return icono_chincheta;
	      }//function crear_punto(punto,nombre,continente,pais) 
			
//Llamamos a la funcion que crea la chincheta en las coordenadas definidas anteriormente
         var chincheta = crear_punto (coordenadas,titulo_bocadillo,datos_empresa);
//Añadimos el punto creado al mapa		 
         mapa.addOverlay(chincheta); 
//SE PUEDEN AÑADIR TANTOS PUNTOS COMO SE DESEE		 
		 		 		 
//PARA LA SOMBRA AÑADIR SOMBRA A LA IMAGEN
//       icono_chincheta.shadow = "/images/sombra-bandera2.png";
//       var tamanoSombra = new GSize(22,18);
//       icono_chincheta.shadowSize = tamanoSombra;
//       icono_chincheta.iconAnchor = new GPoint(11, 16);			

      }//if (GBrowserIsCompatible()) 
}//function cargar_googlemaps() 
-->   
</script>
