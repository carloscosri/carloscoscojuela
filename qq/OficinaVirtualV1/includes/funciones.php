<?php
// ESTE FICHERO CONTIENE LAS FUNCIONES QUE SE UTILIZARAN EN LA WEB ´

//Esta funcion redirige a la URL que recibe como parametro
function redirigir($url)
{
	$codigo="<script type='text/javascript'>\n";
	$codigo.=" location.href='$url';\n";
	$codigo.="</script>\n";		
	echo($codigo);
}//function redirigir($url)

//Esta funcion quita una serie de caracteres de la cadena de texto que recibe como parametro
function quitar($mensaje)
{
	$mensaje = str_replace("<","&lt;",$mensaje);
	$mensaje = str_replace(">","&gt;",$mensaje);
	$mensaje = str_replace("\'","&#39;",$mensaje);
	$mensaje = str_replace('\"',"&quot;",$mensaje);
	$mensaje = str_replace("\\\\","&#92;",$mensaje);
	
	return $mensaje;
}

//Esta funcion muestra una ventana modal informativa con el mansaje que recibe como parametro
function MsgBox($frase)
{
	$codigo="<script type='text/javascript'>\n";
	$codigo.=" alert('$frase');\n";
	$codigo.="</script>\n";		
	echo($codigo);
}//function MsgBox($frase)

function nombre_script_en_ejecucion()
{	
	$ruta_origen=explode("/",$_SERVER['SCRIPT_FILENAME']);	
	$num_carpetas_hasta_raiz_web=1;						
	$indice_ruta=sizeof($ruta_origen)-$num_carpetas_hasta_raiz_web;
	$pagina_actual=$ruta_origen[$indice_ruta];
	
	$seccion_actual = explode('_',$pagina_actual);					
	return $seccion_actual[1];
}//function nombre_script_en_ejecucion()

?>