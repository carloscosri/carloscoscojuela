<?php
/*************************************************************TRATAMIENTO DE IMAGENES***************************************************************/
/***************************************************************************************************************************************************/

//Esta funcion genera una miniatura a partir de una imagen grande. Recibe como parametros:
//1 -> Ruta completa donde se encuentra la imagen a escalar
//2 -> Numero de pixeles que puede medir el mayor de los lados del thumb
//3 -> Ruta completa, con el nombre del archivo incluido, donde se almacenara el thumb generado

function crear_miniatura($ruta_imagen,$lado_mayor_img_thum,$ruta_archivo_destino)
{	
//Almacenamos la informacion sobre la imagen, sus dimensiones y extension
//LOs valores que puede tomar la extension son:
//1 -> GIF
//2 -> JPG
//3 -> PNG
//	list($ancho_img_original, $alto_img_original, $extension_img, $color) = getimagesize("$ruta_imagen");

	list($ancho_img_original, $alto_img_original, $extension_img) = getimagesize($ruta_imagen);
								
//Esta variable determinara el tipo que imagen que es segun sus proporciones:
//0-> Horizontal
//1-> Vertical
//2-> Cuadrada
	$forma_imagen=0;
//Esta varioable definira si se escala o no la imagen 0-> NO ESCALAR, 1 -> ESCALAR
	$escalar_imagen=0;
	
//Si el tamaño de la imagen es menor que el establecido para las miniaturas, no se hara el proceso, simplemente se copiará la imagen con su tamaño
//original en la carpeta de thumb de las noticias	

//IMAGEN HORIZONTAL		
	if($ancho_img_original>$alto_img_original)
	{		
		if($ancho_img_original>=$lado_mayor_img_thum)
		{
			$escalar_imagen=1;			
//Ahora calcularemos el alto proporcional, cuando se escale la imagen teniendo en cuenta el numero de pixeles que mide la componente mayor de la imagen
			$proporcion=$lado_mayor_img_thum/$ancho_img_original;
			$lado_menor_img_thum=round($alto_img_original*$proporcion);						
		}//if($ancho_img_original>lado_mayor_img_thum)		
	}//if($ancho_img_original>$alto_img_original)
	else
	{
//IMAGEN VERTICAL			
		if($alto_img_original>$ancho_img_original)
		{
			$forma_imagen=1;
			
			if($alto_img_original>=$lado_mayor_img_thum)
			{
				$escalar_imagen=1;				
//Ahora calcularemos el ancho proporcional, cuando se escale la imagen teniendo en cuenta el numero de pixeles que mide la componente mayor de la imagen				
				$proporcion=$lado_mayor_img_thum/$alto_img_original;
				$lado_menor_img_thum=round($ancho_img_original*$proporcion);			
			}//if($ancho_img_original>lado_mayor_img_thum)
		}//if($alto_img_original>$ancho_img_original)
		
//IMAGEN CUADRADA				
		else
		{
			$forma_imagen=2;		
			
			if($ancho_img_original>=$lado_mayor_img_thum)
			{
				$escalar_imagen=1;
//En este caso al ser los dos lados iguales no es necesario ningun calculo			
				$lado_menor_img_thum=$lado_mayor_img_thum;				
			}//if($ancho_img_original>lado_mayor_img_thum)
		}//else($alto_img_original>$ancho_img_original)
	}//else($ancho_img_original>$alto_img_original)
	
//Si tras las comprobaciones han dado como resultado quye hay que escalar la imagen
	if($escalar_imagen==1)
	{
//De toda la ruta almacenamos por un lado el nombre de la imagen
		$partes_ruta_imagen=explode("/",$ruta_imagen);

/*LALALA 
		switch($_SERVER["HTTP_HOST"])
		{
//Si el servidor es localhost (Ejecucion en servidor local)
			case "localhost":		
				$num_carpetas_hasta_raiz_web=1;		
			break;
//Si el servidor es el de AUDINFOR
			case "www.audinforsystem.es":		
				$num_carpetas_hasta_raiz_web=4;
			break;
	
			default:
				$num_carpetas_hasta_raiz_web=4;
			break;	
		}//switch($_SERVER["HTTP_HOST"])
*/
		
//LALALA!!!! EL NUMERO TENDRA QUE CAMBIAR DEPENDIENDO DE EN QUE SERVIDOR SE ESTE EJECUTANDO LA WEB, CONTAR EL NUMERO DE CARPETAS EN CADA CASO Y HABILITAR EL SCRIPT DE ARRIBA!!!!!!!!!!!!!!!!!!!!	
		$nombre_imagen=$partes_ruta_imagen[3];

/*!!!!!!PARA DEPURACION
		echo("----------------------<br />");
		echo("RUTA IMAGEN CREAR MINIATURA: ".$ruta_imagen."<BR />");
		echo("NOMBRE_IMAGEN: ".$nombre_imagen."<BR />");
		echo("extension_img: ".$extension_img."<br />");							
	
		echo("ancho_img_original: ".$ancho_img_original."<br />");
		echo("alto_img_original : ".$alto_img_original."<br />");
	
		echo("lado_mayor_img_thum: ".$lado_mayor_img_thum."<br />");
		echo("lado_menor_img_thum: ".$lado_menor_img_thum."<br />");				
		echo("Forma imagen : ".$forma_imagen."<br />");
		echo("escalar_imagen : ".$escalar_imagen."<br />");	
		echo("DESTINO DE LA MINIATURA: ".$ruta_archivo_destino."<BR />");
*/

//Dependiendo del tipo de imagen el proceso cambiara, habra que emplear unas funciones ne vez de otras		
		switch($extension_img)
		{
//IMAGEN GIF		
			case 1:		
//Esta variable almacenara una referencia a la imagen original													
				$img = imagecreatefromgif($ruta_imagen);														
			break;
//IMAGEN JPG				
			case 2:
//Esta variable almacenara una referencia a la imagen original													
				$img = imagecreatefromjpeg($ruta_imagen);																  		  
			break;		
//IMAGEN PNG				
			case 3:
//Esta variable almacenara una referencia a la imagen original													
				$img = imagecreatefrompng($ruta_imagen);												
			break;	
		}//switch($extension_img)
		
//EL SIGUIENTE PROCESO ES COMUN, SEA CUAL SEA EL TIPO DE IMAGEN		
//Si la imagen es horizontal o cuadrada, se aplicara el lado mayor al ancho
		if($forma_imagen==0)
		{				
//Esta será la nueva imagen reescalada, en un principio tiene el mismo ancho y alto que la original
	      	$thumb = imagecreatetruecolor($lado_mayor_img_thum,$lado_menor_img_thum);      				
//Ahora reescalamos la imagen
	      	imagecopyresampled ($thumb, $img, 0, 0, 0, 0, $lado_mayor_img_thum, $lado_menor_img_thum,$ancho_img_original, $alto_img_original);
		}//if($forma_imagen==0 or $forma_imagen==2)
				
		else
		{
//En el caso de que la imagen sea vertical se aplicara el lado mayor al alto				
			if($forma_imagen==1)
			{
//Esta será la nueva imagen reescalada, en un principio tiene el mismo ancho y alto que la original
	      		$thumb = imagecreatetruecolor($lado_menor_img_thum,$lado_mayor_img_thum);      				
//Ahora reescalamos la imagen
		      	imagecopyresampled ($thumb, $img, 0, 0, 0, 0, $lado_menor_img_thum, $lado_mayor_img_thum,$ancho_img_original, $alto_img_original);				
			}//if($forma_imagen==1)
				
			else
			{
//Por ultimo si la imagen es cuadrada se aplicara la mayor componente a ambos lados					
				if($forma_imagen==2)
				{
//Esta será la nueva imagen reescalada, en un principio tiene el mismo ancho y alto que la original
		      		$thumb = imagecreatetruecolor($lado_mayor_img_thum,$lado_mayor_img_thum);      				
//Ahora reescalamos la imagen
			      	imagecopyresampled ($thumb, $img, 0, 0, 0, 0, $lado_mayor_img_thum, $lado_mayor_img_thum,$ancho_img_original, $alto_img_original);										
				}//if($forma_imagen==2)
			}//else($forma_imagen==1)
		}//else($forma_imagen==0 or $forma_imagen==2)

				
//Por ultimo guardamos la imagen en el formato adecuado con el nombre y en la ruta que nos interesa
		switch($extension_img)
		{
//IMAGEN GIF				
			case 1:						
	    	  	imagegif($thumb,$ruta_archivo_destino);													
			break;
//IMAGEN JPG			
			case 2:				
	    	  	imagejpeg($thumb,$ruta_archivo_destino);													  		  
			break;		
//IMAGEN ONG				
			case 3:
	    	  	imagepng($thumb,$ruta_archivo_destino);			
			break;	
		}//switch($extension_img)		
						
	}//if($escalar_imagen==1)				
	
//Si las comprobaciones han hecho que la imagen no haya tenido que escalarse, debido a que es mas pequeña que el tamaño establecido para las propias miniaturas. Se copiará la imagen en el directorio de thumbs, sin ningun tipo de modificacion
	else
	{
//SOlo en caso de que el tipo de archivo es uno de los admitidos (JPG, GIF o PNG) se copiara el archivo		
		if($extension_img==1 or $extension_img==2 or $extension_img==3)
		{
			copy($ruta_imagen,$ruta_archivo_destino); 		
		}//if($extension_img==1 or $extension_img==2 or $extension_img==3)
	}//else($escalar_imagen==1)
}//function crear_miniatura($imagen)

/*************************************************************(FIN)TRATAMIENTO DE IMAGENES**********************************************************/
/***************************************************************************************************************************************************/
?>