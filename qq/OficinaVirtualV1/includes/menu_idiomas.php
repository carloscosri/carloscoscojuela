<?php	
//Llamamos a la funcion que nos devuelve el nombre del script en ejecucion, para hacer el enlace a la misma seccion en la que el usuario cambie
//de idioma.
	$pagina_actual=nombre_script_en_ejecucion();
	$pagina_actual = explode('httpdocs', $pagina_actual);
	$pagina_actual = $pagina_actual[1];
//Esta variable almacenara los espacios que se dejan entre una banderas y otra
	$espacio_entre_banderass="";

//Dependiendo del idioma activo se remarcara una banderas u otra	y se quitará el enlace del idioma activo
	if($_SESSION["idioma"]=="es")
	{
//La ruta de la imagen de la cabecera cambiara si se accede desde la oficina virtual
		if($_SESSION['oficina_virtual']!=1){
?>
			<img src="img/banderas/sp.png" alt="<?=$idioma_activo?>" title="<?=$idioma_activo?>" class="idioma_marcado" />
        
<?php
		}//if($_SESSION['oficina_virtual']!=1)
		else
		{	
?>
			<img src="../img/banderas/sp.png" alt="<?=$idioma_activo?>" title="<?=$idioma_activo?>" class="idioma_marcado" />
<?php	
		}//else($_SESSION['oficina_virtual']!=1)
	}//if($_SESSION["idioma"]=="es")
	else
	{
//La ruta de la imagen de la cabecera cambiara si se accede desde la oficina virtual
		if($_SESSION['oficina_virtual']!=1)
		{
?>
			<a href="<?=$pagina_actual?>?id=es"><img src="img/banderas/sp.png" alt="<?=$menu_idiomas1?>" title="<?=$menu_idiomas1?>" border="0" class="idioma_marcado"  /></a>
        
<?php
		}//if($_SESSION['oficina_virtual']!=1)
		else
		{	
?>
			<a href="<?=$pagina_actual?>?id=es"><img src="../img/banderas/sp.png" alt="<?=$menu_idiomas1?>" title="<?=$menu_idiomas1?>" border="0" class="idioma_marcado"  /></a>
<?php	
		}//else($_SESSION['oficina_virtual']!=1)	
	}//else($_SESSION["idioma"]=="es")	
	
	echo($espacio_entre_banderass);
	
	
	if($_SESSION["idioma"]=="ca")
	{   
//La ruta de la imagen de la cabecera cambiara si se accede desde la oficina virtual
		if($_SESSION['oficina_virtual']!=1)
		{
?>
			<img src="img/banderas/cat.png" alt="<?=$idioma_activo?>" title="<?=$idioma_activo?>" class="idioma_marcado" />
        
<?php
		}//if($_SESSION['oficina_virtual']!=1)
		else
		{	
?>
			<img src="../img/banderas/cat.png" alt="<?=$idioma_activo?>" title="<?=$idioma_activo?>" class="idioma_marcado" />
<?php	
		}//else($_SESSION['oficina_virtual']!=1)	
		
	}//if($_SESSION["idioma"]=="ca")
	else
	{
//La ruta de la imagen de la cabecera cambiara si se accede desde la oficina virtual
		if($_SESSION['oficina_virtual']!=1)
		{
?>
			<a href="<?=$pagina_actual?>?id=ca"><img src="img/banderas/cat.png" alt="<?=$menu_idiomas2?>" title="<?=$menu_idiomas2?>" border="0" class="idioma_marcado"  /></a>
        
<?php
		}//if($_SESSION['oficina_virtual']!=1)
		else
		{	
?>
			<a href="<?=$pagina_actual?>?id=ca"><img src="../img/banderas/cat.png" alt="<?=$menu_idiomas2?>" title="<?=$menu_idiomas2?>" border="0" class="idioma_marcado"  /></a>
<?php	
		}//else($_SESSION['oficina_virtual']!=1)		
	
	}//else($_SESSION["idioma"]=="ca")	
	
	echo($espacio_entre_banderass);
		
	
	if($_SESSION["idioma"]=="eu")
	{
//La ruta de la imagen de la cabecera cambiara si se accede desde la oficina virtual
		if($_SESSION['oficina_virtual']!=1)
		{
?>
			<img src="img/banderas/eu.png" alt="<?=$idioma_activo?>" title="<?=$idioma_activo?>" class="idioma_marcado" />
        
<?php
		}//if($_SESSION['oficina_virtual']!=1)
		else
		{	
?>
			<img src="../img/banderas/eu.png" alt="<?=$idioma_activo?>" title="<?=$idioma_activo?>" class="idioma_marcado" />
<?php	
		}//else($_SESSION['oficina_virtual']!=1)			
	
	}//if($_SESSION["idioma"]=="eu")
	else
	{
//La ruta de la imagen de la cabecera cambiara si se accede desde la oficina virtual
		if($_SESSION['oficina_virtual']!=1)
		{
?>
			<a href="<?=$pagina_actual?>?id=eu"><img src="img/banderas/eu.png" alt="<?=$menu_idiomas3?>" title="<?=$menu_idiomas3?>" border="0" class="idioma_marcado"  /></a>
        
<?php
		}//if($_SESSION['oficina_virtual']!=1)
		else
		{	
?>
			<a href="<?=$pagina_actual?>?id=eu"><img src="../img/banderas/eu.png" alt="<?=$menu_idiomas3?>" title="<?=$menu_idiomas3?>" border="0" class="idioma_marcado"  /></a>
<?php	
		}//else($_SESSION['oficina_virtual']!=1)		
	}//else($_SESSION["idioma"]=="eu")	
	
	echo($espacio_entre_banderass);	

	if($_SESSION["idioma"]=="ga")
	{
//La ruta de la imagen de la cabecera cambiara si se accede desde la oficina virtual
		if($_SESSION['oficina_virtual']!=1)
		{
?>
			<img src="img/banderas/ga.png" alt="<?=$idioma_activo?>" title="<?=$idioma_activo?>" class="idioma_marcado" />
        
<?php
		}//if($_SESSION['oficina_virtual']!=1)
		else
		{	
?>
			<img src="../img/banderas/ga.png" alt="<?=$idioma_activo?>" title="<?=$idioma_activo?>" class="idioma_marcado" />
<?php	
		}//else($_SESSION['oficina_virtual']!=1)		
	
	}//if($_SESSION["idioma"]=="ga")
	else
	{
//La ruta de la imagen de la cabecera cambiara si se accede desde la oficina virtual
		if($_SESSION['oficina_virtual']!=1)
		{
?>
			<a href="<?=$pagina_actual?>?id=ga"><img src="img/banderas/ga.png" alt="<?=$menu_idiomas4?>" title="<?=$menu_idiomas4?>" border="0" class="idioma_marcado"  /></a>
        
<?php
		}//if($_SESSION['oficina_virtual']!=1)
		else
		{	
?>
			<a href="<?=$pagina_actual?>?id=ga"><img src="../img/banderas/ga.png" alt="<?=$menu_idiomas4?>" title="<?=$menu_idiomas4?>" border="0" class="idioma_marcado"  /></a>
<?php	
		}//else($_SESSION['oficina_virtual']!=1)		
	
	}//else($_SESSION["idioma"]=="ga")	
	
	echo($espacio_entre_banderass);
	
	if($_SESSION["idioma"]=="en")
	{
//La ruta de la imagen de la cabecera cambiara si se accede desde la oficina virtual
		if($_SESSION['oficina_virtual']!=1)
		{
?>
			<img src="img/banderas/uk.png" alt="<?=$idioma_activo?>" title="<?=$idioma_activo?>" class="idioma_marcado" />
        
<?php
		}//if($_SESSION['oficina_virtual']!=1)
		else
		{	
?>
			<img src="../img/banderas/uk.png" alt="<?=$idioma_activo?>" title="<?=$idioma_activo?>" class="idioma_marcado" />
<?php	
		}//else($_SESSION['oficina_virtual']!=1)		

	}//if($_SESSION["idioma"]=="en")
	else
	{
//La ruta de la imagen de la cabecera cambiara si se accede desde la oficina virtual
		if($_SESSION['oficina_virtual']!=1)
		{
?>
			<a href="<?=$pagina_actual?>?id=en"><img src="img/banderas/uk.png" alt="<?=$menu_idiomas5?>" title="<?=$menu_idiomas5?>" border="0" class="idioma_marcado"  /></a>
        
<?php
		}//if($_SESSION['oficina_virtual']!=1)
		else
		{	
?>
			<a href="<?=$pagina_actual?>?id=en"><img src="../img/banderas/uk.png" alt="<?=$menu_idiomas5?>" title="<?=$menu_idiomas5?>" border="0" class="idioma_marcado"  /></a>
<?php	
		}//else($_SESSION['oficina_virtual']!=1)		
	}//else($_SESSION["idioma"]=="en")		
?>