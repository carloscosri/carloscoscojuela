<?php
/*
NOTAS:

1- ESTA SECCION TOMA LOS DATOS DE LA TABLA ContratosPDF

*/

/* NOTA:

 COMO NO SE CIERRA LA CONEXION TRAS CADA CONEXION CON LA BASE DE DATOS, LAS CONSULTAS DEJAN DE FUNCIONAR SI SE ESTABLECE COMO UNICA CONEXION
 LA QUE UTILIZA EL RESTO DE LA WEB, POR LO TANTO HASTA QUE SE HAGA LA MIGRACION COMPLETA DE LOS TEXTOS, COEXISTIRAN LAS DOS CONEXIONES, YA QUE
 SE HA AÑADIDO UNA LLAMADA A LA CONEXION BUENA EN ESTE ARCHIVO.
*/

include("_conexion.php");

//Se incluye el archivo con los textos especifico de esta seccion en el idioma oportuno
//include($_SESSION["directorio_raiz"]."idiomas/".$_SESSION["idioma"]."/oficina_datos_titular.php");

//include($_SESSION["directorio_raiz"]."idiomas/".$_SESSION["idioma"]."/oficina_informacion_contratos.php");

include("../idiomas/".$_SESSION["idioma"]."/oficina_datos_titular.php");

include("../idiomas/".$_SESSION["idioma"]."/oficina_informacion_contratos.php");

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<title><?=$titulo_web;?></title>

<!--Se añaden las hojas de estilos genericas que utiliza esta seccion-->
<link href="../css/estilos.css" rel="stylesheet" type="text/css" />
<link href="../css/secciones.css" rel="stylesheet" type="text/css" />
<link href="css/oficina.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_vieja.css" rel="stylesheet" type="text/css" />

<link href="css/oficina_informacion_contratos_y_facturas.css" rel="stylesheet" type="text/css" />

<script language=javascript type=text/javascript>
function stopRKey(evt) {
var evt = (evt) ? evt : ((event) ? event : null);
var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
if ((evt.keyCode == 13) && (node.type=="text")) {return false;}
}
document.onkeypress = stopRKey; 
</script>

<!--Ahora se añaden las reglas para Internet Explorer. Asi corregiremos las diferencias de visualizacion entre navegadores
Es importante añadirlo despues de las reglas principales, para que se sobreescriban las correcciones pertinentes. En este caso las diferencias
solo son para internet explorer 7-->

<!--[if IE 7]>
	<link href="../css/estilos_ie7.css" rel="stylesheet" type="text/css" />
	<link href="../css/secciones_ie7.css" rel="stylesheet" type="text/css" />    
<![endif]-->

<!--[if IE]>
	<link href="css/tablas_informacionIE.css" rel="stylesheet" type="text/css" />
<![endif]-->

</head>
	
<body>
<!--*************************************************Web realizada por ****************************************-->
	<div id="central_oficina">
    
<?php
//Se comprueba que el usuario se haya identificado, si no es asi se da un aviso y redirigimos a la seccion de identificacion
if ($_SESSION['usuario']=="")
{	
	MsgBox($error_identificacion);
	redirigir("oficina_index.php?id=".$_SESSION["idioma"]);
	
}//if ($_SESSION['usuario'])

//Si el usuario se ha identificado la seccion se ejecutara con normalidad
else
{
//Añadimos los fragmentos comunes, la cabecera, el menu, y el submenu
	include("oficina_fragmentos_comunes.php");
?>                    
		<div class="contenido_seccion_oficina_informacion_contratos_y_facturas">                                         
			<div class="tipotablacentrado arialblack18Titulo p4 aligncenter">
               <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 264');
			   mysql_query("SET NAMES 'utf8'");
			   $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>                  
            </div>
					  
			<fieldset style="border-radius:10px; margin-bottom:10px;">
				<table border="0">
				<tr>
				   <td width="100"><img src="../img/oficina/info.png" width="60"/></td>
				   <td  style="font-size:12px; text-align: justify;">
				<?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 265'); 
			 $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
					</td>
				</tr>
				</table>
			</fieldset>					

			<p class="p3 top-3 clr-4 aligncenter"> </p>
            
            <div id="resumenContratos" style="margin-left: -22px;"> 
            <br>
            
<?php 
		   
			$consulta_contratos="SELECT * FROM ContratosPDF WHERE NumeroCliente = '".$_SESSION['numeroCliente']."';";
			$resultados_contratos = mysql_query($consulta_contratos);			
			$num_contratos=mysql_num_rows($resultados_contratos);
									
//Si hay contrayps se mostrara un resumen de los datos de cada uno de ellos			
			if($num_contratos != 0)
			{
				while($registro_contrato = mysql_fetch_array($resultados_contratos))
				{
?>
                 <table class="tablaContratos" style="font-size: 10px; margin: 10px auto auto; text-align: center; border: 1px solid rgb(153, 153, 153); border-radius: 5px; padding-left: 10px; padding-right: 5px; min-width: 620px; background-color: rgb(242, 242, 242); box-shadow: 0px 3px 2px rgb(153, 153, 153);">
                      <tr class="clr-4 aligncenter" style="text-decoration:underline">
                        <td>
						   <strong><?php 
						   // Num Poliza
						   $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 267'); 
						   $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?></strong>
                         </td>
                         <td>
                        	<strong><?php 
							//Fecha Alta 
							$resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 268'); 
							$rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?></strong>
                        </td>
                        <td>
                        	<strong><?php 
							//Fecha Baja
							$resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 269'); 
							$rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?></strong>
                         </td>
                         <td>
                         	<!-- TITULAR -->
                         	<strong><?=$oficina_informacion_contratos1?></strong>
                         </td>
                         <td>
                         	<!-- TITULAR DNI -->
                            <strong><?=$oficina_datos_titular41?></strong>
                         </td>
                         <td>
                         	<!-- TARIFA -->
                         	<strong><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 270'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?></strong>
                         </td>
                         <td>
                         	<!--POTENCIA-->
                            <strong><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 271'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?></strong>
                         </td>
                         <td rowspan="2" class="aligncenter" style="padding-left:10px; padding-right:10px;">
                         	<a href="imprimir_contrato.php?id=<?=$_SESSION["idioma"].$ampersand?>fe=<?=$registro_contrato["FechaAlta"]?>" title="Descargar" target="_blank"><img src="../img/oficina/descargaFactura.png"></a>
                         </td>
                      </tr>   
                           
            		<tr class="aligncenter">
                    	<td>
                       	 	<?=$registro_contrato["NumeroPoliza"]?>
                        </td>
                        <td>
                        	<!-- <?=fecha_normal($registro_contrato["FechaAlta"])?> --> --
                     	</td>
                        <td>
                        	<!-- <?=fecha_normal($registro_contrato["FechaBaja"])?> --> --
                        </td>
                        <td>
                        	<?=$registro_contrato["TitularNombre"]?>
                        </td>
                        <td>
                        	<?=$registro_contrato["TitularDNI"]?>
                        </td>
                        <td>
                        	<?=$registro_contrato["Tarifa"]?>
                        </td>
                        <td>
                        	<?=$registro_contrato["Potencia"]?>
                        </td>
                    </tr>
                    </table>
                
<?php 
				}//while($row = mysql_fetch_array($result))				  
		   }//if($num_contratos != 0)
		   else
		   {
?>

<span class="error_no_registros"><?=$error_no_contratos?></span>

<?php		   
		   }//if($num_contratos != 0)
?>
 
  	</div><!--<div class="contenido_seccion_oficina">-->  
        	</div><!--<div class="contenido_seccion_oficina">-->                        
		
        
        <div class="limpiar"></div>
                        	
<!--Por ultimo aparecera el pie de la web-->
<?php 
	include("../includes/pie.php");
        
}//else ($_SESSION['usuario'])
?>        
    </div><!--<div id="central_oficina"-->
</body>
</html>
