<?php
//Se incluye el script que contiene el inicio de la sesion.Tiene que se antes de ningun código, ya que contiene las funciones que inicializan la sesion de usuario. Si se pone antes de un codigo que genere una salida por pantalla o delimitadores HTML da error.
//Aunque solo sea para destrirla se requiere la inicializacion de la sesion
	include("../includes/sesion.php");
	
	session_unset();
//Destrimos la sesion actual	
	session_destroy();
//Redirigimos a la pagina principal	
	redirigir("oficina_index.php");
	
//Esta funcion redirige a la URL que recibe como parametro
	function redirigir($url)
	{
		$codigo="<script type='text/javascript'>\n";
		$codigo.=" location.href='$url';\n";
		$codigo.="</script>\n";		
		echo($codigo);
	}//function redirigir($url)
?>