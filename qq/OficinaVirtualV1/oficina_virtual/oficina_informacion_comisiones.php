<?php
include("_conexion.php");

include("../includes/verificaciones.php");

include("../idiomas/".$_SESSION["idioma"]."/oficina_datos_titular.php");
include("../idiomas/".$_SESSION["idioma"]."/oficina_datos_instalacion.php");
include("../idiomas/".$_SESSION["idioma"]."/oficina_contratacion_alta.php");
include("../idiomas/".$_SESSION["idioma"]."/oficina_modificacion_datostitular.php");
include("../idiomas/".$_SESSION["idioma"]."/oficina_modificacion_datossuministro.php");
include("../idiomas/".$_SESSION["idioma"]."/oficina_datos.php");
include("../idiomas/".$_SESSION["idioma"]."/oficina_presentacion.php");
include("../idiomas/".$_SESSION["idioma"]."/oficina_presentacion_mp.php");
include ('../../idiomas/'.$_SESSION['idioma'].'/comun.php');
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <meta name="viewport" content="width=device-width; initial-scale=1.0">

<title><?=$titulo_web;?></title>

<!--Se añaden las hojas de estilos genericas que utiliza esta seccion-->
<link href="../css/estilos.css" rel="stylesheet" type="text/css" />
<link href="../css/secciones.css" rel="stylesheet" type="text/css" />
<link href="css/oficina.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_vieja.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_modificacion_titular.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_modificacion_suministro.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_datos.css" rel="stylesheet" type="text/css" />

<!-- Insertamos los estilos de la web -->
<link rel="icon" href="../../images/favicon.ico" type="image/x-icon">
<link href="css/colores.css" rel="stylesheet" type="text/css" />

<!--[if IE 7]>
	<link href="../css/estilos_ie7.css" rel="stylesheet" type="text/css" />
	<link href="../css/secciones_ie7.css" rel="stylesheet" type="text/css" />    
<![endif]-->

<!--[if lt IE 8]>
   <div style=' clear: both; text-align:center; position: relative;'>
     <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
       <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
    </a>
  </div>
<![endif]-->
    <!--[if lt IE 9]>
   	<script type="text/javascript" src="js/html5.js"></script>
    	<link rel="stylesheet" type="text/css" media="screen" href="../../css/ie.css">
    <![endif]-->

<!-- Javascripts -->
	 <script type="text/javascript" src="../../js/jquery-1.7.min.js"></script>
     <script type="text/javascript" src="../../js/superfish.js"></script>
	 <script type="text/javascript" src="../../js/jquery.responsivemenu.js"></script>
	 <script type="text/javascript" src="../../js/jquery.flexslider.js"></script>
	 <script type="text/javascript" src="../../js/jquery.easing.1.3.js"></script>
	 <script type="text/javascript" src="../../js/jquery.tweet.js"></script>
	 <script type="text/javascript" src="../../js/forms.js"></script>
	 <script type="text/javascript">
		  $(function() {
				$('.flexslider').flexslider({
				  animation: "slide"
				});
				$(".flex-control-nav li a").text("")
				$('.main-menu').prepend('<div id="menu-icon"><span>Menu</span></div>');
	
				/* toggle nav */
				$("#menu-icon").on("click", function(){
					$(".sf-menu").slideToggle();
					$(this).toggleClass("active");
				});
			 });
    </script>
    
   <style type="text/css">
   	.nover {display:none;}
	.ver {display:block;}
	table.resultados{width:950px;border-collapse:collapse; text-align:left}
	tr.cabecera{background: url("../img/comun/gradientenaranja.jpg") repeat-x scroll 0 center #254B70; font-weight:bold;}
	tr.cabecera td{padding:7px; color:#FFF}
	tr.linea td{padding:3px; border-bottom:1px #666 solid;}
	.calendar{z-index:10000;margin-top:500px;}
   </style>
   
  <script type="text/javascript">
		function mostrar(capa,invoca, oculta){
			var obj = document.getElementById(capa);
			var enl = document.getElementById(invoca);
			var enl2 = document.getElementById(oculta);
			obj.style.display = 'block';
			
			enl.style.display = 'none';
			enl2.style.display = 'inline';
		}
		
		function ocultar(capa,invoca, oculta){
			var obj = document.getElementById(capa);
			var enl = document.getElementById(invoca);
			var enl2 = document.getElementById(oculta);
			obj.style.display = 'none';	
			
			enl.style.display = 'none';
			enl2.style.display = 'inline';
		}
		
		function oculta(boton){
			var enl2 = document.getElementById(boton);
			enl2.style.display = 'none';
		}
   </script>

    <!-- CALENDARIO -->   
    <link rel="stylesheet" type="text/css" media="all" href="scripts/calendar/skins/aqua/theme.css" title="Aqua" />
    <script type="text/javascript" src="scripts/calendar/calendar.js"></script>
    <script type="text/javascript" src="scripts/calendar/lang/calendar-es.js"></script>
    <script type="text/javascript" src="scripts/calendar/calendar-setup.js"></script>
    
</head>
	
<body>
	<div id="central_oficina">
    <?php
		if ($_SESSION['usuario_com'] != 1){	
			MsgBox($error_identificacion);
			redirigir("oficina_index.php?id=".$_SESSION["idioma"]);
		} else {
			include("oficina_fragmentos_comunes_comerciales.php");
	?>   
	<script type="text/javascript">
		function mostrar_extra(cliente){
			var capa = 'extra'+ cliente;
			var obj = document.getElementById(capa);
			if (obj.style.display == "block"){
				obj.style.display = "none";
			} else {
				obj.style.display = "block";
			}
		}
	</script>
                  
    <div class="contenido_seccion_oficina_datos" style="margin-left:0px;">
        <div class="contenido_ajustado" style="width:950px;">
            <div class="tipotablacentrado arialblack18Titulo p4"> Comisiones Detalladas </div>
            <!--
            	<fieldset style="padding-left:0px; padding-top:10px; width:550px; margin:auto">
                    <div align="justify" class="arial12Importanteb padtot" style="font-size:10px;">
                      <img src="../img/iconos/info.png" align="left" width="50" style="margin-right:15px; padding-top:0px;" />
                      Esta seccion muestra el resumen de todas sus comisiones agrupadas por Clientes.
                      <br><br>
                      Haga click en el icono '+' para desplegar la informacion
                    </div>
                 </fieldset> 
            --> 
            <br><br>
            <?php
				function cambiarfecha_mysql($fecha){
					list($ano,$mes,$dia) = explode("-",$fecha);
					$dia = explode(' ',$dia);
					$fecha="$dia[0]-$mes-$ano";
					return $fecha;
				}
			?>
            <div style="text-align:center">
            <span style="font-weight:bold">Seleccione el intervalo de fechas que desee cosultar:<br><br></span>
				<form action="#" method="post" style="margin:auto">
                
                <strong>Fecha de Inicio</strong>
                 <input name="fechainicio"  id="sel1" value=""  type="text" class="textfield" style="width:120px;">                   
                    <input type="button" id="lanzador" value="..." class="textfield" />
                        <!-- script que define y configura el calendario-->
                     <script type="text/javascript">
                        Calendar.setup({
                        inputField : "sel1", // id del campo de texto
                        ifFormat : "%Y-%m-%d", // formato de la fecha que se escriba en el campo de texto
                        button : "lanzador" // el id del botón que lanzará el calendario
                        });
                     </script>
                   &nbsp;&nbsp;&nbsp;&nbsp;  
                 <strong>Fecha Final</strong>    
                     
                <input name="fechafin" id="sel2" value="" type="text" class="textfield" style="width:120px;" >
                <input type="button" id="lanzador2" value="..." class="textfield" />
                <!-- script que define y configura el calendario-->
                <script type="text/javascript">
					Calendar.setup({
					inputField : "sel2", // id del campo de texto
					ifFormat : "%Y-%m-%d", // formato de la fecha que se escriba en el campo de texto
					button : "lanzador2" // el id del botón que lanzará el calendario
					});
                </script>
                 &nbsp;&nbsp;&nbsp;&nbsp;
                <input type="submit" name="submit" value="aceptar">
                </form>
                </div>
                <br><br>
                <p><br><br></p>
                
                <table class="resultados">
                   <tr class="cabecera">
                       <td style="width:50px">Cliente</td>
                       <td>DNI</td>
                       <td>Titular</td>
                       <td>Direccion</td>
                       <td>Numero</td>
                       <td>Provincia</td>
                       <td>CUPS</td>
                       <td>Comision</td>
                   </tr>
                
                <?php
					$i=0;
					$mes = Date('m');
					$anio = Date('Y');
					$mes = $mes -1;
				
				/*if (!isset($_POST['submit'])){
					$fechaInicio = $anio.'-'.$mes.'-01';
					$fechaFin = $anio.'-'.$mes.'-31';
				} else {*/
					$fechaInicio = $_POST['fechainicio'];
					$fechaFin = $_POST['fechafin'];
					//echo $fechaInicio;
					//echo $fechaFin;
				/*}*/
					
				$sqlClientesComisionista = "select distinct Cliente from Comisiones where Agente IN (select Numero from Comisionistas where Nombre like '".$_SESSION['usuario_mp']."%') and DATE(FechaRegistro) 
				between '".$fechaInicio."' AND  '".$fechaFin."'";
				$queryClientesComisionista = mysql_query($sqlClientesComisionista);
				//echo $sqlClientesComisionista;
				$total=0;
				
				while($cliente = mysql_fetch_array($queryClientesComisionista)){
					
				$sqlCliente = 'select * from DatosRegistrados where Usuario = '.$cliente['Cliente'].'';
				$sqlQueryCliente=mysql_query($sqlCliente);
				$RegistroCliente = mysql_fetch_array($sqlQueryCliente);	
				if (!$RegistroCliente){   } else {
					?>
           
					<tr class="linea" style="background-color:<?php if($i%2==0){echo'#F2F2F2';} else { echo'#DDD';}?>;">
                    	<td>
                        	<?php
								
								echo '<a name="'.$cliente['Cliente'].'"> </a>';
								echo '
								<span style="font-size:14px; font-weight:bold;">
								<a style="border:none" href="#'.$clienteAnterior.'" onClick="mostrar(\''.$cliente['Cliente'].'\',\'mostrar'.$cliente['Cliente'].'\',\'ocultar'.$cliente['Cliente'].'\')" id="mostrar'.$cliente['Cliente'].'">
								<img src="../img/iconos/mas.gif" width="15"></a>
								
								<a style="border:none" href="#'.$clienteAnterior.'" onClick="ocultar(\''.$cliente['Cliente'].'\',\'ocultar'.$cliente['Cliente'].'\',\'mostrar'.$cliente['Cliente'].'\')" id="ocultar'.$cliente['Cliente'].'">
								<img src="../img/iconos/menos.gif" width="15"></a>
								<script>oculta(\'ocultar'.$cliente['Cliente'].'\');</script>
								'.$cliente['Cliente'].'</span>
							';
							?>
                        </td>
                        
                        <?php
						$sqlComisiones="select * from Comisiones where Agente IN (select Numero from Comisionistas where Nombre like '".$_SESSION['usuario_mp']."%') and Cliente = ".$cliente['Cliente']." and DATE(FechaRegistro) 
						between '".$fechaInicio."' AND  '".$fechaFin."'";
						$queryComisiones=mysql_query($sqlComisiones);
						$totalCliente = 0;
                        
						while($comision = mysql_fetch_array($queryComisiones)){
                        $totalCliente = $totalCliente + $comision['ComisionEnergia'] + $comision['ComisionVarios'] + $comision['ComisionkWh'];
						}
                        ?>
                        <td><?=$RegistroCliente['DNI'];?></td>
                        <td><?=utf8_decode($RegistroCliente['TitularNombre']);?></td>
                        <td><?=$RegistroCliente['TitularCalle'];?></td>
                        <td><?=utf8_decode($RegistroCliente['TitularNumero']);?></td>
                        <td><?=utf8_decode($RegistroCliente['TitularProvincia']);?></td>
                        <td><?=$RegistroCliente['CUPS'];?></td>
                        <td><span style="font-size:11px; font-weight:bold; color:#F00"><?=number_format($totalCliente,2,',','.');?> </span></td>
                        </tr>
                        
                        <tr style="background-color:<?php if($i%2==0){echo'#F2F2F2';} else { echo'#DDD';}?>;">
                        	<td colspan="8">
					<?				
						$queryComisiones=mysql_query($sqlComisiones);
						echo '<div class="nover" id="'.$cliente['Cliente'].'">';
					?>
                    
                    	<table class="mostrarDatos" style="width:900px; margin-left:20px;">
                        	<tr style="font-size:10px; font-weight:bold;">
                            	<td>Fecha</td>
                                <td>Importe Energia</td>
                                <td>%</td>
                                <td>Comision Energia</td>
                                <td>Importe Varios</td>
                                <td>%</td>
                                <td>Comision Varios</td>
                                <td>Importe kWh</td>
                                <td>%</td>
                                <td>Comision kWh</td>
                            </tr>
                        <?php
					while($comision = mysql_fetch_array($queryComisiones)){
						?>
                            <tr style="font-size:10px;">
                            	<td><?=cambiarfecha_mysql($comision['FechaRegistro']);?></td>
                            	<td><?php if ($comision['ImporteEnergia'] == 0){echo '-';} else { echo $comision['ImporteEnergia'].'';} ?></td>
                                <td><?php if ($comision['PorcentajeEnergia'] == 0){echo '-';} else { echo $comision['PorcentajeEnergia'].'%';} ;?></td>
                                <td><?php if ($comision['ComisionEnergia'] == 0){echo '-';} else { echo $comision['ComisionEnergia'].'';} ?></td>
                            	<td><?php if ($comision['ImporteVarios'] == 0){echo '-';} else { echo $comision['ImporteVarios'].'';} ?></td>
                                <td><?php if ($comision['PorcentajeVarios'] == 0){echo '-';} else { echo $comision['PorcentajeVarios'].'%';};?></td>
                                <td><?php if ($comision['ComisionVarios'] == 0){echo '-';} else { echo $comision['ComisionVarios'].'';}?></td>
                                <td><?php if ($comision['ImportekWh'] == 0){echo '-';} else { echo $comision['ImportekWh'].'';}?></td>
                                <td><?php if ($comision['PorcentajekWh'] == 0){echo '-';} else { echo $comision['PorcentajekWh'].'%';}?></td>
                                <td><?php if ($comision['ComisionkWh'] == 0){echo '-';} else { echo $comision['ComisionkWh'].'';}?></td>
                            </tr>
					<?php
					}
					$i++;
					$total = $total + $totalCliente;
					?>
                    </table>
                    </td></tr>
                    <?php
						echo '</tr>&nbsp;&nbsp;&nbsp;';
						echo '</div>';
						
						$clienteAnterior=$cliente['Cliente'];
					}	
				}
				
				echo '</table>
				<div style="background: none repeat scroll 0 0 #FFFFFF;
					border: 1px solid #DDDDDD;
					border-radius: 6px 6px 6px 6px;
					padding: 10px;
					text-align: center;
					z-index: 15; position:absolute; top:350px; width:820px; margin-left:65px;"> 
					  <span style="font-size:13px; font-weight:bold">
					  Resumen de las Comisiones entre '.cambiarfecha_mysql($fechaInicio).' y '.cambiarfecha_mysql($fechaFin).'</span>:
					  <span style="font-size:13px; font-weight:bold; color:#F00"> '.number_format($total,2,',','.').'  </span>
				</div>';
			 ?>
       		<br><br>
        </div>      	
    </div> <!--<div class="contenido_seccion_oficina">-->                        
    
    <div class="limpiar"></div>

<!-- Por ultimo aparecera el pie de la web -->
<?php 
}	

include("../../includes/pie.php");
?>
      </div><!--<div id="central_oficina"-->
   </body>
</html>