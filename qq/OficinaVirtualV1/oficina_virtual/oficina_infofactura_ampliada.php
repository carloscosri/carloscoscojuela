<?php
/* NOTA:

 COMO NO SE CIERRA LA CONEXION TRAS CADA CONEXION CON LA BASE DE DATOS, LAS CONSULTAS DEJAN DE FUNCIONAR SI SE ESTABLECE COMO UNICA CONEXION
 LA QUE UTILIZA EL RESTO DE LA WEB, POR LO TANTO HASTA QUE SE HAGA LA MIGRACION COMPLETA DE LOS TEXTOS, COEXISTIRAN LAS DOS CONEXIONES, YA QUE
 SE HA AÑADIDO UNA LLAMADA A LA CONEXION BUENA EN ESTE ARCHIVO.
*/

include("_conexion.php");

//Se incluye el archivo con los textos especifico de esta seccion en el idioma oportuno
//include($_SESSION["directorio_raiz"]."idiomas/".$_SESSION["idioma"]."/oficina_infofactura_ampliada.php");
include("../idiomas/".$_SESSION["idioma"]."/oficina_infofactura_ampliada.php");

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<title><?=$titulo_web;?></title>

<!--Se añaden las hojas de estilos genericas que utiliza esta seccion-->
<link href="../css/estilos.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_infofactura_ampliada.css" rel="stylesheet" type="text/css" />

</head>
	
<body>
<!--*************************************************Web realizada por ****************************************-->
<?php
//Se comprueba que el usuario se haya identificado, si no es asi se da un aviso y redirigimos a la seccion de identificacion
if ($_SESSION['usuario']=="")
{	
	MsgBox($error_identificacion);
	redirigir("oficina_index.php?id=".$_SESSION["idioma"]);
	
}//if ($_SESSION['usuario'])

//Si el usuario se ha identificado la seccion se ejecutara con normalidad
else
{
//Captutamos el idioma activo y la parte de la factura a mostrar
$idioma=$_GET["id"];
$parte_factura=$_GET["p"];

/*
echo("idioma: ".$idioma."<br />");
echo("parte_factura: ".$parte_factura."<br />");
*/

//Esta variable almacenara la ruta donde se encuentran las ampliaciones de las partes de la factura
$ruta_imagenes="flash/info_factura/ampliaciones/";

//La factura solo aparece en 2 idiomas, por lo tanto para los demas idiomas se asignara el español como idioma por defecto
if($_SESSION["idioma"]!="es" and $_SESSION["idioma"]!="ca")
{
	$idioma_factura="es";
}//if($_SESSION["idioma"]!="es" and $_SESSION["idioma"]!="ca")
else
{
	$idioma_factura=$_SESSION["idioma"];
}//else($_SESSION["idioma"]!="es" and $_SESSION["idioma"]!="ca")

switch($parte_factura)
{
	case 1:
?>
		<div align="center"><img src="<?=$ruta_imagenes.$idioma_factura?>/parte1.gif" alt=""/></div>
        <div class="texto_imagen_ampliada"><br /><br /><?=$oficina_infofactura_ampliada1?></div>
<?php	
	break;
	
	case 2:
?>	
		<div align="center"><img src="<?=$ruta_imagenes.$idioma_factura?>/parte2.gif" alt=""/></div>
        <div class="texto_imagen_ampliada"><br /><br /><?=$oficina_infofactura_ampliada2?></div>
<?php        
	break;	
	
	case 3:
?>	
		<div align="center"><img src="<?=$ruta_imagenes.$idioma_factura?>/parte3.gif" alt=""/></div>
        <div class="texto_imagen_ampliada"><br /><br /><?=$oficina_infofactura_ampliada3?></div>
<?php        
	break;		

	case 4:
?>	
		<div align="center"><img src="<?=$ruta_imagenes.$idioma_factura?>/parte4.gif" alt=""/></div>
        <div class="texto_imagen_ampliada"><br /><br /><?=$oficina_infofactura_ampliada4?></div>
<?php        
	break;	
		
	case 5:
?>	
		<div align="center"><img src="<?=$ruta_imagenes.$idioma_factura?>/parte5.gif" alt=""/></div>
        <div class="texto_imagen_ampliada"><br /><br /><?=$oficina_infofactura_ampliada5?></div>
<?php        
	break;		

	case 6:
?>	
		<div align="center"><img src="<?=$ruta_imagenes.$idioma_factura?>/parte6.gif" alt=""/></div>
        <div class="texto_imagen_ampliada"><br /><br /><?=$oficina_infofactura_ampliada6?></div>
<?php        
	break;		
}//switch($parte_factura)

?>    
                
<?php
}//else ($_SESSION['usuario'])
?>        
</body>
</html>
