<?php
/*
NOTAS:

1- ESTA SECCION TOMA LOS DATOS DE LA TABLA FacturasPDF

- ESCRIBE EN LA TABLA SolicitudFacturas

*/

/* NOTA:

 COMO NO SE CIERRA LA CONEXION TRAS CADA CONEXION CON LA BASE DE DATOS, LAS CONSULTAS DEJAN DE FUNCIONAR SI SE ESTABLECE COMO UNICA CONEXION
 LA QUE UTILIZA EL RESTO DE LA WEB, POR LO TANTO HASTA QUE SE HAGA LA MIGRACION COMPLETA DE LOS TEXTOS, COEXISTIRAN LAS DOS CONEXIONES, YA QUE
 SE HA AÑADIDO UNA LLAMADA A LA CONEXION BUENA EN ESTE ARCHIVO.
*/

include("_conexion.php");

//Se incluye el archivo que contiene las verificaciones de datos
include("../includes/verificaciones.php");

//Se incluye el archivo con los textos especifico de esta seccion en el idioma oportuno
//include($_SESSION["directorio_raiz"]."idiomas/".$_SESSION["idioma"]."/oficina_informacion_facturas.php");
include("../idiomas/".$_SESSION["idioma"]."/oficina_datos_titular.php");
include("../idiomas/".$_SESSION["idioma"]."/oficina_informacion_facturas.php");

include("../idiomas/".$_SESSION["idioma"]."/oficina_informacion_certificadofacturas.php");

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<title><?=$titulo_web;?></title>

<!--Se añaden las hojas de estilos genericas que utiliza esta seccion-->
<link href="../css/estilos.css" rel="stylesheet" type="text/css" />
<link href="../css/secciones.css" rel="stylesheet" type="text/css" />
<link href="css/oficina.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_vieja.css" rel="stylesheet" type="text/css" />

<link href="css/oficina_informacion_contratos_y_facturas.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_informacion_facturas.css" rel="stylesheet" type="text/css" />

<link href="css/oficina_informacion_contratos_y_facturas.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_informacion_certificadoconsumos.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_informacion_certificadofacturas.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" media="all" href="scripts/calendar/skins/aqua/theme.css" title="Aqua" />


<!-- librería principal del calendario -->
<script type="text/javascript" src="scripts/calendar/calendar.js"></script>

<!-- librería para cargar el lenguaje deseado -->
<script type="text/javascript" src="scripts/calendar/lang/calendar-es.js"></script>

<!-- librería que declara la función Calendar.setup, que ayuda a generar un calendario en unas pocas líneas de código -->
<script type="text/javascript" src="scripts/calendar/calendar-setup.js"></script>




<!--Ahora se añaden las reglas para Internet Explorer. Asi corregiremos las diferencias de visualizacion entre navegadores
Es importante añadirlo despues de las reglas principales, para que se sobreescriban las correcciones pertinentes. En este caso las diferencias
solo son para internet explorer 7-->

<!--[if IE 7]>
	<link href="../css/estilos_ie7.css" rel="stylesheet" type="text/css" />
	<link href="../css/secciones_ie7.css" rel="stylesheet" type="text/css" />    
<![endif]-->

<!--[if IE]>
	<link href="css/tablas_informacionIE.css" rel="stylesheet" type="text/css" />
<![endif]-->

<!--[if IE 7]>
	<link href="css/tablas_informacionIE7.css" rel="stylesheet" type="text/css" />
<![endif]-->

<script language="javascript" type="text/javascript">
function avisa()
{
	alert("OPCION NO DISPONIBLE");
}//function avisa()
//-->
</script>

<?php

//Asignamos los valores por defecto a la fecha de inicio y la fecha de fin, estos cambiaran si el usuario pulsa el boton del formulario habiendo escrito algo
$fecha_inicio="dd-mm-aaaa";
$fecha_fin="dd-mm-aaaa";

if(isset($_POST["Submit"]))
{	
	$fecha_inicio=$_POST["DesdeFecha"];
	$fecha_fin=$_POST["HastaFecha"];
	$observaciones=$_POST["observaciones"];	
	
	$error=comparar_fechas($fecha_inicio,$fecha_fin);		
}//if(isset($_POST["Submit"]))

?>

</head>
	
<body>


<script> 
var variableX = 3; 

function sumaCheck(num){ var suma=0; 
for(i=1;i<num+1;i++){ 
if(document.getElementById("check"+i).checked) 
suma = suma + parseFloat(document.getElementById("check"+i).value)


 
} 
/*le podrias sumar la variableX aca: 
return (suma+variableX) 
*/ 
document.getElementById("importeseleccionado").value=suma;
return (suma); 
} 
</script> 





<!--*************************************************Web realizada por ****************************************-->
	<div id="central_oficina">
    
<?php
//Se comprueba que el usuario se haya identificado, si no es asi se da un aviso y redirigimos a la seccion de identificacion
if ($_SESSION['usuario']=="")
{	
	MsgBox($error_identificacion);
	redirigir("oficina_index.php?id=".$_SESSION["idioma"]);
	
}//if ($_SESSION['usuario'])

//Si el usuario se ha identificado la seccion se ejecutara con normalidad
else
{
//Añadimos los fragmentos comunes, la cabecera, el menu, y el submenu
	include("oficina_fragmentos_comunes.php");
?>    
	  <div class="contenido_seccion_oficina_consultadeuda">                                         
<!--VERSION VIEJA-->        	
<!--VERSION VIEJA-->        	
        <table width="600" border="0" cellspacing="0" cellpadding="0">
		  <?php if (isset($_SESSION['numeroCliente'])){
					 		$result = mysql_query("SELECT * FROM DatosRegistrados WHERE CodigoCliente = ".$_SESSION['numeroCliente']." ");
							$row = mysql_fetch_array($result);
					 }else{
					 		$result = mysql_query("SELECT * FROM DatosRegistrados WHERE CodigoCliente = -1");
							//Para que no tome ningun cliente pero no de ningun error en la pagina
							$row = mysql_fetch_array($result);
					 }
					 ?>
            <tr> 
              <td valign="top"><div align="center">
  <table width="633" border="0" cellpadding="0" cellspacing="0" class="tabla_datos_contratos_y_facturas">
    
    
    <tr> 
      
      <td width="633" height="30" align="center" class="arialblack18Titulo">
        Medio Electr&oacute;nico Pago Facturas Pendientes</td>
      </tr> 
    <tr> 
      <td class="arial14">&nbsp;</td>
      </tr> 
    <tr>
      <td class="arial14"><fieldset >
        <legend class="arialblack14">
          <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 130'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
          </legend>
        <table width="tabla_datos_contratos_y_facturas" align="center">
          <tr>
            <td height="3">&nbsp;</td>
            </tr>
          <tr>
            <td height="22" valign="top" class="arial14"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 196'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
              &nbsp;</td>
            <td valign="top" class="arial14"><input name="CodigoCliente" type="text" class="textfieldLecturaNum" id="CodigoCliente" value="<?php echo($row['CodigoCliente']);?>" size="5" maxlength="5"  readonly="readonly" />
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 118'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
              &nbsp;
              <input name="SuministroDNI"  type="text" class="textfieldLecturaNum" id="SuministroDNI" value="<?php echo($row['DNI']);?>" size="9" maxlength="15" readonly="readonly" />
              &nbsp;</td>
            </tr>
          <tr>
            <td height="22" class="arial14">CUPS&nbsp;</td>
            <td valign="top" class="arial14"><input name="CUPSFijo" value="<?php echo($row['CUPS']);?>"  type="text" class="textfieldLectura" id="CUPSFijo" size="25" maxlength="25" /></td>
            </tr>
          <tr>
            <td height="22" valign="top" class="arial14"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 119'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
              &nbsp;&nbsp;</td>
            <td valign="top"><input name="SuministroNombre" type="text"  class="textfieldLectura" id="SuministroNombre" value="<?php echo(str_replace('"','&quot;',$row['TitularNombre']));?>" size="50" maxlength="50" readonly="readonly" /></td>
            </tr>
          <tr>
            <td height="22" valign="top" class="arial14"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 123'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
              &nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td valign="top"><input name="SuministroCalle" type="text"  class="textfieldLectura" id="SuministroExtensionNombre3" value="<?php echo(str_replace('"','&quot;',$row['SuministroCalle']));?>" size="50" maxlength="50" readonly="readonly" /></td>
            </tr>
          <tr>
            <td height="22" valign="top" class="arial14"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 124'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
              &nbsp;</td>
            <td valign="top" class="arial14"><input readonly="readonly"  name="SuministroNumero" type="text" class="textfieldLecturaNum" id="SuministroNumero" value="<?php echo($row['SuministroNumero']);?>" size="5" maxlength="5" />
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 125'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
              &nbsp;
              <input name="SuministroExtension" type="text" class="textfieldLectura" id="SuministroExtension" value="<?php echo(str_replace('"','&quot;',$row['SuministroExtension']));?>" size="20" maxlength="20" readonly="readonly" /></td>
            </tr>
          <tr>
            <td height="22" valign="top" class="arial14"><?=$oficina_datos_titular21?></td>
            <td valign="top"><input name="SuministroAclaradorFijo" type="text" class="textfieldLectura" value="<?php echo(str_replace('"','&quot;',$row['SuministroExtension2']));?>" size="50" maxlength="50" /></td>
            </tr>
          <tr>
            <td height="24" valign="middle" class="arial14"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 122'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?></td>
            <td valign="top"><input readonly="readonly"  name="SuministroCP2" type="text" class="textfieldLecturaNum" id="SuministroCP3" value="<?php echo($row['SuministroCP']);?>" size="5" maxlength="5" /></td>
            </tr>
          
          
          <tr>
            <td height="21" valign="middle" class="arial14"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 121'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?></td>
            <td valign="top"><input  name="SuministroExtension2" type="text" class="textfieldLectura" id="SuministroExtension3" value="<?php echo(str_replace('"','&quot;',$row['SuministroCiudad']));?>" size="50" maxlength="50" readonly="readonly" /></td>
            </tr>
          </table>
        </fieldset></td>
      </tr>
    <tr align="center"> 
      <td class="arialblack16">&nbsp;</td>
      </tr>
    <tr align="center"> 
      <td align="center" class="arial12Importanteb">
        <p>Este m&oacute;dulo le permite realizar el Pago de Facturas Pendientes por medio de una Tarjeta de Cr&eacute;dito, sin necesidada de desplazarse a nuestra Oficina o a cualquier Entidad Bancaria.<br />
          <br />
          Le detallamos el contenido de todas las facturas que est&aacute;n pendientes. Debe seleccionar aquellas que desee pagar y las facturas seleccionadas se reflejar&aacute;n en la parte inferior obteniendo el importe total que va a ser cargado en Tarjeta de Cr&eacute;dito.</p>
        <p>Si el pago se realiza porque ya le hemos efectuado un Corte de Energ&iacute;a en su domicilio, debe de pagar no solamente la factura que tiene asignado el Corte, sino la de Reconexi&oacute;n a que hace Referencia y todas las que correspondan a fechas anteriores. De no ser as&iacute; no podremos efectuarle la reposici&oacute;n de energ&iacute;a en su Punto de Suministro.</p></td>
      </tr>
    <tr align="center"> 
      <td height="50" class="arialblack16">&nbsp;</td>
      </tr>
    <tr align="center"> 
      <td class="arialblack16Titulo">Relación de Facturas Pendientes de Pago</td>
      </tr>
    
  </table>
  <?php 
		   
		
			$importetotal=0;
			$i=1;
			$consulta_medio_pago="SELECT * FROM RecibosPendientes WHERE  NumeroCliente=".$_SESSION['numeroCliente']." ORDER BY `FechaFactura` DESC";
			//echo($consulta_medio_pago);
			//echo("<br/>");
			$resultados_medio_pago = mysql_query($consulta_medio_pago);
			//echo($resultados_medio_pago);			
			//echo("<br/>");
			$num_medio_pago=mysql_num_rows($resultados_medio_pago);
			$n2=$num_medio_pago;
			//echo($num_medio_pago);
			//echo("<br/>");
			//$registro_medio_pago = mysql_fetch_array($resultados_medio_pago);
			//echo($registro_medio_pago);
			//echo("<br/>");


//Si hay facturas se mostrara un resumen de los datos de cada una de ellas			
			if($num_medio_pago != 0)
			{
				
				while($registro_medio_pago = mysql_fetch_array($resultados_medio_pago))
				{
					
			//$consulta_medio_pago="SELECT * FROM FacturasPDF WHERE (NumeroCliente=".$_SESSION['numeroCliente']." AND FechaFactura = '".$registro_medio_pago["FechaFactura"]."')";
			//$resultados_medio_pago = mysql_query($consulta_medio_pago);			
			//$num_medio_pago=mysql_num_rows($resultados_medio_pago);
			//$registro_medio_pago = mysql_fetch_array($resultados_medio_pago);


		
?>
                <br />
                
              </div>
                <div >
                
	  <div class="columnas_fila4_contrato">
                    	<div class="color_fondo_cabecera_contrato times12Azul4">Nº Factura</div>
                       	<div class="color_fondo_datos_contrato alto_factura_periodo arial12"><?=$registro_medio_pago["NumeroFactura"]?></div>                                         
                  </div><!--<div class="columnas_fila3_contrato">-->

			  <div class="columnas_fila4_contrato">
                    	<div class="color_fondo_cabecera_contrato times12Azul4">Fecha	</div>
                       	<div class="color_fondo_datos_contrato alto_factura_periodo arial12"><?=fecha_normal($registro_medio_pago["FechaFactura"])?></div>                                           
                  </div><!--<div class="columnas_fila3_contrato">-->

			  <div class="columnas_fila4_contrato">
                    	<div class="color_fondo_cabecera_contrato times12Azul4">N&ordm; Recibo	</div>
                       	<div class="color_fondo_datos_contrato alto_factura_periodo arial12"><?=$registro_medio_pago["NumeroRecibo"]?></div>                                                
                  </div><!--<div class="columnas_fila3_contrato">-->

			  <div class="columnas_fila4_contrato">
                    	<div class="color_fondo_cabecera_contrato times12Azul4">Origen Fact	</div>
                       	<div class="color_fondo_datos_contrato alto_factura_periodo arial12"><?
						
						switch ($registro_medio_pago["TipoFactura"])
						{
						case 'E':						{$tipofactura="Energía";}						break;
						case 'R':						{$tipofactura="Reconexión";}					break;
						case 'D':						{$tipofactura="Derechos";}						break;
						case 'V':						{$tipofactura="Varios";}						break;
						default :						{$tipofactura="------";}      					break;
						}
						echo($tipofactura);
						
						
						?></div>
                  </div><!--<div class="columnas_fila3_contrato columna_factura_periodo">-->                    

			  <div class="columnas_fila4_contrato">
                    	<div class="color_fondo_cabecera_contrato times12Azul4">Tipo Recibo	</div>
                       	<div class="color_fondo_datos_contrato alto_factura_periodo arial12">
						
						<?
						
						switch ($registro_medio_pago["TipoRecibo"])
						{
						case 'F':				{$tiporecibo="Facturado";}					break;
						case 'I':				{$tiporecibo="Impagado";}					break;
						default :				{$tiporecibo="------";}     				break;
						}
						echo($tiporecibo);
									
						?>
			  </div>                                                                     
                  </div><!--<div class="columnas_fila3_contrato">-->                    

			  <div class="columnas_fila4_contrato">
                    	<div class="color_fondo_cabecera_contrato times12Azul4">Est Impagado	</div>
                       	<div class="color_fondo_datos_contrato alto_factura_periodo arial12">
						
						<?
						
						switch ($registro_medio_pago["EstadoImpagado"])
						{
						case '1':				{$tipoestadoimpagado="Pendiente";}					break;
						case 'C':				{$tipoestadoimpagado="Carta de Corte";}				break;
						case 'O':				{$tipoestadoimpagado="Cortado";}					break;
						default :				{$tipoestadoimpagado="------";}     				break;
						}
						echo($tipoestadoimpagado);
									
						?>
              </div>                                                
                  </div><!--<div class="columnas_fila3_contrato">-->                    

					<div class="columnas_fila5_contrato">
                    	<div class="color_fondo_cabecera_contrato times12Azul4">Importe</div>
                        <div class="color_fondo_datos_contrato alto_factura_periodo arial12"><?=$registro_medio_pago["ImportePendiente"]?><? echo(" ");?></div>                                                                         
                    </div><!--<div class="columnas_fila3_contrato">-->                                        

           


</div><!--<div class="columnas_fila1_larga_contrato">--> 

                    <div class="limpiar"></div>
                    
                   

                                        
          <div class="columnas_fila6_contrato">
                    	<div class="color_fondo_cabecera_contrato times12Azul4">Nombre Titular	</div>
                       	<div class="color_fondo_datos_contrato arial12">
						
						<?php echo($row['TitularNombre']);?>
						
						</div>                                         
                </div><!--<div class="columnas_consumos_informacion_factura">-->
                    
		  <div class="columnas_fila6_contrato">
                    	<div class="color_fondo_cabecera_contrato times12Azul4">Direcci&oacute;n Punto Suministro</div>
                       	<div class="color_fondo_datos_contrato arial12">
						
						<?php echo($row['SuministroCalle']);?>
                        
	</div>                                         
                </div><!--<div class="columnas_consumos_informacion_factura">-->

		  <div class="columnas_fila6_contrato">
                    	<div class="color_fondo_cabecera_contrato times12Azul4">Poblaci&oacute;n Punto Suministro</div>
                       	<div class="color_fondo_datos_contrato arial12">
						
					    <?php echo($row['SuministroCiudad']);?>
                        
    </div>                                         
                </div><!--<div class="columnas_consumos_informacion_factura">-->
                    
		
                <div class="limpiar"> </div>
                    
                  
<div class="posicion_boton_imprimir_certificado_consumo">

			<form action="" method="post">
   	   	    
   	   	    <input type="checkbox" name="check<?=$i?>" id="check<?=$i?>" value="<?=$registro_medio_pago["ImportePendiente"]?>" onchange="sumaCheck(<?=$n2?>)"/>
   	   		
           </form>

   	   	    
</div><!--<div class="posicion_boton_imprimir_certificado_consumo">-->
                        
                        
						<div class="posicion_texto_imprimir_certificado_consumo">		
                    		<a class="texto_resaltado">Seleccionar Factura</a>
              </div><!--<div class="posicion_boton_imprimir_certificado_consumo">-->
                    
     
                     
                     <div class="limpiar"></div>
                   <br />
    
   
                       
  <?php 
			$importetotal=$importetotal+$registro_medio_pago["ImportePendiente"];
			$i=$i+1;

				}//while($row = mysql_fetch_array($result))				  
		   }//if($num_facturas != 0)
		   
//Si no hay facturas en la base de datos mostraremos un mensaje al usuario
		   else
		   {
?>
                
                  
                <p>&nbsp;</p>
                <div class="error_no_registros"><?=$error_no_facturas?></div>
                <p>
                  <?php		   
		   }//else($num_facturas != 0)
$i=$i-1;

?>

                <p>
                <p>&nbsp;</p>
                <table width="642" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                        <td width="284">&nbsp;</td>
                        <td width="178" class="arial14">Importe total a pagar</td>
                        <td width="180"><div class="columnas_fila5_contrato">
                          
                          <div class="color_fondo_cabecera_contrato times12Azul4">
                          <?php echo(number_format($importetotal,2,',','.'));?>
                          <? echo(" &euro;");?></div>
                        </div></td>
                    

                  </tr>
                </table>
                <p>&nbsp;                </p>
                <p>
                  <input type="button" onclick="sumaCheck(<?=$i?>)" value="Calcula la suma de las facturas seleccionadas." />
                  <br />
                </p>
                <table width="642" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="194">&nbsp;</td>
                    <td width="268" class="arial14b">Importe seleccionado a pagar</td>
                    <td width="180"><div class="columnas_fila5_contrato">
                      
                      <input name="" type="text" id="importeseleccionado"/>
                      
                       sumaCheck(<?=$i?>) <? echo(" &euro;");?>
                    </div></td>
                  </tr>
                </table>
                <br />
<br />
</tr>
            <tr class="tabla_datos_contratos_y_facturas">
              <td class="arial14">&nbsp;</td>
            </tr>
            <tr class="tabla_datos_contratos_y_facturas">
              <td class="arial14b">
               
               
               
               
               
                <div align="center">
                  <table width="550" border="0" cellpadding="0" cellspacing="0">
                    <tr class="arial12"> 
                      <td colspan="2" class="arial14" width="550">
                        
                        <fieldset   >
                          <legend class="arialblack14">Datos Personales
                            
                          <? //   =$oficina_datos_titular16?>
                            
                          </legend>
                          
                       <table width="500" align="center">
                             <tr> 
                              <td width="119" height="24" valign="top">
                                
                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                  
                                  <tr>
                                    <td width="75" height="20" valign="middle" class="arial14">Nombre</td>
                                  </tr>
                                </table>
                                
                              </td>
                              
                              
                              <td width="319" valign="top"><input name="FacturaNombre" value="<?=$factura_nombre?>" type="text" class="textfield" id="FacturaNombre" size="52" maxlength="50" /></td>
                            </tr>
                            <tr> 
                               <td height="24" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                                  
                                 <tr>
                                   <td width="75" height="20" valign="middle" class="arial14">Apelidos</td>
                                 </tr>
                               </table>                        </td>
                               <td valign="top"><input name="FacturaCalle" value="<?=$factura_calle?>" type="text" class="textfield" id="FacturaCalle" size="52" maxlength="50" /></td>
                            </tr>
                          
                                                                             
                            
                            <tr> 
                              <td height="24" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                                
                                <tr>
                                  <td width="75" height="20" valign="middle" class="arial14">Direcci&oacute;n</td>
                                </tr>
                              </table>                        </td>
                              <td valign="top"><input name="FacturaAclarador" value="<?=$factura_aclarador?>" type="text" class="textfield" id="FacturaAclarador" size="52" maxlength="50" /></td>
                            </tr>
                            <tr>
                              <td height="24" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                                  
                                <tr>
                                  <td width="73" height="20" valign="middle" class="arial14">C&oacute;digo Postal</td>
                                </tr>
                              </table>                        </td>
                              <td valign="top"><input name="FacturaCP" type="text"  value="<?=$factura_cp?>"  class="textfieldnum" id="FacturaCP" size="10" maxlength="10" /></td>
                            </tr>
                            
                            
                            
                            
                            <tr>
                              <td height="24" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                                
                                <tr>
                                  <td width="73" height="20" valign="middle" class="arial14">Poblaci&oacute;n</td>
                                </tr>
                              </table>                        </td>
                              <td valign="top"><input name="FacturaPoblacion" type="text"  value="<?=$factura_poblacion?>"  class="textfield" id="FacturaPoblacion" size="52" maxlength="50" /></td>
                            </tr>
                            
                            
                            
                             <tr>
                              <td height="24" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                                
                                <tr>
                                  <td width="73" height="20" valign="middle" class="arial14">Provincia</td>
                                </tr>
                              </table>                        </td>
                              <td valign="top"><input name="FacturaPoblacion" type="text"  value="<?=$factura_provincia?>"  class="textfield" id="FacturaPoblacion" size="52" maxlength="50" /></td>
                            </tr>
                            
                            
                            
                            <tr>
                              <td height="24" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                                
                                <tr>
                                  <td width="73" height="20" valign="middle" class="arial14">Tel&eacute;fono *</td>
                                </tr>
                              </table>                        </td>
                              <td valign="top"><input name="FacturaProvincia" type="text"  value="<?=$factura_provincia?>"  class="textfield" id="FacturaProvincia" size="52" maxlength="50" /></td>
                            </tr>
                            
                            
                           
                            <tr> 
                              <td height="24" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                                
                                <tr>
                                  <td width="75" height="20" valign="middle" class="arial14">Email</td>
                                </tr>
                              </table>                        </td>
                              <td valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                                
                                <tr>
                                  <td width="363" height="20" valign="top" class="arial14"><input name="FacturaTelefono" type="text" class="textfield" value="<?=$factura_telefono?>"  id="FacturaTelefono" size="52" maxlength="50" />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                </tr>
                              </table>                        </td>
                            </tr>
                            
                              <tr> 
                              <td height="16" colspan="2" valign="top" class="arial11"><div align="left">
                                <blockquote>
                                  <p>* Campos Obligatorios </p>
                                </blockquote>
                              </div></td>
                            </tr>
                            
                            
  <!--CIERRE DEL APARTADO-->                      
                          </table>
                        </fieldset></td>
                    </tr>
  <!--CIERRE DEL APARTADO-->                      
  </table>
                  <br />
                </div></td></tr>
            <tr class="tabla_datos_contratos_y_facturas">
              <td class="arial14b"><div align="center">
                <p align="left"><span class="arialblack14">Forma de Pago
                <? //   =$oficina_datos_titular16?>
                </span></p>
                <table width="550" border="0" cellpadding="0" cellspacing="0">
                  <tr class="arial12">
                    <td colspan="2" class="arial14" width="550"><fieldset   >
                      <legend class="arialblack14"></legend>
                      <p align="center" class="arial12Importanteb">El Sistema de Pago no admite el uso de Tarjetas Virtuales.<br />
                      Es conveniente tener a la vista la Tarjeta de Cr&eacute;dito a trav&eacute;s de la cual va a realizar el Pago</p>
                      <table width="600" border="0" cellspacing="2" cellpadding="0">
                        <tr>
                          <td width="148">Titular de la Tarjeta</td>
                          <td width="33">&nbsp;</td>
                          <td colspan="4"><input name="FacturaProvincia2" type="text"  value="<?=$factura_provincia?>"  class="textfield" id="FacturaProvincia2" size="60" maxlength="50" /></td>
                        </tr>
                        <tr>
                          <td>&nbsp;</td>
                          <td>&nbsp;</td>
                          <td width="102">&nbsp;</td>
                          <td width="95">&nbsp;</td>
                          <td width="97">&nbsp;</td>
                          <td width="111">&nbsp;</td>
                        </tr>
                        <tr>
                          <td>N&uacute;mero de Tarjeta</td>
                          <td>&nbsp;</td>
                          <td colspan="4"><div align="left">
                            <input name="FacturaCP2" type="text"  value="<?=$factura_cp?>"  class="textfieldnum" id="FacturaCP2" size="3" maxlength="4" />
                            <input name="FacturaCP3" type="text"  value="<?=$factura_cp?>"  class="textfieldnum" id="FacturaCP3" size="3" maxlength="4" />
                            <input name="FacturaCP4" type="text"  value="<?=$factura_cp?>"  class="textfieldnum" id="FacturaCP4" size="3" maxlength="4" />
                            <input name="FacturaCP5" type="text"  value="<?=$factura_cp?>"  class="textfieldnum" id="FacturaCP5" size="3" maxlength="4" />
                          </div>                            <div align="left"></div>                          <div align="left"></div>                          <div align="left"></div></td>
                        </tr>
                        <tr>
                          <td>&nbsp;</td>
                          <td>&nbsp;</td>
                          <td>&nbsp;</td>
                          <td>&nbsp;</td>
                          <td>&nbsp;</td>
                          <td>&nbsp;</td>
                        </tr>
                        <tr>
                          <td>Fecha de Caducidad</td>
                          <td>&nbsp;</td>
                          <td><div align="right">Mes</div></td>
                          <td><select name="MesCaducidad" id="MesCaducidad">
                            <option>1</option>
                              <option>2</option>
                              <option>3</option>
                              <option>4</option>
                              <option>5</option>
                              <option>6</option>
                              <option>7</option>
                              <option>8</option>
                              <option>9</option>
                              <option>10</option>
                              <option>11</option>
                              <option>12</option>
                          </select></td>
                          <td><div align="right">A&ntilde;o</div></td>
                          <td><form id="form1" name="form1" method="post" action="">
                            <label for="AnoCaducidad"></label>
                            <select name="AnoCaducidad" id="AnoCaducidad">
<option>2011</option>
                              <option>2012</option>
                              <option>2013</option>
                              <option>2014</option>
                              <option>2015</option>
                              <option>2016</option>
                              <option>2017</option>
                              <option>2018</option>
                              <option>2019</option>
                              <option>2020</option>
                            </select>
                          </form></td>
                        </tr>
                        <tr>
                          <td>&nbsp;</td>
                          <td>&nbsp;</td>
                          <td>&nbsp;</td>
                          <td>&nbsp;</td>
                          <td>&nbsp;</td>
                          <td>&nbsp;</td>
                        </tr>
                        <tr>
                          <td>CCV2</td>
                          <td>&nbsp;</td>
                          <td><input name="FacturaCP6" type="text"  value="<?=$factura_cp?>"  class="textfieldnum" id="FacturaCP6" size="2" maxlength="3" /></td>
                          <td colspan="2"> <div align="center" class="arial12black">&iquest;Que es el CVV2?</div></td>
                          <td>&nbsp;</td>
                        </tr>
                      </table>
                      <p>&nbsp;</p>
                      <table width="600" border="0" cellspacing="2" cellpadding="0">
                        <tr>
                          <td height="36" colspan="2" class="arialblack14">Condiciones Legales</td>
                        </tr>
                        <tr>
                          <td colspan="2">AVISO LEGAL: Este aviso regula la utilizaci&oacute;n por parte de los usuarios de las p&aacute;ginas web contenidas en este sitio de Internet (en adelante website), propiedad de esta empresa.</td>
                        </tr>
                        <tr>
                          <td colspan="2">&nbsp;</td>
                        </tr>
                        <tr>
                          <td colspan="2"><form id="form2" name="form2" method="post" action="" readonly="readonly">
                            <label for="CondicionesLegales"></label>
                            <textarea name="CondicionesLegales" cols="145" rows="7" class="arial10Negro" id="CondicionesLegales">El acceso y la navegación en el website implican el conocimiento y aceptación de las advertencias legales, las condiciones y los términos de uso contenidos en el mismo.

Entradas See Tickets S.A. aplica medidas dirigidas a garantizar que la navegación en el website se realice en condiciones óptimas, aunque no garantiza su disponibilidad y continuidad en todo momento. Particularmente, Entradas See Tickets S.A. no se responsabiliza de las caídas o interrupciones del servicio de comunicaciones electrónicas que haya de servir de soporte a los servicios de la sociedad de la información que son ofrecidos a través del website.

Entradas See Tickets S.A. aplica medidas dirigidas a garantizar la privacidad y la seguridad de las comunicaciones electrónicas precisas para la contratación de los productos y de los servicios que comercializa a través del presente website o que impliquen la captura de datos personales y cumple con los estándares de seguridad tecnológicamente disponibles en la actualidad (como el sistema "Verisign"). No obstante, Entradas See Tickets S.A. no garantiza la privacidad y la seguridad en la utilización del website y, en particular, no garantiza que terceros no autorizados no puedan tener conocimiento acerca de la clase, condiciones, características y circunstancias del uso que los usuarios realizan del website.

Entradas See Tickets S.A. aplica medidas dirigidas a garantizar la no presencia en el website o en sus contenidos de elementos de carácter informático que puedan producir alteraciones en los equipos para el proceso de información de los usuarios o en cualquier tipo de archivo almacenado en los mismos. No obstante, Entradas See Tickets S.A. no garantiza la no introducción en el website o en sus contenidos por parte de los usuarios o de terceros de elementos de carácter informático que produzcan una alteración de los equipos para el proceso de información de los usuarios o de cualquier tipo de archivo almacenado en los mismos.

La información que aparece en el website es la vigente en la fecha de su última actualización. Entradas See Tickets S.A. se reserva el derecho a, en cualquier momento, actualizar, modificar o eliminar la información de este website, pudiendo limitar o no permitir el acceso al mismo. Entradas See Tickets S.A. declina la responsabilidad frente al uso indebido de los contenidos de su página web por parte de los usuarios, así como frente a informaciones no contenidas en su página web o que no hayan sido elaboradas por Entradas See Tickets S.A.

Entradas See Tickets S.A. realiza los máximos esfuerzos para evitar cualquier error en los contenidos del website, aunque no garantiza ni se responsabiliza de las consecuencias derivadas de los errores en los contenidos del website proporcionados por terceros.

Entradas See Tickets S.A. no concede ninguna licencia o autorización de uso de ninguna clase sobre sus derechos de propiedad industrial e intelectual o sobre cualquier otra propiedad o derecho relacionado con el website o sus contenidos.

                            </textarea>
                          </form></td>
                        </tr>
                        <tr>
                          <td colspan="2">&nbsp;</td>
                        </tr>
                        <tr>
                          <td width="229"><div align="center"><img src="../img/oficina/formaspago.jpg" width="149" height="99" /></div></td>
                          <td width="366" class="arial12Negro">Servicio Garantizado y respaldado por Verisign.<br />
                            Los Datos facilitados se enviar&aacute;n encriptados a un servidor seguro en el que ser&aacute;n tratados de forma estr&iacute;ctamente confidencial</td>
                        </tr>
                        <tr>
                          <td colspan="2">&nbsp;</td>
                        </tr>
                        <tr>
                          <td colspan="2" class="arial12Negro"><blockquote>
                            <p>He le&iacute;do y al pulsar la opci&oacute;n de &quot;comprar&quot;, acepto y estoy conforme con las condiciones generales del contrato y la pol&iacute;tica de privacidad y protecci&oacute;n de datos</p>
                          </blockquote></td>
                        </tr>
                        <tr>
                          <td colspan="2" class="arial12Negro">&nbsp;</td>
                        </tr>
                        <tr>
                          <td colspan="2" class="arial12Negro"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td width="58"><div align="center">
                                <input type="checkbox" name="recibircomunicaciones" id="recibircomunicaciones" />
                              </div>
                              <label for="recibircomunicaciones"></label><div align="center"></div></td>
                              <td width="524">No deseo recibir comunicaciones electr&oacute;nica de car&aacute;cter comercial enviadas por esta empresa</td>
                            </tr>
                          </table></td>
                        </tr>
                      </table>
                      <p align="right">
                        <input type="submit" name="finalizarcompra" id="finalizarcompra" value="Finalizar Compra" />
                      </p>
                      <p>&nbsp;</p>
                    </fieldset></td>
                  </tr>
                  <!--CIERRE DEL APARTADO-->
                </table>
              </div>
              <div align="center"></div></td>
            </tr>
                </table>
<!--CIERRE DEL APARTADO-->                      
<table width="600" border="0" cellspacing="0" cellpadding="0"><tr class="tabla_datos_contratos_y_facturas"><td class="arial14b">
               
               
               
               
               
          </td>
          </tr>
        </table>
	      </td>
          </tr>
          </table>
       
        <!--VERSION VIEJA-->        	
        	
  </div><!--<div class="contenido_seccion_oficina">-->                        
		
        
        <div class="limpiar"></div>
                        	
<!--Por ultimo aparecera el pie de la web-->
<?php 
	include("../includes/pie.php");

//NOTA: EN ESTA OCASION, EL ERROR SE MOSTRARA DESPUES DE LA CARGA DE LA PAGINA, SI NO POR ALGUN MOTIVO EN OCASIONES DESPUES DE DAR EL ERROR CESA LA CARGA DE LA PAGINA
	if(isset($_POST["Submit"]))
	{
//Por ultimo mostraremos el error correspondiente, en el caso de que haya fallado alguna de las verificaciones, o continuaremos con el proceso		
		switch($error)
		{		
//Proceso OK	
			case 0:						
//Antes de realizar el alta en la base de datos, deberemos de preparar los datos que no nos da el formulario
			
				$insertar_solicitud_factura="INSERT INTO `SolicitudFacturas` (`NumeroCliente` ,`FechaInicial` ,`FechaFinal` ,`FechaRegistro` ,
`HoraRegistro` ,`Observaciones`)VALUES ('".$_SESSION['numeroCliente']."', '".fecha_mysql($fecha_inicio)."', '".fecha_mysql($fecha_fin)."', '".date("Y-m-d")."', '".date("Y-m-d H:i:s")."', '".$observaciones."')";
								
				$ejecutar_solicitud_facturas=mysql_query($insertar_solicitud_factura);
															
				MsgBox($solicitud_ok);
														
			break;
				
//Error por no rellenar los campos obligatorios
			case 1:
				MsgBox($error_rellenar);
			break;								
			
//Error por escribir la fecha incorrectamente
			case 2:
				MsgBox($error_fecha);
			break;									
		
//Error por fechas escritas posterior a la actual
			case 3:
				MsgBox($error_fecha_posterior);
			break;									

//Error por que la fecha de inicio es mayor que la fecha final
			case 4:
				MsgBox($error_fecha_inicio_mayor);
			break;											
			
		}//switch($error)
	}//if(isset($_POST["Submit"]))	
}//else ($_SESSION['usuario'])
?>        
    </div><!--<div id="central_oficina"-->
</body>
</html>
