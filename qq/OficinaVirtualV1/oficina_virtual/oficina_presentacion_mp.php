<?php
/* NOTA:
 COMO NO SE CIERRA LA CONEXION TRAS CADA CONEXION CON LA BASE DE DATOS, LAS CONSULTAS DEJAN DE FUNCIONAR SI SE ESTABLECE COMO UNICA CONEXION
 LA QUE UTILIZA EL RESTO DE LA WEB, POR LO TANTO HASTA QUE SE HAGA LA MIGRACION COMPLETA DE LOS TEXTOS, COEXISTIRAN LAS DOS CONEXIONES, YA QUE
 SE HA AÑADIDO UNA LLAMADA A LA CONEXION BUENA EN ESTE ARCHIVO.
*/

include("_conexion.php");

//Se incluye el archivo con los textos especifico de esta seccion en el idioma oportuno
include("../idiomas/".$_SESSION["idioma"]."/oficina_presentacion.php");
include("../idiomas/".$_SESSION["idioma"]."/oficina_submenu.php");

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<title><?=$titulo_web;?></title>
<link rel="shortcut icon" href="../favicon.ico">

<!--Se añaden las hojas de estilos genericas que utiliza esta seccion-->
<link href="../css/estilos.css" rel="stylesheet" type="text/css" />
<link href="../css/secciones.css" rel="stylesheet" type="text/css" />
<link href="css/oficina.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_presentacion_mp.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_presentacion.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_vieja.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_informacion_contratos_y_facturas.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_informacion_certificadofacturas.css" rel="stylesheet" type="text/css" />

<link rel="stylesheet" type="text/css" media="all" href="scripts/calendar/skins/aqua/theme.css" title="Aqua" />


<script type="text/javascript" src="scripts/calendar/calendar.js"></script>
<!-- librería para cargar el lenguaje deseado -->
<script type="text/javascript" src="scripts/calendar/lang/calendar-es.js"></script>
<!-- librería que declara la función Calendar.setup, que ayuda a generar un calendario en unas pocas líneas de código -->
<script type="text/javascript" src="scripts/calendar/calendar-setup.js"></script>

<!--Ahora se añaden las reglas para Internet Explorer. Asi corregiremos las diferencias de visualizacion entre navegadores
Es importante añadirlo despues de las reglas principales, para que se sobreescriban las correcciones pertinentes. En este caso las diferencias
solo son para internet explorer 7-->

<!--[if IE 7]>
	<link href="../css/estilos_ie7.css" rel="stylesheet" type="text/css" />
	<link href="../css/secciones_ie7.css" rel="stylesheet" type="text/css" />    
<![endif]-->

<style>
#fechasExcel, #fechasPDF, #comisiones{
	position:fixed;
	left:50%;
	top:50%;
	margin-left:-250px; 
	margin-top:-190px;        
    z-index:2;
    /*overflow:auto;*/
    display:none;               
	background:#f0f0f0;
	border: 2px solid #CCC;                   
	width:500px;
	padding:10px 10px 10px 10px;
	height:420px;
	/*margin-top:-20px;*/ /*margin:auto;*/
}
#comisiones{ height:240px;}
input.submit1{width:70px; height:70px; background-image:url(../img/oficina/excel.png); text-indent:-5000px}
input.submit2{width:70px; height:70px; background-image:url(../img/oficina/pdf.png); text-indent:-5000px}
#fechasExcel table{ font-weight:normal;}

table.resultados{width:950px;border-collapse:collapse; text-align:center}
tr.cabecera{font-size:15px; font-style:italic; border-bottom:2px solid #FFF; font-weight:bold;}
tr.cabecera td{padding:7px; color:#003C58}
tr.linea td{ border-bottom: 1px solid rgba(0, 60, 88, 0.25);
    height: 15px;
    padding: 3px 3px 8px;}
	
tr.inputs{ background-color: #003c58;
    height: 10px;
    padding: 10px 0;}
tr.inputs input { font-weight:bold; border-radius:5px;  box-shadow:none }
.calendar{z-index:10000;}
</style>

</head>
	
<body>
<?php
$hoy = date("Y-m-d");
$hoy = explode("-",$hoy);

$fecha_inicio_ver = '2013-'.$hoy[1].'-01';
$fecha_fin_ver=date("Y-m-d");
?>

<div id="fechasExcel">
<a href="#" onclick="muestra_oculta('fechasExcel')"><?=$cerrar;?></a>
<h3 align="center" style="color:#000"><?=$resumenFacturas;?></h3>
<form action="obtenerfacturas.php" method="POST" onsubmit="muestra_oculta('fechasExcel')">
    <input type="text" value="<?=$_SESSION['password_mp'];?>" name="nif" style="display:none;" />
	<input type="text" value="<?=$_SESSION['usuario_mp'];?>" name="dni" style="display:none;" />
    
    <table borde="0" align="center" width="480" style="color:#000">
    <tr>
    	<td colspan="2" align="center"><?=$resumenFacturasIntro;?> <i></i><br /><br /></td>
    </tr>
    <tr>
    	<td><?=$fechaInicio;?></td><td><?=$fechaFin;?></td>
    </tr>
    <tr>	
        <td>
        <input name="FechaInicial"  id="sel1" value="<?=$fecha_inicio_ver?>"  type="text" class="textfield" size="15" maxlength="10"  onchange="MostrarConsultaAgentes(this.value, this.form.FechaFinal.value, this.form.nif.value)"> 
                            &nbsp;                    
        <input type="button" id="lanzador" value="..." />
        <!-- script que define y configura el calendario-->
        <script type="text/javascript">
			Calendar.setup({
				inputField : "sel1", // id del campo de texto
				ifFormat : "%Y-%m-%d", // formato de la fecha que se escriba en el campo de texto
				button : "lanzador" // el id del botón que lanzará el calendario
			});
        </script>
        </td><td>
        <input name="FechaFinal" id="sel2" value="<?=$fecha_fin_ver?>"type="text" class="textfield" size="15" maxlength="10"  onchange="MostrarConsultaAgentes(this.form.FechaInicial.value, this.value, this.form.nif.value); return false">&nbsp;
        
        <input type="button" id="lanzador2" value="..." />
        <!-- script que define y configura el calendario-->
        <script type="text/javascript">
			Calendar.setup({
				inputField : "sel2", // id del campo de texto
				ifFormat : "%Y-%m-%d", // formato de la fecha que se escriba en el campo de texto
				button : "lanzador2" // el id del botón que lanzará el calendario
			});
        </script></td>
    </tr>
    <tr>
        <td colspan="2" align="center">
        <br />
		<!-- DEJAMOS COMENTADA LA LINEA DEL EXCEL A LA ESPERA DE INSTRUCCIONES -->
        <!--<input type="submit" name="submit" class="submit1" value="GENERAR EXCEL" />&nbsp;&nbsp;-->
        <!--<input type="submit" name="submit" class="submit2" value="GENERAR PDF" />-->
		<input type="submit" name="submit" class="submit2" value="DESCARGAR PDF" />
        </td>
    </tr>
	<tr>
        <td colspan="2" align="center">
        <br />
        <!-- <span>GENERAR EXCEL</span> -->
		&nbsp;&nbsp;
        <!-- <input type="submit" name="submit" class="submit2" value="GENERAR PDF" /> -->
        <span>DESCARGAR PDF</span>
        </td>
    </tr>
    </table>
    <div id="resultado" style="padding-top:10px;min-height:200px; max-height:300px;"></div>
    
</form>
</div>	
	
<!--*************************************************Web realizada por ****************************************-->
	<div id="central_oficina">
    
<?php
//Se comprueba que el usuario se haya identificado, si no es asi se da un aviso y redirigimos a la seccion de identificacion
if ($_SESSION['usuario_mp']=="")
{	
	MsgBox($error_identificacion);
	redirigir("oficina_index.php?id=".$_SESSION["idioma"]);
	
}//if ($_SESSION['usuario'])

//Si el usuario se ha identificado la seccion se ejecutara con normalidad
else
{
//Añadimos los fragmentos comunes, la cabecera, el menu, y el submenu
		
	//$ord=0;
	$ord=$_GET['ord'];
	?>
			<div>
<?php
        	//include($_SESSION["directorio_raiz"]."includes/cabecera.php");
			include("../includes/cabecera.php");            
?>
        </div>                     
          
   		<div class="posicion_menu">
<?php        
		 	//include($_SESSION["directorio_raiz"]."includes/menu.php");
			include("../includes/menu.php");
			
			
?>			            
        </div><!--<div class="posicion_menu">-->
	

		
		

<div class="contenido_seccion_oficina_presentacion">

<script>
function muestra_oculta(id){
	if (document.getElementById){ 
	var el = document.getElementById(id); 
	el.style.display = (el.style.display == 'none') ? 'block' : 'none'; 
	}
}
window.onload = function(){
	muestra_oculta('fechasExcel');
	muestra_oculta('comisiones');
	muestra_oculta('fechasPDF');
}
</script>

<!--Como el texto de oficina virtual ya lo tenemos añadido en el menu, en vez de duplicar el texto en la seccion correspondiente llamamos a la misma variable que el menu ($menu4)-->        
			<div class="titulo_seccion"><?=$menu4?></div>        
            
		<div style="margin-top:5px;width:600x;"><?php include("menu_oficina_mp.php");?></div>		                     
			<div class="texto_presentacion"> 
			  <p>&nbsp;</p>
			  <p>La contraseña introducida esta asociada a varios Puntos de Suministro.<br />
Debe seleccionar a que Punto de Suministro desea acceder </p></div>
			 
              <br>
			  
			  <p align="right" style="padding-right:10px;">
			    <a href="#" onclick="muestra_oculta('fechasExcel')" style="text-decoration:none; color:#FFF">
			    Resumen Facturas &nbsp; <img src="../img/oficina/facturas.png" border="0" align="absmiddle" width="35" /></a>
			    <br>						
			  </p>

              <table width="100%" border="0" align="right" cellpadding="4" cellspacing="0">
                <tr>
                  <td>
                    <div >
                      <div class="columnas_fila3_contrato">
                        <div class="color_fondo_cabecera_contrato times12Azul4">
                        
                        <!--<a href="oficina_presentacion_mp.php?id=<?=$_SESSION["idioma"]?>&amp;ord=0">-->
                       Cliente
                        <!--</a>-->
                        
                        
                        </div>
                      </div>
                      <!--<div class="columnas_fila3_contrato">-->
                      <div class="columnas_fila7_contrato">
                        <div class="color_fondo_cabecera_contrato times12Azul4">
                        
                         <!--<a href="oficina_presentacion_mp.php?id=<?=$_SESSION["idioma"]?>&amp;ord=2">-->
                        Titular
                        <!--</a>-->
                       </div>
                      </div>
                      <!--<div class="columnas_fila3_contrato">-->
                      <div class="columnas_fila6_contrato">
                        <div class="color_fondo_cabecera_contrato times12Azul4">
                        
                          <!--<a href="oficina_presentacion_mp.php?id=<?=$_SESSION["idioma"]?>&amp;ord=4">-->
                       Direcci&oacute;n
                        <!--</a>-->
                        </div>
                      </div>
                      <!--<div class="columnas_fila3_contrato">-->
                      <div class="columnas_fila4_contrato ">
                        <div class="color_fondo_cabecera_contrato times12Azul4">N&uacute;mero</div>
                      </div>
                      <!--<div class="columnas_fila3_contrato columna_factura_periodo">-->
                      <div class="columnas_fila3_contrato">
                        <div class="color_fondo_cabecera_contrato times12Azul4">Extensi&oacute;n</div>
                      </div>
                      <!--<div class="columnas_fila3_contrato">-->
                      <div class="columnas_fila6_contrato">
                        <div class="color_fondo_cabecera_contrato times12Azul4">
                        
                        <!--<a href="oficina_presentacion_mp.php?id=<?=$_SESSION["idioma"]?>&amp;ord=1">-->
                        Poblaci&oacute;n
                        <!--</a>-->
                        
                        </div>
                      </div>

                      <div class="columnas_fila3_contrato">
                        <div class="color_fondo_cabecera_contratob times12Azul4">Acceso</div>
                      </div>
                      <!--<div class="columnas_fila3_contrato">-->
                    </div>
                    <div align="center">
                      <!--<div class="columnas_fila1_larga_contrato">-->
                     <br />

                   
                      <?php
					  			
			$consulta_usuarios_mp = "SELECT * FROM DatosRegistrados WHERE DNI = '".$_SESSION['usuario_mp']."' ORDER BY CodigoCliente ASC";
 
           
			$resultados_consulta_usuarios_mp = mysql_query($consulta_usuarios_mp);			
			$num_usuarios_mp=mysql_num_rows($resultados_consulta_usuarios_mp);

			//Si hay facturas se mostrara un resumen de los datos de cada una de ellas			
			if($num_usuarios_mp != 0)
			{
				
				while($registro_usuarios_mp = mysql_fetch_array($resultados_consulta_usuarios_mp))
				{
?>
                    </div><br />

                    <div >
                      <div class="columnas_fila3_contrato">
                        <div class="color_fondo_datos_contrato alto_factura_periodo arial12">
                          <?=$registro_usuarios_mp["CodigoCliente"]?>
                        </div>
                      </div>
                      <!--<div class="columnas_fila3_contrato">-->
                      <div class="columnas_fila7_contrato">
                        <div class="color_fondo_datos_contrato alto_factura_periodo arial12">
                          <?=$registro_usuarios_mp["TitularNombre"]?>
                        </div>
                      </div>
                      <!--<div class="columnas_fila3_contrato">-->
                      <div class="columnas_fila6_contrato">
                        <div class="color_fondo_datos_contrato alto_factura_periodo arial12">
                          <?=$registro_usuarios_mp["SuministroCalle"]?>
                        </div>
                      </div>
                      <!--<div class="columnas_fila3_contrato">-->
                      <div class="columnas_fila4_contrato ">
                        <div class="color_fondo_datos_contrato alto_factura_periodo arial12">
                          <?=$registro_usuarios_mp["SuministroNumero"]?>
                        </div>
                      </div>
                      <!--<div class="columnas_fila3_contrato columna_factura_periodo">-->
                      <div class="columnas_fila3_contrato">
                        <div class="color_fondo_datos_contrato alto_factura_periodo arial12">
                          <?=$registro_usuarios_mp["SuministroExtension"]?>
                        </div>
                      </div>
                      <!--<div class="columnas_fila3_contrato">-->
                      <div class="columnas_fila6_contrato">
                        <div class="color_fondo_datos_contrato alto_factura_periodo arial12">
                          <?=$registro_usuarios_mp["SuministroCiudad"]?>
                        </div>
                      </div>
                      <!--<div class="columnas_fila3_contrato">-->
                      <!--<div class="columnas_fila3_contrato">
                        <div class="color_fondo_datos_contrato alto_factura_periodo arial12">
                          <?=$registro_usuarios_mp["Password"]?>
                        </div>
                      </div> -->
                      <!--<div class="columnas_fila3_contrato">-->
                      <div class="columnas_fila3_contrato">
                        <div class="color_fondo_datos_contratob alto_factura_periodo arial12"><a href="oficina_valida_login_mp.php?CodigoCliente=<?=$registro_usuarios_mp["CodigoCliente"]?>&amp;Password=<?=$registro_usuarios_mp["Password"]?>"> <img src="../img/comun/entrar.gif" border="0"  /> </a></div>
                      </div>
                    </div>
					<br />

                    <!--<div class="columnas_fila1_larga_contrato">-->
                 
                    <?php 
				}//while($row = mysql_fetch_array($result))				  
		   }//if($num_facturas != 0)
		   
//Si no hay facturas en la base de datos mostraremos un mensaje al usuario
		   else{
			?>
				<div class="error_no_registros">ERROR</div>
            
			<?php
			}
				?>
                    <br />
            
                    <div class="limpiar"></div>
                    <br />
                
                    <!--Por ultimo aparecera el pie de la web-->
                  </td>
                </tr>
              </table>
              <p>&nbsp;</p>
</div>
<?php 
//	include($_SESSION["directorio_raiz"]."includes/pie.php");
	include("../includes/pie.php");
        
}//else ($_SESSION['usuario'])
?>
<!--<div id="central_oficina"-->
</body>
</html>
