<?php
//A�adimos el archivo que contiene el inicio de la sesion y las variables de sesion.  Es importante a�adir
//este archivo antes de las cabeceras del documento o dara error
include("../includes/sesion.php");

//Una vez con la sesion iniciada, se puede dar valor a la variable de sesion que indica si nos encontramos en la oficina virtual
$_SESSION["oficina_virtual"]=1;

//A�adimos la libreria de conexion a base de datos, que a su vez contiene las referencias a todos los archivos necesarios
include("../includes/conexion.php");

//Se incluye el archivo con los textos especifico de esta seccion en el idioma oportuno
//include($_SESSION["directorio_raiz"]."idiomas/".$_SESSION["idioma"]."/oficina_virtual_login.php");
include("../idiomas/".$_SESSION["idioma"]."/oficina_virtual_login.php");

if (!isset($_SESSION['usuario'])){
?>    
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title><?=$titulo_web;?></title>
<!--CSS-->
<link rel="stylesheet" href="css/estilos.css">
<script src="external/jquery-1.8.0.min.js" type="text/javascript"></script>
<script src="external/jquery-ui-1.8.23.custom.min.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" media="all" href="scripts/calendar/skins/aqua/theme.css" title="Aqua" />
<script type="text/javascript" src="scripts/calendar/calendar.js"></script>

<!-- librer�a para cargar el lenguaje deseado -->
<script type="text/javascript" src="scripts/calendar/lang/calendar-es.js"></script>

<!-- librer�a que declara la funci�n Calendar.setup, que ayuda a generar un calendario en unas pocas l�neas de c�digo -->
<script type="text/javascript" src="scripts/calendar/calendar-setup.js"></script>


    
     <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    
 <style>
	 input{height:inherit}
	 </style>
</head>

<body>
<div id="contenido">
	<div id="cabecera">
    	<a href="graficos_index.php" id="logo"><img src="img/logo.png" style="margin: 12px;"></a>
         <div id="enlacesMenu">
          <div class="menu_oficina"><img src="img/seccion.png" alt="TituloSeccion"/></div>
        </div>  
        
    </div>
    <div id="cuerpo" style="border: 2px solid #555;background-color: white;">
    	<section class="ib-container" id="ib-container" style="text-align:center">

	
        <script src="js/highcharts.js"></script>
        <script src="js/highcharts-3d.js"></script>
        <script src="js/modules/exporting.js"></script>
        <script src="js/modules/data.js"></script>
        <script src="js/modules/drilldown.js"></script>

	<div class="contenido_ajustado"> 
 
    <div id="caja" style="text-align:center">
    				<div id="tituloLogin">
              			<img src="img/titulo.png" alt="TituloOficina"/>
                 	</div>
   <div class="contenido_seccion_oficina_index">

	      <div class="posicion_login" id="formLogin">
      
        	<form id="login" name="login" style="margin: 0 auto;width: 200px;" action="oficina_valida_login.php?id=<?=$_SESSION["idioma"]?>" method="post">
            
	            <div class="componente_login">Usuario :</div>
                
              <div class="componente_login">	                    
                <input id="usuario" name="usuario" style="margin:0;float:none;" type="text" size="26" maxlength="15">
              </div>
                
                <div class="componente_login">Contrase&ntilde;a :</div>
                 
                 <div class="componente_login">
	                 <input id="password" name="password" type="password" size="26" maxlength="15">
                </div>                
                                
                <div class="boton_login">  
                	<input type="submit" name="btn_aceptar" style="text-decoration: none;  box-shadow: 0px 2px 0px 0px #878e98;  font-weight: bold;  border: 1px solid;  padding: 3px;  -webkit-border-top-left-radius: 10px;  -webkit-border-bottom-right-radius: 10px;  border-radius: 10px 10px 10px 10px;  background-color: #616975;  background-image: -webkit-gradient(linear, left top, left bottom, from(rgb(114, 122, 134)),to(rgb(80, 88, 100)));  background-image: -webkit-linear-gradient(top, rgb(114, 122, 134), rgb(80, 88, 100));  color: white;margin-top: 20px;"	 value="Aceptar" />
                </div>
                
                <div class="olvido_contrasena_login">

					<a href="oficina_recordar_contrasena.php?id=<?=$_SESSION["idioma"]?>" class="enlace"><?=$oficina_virtual_login3?></a>
                </div>                
                                                
        </form>
		</div><!--<div class="posicion_login">-->
        
       <br /><br /><br />
        
   
    <div class="limpiar"></div>
   
    </div>
</div>
</div>
</div>

<?php
	include ('includes/footer.php');
?>
</div>
<div class="limpiar"></div>
</body>
</html>
<?php
} else {
	header ('Location: graficos_presentacion.php');	
}
?>