<?php
/*
NOTAS:

1- SE COMPRUEBAN LOS DATOS DEL LOGIN Y SE LE DA VALOR A LAS VARIABLES DE SESION EN VALIDA_LOGIN.PHP

2- EL OLVIDO DE LA CONTRASEÑA LLAMA A UNA SECCION CON UN FORMULARIO olvido_password.php

*/

//NOTA: NO SE PUEDE INCLUIR EN EL ARCHIVO DE CONEXION, PORQUE ENTONCES LE DARIAMOS EL VALOR A LA VARIABLE DE SESION Y SE PERDERIA, DEBIDO A LAS 
//DIFERENTES RUTAS RELATIVAS DEPENDIOENDO DE SI SE LLAMA DESDE LA WEB O LA OFICINA VIRTUAL


///Añadimos el archivo que contiene el inicio de la sesion y las variables de sesion.  Es importante añadir
//este archivo antes de las cabeceras del documento o dara error
include("../includes/sesion.php");

//Una vez con la sesion iniciada, se puede dar valor a la variable de sesion que indica si nos encontramos en la oficina virtual
$_SESSION["oficina_virtual"]=1;

//Añadimos la libreria de conexion a base de datos, que a su vez contiene las referencias a todos los archivos necesarios
include("../includes/conexion.php");

//Se incluye el archivo que contiene las verificaciones de datos
include("../includes/verificaciones.php");

//Se incluye el archivo con los textos especifico de esta seccion en el idioma oportuno
//include($_SESSION["directorio_raiz"]."idiomas/".$_SESSION["idioma"]."/oficina_virtual_recordar.php");

include("../idiomas/".$_SESSION["idioma"]."/oficina_virtual_recordar.php");
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<title><?=$titulo_web;?></title>

<!--Se añaden las hojas de estilos genericas que utiliza esta seccion-->
<link href="../css/estilos.css" rel="stylesheet" type="text/css" />
<link href="../css/secciones.css" rel="stylesheet" type="text/css" />
<link href="css/oficina.css" rel="stylesheet" type="text/css" />

<link href="css/oficina_login.css" rel="stylesheet" type="text/css" />

<link href="css/oficina_recordar.css" rel="stylesheet" type="text/css" />

<!--Ahora se añaden las reglas para Internet Explorer. Asi corregiremos las diferencias de visualizacion entre navegadores
Es importante añadirlo despues de las reglas principales, para que se sobreescriban las correcciones pertinentes. En este caso las diferencias
solo son para internet explorer 7-->

<!--[if IE 8]>
	<link href="../css/secciones_ie8.css" rel="stylesheet" type="text/css" />    
<![endif]-->

<!--[if IE 7]>
	<link href="../css/estilos_ie7.css" rel="stylesheet" type="text/css" />
	<link href="../css/secciones_ie7.css" rel="stylesheet" type="text/css" />    
<![endif]-->

<?php
//Cuando se pulse el boton aceptar del formulario, recogeremos el valor del boton
if (isset($_POST['btn_aceptar'])) 
{	
//Recogemos los valores de los campos de texto del formulario escritas por el usuario
	$usuario=$_POST['usuario'];
	$telefono=$_POST['telefono'];
	$email=$_POST['email'];
	
//Con esta variable se comprobara si hay algun tipo de error en el formulario	
	$error=0;	

//Con esta variable se comprobara si hay algun tipo de error en el formulario	
	$error=0;	
	
//Primero se comprueba si se ha escrito el numero de usuario y o bien el telefono o bien el e-mail
	if(($usuario=="") or ($telefono=="" and $email==""))
	{
		$error=1;	
	}//if(($usuario=="") or ($telefono=="" and $email==""))				

//Si no se ha producdo un error previamente	
	if($error==0)
	{	
//Consultamos en la tabla DatosRegistrados si existe el usuario
			$rs_usuario = seleccion_condicional_campos("DatosRegistrados","Usuario","Usuario='".$usuario."'","Usuario",0,$ruta_sevidor_bd,$usuario_bd,$contrasena_bd,$nombre_bd);				

			$registro_usuario=mysql_fetch_array($rs_usuario);
		
//Si no obtenemos resultados en la consulta se dara un mensaje de error
			if(!$registro_usuario)
			{
				$error=2;
			}//if(!$registro_usuario)
	}//if($error==0)
			
//Ahora comprobamos el telefono, en el caso en el que se haya escrito
	if($telefono!="" and solo_numero($telefono))
	{				
		$error=3;
	}//if($telefono!="" and solo_numero($telefono))			
			
//En el caso en que no se haya producido un error en comprobaciones anteriores y se haya escrito algo en el campo del email			
	if($error==0 and $email!="")
	{
		if(!comprobar_email($email))
		{
			$error=4;
		}//if(comprobar_email($email)==1)
	}//if($error==0 and $email!="")			
}//if (isset($_POST['btn_aceptar'])
?>

</head>
	
<body>
<!--*************************************************Web realizada por ****************************************-->
<div id="central_login_oficina">

  <!--Añadimos los fragmentos comunes, la cabecera, el menu, y el submenu-->
  <div>
  	<?php
		//include($_SESSION["directorio_raiz"]."includes/cabecera.php");
		include("../includes/cabecera.php");		
	?>
  </div>
  
  <div class="posicion_menu">
    <?php
//    	include($_SESSION["directorio_raiz"]."includes/menu.php");
		include("../includes/menu.php");		
	?>
  </div>
  <div class="contenido_seccion_oficina_recordar">
  
  <div class="texto_recordar"><?=$oficina_virtual_recordar1?></div>
  
    <div class="posicion_recordar">
      <form id="form_recordar" name="form_recordar" action="oficina_recordar_contrasena.php?id=<?=$_SESSION["idioma"]?>" method="post">
        <div class="componente_recordar"><?=$oficina_virtual_recordar2?>*
        </div>
        <div class="componente_recordar">
          <input id="usuario" name="usuario" type="text" size="50" maxlength="15" value="<?=$usuario?>" />
        </div>
        
        <div class="componente_recordar"><?=$oficina_virtual_recordar3?></div>
        
        <div class="componente_recordar">
          <input id="telefono" name="telefono" type="text" size="50" maxlength="15" value="<?=$telefono?>" />
        </div>
        
        <div class="componente_recordar"><?=$oficina_virtual_recordar4?></div>
        
        <div class="componente_recordar">
          <input id="email" name="email" type="text" size="50" maxlength="60" value="<?=$email?>" />
        </div>
        <div class="boton_recordar">
          <input type="submit" name="btn_aceptar" value="<?=$oficina_virtual_recordar5?>" />
        </div>
        <div class="enlace_volver_recordar"><a href="oficina_index.php?id=<?=$_SESSION["idioma"]?>" class="enlace" style="color:#fff;"><?=$oficina_virtual_recordar6?></a> </div>
      </form>
    </div>
    <!--<div class="posicion_login">-->
  </div>
  <!--<div class="contenido_seccion">-->
  <!--Por ultimo aparecera el pie de la web-->
<?php 
//	include($_SESSION["directorio_raiz"]."includes/pie.php");
	include("../includes/pie.php");
	
//Cuando se pulse el boton aceptar del formulario se ejecutaran las consecuencias de las verificaciones
if (isset($_POST['btn_aceptar'])) 
{	
//Mostraremos el mensaje correspondiente dependiendo de si el proceso ha ido bien o se ha encontrado algun problema		
	switch($error)
	{
//Si todo ha ido bien se informara al usuario		
		case 0:								
//Establecemos los parametros de envio del correo electronico, en este caso el destinatario no se define, ya que lo leeremos de la base de datos
			$asunto=$consulta2." Web ".$nombre_empresa." - Recordar clave de acceso";
				
//Preparamos la cabecera para el envío en formato HTML
			$headers = "MIME-Version: 1.0\r\n";
			$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
				
//Establecemos la direccion del remitente
			$headers .= "From: Web ".$nombre_empresa." <".$mail_empresa.">\r\n"; 				
				
//Esta variable almacenara el contenido del correo electrónico que reciben en la empresa cliente avisandoles de la solicitud de la contraseña
			$cuerpo="Se ha solicitado un recordatorio de contraseña desde la Web:<br /><br />";
			$cuerpo.="<strong>Nº Usuario:</strong> ".$usuario."<br /><br />";

//Se añadirán el mail, el telefono o ambos dependiendo de lo que haya rellenado el usuario en el formulario				
			if($telefono!="")
			{				
				$cuerpo.="<strong>Teléfono:</strong> ".$telefono."<br /><br />";
			}//if($telefono!="")
				
			if($email!="")
			{
				$cuerpo.="<strong>E-mail:</strong> ".$email."<br /><br />";	
			}//if($email!="")
						
//Consultamos en la tabla DireccionesEmail las direcciones a las que la empresa cliente quiere enviar los correos
			$rs_destinatarios = seleccion("DireccionesEmail","IdEmail",0,$ruta_sevidor_bd,$usuario_bd,$contrasena_bd,$nombre_bd);				

//Se envian mails a todas las direcciones que haya dadas de alta en la tabla. Aunque la direccion no exista no se produce un error, es decir que el
//usuario no será consciente del error
			while($destinatario=mysql_fetch_array($rs_destinatarios))
			{
				mail($destinatario["Email"],$asunto,$cuerpo,$headers);										
			}//while($destinatario=mysql_fetch_array($rs_destinatarios))
				
//Se informa al usuario del exito del envio de los datos				
			MsgBox($recordar_ok);
			redirigir("oficina_index.php?id=".$_SESSION["idioma"]);																				
		break;
		
//Error por usuario inexistente			
		case 1:
			MsgBox($error_recordar_vacio);		
		break;
			
//Error por usuario inexistente			
		case 2:
			MsgBox($error_no_usuario);					
		break;

//Error por telefono			
		case 3:
			MsgBox($error_telefono);									
		break;						

//Error por direccion mail
		case 4:
			MsgBox($error_mail);																
		break;										
	}//switch($error)
}//if (isset($_POST['btn_aceptar'])) 			
?>

</div>
<!--<div id="central_login_oficina"-->
</body>
</html>
