<?php
/* NOTA:

 COMO NO SE CIERRA LA CONEXION TRAS CADA CONEXION CON LA BASE DE DATOS, LAS CONSULTAS DEJAN DE FUNCIONAR SI SE ESTABLECE COMO UNICA CONEXION
 LA QUE UTILIZA EL RESTO DE LA WEB, POR LO TANTO HASTA QUE SE HAGA LA MIGRACION COMPLETA DE LOS TEXTOS, COEXISTIRAN LAS DOS CONEXIONES, YA QUE
 SE HA AÑADIDO UNA LLAMADA A LA CONEXION BUENA EN ESTE ARCHIVO.
*/

include("_conexion.php");

//Se incluye el archivo con los textos especifico de esta seccion en el idioma oportuno
//include($_SESSION["directorio_raiz"]."idiomas/".$_SESSION["idioma"]."/oficina_presentacion.php");
include("../idiomas/".$_SESSION["idioma"]."/oficina_presentacion.php");

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<title><?=$titulo_web;?></title>

<!--Se añaden las hojas de estilos genericas que utiliza esta seccion-->
<link href="../css/estilos.css" rel="stylesheet" type="text/css" />
<link href="../css/secciones.css" rel="stylesheet" type="text/css" />
<link href="css/oficina.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_presentacion.css" rel="stylesheet" type="text/css" />

<!--Ahora se añaden las reglas para Internet Explorer. Asi corregiremos las diferencias de visualizacion entre navegadores
Es importante añadirlo despues de las reglas principales, para que se sobreescriban las correcciones pertinentes. En este caso las diferencias
solo son para internet explorer 7-->

<!--[if IE 7]>
	<link href="../css/estilos_ie7.css" rel="stylesheet" type="text/css" />
	<link href="../css/secciones_ie7.css" rel="stylesheet" type="text/css" />    
<![endif]-->

</head>
	
<body>
<!--*************************************************Web realizada por ****************************************-->
	<div id="central_oficina">
    
<?php
//Se comprueba que el usuario se haya identificado, si no es asi se da un aviso y redirigimos a la seccion de identificacion
if ($_SESSION['usuario']=="")
{	
	MsgBox($error_identificacion);
	redirigir("oficina_index.php?id=".$_SESSION["idioma"]);
	
}//if ($_SESSION['usuario'])

//Si el usuario se ha identificado la seccion se ejecutara con normalidad
else
{
//Añadimos los fragmentos comunes, la cabecera, el menu, y el submenu
		include("oficina_fragmentos_comunes.php");
?>    
                
		<div class="contenido_seccion_oficina_presentacion">

<!--Como el texto de oficina virtual ya lo tenemos añadido en el menu, en vez de duplicar el texto en la seccion correspondiente llamamos a la misma variable que el menu ($menu4)-->        
			<!--<div class="posicion_titulo_seccion titulo_seccion"><?=$menu4?></div>        -->
            
			<!--<div><img src="../img/oficina/oficina_presentacion.gif" alt=""/></div>-->
			<div><img src="../img/oficina/oficina_presentacion.jpg" alt="" width="720px"/></div>
                                            
			<div class="texto_presentacion"><?=$oficina_presentacion1?> <span class="texto_resaltado"><?=$nombre_empresa_completo?></span></div>            
		</div>
		<div align="center"><!--<div class="contenido_seccion_oficina_presentacion">-->                        
		  
		  
		  <br /><br />
		  
		  <!--
		  <table class="contenido_seccion_oficina_presentacionb"><tr><td>
		    <fieldset>
		      <div align="center"><br />
		        Le recordamos que, para <strong>optimizar</strong> el visionado de la Oficina Virtual,<br />
se recomienda utilizar los exploradores:<br />
		        <a href="http://windows.microsoft.com/es-ES/internet-explorer/products/ie/home" target="_blank" class="enlace_nota_pdf"><img src="../img/oficina/ie.png" width="24" height="23" border="0" align="absmiddle" /> Internet Explorer 10 o superior</a>, o <a href="http://www.mozilla.org/es-ES/firefox/new/" target="_blank" class="enlace"><img src="../img/oficina/mf.png" width="22" height="21" border="0" align="absmiddle" /> Mozilla Firefox 30 o superior.</a>
		        <br />
	          </div>
	        </fieldset>
		    </td></tr> </table>
-->
	  </div>
		<div class="limpiar"> </div>
               
        
        
      <div class="limpiar"></div>
                        	
<!--Por ultimo aparecera el pie de la web-->
<?php 
//	include($_SESSION["directorio_raiz"]."includes/pie.php");
	include("../includes/pie.php");
        
}//else ($_SESSION['usuario'])
?>        
    </div><!--<div id="central_oficina"-->
</body>
</html>
