$('#GraficoLine').highcharts({
            chart: {
                type: 'area',
                zoomType: 'x',
                height: 500,
                marginBottom : 100
            },
            
            credits: {
              enabled: false
          },
             title: {
            text: '',
            x: 20,
            style: {
                     fontSize: '14px',
                   }
        },
            subtitle: {
                text: ' '
            },
             xAxis: {
            categories: [<?=$mesFactura;?>],
            tickInterval: 1,
            labels: {
 
                    style: {
                        fontSize: '11px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
        },
            yAxis: {
                title: {
                    text: 'Importes en Euros'
                },
                labels: {
                    formatter: function() {
                        return this.value;
                    }
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:14px"><b>{point.x}</b></span><br>',
                pointFormat: '{series.name}: <b>{point.y}</b><br/>',
                valueSuffix: ' Euros',
                shared: true
            },
            plotOptions: {
                area: {
                    pointStart: 0,
                    marker: {
                        enabled: false,
                        symbol: 'circle',
                        radius: 2,
                        states: {
                            hover: {
                                enabled: true
                            }
                        }
                    }
                }
            },
            series: [{
                name: 'Importe',
                data: [<?=$importeFactura;?>],
                color: '#006699'
            }]
        });