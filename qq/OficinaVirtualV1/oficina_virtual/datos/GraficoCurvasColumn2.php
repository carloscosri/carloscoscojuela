$('#GraficoColumn').highcharts({
        chart: {
            type: 'column',
            marginBottom : 140,
            marginTop: 15,
            zoomType: 'x'
            
        },
        credits: {
          enabled: false
      },
        title: {
            text: ' '
        },
        subtitle: {
            text: ' '
        },
        plotOptions: {
            column: {
                depth: 15
            },
            threshold: null
        },
       xAxis: {
            categories: [<?=$mesFactura;?>], 
            <?php if ($multi==1) {
            echo $tick;
             } else { ?>
            tickInterval: 1,
            <?php } ?>
            labels: {
                    rotation: -45,
                    style: {
                        fontSize: '11px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
        },
        yAxis: {
        	title: {
                    text: 'Consumo en KWH'
                },
                labels: {
                    formatter: function() {
                        return this.value;
                    }
                }
            
        },
        tooltip: {
        	headerFormat: '<span style="font-size:14px"><b>{point.x}</b></span><br>',
            pointFormat: '{series.name}: <b>{point.y}</b><br/>',
            valueSuffix: ' KW',
            shared: true
        },
       series: [{
                name: 'Activa',
                data: [<?=$ConsumosActiva;?>],
                color: '#006699'
            }, {
                name: 'Reactiva',
                data: [<?=$ConsumosReActiva;?>],
            	color: '#2BD551'
            }]
});