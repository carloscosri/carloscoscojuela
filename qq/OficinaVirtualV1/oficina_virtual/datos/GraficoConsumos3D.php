
            
$(function () {
    $('#Grafico3D').highcharts({
        chart: {
            zoomType: 'x',
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        xAxis: [{
             categories: [<?=$mesFactura;?>],
             
        }],
        yAxis: [{ // Primary yAxis
        	
             labels: {
                format: '{value} KWh',
                style: {
                    color: ' #006699'
                }
            },
            title: {
                text: 'Consumo en KWh',
                style: {
                    color: ' #006699'
                }
            }
        }, { // Secondary yAxis
       
            title: {
                text: 'KW',
                style: {
                    color: '#FF0000'
                }
            },
            min: 0,
            labels: {
                 format: '{value:.2f} Kw',
                style: {
                    color: '#FF0000'
                }
            },
            opposite: true
        }],
        tooltip: {
            shared: true
        },
        legend: {
            layout: 'vertical',
            align: 'left',
            x: 120,
            verticalAlign: 'top',
            y: 100,
            floating: true,
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
        },
        series: [{
            name: 'Activa',
            type: 'column',
             data: [<?=$ConsumosActiva;?>],
             color: ' #006699',
            tooltip: {
                valueSuffix: ' KWh'
            }

        },{
            name: 'Reactiva',
            type: 'column',
             data: [<?=$ConsumosReActiva;?>],
              color: '#30C36A',
            tooltip: {
                valueSuffix: ' KWh'
            }

        }, {
         name: 'Maximetro',
            type: 'column',
            yAxis: 1,
            color: '#FF0000',
            data: [<?=$ConsumosMax;?>],
            tooltip: {
                valueSuffix: ' KW'
           
            }
        }]
    });
});
