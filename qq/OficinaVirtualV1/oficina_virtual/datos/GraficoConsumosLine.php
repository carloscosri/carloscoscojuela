$(function () {
    $('#GraficoLine').highcharts({
        chart: {
            zoomType: 'x',
            height: 500,
            marginBottom: 100
        },
        credits: {
      enabled: false
  },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        xAxis: [{
             categories: [<?=$mesFactura;?>],
             
        }],
        yAxis: [{ // Primary yAxis
        	
             labels: {
                format: '{value} KWh',
                style: {
                    color: ' #006699'
                }
            },
            title: {
                text: 'Consumo en KWh',
                style: {
                    color: ' #006699'
                }
            }
        }, { // Secondary yAxis
       
            title: {
                text: 'KW',
                style: {
                    color: '#FF0000'
                }
            },
            min: 0,
            labels: {
                format: '{value:.2f} Kw',
                style: {
                    color: '#FF0000'
                }
            },
            opposite: true
        }],
        tooltip: {
            shared: true
        },
        legend: {
            layout: 'horizontal',
            align: 'left',
            x: 200,
            verticalAlign: 'bottom',
            y: 0,
            width: 400,
            floating: true,
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
        },
        series: [{
            name: 'Activa',
            type: 'area',
             data: [<?=$ConsumosActiva;?>],
             color: ' #006699',
            tooltip: {
                valueSuffix: ' KWh'
            }

        },{
            name: 'Reactiva',
            type: 'area',
             data: [<?=$ConsumosReActiva;?>],
              color: '#30C36A',
            tooltip: {
                valueSuffix: ' KWh'
            }

        }, {
         name: 'Maximetro',
            type: 'spline',
            pointFormat: '{point.percentage:.2f}',
            yAxis: 1,
            color: '#FF0000',
            data: [<?=$ConsumosMax;?>],
            tooltip: {
                valueSuffix: ' KW'
           
            }
        }]
    });
});


<?php /*?>$('#GraficoColumn').highcharts({
        chart: {
            type: 'column',
            margin: 75,
            options3d: {
				enabled: true,
                alpha: 10,
                beta: 15,
                depth: 50
            },
            zoomType: 'x'
        },
                    credits: {
              enabled: false
          },

        title: {
            text: 'Visualizando datos <strong><?=$tituloGrafico;?></strong>',
            x: 20,
            style: {
                     fontSize: '14px',
                   }
        },
        subtitle: {
            text: ' '
        },
        plotOptions: {
        	spline: {
                lineWidth: 4,
                },
            column: {
                depth: 15
            },
            threshold: null
        },
        xAxis: {
            categories: [<?=$mesFactura;?>],
            tickInterval: 1,
            labels: {
                    rotation: -45,
                    style: {
                        fontSize: '11px',
                        fontFamily: 'Verdana, sans-serif',
                        marginTop: '20px'
                    }
                }
        },
        yAxis: {
        	title: {
                    text: 'Consumo en KWh'
                },
                labels: {
                    formatter: function() {
                        return this.value;
                    }
                }
            
        },
        tooltip: {
        	
            valueSuffix: ' Kw',
        },
        series: [{
                name: 'Activa',
                data: [<?=$ConsumosActiva;?>],
                color: '#006699'
            }, {
                name: 'Reactiva',
                data: [<?=$ConsumosReActiva;?>],
            	color: '#2BD551'
            },{
                name: 'Maximetro',
                data: [<?=$ConsumosMax;?>],
            	color: '#FF0000'
            }]
});<?php */?>