$('#GraficoLine').highcharts({
            chart: {
                type: 'area'
            },
            title: {
                text: 'US and USSR nuclear stockpiles'
            },
            subtitle: {
                text: ' '
            },
             xAxis: {
            categories: Highcharts.getOptions().lang.shortMonths
        },
            yAxis: {
                title: {
                    text: 'Nuclear weapon states'
                },
                labels: {
                    formatter: function() {
                        return this.value;
                    }
                }
            },
            tooltip: {
                pointFormat: '{series.name} produced <b>{point.y:,.0f}</b><br/>warheads in {point.x}'
            },
            plotOptions: {
                area: {
                    pointStart: 0,
                    marker: {
                        enabled: false,
                        symbol: 'circle',
                        radius: 2,
                        states: {
                            hover: {
                                enabled: true
                            }
                        }
                    }
                }
            },
            series: [{
                name: 'Activa',
                data: [<?=$ConsumosActiva;?>]
            }, {
                name: 'Reactiva',
                data: [<?=$ConsumosReActiva;?>]
            },{
                name: 'Maximetro',
                data: [<?=$ConsumosMax;?>]
            }]
        });