$(function () {

global: {
		Date: <?=$mesFactura;?>
	}
        // Create the chart
        $('#GraficoTime').highcharts('StockChart', {


            rangeSelector : {
                selected : 1
            },

            title : {
                text : ''
            },

            series : [{
                name : 'Importe',
               data: [<?=$importeFactura;?>],
                tooltip: {
                    valueDecimals: 2
                }
            }]
        });
   
});
<?php /*?>
$(function () {
   $('#GraficoTime').highcharts({
        chart: {
            zoomType: 'x'
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            type: 'datetime',
            minRange: 14 * 24 * 3600000 // fourteen days
        },
        yAxis: {
            title: {
                text: 'Exchange rate'
            }
        },
        legend: {
            enabled: false
        },
        plotOptions: {
            area: {
                fillColor: {
                    linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1},
                    stops: [
                        [0, Highcharts.getOptions().colors[0]],
                        [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                    ]
                },
                marker: {
                    radius: 2
                },
                lineWidth: 1,
                states: {
                    hover: {
                        lineWidth: 1
                    }
                },
                threshold: null
            }
        },

        series: [{
            type: 'area',
            name: 'Importe',
            pointInterval: 24 * 3600 * 1000,
            pointStart: Date.UTC(2006, 0, 1),
           data: [<?=$importeFactura;?>],
        }]
    });
}); <?php /*?>
$('#GraficoTime').highcharts({
        chart: {
            type: 'column',
            margin: 75,
            options3d: {
				enabled: true,
                alpha: 10,
                beta: 15,
                depth: 50
            },
            zoomType: 'x'
        },
        credits: {
          enabled: false
      },
         title: {
            text: 'Visualizando datos <strong><?=$tituloGrafico;?></strong>',
            x: 20,
            style: {
                     fontSize: '14px',
                   }
        },
        subtitle: {
            text: ' '
        },
        plotOptions: {
            column: {
                depth: 15
            },
            threshold: null
        },
        xAxis: {
            categories: [<?=$mesFactura;?>],
            tickInterval: 1,
            labels: {
                    rotation: -45,
                    style: {
                        fontSize: '11px',
                        fontFamily: 'Verdana, sans-serif',
                        marginTop: '20px'
                    }
                }
        },
        yAxis: {
        	title: {
                    text: 'Importes en Euros'
                },
                labels: {
                    formatter: function() {
                        return this.value;
                    }
                }
            
        },
        tooltip: {
        	headerFormat: '<span style="font-size:14px"><b>{point.x}</b></span><br>',
            pointFormat: '{series.name}: <b>{point.y}</b><br/>',
            valueSuffix: ' Euros',
            shared: true
        },
        series: [{
            name: 'Importe',
            data: [<?=$importeFactura;?>],
            color: '#006699',
                    
        }
        
    ]

});<?php */?>