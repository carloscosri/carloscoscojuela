$('#GraficoLine').highcharts({
            chart: {
            	type: 'spline',
                zoomType: 'x',
                height: 500,
                marginBottom : 100
            },
          credits: {
              enabled: false
          },

            title: {
               text: 'Curvas de carga',
             style: {
                color: '#fff',
                fontSize: '1px'
            }
            },
            subtitle: {
                text: ' '
            },
            xAxis: [{
            categories: [<?=$mesFactura;?>],
            <?php if ($multi==1) {
            echo $tick;
             } else { ?>
            tickInterval: 1,
            <?php } ?>
            labels: {
                    
                    style: {
                        fontSize: '11px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
        }, {  categories: [<?=$diaFactura;?>],
            <?php if ($multi==1) {?>
            tickInterval: 12,
            <?php } else { ?>
            tickInterval: 12,
            <?php } ?>
            linkedTo: 0,
            opposite: true,
            labels: {
                    
                    style: {
                        fontSize: '11px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
        }],
            yAxis: {
                title: {
                    text: 'Consumo en KWh'
                },
                labels: {
                    formatter: function() {
                        return this.value;
                    }
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:14px"><b>{point.x}</b></span><br>',
                pointFormat: '{series.name}: <b>{point.y}</b><br/>',
                valueSuffix: ' Kw',
                shared: true
            },
            plotOptions: {
            	spline: {
                	lineWidth: 2,
                    states: {
                        hover: {
                            lineWidth: 3
                        }
                    },
                marker: {
                    enabled: false
                },
                }
            },
            series: [{
                name: 'Activa',
                data: [<?=$ConsumosActiva;?>],
                color: '#006699'
            }, {
                name: 'Reactiva',
                data: [<?=$ConsumosReActiva;?>],
            	color: '#2BD551'
            }]
        });
        var chart = $('#GraficoLine').highcharts(),
        name = false,
        enableDataLabels = true,
        enableMarkers = true,
        color = false;

// Toggle point markers
    $('#data-labels').click(function () {
        chart.series[0].update({
            dataLabels: {
                enabled: enableDataLabels
            },
              marker: {
                enabled: enableMarkers
                }
             }),
            chart.series[1].update({
            dataLabels: {
                enabled: enableDataLabels
            },
             marker: {
                enabled: enableMarkers
                }
        });
        enableDataLabels = !enableDataLabels;
        enableMarkers = !enableMarkers;
    });

    // Toggle point markers
    $('#color').click(function () {
        chart.series[0].update({
            color: color ? null : Highcharts.getOptions().colors[3]
        }),
        chart.series[1].update({
            color: color ? null : Highcharts.getOptions().colors[2]
        });
        color = !color;
    });
