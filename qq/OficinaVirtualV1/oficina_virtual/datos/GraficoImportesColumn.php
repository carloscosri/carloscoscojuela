$('#GraficoColumn').highcharts({
        chart: {
            type: 'column',
            marginBottom : 100,
            height: 500,
            zoomType: 'x'
        },
        credits: {
          enabled: false
      },
         title: {
            text: '',
            x: 20,
           
        },
        subtitle: {
            text: ' '
        },
        plotOptions: {
            column: {
                depth: 15
            },
            threshold: null
        },
        xAxis: {
            categories: [<?=$mesFactura;?>],
            tickInterval: 1,
            labels: {
                    
                    style: {
                        fontSize: '11px',
                        fontFamily: 'Verdana, sans-serif',
                        marginTop: '20px'
                    }
                }
        },
        yAxis: {
        	title: {
                    text: 'Importes en Euros',
                  
                },
                labels: {
                  	
                    formatter: function() {
                        return this.value;
                    }
                }
            
        },
        tooltip: {
        	headerFormat: '<span style="font-size:14px"><b>{point.x}</b></span><br>',
            pointFormat: '{series.name}: <b>{point.y}</b><br/>',
            valueSuffix: ' Euros',
            shared: true
        },
        series: [{
            name: 'Importe',
            data: [<?=$importeFactura;?>],
            color: '#006699',
                    
        }
        
    ]
});