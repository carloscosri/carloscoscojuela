$('#GraficoLine').highcharts({
            chart: {
            	type: 'spline',
                zoomType: 'x'
            },
          credits: {
              enabled: false
          },

            title: {
                text: 'Grafico de CURVAS <?=$tituloGrafico;?>'
            },
            subtitle: {
                text: ' '
            },
             xAxis: {
            categories: [<?=$mesFactura;?>],
            <?php if ($multi==1) { ?>
            tickInterval: 10,
            <?php } else { ?>
            tickInterval: 1,
            <?php } ?>
            labels: {
                    rotation: -45,
                    style: {
                        fontSize: '11px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
        },
            yAxis: {
                title: {
                    text: 'Consumo en KWh'
                },
                labels: {
                    formatter: function() {
                        return this.value;
                    }
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:14px"><b>{point.x}</b></span><br>',
                pointFormat: '{series.name}: <b>{point.y}</b><br/>',
                valueSuffix: ' Kw',
                shared: true
            },
            plotOptions: {
            	spline: {
                	lineWidth: 2,
                    states: {
                        hover: {
                            lineWidth: 3
                        }
                    },
                marker: {
                    enabled: false
                },
                }
            },
            series: [{
                name: 'Activa',
                data: [<?=$ConsumosActiva;?>],
                color: '#006699'
            }, {
                name: 'Reactiva',
                data: [<?=$ConsumosReActiva;?>],
            	color: '#2BD551'
            }]
        });