$(function () {
    $('#GraficoColumn').highcharts({
        chart: {
            zoomType: 'x',
            height: 500,
            marginBottom: 100
        },
        credits: {
      enabled: false
  },
  legend: {
            itemStyle: {
                color: '#000000',
                fontWeight: 'bold'
            }
            },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        xAxis: [{
             categories: [<?=$mesFactura;?>],
             
        }],
        yAxis: [{ // Primary yAxis
        	
             labels: {
                format: '{value} EUR',
                style: {
                    color: Highcharts.getOptions().colors[0]
                }
            },
            title: {
                text: 'Euros',
                style: {
                    color: Highcharts.getOptions().colors[0]
                }
            }
        }, { // Secondary yAxis
       
            title: {
                text: 'KW',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            min: 0,
            labels: {
                format: '{value} Kw',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            opposite: true
        }],
        tooltip: {
            shared: true
        },
        legend: {
            layout: 'horizontal',
            align: 'left',
            x: 250,
            verticalAlign: 'bottom',
            y: 0,
            floating: true,
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
        },
        series: [{
            name: 'Importe',
            type: 'column',
             data: [<?=$importeFactura;?>],
            tooltip: {
                valueSuffix: ' EUR'
            }

        }, {
         name: 'Consumo',
            type: 'spline',
            yAxis: 1,
            data: [<?=$ConsumosActiva;?>],
            tooltip: {
                valueSuffix: ' kw'
           
            }
        }]
    });
});


<?php /*?>$('#GraficoColumn').highcharts({
        chart: {
            type: 'column',
            margin: 75,
            options3d: {
				enabled: true,
                alpha: 10,
                beta: 15,
                depth: 50
            },
            zoomType: 'x'
        },
        credits: {
          enabled: false
      },
         title: {
            text: 'Visualizando datos <strong><?=$tituloGrafico;?></strong>',
            x: 20,
            style: {
                     fontSize: '14px',
                   }
        },
        subtitle: {
            text: ' '
        },
        plotOptions: {
            column: {
                depth: 15
            },
            threshold: null
        },
        xAxis: {
            categories: [<?=$mesFactura;?>],
            tickInterval: 1,
            labels: {
                    rotation: -45,
                    style: {
                        fontSize: '11px',
                        fontFamily: 'Verdana, sans-serif',
                        marginTop: '20px'
                    }
                }
        },
        yAxis: {
        	title: {
                    text: 'Importes en Euros'
                },
                labels: {
                    formatter: function() {
                        return this.value;
                    }
                }
            
        },
        tooltip: {
        	headerFormat: '<span style="font-size:14px"><b>{point.x}</b></span><br>',
            pointFormat: '{series.name}: <b>{point.y}</b><br/>',
            valueSuffix: ' Euros',
            shared: true
        },
        series: [{
            name: 'Importe',
            data: [<?=$importeFactura;?>],
           
                    
        }
        
    ]
});



$(function () {
    $('#container').highcharts({
        chart: {
            zoomType: 'xy'
        },
        title: {
            text: 'Average Monthly Temperature and Rainfall in Tokyo'
        },
        subtitle: {
            text: 'Source: WorldClimate.com'
        },
         xAxis: {
            categories: [<?=$mesFactura;?>],
            tickInterval: 1,
            labels: {
                    rotation: -45,
                    style: {
                        fontSize: '11px',
                        fontFamily: 'Verdana, sans-serif',
                        marginTop: '20px'
                    }
                }
        },
        yAxis: {
        	title: {
                    text: 'Euros'
                },
                labels: {
                    formatter: function() {
                        return this.value;
                    }
                }
            
        },  yAxis: {
        	title: {
                    text: 'Consumo en KWh'
                },
                labels: {
                    formatter: function() {
                        return this.value;
                    }
                }
            
        },
            opposite: true
        }],
        tooltip: {
            shared: true
        },
        legend: {
            layout: 'vertical',
            align: 'left',
            x: 120,
            verticalAlign: 'top',
            y: 100,
            floating: true,
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
        },
        series: [{
            name: 'Importe',
            type: 'column',
            yAxis: 1,
            data: [<?=$importeFactura;?>],
            color: '#006699',
            tooltip: {
                valueSuffix: ' mm'
            }

        }, {
            name: 'Temperature',
            type: 'spline',
            data: [7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6],
            tooltip: {
                valueSuffix: '°C'
            }
        }]
    });
});<?php */?>