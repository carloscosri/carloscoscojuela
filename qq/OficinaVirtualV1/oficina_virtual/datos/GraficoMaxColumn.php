$('#GraficoColumn').highcharts({
        chart: {
            type: 'column',
            margin: 75,
            options3d: {
				enabled: true,
                alpha: 10,
                beta: 15,
                depth: 50
            },
            zoomType: 'x'
        },
                    credits: {
              enabled: false
          },

        title: {
            text: '',
            x: 20,
            style: {
                     fontSize: '14px',
                   }
        },
        subtitle: {
            text: ' '
        },
        plotOptions: {
        	spline: {
                lineWidth: 4,
                },
            column: {
                depth: 15
            },
            threshold: null
        },
        xAxis: {
            categories: [<?=$mesFactura;?>],
            tickInterval: 1,
            labels: {
                    rotation: -45,
                    style: {
                        fontSize: '11px',
                        fontFamily: 'Verdana, sans-serif',
                        marginTop: '20px'
                    }
                }
        },
        yAxis: {
        	title: {
                    text: 'Consumo en KWh'
                },
                labels: {
                    formatter: function() {
                        return this.value;
                    }
                }
            
        },
        tooltip: {
        	headerFormat: '<span style="font-size:14px"><b>{point.x}</b></span><br>',
            pointFormat: '{series.name}: <b>{point.y}</b><br/>',
            valueSuffix: ' Kw',
            shared: true
        },
        series: [{
           
                name: 'Maximetro',
                data: [<?=$ConsumosMax;?>],
            	
            }]
});