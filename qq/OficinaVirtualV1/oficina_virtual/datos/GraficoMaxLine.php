$('#GraficoMaxLine').highcharts({
            chart: {
                zoomType: 'x',
                type: 'spline'
            },
                        credits: {
              enabled: false
          },

            title: {
                text: 'Grafico de CURVAS <?=$tituloGrafico;?>'
            },
            subtitle: {
                text: ' '
            },
             xAxis: {
            categories: [<?=$mesFactura;?>], 
            <?php if ($multi==1) { ?>
            tickInterval: 5,
            <?php } else { ?>
            tickInterval: 1,
            <?php } ?>
            labels: {
                    rotation: -45,
                    style: {
                        fontSize: '11px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
        },
            yAxis: {
                title: {
                    text: 'Consumo en KWh'
                },
                labels: {
                    formatter: function() {
                        return this.value;
                    }
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:14px"><b>{point.x}</b></span><br>',
                pointFormat: '{series.name}: <b>{point.y}</b><br/>',
                valueSuffix: ' Kw',
                shared: true
            },
            plotOptions: {
            	spline: {
                	lineWidth: 2,
                    states: {
                        hover: {
                            lineWidth: 3
                        }
                    },
                marker: {
                    enabled: false
                },
                }
            },
            series: [{
                name: 'Maximetro',
                data: [<?=$ConsumosActiva;?>],
                color: '#006699'
            }]
        });