$('#GraficoColumn').highcharts({
        chart: {
            type: 'column',
            
            margin: 75,
            options3d: {
				enabled: true,
                alpha: 0,
                beta: 15,
                depth: 50
            },
            
        },
        credits: {
          enabled: false
      },
        title: {
            text: 'Visualizando datos <strong><?=$tituloGrafico;?></strong>',
            x: 20,
            style: {
                     fontSize: '14px',
                   }
        },
        subtitle: {
            text: ' '
        },
        plotOptions: {
            column: {
                depth: 15
            },
            threshold: null
        },
      xAxis: {
            categories: [<?=$mesFactura2;?>],
            <?php if ($multi==1) { 
             echo $tick;
            } else { ?>
            tickInterval: 1,
            <?php } ?>
            labels: {
                    rotation: -45,
                    style: {
                        fontSize: '11px',
                        fontFamily: 'Verdana, sans-serif',
                        marginTop: '20px'
                    }
                }
        },
        yAxis: {
        	title: {
                    text: 'Consumo en KWH'
                },
                labels: {
                    formatter: function() {
                        return this.value;
                    }
                }
            
        },
        tooltip: {
        	headerFormat: '<span style="font-size:14px"><b>{point.x}</b></span><br>',
            pointFormat: '{series.name}: <b>{point.y}</b><br/>',
            valueSuffix: ' KW',
            shared: true
        },
       series: [{
                name: 'Activa',
                data: [<?=$ConsumosActiva;?>],
                color: '#006699'
            }, {
                name: 'Reactiva',
                data: [<?=$ConsumosReActiva;?>],
            	color: '#2BD551'
            },
            {
                name: 'Perfiladas',
                data: [<?=$ConsumosActivaPerf;?>],
                color: '#FF8000'
            }]
});