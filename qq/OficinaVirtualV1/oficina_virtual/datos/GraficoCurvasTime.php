$('#GraficoTime').highcharts({
        chart: {
            type: 'area',
            zoomType: 'x',
            height: 500,
            marginBottom : 100            
        },
        credits: {
          enabled: false
      },
        title: {
         text: 'Curvas de carga',
             style: {
                color: '#fff',
                fontSize: '1px'
            }
        },
        subtitle: {
            text: ' '
        },
        plotOptions: {
            column: {
                depth: 15
            },
            threshold: null
        },
       xAxis: [{
            categories: [<?=$mesFactura;?>],
            tickmarkPlacement: 'on',
            <?php if ($multi==1) {
            echo $tick;
             } else { ?>
            tickInterval: 1,
            <?php } ?>
            labels: {
                    
                    style: {
                        fontSize: '11px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
        }, {  categories: [<?=$diaFactura;?>],
            <?php if ($multi==1) {?>
           tickInterval: 12,
            <?php } else { ?>
            tickInterval: 12,
            <?php } ?>
            linkedTo: 0,
            opposite: true,
            labels: {
                    
                    style: {
                        fontSize: '11px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
        }],
        yAxis: {
        	title: {
                    text: 'Consumo en KWH'
                },
                labels: {
                    formatter: function() {
                        return this.value;
                    }
                }
            
        },
        tooltip: {
        	headerFormat: '<span style="font-size:14px"><b>{point.x}</b></span><br>',
            pointFormat: '{series.name}: <b>{point.y}</b><br/>',
            valueSuffix: ' KW',
            shared: true
        },
        plotOptions: {
            area: {
                marker: {
                    enabled: false,
                    symbol: 'circle',
                    radius: 2,
                    states: {
                        hover: {
                            enabled: true
                        }
                    }
                }
            }
        },
       series: [{
                name: 'Activa',
                data: [<?=$ConsumosActiva;?>],
                color: '#006699'
            }, {
                name: 'Reactiva',
                data: [<?=$ConsumosReActiva;?>],
            	color: '#2BD551'
            }]
});
