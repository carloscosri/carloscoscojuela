	/*Highcharts.getOptions().plotOptions.pie.colors = (function () {
            var colors = [],
                base = Highcharts.getOptions().colors[0],
                i

            for (i = 0; i < 10; i++) {
                // Start out with a darkened base color (negative brighten), and end
                // up with a much brighter color
                colors.push(Highcharts.Color(base).brighten((i - 3) / 7).get());
            }
            return colors;
		}());*/
    
    Highcharts.data({
        csv: document.getElementById('tsv').innerHTML,
        itemDelimiter: '\t',
        parsed: function (columns) {

            var brands = {},
                brandsData = [],
                versions = {},
                drilldownSeries = [];
            
            // Parse percentage strings
            columns[1] = $.map(columns[1], function (value) {
                if (value.indexOf('%') === value.length - 1) {
                    value = parseFloat(value);
                }
                return value;
            });

            $.each(columns[0], function (i, name) {
                var brand,
                    version;

                if (i > 0) {
                    // Remove special edition notes
                    var name2 = name;
                    name = name.split('-')[0];

                    // Split into brand and version
                    // version = name2.match(/([a-z]*)/);
                    version = name2.split('-');
					version = version[1];
                    
                    //if (version) {
                    //    version = version[0];
                    //}
                    brand = name.replace(version, '');

                    // Create the main data
                    if (!brands[brand]) {
                        brands[brand] = columns[1][i];
                    } else {
                        brands[brand] += columns[1][i];
                    }

                    // Create the version data
                    if (version !== null) {
                        if (!versions[brand]) {
                            versions[brand] = [];
                        }
                        versions[brand].push(['' + version, columns[1][i]]);
                    }
                }
                
            });

            $.each(brands, function (name, y) {
                brandsData.push({ 
                    name: name, 
                    y: y,
                    drilldown: versions[name] ? name : null
                });
            });
            $.each(versions, function (key, value) {
                drilldownSeries.push({
                    name: key,
                    id: key,
                    data: value
                });
            });

  // Create the chart
  $('#GraficoPie').highcharts({
                chart: {
                    type: 'pie'
                },
                 credits: {
                      enabled: false
                  },
                title: {
                    text: 'Consumos 2014'
                },
                subtitle: {
                    text: 'Haz clic para desglosar la informacion'
                },
                plotOptions: {
                    series: {
                        dataLabels: {
                            enabled: true,
                            format: '{point.name}: {point.y:.1f} Kw'
                        }
                    },
                    pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true,
                    style: {
                            color: ('#006699','#2BD551','#FF0000') || 'black'
                        }
                }
                },

                tooltip: {
                    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f} Kw</b><br/>'
                }, 

                series: [{
                    name: 'Importes',
                    colorByPoint: true,
                    data: brandsData
                }],
                
                drilldown: {
                    series: drilldownSeries
                }
            })

        }
});