<?php
session_start();
require('fpdf/rotation.php');
header("Content-Type: text/html; charset=UTF-8 ");
class PDF extends PDF_Rotate
{
	function Header()
	{
		$this->Image('../img/oficina/logo_datos.gif',15,20,130);// Logo
		$this->SetFont('Arial','B',15);// Arial bold 15
		$this->Ln(20);// Salto de línea
	}
	
	function Footer()
	{
		$this->SetY(-15);// Posición: a 1,5 cm del final
		$this->SetFont('Arial','I',6);// Arial italic 8
		$this->Ln(3);// Salto de línea
	}
	
	function SeccionTitle($x, $y, $width, $height, $label)
	{
		$this->SetY($y);
		$this->SetX($x);
		$this->SetFont('Arial','B',7);// Arial 12
		$this->SetFillColor(190,190,190);// Color de fondo
		$this->Cell($width,$height,$label,1,1,'C',true);		
		$this->Ln(4); // Salto de línea
	}

	function SeccionBody($x, $y, $width, $text, $height_cont)
	{
		$this->SetY($y+5);
		$this->SetX($x);
		$this->SetFont('Arial','B',7);// Times 12
		$this->SetFillColor(220,220,220);// Color de fondo
		$this->MultiCell($width,$height_cont,$text,1);
		$this->Ln(); // Salto de línea
	}
		

	function PrintSeccion($x, $y, $width, $height, $title, $text, $height_cont)
	{
		$this->SeccionTitle($x, $y, $width, $height, $title);
		$this->SeccionBody($x, $y, $width, $text, $height_cont);
	}
	
	function CreaCelda($x, $y, $width, $label, $r, $g, $b, $fuente, $especial, $tamano){
		$this->SetY($y);
		$this->SetX($x);
		$this->SetFont($fuente, $especial, $tamano);
		$this->SetFillColor($r,$g,$b); // Color de fondo
		$this->Cell($width,3,$label,0,0,'L',true);
	}
	
	function CreaCeldaCentrada($x, $y, $width, $label, $r, $g, $b){
		$this->SetY($y);
		$this->SetX($x);
		$this->SetFont('Arial','',8);
		$this->SetFillColor($r,$g,$b); // Color de fondo
		$this->Cell($width,3,$label,0,0,'C',true);
	}
	function CreaCeldaAlDer($x, $y, $width, $label, $r, $g, $b){
		$this->SetY($y);
		$this->SetX($x);
		$this->SetFont('Arial','',7);
		$this->SetFillColor($r,$g,$b);// Color de fondo
		$this->Cell($width,3,$label,0,0,'R',true);
	}
	
	function RotatedText($x, $y, $txt, $angle)
	{
		$this->Rotate($angle, $x, $y);
		$this->SetFont('Arial','',6);
		$this->Text($x, $y, $txt);
		$this->Rotate(0);
	}
}

define('CLIENT_LONG_PASSWORD', 1);
$link = mysql_connect('82.223.114.64', 'ceneruser', '123456789', false, CLIENT_LONG_PASSWORD);
if (!$link) {die('Could not connect: ' . mysql_error());}
$db_selected = mysql_select_db('cenermed', $link);
if (!$db_selected){die ('Can\'t use foo : ' . mysql_error());}

if (isset($_SESSION['numeroCliente'])){

$fecha=$_GET['Registro_Factura_Fecha'];
$numFactura = $_GET['NumFactura'];
$fechafactura = $fecha." 00:00:00";
 
$consulta_facturas="SELECT * FROM FacturasVarios WHERE CodCliente = ".$_SESSION['numeroCliente']." and Fecha= '".$fechafactura."' and NumFactura = ".$numFactura.";";
$resultados_facturas = mysql_query($consulta_facturas);	

while($registro_factura = mysql_fetch_array($resultados_facturas))
{
   	if($registro_factura["Fecha"]==$fechafactura){

	$meses = array("ENERO", "FEBRERO", "MARZO", "ABRIL", "MAYO", "JUNIO", "JULIO", "AGOSTO", "SEPTIEMBRE", "OCTUBRE", "NOVIEMBRE", "DICIEMBRE");

	$fecha_secciones = explode('-',$fechafactura);
	$mes=$registro_factura["NumeroPeriodo"];
	
	$meses_factura = array();
	$anios = array();
	
	$anio = substr($registro_factura["PeriodoTexto"], -4);

	for ($i=0; $i<12; $i++){
		$mes = $mes-1;
		
		if ($mes == 0){
			$mes = 12;
			$anio = $anio-1;
		}
		$meses_factura[$i] = $mes;
		$anios[$i] = $anio;
	}
	
	$consulta_Cliente= "SELECT * FROM DatosRegistrados where Usuario = ".$_SESSION['numeroCliente'];
	$resultados_Cliente = mysql_query($consulta_Cliente);
	$registro_Cliente = mysql_fetch_array($resultados_Cliente);
	
/*  RECOGIDA DE DATOS  */
$dni = $registro_Cliente['DNI'];
$poliza = $registro_Cliente['NumeroPoliza'];
$FechaAlta = $registro_Cliente['ComercializadoraFechaAlta'];

$NombreTitular = $registro_Cliente['TitularNombre'];
$Ciudad = $registro_Cliente['SuministroCiudad'];
$CP = $registro_Cliente['SuministroCP'];
$Domicilio = $registro_Cliente['SuministroCalle'].' '.$registro_Cliente['SuministroNumero'].' '.$registro_Cliente['SuministroExtension'];
$CUPS = $registro_Cliente['CUPS'];

$BaseImponible = $registro_factura['ImporteBase'];
$TotalFactura = $registro_factura['TotalFactura']; 
$ImporteIVA = $registro_factura['Iva1'] + $registro_factura['Iva2']  +$registro_factura['Iva3'];

$FormaPago = $registro_Cliente['FormaPago'];
$Entidad = $registro_Cliente['BancoDomiciliado'];
$NumCuenta = $registro_Cliente['NumeroCuenta'];
$NumCuenta1 = substr($NumCuenta, 0,4);
$NumCuenta2 = substr($NumCuenta, 4,4);
$NumCuenta3 = substr($NumCuenta, 8,2);
$NumCuenta4 = substr($NumCuenta, 10,6);

$FechaPartes = explode('-', $registro_factura['Fecha']);
$FechaPartesDia = explode (' ',$FechaPartes[2]);
$fechaFacturaGrafico = $FechaPartesDia[0].'/'.$FechaPartes[1].'/'.$FechaPartes[0];

$SumaImportes;

/* CREACION PDF */
/* IMPORTANTE: Los saltos de linea se interpretan y generan igual */
$pdf=new PDF();
$pdf->SetDisplayMode('real');
$pdf->AddPage();
$pdf->SetFont('Arial');

// DATOS DE ENVIO //
$pdf->SeccionBody(112,36,80,'',25);
$pdf->CreaCelda(114,44,75,$NombreTitular,255,255,255,'Arial','B',9);
$pdf->CreaCelda(114,51,75,$Domicilio,255,255,255,'Arial','B',9);
$pdf->CreaCelda(114,55,75,$CP.'  '.$Ciudad,255,255,255,'Arial','B',9);

//CAMPOS BLANCOS PARA BORDES
$pdf->CreaCelda(120,38,65,' ',255,255,255,'Arial','B',9);
$pdf->CreaCelda(120,63,65,' ',255,255,255,'Arial','B',9);
$pdf->CreaCelda(112,48,2,' ',255,255,255,'Arial','B',9);
$pdf->CreaCelda(112,51,2,' ',255,255,255,'Arial','B',9);
$pdf->CreaCelda(112,54,2,' ',255,255,255,'Arial','B',9);
$pdf->CreaCelda(112,57,2,' ',255,255,255,'Arial','B',9);
$pdf->CreaCelda(192,48,2,' ',255,255,255,'Arial','B',9);
$pdf->CreaCelda(192,51,2,' ',255,255,255,'Arial','B',9);
$pdf->CreaCelda(192,54,2,' ',255,255,255,'Arial','B',9);
$pdf->CreaCelda(192,57,2,' ',255,255,255,'Arial','B',9);

// NUMERO DE FACTURA
$pdf->PrintSeccion(17,70,28,5,utf8_decode('Factura Nº'),'',0);
$pdf->SeccionBody(17,65,28,'',10);
$pdf->CreaCeldaCentrada(18,76,26,$numFactura,255,255,255,'Arial','',9);
// FECHA DE EMISION
$pdf->PrintSeccion(45,70,28,5,utf8_decode('Fecha'),'',0);
$pdf->SeccionBody(45,65,28,'',10);
$pdf->CreaCeldaCentrada(46,76,26,$fechaFacturaGrafico,255,255,255,'Arial','',9);
// NUMERO DE CLIENTE
$pdf->PrintSeccion(73,70,30,5,utf8_decode('Nº Cliente'),'',0);
$pdf->SeccionBody(73,65,30,'',10);
$pdf->CreaCeldaCentrada(74,76,28,$_SESSION['numeroCliente'],255,255,255,'Arial','',9);
// DNI/CIF
$pdf->PrintSeccion(103,70,28,5,utf8_decode('DNI/CIF'),'',0);
$pdf->SeccionBody(103,65,28,'',10);
$pdf->CreaCeldaCentrada(104,76,26,$dni,255,255,255,'Arial','',9);
// POLIZA
$pdf->PrintSeccion(131,70,30,5,utf8_decode('Nº Poliza'),'',0);
$pdf->SeccionBody(131,65,30,'',10);
$pdf->CreaCeldaCentrada(132,76,28,$poliza,255,255,255,'Arial','',9);
// FECHA DE ALTA
$pdf->PrintSeccion(161,70,31,5,utf8_decode('Fecha Alta'),'',0);
$pdf->SeccionBody(161,65,31,'',10);
$pdf->CreaCeldaCentrada(162,76,29,$FechaAlta,255,255,255,'Arial','',9);


//PUNTO DE SUMINISTRO
$pdf->PrintSeccion(17,82,175,5,utf8_decode('Datos del Punto de Suministro'),'',0); 

$pdf->PrintSeccion(17,87,100,5,utf8_decode('Nombre del titular'),'',0); 
$pdf->SeccionBody(17,82,100,'',10);
$pdf->CreaCelda(18,93,98,$NombreTitular,255,255,255,'Arial','',8);

$pdf->PrintSeccion(117,87,75,5,utf8_decode('Ciudad'),'',0);
$pdf->SeccionBody(117,82,75,'',10);
$pdf->CreaCelda(118,93,73,$Ciudad,255,255,255,'Arial','',8);

$pdf->PrintSeccion(17,97,115,5,utf8_decode('Domicilio'),'',0); 
$pdf->SeccionBody(17,92,115,'',10);
$pdf->CreaCelda(18,103,113,$Domicilio,255,255,255,'Arial','',8);

$pdf->PrintSeccion(132,97,60,5,utf8_decode('CUPS'),'',0);
$pdf->SeccionBody(132,92,60,'',10);
$pdf->CreaCelda(133,103,58,$CUPS,255,255,255,'Arial','',8);

// DESCRIPCION DETALLES CONCEPTOS
$pdf->PrintSeccion(17,109,135,5,utf8_decode('Descripcion del Concepto'),'',0); 
$pdf->PrintSeccion(152,109,15,5,utf8_decode('% I.V.A.'),'',0); 
$pdf->PrintSeccion(167,109,25,5,utf8_decode('IMPORTE'),'',0);

$pdf->SeccionBody(17,109,135,'',100);
$pdf->SeccionBody(152,109,15,'',100);
$pdf->SeccionBody(167,109,25,'',100);

/* GENERACION LINEAS CONCEPTOS */
$consultaConceptos="SELECT * FROM DetallesFacturasVarios WHERE Numfactura =".$numFactura;
$resultadosConceptos=mysql_query($consultaConceptos);
$posy = 118;
while ($registroConcepto = mysql_fetch_array($resultadosConceptos)){
	$Concepto=$registroConcepto['Concepto'];
	$Importe=$registroConcepto['Precio'];
	$Cantidad=$registroConcepto['Cantidad'];
	$IVA=$registroConcepto['IVA'];
	
	$SumaImportes += $registroConcepto['Precio'];
	
	/* AÑADIR LINEAS */
	$pdf->CreaCelda(20,$posy,130,$Concepto,255,255,255,'Arial','',8);
	if ($IVA != 0){$pdf->CreaCeldaAlDer(153,$posy,13,$IVA,255,255,255,'Arial','',8);}
	$pdf->CreaCeldaAlDer(168,$posy,23,number_format($Importe,2, ',', '.'),255,255,255,'Arial','',8);
	
	$posy += 4; 
}



// SUMA IMPORTES
$pdf->PrintSeccion(17,217,40,5,utf8_decode('Suma Importes'),'',0);
$pdf->SeccionBody(17,212,40,'',10);
$pdf->CreaCeldaCentrada(18,223,38,number_format($SumaImportes,2, ',', '.'),255,255,255,'Arial','',9);
// BASE IMPONIBLE
$pdf->PrintSeccion(57,217,40,5,utf8_decode('Base Imponible'),'',0);
$pdf->SeccionBody(57,212,40,'',10);
$pdf->CreaCeldaCentrada(58,223,38,number_format($BaseImponible,2, ',', '.'),255,255,255,'Arial','',9);
// % IVA
$pdf->PrintSeccion(97,217,25,5,utf8_decode('% IVA'),'',0);
$pdf->SeccionBody(97,212,25,'',10);
$pdf->CreaCeldaCentrada(98,223,23,'XX',255,255,255,'Arial','',9);
// IMPORTE IVA
$pdf->PrintSeccion(122,217,35,5,utf8_decode('Importe IVA'),'',0);
$pdf->SeccionBody(122,212,35,'',10);
$pdf->CreaCeldaCentrada(123,223,33,number_format($ImporteIVA,2, ',', '.'),255,255,255,'Arial','',9);
// TOTAL FACTURA
$pdf->PrintSeccion(157,217,35,5,utf8_decode('TOTAL FACTURA'),'',0);
$pdf->SeccionBody(157,212,35,'',10);
$pdf->CreaCeldaCentrada(158,223,33,number_format($TotalFactura,2, ',', '.'),255,255,255,'Arial','',9);


//FORMA DE PAGO
$pdf->SeccionBody(17,225,30,'',5);
$pdf->CreaCelda(18,231,28,'Forma de Pago',255,255,255,'Arial','B',8);
$pdf->SeccionBody(47,225,70,'',5);
$pdf->CreaCelda(48,231,68,$FormaPago,255,255,255,'Arial','',8);


//DOMICILIACION
$pdf->SeccionBody(17,230,30,'',5);
$pdf->CreaCelda(18,236,28,utf8_decode('Domiciliacion'),255,255,255,'Arial','B',8);
$pdf->SeccionBody(47,230,70,'',5);
$pdf->CreaCelda(48,236,68,$Entidad,255,255,255,'Arial','',8);


//NUM CUENTA
$pdf->SeccionBody(17,235,30,'',5);
$pdf->CreaCelda(18,241,28,utf8_decode('Nº de Cuenta'),255,255,255,'Arial','B',8);
$pdf->SeccionBody(47,235,70,'',5);
$pdf->CreaCelda(48,241,68,$NumCuenta1.'-'.$NumCuenta2.'-'.$NumCuenta3.'-'.$NumCuenta4.'****',255,255,255,'Arial','',8);


$pdf->Output();
		}
	}
	echo "<p align='center' style='margin-top:40px'><strong>La factura solicitada no esta disponible en estos momentos.</strong>";
	echo "<br><br>";
	echo "Por favor, pongase en contacto con nosotros para informanos de este error.<br>";
	echo "Lamentamos las molestias.</p>";
} else {
	echo "Esta seccion es solo para usuarios identificados.<br>";	
	echo "<a href='oficina_index.php'>Volver</a>";
}
?>