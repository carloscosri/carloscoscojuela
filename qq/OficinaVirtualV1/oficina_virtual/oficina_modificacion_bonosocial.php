<?php
/*
NOTAS:

1- ESTA SECCION TOMA LOS DATOS DE LA TABLA DatosRegistrados

2- ESCRIBE EN LA TABLA DatosModificados

*/

/* NOTA:

 COMO NO SE CIERRA LA CONEXION TRAS CADA CONEXION CON LA BASE DE DATOS, LAS CONSULTAS DEJAN DE FUNCIONAR SI SE ESTABLECE COMO UNICA CONEXION
 LA QUE UTILIZA EL RESTO DE LA WEB, POR LO TANTO HASTA QUE SE HAGA LA MIGRACION COMPLETA DE LOS TEXTOS, COEXISTIRAN LAS DOS CONEXIONES, YA QUE
 SE HA AÑADIDO UNA LLAMADA A LA CONEXION BUENA EN ESTE ARCHIVO.
*/

include("_conexion.php");

//Se incluye el archivo que contiene las verificaciones de datos
include("../includes/verificaciones.php");

//Como tienen textos en comun, para evitar el crecimiento innecesario de la base de datos, se incluye el archivo de textos de las secciones de datos del titular y del alta, ya que la mayoria de los textos son comunes
//include($_SESSION["directorio_raiz"]."idiomas/".$_SESSION["idioma"]."/oficina_datos_titular.php");
//include($_SESSION["directorio_raiz"]."idiomas/".$_SESSION["idioma"]."/oficina_contratacion_alta.php");

//include($_SESSION["directorio_raiz"]."idiomas/".$_SESSION["idioma"]."/oficina_modificacion_datostitular.php");
//include($_SESSION["directorio_raiz"]."idiomas/".$_SESSION["idioma"]."/oficina_modificacion_bonosocial.php");

include("../idiomas/".$_SESSION["idioma"]."/oficina_datos_titular.php");
include("../idiomas/".$_SESSION["idioma"]."/oficina_contratacion_alta.php");

include("../idiomas/".$_SESSION["idioma"]."/oficina_modificacion_datostitular.php");
include("../idiomas/".$_SESSION["idioma"]."/oficina_modificacion_bonosocial.php");

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<title><?=$titulo_web;?></title>

<!--Se añaden las hojas de estilos genericas que utiliza esta seccion-->
<link href="../css/estilos.css" rel="stylesheet" type="text/css" />
<link href="../css/secciones.css" rel="stylesheet" type="text/css" />
<link href="css/oficina.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_vieja.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_modificacion_titular.css" rel="stylesheet" type="text/css" />

<!--Ahora se añaden las reglas para Internet Explorer. Asi corregiremos las diferencias de visualizacion entre navegadores
Es importante añadirlo despues de las reglas principales, para que se sobreescriban las correcciones pertinentes. En este caso las diferencias
solo son para internet explorer 7-->

<!--[if IE 7]>
	<link href="../css/estilos_ie7.css" rel="stylesheet" type="text/css" />
	<link href="../css/secciones_ie7.css" rel="stylesheet" type="text/css" />    
<![endif]-->


<?php
if (isset($_POST["submit"]))
{
//Recogemos los valores de las variables
	$estado_bono_social=$_POST["EstadoBonoSocial"];							
	$fecha_bono_social=$_POST["FechaTraspaso"];								
			
//Esta variable controlara si se produce algun error en las verificaciones
	$error=0;	
	
//Ahora comenzaremos con las verficaciones. En primer lugar comprobaremos si los campos obligatorios han sido todos escritos
	if($estado_bono_social=="-1" or $fecha_bono_social=="")
	{
		$error=1;
	}//if($estado_bono_social=="" or $fecha_bono_social=="")

//Comprobamos que la fecha se ha escrito correctamente. En este caso no se añade la comprobacion de si la fecha es posterior a la actual, ya que la fecha de vigencia puede ser posterior (Ejemplo: Alguien que se jubile dentro de un mes)
	if($error==0 and (comprobar_fecha($fecha_bono_social)))
	{
		$error=2;
	}//if($error==0 and (comprobar_fecha($fecha_bono_social)))
	
}//if (isset($_POST["submit"]))

?>

</head>
	
<body>
<!--*************************************************Web realizada por ****************************************-->
	<div id="central_oficina">
    
<?php
//Se comprueba que el usuario se haya identificado, si no es asi se da un aviso y redirigimos a la seccion de identificacion
if ($_SESSION['usuario']=="")
{	
	MsgBox($error_identificacion);
	redirigir("oficina_index.php?id=".$_SESSION["idioma"]);
	
}//if ($_SESSION['usuario'])

//Si el usuario se ha identificado la seccion se ejecutara con normalidad
else
{
//Añadimos los fragmentos comunes, la cabecera, el menu, y el submenu
	include("oficina_fragmentos_comunes.php");
?>                    
		<div class="contenido_seccion_oficina_modificacion_titular">

<!--VERSION VIEJA-->


		<form action="oficina_modificacion_bonosocial.php?id=<?=$_SESSION["idioma"]?>"  method="post" name="form_cambios_datos_titular" id="form_cambios_datos_titular">
		  <table width="500" border="0" cellspacing="0" cellpadding="0">
            <tr> 
              <td><table width="500" border="0" cellspacing="0" cellpadding="0">
                  <tr align="center"> 
                    <td width="500" height="30" class="arialblack18Titulo" style=" background-image:url(imagenes/MarcoAlta.gif);background-repeat:no-repeat"><?=$oficina_submenu4_3?></td>
                  </tr>
                  <tr> 
                    <td height="18" valign="middle"> </td>
                  </tr>

<?php
//Comprobamos si el cliente, tiene ya la tarifa de Bono Social. 
//NOTA: PUEDE QUE TAMBIEN SE QUISIERA VERIFICAR SI HAY CONTENIDO EN LOS CAMPOS "EstadoBonoSocial" Y "FechaVigenciaBonoSocial"
					$consulta_tarifa=mysql_query("SELECT TarifaContratada FROM DatosRegistrados WHERE CodigoCliente  = ".$_SESSION["numeroCliente"]); 
					$tiene_bono_social = mysql_fetch_array($consulta_tarifa);

//Si el cliente ya tiene esa tarifa se mostrara un mensaje										
					if($tiene_bono_social["TarifaContratada"]=="Bono Social")
					{
?>
		                  <tr>
    		              	<td valign="middle" class="texto_advertencia"><?=$error_ya_tiene_bono_social?></td>
        		          </tr>
<?php
					}//if($tiene_bono_social["TarifaContratada"]=="BonoSocial")
					
//Para cualquier otro tipo de tarifa la seccion se ejecutara normalmente
					else
					{					
?>					

<?php
//Esta variable controlara si el cliente tiene o no modificaciones pendientes
					$tiene_modificaciones_pendientes=0;

//Comprobamos si el cliente, tiene modificaciones pendientes
					$consulta_cliente=mysql_query("SELECT * FROM DatosModificados WHERE CodigoCliente  = ".$_SESSION["numeroCliente"]); 
					$hay_cliente = mysql_fetch_array($consulta_cliente);
										
					if($hay_cliente)
					{
						$tiene_modificaciones_pendientes=1;
					}//if($hay_cliente)
										
//Si el cliente tiene modificaciones pendientes se mostrara un mensaje					
					if($tiene_modificaciones_pendientes==1)
					{
?>                  
	                 <!-- <tr>
    	              	<td valign="middle" class="texto_advertencia"><?=$advertencia_cambios_pendientes?></td>
        	          </tr>-->
<?php				}//if($tiene_modificaciones_pendientes==1)?>                  

                  <tr> 
                    <td height="94" class="arial14"><fieldset>
					<legend class="arialblack14">
					<?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 562'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
					</legend>
                      <table width="500" align="center">
<?php
							
							$result = mysql_query("SELECT * FROM DatosRegistrados WHERE CodigoCliente = ".$_SESSION['numeroCliente']." ");
							$row = mysql_fetch_array($result);
							
?>
                        <tr> 
                          <td width="461" height="22" class="arial14"> 
                            <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 119'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                          &nbsp;&nbsp; <input name="TitularNombreFijo"   value="<?php echo(str_replace('"','&quot;',$row['TitularNombre']));?>"type="text" class="textfieldLectura" id="TitularNombreFijo" size="50" maxlength="50" /></td>
                        </tr>
                        <tr> 
                          <td height="22" class="arial14"> 
                            <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 118'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                            &nbsp; <input   value="<?php echo($row['DNI']);?>" name="TitularDNIFijo" type="text" class="textfieldLectura" id="TitularDNIFijo" size="12" maxlength="15" /> 
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                            <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 196'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                            &nbsp; <input name="TitularCodigoFijo"   value="<?php echo($row['CodigoCliente']);?>" type="text" class="textfieldLectura" id="TitularCodigoFijo" size="10" maxlength="10" /> 
                            &nbsp;&nbsp;</td>
                        </tr>
                        <tr> 
                          <td height="22" class="arial14"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 197'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                            &nbsp; <input name="TitularPolizaFijo" value="<?php echo($row['NumeroPoliza']);?>"  type="text" class="textfieldLectura" id="TitularPolizaFijo" size="20" maxlength="20" /></td>
                            
                        </tr>
<!--CAMPO NUEVO-->
                        <tr> 
                          <td height="22" class="arial14">CUPS&nbsp; <input name="CUPSFijo" value="<?php echo($row['CUPS']);?>"  type="text" class="textfieldLectura" id="CUPSFijo" size="50" maxlength="50" /></td>
                        </tr>                        
<!--CAMPO NUEVO-->                        
                        
                        
                      </table>
                      </fieldset></td>
                  </tr>
                  <tr> 
                    <td height="10"></td>
                  </tr>
                  <tr>
                    <td height="125" valign="top"><fieldset  ><legend class="arialblack14"> <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 130'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?></legend>
                    <table width="469" border="0" cellpadding="0" cellspacing="0" align="center">                    
                      <tr>
	                    	<td height="5">&nbsp;</td>                    
    	               </tr>
                        
                      <tr>
                        <td height="22" colspan="2" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">                                               
                          <tr>
                            <td width="105" height="22" valign="middle" class="arial14"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 121'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?></td>
                        </tr>
                        </table>                        </td>
                        <td width="373" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">                          
                          <tr>
                            <td width="375" height="22" valign="top"><input name="SuministroCiudadFijo" type="text" class="textfieldLectura" value="<?php echo(str_replace('"','&quot;',$row['SuministroCiudad']));?>" size="50" maxlength="50" readonly="readonly"></td>
                      </tr>
                        </table>                        </td>
                      </tr>
                      <tr>
                        <td height="25" colspan="2" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                          <tr>
                            <td width="105" height="3"></td>
                          </tr>
                          <tr>
                            <td height="22" valign="middle" class="arial14"><?php $resultTexto=mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto=122');
						$rowTexto=mysql_fetch_array($resultTexto);
						echo($rowTexto['Texto']); ?> </td>
                        </tr>
                        </table>                        </td>
                        <td valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                          <tr>
                            <td width="375" height="3"></td>
                          </tr>
                          <tr>
                            <td height="22" valign="top"><input name="SuministroCPFijo" type="text" class="textfieldLectura" value="<?php echo($row['SuministroCP']);?>" size="5" maxlength="5"></td>
                        </tr>
                        </table>                        </td>
                      </tr>
                      
                      <tr>
                        <td height="25" colspan="2" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                          <tr>
                            <td width="105" height="3"></td>
                          </tr>
                          <tr>
                            <td height="22" valign="middle" class="arial14"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 123'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?></td>
                        </tr>
                        </table>                        </td>
                        <td valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                          <tr>
                            <td width="375" height="3"></td>
                          </tr>
                          <tr>
                            <td height="22" valign="top"><input name="SuministroCalleFijo" type="text" class="textfieldLectura" value="<?php echo(str_replace('"','&quot;',$row['SuministroCalle']));?>" size="50" maxlength="50"></td>
                        </tr>
                        </table>                        </td>
                        </tr>
                      
                      <tr>
                        <td width="63" height="25" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                          <tr>
                            <td width="68" height="3"></td>
                          </tr>
                          <tr>
                            <td height="22" valign="middle" class="arial14"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 124'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?></td>
                        </tr>
                        </table>                        </td>
                        <td colspan="2" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                          <tr>
                            <td width="412" height="3"></td>
                          </tr>
                          <tr>
                            <td height="22" valign="top" class="arial14"><input name="SuministroNumeroFijo" type="text" size="10" maxlength="10"class="textfieldLectura" value="<?php echo($row['SuministroNumero']);?>">&nbsp;&nbsp;&nbsp;
                              <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 125'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                              <input name="SuministroExtensionFijo" type="text" class="textfieldLectura" value="<?php echo(str_replace('"','&quot;',$row['SuministroExtension']));?>" size="20" maxlength="20"></td>
                        </tr>
                                                
                        	</table>
                           </td>
                        </tr>
                        
                        <tr>
                            <td width="412" height="3"></td>
                         </tr>
<!--CAMPO NUEVO-->     
 <tr>
                        <td height="25" colspan="2" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                          <tr>
                            <td width="105" height="3"></td>
                          </tr>
                          <tr>
                            <td height="22" valign="middle" class="arial14"><?=$oficina_datos_titular21?></td>
                        </tr>
                        </table>                        </td>
                        <td valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                          <tr>
                            <td width="375" height="3"></td>
                          </tr>
                          <tr>
                            <td height="22" valign="top"><input name="SuministroAclaradorFijo" type="text" class="textfieldLectura" value="<?php echo(str_replace('"','&quot;',$row['SuministroExtension2']));?>" size="50" maxlength="50"></td>
                        </tr>
                        </table>                        </td>
                        </tr>

<!--CAMPO NUEVO-->    
                                          
                      <tr>
                        <td height="10"></td>
                        <td width="34"></td>
                        <td></td>
                        </tr>
                    </table>
                    </fieldset>                    </td>
                  </tr>
                  
                  <tr>
                    <td height="10"></td>
                  </tr>
                  
<!--SEPARACION ENTRE APARTADOS-->    
                    <tr class="arial12"> 
                	  <td width="451" height="10" class="arial14"></td>
	                  <td width="9" class="arial14"></td>
                </tr>
<!--SEPARACION ENTRE APARTADOS-->                                    

                <tr> 
                	<td width="451" class="nota_campos_obligatorios"><span class="nota_campos_obligatorios"><?=$nota_campos_obligatorios?></span><br /><br /></td>
                </tr>
                
<!--APARTADO NUEVO-->

                <tr class="arial12"> 
                  <td colspan="2" class="arial14" width="600"><fieldset>
                    <legend class="arialblack14"><?=$oficina_modificacion_bonosocial1?></legend>
<!--LOLOLO-->                                              
                    <table width="500" align="center" border="0">
                      
                      <tr> 
                        <td height="5" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                                 
                        </table>                        </td>
                      </tr>
                                      
                      
                      <tr> 
                        <td height="24" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                          <tr>
<!--LALALA-->                          
                            <td height="20" class="arial14" align="right"><?=$oficina_modificacion_bonosocial1?>*</td>
                        </tr>
                        </table>                        </td>
                        <td width="230" valign="top" align="center">

<?php
//Este array contendrá los posibles estados en los cuales se puede solicitar el bono social
							$array_estados[0]=$estado_bono_social1;
							$array_estados[1]=$estado_bono_social2;
							$array_estados[2]=$estado_bono_social3;
							$array_estados[3]=$estado_bono_social4;

//Esta variable controlara el numero de idiomas que se han mostrado en el combobox de idiomas de facturacion	
							$estados_mostrados=0;
?>									                        
						  <select name="EstadoBonoSocial" id="EstadoBonoSocial">
						    <option selected="selected" value="-1"></option>
<?php 
//Recorremos el array de idiomas y los mostramos en el combobox correspondiente
							while($estados_mostrados<count($array_estados))
							{
?>					
								<option value="<?=$estados_mostrados;?>"><?=$array_estados[$estados_mostrados];?></option>
<?php 		
								$estados_mostrados++;
							}//while($estados_mostrados<count($array_estados)) 
?>
						  </select>                                 
                        </td>
                      </tr>
                      <tr> 
    
                     <tr> 
                        <td height="24" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                          
                          <tr>
<!--LALALA-->
                            <td height="20" class="arial14" align="right"><?=$oficina_modificacion_bonosocial2?>*</td>
                        </tr>
                        </table>                        </td>
                        
<?php
//Si el valor de la fecha esta vacio, mostraremos la fecha actual
						if($_POST["FechaTraspaso"]=="")
						{
							$fecha_traspaso=date("d-m-Y");
						}//if($_POST["FechaTraspaso"]=="")
						
						else
						{
							$fecha_traspaso=$_POST["FechaTraspaso"];						
						}//else($_POST["FechaTraspaso"]=="")
?>                        
                        <td width="230" valign="top" align="center"><input name="FechaTraspaso" value="<?=$fecha_traspaso?>" type="text" class="textfield" id="FechaTraspaso" size="19" maxlength="10" /></td>
                      </tr>                       
                    </table>
                    </fieldset></td>
                </tr>
<!--CIERRE DEL APARTADO-->                      
               
<!--APARTADO NUEVO OBSERVACIONES-->                   
                   
				  <tr><td><fieldset>
                      <legend class="arialblack14"> 
                      <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto =152'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                       </legend>
                    <table width="469" align="center">
                        <tr> 
                          <td width="461" align="center" class="arial12"> <textarea name="Observaciones" cols="55" rows="5" id="Observaciones" class="arial14"><?=$observaciones?></textarea> 
                          </td>
                        </tr>
                      </table>
                      </fieldset></td></tr>
<!--APARTADO NUEVO OBSERVACIONES-->                   

                <tr class="arial12"> 
                  <td height="20" class="arial14">&nbsp;</td>
                </tr>
                                      
				<tr>
	                <td height="30" align="center" class="arial14">
                    <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 153'); $rowTexto = mysql_fetch_array($resultTexto);?>
                    <input name="submit" type="submit" id="submit" value="<?php echo($rowTexto['Texto']);?>"> 
                </tr>
                      
                <tr class="arial12"> 
                  <td height="20" class="arial14">&nbsp;</td>
                </tr>
               
              </table></td>
            </tr>
          </table>
          
          </form>



<!--VERSION VIEJA-->
                	
		</div><!--<div class="contenido_seccion_oficina">-->                        
		
        
        <div class="limpiar"></div>
                        	
<!--Por ultimo aparecera el pie de la web-->
<?php 
	include("../includes/pie.php");

//NOTA: EN ESTA OCASION, EL ERROR SE MOSTRARA DESPUES DE LA CARGA DE LA PAGINA, SI NO POR ALGUN MOTIVO EN OCASIONES DESPUES DE DAR EL ERROR CESA LA CARGA DE LA PAGINA
	if(isset($_POST["submit"]))
	{		
//Por ultimo mostraremos el error correspondiente, en el caso de que haya fallado alguna de las verificaciones, o continuaremos con el proceso		
		switch($error)
		{		
//Proceso OK	
			case 0:						
//Antes de realizar el alta en la base de datos, deberemos de preparar los datos que no nos da el formulario

//Como en este caso el cliente ya existe, el codigo del cliente no hace falta calcularlo. Lo mismo sucede con el CUPS
				$cod_usuario=$row["CodigoCliente"];
								
//A continuacion asignaremos el valor a las variables, que como no nos han hecho falta hasta ahora no habiamos asignado de la consulta de los datos actuales del cliente. En este caso los datos son los del suministro

				$suministro_poblacion=$row["SuministroCiudad"];
				$suministro_provincia=$row["SuministroProvincia"];				
				$suministro_calle=$row["SuministroCalle"];								
				$suministro_numero=$row["SuministroNumero"];				
				$suministro_extension=$row["SuministroExtension"];								
				$suministro_aclarador=$row["SuministroExtension2"];				
				$suministro_cp=$row["SuministroCP"];				
				$suministro_telefono=$row["SuministroTelefono1"];				
				$suministro_movil=$row["SuministroTelefono2"];								
				$suministro_fax=$row["SuministroFax"];												
				$suministro_email=$row["SuministroEmail"];																

				$titular_nombre=$row["TitularNombre"];
				$titular_calle=$row["TitularCalle"];
				$titular_numero=$row["TitularNumero"];
				$titular_extension=$row["TitularExtension"];
				$titular_aclarador=$row["TitularAclarador"];
				$titular_poblacion=$row["TitularPoblacion"];;
				$titular_provincia=$row["TitularProvincia"];
				$titular_telefono=$row["TitularTelefono1"];
				$titular_movil=$row["TitularTelefono2"];
				$titular_fax=$row["TitularFax"];
				$titular_email=$row["TitularEmail"];
				$titular_dni=$row["TitularDNI"];
							
				$factura_nombre=$row["FacturaNombre"];
				$factura_calle=$row["FacturaCalle"];
				$factura_numero=$row["FacturaNumero"];
				$factura_extension=$row["FacturaExtension"];
				$factura_aclarador=$row["FacturaAclarador"];
				$factura_poblacion=$row["FacturaPoblacion"];
				$factura_provincia=$row["FacturaProvincia"];
				$factura_telefono=$row["FacturaTelefono1"];
				$factura_movil=$row["FacturaTelefono2"];
				$factura_fax=$row["FacturaFax"];
				$factura_email=$row["FacturaEmail"];
				$factura_dni=$row["FacturaDNI"];
							
				$pagador_nombre=$row["PagadorNombre"];
				$pagador_calle=$row["PagadorCalle"];
				$pagador_numero=$row["PagadorNumero"];
				$pagador_extension=$row["PagadorExtension"];
				$pagador_aclarador=$row["PagadorAclarador"];
				$pagador_poblacion=$row["PagadorPoblacion"];
				$pagador_provincia=$row["PagadorProvincia"];
				$pagador_telefono=$row["PagadorTelefono1"];
				$pagador_movil=$row["PagadorTelefono2"];
				$pagador_fax=$row["PagadorFax"];
				$pagador_email=$row["PagadorEmail"];
				$pagador_dni=$row["PagadorDNI"];
		
				$representante_nombre=$row["RepresentanteEmpresa"];
				$representante_dni=$row["DNIRepresentante"];
													
				$num_cuenta_completo=$row["NumeroCuenta"];
							
				$forma_pago=$row["FormaPago"];
				$idioma_factura=$row["IdiomaFactura"];	
				$entidad_bancaria=$row["BancoDomiciliado"];
				
				$tarifa_contratada=$row["TarifaContratada"];																
				$potencia_contratada=$row["PotenciaContratada"];																				
				$tipo_tension=$row["TipoTension"];				
				$discriminador=$row["Discriminador"];								
				$tipo_alquiler=$row["TipoAlquiler"];				
				$maximetro=$row["Maximetro"];								
				$modo=$row["Modo"];
				$observaciones=$row["Observaciones"];				
				$fases=$row["Fases"];				
				$reactiva=$row["Reactiva"];
								
				$p1_activa=$row["P1Activa"];				
				$p1_reactiva=$row["P1Reactiva"];				
				$p1_maximetro=$row["P1Maximetro"];			

				$p2_activa=$row["P2Activa"];				
				$p2_reactiva=$row["P2Reactiva"];				
				$p2_maximetro=$row["P2Maximetro"];			
				
				$p3_activa=$row["P3Activa"];				
				$p3_reactiva=$row["P3Reactiva"];				
				$p3_maximetro=$row["P3Maximetro"];			
				
				$p4_activa=$row["P4Activa"];				
				$p4_reactiva=$row["P4Reactiva"];				
				$p4_maximetro=$row["P4Maximetro"];			
				
				$p5_activa=$row["P5Activa"];				
				$p5_reactiva=$row["P5Reactiva"];				
				$p5_maximetro=$row["P5Maximetro"];			
				
				$p6_activa=$row["P6Activa"];				
				$p6_reactiva=$row["P6Reactiva"];				
				$p6_maximetro=$row["P6Maximetro"];																													
					
//Los equipos de medida envia un indice y no la descripcion del equipo de medida, ahora lo sustituiremos antes de dar el alta
				switch($estado_bono_social)
				{
					case 0:
						$estado_bono_social="Jubilado";
					break;
					
					case 1:
						$estado_bono_social="Desempleado";					
					break;	
					
					case 2:
						$estado_bono_social="Minusvalido";					
					break;
					
					case 3:
						$estado_bono_social="Familia Numerosa";					
					break;																
				}//switch($estado_bono_social)
								
//Las modificaciones en realidad no cambian los datos de la tabla DatosRegistrados, si no que dan un alta en la tabla DatosModificados. Aunque se hace la inserccion completa, algunos campos quedaran en blanco, ya que se cambian desde la seccion de oficina_modificacion_datossuministro

//Ahora dependiendo de si el usuario ya tiene un registro o no en la tabla de DatosModificados, se realizara un alta nueva o una actualizacion del registro

					if($tiene_modificaciones_pendientes==0)
					{
						$solicitud_modificacion="INSERT INTO `DatosModificados` (`CodigoCliente`, `Usuario`, `Password`, `DNI`, `SuministroCiudad`, `SuministroProvincia`, `SuministroCalle`, `SuministroNumero`, `SuministroExtension`, `SuministroExtension2`, `SuministroCP`, `SuministroTelefono1`, `SuministroTelefono2`, `SuministroFax`, `SuministroEmail`, `TitularNombre`, `TitularCalle`, `TitularNumero`, `TitularExtension`, `TitularAclarador`, `TitularPoblacion`, `TitularProvincia`, `TitularTelefono1`, `TitularTelefono2`, `TitularFax`, `TitularEmail`, `TitularDNI`, `FacturaNombre`, `FacturaCalle`, `FacturaNumero`, `FacturaExtension`, `FacturaAclarador`, `FacturaPoblacion`, `FacturaProvincia`, `FacturaTelefono1`, `FacturaTelefono2`, `FacturaFax`, `FacturaEmail`, `FacturaDNI`, `PagadorNombre`, `PagadorCalle`, `PagadorNumero`, `PagadorExtension`, `PagadorAclarador`, `PagadorPoblacion`, `PagadorProvincia`, `PagadorTelefono1`, `PagadorTelefono2`, `PagadorFax`, `PagadorEmail`, `PagadorDNI`, `RepresentanteEmpresa`, `DNIRepresentante`, `CUPS`, `FormaPago`, `BancoDomiciliado`, `NumeroCuenta`,
`IdiomaFactura`,`TarifaContratada`, `PotenciaContratada`, `TipoTension`, `Discriminador`, `TipoAlquiler`, `Maximetro`, `Modo`, `Observaciones`, `FechaRegistro`, `HoraRegistro`, `Fases`, `Reactiva`, `P1Activa`, `P1Reactiva`, `P1Maximetro`, `P2Activa`, `P2Reactiva`, `P2Maximetro`, `P3Activa`, `P3Reactiva`, `P3Maximetro`, `P4Activa`, `P4Reactiva`, `P4Maximetro`, `P5Activa`, `P5Reactiva`, `P5Maximetro`, `P6Activa`, `P6Reactiva`, `P6Maximetro`, `EstadoBonoSocial`, `FechaVigenciaBonoSocial`) VALUES ('".$cod_usuario."', '".$_SESSION['usuario']."', '".$_SESSION['password']."', '".$_SESSION['DNI']."', '".$suministro_poblacion."', '".$suministro_provincia."', '".$suministro_calle."', '".$suministro_numero."', '".$suministro_extension."', '".$suministro_aclarador."', '".$suministro_cp."','".$suministro_telefono."', '".$suministro_movil."', '".$suministro_fax."', '".$suministro_email."', '".$titular_nombre."','".$titular_calle."','".$titular_numero."' , '".$titular_extension."', '".$titular_aclarador."', '".$titular_poblacion."', '".$titular_provincia."', '".$titular_telefono."', '".$titular_movil."', '".$titular_fax."', '".$titular_email."', '".$titular_dni."', '".$factura_nombre."', '".$factura_calle."', '".$factura_numero."', '".$factura_extension."', '".$factura_aclarador."', '".$factura_poblacion."', '".$factura_provincia."', '".$factura_telefono."', '".$factura_movil."', '".$factura_fax."', '".$factura_email."', '".$factura_dni."', '".$pagador_nombre."', '".$pagador_calle."', '".$pagador_numero."', '".$pagador_extension."', '".$pagador_aclarador."', '".$pagador_poblacion."', '".$pagador_provincia."', '".$pagador_telefono."', '".$pagador_movil."', '".$pagador_fax."', '".$pagador_email."', '".$pagador_dni."', '".$representante_nombre."', '".$representante_dni."', '".$_POST["CUPSFijo"]."', '".$forma_pago."', '".$entidad_bancaria."', '".$num_cuenta_completo."','".$idioma_factura."','".$tarifa_contratada."','".$potencia_contratada."', '".$tipo_tension."', '".$discriminador."','".$tipo_alquiler."','".$maximetro."', '".$modo."', '".$observaciones."', '".date("Y-m-d")."', '".date("Y-m-d H:i:s")."','".$fases."', '".$reactiva."', '".$p1_activa."', '".$p1_reactiva."', '".$p1_maximetro."', '".$p2_activa."', '".$p2_reactiva."', '".$p2_maximetro."', '".$p3_activa."', '".$p3_reactiva."', '".$p3_maximetro."', '".$p4_activa."', '".$p4_reactiva."', '".$p4_maximetro."', '".$p5_activa."', '".$p5_reactiva."', '".$p5_maximetro."', '".$p6_activa."', '".$p6_reactiva."', '".$p6_maximetro."', '".$estado_bono_social."', '".fecha_mysql($fecha_bono_social)."');";																												
				}//if($tiene_modificaciones_pendientes==0)

//Si el usuario ya tenia un registro en la base de datos, se realizara una actualizacion del registro				
				else				
				{
					$solicitud_modificacion="UPDATE `DatosModificados` SET `EstadoBonoSocial` = '".$estado_bono_social."', `FechaVigenciaBonoSocial` = '".fecha_mysql($fecha_bono_social)."' WHERE `CodigoCliente` =".$cod_usuario.";";
																													
				}//else($tiene_modificaciones_pendientes==0)
								
//En cualquier caso ejecutamos la sentencia SQL contruida				
				$ejecutar_solicitud_modificacion=mysql_query($solicitud_modificacion);
															
				MsgBox($modificacion_ok);											
			break;

//Error por no rellenar los campos obligatorios
			case 1:
				MsgBox($error_rellenar);				
			break;	

//Error por caracteres no numericos en telefonos o faxes						
			case 2:
				MsgBox($error_fecha);							
			break;	
																	
		}//switch($error
	}//if (isset($_POST["submit"]))
	
	}//else($tiene_bono_social["TarifaContratada"]=="BonoSocial")
}//else ($_SESSION['usuario'])
		
?>        
    </div><!--<div id="central_oficina"-->
</body>
</html>

