<?php
session_start();
$ie=0;
//Asignamos los valores por defecto a la fecha de inicio y la fecha de fin, estos cambiaran si el usuario pulsa el boton del formulario habiendo escrito algo
$ano = intval(substr(date("d-m-Y"),6,4));
$fecha_inicio_ver = "01-01-2014";
$fecha_fin_ver = "31-12-2014";
$fechaInputIni = "2014-01-01";
$fechaInputFin = "2014-12-31";
$fecha_inicio = "dd-mm-aaaa";
$fecha_fin = "dd-mm-aaaa";

include("_conexion.php");
include("../idiomas/".$_SESSION["idioma"]."/oficina_presentacion.php");

if (isset($_POST["Ver"])){
	include('datos/GetConsumosFechas.php'); 
} else {
	include('datos/GetConsumos.php'); 
}

?>

<?php
//Se comprueba que el usuario se haya identificado, si no es asi se da un aviso y redirigimos a la seccion de identificacion
if ($_SESSION['usuario']=="")
{	
	MsgBox($error_identificacion);
	redirigir("graficos_index.php");
	
}
else
{
		//include("oficina_fragmentos_comunes.php");
?>       
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title><?=$titulo_web;?></title>
<!--CSS-->
<link rel="stylesheet" href="css/estilos.css">
<script src="external/jquery-1.8.0.min.js" type="text/javascript"></script>
<script src="external/jquery-ui-1.8.23.custom.min.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" media="all" href="scripts/calendar/skins/aqua/theme.css" title="Aqua" />
<script type="text/javascript" src="scripts/calendar/calendar.js"></script>

<!-- librer�a para cargar el lenguaje deseado -->
<script type="text/javascript" src="scripts/calendar/lang/calendar-es.js"></script>

<!-- librer�a que declara la funci�n Calendar.setup, que ayuda a generar un calendario en unas pocas l�neas de c�digo -->
<script type="text/javascript" src="scripts/calendar/calendar-setup.js"></script>

<script id="scriptInit" type="text/javascript">
		$(document).ready(function () {
			document.getElementById('inputConsumos').checked = true;

			var column = 0;
			var line = 0;
			colorear('lineal');
			
			$('#inputConsumos').click(function(){
				ocultargrafico('GraficoLine');
				vergrafico('GraficoColumn');
				colorear('barras');
				original('lineal');
			});
			
			
			$('#inputLineal').click(function(){
				ocultargrafico('GraficoColumn');
				vergrafico('GraficoLine');
				colorear('lineal');
				original('barras');
			});
			
		});
	</script>

    
     <script type="text/javascript" src="https://www.google.com/jsapi"></script>
      <script type="text/javascript">
	 $(function () {
		<?php include('datos/GraficoMaxColumn.php'); ?>
    	<?php include('datos/GraficoMaxLine.php'); ?>
	 });
	</script>
 <style>
	 input{height:inherit}
	 </style>
</head>

<body>
<div id="contenido">
	<div id="cabecera">
    	<a href="graficos_index.php" id="logo"><img src="img/logo.png" style="margin: 5px;height: 50px;"></a>
         <div id="enlacesMenu">
          <div class="menu_oficina"><?php include("menu_oficina.php");?></div>
        </div>  
    </div>
    <div id="menu">
 	<p style="font-size: 14px;font-weight: bold;height: 20px;;padding:5px;margin: 0px;background-color: #C0C1C3;">Tipos de gr&aacute;ficos</p>
    <ul class="menu">
        <li class="item1"><a href="graficos_consumos.php">Consumos El&eacute;ctricos</a></li>
        <li class="item2" ><a href="graficos_curvas.php">Curvas de Carga</a></li>
        <li class="item3"><a href="graficos_importes.php">Importes</a></li>
        <li class="item4"><a class="active" href="graficos_maximetros.php">Max&iacute;metro</a></li>
        <li class="item5"><a href="graficos_kwh.php">�/KWH</a></li>
    </ul>
 
</div>
    <div id="cuerpo">
    	<section class="ib-container" id="ib-container" style="text-align:center">

<script>
 function vergrafico(tipo){
	document.getElementById(tipo).style.display = 'block';
 }
 function ocultargrafico(tipo){
	document.getElementById(tipo).style.display = 'none';
 }
  function colorear(tipo){
	document.getElementById(tipo).style.backgroundColor = '#ECC634';
 }
 function original(tipo){
	document.getElementById(tipo).style.backgroundColor = '#CDCDCD';
 }
</script>
<script src="js/highcharts.js"></script>
<script src="js/highcharts-3d.js"></script>
<script src="js/modules/exporting.js"></script>
<script src="js/modules/data.js"></script>
<script src="js/modules/drilldown.js"></script>

	<div class="contenido_ajustado"> 
	<div class="titulo" style="padding-top:25px; border-bottom:none">Grafico M&aacute;ximetros<br /><br /></div>
 
    <div style="text-align:center">
    <?php if($curvas==0){ ?>
    <div id="titulosForm" style="margin-bottom: 5px;"> <r>Fechas:</r><r style="margin-left:430px;">Tipo de gr&aacute;fico:</r>  </div>
    	<form action="graficos_maximetros.php?id=<?=$_SESSION["idioma"]?>" method="post" name="copias_certificados" id="copias_certificados" style="margin-bottom:20px">
		   &nbsp; 
           <?php if($_POST['FechaInicial']==""){
			  $fechaInputIni = $fechaInputIni;
			  }else{
				 $fechaInputIni = $_POST['FechaInicial'];
				  };?>
                    <?php if($_POST['FechaFinal']==""){
			  $fechaInputFin = $fechaInputFin;
			  }else{
				 $fechaInputFin = $_POST['FechaFinal'];
				  };?>
		  <input name="FechaInicial"  id="sel1"  type="text" class="textfield" size="10" maxlength="10" style="box-shadow: 0px 0px;border-radius: 4px 0px 0px 4px;margin-top: -5px;vertical-align: middle;  padding-left: 5px;  border: 1px solid #555;  height: 20px;" value="<?=$fechaInputIni?>"> 
		                
           <input type="button" id="lanzador" style="height: 24px;border: 1px #555 solid;  padding: 3px;box-shadow: 0px 0px;  margin-left: -6px;  border-radius: 0px 4px 4px 0px;" value="..." />
                <!-- script que define y configura el calendario-->
                <script type="text/javascript">
                Calendar.setup({
                inputField : "sel1", // id del campo de texto
                ifFormat : "%Y-%m-%d", // formato de la fecha que se escriba en el campo de texto
                button : "lanzador" // el id del bot�n que lanzar� el calendario
                });
                </script>
                            
                   
		   <input name="FechaFinal" id="sel2" type="text" class="textfield" size="10" maxlength="10" style="box-shadow: 0px 0px;border-radius: 4px 0px 0px 4px;margin-top: -5px;vertical-align: middle;  padding-left: 5px;  border: 1px solid #555;  height: 20px;" value="<?=$fechaInputFin?>">
          <input type="button" id="lanzador2" style="height: 24px;border: 1px #555 solid;  padding: 3px;box-shadow: 0px 0px;  margin-left: -6px;  border-radius: 0px 4px 4px 0px;" value="..." />
<!-- script que define y configura el calendario-->
<script type="text/javascript">
Calendar.setup({
inputField : "sel2", // id del campo de texto
ifFormat : "%Y-%m-%d", // formato de la fecha que se escriba en el campo de texto
button : "lanzador2" // el id del bot�n que lanzar� el calendario
});
</script>
                    
		            <input type="submit" name="Ver" value="Mostrar" style="box-shadow: 0px 0px;background-color:#FFF;height: 24px;">
       
           
   
	<div id="lineal" style="width: 30px; height: 30px; float: right; border-top-left-radius: 5px; border-top-right-radius: 5px; border-bottom-right-radius: 5px; border-bottom-left-radius: 5px; border: 1px solid rgb(153, 153, 153); margin-left: 5px; margin-right: 15.5%; background-color: rgb(236, 198, 52);"><img src="img/lineal.png" id="inputLineal" title="Gr&aacute;fico de lineas" style="margin-top: 3px;"/></div>
    <div id="barras" style="width:30px;height:30px;float:right;background-color: #CDCDCD;border-radius: 5px 5px 5px 5px;border: 1px solid #999;"><img src="img/barras.png" id="inputConsumos" title="Gr&aacute;fico de barras"  style="margin-top: 3px;" /></div>
    <!--<input type="checkbox" id="inputLineal" /> <strong style="text-align:center; text-decoration:none; text-transform:capitalize">Lineal</strong>
    <input type="checkbox" id="inputPie" style="visibility:hidden" /> <strong style="visibility:hidden; text-decoration:none; text-transform:capitalize">Sectores</strong>-->

                  </form>
    <?php } else { ?>
	<?php } ?>
   

    
    
    <div id="GraficoColumn" style="width: 850px;float: left; height: 500px; display:none;margin-left: 20px;"></div>
    <div id="GraficoLine" style="width:850px;float: left; height: 500px; display:block;margin-left: 20px;"></div> 
       <div class="limpiar"></div>
    </div>
</div>


<div class="limpiar"></div>
<?php
}
	include ('includes/footer.php');
?></div></div>
</body>
</html>