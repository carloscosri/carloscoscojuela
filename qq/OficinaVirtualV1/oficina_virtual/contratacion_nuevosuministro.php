<?php
/*
NOTAS:

1- ESTA SECCION TOMA LOS DATOS DE LA TABLA DatosRegistrados

*/

/* NOTA:

 COMO NO SE CIERRA LA CONEXION TRAS CADA CONEXION CON LA BASE DE DATOS, LAS CONSULTAS DEJAN DE FUNCIONAR SI SE ESTABLECE COMO UNICA CONEXION
 LA QUE UTILIZA EL RESTO DE LA WEB, POR LO TANTO HASTA QUE SE HAGA LA MIGRACION COMPLETA DE LOS TEXTOS, COEXISTIRAN LAS DOS CONEXIONES, YA QUE
 SE HA AÑADIDO UNA LLAMADA A LA CONEXION BUENA EN ESTE ARCHIVO.
*/

include("_conexion.php");

//Se incluye el archivo con los textos especifico de esta seccion en el idioma oportuno
//include($_SESSION["directorio_raiz"]."idiomas/".$_SESSION["idioma"]."/oficina_contratacion.php");
include("../idiomas/".$_SESSION["idioma"]."/oficina_contratacion.php");

include("../idiomas/".$_SESSION["idioma"]."/contratacion_nuevosuministro.php");

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<title><?=$titulo_web;?></title>

<!--Se añaden las hojas de estilos genericas que utiliza esta seccion-->
<link href="../css/estilos.css" rel="stylesheet" type="text/css" />
<link href="../css/secciones.css" rel="stylesheet" type="text/css" />
<link href="css/oficina.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_vieja.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_contratacion.css" rel="stylesheet" type="text/css" />


<!--Ahora se añaden las reglas para Internet Explorer. Asi corregiremos las diferencias de visualizacion entre navegadores
Es importante añadirlo despues de las reglas principales, para que se sobreescriban las correcciones pertinentes. En este caso las diferencias
solo son para internet explorer 7-->

<!--[if IE 7]>
	<link href="../css/estilos_ie7.css" rel="stylesheet" type="text/css" />
	<link href="../css/secciones_ie7.css" rel="stylesheet" type="text/css" />    
<![endif]-->

</head>
	
<body>
<!--*************************************************Web realizada por ****************************************-->
	<div id="central_oficina">    
		<div>
<?php
			//include($_SESSION["directorio_raiz"]."includes/cabecera.php");
			include("../includes/cabecera.php");            
?>
		</div>                               

		<div class="posicion_menu">
<?php        
			//include($_SESSION["directorio_raiz"]."includes/menu.php");
			include("../includes/menu.php");
?>			            
		</div><!--<div class="posicion_menu">-->
           
		<div class="posicion_submenu">
<?php
			//include($_SESSION["directorio_raiz"]."includes/submenu.php");
			include("../includes/submenu.php");
?>
        </div> <!--<div class="posicion_menu">-->                                 
                   
		<div class="contenido_seccion_oficina_contratacion_no_registrado">

<!--VERSION VIEJA-->

	 <table width="600" border="0" cellspacing="0" cellpadding="0">
            		 
            <tr> 
              <td width="600" height="462" valign="top">
              	<table width="600" border="0" cellpadding="0" cellspacing="0" class="times12Azul4">
                
                <tr align="center"> 
                  <td colspan="4" height="100"  valign="middle" class="arialblack18Titulo"><?=$contratacion_nuevosuministro1?></tr>                                                                                                
                      
				<tr> 
                  <td height="34" colspan="3" valign="top"> <div align="justify" class="nota_suministro_no_registrado"><?=$oficina_contratacion7?></div></td>
                    
                <td width="9">&nbsp;</td>
                </tr>
                
                <tr> 
                
                  <td height="32" colspan="3"> 

                      <div class="submenu_contratacion"><?=$submenu_contratacion_oficina1?></div>
                                        
               
                      <div class="submenu_contratacion"><a href="contratacion_nuevosuministro_alta.php?id=<?=$_SESSION["idioma"].$ampersand?>s=0" class="enlace_submenu_contratacion"><?=$submenu_contratacion_oficina3?></a></div>
                      <div class="submenu_contratacion"><a href="contratacion_nuevosuministro_alta.php?id=<?=$_SESSION["idioma"].$ampersand?>s=1" class="enlace_submenu_contratacion"><?=$submenu_contratacion_oficina4?></a></div>
                      <div class="submenu_contratacion"><a href="contratacion_nuevosuministro_alta.php?id=<?=$_SESSION["idioma"].$ampersand?>s=2" class="enlace_submenu_contratacion"><?=$submenu_contratacion_oficina5?></a></div>               

<!--LOLOLO-->
      

				<div class="fondo_requerimientos">                                                                                                                                                                    	<div><?=$oficina_contratacion1?></div>                            
                                                    
                    <div class="separacion_apartados arial12"><?php  $result = mysql_query("SELECT * FROM ".$_SESSION['tablaIdioma']." WHERE idTexto= 102"); $row = mysql_fetch_array($result); echo($row[1]);?></div>

                    <div class="arial12"><?=$oficina_contratacion8?><br /><br /></div>
                    
                    <div class="arial12"><?=$oficina_contratacion9?><br /><br /></div>
                                                            
                    <div class="arial12"><?php  $result = mysql_query("SELECT * FROM ".$_SESSION['tablaIdioma']." WHERE idTexto= 677"); $row = mysql_fetch_array($result); echo($row[1]);?></div>
                                
                    <div class="arial12"><?php  $result = mysql_query("SELECT * FROM ".$_SESSION['tablaIdioma']." WHERE idTexto= 103"); $row = mysql_fetch_array($result); echo($row[1]);?></div>

                    <div class="arial12"><?=$oficina_contratacion5?><br /><br /></div>
                    
                    <div class="arial12"><?=$oficina_contratacion6?><br /><br /></div>
                    
                    <div class="arial12"><?php  $result = mysql_query("SELECT * FROM ".$_SESSION['tablaIdioma']." WHERE idTexto= 104"); $row = mysql_fetch_array($result); echo($row[1]);?></div>
                                        
				</div><!--<div class="fondo_requerimientos">-->
<!--LOLOLO-->                                                                                                                                                                  
					</td>
                </tr>
                         
                <tr>
	                <td height="20">&nbsp;</td>
                </tr>
                
                <tr align="left">
                  <td height="18" colspan="4" valign="top" class="nota_contratacion">
		               <strong><?=$oficina_contratacion2?></strong>
                  </td>
                </tr>
                <tr>
                  <td height="1"></td>
                  <td width="203"></td>
                  <td width="237"></td>
                  <td></td>
                </tr>
                
              </table>
              </td>
            </tr>
            <tr>
              <td height="19">&nbsp;</td>
            </tr>
          </table>

<!--VERSION VIEJA-->

        	
		</div><!--<div class="contenido_seccion_oficina_contratacion_no_registrado">-->                        
		
        
        <div class="limpiar"></div>
                        	
<!--Por ultimo aparecera el pie de la web-->
<?php 
//	include($_SESSION["directorio_raiz"]."includes/pie.php");
	include("../includes/pie.php");        
?>        
    </div><!--<div id="central_oficina"-->
</body>
</html>
