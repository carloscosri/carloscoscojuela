<?php

include("_conexion.php");

//Se incluye el archivo que contiene las verificaciones de datos
include("../includes/verificaciones.php");

//Se incluye el archivo con los textos especifico de esta seccion en el idioma oportuno
//include($_SESSION["directorio_raiz"]."idiomas/".$_SESSION["idioma"]."/oficina_informacion_facturas.php");
include("../idiomas/".$_SESSION["idioma"]."/oficina_datos_titular.php");
include("../idiomas/".$_SESSION["idioma"]."/oficina_informacion_facturas.php");

include("../idiomas/".$_SESSION["idioma"]."/oficina_informacion_certificadofacturas.php");

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<title><?=$titulo_web;?></title>

<!--Se añaden las hojas de estilos genericas que utiliza esta seccion-->
<link href="../css/estilos.css" rel="stylesheet" type="text/css" />
<link href="../css/secciones.css" rel="stylesheet" type="text/css" />
<link href="css/oficina.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_vieja.css" rel="stylesheet" type="text/css" />

<link href="css/oficina_informacion_contratos_y_facturas.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_informacion_facturas.css" rel="stylesheet" type="text/css" />

<link href="css/oficina_informacion_contratos_y_facturas.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_informacion_certificadoconsumos.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_informacion_certificadofacturas.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" media="all" href="scripts/calendar/skins/aqua/theme.css" title="Aqua" />


<!-- librería principal del calendario -->
<script type="text/javascript" src="scripts/calendar/calendar.js"></script>

<!-- librería para cargar el lenguaje deseado -->
<script type="text/javascript" src="scripts/calendar/lang/calendar-es.js"></script>

<!-- librería que declara la función Calendar.setup, que ayuda a generar un calendario en unas pocas líneas de código -->
<script type="text/javascript" src="scripts/calendar/calendar-setup.js"></script>


</head>
	
<body>

<h5>&iquest;Qu&eacute; es el  CVV2?</h5>
<p>CVV2 es una medida de seguridad que requerimos para toda   transacci&oacute;n. Ya que el n&uacute;mero CVV2 se encuentra en su tarjeta de cr&eacute;dito pero no   se almacena en ning&uacute;n lado, la &uacute;nica forma de saber cu&aacute;l es el n&uacute;mero CVV2   correcto es cuando se tiene posesi&oacute;n f&iacute;sica de la tarjeta en s&iacute;. Todas las   tarjetas de cr&eacute;dito VISA, MasterCard, Discover y American Express que se   fabricaron en los Estados Unidos en los &uacute;ltimos 5 a&ntilde;os m&aacute;s o menos, cuentan con   un n&uacute;mero CVV2. C&oacute;mo encontrar su n&uacute;mero CVV2:</p>
<p>En una tarjeta Visa, MasterCard o Discover, el n&uacute;mero CVV2 est&aacute; en   la franja de la firma. Lo encontrar&aacute; (ya sea la serie entera de 16 d&iacute;gitos del   n&uacute;mero de su tarjeta O BIEN, s&oacute;lo los &uacute;ltimos 4 d&iacute;gitos) seguido de un espacio,   seguido de un n&uacute;mero de 3 d&iacute;gitos. Este n&uacute;mero de 3 d&iacute;gitos es su n&uacute;mero CVV2.   (Vea a continuaci&oacute;n) </p>
<p><img src="../img/oficina/cvv2.jpg" width="215" height="234" /></p>
<p>En las tarjetas American Express, el n&uacute;mero CVV2 es el n&uacute;mero de   d&iacute;gitos que aparece arriba del final del n&uacute;mero de su tarjeta.</p>
</body>
</html>
