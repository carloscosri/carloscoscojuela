<?php
session_start();
require('fpdf/rotation.php');
header("Content-Type: text/html; charset=UTF-8 ");
include ("jpgraph/jpgraph.php");
include ("jpgraph/jpgraph_pie.php");
include ("jpgraph/jpgraph_bar.php");

class PDF extends PDF_Rotate
{
	function Header()
	{
		$this->SetFont('Arial','B',15);
		$this->Ln(20);
	}
	
	function Footer()
	{
		$this->SetY(-15); 
		$this->SetFont('Arial','I',6); 
		$this->Ln(3);
	}
	
	function SeccionTitle($x, $y, $width, $height, $label)
	{
		$this->SetY($y);
		$this->SetX($x);
		$this->SetFont('Arial','B',8); 
		$this->SetFillColor(190,190,190); 
		$this->Cell($width,$height,$label,1,1,'C',true);		
		$this->Ln(4);
	}

	function SeccionBody($x, $y, $width, $text, $height_cont)
	{
		$this->SetY($y+5);
		$this->SetX($x);
		$this->SetFont('Arial','B',8);
		$this->SetFillColor(220,220,220);
		$this->MultiCell($width,$height_cont,$text,1);
		$this->Ln(); 
	}
	
	function SeccionBodyBorder($x, $y, $width, $text, $height_cont,$r,$g,$b)
	{
		$this->SetY($y+5);
		$this->SetX($x);
		$this->SetFont('Arial','B',8);
		$this->SetDrawColor($r, $g, $b);
		$this->MultiCell($width,$height_cont,$text,1);
		$this->Ln(); 
	}
	
	function SeccionBodyBG($x, $y, $width, $text, $height_cont,$r,$g,$b)
	{
		$this->SetY($y);
		$this->SetX($x);
		$this->SetFont('Arial','',6);
		$this->MultiCell($width,$height_cont,$text);
		$this->SetFillColor($r,$g,$b);
		$this->Ln(); 
	}

	function PrintSeccion($x, $y, $width, $height, $title, $text, $height_cont)
	{
		$this->SeccionTitle($x, $y, $width, $height, $title);
		$this->SeccionBody($x, $y, $width, $text, $height_cont);
	}
	
	function PrintSeccion2($x, $y, $width, $height, $title, $text, $height_cont)
	{
		$this->SeccionTitle($x, $y, $width, $height, $title);
		$this->SetY($y+5);
		$this->SetX($x);
		$this->SetFont('Arial','',8);
		$this->SetFillColor(220,220,220);
		$this->MultiCell($width,$height_cont,$text,1);
		$this->Ln(); 
	}
	
	function CreaCelda($x, $y, $width, $label, $r, $g, $b, $fuente, $especial, $tamano){
		$this->SetY($y);
		$this->SetX($x);
		$this->SetFont($fuente, $especial, $tamano);
		$this->SetFillColor($r,$g,$b); 
		$this->Cell($width,3,$label,0,0,'L',true);
	}
	
	function CreaCelda2($x, $y, $width, $label, $r, $g, $b){
		$this->SetY($y);
		$this->SetX($x);
		$this->SetFont('Arial','B',8);
		$this->SetFillColor($r,$g,$b);
		$this->Cell($width,4,$label,0,0,'C',true);
	}
	
	function CreaCeldaCentrada($x, $y, $width, $label, $r, $g, $b){
		$this->SetY($y);
		$this->SetX($x);
		$this->SetFont('Arial','',8);
		$this->SetFillColor($r,$g,$b); 
		$this->Cell($width,3,$label,0,0,'C',true);
	}
	function CreaCeldaAlDer($x, $y, $width, $label, $r, $g, $b){
		$this->SetY($y);
		$this->SetX($x);
		$this->SetFont('Arial','',8);
		$this->SetFillColor($r,$g,$b);
		$this->Cell($width,3,$label,0,0,'R',true);
	}
	
	function CreaCeldaAlDer2($x, $y, $width, $label, $r, $g, $b){
		$this->SetY($y);
		$this->SetX($x);
		$this->SetFont('Arial','',6);
		$this->SetFillColor($r,$g,$b);
		$this->Cell($width,3,$label,0,0,'R',true);
	}
	
	function CreaCeldaAlDer3($x, $y, $width, $label, $r, $g, $b, $fuente, $especial, $tamano){
		$this->SetY($y);
		$this->SetX($x);
		$this->SetFont($fuente, $especial, $tamano);
		$this->SetFillColor($r,$g,$b); 
		$this->Cell($width,3,$label,0,0,'R',true);
	}
	
	function RotatedText($x, $y, $txt, $angle)
	{
		//Text rotated around its origin
		$this->Rotate($angle, $x, $y);
		$this->SetFont('Arial','',7);
		$this->Text($x, $y, $txt);
		$this->Rotate(0);
	}
}

$nif=$_GET['nif'];
$fechaInicio=$_GET['FechaInicial'];
$fechaFin=$_GET['FechaFinal'];
$fechafactura = $fecha." 00:00:00";
 
include("../includes/sesion.php");

$link = mysql_connect($ruta_sevidor_bd, $usuario_bd, $contrasena_bd);
if (!$link) {die('Could not connect: ' . mysql_error());}

$db_selected = mysql_select_db($nombre_bd, $link);
if (!$db_selected){die ('Can\'t use foo : ' . mysql_error());}

$consulta_facturas="SELECT * FROM FacturasImprimir where Nif='".$nif."' and Fecha>'".$fechaInicio."' and Fecha<'".$fechaFin."' ";
$resultados_facturas = mysql_query($consulta_facturas);	


$pdf=new PDF();
$pdf->SetDisplayMode('real');

while($registro_factura = mysql_fetch_array($resultados_facturas)){
$meses = array("ENERO", "FEBRERO", "MARZO", "ABRIL", "MAYO", "JUNIO", "JULIO", "AGOSTO", "SEPTIEMBRE", "OCTUBRE", "NOVIEMBRE", "DICIEMBRE");

$fecha_secciones = explode('-',$fechafactura);
$mes=$registro_factura["NumeroPeriodo"];
	
$meses_factura = array();
$anios = array();
	
$anio = substr($registro_factura["PeriodoTexto"], -4);

for ($i=0; $i<12; $i++){
	$mes = $mes-1;
		
	if ($mes == 0){
		$mes = 12;
		$anio = $anio-1;
	}
	$meses_factura[$i] = $mes;
	$anios[$i] = $anio;
}
	
//Fecha cobro
$anio = explode('-',$fecha);
$mes = explode('-',$fecha);
$Cobro;
$mes1 = $mes[1] + 1;
$anio1 = $anio[0];
if ($mes1 == 12){
	$anio1 + 1;
}
$dia = explode(' ',$fecha);
$dia = substr($dia[0], -2);
if ($dia >= 1 && $dia<=15) {
	$Cobro = "20"."/".$mes[1]."/".$anio[0];
}else{
	$Cobro="05"."/".$mes1."/".$anio1;
}

$euro = iconv('UTF-8', 'windows-1252', '€');
//////////////////////////////
	
/* RECOGIDA DE DATOS */	
//Datos de la factura	
$fecha = explode(' ',$registro_factura["Fecha"]);
$fechavencimientoContrato = explode(' ',$registro_factura['VencimientoContrato']);
$periodo_factura = $registro_factura["periodo"];
$num_factura = $registro_factura["NumFactura"];
$importe_total=number_format($registro_factura["importe"],2,",",".");
$fechavencimiento = $registro_factura["FechaVencimiento"];
$vencimiento = "Comprobar";
$fechaCobro = "00/00/0000";
$telegestion = $registro_factura['Telegestion'];
$vencimientoContrato = $registro_factura['VencimientoContrato'];
// Datos de titular
$nombre_titular = $registro_factura['NombreAbonado'];
$direccion_titular = $registro_factura['Ndomicilio1'];
$direccion_titular2 = $registro_factura['CodPostal1']." - ".$registro_factura['texto-ciudad'];;
$dni_titular = $registro_factura['Nif'];
$cod_barras = $registro_factura['CodigoBarras'];
$titularCP=$registro_factura['CodPostal1'];
$titularPob=$registro_factura['texto-ciudad1'];

// Datos de suministro
$nombre_suministro = $registro_factura['Nombre2Abonado'];
$direccion_suministro = $registro_factura['Ndomicilio2'];
$poblacion_suministro = $registro_factura['texto-ciudad'];
$suministro_CodPos=$registro_factura['CodPostal2'];
$suministro_ciudad = $registro_factura['texto-ciudad'];
$cups = $registro_factura['CUPS'];
$cnae = $registro_factura['cnae'];
$potencia_contratada=$registro_factura['pckw_ab'];
if($registro_factura['PotContP1']==0){$p1="";} else {$p1=$registro_factura['PotContP1'];}
if($registro_factura['PotContP2']==0){$p2="";} else {$p2=$registro_factura['PotContP2'];}
if($registro_factura['PotContP3']==0){$p3="";} else {$p3=$registro_factura['PotContP3'];}
if($registro_factura['PotContP4']==0){$p4="";} else {$p4=$registro_factura['PotContP4'];}
if($registro_factura['PotContP5']==0){$p5="";} else {$p5=$registro_factura['PotContP5'];}
if($registro_factura['PotContP6']==0){$p6="";} else {$p6=$registro_factura['PotContP6'];}
$tarifa = $registro_factura['nombretarifa'];
if($registro_factura['TensionSuministro']==""){ $tension = " "; } else { $tension = $registro_factura['TensionSuministro']; }
if($registro_factura['contadoractiva']==""){ $contador=" "; } else { $contador=$registro_factura['contadoractiva']; }
$ref_catastral = $registro_factura['ReferenciaCatastral'];
$discHorario = $registro_factura['textodisriminador'];

//Datos de pago
$forma_pago = $registro_factura['plazo1'];
$banco = $registro_factura['ban2-nom'];
if ($registro_factura['dirpago1']=="."){
	$banco = "A ingresar en cuenta";
}
$num_cuenta_banco = substr($registro_factura["numcuenta"],0,4);
$num_cuenta_sucursal = substr($registro_factura["numcuenta"],4,4);
$num_cuenta_digito_control = substr($registro_factura["numcuenta"],8,2);
$num_cuenta_cuenta = substr($registro_factura["numcuenta"],10,10);
$num_cuenta = $num_cuenta_banco.' - '. $num_cuenta_sucursal.' - '.$num_cuenta_digito_control.' - '.substr($num_cuenta_cuenta,1,5).'*****';
if($num_cuenta!="" and $banco!=""){
	$forma_pago="BANCO";}
$base_imponible = number_format($registro_factura["BaseImponible"],2,",",".");
$iva = number_format($registro_factura["CuotaIva"],2,",",".");
$total_factura = number_format($registro_factura["Total"],2,",",".");
$termino_pago = $registro_factura['plazo1'];
$plazo = $registro_factura['plazo2'];

$base1=$registro_factura['Base1'];
$base2=$registro_factura['Base2'];
$iva1=$registro_factura['Iva1'];
$iva2=$registro_factura['Iva2'];
$cuota1=$registro_factura['Cuota1'];
$cuota2=$registro_factura['Cuota2'];

//Lecturas
$fechalectura = explode(" ",$registro_factura['fechalectura']);
$fechalecturaant = explode(" ",$registro_factura['FechaLecturaAnt']);
if($registro_factura['LecturaP1Ant']==0){$lecturaP1ant=' ';}else{$lecturaP1ant=number_format($registro_factura['LecturaP1Ant'],0,",",".").' kWh';}
if($registro_factura['LecturaP2Ant']==0){$lecturaP2ant=' ';}else{$lecturaP2ant=number_format($registro_factura['LecturaP2Ant'],0,",",".").' kWh';}
if($registro_factura['LecturaP3Ant']==0){$lecturaP3ant=' ';}else{$lecturaP3ant=number_format($registro_factura['LecturaP3Ant'],0,",",".").' kWh';}
if($registro_factura['LecturaP4Ant']==0){$lecturaP4ant=' ';}else{$lecturaP4ant=number_format($registro_factura['LecturaP4Ant'],0,",",".").' kWh';}
if($registro_factura['LecturaP5Ant']==0){$lecturaP5ant=' ';}else{$lecturaP5ant=number_format($registro_factura['LecturaP5Ant'],0,",",".").' kWh';}
if($registro_factura['LecturaP6Ant']==0){$lecturaP6ant=' ';}else{$lecturaP6ant=number_format($registro_factura['LecturaP6Ant'],0,",",".").' kWh';}

if($registro_factura['LecturaP1Act']==0){$lecturaP1act=' ';}else{$lecturaP1act=number_format($registro_factura['LecturaP1Act'],0,",",".").' kWh';}
if($registro_factura['LecturaP2Act']==0){$lecturaP2act=' ';}else{$lecturaP2act=number_format($registro_factura['LecturaP2Act'],0,",",".").' kWh';}
if($registro_factura['LecturaP3Act']==0){$lecturaP3act=' ';}else{$lecturaP3act=number_format($registro_factura['LecturaP3Act'],0,",",".").' kWh';}
if($registro_factura['LecturaP4Act']==0){$lecturaP4act=' ';}else{$lecturaP4act=number_format($registro_factura['LecturaP4Act'],0,",",".").' kWh';}
if($registro_factura['LecturaP5Act']==0){$lecturaP5act=' ';}else{$lecturaP5act=number_format($registro_factura['LecturaP5Act'],0,",",".").' kWh';}
if($registro_factura['LecturaP6Act']==0){$lecturaP6act=' ';}else{$lecturaP6act=number_format($registro_factura['LecturaP6Act'],0,",",".").' kWh';}

if($registro_factura['LecturaReacP1Ant']==0){$lecturaReacP1ant=' ';}else{$lecturaReacP1ant=number_format($registro_factura['LecturaReacP1Ant'],0,",",".");}
if($registro_factura['LecturaReacP2Ant']==0){$lecturaReacP2ant=' ';}else{$lecturaReacP2ant=number_format($registro_factura['LecturaReacP2Ant'],0,",",".");}
if($registro_factura['LecturaReacP3Ant']==0){$lecturaReacP3ant=' ';}else{$lecturaReacP3ant=number_format($registro_factura['LecturaReacP3Ant'],0,",",".");}
if($registro_factura['LecturaReacP4Ant']==0){$lecturaReacP4ant=' ';}else{$lecturaReacP4ant=number_format($registro_factura['LecturaReacP4Ant'],0,",",".");}
if($registro_factura['LecturaReacP5Ant']==0){$lecturaReacP5ant=' ';}else{$lecturaReacP5ant=number_format($registro_factura['LecturaReacP5Ant'],0,",",".");}
if($registro_factura['LecturaReacP6Ant']==0){$lecturaReacP6ant=' ';}else{$lecturaReacP6ant=number_format($registro_factura['LecturaReacP6Ant'],0,",",".");}

if($registro_factura['LecturaReacP1Act']==0){$lecturaReacP1act=' ';}else{$lecturaReacP1act=number_format($registro_factura['LecturaReacP1Act'],0,",",".");}
if($registro_factura['LecturaReacP2Act']==0){$lecturaReacP2act=' ';}else{$lecturaReacP2act=number_format($registro_factura['LecturaReacP2Act'],0,",",".");}
if($registro_factura['LecturaReacP3Act']==0){$lecturaReacP3act=' ';}else{$lecturaReacP3act=number_format($registro_factura['LecturaReacP3Act'],0,",",".");}
if($registro_factura['LecturaReacP4Act']==0){$lecturaReacP4act=' ';}else{$lecturaReacP4act=number_format($registro_factura['LecturaReacP4Act'],0,",",".");}
if($registro_factura['LecturaReacP5Act']==0){$lecturaReacP5act=' ';}else{$lecturaReacP5act=number_format($registro_factura['LecturaReacP5Act'],0,",",".");}
if($registro_factura['LecturaReacP6Act']==0){$lecturaReacP6act=' ';}else{$lecturaReacP6act=number_format($registro_factura['LecturaReacP6Act'],0,",",".");}

if($registro_factura['LecturaMaxP1']==0){$lecturaMaxP1=' ';}else{$lecturaMaxP1=number_format($registro_factura['LecturaMaxP1'],2,",",".");}
if($registro_factura['LecturaMaxP2']==0){$lecturaMaxP2=' ';}else{$lecturaMaxP2=number_format($registro_factura['LecturaMaxP2'],2,",",".");}
if($registro_factura['LecturaMaxP3']==0){$lecturaMaxP3=' ';}else{$lecturaMaxP3=number_format($registro_factura['LecturaMaxP3'],2,",",".");}
if($registro_factura['LecturaMaxP4']==0){$lecturaMaxP4=' ';}else{$lecturaMaxP4=number_format($registro_factura['LecturaMaxP4'],2,",",".");}
if($registro_factura['LecturaMaxP5']==0){$lecturaMaxP5=' ';}else{$lecturaMaxP5=number_format($registro_factura['LecturaMaxP5'],2,",",".");}
if($registro_factura['LecturaMaxP6']==0){$lecturaMaxP6=' ';}else{$lecturaMaxP6=number_format($registro_factura['LecturaMaxP6'],2,",",".");}

if($registro_factura['ConsumoReacP1']==0){$ConsumoReacP1=' ';}else{$ConsumoReacP1=number_format($registro_factura['ConsumoReacP1'],0,",",".");}
if($registro_factura['ConsumoReacP2']==0){$ConsumoReacP2=' ';}else{$ConsumoReacP2=number_format($registro_factura['ConsumoReacP2'],0,",",".");}
if($registro_factura['ConsumoReacP3']==0){$ConsumoReacP3=' ';}else{$ConsumoReacP3=number_format($registro_factura['ConsumoReacP3'],0,",",".");}
if($registro_factura['ConsumoReacP4']==0){$ConsumoReacP4=' ';}else{$ConsumoReacP4=number_format($registro_factura['ConsumoReacP4'],0,",",".");}
if($registro_factura['ConsumoReacP6']==0){$ConsumoReacP5=' ';}else{$ConsumoReacP5=number_format($registro_factura['ConsumoReacP5'],0,",",".");}
if($registro_factura['ConsumoReacP5']==0){$ConsumoReacP6=' ';}else{$ConsumoReacP6=number_format($registro_factura['ConsumoReacP6'],0,",",".");}

if($registro_factura['ConsumoP1']==0){$ConsumoP1=' ';}else{$ConsumoP1=number_format($registro_factura['ConsumoP1'],0,",",".").' kWh';}
if($registro_factura['ConsumoP2']==0){$ConsumoP2=' ';}else{$ConsumoP2=number_format($registro_factura['ConsumoP2'],0,",",".").' kWh';}
if($registro_factura['ConsumoP3']==0){$ConsumoP3=' ';}else{$ConsumoP3=number_format($registro_factura['ConsumoP3'],0,",",".".' kWh');}
if($registro_factura['ConsumoP4']==0){$ConsumoP4=' ';}else{$ConsumoP4=number_format($registro_factura['ConsumoP4'],0,",",".").' kWh';}
if($registro_factura['ConsumoP6']==0){$ConsumoP5=' ';}else{$ConsumoP5=number_format($registro_factura['ConsumoP5'],0,",",".").' kWh';}
if($registro_factura['ConsumoP5']==0){$ConsumoP6=' ';}else{$ConsumoP6=number_format($registro_factura['ConsumoP6'],0,",",".").' kWh';}

$total_actual_reactiva = $lecturaReacP1act+$lecturaReacP2act+$lecturaReacP3act+$lecturaReacP4act+$lecturaReacP5act+$lecturaReacP6act;
$total_anterior_reactiva = $lecturaReacP1ant+$lecturaReacP2ant+$lecturaReacP3ant+$lecturaReacP4ant+$lecturaReacP5ant+$lecturaReacP6ant;
$consumo_reactiva = ($total_actual_reactiva - $total_anterior_reactiva)*1000;

$total_actual_activa = $lecturaP1act+$lecturaP2act+$lecturaP3act+$lecturaP4act+$lecturaP5act+$lecturaP6act;
$total_anterior_activa = $lecturaP1ant+$lecturaP2ant+$lecturaP3ant+$lecturaP4ant+$lecturaP5ant+$lecturaP6ant;
$consumo_activa = ($total_actual_activa - $total_anterior_activa)*1000;
$señal_actual = $registro_factura['SeñalActual'];

//Calculo de factura
$textoPotP1 = $registro_factura['TextoPotP1'];
$textoPotP2 = $registro_factura['TextoPotP2'];
$textoPotP3 = $registro_factura['TextoPotP3'];
$textoPotP4 = $registro_factura['TextoPotP4'];
$textoPotP5 = $registro_factura['TextoPotP5'];
$textoPotP6 = $registro_factura['TextoPotP6'];

$importePotP1 = number_format($registro_factura['ImportePotP1'],2,",",".");
$importePotP2 = number_format($registro_factura['ImportePotP2'],2,",",".");
$importePotP3 = number_format($registro_factura['ImportePotP3'],2,",",".");
$importePotP4 = number_format($registro_factura['ImportePotP4'],2,",",".");
$importePotP5 = number_format($registro_factura['ImportePotP5'],2,",",".");
$importePotP6 = number_format($registro_factura['ImportePotP6'],2,",",".");

$textoDtoPotP1 = $registro_factura['TextoDtoPotP1'];
$textoDtoPotP2 = $registro_factura['TextoDtoPotP2'];
$textoDtoPotP3 = $registro_factura['TextoDtoPotP3'];
$textoDtoPotP4 = $registro_factura['TextoDtoPotP4'];
$textoDtoPotP5 = $registro_factura['TextoDtoPotP5'];
$textoDtoPotP6 = $registro_factura['TextoDtoPotP6'];

$importeDtoPotP1 = number_format($registro_factura['ImporteDtoPotP1'],2,",",".");
$importeDtoPotP2 = number_format($registro_factura['ImporteDtoPotP2'],2,",",".");
$importeDtoPotP3 = number_format($registro_factura['ImporteDtoPotP3'],2,",",".");
$importeDtoPotP4 = number_format($registro_factura['ImporteDtoPotP4'],2,",",".");
$importeDtoPotP5 = number_format($registro_factura['ImporteDtoPotP5'],2,",",".");
$importeDtoPotP6 = number_format($registro_factura['ImporteDtoPotP6'],2,",",".");


$textoDtoEnerP1 = $registro_factura['TextoDtoEnerP1'];

$textoDtoEnerP2 = $registro_factura['TextoDtoEnerP2'];
$textoDtoEnerP3 = $registro_factura['TextoDtoEnerP3'];
$textoDtoEnerP4 = $registro_factura['TextoDtoEnerP4'];
$textoDtoEnerP5 = $registro_factura['TextoDtoEnerP5'];
$textoDtoEnerP6 = $registro_factura['TextoDtoEnerP6'];

$importeDtoEnerP1 = number_format($registro_factura['ImporteDtoEnerP1'],2,",",".");
$importeDtoEnerP2 = number_format($registro_factura['ImporteDtoEnerP2'],2,",",".");
$importeDtoEnerP3 = number_format($registro_factura['ImporteDtoEnerP3'],2,",",".");
$importeDtoEnerP4 = number_format($registro_factura['ImporteDtoEnerP4'],2,",",".");
$importDtoeEnerP5 = number_format($registro_factura['ImporteDtoEnerP5'],2,",",".");
$importeDtoEnerP6 = number_format($registro_factura['ImporteDtoEnerP6'],2,",",".");

$textoEnerP1 = $registro_factura['TextoEnerP1'];
$textoEnerP2 = $registro_factura['TextoEnerP2'];
$textoEnerP3 = $registro_factura['TextoEnerP3'];
$textoEnerP4 = $registro_factura['TextoEnerP4'];
$textoEnerP5 = $registro_factura['TextoEnerP5'];
$textoEnerP6 = $registro_factura['TextoEnerP6'];

$importeEnerP1 = number_format($registro_factura['ImporteEnerP1'],2,",",".");
$importeEnerP2 = number_format($registro_factura['ImporteEnerP2'],2,",",".");
$importeEnerP3 = number_format($registro_factura['ImporteEnerP3'],2,",",".");
$importeEnerP4 = number_format($registro_factura['ImporteEnerP4'],2,",",".");
$importeEnerP5 = number_format($registro_factura['ImporteEnerP5'],2,",",".");
$importeEnerP6 = number_format($registro_factura['ImporteEnerP6'],2,",",".");

$textoReacP1 = $registro_factura['TextoReacP1'];
$textoReacP2 = $registro_factura['TextoReacP2'];
$textoReacP3 = $registro_factura['TextoReacP3'];
$textoReacP4 = $registro_factura['TextoReacP4'];
$textoReacP5 = $registro_factura['TextoReacP5'];
$textoReacP6 = $registro_factura['TextoReacP6'];

$importeReacP1 = number_format($registro_factura['ImporteReacP1'],2,",",".");
$importeReacP2 = number_format($registro_factura['ImporteReacP2'],2,",",".");
$importeReacP3 = number_format($registro_factura['ImporteReacP3'],2,",",".");
$importeReacP4 = number_format($registro_factura['ImporteReacP4'],2,",",".");
$importeReacP5 = number_format($registro_factura['ImporteReacP5'],2,",",".");
$importeReacP6 = number_format($registro_factura['ImporteReacP6'],2,",",".");

$importeEnerP1Acceso = number_format($registro_factura['ImporteEnerAccesoP1'],2,",",".");
$importeEnerP2Acceso = number_format($registro_factura['ImporteEnerAccesoP2'],2,",",".");
$importeEnerP3Acceso = number_format($registro_factura['ImporteEnerAccesoP3'],2,",",".");
$importeEnerP4Acceso = number_format($registro_factura['ImporteEnerAccesoP4'],2,",",".");
$importeEnerP5Acceso = number_format($registro_factura['ImporteEnerAccesoP5'],2,",",".");
$importeEnerP6Acceso = number_format($registro_factura['ImporteEnerAccesoP6'],2,",",".");

$textoEnerAccesoP1 = $registro_factura['TextoEnerAccesoP1'];
$textoEnerAccesoP2 = $registro_factura['TextoEnerAccesoP2'];
$textoEnerAccesoP3 = $registro_factura['TextoEnerAccesoP3'];
$textoEnerAccesoP4 = $registro_factura['TextoEnerAccesoP4'];
$textoEnerAccesoP5 = $registro_factura['TextoEnerAccesoP5'];
$textoEnerAccesoP6 = $registro_factura['TextoEnerAccesoP6'];

if($registro_factura['ConsumoReacP1']==0){$ConsumoReacP1=' ';}else{$ConsumoReacP1=number_format($registro_factura['ConsumoReacP1'],0,",",".");}
if($registro_factura['ConsumoReacP2']==0){$ConsumoReacP2=' ';}else{$ConsumoReacP2=number_format($registro_factura['ConsumoReacP2'],0,",",".");}
if($registro_factura['ConsumoReacP3']==0){$ConsumoReacP3=' ';}else{$ConsumoReacP3=number_format($registro_factura['ConsumoReacP3'],0,",",".");}
if($registro_factura['ConsumoReacP4']==0){$ConsumoReacP4=' ';}else{$ConsumoReacP4=number_format($registro_factura['ConsumoReacP4'],0,",",".");}
if($registro_factura['ConsumoReacP6']==0){$ConsumoReacP5=' ';}else{$ConsumoReacP5=number_format($registro_factura['ConsumoReacP5'],0,",",".");}
if($registro_factura['ConsumoReacP5']==0){$ConsumoReacP6=' ';}else{$ConsumoReacP6=number_format($registro_factura['ConsumoReacP6'],0,",",".");}

if($registro_factura['ConsumoP1']==0){$ConsumoP1=' ';}else{$ConsumoP1=number_format($registro_factura['ConsumoP1'],0,",",".");}
if($registro_factura['ConsumoP2']==0){$ConsumoP2=' ';}else{$ConsumoP2=number_format($registro_factura['ConsumoP2'],0,",",".");}
if($registro_factura['ConsumoP3']==0){$ConsumoP3=' ';}else{$ConsumoP3=number_format($registro_factura['ConsumoP3'],0,",",".");}
if($registro_factura['ConsumoP4']==0){$ConsumoP4=' ';}else{$ConsumoP4=number_format($registro_factura['ConsumoP4'],0,",",".");}
if($registro_factura['ConsumoP6']==0){$ConsumoP5=' ';}else{$ConsumoP5=number_format($registro_factura['ConsumoP5'],0,",",".");}
if($registro_factura['ConsumoP5']==0){$ConsumoP6=' ';}else{$ConsumoP6=number_format($registro_factura['ConsumoP6'],0,",",".");}

$impuesto = $registro_factura['impuesto'];
$sinimpuesto = $registro_factura['simpuesto'];
$coeficiente = $registro_factura['coeficiente'];
$total_impuesto = number_format($registro_factura['totalimpuesto'],2,",",".");

$alquiler = $registro_factura['talqreac'];

$boe2012texto42011=$registro_factura['BOE2012Texto42011'];
$boe2012texto12012=$registro_factura['BOE2012Texto12012'];
$boe2012textototal=$registro_factura['BOE2012TextoTotal'];
$boe2012consumo42011=$registro_factura['BOE2012Consumo42011'];
$boe2012consumo12012=$registro_factura['BOE2012Consumo12012'];
$boe2012consumototal=$registro_factura['BOE2012ConsumoTotal'];
$boe2012importe42011=$registro_factura['BOE2012Importe42011'];
$boe2012importe12012=$registro_factura['BOE2012Importe12012'];
$boe2012importetotal=$registro_factura['BOE2012ImporteTotal'];
$boe2012textoabril=$registro_factura['BOE2012TextoAbril'];
$boe2012consumoabril=$registro_factura['BOE2012ConsumoAbril'];
$boe2012importeabril=$registro_factura['BOE2012ImporteAbril'];

//Historial consumos
$mes1 = $registro_factura['Mes1'];
$mes2 = $registro_factura['Mes2'];
$mes3 = $registro_factura['Mes3'];
$mes4 = $registro_factura['Mes4'];
$mes5 = $registro_factura['Mes5'];
$mes6 = $registro_factura['Mes6'];
$mes7 = $registro_factura['Mes7'];
$mes8 = $registro_factura['Mes8'];
$mes9 = $registro_factura['Mes9'];
$mes10 = $registro_factura['Mes10'];
$mes11 = $registro_factura['Mes11'];
$mes12 = $registro_factura['Mes12'];
$mes13 = $registro_factura['Mes13'];
$mes14 = $registro_factura['Mes14'];

$anio1 = $registro_factura['An1'];
$anio2 = $registro_factura['An2'];
$anio3 = $registro_factura['An3'];
$anio4 = $registro_factura['An4'];
$anio5 = $registro_factura['An5'];
$anio6 = $registro_factura['An6'];
$anio7 = $registro_factura['An7'];
$anio8 = $registro_factura['An8'];
$anio9 = $registro_factura['An9'];
$anio10 = $registro_factura['An10'];
$anio11 = $registro_factura['An11'];
$anio12 = $registro_factura['An12'];
$anio13 = $registro_factura['An13'];
$anio14 = $registro_factura['An14'];

$con1 = $registro_factura['Con1'];
$con2 = $registro_factura['Con2'];
$con3 = $registro_factura['Con3'];
$con4 = $registro_factura['Con4'];
$con5 = $registro_factura['Con5'];
$con6 = $registro_factura['Con6'];
$con7 = $registro_factura['Con7'];
$con8 = $registro_factura['Con8'];
$con9 = $registro_factura['Con9'];
$con10 = $registro_factura['Con10'];
$con11 = $registro_factura['Con11'];
$con12 = $registro_factura['Con12'];
$con13 = $registro_factura['Con13'];
$con14 = $registro_factura['Con14'];

$i=0;
if($mes1=="") {} else {$meses[$i]=$mes1; $i+=1;}
if($mes2=="") {} else {$meses[$i]=$mes2; $i+=1;}
if($mes3=="") {} else {$meses[$i]=$mes3; $i+=1;}
if($mes4=="") {} else {$meses[$i]=$mes4; $i+=1;}
if($mes5=="") {} else {$meses[$i]=$mes5; $i+=1;}
if($mes6=="") {} else {$meses[$i]=$mes6; $i+=1;}
if($mes7=="") {} else {$meses[$i]=$mes7; $i+=1;}
if($mes8=="") {} else {$meses[$i]=$mes8; $i+=1;}
if($mes9=="") {} else {$meses[$i]=$mes9; $i+=1;}
if($mes10=="") {} else {$meses[$i]=$mes10; $i+=1;}
if($mes11=="") {} else {$meses[$i]=$mes11; $i+=1;}
if($mes12=="") {} else {$meses[$i]=$mes12; $i+=1;}
if($mes13=="") {} else {$meses[$i]=$mes13; $i+=1;}
if($mes14=="") {} else {$meses[$i]=$mes14; $i+=1;}

$i=0;
if($anio1=="") {} else {$anios[$i]=$anio1; $i+=1;}
if($anio2=="") {} else {$anios[$i]=$anio2; $i+=1;}
if($anio3=="") {} else {$anios[$i]=$anio3; $i+=1;}
if($anio4=="") {} else {$anios[$i]=$anio4; $i+=1;}
if($anio5=="") {} else {$anios[$i]=$anio5; $i+=1;}
if($anio6=="") {} else {$anios[$i]=$anio6; $i+=1;}
if($anio7=="") {} else {$anios[$i]=$anio7; $i+=1;}
if($anio8=="") {} else {$anios[$i]=$anio8; $i+=1;}
if($anio9=="") {} else {$anios[$i]=$anio9; $i+=1;}
if($anio10=="") {} else {$anios[$i]=$anio10; $i+=1;}
if($anio11=="") {} else {$anios[$i]=$anio11; $i+=1;}
if($anio12=="") {} else {$anios[$i]=$anio12; $i+=1;}
if($anio13=="") {} else {$anios[$i]=$anio13; $i+=1;}
if($anio14=="") {} else {$anios[$i]=$anio14; $i+=1;}

$i=0;
if($con1=="") {} else {$consu[$i]=$con1; $i+=1;}
if($con2=="") {} else {$consu[$i]=$con2; $i+=1;}
if($con3=="") {} else {$consu[$i]=$con3; $i+=1;}
if($con4=="") {} else {$consu[$i]=$con4; $i+=1;}
if($con5=="") {} else {$consu[$i]=$con5; $i+=1;}
if($con6=="") {} else {$consu[$i]=$con6; $i+=1;}
if($con7=="") {} else {$consu[$i]=$con7; $i+=1;}
if($con8=="") {} else {$consu[$i]=$con8; $i+=1;}
if($con9=="") {} else {$consu[$i]=$con9; $i+=1;}
if($con10=="") {} else {$consu[$i]=$con10; $i+=1;}
if($con11=="") {} else {$consu[$i]=$con11; $i+=1;}
if($con12=="") {} else {$consu[$i]=$con12; $i+=1;}
if($con13=="") {} else {$consu[$i]=$con13; $i+=1;}
if($con14=="") {} else {$consu[$i]=$con14; $i+=1;}

$emisora=$registro_factura['NIFEmisora'];
$referencia=$registro_factura['Referencia'];
$identificacion=$registro_factura['Identi'];
$meses_nombres=array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
$fecha_partes=explode('-',$fecha[0]);
$fecha_partesV=explode('-',$fechavencimientoContrato[0]);


//formula Por potencia contratada
$ImportePotencia=0;
if ($registro_factura['ImportePotP1'] != ""){
	 $ImportePotencia=$ImportePotencia + $registro_factura['ImportePotP1'];
}elseif ($registro_factura['ImportePotP2'] != ""){
	 $ImportePotencia=$ImportePotencia + $registro_factura['ImportePotP2'];
}elseif ($registro_factura['ImportePotP3'] != ""){
	 $ImportePotencia=$ImportePotencia + $registro_factura['ImportePotP3'];
}elseif ($registro_factura['ImportePotP4'] != ""){
	 $ImportePotencia=$ImportePotencia + $registro_factura['ImportePotP4'];
}elseif ($registro_factura['ImportePotP5'] != ""){
	 $ImportePotencia=$ImportePotencia + $registro_factura['ImportePotP5'];
}elseif ($registro_factura['ImportePotP6'] != ""){
	 $ImportePotencia=$ImportePotencia + $registro_factura['ImportePotP6'];
};
$ImportePotencia;

//formula Por energia contratada

$ImporteEnergia=0;
if ($importeEnerP1 != ""){
	 $ImporteEnergia=$ImporteEnergia + $importeEnerP1;
}elseif ($importeEnerP2 != ""){
	 $ImporteEnergia=$ImporteEnergia + $importeEnerP2;
}elseif ($importeEnerP3 != ""){
	 $ImporteEnergia=$ImporteEnergia + $importeEnerP3;
}elseif ($importeEnerP4 != ""){
	 $ImporteEnergia=$ImporteEnergia + $importeEnerP4;
}elseif ($importeEnerP5 != ""){
	 $ImporteEnergia=$ImporteEnergia + $importeEnerP5;
}elseif ($importeEnerP6 != ""){
	 $ImporteEnergia=$ImporteEnergia + $importeEnerP6;
};
$ImporteEnergia;

//media diaria
$total= $registro_factura['TotConsumoP1'] + $registro_factura['TotConsumoP2'] + $registro_factura['TotConsumoP3'] + $registro_factura['TotConsumoP4'] +$registro_factura['TotConsumoP5'] + $registro_factura['TotConsumoP6'];
$diasfacturar1=$registro_factura['diasfacturar1'];
if (($diasfacturar1 == "") or ($diasfacturar1==0)){
}else{
	$media = $total/$diasfacturar1;
}

//media diaria 14 m
/*if ($registro_factura['TipoPeriodo']=="BIM"){
$media_diaria =(($con08/61)+($con09/61)+($con10/61)+($con11/61)+($con12/61)+($con13/61)+($con14/61))/14;
}else{*/
$media_diaria =(($con1/30.5)+($con2/30.5)+($con3/30.5)+($con4/30.5)+($con5/30.5)+($con6/30.5)+($con7/30.5)+($con8/30.5)+($con9/30.5)+($con10/30.5)+($con11/30.5)+($con12/30.5)+($con13/30.5)+($con14/30.5))/14;
//}

//consumo anual

if ($registro_factura['TipoPeriodo']=="BIM") {
$consumoAnual = ($con14)+($con13)+($con12)+($con11)+($con10)+($con9)+($con8);
}else{
$consumoAnual =($con14)+($con13)+($con12)+($con11)+($con10)+($con9)+($con8)+($con7)+($con6)+($con5)+($con4)+($con3)+($con2)+($con1);
}

//Impuestos aplicados
$cuotaIE = $registro_factura['CuotaIE'];
if ( ($cuotaIE != 0 & $cuota1 != 0) or ($cuota1 != NULL or $cuotaIE != NULL))
{
		$impuestosAplicados = $cuotaIE + $cuota1;
}else if ($cuotaIE != 0 or $cuotaIE != NULL){
	$impuestosAplicados = $cuotaIE;	
}else if ($cuota1 != 0 or $cuota1 != NULL){
	$impuestosAplicados = $cuota1;	
}

//Costes Regulados
$costesRegulados = $registro_factura['ImporteEnerAccesoP1'] + $registro_factura['ImporteEnerAccesoP2'] +$registro_factura['ImporteEnerAccesoP3'] +$registro_factura['ImporteEnerAccesoP4'] +$registro_factura['ImporteEnerAccesoP5'] +$registro_factura['ImporteEnerAccesoP6'] + $registro_factura['ImporteReacP1'] + $registro_factura['ImporteReacP2'] +$registro_factura['ImporteReacP3'] +$registro_factura['ImporteReacP4'] +$registro_factura['ImporteReacP5'] +$registro_factura['ImporteReacP6'] + + $registro_factura['ImportePotAccesoP1'] + $registro_factura['ImportePotAccesoP2'] + $registro_factura['ImportePotAccesoP3'] + $registro_factura['ImportePotAccesoP4'] + $registro_factura['ImportePotAccesoP5'] + $registro_factura['ImportePotAccesoP6'];

//Costes Produccion
$talqreac = $registro_factura['talqreac'];
if ($talqreac == 0 or $talqreac == NULL){
	$costeProduccion = $registro_factura['importe'] - $costesRegulados - $impuestosAplicados ; 		
} else{
	$costeProduccion = $registro_factura['importe'] - $costesRegulados - $impuestosAplicados - $talqreac;
}
//HISTORIAL
if ($con1 != 0){
(int)$historial1 = $con1;
	$historialmes1 = substr($mes1 , 0, 5);

}else{
(int)$historial1 = '';
$historialmes1 = '';
}
if($con2 != 0){
	(int)$historial2 = $con2;
	$historialmes2 = substr($mes2 , 0, 5);
}else{
(int)$historial2 = '';
$historialmes2 = '';
}
if($con3 != 0){
	(int)$historial3 = $con3;
	$historialmes3 = substr($mes3 , 0, 5);
}else{
(int)$historial3 = '';
$historialmes3 = '';
}
if($con4 != 0){
	(int)$historial4 = $con4;
	$historialmes4 = substr($mes4 , 0, 5);
}else{
(int)$historial4 = '';
$historialmes4 = '';
}
if($con5 != 0){
	(int)$historial5 = $con5;
	$historialmes5 = substr($mes5 , 0, 5);
}else{
(int)$historial5 = '';
$historialmes5 = '';
}
if($con6 != 0){
	(int)$historial6 = $con6;
	$historialmes6 = substr($mes6 , 0, 5);
}else{
(int)$historial6 = '';
$historialmes6 = '';
}
if($con7 != 0){
	(int)$historial7 = $con7;
	$historialmes7 = substr($mes7 , 0, 5);
}else{
(int)$historial7 = '';
$historialmes7 = '';
}
if($con8 != 0){
	(int)$historial8 = $con8;
	$historialmes8 = substr($mes8 , 0, 5);
}else{
(int)$historial8 = '';
$historialmes8 = '';
}
if($con9 != 0){
	(int)$historial9 = $con9;
	$historialmes9 = substr($mes9 , 0, 5);
}else{
(int)$historial9 = '';
$historialmes9 = '';
}
if($con10 != 0){
	(int)$historial10 = $con10;
	$historialmes10 = substr($mes10 , 0, 5);

}else{
(int)$historial10 = '';
$historialmes10 = '';
}
if($con11 != 0){
	(int)$historial11 = $con11;
	$historialmes11 = substr($mes11 , 0, 5);
}else{
(int)$historial11 = '';
$historialmes11 = '';
}
if($con12 != 0){
	(int)$historial12 = $con12;
	$historialmes12 = substr($mes12 , 0, 5);
}else{
(int)$historial12 = '';
$historialmes12 = '';
}
if($con13 != 0){
	(int)$historial13 = $con13;
	$historialmes13 = substr($mes13 , 0, 5);
}else{
(int)$historial13 = '';
$historialmes13 = '';
}
if($con14 != 0){
	(int)$historial14 = $con14;
	$historialmes14 = substr($mes14 , 0, 5);
}else{
(int)$historial14 = '';
$historialmes14 = '';
}
//$historial = trim($historial, ', ');
//$historial = substr($historial, -1);
//Graficos


//BARRAS
$grafica = new Graph(360, 150);
$grafica->img->SetMargin(50,20,20,40);

/*Define el tipo de escala que va a utilizar y el
valor minimo y maximo para el eje y*/
$maximoConsumo = max($historial1, $historial2, $historial3, $historial4, $historial5, $historial6, $historial7, $historial8, $historial9, $historial10, $historial11, $historial12, $historial13, $historial14);
$grafica->SetScale("textlin", 0, $maximoConsumo);
if ($maximoConsumo < 400){
$grafica->yaxis->scale->ticks->Set(40);
}elseif (($maximoConsumo > 400) and ($maximoConsumo < 1000)){
$grafica->yaxis->scale->ticks->Set(100); // Set major and minor tick to 10 
}elseif ($maximoConsumo > 1000){
	$grafica->yaxis->scale->ticks->Set(500); // Set major and minor tick to 10 
}
// Asigna el titulo de la gráfica
$grafica->title->Set("Historial de Consumo");

//Define una serie, en este caso para un grafico de barras
$consumos = array($historial1, $historial2, $historial3, $historial4, $historial5, $historial6, $historial7, $historial8, $historial9, $historial10, $historial11, $historial12, $historial13, $historial14);
$valores = new BarPlot($consumos);
$meses = array($historialmes1, $historialmes2, $historialmes3, $historialmes4, $historialmes5, $historialmes6, $historialmes7, $historialmes8, $historialmes9, $historialmes10, $historialmes11, $historialmes12, $historialmes13, $historialmes14);
$grafica->xaxis->SetTickLabels($meses);
$grafica->xaxis->SetLabelAngle(90);
$grafica->xaxis->SetFont(FF_DV_SANSSERIF,FS_BOLD,5);
// Configuramos color de las barras
$valores->SetColor('#C0C0C0');
$valores->SetFillColor('#C0C0C0');
//agrega la serie x al grafico
$grafica->Add($valores);

//Muestra el grafico
$grafica->Stroke("nameGraph2.png");

//TARTA
$data = array($costeProduccion,$impuestosAplicados,$costesRegulados);

$graph = new PieGraph(450,95);
$graph->SetShadow();
//$graph->SetShadow();
// Setup margin and titles

$p1 = new PiePlot($data);
$p1->SetSize(0.44);
$p1->SetCenter(0.5,0.6);

// Setup slice labels and move them into the plot
$p1->value->SetFont(FF_FONT1,FS_REGULAR);
$p1->SetColor("black");

$p1->SetLabelPos(3);
$p1->SetLabelType ("PIE_VALUE_ABS" );
//$p1->SetSliceColors(array('blue','green','yellow'));
 $p1->SetStartAngle(45); 
 $p1->SetSliceColors(array("blue","green","yellow"));
$nombres=array("Coste de produccion (%d)","Impuestos aplicados (%d)","Costes Regulados (%d)");
$p1->SetLegends($nombres);
$graph->legend->SetPos(0.63, 0.4);
//$graph->legend->SetFont(FF_FONT2,FS_NORMAL,4);
$graph->legend->SetColumns(1);

//$p1->SetSize(30); 
// Explode all slices
$p1->ExplodeAll(3);

$graph->Add($p1);
$graph->Stroke("nameGraph.png");
 
//Aqui agrego la imagen que acabo de crear con jpgraph




/* CREACION PDF */
/* IMPORTANTE: Los saltos de linea se interpretan y generan igual */
$pdf=new PDF();
$pdf->SetDisplayMode('real');
$pdf->AddPage();
$pdf->SetFont('Arial');
$pdf->SetAutoPageBreak(true,5);  
//$pdf->Image('../img/oficina/logo_datos.gif',10,8,120);

/* CABECERA FACTURA */
/*$pdf->CreaCelda(110,12,25,'Avda. Barcelona 109, 3ra planta. 08970 San Joan Despi.',255,255,255,'Arial','',7);
$pdf->CreaCelda(110,16,25,'Telf. 93 118 06 00   Fax 93 333 38 62',255,255,255,'Arial','',7);
$pdf->CreaCelda(110,20,25,'email: administracion@aura-energia.com   www.aura-energia.com',255,255,255,'Arial','',7);
$pdf->CreaCelda(110,31,25,'AVERIAS',255,255,255,'Arial','B',10);*/


$pdf->Image('fpdf/front.png',0, 0, 210);

/* INFORMACION DE FACTURA */
/*$pdf->CreaCelda(15,43,25,$num_factura,255,255,255,'Arial','',8);
$pdf->CreaCelda(61,47,25,$fecha_partes[2].' de '.$meses_nombres[number_format($fecha_partes[1]-1)].' de '.$fecha_partes[0],255,255,255,'Arial','',8);
$pdf->CreaCelda(61,51,25,$periodo_factura,255,255,255,'Arial','',8);
$pdf->CreaCelda(61,55,25,$importe_total.' Euros',255,255,255,'Arial','b',10);
$pdf->CreaCelda(61,62,25,$registro_factura['Energia-Cal'],255,255,255,'Arial','',8);
$pdf->CreaCelda(61,69,25,$banco,255,255,255,'Arial','',8);
$pdf->CreaCelda(61,73,25,$num_cuenta_banco.''.$num_cuenta_sucursal.''.$num_cuenta_digito_control.''.substr($num_cuenta_cuenta,1,5).'*****',255,255,255,'Arial','',8);*/
$fechavencimientoFor=date("d/m/Y",strtotime($fechavencimiento));

//$pdf->CreaCelda(61,61,25,$registro_factura['Energia-Cal'],255,255,255,'Arial','',8);

//$pdf->CreaCelda(61,73.5,25,$fechavencimientoFor,255,255,255,'Arial','',8);


///Fatura Resumen
$ImportePotencia = $registro_factura['ImportePotP1'] + $registro_factura['ImportePotP2'] + $registro_factura['ImportePotP3'] + $registro_factura['ImportePotP4'] + $registro_factura['ImportePotP5'] + $registro_factura['ImportePotP6'];
$ImporteEnergia = $registro_factura['ImporteEnerP1'] + $registro_factura['ImporteEnerP2'] + $registro_factura['ImporteEnerP3'] + $registro_factura['ImporteEnerP4'] + $registro_factura['ImporteEnerP5'] + $registro_factura['ImporteEnerP6'];
$ImporteEnergiaAcceso = $registro_factura['ImporteEnerAccesoP1'] + $registro_factura['ImporteEnerAccesoP2'] + $registro_factura['ImporteEnerAccesoP3'] + $registro_factura['ImporteEnerAccesoP4'] + $registro_factura['ImporteEnerAccesoP5'] + $registro_factura['ImporteEnerAccesoP6'];
$ImporteEnergiaReac = $importeReacP1 + $importeReacP2 + $importeReacP3 + $importeReacP4 + $importeReacP5 + $importeReacP6;
$ImporteTotMax = $registro_factura['ImporteMaxP1'] + $registro_factura['ImporteMaxP2'] + $registro_factura['ImporteMaxP3'] + $registro_factura['ImporteMaxP4'] + $registro_factura['ImporteMaxP5'] + $registro_factura['ImporteMaxP6'];
if($ImporteEnergiaReac==0){$TextoEnergiaReac='';}else{$TextoEnergiaReac = utf8_decode('Por energía reactiva');};
if($ImporteEnergiaAcceso==0){$TextoEnergiaAcceso='';}else{$TextoEnergiaAcceso = utf8_decode('Por energía BOE');};
if($ImporteEnergiaReac==0){$ImporteEnergiaReac='';}else{$ImporteEnergiaReac = number_format($ImporteEnergiaReac, 2, ',', '.').' '.$euro;};
if($ImporteEnergiaAcceso==0){$ImporteEnergiaAcceso='';}else{$ImporteEnergiaAcceso = number_format($ImporteEnergiaAcceso, 2, ',', '.').' '.$euro;};
if($registro_factura['ImporteLinea']==0){$TextoEnergiaLinea='';}else{$TextoEnergiaLinea = utf8_decode('Lineas adicionales');};
if($registro_factura['ImporteLinea']==0){$ImporteLinea='';}else{$ImporteLinea = number_format($registro_factura['ImporteLinea'], 2, ',', '.').' '.$euro;};
if($ImporteTotMax==0){$TextoTotMax='';}else{$TextoTotMax = utf8_decode('Excesos de potencia');};
if($ImporteTotMax==0){$ImporteTotMax='';}else{$ImporteTotMax = number_format($ImporteTotMax, 2, ',', '.').' '.$euro;};
if($registro_factura['IndexadoImporte']==0){$IndexadoTexto='';}else{$IndexadoTexto = utf8_decode($registro_factura['IndexadoTexto']);};
if($registro_factura['IndexadoImporte']==0){$IndexadoImporte='';}else{$IndexadoImporte = number_format($registro_factura['IndexadoImporte'], 2, ',', '.').' '.$euro;};

$pdf->CreaCelda(144 -2,27 -8,25,$importe_total.' '.$euro,255,255,255,'Arial','b',9);
$pdf->CreaCelda(124 -2,31 -8,25,$num_factura,255,255,255,'Arial','',7);
$pdf->CreaCelda(136 -2,35 -8,25,$periodo_factura,255,255,255,'Arial','',7);
$pdf->CreaCelda(152 -2,38.5 -8,25,$fechavencimientoFor,255,255,255,'Arial','',7);
$pdf->CreaCelda(112 -2,60 -8,25,$nombre_titular,255,255,255,'Arial','B',7);
$pdf->CreaCelda(112 -2,64 -8,25,$direccion_titular,255,255,255,'Arial','B',7);
$pdf->CreaCelda(112 -2,68 -8,25,$direccion_titular2,255,255,255,'Arial','B',7);
$pdf->CreaCelda(19 -2,57.5 -11,6,'Por potencia contratada',255,255,255,'Arial','',7);
$pdf->CreaCeldaAlDer(86.5 +2,57.5 -11,6,number_format($ImportePotencia, 2, ',', '.').' '.$euro,255,255,255,'Arial','',7);
$pdf->CreaCelda(19 -2,60.5 -11,6,utf8_decode('Por energía consumida'),255,255,255,'Arial','',7);
$pdf->CreaCeldaAlDer(86.5 +2,60.5 -11,6,number_format($ImporteEnergia, 2, ',', '.').' '.$euro,255,255,255,'Arial','',7);
$pdf->CreaCelda(19 -2,63.5 -11,6,$TextoEnergiaReac,255,255,255,'Arial','',7);
$pdf->CreaCeldaAlDer(86.5 +2,63.5 -11,6,$ImporteEnergiaReac,255,255,255,'Arial','',7);
if(($registro_factura['ptspotencia']==0) or ($registro_factura['ptspotencia']==1 and (($registro_factura['TextoEnerAccesoP1']=="") and ($registro_factura['TextoEnerAccesoP2']=="") and ($registro_factura['TextoEnerAccesoP3']=="") and ($registro_factura['TextoEnerAccesoP4']=="") and ($registro_factura['TextoEnerAccesoP5']=="") and ($registro_factura['TextoEnerAccesoP6']=="")))){} else {
$pdf->CreaCelda(19 -2,66.5 -11,6,$TextoEnergiaAcceso,255,255,255,'Arial','',7);
$pdf->CreaCeldaAlDer(86.5 +2,66.5 -11,6,$ImporteEnergiaAcceso,255,255,255,'Arial','',7);
}
$pdf->CreaCelda(19 -2,72.5 -11,6,$TextoEnergiaLinea,255,255,255,'Arial','',7);
$pdf->CreaCeldaAlDer(86.5 +2,72.5 -11,6,$ImporteLinea,255,255,255,'Arial','',7);
$pdf->CreaCelda(19 -2,69.5 -11,6,$TextoTotMax,255,255,255,'Arial','',7);
$pdf->CreaCeldaAlDer(86.5 +2,69.5 -11,6,$ImporteTotMax,255,255,255,'Arial','',7);
$pdf->CreaCelda(19 -2,75.5 -11,6,$IndexadoTexto,255,255,255,'Arial','',7);
$pdf->CreaCeldaAlDer(86.5 +2,75.5 -11,6,$IndexadoImporte.'',255,255,255,'Arial','',7);;


$pdf->CreaCelda(19 -2,72.5 -5,6,utf8_decode('Impuesto electricidad'),255,255,255,'Arial','',7);
$pdf->CreaCeldaAlDer(86.5 +2,72.5 -5,6,number_format($registro_factura['CuotaIE'], 2, ',', '.').' '.$euro,255,255,255,'Arial','',7);
$pdf->CreaCelda(19 -2,75.5 -5,6,utf8_decode('Alquiler de equipos de medida y control'),255,255,255,'Arial','',7);
$pdf->CreaCeldaAlDer(86.5 +2,75.5 -5,6,number_format($registro_factura['talqreac'], 2, ',', '.').' '.$euro,255,255,255,'Arial','',7);
$pdf->CreaCelda(19 -2,79 -5,6,utf8_decode('Impuesto aplicado'),255,255,255,'Arial','',7);
$pdf->CreaCeldaAlDer(49 +2,79 -5,6,$registro_factura['IvaGrafico'].' %.',255,255,255,'Arial','',7);
$pdf->CreaCeldaAlDer(86.5 +2,79 -5,6,number_format($registro_factura['importeiva'], 2, ',', '.').' '.$euro,255,255,255,'Arial','',7);
$pdf->CreaCelda(19 -2,84.5 -5,6,utf8_decode('TOTAL IMPORTE FACTURA'),255,255,255,'Arial','B',7);
$pdf->CreaCeldaAlDer(86 +2,84.5 -5,6,number_format($registro_factura['importe'], 2, ',', '.').' '.$euro,255,255,255,'Arial','',7);

/* DATOS DE SUMINISTRO */
/*$pdf->CreaCelda(47,82,25,$nombre_titular,255,255,255,'Arial','',7);
$pdf->CreaCelda(47,86.5,25,$direccion_titular,255,255,255,'Arial','',7);
$pdf->CreaCelda(47,91.5,25,$titularCP.'   '.$titularPob,255,255,255,'Arial','',7);
$pdf->CreaCelda(47,96.5,25,$dni_titular,255,255,255,'Arial','',7);
$pdf->CreaCelda(47,101.5,25,$tarifa,255,255,255,'Arial','',7);*/
$pdf->CreaCelda(19 -2,159 ,20,'Titular:'.'   '.$nombre_titular,255,255,255,'Arial','',6);
//$pdf->CreaCelda(32 -2,159 ,20,$nombre_titular,255,255,255,'Arial','',6);
$pdf->CreaCelda(92.5 -2,159 ,20,'Nif:'.'   '.$dni_titular,255,255,255,'Arial','',6);
//$pdf->CreaCelda(105.5 -2,159 ,20,$dni_titular,255,255,255,'Arial','',6);
//$pdf->CreaCelda(144 -2,159 -8,20,$_SESSION['numeroCliente'],255,255,255,'Arial','',6);
$pdf->CreaCelda(19 -2,162 ,20,utf8_decode('Dirección de suministro:').'   '.$direccion_suministro.' '.$direccion_titular2,255,255,255,'Arial','',6);
//$pdf->CreaCelda(48 -2,161.5 ,20,$direccion_suministro.' '.$direccion_titular2,255,255,255,'Arial','',6);

$pdf->CreaCelda(19 -2,165 ,20,utf8_decode('TIPO DE CONTRATO: Discriminación horaria de ').$registro_factura['textodisriminador'].' periodos.',255,255,255,'Arial','B',6);

//$pdf->CreaCelda(179,156.5,15,$vencimientoContratoFor,255,255,255,'Arial','',6);
//telegestion
$pdf->CreaCelda(19 -2,168 ,20,utf8_decode('TIPO DE CONTADOR:'),255,255,255,'Arial','',6);
if ($telegestion == 'S'){
	$pdf->CreaCelda(44 -2,168 ,15,'Telegestion',255,255,255,'Arial','',6);
	$facturacion = utf8_decode('Facturación por consumo real horario.');
}else if($telegestion == 'N'){
	$pdf->CreaCelda(44 -2,168 ,15,'No Telegestion',255,255,255,'Arial','',6);
	$facturacion = utf8_decode('Facturación con tarifa promedio del periodo de facturación');
}else{
	$pdf->CreaCelda(44 -2,168 ,15,'Telegestion no operativa',255,255,255,'Arial','',6);
	$facturacion = utf8_decode('Facturación por consumo real horario.');
};
$pdf->CreaCelda(19 -2,171 ,20,$facturacion,255,255,255,'Arial','B',6);
//potencias
if($pot1==0 or $pot1 == NULL){$pot1='';}else{$pot1='P1 '.number_format($pot1,3).' ';};
if($pot2==0 or $pot2 == NULL){$pot2='';}else{$pot2='P2 '.number_format($pot2,3).' ';};
if($pot3==0 or $pot3 == NULL){$pot3='';}else{$pot3='P3 '.number_format($pot3,3).' ';};
if($pot4==0 or $pot4 == NULL){$pot4='';}else{$pot4='P4 '.number_format($pot4,3).' ';};
if($pot5==0 or $pot5 == NULL){$pot5='';}else{$pot5='P5 '.number_format($pot5,3).' ';};
if($pot6==0 or $pot6 == NULL){$pot6='';}else{$pot6='P6 '.number_format($pot6,3).' ';};
$pdf->CreaCelda(19 -2,174 ,20,utf8_decode('Peaje de acceso:').'   '.$registro_factura['nombretarifa'],255,255,255,'Arial','',6);
//$pdf->CreaCelda(42 -2,172 ,20,$registro_factura['nombretarifa'],255,255,255,'Arial','',6);
$pdf->CreaCelda(71 -2,174 ,20,utf8_decode('Potencia contratada:').'   '.$pot1.$pot2.$pot3.$pot4.$pot5.$pot6,255,255,255,'Arial','',6);
//$pdf->CreaCelda(88 -2,172 ,10,$pot1.$pot2.$pot3.$pot4.$pot5.$pot6,255,255,255,'Arial','',6);
$pdf->CreaCelda(19 -2,177 ,20,utf8_decode('Referencia del contrato de suministro').'   '.utf8_decode('ELECNOVA SIGLO XXI S.L.: ').$registro_factura['ReferenciaExt1'],255,255,255,'Arial','',6);
//$pdf->CreaCelda(60 -2,174.5 ,10,utf8_decode('ELECNOVA SIGLO XXI S.L.: ').$registro_factura['ReferenciaExt1'],255,255,255,'Arial','',6);
$pdf->CreaCelda(19 -2,180 ,20,utf8_decode('Referencia del contrato de acceso').'   '.$registro_factura['NombreEmpresaDistribuidora'].': '.$registro_factura['ReferenciaExt1'].' renovacion anual automatica',255,255,255,'Arial','',6);
//$pdf->CreaCelda(58 -2,177.5 ,10,$registro_factura['NombreEmpresaDistribuidora'].': '.$registro_factura['ReferenciaExt1'].' renovacion anual automatica',255,255,255,'Arial','',6);
//$vencimientoContratoFor=date("d/m/Y",strtotime($vencimientoContrato));
$pdf->CreaCelda(19 -2,183 ,20,utf8_decode('Fecha final contrato:').'   '.$fecha_partesV[2].' de '.$meses_nombres[number_format($fecha_partesV[1]-1)].' de '.$fecha_partesV[0],255,255,255,'Arial','',6);
//$pdf->CreaCelda(46 -2,180 ,10,$fecha_partesV[2].' de '.$meses_nombres[number_format($fecha_partesV[1]-1)].' de '.$fecha_partesV[0],255,255,255,'Arial','',6);
$pdf->CreaCelda(19 -2,186 ,20,utf8_decode('Fecha emisión contrato:').'   '.$fecha_partes[2].' de '.$meses_nombres[number_format($fecha_partes[1]-1)].' de '.$fecha_partes[0],255,255,255,'Arial','',6);
//$pdf->CreaCelda(46 -2,182.5 ,25,$fecha_partes[2].' de '.$meses_nombres[number_format($fecha_partes[1]-1)].' de '.$fecha_partes[0],255,255,255,'Arial','',6);
$pdf->CreaCelda(19 -2,189 ,20,utf8_decode('Código unificado de punto de suministro (CUPS):').'   '.$cups,255,255,255,'Arial','B',6);
//$pdf->CreaCelda(72 -2,186.5 ,25,$cups,255,255,255,'Arial','',6);
$pdf->CreaCelda(19 -2,192 ,20,utf8_decode('Atención al cliente').'   '.utf8_decode('ELECNOVA SIGLO XXI S.L.: 900 525 662(Gratuito) '),255,255,255,'Arial','B',6);
//$pdf->CreaCelda(46 -2,189.5 ,10,utf8_decode('ELECNOVA SIGLO XXI S.L.: 900 373 136(Gratuito) '),255,255,255,'Arial','',6);
$pdf->CreaCelda(103 -2,192 ,20,utf8_decode('Reclamaciones:    900 525 662         info@elecnova.com'),255,255,255,'Arial','B',6);
//$pdf->CreaCelda(130 -2,189.5 ,10,'900 373 136 info@nobe.es',255,255,255,'Arial','',6);
$pdf->CreaCelda(19 -2,195 ,20,utf8_decode('Averias y Urgencias').'   '.'('.$registro_factura['NombreEmpresaDistribuidora'].'): '.$registro_factura['TelefonoAverias'],255,255,255,'Arial','B',6);
$pdf->SeccionBodyBG(19 -2,202,170,$registro_factura['textolibre'],2,255,255,255);
//$pdf->CreaCelda(46 -2,192.5 ,10,'('.$registro_factura['NombreEmpresaDistribuidora'].'): '.$registro_factura['TelefonoAverias'],255,255,255,'Arial','',6);
//$pdf->CreaCelda(25.5,195.5,10,$registro_factura['textolibre'],255,255,255,'Arial','',6);


//$pdf->CreaCelda(46,86.5,20,$registro_factura['contadoractiva'],255,255,255,'Arial','',8);


//Consumo Electrico
$lecturaP1P4ant = $lecturaP1ant + $lecturaP4ant;
$lecturaP2P5ant = $lecturaP2ant + $lecturaP5ant;
$lecturaP3P6ant = $lecturaP3ant + $lecturaP6ant;
$lecturaP1P4act = $lecturaP1act + $lecturaP4act;
$lecturaP2P5act = $lecturaP2act + $lecturaP5act;
$lecturaP3P6act = $lecturaP3act + $lecturaP6act;
$ConsumoP1P4 = $ConsumoP1 + $ConsumoP4;
$ConsumoP2P5 = $ConsumoP2 + $ConsumoP5;
$ConsumoP3P6 = $ConsumoP3 + $ConsumoP6;

if($registro_factura['acfactura'] != 1){

$pdf->CreaCelda(37 -1.5,106.5 -8,5,'Consumo en el',255,255,255,'Arial','',6);
$pdf->CreaCelda(38 -1.5,109 -8,5,'periodo '.$registro_factura['TextP1'],255,255,255,'Arial','',6);
$pdf->CreaCelda(37 -1.5,112.5 -8,5,'00:00h - 24:00h',255,255,255,'Arial','',6);
}
if($registro_factura['acfactura'] != 2){
$pdf->CreaCelda(55 -0.5,106.5 -8,5,'Consumo en el',255,255,255,'Arial','',6);
$pdf->CreaCelda(56 -0.5,109 -8,5,'periodo '.$registro_factura['TextP2'],255,255,255,'Arial','',6);
$pdf->CreaCelda(55 -0.5,112.5 -8,5,'00:00h - 24:00h',255,255,255,'Arial','',6);
		}
if($registro_factura['acfactura'] != 1){
$pdf->CreaCelda(75 ,106.5 -8,5,'Consumo en el',255,255,255,'Arial','',6);
$pdf->CreaCelda(76 ,109 -8,5,'periodo '.$registro_factura['TextP3'],255,255,255,'Arial','',6);
$pdf->CreaCelda(75 ,112.5 -8,5,'00:00h - 24:00h',255,255,255,'Arial','',6);
}
$pdf->CreaCelda(19 -2,117.5 -8,5,'Lectura anterior',255,255,255,'Arial','',6);
$pdf->CreaCelda(19 -2,122.5 -8,5,$registro_factura['SeñalAnterior'],255,255,255,'Arial','',6);
if ($lecturaP1P4ant==0){}else{
$pdf->CreaCeldaAlDer3(42.5 +2,119.5 -8,5,number_format($lecturaP1P4ant,3,",","."),255,255,255,'Arial','',6);
}
if ($lecturaP2P5ant==0){}else{
$pdf->CreaCeldaAlDer3(62.5 +2,119.5 -8,5,number_format($lecturaP2P5ant,3,",","."),255,255,255,'Arial','',6);
}
if ($lecturaP3P6ant==0){}else{
$pdf->CreaCeldaAlDer3(82.5 +2,119.5 -8,5,number_format($lecturaP3P6ant,3,",","."),255,255,255,'Arial','',6);
}
/*$pdf->CreaCelda(164,101,5,number_format($p4,3,",","."),255,255,255,'Arial','',6);
$pdf->CreaCelda(176,101,5,number_format($p5,3,",","."),255,255,255,'Arial','',6);
$pdf->CreaCelda(188,101,5,number_format($p6,3,",","."),255,255,255,'Arial','',6);*/
$fechalecturaant = explode('-',$fechalecturaant[0]);
$pdf->CreaCelda(19 -2,124 -8,5,$fechalecturaant[2].'-'.$fechalecturaant[1].'-'.$fechalecturaant[0],255,255,255,'Arial','',7);
$pdf->CreaCelda(19 -2,127 -8,5,'Lectura actual',255,255,255,'Arial','',6);
$pdf->CreaCelda(19 -2,130 -8,5,$registro_factura['SeñalActual'],255,255,255,'Arial','',6);
if ($lecturaP1P4act==0){}else{
$pdf->CreaCeldaAlDer3(42.5 +2,129 -8,5,number_format($lecturaP1P4act,3,",","."),255,255,255,'Arial','',6);
}
if ($lecturaP2P5act==0){}else{
$pdf->CreaCeldaAlDer3(62.5 +2,129 -8,5,number_format($lecturaP2P5act,3,",","."),255,255,255,'Arial','',6);
}
if ($lecturaP3P6act==0){}else{
$pdf->CreaCeldaAlDer3(82.5 +2,129 -8,5,number_format($lecturaP3P6act,3,",","."),255,255,255,'Arial','',6);
}
$fechalectura= explode('-',$fechalectura[0]);
$pdf->CreaCelda(19 -2,131.5-8,5,$fechalectura[2].'-'.$fechalectura[1].'-'.$fechalectura[0],255,255,255,'Arial','',7);
$pdf->CreaCelda(17 ,135.5 -8,5,'Consumo en el',255,255,255,'Arial','',6);
$pdf->CreaCelda(17 ,138 -8,5,'periodo',255,255,255,'Arial','',6);
if ($ConsumoP1P4==0){}else{
$pdf->CreaCeldaAlDer3(42.5 +2,136.5 -8,5,number_format($ConsumoP1P4,0,",","."),255,255,255,'Arial','',6);
}
if ($ConsumoP2P5==0){}else{
$pdf->CreaCeldaAlDer3(62.5 +2,136.5 -8,5,number_format($ConsumoP2P5,0,",","."),255,255,255,'Arial','',6);
}
if ($ConsumoP3P6==0){}else{
$pdf->CreaCeldaAlDer3(82.5 +2,136.5 -8,5,number_format($ConsumoP3P6,0,",","."),255,255,255,'Arial','',6);
}

$pdf->CreaCelda(19 -2,148 -8,5,utf8_decode('Maxímetro'),255,255,255,'Arial','',6);
if (($registro_factura['LecturaMaxP1']==0) or ($registro_factura['LecturaMaxP1']<$registro_factura['LecturaMaxP4'])){}else{$pdf->CreaCeldaAlDer3(42.5 +2,148 -8,5,number_format($registro_factura['LecturaMaxP1'],0,",","."),255,255,255,'Arial','',6);};
if (($registro_factura['LecturaMaxP4']==0) or ($registro_factura['LecturaMaxP1']>$registro_factura['LecturaMaxP4'])){}else{$pdf->CreaCeldaAlDer3(42.5 +2,148 -8,5,number_format($registro_factura['LecturaMaxP4'],0,",","."),255,255,255,'Arial','',6);};
if (($registro_factura['LecturaMaxP2']==0) or ($registro_factura['LecturaMaxP2']<$registro_factura['LecturaMaxP5'])){}else{$pdf->CreaCeldaAlDer3(62.5 +2,148 -8,5,number_format($registro_factura['LecturaMaxP2'],0,",","."),255,255,255,'Arial','',6);};
if (($registro_factura['LecturaMaxP5']==0) or ($registro_factura['LecturaMaxP2']>$registro_factura['LecturaMaxP5'])){}else{$pdf->CreaCeldaAlDer3(62.5 +2,148 -8,5,number_format($registro_factura['LecturaMaxP5'],0,",","."),255,255,255,'Arial','',6);};
if (($registro_factura['LecturaMaxP3']==0) or ($registro_factura['LecturaMaxP3']<$registro_factura['LecturaMaxP6'])){}else{$pdf->CreaCeldaAlDer3(82.5 +2,148 -8,5,number_format($registro_factura['LecturaMaxP3'],0,",","."),255,255,255,'Arial','',6);};
if (($registro_factura['LecturaMaxP6']==0) or ($registro_factura['LecturaMaxP3']>$registro_factura['LecturaMaxP6'])){}else{$pdf->CreaCeldaAlDer3(82.5 +2,148 -8,5,number_format($registro_factura['LecturaMaxP6'],0,",","."),255,255,255,'Arial','',6);};
//$pdf->Image('fpdf/bg2.jpg', 7, 110, 197);
$pdf->Image("nameGraph2.png",100 -2,102 -4);
//HISTORIAL
/*$pdf->CreaCelda(126,107.5,25,$mes1,255,255,255,'Arial','',6);
$pdf->CreaCelda(126,110,25,$mes2,255,255,255,'Arial','',6);
$pdf->CreaCelda(126,112.5,25,$mes3,255,255,255,'Arial','',6);
$pdf->CreaCelda(126,115,25,$mes4,255,255,255,'Arial','',6);
$pdf->CreaCelda(126,117.5,25,$mes5,255,255,255,'Arial','',6);
$pdf->CreaCelda(126,120,25,$mes6,255,255,255,'Arial','',6);
$pdf->CreaCelda(126,122.5,25,$mes7,255,255,255,'Arial','',6);
$pdf->CreaCelda(126,125,25,$mes8,255,255,255,'Arial','',6);
$pdf->CreaCelda(126,127.5,25,$mes9,255,255,255,'Arial','',6);
$pdf->CreaCelda(126,130,25,$mes10,255,255,255,'Arial','',6);
$pdf->CreaCelda(126,132.5,25,$mes11,255,255,255,'Arial','',6);
$pdf->CreaCelda(126,135,25,$mes12,255,255,255,'Arial','',6);
$pdf->CreaCelda(126,137.5,25,$mes13,255,255,255,'Arial','',6);
$pdf->CreaCelda(126,140.5,25,$mes14,255,255,255,'Arial','',6);

$pdf->CreaCelda(140,107.5,25,$anio1,255,255,255,'Arial','',6);
$pdf->CreaCelda(140,110.5,25,$anio2,255,255,255,'Arial','',6);
$pdf->CreaCelda(140,112.5,25,$anio3,255,255,255,'Arial','',6);
$pdf->CreaCelda(140,115,25,$anio4,255,255,255,'Arial','',6);
$pdf->CreaCelda(140,117.5,25,$anio5,255,255,255,'Arial','',6);
$pdf->CreaCelda(140,120,25,$anio6,255,255,255,'Arial','',6);
$pdf->CreaCelda(140,122.5,25,$anio7,255,255,255,'Arial','',6);
$pdf->CreaCelda(140,125,25,$anio8,255,255,255,'Arial','',6);
$pdf->CreaCelda(140,127.5,25,$anio9,255,255,255,'Arial','',6);
$pdf->CreaCelda(140,130,25,$anio10,255,255,255,'Arial','',6);
$pdf->CreaCelda(140,132.5,25,$anio11,255,255,255,'Arial','',6);
$pdf->CreaCelda(140,135,25,$anio12,255,255,255,'Arial','',6);
$pdf->CreaCelda(140,137.5,25,$anio13,255,255,255,'Arial','',6);
$pdf->CreaCelda(140,140,25,$anio14,255,255,255,'Arial','',6);

$pdf->CreaCelda(160,107.5,18,$con1,255,255,255,'Arial','',6);
$pdf->CreaCelda(160,110,18,$con2,255,255,255,'Arial','',6);

$pdf->CreaCelda(160,112.5,18,$con3,255,255,255,'Arial','',6);
$pdf->CreaCelda(160,115,18,$con4,255,255,255,'Arial','',6);
$pdf->CreaCelda(160,117.5,18,$con5,255,255,255,'Arial','',6);
$pdf->CreaCelda(160,120,18,$con6,255,255,255,'Arial','',6);
$pdf->CreaCelda(160,122.5,18,$con7,255,255,255,'Arial','',6);
$pdf->CreaCelda(160,125,18,$con8,255,255,255,'Arial','',6);
$pdf->CreaCelda(160,127.5,18,$con9,255,255,255,'Arial','',6);
$pdf->CreaCelda(160,130,18,$con10,255,255,255,'Arial','',6);
$pdf->CreaCelda(160,132.5,18,$con11,255,255,255,'Arial','',6);
$pdf->CreaCelda(160,135,18,$con12,255,255,255,'Arial','',6);
$pdf->CreaCelda(160,137.5,18,$con13,255,255,255,'Arial','',6);
$pdf->CreaCelda(160,140,18,$con14,255,255,255,'Arial','',6);*/

//medias
$pdf->CreaCelda(105 +5,146 -8,18,'Su consumo medio diario en el periodo facturado ha sido de',255,255,255,'Arial','',5);
$pdf->CreaCeldaAlDer3(155 +5,146 -8,18,number_format($media,2,",","."). ' kWh',255,255,255,'Arial','',5);
$pdf->CreaCelda(105 +5,148.5 -8,18,utf8_decode('Su consumo medio diario en los últimos 14 meses ha sido de'),255,255,255,'Arial','',5);
$pdf->CreaCeldaAlDer3(155 +5,148.5 -8,18,number_format($media_diaria,2,",","."). ' kWh',255,255,255,'Arial','',5);
$pdf->CreaCelda(105 +5,151 -8,18,utf8_decode('Su consumo acumulado del último año ha sido de'),255,255,255,'Arial','',5);
$pdf->CreaCeldaAlDer3(155 +5,151 -8,18,number_format($consumoAnual,2,",","."). ' kWh',255,255,255,'Arial','',5);

/* LECTURAS */
$fechalecturaant = explode('-',$fechalecturaant[0]);
$fechalectura = explode('-',$fechalectura[0]);
//$pdf->Image('fpdf/bg3.jpg', 7, 222, 197);

/*
$pdf->CreaCeldaAlDer3(45,249,10,'Anterior',255,255,255,'Arial','B',8);
$pdf->CreaCeldaAlDer(45,252.5,10,$fechalecturaant[2].'/'.$fechalecturaant[1].'/'.$fechalecturaant[0],255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(45,256.5,10,$lecturaP1ant,255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(45,259.5,10,$lecturaP2ant,255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(45,262.5,10,$lecturaP3ant,255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(45,266,10,$lecturaP4ant,255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(45,269,10,$lecturaP5ant,255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(45,272,10,$lecturaP6ant,255,255,255,'Arial','',8);

$pdf->CreaCeldaAlDer3(60,249,10,'Actual',255,255,255,'Arial','B',8);
$pdf->CreaCeldaAlDer(60,252.5,10,$fechalectura[2].'/'.$fechalectura[1].'/'.$fechalectura[0],255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(60,256.5,10,$lecturaP1act,255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(60,259.5,10,$lecturaP2act,255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(60,262.5,10,$lecturaP3act,255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(60,266.5,10,$lecturaP4act,255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(60,269,10,$lecturaP5act,255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(60,272,10,$lecturaP6act,255,255,255,'Arial','',8);

$pdf->CreaCeldaAlDer3(75,249,10,'Consumo',255,255,255,'Arial','B',8);
$pdf->CreaCeldaAlDer(75,252.5,10,'',255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(75,256.5,10,$ConsumoP1,255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(75,259.5,10,$ConsumoP2,255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(75,262.5,10,$ConsumoP3,255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(75,266,10,$ConsumoP4,255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(75,269,10,$ConsumoP5,255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(75,272,10,$ConsumoP6,255,255,255,'Arial','',8);

$pdf->CreaCeldaAlDer3(95,249,10,'Anterior',255,255,255,'Arial','B',8);
$pdf->CreaCeldaAlDer(95,252.5,10,$fechalecturaant[2].'/'.$fechalecturaant[1].'/'.$fechalecturaant[0],255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(95,256.5,10,$lecturaReacP1ant,255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(95,259.5,10,$lecturaReacP2ant,255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(95,262.5,10,$lecturaReacP3ant,255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(95,266,10,$lecturaReacP4ant,255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(95,269,10,$lecturaReacP5ant,255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(95,272,10,$lecturaReacP6ant,255,255,255,'Arial','',8);

$pdf->CreaCeldaAlDer3(110,249,10,'Actual',255,255,255,'Arial','B',8);
$pdf->CreaCeldaAlDer(110,252.5,10,$fechalectura[2].'/'.$fechalectura[1].'/'.$fechalectura[0],255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(110,256.5,10,$lecturaReacP1act,255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(110,259.5,10,$lecturaReacP2act,255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(110,262.5,10,$lecturaReacP3act,255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(110,266,10,$lecturaReacP4act,255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(110,269,10,$lecturaReacP5act,255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(110,272,10,$lecturaReacP6act,255,255,255,'Arial','',8);

$pdf->CreaCeldaAlDer3(125,249,10,'Consumo',255,255,255,'Arial','B',8);
$pdf->CreaCeldaAlDer(125,252.5,10,'',255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(125,256.5,10,$ConsumoReacP1,255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(125,259.5,10,$ConsumoReacP2,255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(125,262.5,10,$ConsumoReacP3,255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(125,266,10,$ConsumoReacP4,255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(125,269,10,$ConsumoReacP5,255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(125,272,10,$ConsumoReacP6,255,255,255,'Arial','',8);


$pdf->CreaCeldaAlDer(140,252.5,10,$señal_actual,255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(140,256.5,10,$lecturaMaxP1,255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(140,259.5,10,$lecturaMaxP2,255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(140,262.5,10,$lecturaMaxP3,255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(140,266,10,$lecturaMaxP4,255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(140,269,10,$lecturaMaxP5,255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(140,272,10,$lecturaMaxP6,255,255,255,'Arial','',8);
*/


/*
if($registro_factura['ImporteAdicional']==0){} else {
	$pdf->CreaCelda(23,204,40,$registro_factura['ConceptoAdicional'],255,255,255,'Arial','',8);
	$pdf->CreaCeldaAlDer(118,204,6,$registro_factura['ImporteAdicional'],255,255,255,'Arial','',8);
}

if ($boe2012consumo42011==0 and $boe2012importe42011 == 0 and $boe2012texto42011=="") {} else {
	$pdf->CreaCelda(23,210,40,$boe2012texto42011,255,255,255, 'Arial','',8);
	$pdf->CreaCeldaAlDer(185,210,6,$boe2012importe42011,255,255,255);
	$posy+=3;
}

if ($boe2012consumo12012==0 and $boe2012importe12012 == 0 and $boe2012texto12012=="") {} else {
	$pdf->CreaCelda(23,210,40,$boe2012texto12012,255,255,255, 'Arial','',8);
	$pdf->CreaCeldaAlDer(185,210,6,$boe2012importe12012,255,255,255);
	$posy+=3;
}

if ($boe2012consumototal==0 and $boe2012importetotal == 0 and $boe2012textototal=="") {} else {
	$pdf->CreaCelda(23,$posy,80,$boe2012textototal,255,255,255, 'Arial','',8);
	$pdf->CreaCeldaAlDer(185,$posy,7,$boe2012importetotal,255,255,255);
	$posy+=3;
}
*/

//banco
$pdf->CreaCelda(19 -2,215.5 ,25,'Forma de Pago:',255,255,255,'Arial','',6);
$pdf->CreaCelda(43 -2,215.5 ,25,$forma_pago,255,255,255,'Arial','',6);
$pdf->CreaCelda(19 -2,218 ,25,'Entidad Bancaria:',255,255,255,'Arial','',6);
$pdf->CreaCelda(43 -2,218 ,35,$banco,255,255,255,'Arial','',6);
$pdf->CreaCelda(19 -2,220.5 ,25,utf8_decode('Nº de cuenta:'),255,255,255,'Arial','',6);
$pdf->CreaCelda(43 -2,220.5 ,25,$num_cuenta_banco.''.$num_cuenta_sucursal.''.$num_cuenta_digito_control.''.substr($num_cuenta_cuenta,1,5).'*****',255,255,255,'Arial','',6);
$fechavencimientoFor=date("d/m/Y",strtotime($fechavencimiento));

//codbarras Extra
$pdf->CreaCelda(104 -2,215.5 ,10,'Emisora',255,255,255,'Arial','',6);
$pdf->CreaCelda(101 -2,219.5 ,15,$registro_factura['NIFEmisora']. ' - '.$registro_factura['Sufijo'],255,255,255,'Arial','',6);
$pdf->CreaCelda(121 -2,215.5 ,10,'Referencia',255,255,255,'Arial','',6);
$pdf->CreaCelda(118 -2,219.5 ,15,$registro_factura['Referencia'],255,255,255,'Arial','',6);
$pdf->CreaCelda(138 -2,215.5 ,10,utf8_decode('Identificación'),255,255,255,'Arial','',6);
$pdf->CreaCelda(138 -2,219.5 ,15,$registro_factura['Identi'],255,255,255,'Arial','',6);
$pdf->CreaCelda(155 -2,215.5 ,15,utf8_decode('Fecha Límite'),255,255,255,'Arial','',6);
$pdf->CreaCelda(155 -2,219.5 ,15,$fecha_partes[2].'/'.$fecha_partes[1].'/'.$fecha_partes[0],255,255,255,'Arial','',6);
$pdf->CreaCelda(173.5 -2,215.5 ,10,'Importe',255,255,255,'Arial','',6);
$pdf->CreaCelda(173.5 -2,219.5 ,10,$importe_total .' '. $euro,255,255,255,'Arial','',6);

//Importe de la factura
$pdf->CreaCelda(19 -2.5,237.5 +2,10,'El destino del importe de su factura, '. $importe_total .' '. $euro.', es el siguiente:',255,255,255,'Arial','',7);
$pdf->Image("nameGraph.png",28 -2,244 +4);
$pdf->CreaCelda(40 -2,245 +4,20,'  ',255,255,255,'Arial','',15);
$pdf->CreaCelda(64.5 -2,261 +4,9,$impuestosAplicados .' '. $euro,243,129,185,'Arial','',5.5);
$pdf->CreaCelda(64-2,255.8 +4,9,$costeProduccion .' '. $euro,97,169,243,'Arial','',5.5);
$pdf->CreaCelda(59.5 -2,266 +4,9,$costesRegulados .' '. $euro,97,227,169,'Arial','',5.5);
$esquema1 = ($costesRegulados * 36.28) / 100;
$esquema2 = ($costesRegulados * 32.12) / 100;
$esquema3 = ($costesRegulados * 31.60) / 100;
$pdf->Image("fpdf/esquema.png",102 -2,245  +4,39.5);
$pdf->CreaCelda(114 -2,249 +4,8,number_format($esquema1,2,",",".") .' '. $euro,138,201,135,'Arial','',6);
$pdf->CreaCelda(125 -2,247.5 +4,8,'Incentivos a las energias renovables, ',255,255,255,'Arial','B',6);
$pdf->CreaCelda(125 -2,250 +4,8,'cogeneracion de residuos ',255,255,255,'Arial','B',6);
$pdf->CreaCelda(114 -2,256 +4,8,number_format($esquema2,2,",",".") .' '. $euro,75,167,103,'Arial','',6);
$pdf->CreaCelda(125 -2,255.5 +4,8,'Coste de redes de transporte y distribucion ',255,255,255,'Arial','B',6);
$pdf->CreaCelda(114 -2,263 +4,8,number_format($esquema3,2,",",".") .' '. $euro,52,135,77,'Arial','',6);
$pdf->CreaCelda(125.5 -2,261 +4,8,'Otros costes regulados ',255,255,255,'Arial','B',6);
$pdf->CreaCelda(149 -2,261 +4,8,'(incluida la anualidad ',255,255,255,'Arial','',6);
$pdf->CreaCelda(125 -2,263.5 +4,8,'del deficit) ',255,255,255,'Arial','',6);

$pdf->CreaCelda(19 -2,276.5,10,utf8_decode('A los importes indicados en el diagrama debe añadirse, en su caso, el importe del alquiler de los equipos de medida y control:') . number_format($alquiler,2,",",".") .' '. $euro,255,255,255,'Arial','',6);

//$pdf->Image("$graph",66,241,400,250);
/* BASE IMPONIBLE... */
/*$pdf->CreaCelda(117,226,11,'Base Imponible',255,255,255,'Arial','',8);
$pdf->SetDrawColor(0,0,0);
$pdf->Rect(140,225,22,5,'D');

$pdf->CreaCeldaAlDer3(151,226,10,number_format($base1,2,",","."),255,255,255,'Arial','B',7);
$pdf->CreaCeldaAlDer3(168,226,11,number_format($iva1,2).' %',255,255,255,'Arial','',7);
$pdf->CreaCeldaAlDer3(180,226,11,number_format($cuota1,2,",","."),255,255,255,'Arial','',7);
if ($base2==0 or $base2== ""){} else {
	$pdf->Rect(140,231,22,5,'D');
$pdf->CreaCeldaAlDer3(151,232,11,number_format($base2,2,",","."),255,255,255,'Arial','B',7);
$pdf->CreaCeldaAlDer3(168,232,11,number_format($iva2,2).' %',255,255,255,'Arial','',7);
$pdf->CreaCeldaAlDer3(180,232,11,number_format($cuota2,2,",","."),255,255,255,'Arial','',7);
}*/

/*$pdf->Rect(171.7,235.8,21.5,4.2,'D');
$pdf->SetFillColor(255,114,0);
$pdf->Rect(172,236,21,3.5,'F');
$pdf->CreaCelda(117,236.5,15,'TOTAL FACTURA',255,255,255,'Arial','B',9);
$pdf->CreaCelda(181,236.5,10,$importe_total,255,114,0,'Arial','B',9);

*/
// Siguiente pagina
$pdf->AddPage();
$pdf->Image('fpdf/back.png', 5, 8, 197);


//
/*$pdf->CreaCelda(117,48,25,$nombre_suministro,255,255,255,'Arial','B',9);
$pdf->CreaCelda(117,54,25,$direccion_suministro,255,255,255,'Arial','B',9);
$pdf->CreaCelda(117,60,25,$suministro_CodPos.'     '.$suministro_ciudad,255,255,255,'Arial','B',9);
*/
//Facturas 
/* CALCULO DE LA FACTURA */
$posy=24;
$lineas=0;

//$pdf->CreaCelda(28,$posy-4,40,'Termino de Potencia:',255,255,255,'Arial','B',8);

//Potencia P1
$pdf->CreaCelda(19,$posy,6,utf8_decode('Término de potencia B.O.E.'),255,255,255,'Arial','b',7);
$posy+=3;
if($importePotP1==0 or $textoPotP1==""){} else {
	$pdf->CreaCelda(19,$posy,70,$registro_factura['TextoPotP1'],255,255,255,'Arial','',6);
	$pdf->CreaCeldaAlDer3(174,$posy,6,number_format($registro_factura['ImportePotP1'], 2, ',', '.').$euro,255,255,255,'Arial','',6);
	
}


//Potencia P2
if($importePotP2==0 or $textoPotP2==""){} else {
	$lineas+=1; 
	$posy+=3;
	$pdf->CreaCelda(19,$posy,70,$registro_factura['TextoPotP2'],255,255,255,'Arial','',6);
	$pdf->CreaCeldaAlDer3(174,$posy,6,number_format($registro_factura['ImportePotP2'], 2, ',', '.').$euro,255,255,255,'Arial','',6);
	
}


//Potencia P3
if($importePotP3==0 or $textoPotP3==""){} else {
	$lineas+=1; 
	$posy+=3;
	$pdf->CreaCelda(19,$posy,70,$registro_factura['TextoPotP3'],255,255,255,'Arial','',6);
	$pdf->CreaCeldaAlDer3(174,$posy,6,number_format($registro_factura['ImportePotP3'], 2, ',', '.').$euro,255,255,255,'Arial','',6);
	
}

	if($importePotP4==0 or $textoPotP4==""){} else {
		$lineas+=1; 
	$posy+=3;
	$pdf->CreaCelda(19,$posy,70,$registro_factura['TextoPotP4'],255,255,255,'Arial','',6);
	$pdf->CreaCeldaAlDer3(174,$posy,6,number_format($registro_factura['ImportePotP4'], 2, ',', '.').$euro,255,255,255,'Arial','',6);
	
}

	if($importePotP5==0 or $textoPotP5==""){} else {
		$lineas+=1; 
	$posy+=3;
	$pdf->CreaCelda(19,$posy,70,$registro_factura['TextoPotP5'],255,255,255,'Arial','',6);
	$pdf->CreaCeldaAlDer3(174,$posy,6,number_format($registro_factura['ImportePotP5'], 2, ',', '.').$euro,255,255,255,'Arial','',6);
	
}

	if($importePotP6==0 or $textoPotP6==""){} else {
		$lineas+=1; 
	$posy+=3;
	$pdf->CreaCelda(19,$posy,70,$registro_factura['TextoPotP6'],255,255,255,'Arial','',6);
	$pdf->CreaCeldaAlDer3(174,$posy,6,number_format($registro_factura['ImportePotP6'], 2, ',', '.').$euro,255,255,255,'Arial','',6);
	
}

		//ENERGIA ACCESO
if(($registro_factura['ptspotencia']==0) or ($registro_factura['ptspotencia']==1 and (($registro_factura['TextoEnerAccesoP1']=="") and ($registro_factura['TextoEnerAccesoP2']=="") and ($registro_factura['TextoEnerAccesoP3']=="") and ($registro_factura['TextoEnerAccesoP4']=="") and ($registro_factura['TextoEnerAccesoP5']=="") and ($registro_factura['TextoEnerAccesoP6']=="")))){} else {
	$lineas+=1; 
	$posy+=3;
	$lineas+=1; 
	$posy+=3;
	$pdf->CreaCelda(19,$posy,6,utf8_decode('Término de Energía(Peajes Regulado B.O.E.)'),255,255,255,'Arial','b',7);
$posy+=3;
	}
if(($registro_factura['ptspotencia']==0) or ($registro_factura['ptspotencia']==1 and ($registro_factura['ImporteEnerAccesoP1']==0) or ($registro_factura['TextoEnerAccesoP1']==""))){} else {
	$pdf->CreaCelda(19,$posy,70,$registro_factura['TextoEnerAccesoP1'],255,255,255,'Arial','',6);
	$pdf->CreaCeldaAlDer3(174,$posy,6,number_format($registro_factura['ImporteEnerAccesoP1'], 2, ',', '.').$euro,255,255,255,'Arial','',6);
	
}

	if(($registro_factura['ptspotencia']==0) or ($registro_factura['ptspotencia']==1 and ($registro_factura['ImporteEnerAccesoP2']==0) or ($registro_factura['TextoEnerAccesoP2']==""))){} else {
		$lineas+=1; 
	$posy+=3;
	$pdf->CreaCelda(19,$posy,70,$registro_factura['TextoEnerAccesoP2'],255,255,255,'Arial','',6);
	$pdf->CreaCeldaAlDer3(174,$posy,6,number_format($registro_factura['ImporteEnerAccesoP2'], 2, ',', '.').$euro,255,255,255,'Arial','',6);
	
}

	if(($registro_factura['ptspotencia']==0) or ($registro_factura['ptspotencia']==1 and ($registro_factura['ImporteEnerAccesoP3']==0) or ($registro_factura['TextoEnerAccesoP3']==""))){} else {
		$lineas+=1; 
	$posy+=3;
	$pdf->CreaCelda(19,$posy,70,$registro_factura['TextoEnerAccesoP3'],255,255,255,'Arial','',6);
	$pdf->CreaCeldaAlDer3(174,$posy,6,number_format($registro_factura['ImporteEnerAccesoP3'], 2, ',', '.').$euro,255,255,255,'Arial','',6);
	
}

	if(($registro_factura['ptspotencia']==0) or ($registro_factura['ptspotencia']==1 and ($registro_factura['ImporteEnerAccesoP4']==0) or ($registro_factura['TextoEnerAccesoP4']==""))){} else {
		$lineas+=1; 
	$posy+=3;
	$pdf->CreaCelda(19,$posy,70,$registro_factura['TextoEnerAccesoP4'],255,255,255,'Arial','',6);
	$pdf->CreaCeldaAlDer3(174,$posy,6,number_format($registro_factura['ImporteEnerAccesoP4'], 2, ',', '.').$euro,255,255,255,'Arial','',6);
	
}

	if(($registro_factura['ptspotencia']==0) or ($registro_factura['ptspotencia']==1 and ($registro_factura['ImporteEnerAccesoP5']==0) or ($registro_factura['TextoEnerAccesoP5']==""))){} else {
		$lineas+=1; 
	$posy+=3;
	$pdf->CreaCelda(19,$posy,70,$registro_factura['TextoEnerAccesoP5'],255,255,255,'Arial','',6);
	$pdf->CreaCeldaAlDer3(174,$posy,6,number_format($registro_factura['ImporteEnerAccesoP5'], 2, ',', '.').$euro,255,255,255,'Arial','',6);
	
}

	if(($registro_factura['ptspotencia']==0) or ($registro_factura['ptspotencia']==1 and ($registro_factura['ImporteEnerAccesoP6']==0) or ($registro_factura['TextoEnerAccesoP6']==""))){} else {
		$lineas+=1; 
	$posy+=3;
	$pdf->CreaCelda(19,$posy,70,$registro_factura['TextoEnerAccesoP6'],255,255,255,'Arial','',6);
	$pdf->CreaCeldaAlDer3(174,$posy,6,number_format($registro_factura['ImporteEnerAccesoP6'], 2, ',', '.').$euro,255,255,255,'Arial','',6);
	
}
$lineas+=1; 
	$posy+=3;
	$lineas+=1; 
	$posy+=3;
		//ENERGIA
	
	$pdf->CreaCelda(19,$posy,6,utf8_decode('Término de Energía'),255,255,255,'Arial','b',7);
$posy+=3;
	
if($registro_factura['ImporteEnerP1']==0){} else {
	$pdf->CreaCelda(19,$posy,70,$registro_factura['TextoEnerP1'],255,255,255,'Arial','',6);
	$pdf->CreaCeldaAlDer3(174,$posy,6,number_format($registro_factura['ImporteEnerP1'], 2, ',', '.').$euro,255,255,255,'Arial','',6);
	
}

	if($registro_factura['ImporteEnerP2']==0){} else {
		$lineas+=1; 
	$posy+=3;
	$pdf->CreaCelda(19,$posy,70,$registro_factura['TextoEnerP2'],255,255,255,'Arial','',6);
	$pdf->CreaCeldaAlDer3(174,$posy,6,number_format($registro_factura['ImporteEnerP2'], 2, ',', '.').$euro,255,255,255,'Arial','',6);
	
}

	if($registro_factura['ImporteEnerP3']==0){} else {
		$lineas+=1; 
	$posy+=3;
	$pdf->CreaCelda(19,$posy,70,$registro_factura['TextoEnerP3'],255,255,255,'Arial','',6);
	$pdf->CreaCeldaAlDer3(174,$posy,6,number_format($registro_factura['ImporteEnerP3'], 2, ',', '.').$euro,255,255,255,'Arial','',6);
	
}

	if($registro_factura['ImporteEnerP4']==0){} else {
		$lineas+=1; 
	$posy+=3;
	$pdf->CreaCelda(19,$posy,70,$registro_factura['TextoEnerP4'],255,255,255,'Arial','',6);
	$pdf->CreaCeldaAlDer3(174,$posy,6,number_format($registro_factura['ImporteEnerP4'], 2, ',', '.').$euro,255,255,255,'Arial','',6);
	
}

	if($registro_factura['ImporteEnerP5']==0){} else {
		$lineas+=1; 
	$posy+=3;
	$pdf->CreaCelda(19,$posy,70,$registro_factura['TextoEnerP5'],255,255,255,'Arial','',6);
	$pdf->CreaCeldaAlDer3(174,$posy,6,number_format($registro_factura['ImporteEnerP5'], 2, ',', '.').$euro,255,255,255,'Arial','',6);
	
}

	if($registro_factura['ImporteEnerP6']==0){} else {
		$lineas+=1; 
	$posy+=3;
	$pdf->CreaCelda(19,$posy,70,$registro_factura['TextoEnerP6'],255,255,255,'Arial','',6);
	$pdf->CreaCeldaAlDer3(174,$posy,6,number_format($registro_factura['ImporteEnerP6'], 2, ',', '.').$euro,255,255,255,'Arial','',6);
	
}
$lineas+=1; 
	$posy+=3;
	
	$lineas+=1; 
	$posy+=3;
	$lineas+=1; 
	$posy+=3;
	
		//ENERGIA REACTIVA
	if(($registro_factura['TextoReacP1']=="") and ($registro_factura['TextoReacP2']=="") and ($registro_factura['TextoReacP3']=="") and ($registro_factura['TextoReacP4']=="") and ($registro_factura['TextoReacP5']=="") and ($registro_factura['TextoReacP6']=="")){} else {
	$pdf->CreaCelda(19,$posy,6,utf8_decode('Complemento por reactiva'),255,255,255,'Arial','b',7);
$posy+=3;
	}
	
if($registro_factura['ImporteReacP1']==0 and $registro_factura['TextoReacP1']==""){} else {
	$pdf->CreaCelda(19,$posy,70,$registro_factura['TextoReacP1'],255,255,255,'Arial','',6);
	$pdf->CreaCeldaAlDer3(174,$posy,6,number_format($registro_factura['ImporteReacP1'], 2, ',', '.').$euro,255,255,255,'Arial','',6);
	
}

	if($registro_factura['ImporteReacP2']==0){} else {
		$lineas+=1; 
	$posy+=3;
	$pdf->CreaCelda(19,$posy,70,$registro_factura['TextoReacP2'],255,255,255,'Arial','',6);
	$pdf->CreaCeldaAlDer3(174,$posy,6,number_format($registro_factura['ImporteReacP2'], 2, ',', '.').$euro,255,255,255,'Arial','',6);
	
}

	if($registro_factura['ImporteReacP3']==0){} else {
		$lineas+=1; 
	$posy+=3;
	$pdf->CreaCelda(19,$posy,70,$registro_factura['TextoReacP3'],255,255,255,'Arial','',6);
	$pdf->CreaCeldaAlDer3(174,$posy,6,number_format($registro_factura['ImporteReacP3'], 2, ',', '.').$euro,255,255,255,'Arial','',6);
	
}

	if($registro_factura['ImporteReacP4']==0){} else {
		$lineas+=1; 
	$posy+=3;
	$pdf->CreaCelda(19,$posy,70,$registro_factura['TextoReacP4'],255,255,255,'Arial','',6);
	$pdf->CreaCeldaAlDer3(174,$posy,6,number_format($registro_factura['ImporteReacP4'], 2, ',', '.').$euro,255,255,255,'Arial','',6);
	
}

	if($registro_factura['ImporteReacP5']==0){} else {
		$lineas+=1; 
	$posy+=3;
	$pdf->CreaCelda(19,$posy,70,$registro_factura['TextoReacP5'],255,255,255,'Arial','',6);
	$pdf->CreaCeldaAlDer3(174,$posy,6,number_format($registro_factura['ImporteReacP5'], 2, ',', '.').$euro,255,255,255,'Arial','',6);
	
}

	if($registro_factura['ImporteReacP6']==0){} else {
		$lineas+=1; 
	$posy+=3;
	$pdf->CreaCelda(19,$posy,70,$registro_factura['TextoReacP6'],255,255,255,'Arial','',6);
	$pdf->CreaCeldaAlDer3(174,$posy,6,number_format($registro_factura['ImporteReacP6'], 2, ',', '.').$euro,255,255,255,'Arial','',6);
	
}
$lineas+=1; 
	$posy+=3;
	$lineas+=1; 
	$posy+=3;


		//ENERGIA ENERGIA CONSTANTE
	if(($registro_factura['ImporteEnergiaConstanteP1']==0) and ($registro_factura['ImporteEnergiaConstanteP2']==0) and ($registro_factura['ImporteEnergiaConstanteP3']==0) and ($registro_factura['ImporteEnergiaConstanteP4']==0) and ($registro_factura['ImporteEnergiaConstanteP5']==0) and ($registro_factura['ImporteEnergiaConstanteP6']==0)){} else {
	$pdf->CreaCelda(19,$posy,6,utf8_decode('Costes de gestión'),255,255,255,'Arial','b',7);
$posy+=3;
	}
	
if($registro_factura['ImporteEnergiaConstanteP1']==0 ){} else {
	$pdf->CreaCelda(19,$posy,70,$registro_factura['TextoEnergiaConstanteP1'],255,255,255,'Arial','',6);
	$pdf->CreaCeldaAlDer3(174,$posy,6,number_format($registro_factura['ImporteEnergiaConstanteP1'], 2, ',', '.').$euro,255,255,255,'Arial','',6);
	
}

	if($registro_factura['ImporteEnergiaConstanteP2']==0 ){} else {
		$lineas+=1; 
	$posy+=3;
	$pdf->CreaCelda(19,$posy,70,$registro_factura['TextoEnergiaConstanteP2'],255,255,255,'Arial','',6);
	$pdf->CreaCeldaAlDer3(174,$posy,6,number_format($registro_factura['ImporteEnergiaConstanteP2'], 2, ',', '.').$euro,255,255,255,'Arial','',6);
	
}

	if($registro_factura['ImporteEnergiaConstanteP3']==0 ){} else {
		$lineas+=1; 
	$posy+=3;
	$pdf->CreaCelda(19,$posy,70,$registro_factura['TextoEnergiaConstanteP3'],255,255,255,'Arial','',6);
	$pdf->CreaCeldaAlDer3(174,$posy,6,number_format($registro_factura['ImporteEnergiaConstanteP3'], 2, ',', '.').$euro,255,255,255,'Arial','',6);
	
}

	if($registro_factura['ImporteEnergiaConstanteP4']==0 ){} else {
		$lineas+=1; 
	$posy+=3;
	$pdf->CreaCelda(19,$posy,70,$registro_factura['TextoEnergiaConstanteP4'],255,255,255,'Arial','',6);
	$pdf->CreaCeldaAlDer3(174,$posy,6,number_format($registro_factura['ImporteEnergiaConstanteP4'], 2, ',', '.').$euro,255,255,255,'Arial','',6);
	
}

	if($registro_factura['ImporteEnergiaConstanteP5']==0 ){} else {
		$lineas+=1; 
	$posy+=3;
	$pdf->CreaCelda(19,$posy,70,$registro_factura['TextoEnergiaConstanteP5'],255,255,255,'Arial','',6);
	$pdf->CreaCeldaAlDer3(174,$posy,6,number_format($registro_factura['ImporteEnergiaConstanteP5'], 2, ',', '.').$euro,255,255,255,'Arial','',6);
	
}

	if($registro_factura['ImporteEnergiaConstanteP6']==0 ){} else {
		$lineas+=1; 
	$posy+=3;
	$pdf->CreaCelda(19,$posy,70,$registro_factura['TextoEnergiaConstanteP6'],255,255,255,'Arial','',6);
	$pdf->CreaCeldaAlDer3(174,$posy,6,number_format($registro_factura['ImporteEnergiaConstanteP6'], 2, ',', '.').$euro,255,255,255,'Arial','',6);
	
}
$lineas+=1; 
	$posy+=3;
	$lineas+=1; 
	$posy+=3;

	//ENERGIA Maximetros
	if(($registro_factura['TextoMaxP1']=="") and ($registro_factura['TextoMaxP2']=="") and ($registro_factura['TextoMaxP3']=="") and ($registro_factura['TextoMaxP4']=="") and ($registro_factura['TextoMaxP5']=="") and ($registro_factura['TextoMaxP6']=="")){} else {
	$pdf->CreaCelda(19,$posy,6,utf8_decode('Excesos de potencia'),255,255,255,'Arial','b',7);
$posy+=3;
	}
	
if($registro_factura['ImporteMaxP1']==0 and $registro_factura['TextoMaxP1']==""){} else {
	$pdf->CreaCelda(19,$posy,70,$registro_factura['TextoMaxP1'],255,255,255,'Arial','',6);
	$pdf->CreaCeldaAlDer3(174,$posy,6,number_format($registro_factura['ImporteMaxP1'], 2, ',', '.').$euro,255,255,255,'Arial','',6);
	
}

	if($registro_factura['ImporteMaxP2']==0 and $registro_factura['TextoMaxP2']==""){} else {
		$lineas+=1; 
	$posy+=3;
	$pdf->CreaCelda(19,$posy,70,$registro_factura['TextoMaxP2'],255,255,255,'Arial','',6);
	$pdf->CreaCeldaAlDer3(174,$posy,6,number_format($registro_factura['ImporteMaxP2'], 2, ',', '.').$euro,255,255,255,'Arial','',6);
	
}

	if($registro_factura['ImporteMaxP3']==0 and $registro_factura['TextoMaxP3']==""){} else {
		$lineas+=1; 
	$posy+=3;
	$pdf->CreaCelda(19,$posy,70,$registro_factura['TextoMaxP3'],255,255,255,'Arial','',6);
	$pdf->CreaCeldaAlDer3(174,$posy,6,number_format($registro_factura['ImporteMaxP3'], 2, ',', '.').$euro,255,255,255,'Arial','',6);
	
}

	if($registro_factura['ImporteMaxP4']==0 and $registro_factura['TextoMaxP4']==""){} else {
		$lineas+=1; 
	$posy+=3;
	$pdf->CreaCelda(19,$posy,70,$registro_factura['TextoMaxP4'],255,255,255,'Arial','',6);
	$pdf->CreaCeldaAlDer3(174,$posy,6,number_format($registro_factura['ImporteMaxP4'], 2, ',', '.').$euro,255,255,255,'Arial','',6);
	
}

	if($registro_factura['ImporteMaxP5']==0 and $registro_factura['TextoMaxP5']==""){} else {
		$lineas+=1; 
	$posy+=3;
	$pdf->CreaCelda(19,$posy,70,$registro_factura['TextoMaxP5'],255,255,255,'Arial','',6);
	$pdf->CreaCeldaAlDer3(174,$posy,6,number_format($registro_factura['ImporteMaxP5'], 2, ',', '.').$euro,255,255,255,'Arial','',6);
	
}

	if($registro_factura['ImporteMaxP6']==0 and $registro_factura['TextoMaxP6']==""){} else {
		$lineas+=1; 
	$posy+=3;
	$pdf->CreaCelda(19,$posy,70,$registro_factura['TextoMaxP6'],255,255,255,'Arial','',6);
	$pdf->CreaCeldaAlDer3(174,$posy,6,number_format($registro_factura['ImporteMaxP6'], 2, ',', '.').$euro,255,255,255,'Arial','',6);
	
}
//indexado

$lineas+=1; 
	$posy+=3;
	$lineas+=1; 
	$posy+=3;
	if($registro_factura['IndexadoImporte']==0 ){} else {
$pdf->CreaCelda(19,$posy,6,$registro_factura['IndexadoTexto'],255,255,255,'Arial','',6);
$pdf->CreaCeldaAlDer3(174,$posy,6,number_format($registro_factura['IndexadoImporte'], 2, ',', '.').$euro,255,255,255,'Arial','',6);
	}
//Lineas Adicionales
$lineas+=1; 
	$posy+=3;
	$lineas+=1; 
	$posy+=3;
	if($ImporteLinea==0 ){} else {
$pdf->CreaCelda(19,$posy,6,$TextoEnergiaLinea,255,255,255,'Arial','',6);
$pdf->CreaCeldaAlDer3(174,$posy,6,number_format($ImporteLinea, 2, ',', '.').$euro,255,255,255,'Arial','',6);
	}

//$pdf->CreaCelda(114,$posy +7,6,number_format($registro_factura['simpuesto'],2,",",".").$euro,255,255,255,'Arial','',6);

$posy=150;
$pdf->CreaCelda(19,$posy,10,'Impuesto Electrico',255,255,255,'Arial','B',6);
$pdf->CreaCelda(75,$posy,70,number_format($impuesto,3,",",".").'   % sobre   '.number_format($registro_factura['simpuesto'],2,",",".").'  eur.  x   '.number_format($coeficiente,4,",","."),255,255,255,'Arial','',6);
$pdf->CreaCeldaAlDer3(174,$posy,6,$total_impuesto.$euro,255,255,255,'Arial','',6);

$posy+=3;
$pdf->CreaCelda(19,$posy,70,'Alquiler de equipos de medida y control',255,255,255,'Arial','B',6);
if($alquiler==0){} else {
	//$pdf->CreaCelda(62,$posy,6,'('.$diasfacturar1.' x '.number_format(($alquiler/$diasfacturar1),3,",",",").$euro.'/dia)',255,255,255,'Arial','',6);
	$pdf->CreaCeldaAlDer3(174,$posy,6,number_format($alquiler,2,",",".").$euro,255,255,255,'Arial','',6);
}
$posy+=6;
	$pdf->CreaCelda(114,$posy,10,'Base Imponible                         '.$base_imponible .' '. $euro . utf8_decode('     21,00%'),255,255,255,'Arial','',6);
	$pdf->CreaCeldaAlDer3(174,$posy,6,$iva . ' ' . $euro,255,255,255,'Arial','',6);
	$posy+=7;
	$pdf->CreaCelda(114,$posy,10,'TOTAL',255,255,255,'Arial','B',8);
	$pdf->CreaCeldaAlDer3(174,$posy,6,$importe_total . ' ' . $euro,255,255,255,'Arial','B',8);
	

/*//$pdf->CreaCelda(28,$posy+1,40,'Termino de Energia (Mercado Electrico) ',255,255,255,'Arial','B',6);
$posy+=5;
// Energia P1
if($importeEnerP1==0 and $textoEnerP1==""){} else {
	$pdf->CreaCelda(28,$posy,70,$textoEnerP1,255,255,255,'Arial','',6);
	$pdf->CreaCeldaAlDer(118,$posy,6,$importeEnerP1,255,255,255);
	$lineas+=1; 
	$posy+=3;
}
if($importeDtoEnerP1==0 and $textoDtoEnerP1==""){} else {
	$pdf->CreaCelda(28,$posy,70,$textoDtoEnerP1,255,255,255,'Arial','',6);
	$pdf->CreaCeldaAlDer(118,$posy,6,$importeDtoEnerP1,255,255,255);
	$lineas+=1; 
	$posy+=3;
}
// Energia P2
if($importeEnerP2==0 and $textoEnerP2==""){} else {
	$pdf->CreaCelda(28,$posy,70,$textoEnerP2,255,255,255,'Arial','',6);
	$pdf->CreaCeldaAlDer(118,$posy,6,$importeEnerP2,255,255,255);
	$lineas+=1; 
	$posy+=3;
}
if($importeDtoEnerP2==0 and $textoDtoEnerP2==""){} else {
	$pdf->CreaCelda(28,$posy,70,$textoDtoEnerP2,255,255,255,'Arial','',6);
	$pdf->CreaCeldaAlDer(118,$posy,6,$importeDtoEnerP2,255,255,255);
	$lineas+=1; 
	$posy+=3;
}

//Energia P3
if($importeEnerP3==0 and $textoEnerP3==""){} else {
	$pdf->CreaCelda(28,$posy,70,$textoEnerP3,255,255,255,'Arial','',6);
	$pdf->CreaCeldaAlDer(118,$posy,6,$importeEnerP3,255,255,255);
	$lineas+=1; 
	$posy+=3;
}
if($importeDtoEnerP3==0 and $textoDtoEnerP3==""){} else {
	$pdf->CreaCelda(28,$posy,70,$textoDtoEnerP3,255,255,255,'Arial','',6);
	$pdf->CreaCeldaAlDer(118,$posy,6,$importeDtoEnerP3,255,255,255);
	$lineas+=1; 
	$posy+=3;
}

// Energia P4
if($importeEnerP4==0 and $textoEnerP4==""){} else {
	$pdf->CreaCelda(28,$posy,70,$textoEnerP4,255,255,255,'Arial','',6);
	$pdf->CreaCeldaAlDer(118,$posy,6,$importeEnerP4,255,255,255);
	$lineas+=1; 
	$posy+=3;
}
if($importeDtoEnerP4==0 and $textoDtoEnerP4==""){} else {
	$pdf->CreaCelda(28,$posy,70,$textoDtoEnerP4,255,255,255,'Arial','',6);
	$pdf->CreaCeldaAlDer(118,$posy,6,$importeDtoEnerP4,255,255,255);
	$lineas+=1; 
	$posy+=3;
}

//Energia P5
if($importeEnerP5==0 and $textoEnerP5==""){} else {
	$pdf->CreaCelda(28,$posy,70,$textoEnerP5,255,255,255,'Arial','',6);
	$pdf->CreaCeldaAlDer(118,$posy,6,$importeEnerP5,255,255,255);
	$lineas+=1; 
	$posy+=3;
}
if($importeDtoEnerP5==0 and $textoDtoEnerP5==""){} else {
	$pdf->CreaCelda(28,$posy,70,$textoDtoEnerP5,255,255,255,'Arial','',6);
	$pdf->CreaCeldaAlDer(118,$posy,6,$importeDtoEnerP5,255,255,255);
	$lineas+=1; 
	$posy+=3;
}

//Energia P6
if($importeEnerP6==0 and $textoEnerP6==""){} else {
	$pdf->CreaCelda(28,$posy,70,$textoEnerP6,255,255,255,'Arial','',6);
	$pdf->CreaCeldaAlDer(118,$posy,6,$importeEnerP6,255,255,255);
	$lineas+=1; 
	$posy+=3;
}
if($importeDtoEnerP6==0 and $textoDtoEnerP6==""){} else {
	$pdf->CreaCelda(28,$posy,70,$textoDtoEnerP6,255,255,255,'Arial','',6);
	$pdf->CreaCeldaAlDer(118,$posy,6,$importeDtoEnerP6,255,255,255);
	$lineas+=1; 
	$posy+=3;
}*/


//$pdf->CreaCelda(28,$posy+1,40,'Termino de Energia (Peajes Regulados BOE)',255,255,255,'Arial','B',6);
/*$posy+=5;

if($importeEnerP1Acceso==0 and $textoEnerAccesoP1==""){} else {
	$pdf->CreaCelda(28,$posy,70,$textoEnerAccesoP1,255,255,255,'Arial','',6);
	$pdf->CreaCeldaAlDer(118,$posy,6,$importeEnerP1Acceso.' ',255,255,255);
	$lineas+=1; 
	$posy+=3;
}

if($importeEnerP2Acceso==0 and $textoEnerAccesoP2==""){} else {
	$pdf->CreaCelda(28,$posy,70,$textoEnerAccesoP2,255,255,255,'Arial','',6);
	$pdf->CreaCeldaAlDer(118,$posy,6,$importeEnerP2Acceso.' ',255,255,255);
	$lineas+=1; 
	$posy+=3;
}

if($importeEnerP3Acceso==0 and $textoEnerAccesoP3==""){} else {
	$pdf->CreaCelda(28,$posy,70,$textoEnerAccesoP3,255,255,255,'Arial','',6);
	$pdf->CreaCeldaAlDer(118,$posy,6,$importeEnerP3Acceso.' ',255,255,255);
	$lineas+=1; 
	$posy+=3;
}

if($importeEnerP4Acceso==0 and $textoEnerAccesoP4==""){} else {
	$pdf->CreaCelda(28,$posy,70,$textoEnerAccesoP4,255,255,255,'Arial','',6);
	$pdf->CreaCeldaAlDer(118,$posy,6,$importeEnerP4Acceso.' ',255,255,255);
	$lineas+=1; 
	$posy+=3;
}

if($importeEnerP5Acceso==0 and $textoEnerAccesoP5==""){} else {
	$pdf->CreaCelda(28,$posy,70,$textoEnerAccesoP5,255,255,255,'Arial','',6);
	$pdf->CreaCeldaAlDer(118,$posy,6,$importeEnerP5Acceso.' ',255,255,255);
	$lineas+=1; 
	$posy+=3;
}

if($importeEnerP6Acceso==0 and $textoEnerAccesoP6==""){} else {
	$pdf->CreaCelda(28,$posy,70,$textoEnerAccesoP6,255,255,255,'Arial','',6);
	$pdf->CreaCeldaAlDer(118,$posy,6,$importeEnerP6Acceso.' ',255,255,255);
	$lineas+=1; 
	$posy+=3;
}


$pdf->CreaCelda(28,$posy+1,40,'Termino de Reactiva ',255,255,255,'Arial','B',6);
$posy+=5;
if($importeReacP1==0 and $textoReacP1==""){ } else {
	$textoReacP1=explode('x',$textoReacP1);
	$textoReacP1=explode(' ',$textoReacP1[0]);
	$pdf->CreaCelda(28,$posy,70,$textoReacP1[0].' P1',255,255,255,'Arial','',6);
	$pdf->CreaCeldaAlDer(118,$posy,6,$importeReacP1,255,255,255);
	$lineas+=1; 
	$posy+=3;
}
if($importeReacP2==0 and $textoReacP2==""){ } else {
	$textoReacP2=explode('x',$textoReacP2);
	$textoReacP2=explode(' ',$textoReacP2[0]);

	$pdf->CreaCelda(28,$posy,70,$textoReacP2[0].' P2',255,255,255,'Arial','',6);
	$pdf->CreaCeldaAlDer(118,$posy,6,$importeReacP2,255,255,255);
	$lineas+=1; 
	$posy+=3;
}
if($importeReacP3==0 and $textoReacP3==""){ } else {
	$textoReacP3=explode('x',$textoReacP3);
	$textoReacP3=explode(' ',$textoReacP3[0]);
	$pdf->CreaCelda(28,$posy,70,$textoReacP3[0].' P3',255,255,255,'Arial','',6);
	$pdf->CreaCeldaAlDer(118,$posy,6,$importeReacP3,255,255,255);
	$lineas+=1; 
	$posy+=3;
}
if($importeReacP4==0 and $textoReacP4==""){ } else {
	$textoReacP4=explode('x',$textoReacP4);
	$textoReacP4=explode(' ',$textoReacP4[0]);
	$pdf->CreaCelda(28,$posy,70,$textoReacP4[0],255,255,255,'Arial','',6);
	$pdf->CreaCeldaAlDer(118,$posy,6,$importeReacP4,255,255,255);
	$lineas+=1; 
	$posy+=3;
}
if($importeReacP5==0 and $textoReacP5==""){ } else {
	$textoReacP5=explode('x',$textoReacP5);
	$textoReacP5=explode(' ',$textoReacP5[0]);
	$pdf->CreaCelda(28,$posy,70,$textoReacP5[0],255,255,255,'Arial','',6);
	$pdf->CreaCeldaAlDer(118,$posy,6,$importeReacP5,255,255,255);
	$lineas+=1; 
	$posy+=3;
}
if($importeReacP6==0 and $textoReacP6==""){ } else {
	$textoReacP6=explode('x',$textoReacP6);
	$textoReacP6=explode(' ',$textoReacP6[0]);
	$pdf->CreaCelda(28,$posy,70,$textoReacP6[0],255,255,255,'Arial','',6);
	$pdf->CreaCeldaAlDer(118,$posy,6,$importeReacP6,255,255,255);
	$lineas+=1; 
	$posy+=3;
}
//$fefacturaFormat = date_format($fechafactura,'d-m-Y');
$pdf->CreaCelda(55,51,25,$num_factura,255,255,255,'Arial','',6);
$fechaFor=date("d/m/Y",strtotime($fecha));
$pdf->CreaCelda(55,54.5,25,$fechaFor,255,255,255,'Arial','',8);
$pdf->CreaCelda(55,58,25,$Cobro,255,255,255,'Arial','',8);
//Datos suministros
$pdf->CreaCelda(47,70,25,$nombre_titular,255,255,255,'Arial','',7);
$pdf->CreaCelda(47,73.5,25,$direccion_titular,255,255,255,'Arial','',7);
$pdf->CreaCelda(47,77,25,$titularCP,255,255,255,'Arial','',7);
$pdf->CreaCelda(47,80.5,25,$titularPob,255,255,255,'Arial','',7);
$pdf->CreaCelda(47,84,25,$dni_titular,255,255,255,'Arial','',7);
$pdf->CreaCelda(47,87.5,25,$cups,255,255,255,'Arial','',8);*/

}

$pdf->Output("ResumenFacturas_".$nif."_".date('d-m-Y').".pdf", 'D');

?>