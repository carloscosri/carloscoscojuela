<?php
/* NOTA:
 COMO NO SE CIERRA LA CONEXION TRAS CADA CONEXION CON LA BASE DE DATOS, LAS CONSULTAS DEJAN DE FUNCIONAR SI SE ESTABLECE COMO UNICA CONEXION
 LA QUE UTILIZA EL RESTO DE LA WEB, POR LO TANTO HASTA QUE SE HAGA LA MIGRACION COMPLETA DE LOS TEXTOS, COEXISTIRAN LAS DOS CONEXIONES, YA QUE
 SE HA AÑADIDO UNA LLAMADA A LA CONEXION BUENA EN ESTE ARCHIVO.
*/

include("_conexion.php");

//Se incluye el archivo con los textos especifico de esta seccion en el idioma oportuno
include("../idiomas/".$_SESSION["idioma"]."/oficina_presentacion.php");
include("../idiomas/".$_SESSION["idioma"]."/oficina_submenu.php");

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<title><?=$titulo_web;?></title>
<script language="JavaScript" type="text/javascript" src="ajax.js"></script>
<!--Se añaden las hojas de estilos genericas que utiliza esta seccion-->
<link href="../css/estilos.css" rel="stylesheet" type="text/css" />
<link href="../css/secciones.css" rel="stylesheet" type="text/css" />
<link href="css/oficina.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_presentacion_mp.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_presentacion.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_vieja.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_informacion_contratos_y_facturas.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_informacion_certificadofacturas.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" media="all" href="scripts/calendar/skins/aqua/theme.css" title="Aqua" />
<link href="css/colores.css" rel="stylesheet" type="text/css" />
<!--Ahora se añaden las reglas para Internet Explorer. Asi corregiremos las diferencias de visualizacion entre navegadores
Es importante añadirlo despues de las reglas principales, para que se sobreescriban las correcciones pertinentes. En este caso las diferencias
solo son para internet explorer 7-->

<!--[if IE 7]>
	<link href="../css/estilos_ie7.css" rel="stylesheet" type="text/css" />
	<link href="../css/secciones_ie7.css" rel="stylesheet" type="text/css" />    
<![endif]-->

<style>
	/*
	table.resultados{width:950px;border-collapse:collapse; text-align:center}
	tr.cabecera{background: url("../img/comun/gradientenaranja.jpg") repeat-x scroll 0 center #254B70; font-weight:bold;}
	tr.cabecera td{padding:7px; color:#FFF}
	tr.linea td{padding:3px; border-bottom:1px #666 solid;}
	
	tr.inputs{ background-color:#CCC;}
	input {background: repeat-x scroll 0 center #aaa; font-weight:bold; border-radius:5px; }
	*/
</style>

<style>
#fechasExcel, #fechasPDF, #comisiones{
	position:fixed;
	left:50%;
	top:50%;
	margin-left:-250px; 
	margin-top:-190px;        
    z-index:2;
    overflow:auto;
    display:none;               
	background:#f0f0f0;
	border: 2px solid #CCC;                   
	width:500px;
	/*padding:10px 10px 10px 10px;*/
	padding:8px;
	height:420px;
	/*margin-top:-20px;*/ /*margin:auto;*/
}
#comisiones{ height:240px;}
input.submit1{width:50px; height:50px; background-image:url(../img/oficina/excel.gif); text-indent:-5000px}
input.submit2{width:50px; height:50px; background-image:url(../img/oficina/pdf.png); text-indent:-5000px}
#fechasExcel table{ font-weight:normal;}

table.resultados{width:950px;border-collapse:collapse; text-align:center;font-size: 11px;}
tr.cabecera{font-size:15px; font-style:italic; border-bottom:2px solid #FFF; font-weight:bold;}
tr.cabecera td{padding:7px; color:#003C58}
tr.linea td{ border-bottom: 1px solid rgba(0, 60, 88, 0.25);
    height: 15px;
    padding: 3px 3px 8px;}
	
tr.inputs{ background-color: #003c58;
    height: 10px;
    padding: 10px 0;}
tr.inputs input { font-weight:bold; border-radius:5px;  box-shadow:none }
.calendar{z-index:10000;}
</style>

</head>
<body>
	<div id="central_oficina" style="top:0px">
<?php
//Se comprueba que el usuario se haya identificado, si no es asi se da un aviso y redirigimos a la seccion de identificacion
if ($_SESSION['usuario_mp']=="")
{	
	MsgBox($error_identificacion);
	redirigir("oficina_index.php?id=".$_SESSION["idioma"]);
	
} else {

//Añadimos los fragmentos comunes, la cabecera, el menu, y el submenu
	?>
		<div>
<?php include("../includes/cabecera.php"); ?>
        </div>                     
          
   		<div class="posicion_menu">
<?php  include("../includes/menu.php"); ?>			            
        </div><!--<div class="posicion_menu">-->

<div class="contenido_seccion_oficina_presentacion">

<!--Como el texto de oficina virtual ya lo tenemos añadido en el menu, en vez de duplicar el texto en la seccion correspondiente llamamos a la misma variable que el menu ($menu4)-->        
	<div class="titulo_seccion" style="height:25px;"><?=$menu4?> - Call Center</div>   
    <div class="menu_oficina"><?php include("menu_oficina_mp.php");?></div>     
			<div class="texto_presentacion"> 
              <!--<div style="text-align:center">
              <span class="arial11black">Selecciona un parametro de busqueda:</span>
                <form action="#" method="POST" name="busqueda">
                	<select name="busquedas">
                   		<option value="-">Seleccione</option>
                    	<option value="nombre">Nombre</option>
                        <option value="DNI">DNI</option>
                        <option value="CUPS">CUPS</option>
                        <option value="provincia">Provincia</option>
                    </select>
                	<input type="text" name="buscar" /><input type="button" value="Buscar" onclick="MostrarConsulta(document.forms.busqueda.buscar.value, document.forms.busqueda.busquedas.value)" />
                </form>
			 </div>-->
              
             <!-- <form name="busquedas">
              <div style="float:left">
              	<strong>Filtrar</strong> por 
                
                    <select name="provincia" onchange="MostrarCambiosProvincia(this.value)">
                    <option>Provicnia</option>
                    <?php
                    	$sql="select DISTINCT SuministroProvincia from DatosRegistrados order by SuministroProvincia"; 
						$rs = mysql_query($sql);
						while ($row=mysql_fetch_array($rs)) 
						{ 
							echo '<option value="'.$row['SuministroProvincia'].'">'.$row['SuministroProvincia'].'</option>'; 
						} 		
					?>
                    </select>
              </div>
              &nbsp;&nbsp;
              <div id="selectCiudad" style="display:inline; float:left;"></div>
             </form> -->    
             <!--<div id="selectDNI" style="display:inline; float:left; margin-left:30px; padding-left:30px; border-left:2px #999 solid;">
                    <strong>Filtrar</strong> por 
                    <select name="dni" onchange="MostrarCambiosDNI(this.value)">
                      <option>DNI</option>
                       <?php
                            $sql="select DISTINCT DNI from DatosRegistrados order by DNI"; 
                            $rs = mysql_query($sql);
                            while ($row=mysql_fetch_array($rs)) 
                            { 
                               echo '<option value="'.$row['DNI'].'">'.$row['DNI'].'</option>'; 
                            } 		
                        ?>
                    </select>
             </div>-->
             
		   </div> 
              <br />
              <form name="cambios">
                   <div align="center">
                   <span>
                   <strong>A continuacion se muestran todos los puntos de suministro. </strong><br />
                   Puede filtrar introduciendo el valor deseado en cada columna y pulsando 'ENTER'
                   </span>
                      <br />
                      <br />
                      <div id="resultado" style="padding-top:10px;">
                      <?php
                        $consulta_clientes="select * from DatosRegistrados";
						$resultados_clientes = mysql_query($consulta_clientes);	
						?>
                        <table class="resultados">
                            <tr class="cabecera">
                                <td>Cliente</td>
                                <td>DNI</td>
                                <td>Titular</td>
                                <td>Direccion</td>
                                <td>Numero</td>
                                <td>Provincia</td>
                                <td>CUPS</td>
                                <td>Acceso</td>
                            </tr>
                            
                            <tr class="inputs">
                                <td><input name="cliente" type="text" style="width:50px" onchange="MostrarConsulta(this.value, document.forms.cambios.dni.value,document.forms.cambios.titular.value,document.forms.cambios.direccion.value,document.forms.cambios.numero.value,document.forms.cambios.provincia.value,document.forms.cambios.cups.value)" /></td>
                                
                                <td><input name="dni" type="text" style="width:72px" onchange="MostrarConsulta(document.forms.cambios.cliente.value, this.value,document.forms.cambios.titular.value,document.forms.cambios.direccion.value,document.forms.cambios.numero.value,document.forms.cambios.provincia.value,document.forms.cambios.cups.value)"/></td>
                                
                                <td><input name="titular" type="text" style="width:145px" onchange="MostrarConsulta(document.forms.cambios.cliente.value, document.forms.cambios.dni.value,this.value,document.forms.cambios.direccion.value,document.forms.cambios.numero.value,document.forms.cambios.provincia.value,document.forms.cambios.cups.value)" /></td>
                                
                                <td><input name="direccion" type="text" style="width:120px" onchange="MostrarConsulta(document.forms.cambios.cliente.value, document.forms.cambios.dni.value,document.forms.cambios.titular.value,this.value,document.forms.cambios.numero.value,document.forms.cambios.provincia.value,document.forms.cambios.cups.value)" /></td>
                                
                                <td><input name="numero" type="text" style="width:50px" onchange="MostrarConsulta(document.forms.cambios.cliente.value, document.forms.cambios.dni.value,document.forms.cambios.titular.value,document.forms.cambios.direccion.value,this.value,document.forms.cambios.provincia.value,document.forms.cambios.cups.value)" /></td>
                                
                                <td><input name="provincia" type="text" style="width:80px" onchange="MostrarConsulta(document.forms.cambios.cliente.value, document.forms.cambios.dni.value,document.forms.cambios.titular.value,document.forms.cambios.direccion.value,document.forms.cambios.numero.value,this.value,document.forms.cambios.cups.value)" /></td>
                                
                                <td><input name="cups" type="text" style="width:155px" onchange="MostrarConsulta(document.forms.cambios.cliente.value, document.forms.cambios.dni.value,document.forms.cambios.titular.value,document.forms.cambios.direccion.value,document.forms.cambios.numero.value,document.forms.cambios.provincia.value,this.value)" /></td>
                                <script>
								function borrar(){
									location.href='oficina_presentacion_call.php';
								}
								</script>
                                <td><input type="reset" value="Limpiar" onclick="javascript:borrar()" />  </td>
                            </tr>
                            
                            
                            <?php
                            $i=1;
                            //muestra los datos consultados
                            while($registro_clientes = mysql_fetch_array($resultados_clientes)){
                                ?>
                                    
                              <tr class="linea">
                                <td><?=$registro_clientes['CodigoCliente'];?></td>
                                <td><?=$registro_clientes['DNI'];?></td>
                                <td><?=utf8_decode($registro_clientes['TitularNombre']);?></td>
                                <td><?=utf8_decode($registro_clientes['SuministroCalle']);?></td>
                                <td><?=utf8_decode($registro_clientes['SuministroNumero']);?></td>
                                <td><?=utf8_decode($registro_clientes['SuministroProvincia']);?></td>
                                <td><?=$registro_clientes['CUPS'];?></td>
                                <td>
                                    <a href="oficina_valida_login_mp.php?CodigoCliente=<?=$registro_clientes["CodigoCliente"]?>&amp;Password=<?=$registro_clientes["Password"]?>"> 
                                    <img src="../img/comun/entrar.gif" border="0" /> 
                                    </a></td>
                             </tr>   
                                <?php
                                $i++;
                            }
                       
                        ?>
                        </table>
                      </div>
           			</div></form>
                    <p style="margin-top:50px;"> </p>
                    <div class="limpiar"></div>
                    <br />
                    <!--Por ultimo aparecera el pie de la web-->
                 </div>
<?php 
include("../includes/pie.php");
}
?>
	<p>&nbsp;</p>
</div>
</body>
</html>