<?php
/*
NOTAS:

1- ESTA SECCION TOMA LOS DATOS DE LA TABLA DatosRegistrados

*/

/* NOTA:

 COMO NO SE CIERRA LA CONEXION TRAS CADA CONEXION CON LA BASE DE DATOS, LAS CONSULTAS DEJAN DE FUNCIONAR SI SE ESTABLECE COMO UNICA CONEXION
 LA QUE UTILIZA EL RESTO DE LA WEB, POR LO TANTO HASTA QUE SE HAGA LA MIGRACION COMPLETA DE LOS TEXTOS, COEXISTIRAN LAS DOS CONEXIONES, YA QUE
 SE HA AÑADIDO UNA LLAMADA A LA CONEXION BUENA EN ESTE ARCHIVO.
*/

include("_conexion.php");


//Se incluye el archivo con los textos especifico de esta seccion en el idioma oportuno
//include($_SESSION["directorio_raiz"]."idiomas/".$_SESSION["idioma"]."/oficina_contratacion.php");
include("../idiomas/".$_SESSION["idioma"]."/oficina_avisos.php");




include("../idiomas/".$_SESSION["idioma"]."/oficina_informacion_facturas.php");

include("../idiomas/".$_SESSION["idioma"]."/oficina_informacion_certificadoconsumos.php");

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<title><?=$titulo_web;?></title>

<!--Se añaden las hojas de estilos genericas que utiliza esta seccion-->
<link href="../css/estilos.css" rel="stylesheet" type="text/css" />
<link href="../css/secciones.css" rel="stylesheet" type="text/css" />
<link href="css/oficina.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_vieja.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_contratacion.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_avisos_noticias.css" rel="stylesheet" type="text/css" />

<!--Ahora se añaden las reglas para Internet Explorer. Asi corregiremos las diferencias de visualizacion entre navegadores
Es importante añadirlo despues de las reglas principales, para que se sobreescriban las correcciones pertinentes. En este caso las diferencias
solo son para internet explorer 7-->

<!--[if IE 7]>
	<link href="../css/estilos_ie7.css" rel="stylesheet" type="text/css" />
	<link href="../css/secciones_ie7.css" rel="stylesheet" type="text/css" />    
<![endif]-->

</head>
	
<body>
<!--*************************************************Web realizada por ****************************************-->
	<div id="central_oficina">
    
<?php

//Se comprueba que el usuario se haya identificado, si no es asi se da un aviso y redirigimos a la seccion de identificacion
if ($_SESSION['usuario']=="")
{	
	MsgBox($error_identificacion);
	redirigir("oficina_index.php?id=".$_SESSION["idioma"]);
	
}//if ($_SESSION['usuario'])

//Si el usuario se ha identificado la seccion se ejecutara con normalidad
else
{
//Añadimos los fragmentos comunes, la cabecera, el menu, y el submenu
		include("oficina_fragmentos_comunes.php");
?>                   
		<div class="contenido_seccion_oficina_contratacion">

<!--VERSION VIEJA-->

	 <table width="650" border="0" cellspacing="0" cellpadding="0">
            		 
                <tr align="center">
                  <td colspan="4" height="0" valign="top" class="arialblack18Titulo">Noticias </td>
                </tr>
              
                 </table>
 </form>
<!--VERSION VIEJA-->













            <div class="posicion_cuadro_ventajas">
                <div class="cuadro_ventajas_cabecera"></div>            
                                
              <div class="cuadro_ventajas_cuerpo">
                    
<?php
//este recordset es con diferencia de idioma
//$rs_cortes=seleccion_condicional("cortes_luz", "idioma='".$_SESSION["idioma"]."'","id",0,$ruta_sevidor_bd,$usuario_bd,$contrasena_bd,$nombre_bd);

//este recordset es sin diferencia de idioma
$rs_noticias_seccion = seleccion_condicional("noticias", "id='".$_GET['not']."'","fecha",1,$ruta_sevidor_bd,$usuario_bd,$contrasena_bd,$nombre_bd);
$cont=0;

$registros = mysql_num_rows($rs_noticias_seccion);

if ($registros > 0){


?>		             


<span class="centrar">
<table width="100%" border="0" cellspacing="0" cellpadding="5">
  
<?php


	while($registro_noticia = mysql_fetch_array($rs_noticias_seccion))
		{
		$fecha = strftime('%d/%m/%Y', strtotime($registro_noticia["fecha"]));
		
		
		?>
          <tr>
          <td class="titulo_ventajas_informacion_consejos"><?=$registro_noticia["titular"]?></td>
          </tr>
		  <tr>
          <td class="parrafo_informacion_ventajas pequenno blanco" align="right"><?=$fecha?></td>
          </tr>
          <tr>
			<td class="justificar blanco">
            <?
			if ($registro_noticia["imagen"]!=""){
			?>
            <?php /*?><img src="imgnoticias/<?=$registro_noticia["imagen"]?>" align="left" style="margin-right: 10px; margin-bottom: 4px" /><?php */?>
			<?php 
			}
			?><?=$registro_noticia["texto"]?></td>
		  </tr>
         <?php 
		 if ($registro_noticia["enlace"]!=""){
		 ?>
         <tr>
          <td class="blanco">Más Información: <a href="<?=$registro_noticia["enlace"]?>" class="enlace_pie" target="_blank"><?=$registro_noticia["enlace"]?></a></td>
          </tr>
          
		<?php
		}
	}
	?>
	</table></span>
    <?php
}
	else
{
 ?>
 
                <div class="enlace_nota_pdf centrar"><?=$informacion_cortes4?></div>
  
  <?php 
}
?>
 


              </div><!--<div class="cuadro_ventajas_cuerpo">-->
         
            
                <div class="cuadro_ventajas_pie"></div>                        
                
                <div class="centrar"><a href="oficina_avisos_noticias.php?id=<?=$_SESSION['idioma']?>" class="enlace_pie"><?=$volver_listado_noticias?></a><br /><br /></div>
                </div><!--<div class="posicion_cuadro_ventajas">-->


        
        
        
        
        
        
        
        
        
        
        
        <div class="limpiar"></div>
                        	
<!--Por ultimo aparecera el pie de la web-->













<?php 
	include("../includes/pie.php");

//NOTA: EN ESTA OCASION, EL ERROR SE MOSTRARA DESPUES DE LA CARGA DE LA PAGINA, SI NO POR ALGUN MOTIVO EN OCASIONES DESPUES DE DAR EL ERROR CESA LA CARGA DE LA PAGINA
	if(isset($_POST["Ver"]) or isset($_POST["Submit"]))
	{
//Por ultimo mostraremos el error correspondiente, en el caso de que haya fallado alguna de las verificaciones, o continuaremos con el proceso		
		switch($error)
		{				
			case 0:
//Si no se ha producido ningun error en las verificaciones de las fechas, en el caso de haber pulsado el boton enviar se dara el alta en la tabla CertificadoConsumo
				if(isset($_POST["Submit"]))
				{

//LOLOLO		
					$insertar_solicitud_informacion_cortes="INSERT INTO `SolicitudInformacionCortes` (`NumeroCliente` ,`FechaRegistro` ,`HoraRegistro` ,`Email` ,`Observaciones`) VALUES (
'".$_SESSION['numeroCliente']."', '".date("Y-m-d")."', '".date("Y-m-d H:i:s")."', '".$suministro_email."', 'Solicita Información Cortes');";
	
					$ejecutar_solicitud_informacion_cortes=mysql_query($insertar_solicitud_informacion_cortes);
										
					MsgBox($solicitud_ok);
				}//if(isset($_POST["Submit"]))
				
			break;
//Error por no rellenar los campos obligatorios
			case 1:
				MsgBox($error_rellenar);
			break;								
			
//Error por escribir la fecha incorrectamente
			case 2:
				MsgBox($error_fecha);
			break;									
		
//Error por fechas escritas posterior a la actual
			case 3:
				MsgBox($error_fecha_posterior);
			break;									

//Error por que la fecha de inicio es mayor que la fecha final
			case 4:
				MsgBox($error_fecha_inicio_mayor);
			break;											
			
		}//switch($error)
	}//if(isset($_POST["Submit"]))	
        
}//else ($_SESSION['usuario'])
?>   






     
    </div><!--<div id="central_oficina"-->
</body>
</html>
