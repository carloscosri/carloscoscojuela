<?php
/*
NOTAS:

1- ESTA SECCION TOMA LOS DATOS DE LA TABLA DatosRegistrados

2- ESCRIBE EN LA TABLA DatosModificados

*/

/* NOTA:

 COMO NO SE CIERRA LA CONEXION TRAS CADA CONEXION CON LA BASE DE DATOS, LAS CONSULTAS DEJAN DE FUNCIONAR SI SE ESTABLECE COMO UNICA CONEXION
 LA QUE UTILIZA EL RESTO DE LA WEB, POR LO TANTO HASTA QUE SE HAGA LA MIGRACION COMPLETA DE LOS TEXTOS, COEXISTIRAN LAS DOS CONEXIONES, YA QUE
 SE HA AÑADIDO UNA LLAMADA A LA CONEXION BUENA EN ESTE ARCHIVO.
*/

include("_conexion.php");

//Se incluye el archivo que contiene las verificaciones de datos
include("../includes/verificaciones.php");

//Como tienen textos en comun, para evitar el crecimiento innecesario de la base de datos, se incluye el archivo de textos de las secciones de datos del titular y del alta, ya que la mayoria de los textos son comunes
//include($_SESSION["directorio_raiz"]."idiomas/".$_SESSION["idioma"]."/oficina_datos_titular.php");
//include($_SESSION["directorio_raiz"]."idiomas/".$_SESSION["idioma"]."/oficina_contratacion_alta.php");

//include($_SESSION["directorio_raiz"]."idiomas/".$_SESSION["idioma"]."/oficina_modificacion_datostitular.php");

include("../idiomas/".$_SESSION["idioma"]."/oficina_datos_titular.php");
include("../idiomas/".$_SESSION["idioma"]."/oficina_contratacion_alta.php");
include("../idiomas/".$_SESSION["idioma"]."/oficina_modificacion_datostitular.php");
//include("../idiomas/".$_SESSION["idioma"]."/oficina_modificacion_datosbancarios.php");

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<title><?=$titulo_web;?></title>

<!--Se añaden las hojas de estilos genericas que utiliza esta seccion-->
<link href="../css/estilos.css" rel="stylesheet" type="text/css" />
<link href="../css/secciones.css" rel="stylesheet" type="text/css" />
<link href="css/oficina.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_vieja.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_modificacion_datosbancarios.css" rel="stylesheet" type="text/css" />

<!--Ahora se añaden las reglas para Internet Explorer. Asi corregiremos las diferencias de visualizacion entre navegadores
Es importante añadirlo despues de las reglas principales, para que se sobreescriban las correcciones pertinentes. En este caso las diferencias
solo son para internet explorer 7-->

<!--[if IE 7]>
	<link href="../css/estilos_ie7.css" rel="stylesheet" type="text/css" />
	<link href="../css/secciones_ie7.css" rel="stylesheet" type="text/css" />    
<![endif]-->

<script language="javascript" type="text/javascript">
<!--
//Esta funcion se ejecuta cuando se cambia el valor de la forma de pago, deshabilitara los campos de banco domiciliado y los componentes del numero de la cuenta, cuando se seleccione la opcion de No domiciliado y los habilitara cuando se seleccione la opcion de Domiciliacion
function datos_bancarios()
{
//Esta variable almacenara un indice que representara la forma de pago seleccionada:
//1 = Domiciliacion
//2 = No Domiciliado

	var forma_pago;
	
	forma_pago=document.form_cambios_datos_bancarios.FormaPago.value;
		
	switch(forma_pago)	
	{
//Si la forma de pago es domiciliacion bancaria, se habilitaran los campos correspondientes	
		case "1":					
			document.form_cambios_datos_bancarios.EntidadBancaria.disabled = false;
			document.form_cambios_datos_bancarios.banco.disabled = false;			
			document.form_cambios_datos_bancarios.sucursal.disabled = false;						
			document.form_cambios_datos_bancarios.sucursal2.disabled = false;						
			document.form_cambios_datos_bancarios.sucursal3.disabled = false;													
		break;

//Si la forma de pago es no domicialiacion, se dehabilitaran los campos correspondientes y se eliminaran sus valores
		case "2":
			document.form_cambios_datos_bancarios.EntidadBancaria.selectedIndex=0; 
			document.form_cambios_datos_bancarios.banco.value = "";			
			document.form_cambios_datos_bancarios.sucursal.value = "";						
			document.form_cambios_datos_bancarios.sucursal2.value = "";						
			document.form_cambios_datos_bancarios.sucursal3.value = "";	
					
			document.form_cambios_datos_bancarios.EntidadBancaria.disabled = true;
			document.form_cambios_datos_bancarios.banco.disabled = true;			
			document.form_cambios_datos_bancarios.sucursal.disabled = true;						
			document.form_cambios_datos_bancarios.sucursal2.disabled = true;						
			document.form_cambios_datos_bancarios.sucursal3.disabled = true;							
		break;				
	}//switch(forma_pago)
}//function datos_bancarios()

//Esta funcion establece el codigo de la entidad bancaria en el numero de cuenta
function asignar_codigo_entidad_bancaria()
{	
	var entidad_bancaria=document.form_cambios_datos_bancarios.EntidadBancaria.value;
//Dividimos el valor recibido de la entidad bancaria por el simbolo mas. Esta variable contendra en la primera posicion el nombre de la entidad bancaria y en la segunda el codigo
	var partes_entidad_bancaria= entidad_bancaria.split('+');
		
	document.form_cambios_datos_bancarios.banco.value=partes_entidad_bancaria[1];
	
	document.form_cambios_datos_bancarios.sucursal.value = "";						
	document.form_cambios_datos_bancarios.sucursal2.value = "";						
	document.form_cambios_datos_bancarios.sucursal3.value = "";						
}//function asignar_codigo_entidad_bancaria()





//Luis
function asignar_entidad_bancaria()
{

	var nombre_banco=document.form_cambios_datos_bancarios.banco.value;
	
}

//Esta funcion hace que los textos almacenados en la base de datos para los datos bancarios marquen la opcion oportuna en los combos
function dar_valor_datos_bancarios(forma_pago, banco, idioma) //forma_pago
{			
	switch(forma_pago)
	{
//Si la forma de pago es la domicialiacion la seleccionaremos en el combobox y habilitaremos los campos de entidad bancaria y la cuenta 	
		case "Domiciliacion":
			document.form_cambios_datos_bancarios.FormaPago.selectedIndex=1; 	
			document.form_cambios_datos_bancarios.EntidadBancaria.disabled = false;
			document.form_cambios_datos_bancarios.banco.disabled = false;			
			document.form_cambios_datos_bancarios.sucursal.disabled = false;			
			document.form_cambios_datos_bancarios.sucursal2.disabled = false;			
			document.form_cambios_datos_bancarios.sucursal3.disabled = false;	

//Esta variable almacenara el numero total de entidades bancarias disponibles			
			var num_bancos_disponibles=document.form_cambios_datos_bancarios.EntidadBancaria.options.length;
			
//Esta variable almacenara el numero de entidades bancarias que se han comprobado			
			var num_bancos_comprobados=0;
											
//Ahora seleccionaremos la entidad bancaria en el combobox. Como los nombres de las entidades bancarias no cambian con los idiomas, podemos realizar la busqueda directamente en los valores del combobox
			while (num_bancos_comprobados<num_bancos_disponibles)
			{		
//Como el valor que se pasa desde el combo de bancos es el nombre de la entidad y el codigo de la entidad bancaria separados por un signo "+", para realizar la comparacion, deberemos separar ambas partes
				var partes_banco= document.form_cambios_datos_bancarios.EntidadBancaria.options[num_bancos_comprobados].value.split('+');
						
//Cuando encontramos la tarifa buscada, se selecciona en el combo de tarifas y salimos de la funcion
				if(partes_banco[0]==banco)
				{
					document.form_cambios_datos_bancarios.EntidadBancaria.selectedIndex=num_bancos_comprobados;
				}//if(document.form_cambios_datos_bancarios.EntidadBancaria.options[num_bancos_comprobados].value==banco)
				
				num_bancos_comprobados++;
			}//while (num_bancos_comprobados<num_bancos_disponibles)
														
		break;

//Si la forma de pago es domicializacion, simplemente la seleccionaremos en el combobox		
		case "No Domiciliado":
			document.form_cambios_datos_bancarios.FormaPago.selectedIndex=2; 			
		break;							
	}//switch(forma_pago)	
	
//En cualquiera de los casos le damos valor al idioma de la factura	
	switch(idioma)
	{
		case "Castellano":
			document.form_cambios_datos_bancarios.IdiomaFactura.selectedIndex=0; 			
		break;

		case "Catalán":
			document.form_cambios_datos_bancarios.IdiomaFactura.selectedIndex=1; 					
		break;

		case "Euskera":
			document.form_cambios_datos_bancarios.IdiomaFactura.selectedIndex=2; 							
		break;

		case "Gallego":
			document.form_cambios_datos_bancarios.IdiomaFactura.selectedIndex=3; 							
		break;						

		case "Bable (Asturias)":
			document.form_cambios_datos_bancarios.IdiomaFactura.selectedIndex=4; 							
		break;		
		
	}//switch(idioma)
	
}//function dar_valor_datos_bancarios()

-->
</script>

<?php
if (isset($_POST["submit"]))
{
//Recogemos los valores de las variables
	$titular_nombre=$_POST["TitularNombre"];							
	$titular_calle=$_POST["TitularCalle"];								
	$titular_numero=$_POST["TitularNumero"];
	$titular_extension=$_POST["TitularExtension"];	
	$titular_aclarador=$_POST["TitularAclarador"];		
	$titular_poblacion=$_POST["TitularPoblacion"];		
	$titular_provincia=$_POST["TitularProvincia"];
	
	$titular_telefono=$_POST["TitularTelefono"];					
	$titular_movil=$_POST["TitularMovil"];						
	$titular_fax=$_POST["TitularFax"];								
	$titular_email=$_POST["TitularEmail"];	
	$titular_dni=$_POST["TitularDNI"];

	$factura_nombre=$_POST["FacturaNombre"];							
	$factura_calle=$_POST["FacturaCalle"];								
	$factura_numero=$_POST["FacturaNumero"];	
	$factura_extension=$_POST["FacturaExtension"];
	$factura_aclarador=$_POST["FacturaAclarador"];		
	$factura_poblacion=$_POST["FacturaPoblacion"];		
	$factura_provincia=$_POST["FacturaProvincia"];
	
	$factura_telefono=$_POST["FacturaTelefono"];		
	$factura_movil=$_POST["FacturaMovil"];			
	$factura_fax=$_POST["FacturaFax"];	
	$factura_email=$_POST["FacturaEmail"];	
	$factura_dni=$_POST["FacturaDNI"];
		
	$pagador_nombre=$_POST["PagadorNombre"];							
	$pagador_calle=$_POST["PagadorCalle"];								
	$pagador_numero=$_POST["PagadorNumero"];
	$pagador_poblacion=$_POST["PagadorPoblacion"];		
	$pagador_provincia=$_POST["PagadorProvincia"];	
	$pagador_numero=$_POST["PagadorNumero"];			
	$pagador_extension=$_POST["PagadorExtension"];
	$pagador_aclarador=$_POST["PagadorAclarador"];
		
	$pagador_telefono=$_POST["PagadorTelefono"];		
	$pagador_movil=$_POST["PagadorMovil"];			
	$pagador_fax=$_POST["PagadorFax"];						
	$pagador_email=$_POST["PagadorEmail"];
	$pagador_dni=$_POST["PagadorDNI"];	

	$representante_nombre=$_POST["RepresentanteEmpresa"];
	$representante_dni=$_POST["DNIRepresentante"];
	
	$num_cuenta_banco=$_POST["banco"];
	$num_cuenta_sucursal=$_POST["sucursal"];	
	$num_cuenta_digito_control=$_POST["sucursal2"];		
	$num_cuenta_cuenta=$_POST["sucursal3"];		
	
	$observaciones=$_POST["Observaciones"];

//Estos campos son combos y tendrán un tratamiento especial	
	$forma_pago=$_POST["FormaPago"];
	$idioma_factura=$_POST["IdiomaFactura"];	
	$entidad_bancaria=$_POST["EntidadBancaria"];	
	
//Esta variable controlara si se produce algun error en las verificaciones
	$error=0;	
	
//Ahora comenzaremos con las verficaciones. En primer lugar comprobaremos si los campos obligatorios han sido todos escritos
	
//Ahora pasaremos a comprobar los datos bancarios. Si la forma de pago es "Domiciliada" los campos de Entidad Bancaria y numero de cuenta se vuelven obligatorios
	if($error==0 and $forma_pago==1)
	{				
//Esta variable almacenara el numero de la cuenta bancaria completo		
		$num_cuenta_completo=$num_cuenta_banco.$num_cuenta_sucursal.$num_cuenta_digito_control.$num_cuenta_cuenta;		
//Esta variable almacenara true si el digito de control es correcto y false si no lo es, será otra restriccion mas para qeu el nuemro de cuenta bancaria sea correcto
		$comprobacion=comprobacion_digito_control($num_cuenta_completo);

//Ahora sbdividimos el valor recibido del campo de la entidad bancaria por el signo "+", en la primera posicion se almacenara el nombre de la entidad bancaria y en la segunda el codigo. Lo utilizaremos para comprobar que el usuario no haya cambiado el codigo de entidad bancaria asignado automaticamente
		$partes_entidad_bancaria=explode("+",$entidad_bancaria);

//En las comprobaciones añadimos la comprobacion de si el primer numero de la cuenta bancaria coincide con el codigo de la identidad bancaria seleccionada								
		if($entidad_bancaria==-1 or $num_cuenta_banco=="" or $num_cuenta_sucursal=="" or $num_cuenta_digito_control=="" or $num_cuenta_cuenta=="" or !$comprobacion)
		{
			//$error=5;
		}
		
		
		//	if($entidad_bancaria==-1 or $num_cuenta_banco=="" or $num_cuenta_sucursal=="" or $num_cuenta_digito_control=="" or $num_cuenta_cuenta==""or $partes_entidad_bancaria[1]!=$num_cuenta_banco or !$comprobacion)
		
//Si se ha rellenado el numero de la cuenta comprobaremos que la cuenta bancaria solo contenga caracteres numericos y cada tramo del numero de la cuenta debera de tener la longitud correcta de caracteres
		else
		{							
			if($error==0 and (strlen($num_cuenta_banco)<4 or solo_numero($num_cuenta_banco)) or (strlen($num_cuenta_sucursal)<4 or solo_numero($num_cuenta_sucursal)) or (strlen($num_cuenta_digito_control)<2 or solo_numero($num_cuenta_digito_control)) or (strlen($num_cuenta_cuenta)<10 or solo_numero($num_cuenta_cuenta)))
			{
				$error=6;			
			}//if((strlen($num_cuenta_banco)<4 or solo_numero($num_cuenta_banco)) or (strlen($num_cuenta_sucursal)<4 or solo_numero($num_cuenta_sucursal)) or (strlen($num_cuenta_digito_control)<2 or solo_numero($num_cuenta_digito_control)) or (strlen($num_cuenta_cuenta)<10 or solo_numero($num_cuenta_cuenta)))
			
		}//else($entidad_bancaria==-1 or $num_cuenta_banco=="" or $num_cuenta_sucursal=="" or $num_cuenta_digito_control=="" or $num_cuenta_digito_control=="")
		
	}//if($error==0 and $forma_pago==1)


if($_POST['Condiciones']=='Yes')  

{ } else {$error=9;}

//Ahora se comprobara si se ha escrito algún dato en los apartados Facturacion, Pagador y Representante empresa. En el caso de que se haya escrito se debera de comprobar que los campos marcados como obligatorios en los demas apartados esten llenos
			
//Comprobacion de los datos del facturacion

//Comprobacion de los datos del pagador

}//if (isset($_POST["submit"]))

?>

</head>
	
<body>
<!--*************************************************Web realizada por ****************************************-->
	<div id="central_oficina">
    
<?php
//Se comprueba que el usuario se haya identificado, si no es asi se da un aviso y redirigimos a la seccion de identificacion
if ($_SESSION['usuario']=="")
{	
	MsgBox($error_identificacion);
	redirigir("oficina_index.php?id=".$_SESSION["idioma"]);
	
}//if ($_SESSION['usuario'])

//Si el usuario se ha identificado la seccion se ejecutara con normalidad
else
{
//Añadimos los fragmentos comunes, la cabecera, el menu, y el submenu
		include("oficina_fragmentos_comunes.php");
?>                    
		<div class="contenido_seccion_oficina_modificacion_datosbancarios">

<!--VERSION VIEJA-->


		
		<form action="oficina_modificacion_datosbancarios.php?id=<?=$_SESSION["idioma"]?>"  method="post" name="form_cambios_datos_bancarios" id="form_cambios_datos_bancarios">
		  <table width="458" border="0" cellspacing="0" cellpadding="0">
            <tr> 
              <td><table width="700" border="0" cellspacing="0" cellpadding="0">
                  <tr align="center"> 
                    <td width="700" height="30" class="arialblack18Titulo"> 
                      <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 692'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>                    </td>
                  </tr>
                  <tr> 
                    <td height="18" valign="middle">&nbsp;</td>
                  </tr>
                    <tr> 
                    <td height="36" ><fieldset><strong> 
                    <div class="posicion_submenu_subseccion separacion_submenu_subseccion"><a class="arial12Importanteb"><?=$oficina_datos_titular_nota?></a></div>
                    
                    </strong></fieldset></td>
                    </tr>
                     <tr> 
                    <td height="18" valign="middle">&nbsp;</td>
                  </tr>
                    
<?php
//Esta variable controlara si el cliente tiene o no modificaciones pendientes
					$tiene_modificaciones_pendientes=0;

//Comprobamos si el cliente, tiene modificaciones pendientes
					$consulta_cliente=mysql_query("SELECT * FROM DatosModificados WHERE CodigoCliente  = ".$_SESSION["numeroCliente"]); 
					$hay_cliente = mysql_fetch_array($consulta_cliente);
										
					if($hay_cliente)
					{
						$tiene_modificaciones_pendientes=1;
					}//if($hay_cliente)
										
//Si el cliente tiene modificaciones pendientes se mostrara un mensaje					
					if($tiene_modificaciones_pendientes==1)
					{
?>                  
<!--	                  <tr>
   	              	<td valign="middle" class="texto_advertencia"><?=$advertencia_cambios_pendientes?></td>
        	          </tr>   -->
<?php				}//if($tiene_modificaciones_pendientes==1)?>                  

                  <tr> 
                    <td height="94" class="arial14"><fieldset>
					<legend class="arialblack14">
					<?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 562'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
					</legend>
                    
                      <table width="500" align="center" border="0">
<?php
							
							$result = mysql_query("SELECT * FROM DatosRegistrados WHERE CodigoCliente = ".$_SESSION['numeroCliente']." ");
							$row = mysql_fetch_array($result);
							
//Si se ha aceptado el formulario, las variables que contienen los valores de cada campo ya tienen valor. Si no es asi, asignaremos los valores con el contenido de la base de datos
							if (!isset($_POST["submit"]))	
							{								
//Si el formulario no se ha aceptado, se mostraran los valores almacenados en la base de datos
								$titular_nombre=$row["TitularNombre"];
								$titular_calle=$row["TitularCalle"];
								$titular_numero=$row["TitularNumero"];
								$titular_extension=$row["TitularExtension"];
								$titular_aclarador=$row["TitularAclarador"];
								$titular_poblacion=$row["TitularPoblacion"];;
								$titular_provincia=$row["TitularProvincia"];
								$titular_telefono=$row["TitularTelefono1"];
								$titular_movil=$row["TitularTelefono2"];
								$titular_fax=$row["TitularFax"];
								$titular_email=$row["TitularEmail"];
								$titular_dni=$row["TitularDNI"];
							
								$factura_nombre=$row["FacturaNombre"];
								$factura_calle=$row["FacturaCalle"];
								$factura_numero=$row["FacturaNumero"];
								$factura_extension=$row["FacturaExtension"];
								$factura_aclarador=$row["FacturaAclarador"];
								$factura_poblacion=$row["FacturaPoblacion"];
								$factura_provincia=$row["FacturaProvincia"];
								$factura_telefono=$row["FacturaTelefono1"];
								$factura_movil=$row["FacturaTelefono2"];
								$factura_fax=$row["FacturaFax"];
								$factura_email=$row["FacturaEmail"];
								$factura_dni=$row["FacturaDNI"];
							
								$pagador_nombre=$row["PagadorNombre"];
								$pagador_calle=$row["PagadorCalle"];
								$pagador_numero=$row["PagadorNumero"];
								$pagador_extension=$row["PagadorExtension"];
								$pagador_aclarador=$row["PagadorAclarador"];
								$pagador_poblacion=$row["PagadorPoblacion"];
								$pagador_provincia=$row["PagadorProvincia"];
								$pagador_telefono=$row["PagadorTelefono1"];
								$pagador_movil=$row["PagadorTelefono2"];
								$pagador_fax=$row["PagadorFax"];
								$pagador_email=$row["PagadorEmail"];
								$pagador_dni=$row["PagadorDNI"];
							
								$representante_nombre=$row["RepresentanteEmpresa"];
								$representante_dni=$row["DNIRepresentante"];
													
//Ahora subdividiremos el numero de la cuenta de banco para escribir las cifras correspondientes en cada uno de los cuadros
								$num_cuenta_banco=substr($row["NumeroCuenta"],0,4);
								$num_cuenta_sucursal=substr($row["NumeroCuenta"],4,4);
								$num_cuenta_digito_control=substr($row["NumeroCuenta"],8,2);
								$num_cuenta_cuenta=substr($row["NumeroCuenta"],10,10);
							
								$forma_pago=$row["FormaPago"];
								$idioma_factura=$row["IdiomaFactura"];	
								$entidad_bancaria=$row["BancoDomiciliado"];	
		
								$observaciones=$row["Observaciones"];
							}//if (!isset($_POST["submit"]))
						
?>
                        
                      
                        
                        <tr> 
                          <td width="461" height="22" class="arial14"> 
                           &nbsp;&nbsp;  <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 119'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                         &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;   <input name="TitularNombreFijo"   value="<?php echo(str_replace('"','&quot;',$row['TitularNombre']));?>"type="text" class="textfieldLectura" id="TitularNombreFijo" size="50" maxlength="50" /></td>
                        </tr>
                        <tr> 
                          <td height="22" class="arial14"> 
                           &nbsp;&nbsp; <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 118'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input   value="<?php echo($row['DNI']);?>" name="TitularDNIFijo" type="text" class="textfieldLecturaNum" id="TitularDNIFijo" size="9" maxlength="9" /> 
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                            <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 196'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                            &nbsp;&nbsp; <input name="TitularCodigoFijo"   value="<?php echo($row['CodigoCliente']);?>" type="text" class="textfieldLecturaNum" id="TitularCodigoFijo" size="10" maxlength="10" /> 
                            &nbsp;&nbsp;</td>
                        </tr>
                        <tr> 
                          <td height="22" class="arial14">&nbsp;&nbsp;<?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 197'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; <input name="TitularPolizaFijo" value="<?php echo($row['NumeroPoliza']);?>"  type="text" class="textfieldLecturaNum" id="TitularPolizaFijo" size="10" maxlength="10" /></td>
                            
                        </tr>
<!--CAMPO NUEVO-->
                        <tr> 
                          <td height="22" class="arial14">&nbsp;&nbsp;CUPS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input name="CUPSFijo" value="<?php echo($row['CUPS']);?>"  type="text" class="textfieldLectura" id="CUPSFijo" size="25" maxlength="25" /></td>
                        </tr>                        
<!--CAMPO NUEVO-->                        
                        
                        
                      </table>
                      </fieldset></td>
                  </tr>
                  <tr> 
                    <td height="10"></td>
                  </tr>
                  
                       
                  <tr class="arial12"> 
                  <td colspan="2" class="arial14"><fieldset  >
                    <legend class="arialblack14">
                    <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 138'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                    </legend>
                    
<!--ESPACIO ENTRE EL TITULO Y EL PRIMER CAMPO-->                   
					<table width="450" align="center">
                      
                      <tr> 
                        <td width="73" height="5" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                                 
                        </table>                        </td>
                      </tr>
<!--ESPACIO ENTRE EL TITULO Y EL PRIMER CAMPO-->                   
                    
                    
                    
                    <table width="450" align="center">
                      <tr> 
                        <td width="442" class="arial14b">

                        
<!--La forma de pago ya no se lee de ninguna tabla, se ha introducido como textos en las tablas de idiomas, asi que simplemente mostramos el valor de las variables-->                        
                          <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 292'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>*&nbsp; &nbsp; &nbsp; 
						  <select name="FormaPago" id="FormaPago" onchange="datos_bancarios();">
						    <option selected="selected" value="-1">&nbsp;</option>
						    <option value="1"><?=$forma_pago1?></option>                            
						    <option value="2"><?=$forma_pago2?></option>                                                                                    
                          </select>                       
                        </td>
                      </tr>
                                            
					   <tr>
					   <td height="35" valign="middle" class="arial14b"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 139'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
					     *&nbsp;
                         <select name="EntidadBancaria" id="EntidadBancaria" disabled="disabled" onchange="asignar_codigo_entidad_bancaria();">
                           <option value="-1">&nbsp;</option>
<?php 

						   $consulta_bancos = mysql_query("SELECT * FROM EntidadesBancarias ORDER BY NombreBanco ASC ");
							while ($registro_banco = mysql_fetch_array($consulta_bancos)){
?>                            
                           <option value="<?=$registro_banco["NombreBanco"]."+".$registro_banco["CodigoOficial"]?>">
	                           <?= $registro_banco["NombreBanco"]?>
                           </option>
                           <?php }?>
                         </select></td>
					   </tr>                      
                                           
                    </table><br/><br/>
                    <table width="500" border="0" align="center" cellpadding="0" cellspacing="0" class="arial14">
                      <tr> 
                        <td width="450"><fieldset  >
                          <legend class="arialblack14">
                          <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 140'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                          </legend>
                          
                          <table width="400" align="center" border="0">
                          
                            <tr align="center"> 
                                <td width="390" height="10" valign="top">


                                <table width="380" border="0" cellpadding="0" cellspacing="0" height="10">
                                  <tr>
                                    <td width="50" height="10" valign="top"><table width="76" border="0" cellpadding="0" cellspacing="0">
                                      <tr>                                   
                                        <td width="50" height="10" align="center" valign="top" class="arial14">
                                  <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 141'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                                  *</td>
                              </tr>
                                    </table>                                    </td>
                                    <td width="4">&nbsp;</td>
                                    <td valign="top"><table width="70" border="0" cellpadding="0" cellspacing="0">

                                      <tr>
                                        <td width="92" height="10" align="center" valign="top" class="arial14">
                                  <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 142'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                                  *</td>
                              </tr>
                                    </table>                                    </td>
                                    <td width="120" valign="top"><table width="120" border="0" cellpadding="0" cellspacing="0">

                                      <tr >
                                        <td width="100" height="10" align="center" valign="top" class="arial14">
                                  <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 143'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?> 
                                  *  </td>
                              <td width="3" >&nbsp;</td>
                                      </tr>
                                    </table>                                    </td>
                                    <td width="97" valign="top"><table width="100" border="0" cellpadding="0" cellspacing="0">

                                      <tr>
                                        <td width="100" height="10" align="center" valign="top" class="arial14">
                                  <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 144'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                                  *</td>                                
                            </tr>
                                    </table>                                    </td>
                            </tr>
                                </table>                                </td>
                            </tr>
                            <tr align="center"> 
                              <td height="24" valign="top" class="arial14"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                                
                                <tr>
                                  <td width="55" height="22" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                                    
                                    <tr>
                                      <td width="55" height="22" align="center" valign="top" class="arial14"> <input name="banco" type="text" class="textfieldCentrado" id="banco" value="<?=$num_cuenta_banco?>" size="6" maxlength="4" disabled="disabled" /></td>
                              </tr>
                                  </table>                                  </td>
                                  <td width="23" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                                    
                                    <tr>
                                      <td width="4" height="22">&nbsp;</td>
                                      <td width="14" valign="top" class="arial14">&#8212;</td>
                              <td width="5">&nbsp;</td>
                                    </tr>
                                  </table>                                  </td>
                                  <td width="67" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                                    
                                    <tr>
                                      <td width="67" height="22" align="center" valign="top" class="arial14"> <input name="sucursal" type="text" class="textfieldCentrado" id="sucursal" value="<?=$num_cuenta_sucursal?>" size="6" maxlength="4" disabled="disabled"/></td>
                                      
                              </tr>
                                  </table>                                  </td>
                                  <td width="26" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                                    
                                    <tr>
                                      <td width="6" height="22">&nbsp;</td>
                                      <td width="14" valign="top" class="arial14">&#8212;</td>
                              <td width="6">&nbsp;</td>
                                    </tr>
                                  </table>                                  </td>
                                  <td width="76" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                    
                                    <tr>
                                      <td width="76" height="22" align="center" valign="top" class="arial14"> <input name="sucursal2" type="text" class="textfieldCentrado" id="sucursal2" value="<?=$num_cuenta_digito_control?>" size="4" maxlength="2" disabled="disabled"/></td>
                              </tr>
                                  </table>                                  </td>
                                  <td width="26" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                                    
                                    <tr>
                                      <td width="6" height="22">&nbsp;</td>
                                      <td width="14" valign="top" class="arial14">&#8212;</td>
                              <td width="6">&nbsp;</td>
                                    </tr>
                                  </table>                                  </td>
                                  <td width="119" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                                    
                                    <tr>
                                      <td width="119" height="22" align="center" valign="top" class="arial14"> <input name="sucursal3" type="text" class="textfieldCentrado" id="sucursal3" value="<?=$num_cuenta_cuenta?>" size="12" maxlength="10" disabled="disabled"/></td>
                            </tr>
                                  </table>                                  </td>
                            </tr>
                              </table>                                
                                                          </td>                                                          
                            </tr>
                            
                          </table>
                        </fieldset></td>
                      </tr>
   
                      <tr> 
                      	<td height="10">&nbsp; </td>
                      </tr>
                      
                                          
<!--CAMPO NUEVO-->

                      
                    </table>
                    
                    <br />
                  </fieldset></td>
                </tr>
                <tr class="arial12"> 
                  <td height="10" class="arial14"></td>
                  <td class="arial14"></td>
                </tr>
                    
                    
<!--APARTADO NUEVO OBSERVACIONES-->                   
                       <tr>
	                <td height="30" align="center" class="arial14">
                    
                    <input name="Condiciones" type="checkbox" value="Yes" id="Condiciones"/>He leído y Acepto las Condiciones de Modificación<br/>
                </tr>
	      
				<tr>
	                <td height="30" align="center" class="arial14">
                    <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 153'); $rowTexto = mysql_fetch_array($resultTexto);?>
                    <input name="submit" type="submit" id="submit" value="<?php echo($rowTexto['Texto']);?>"> 
                </tr>
                      
                <tr class="arial12"> 
                  <td height="20" class="arial14">&nbsp;</td>
                </tr>
               
              </table></td>
            </tr>
          </table>
          
          </form>



<!--VERSION VIEJA-->
                	
		</div><!--<div class="contenido_seccion_oficina">-->                        
		
        
        <div class="limpiar"></div>
                        	
<!--Por ultimo aparecera el pie de la web-->
<?php 
//	include($_SESSION["directorio_raiz"]."includes/pie.php");
	include("../includes/pie.php");

//NOTA: EN ESTA OCASION, EL ERROR SE MOSTRARA DESPUES DE LA CARGA DE LA PAGINA, SI NO POR ALGUN MOTIVO EN OCASIONES DESPUES DE DAR EL ERROR CESA LA CARGA DE LA PAGINA
	if(isset($_POST["submit"]))
	{		
//Por ultimo mostraremos el error correspondiente, en el caso de que haya fallado alguna de las verificaciones, o continuaremos con el proceso		
		switch($error)
		{		
//Proceso OK	
			case 0:						
//Antes de realizar el alta en la base de datos, deberemos de preparar los datos que no nos da el formulario

//Como en este caso el cliente ya existe, el codigo del cliente no hace falta calcularlo. Lo mismo sucede con el CUPS
				$cod_usuario=$row["CodigoCliente"];
				
				$procesoB="B";

								
//La forma de pago envia un indice y no la descripcion de la forma de pago, ahora lo sustituiremos antes de dar el alta
				switch($forma_pago)
				{
					case 1:
						$forma_pago="Domiciliacion";
					break;
					
					case 2:
						$forma_pago="No domiciliado";					
					break;					
				}//switch($forma_pago)
												
//El idioma de facturacion envia un indice y no la descripcion del idioma, ahora lo sustituiremos antes de dar el alta
				switch($idioma_factura)
				{
					case 0:
						$idioma_factura="Castellano";
					break;
					
					case 1:
						$idioma_factura="Catalán";					
					break;	

					case 2:
						$idioma_factura="Euskera";					
					break;	

					case 3:
						$idioma_factura="Gallego";					
					break;	

					case 4:
						$idioma_factura="Bable (Asturias)";					
					break;																				
				}//switch($idioma_factura)

								
//Tambien preparamos el numero de la cuenta que sera la concatenacion de todos los campos que lo forman
				$num_cuenta_completo=$num_cuenta_banco.$num_cuenta_sucursal.$num_cuenta_digito_control.$num_cuenta_cuenta;
				
//LOLOLO CONSULTAR DATOS PARA TIPO DE SUMINISTRO Y DATOS DEL SUMINISTRO

//A continuacion asignaremos el valor a las variables, que como no nos han hecho falta hasta ahora no habiamos asignado de la consulta de los datos actuales del cliente. En este caso los datos son los del suministro

				$suministro_poblacion=$row["SuministroCiudad"];
				$suministro_provincia=$row["SuministroProvincia"];				
				$suministro_calle=$row["SuministroCalle"];								
				$suministro_numero=$row["SuministroNumero"];				
				$suministro_extension=$row["SuministroExtension"];								
				$suministro_aclarador=$row["SuministroExtension2"];				
				$suministro_cp=$row["SuministroCP"];				
				$suministro_telefono=$row["SuministroTelefono1"];				
				$suministro_movil=$row["SuministroTelefono2"];								
				$suministro_fax=$row["SuministroFax"];												
				$suministro_email=$row["SuministroEmail"];																
				
				$tarifa_contratada=$row["TarifaContratada"];																
				$potencia_contratada=$row["PotenciaContratada"];																				
				$tipo_tension=$row["TipoTension"];				
				$discriminador=$row["Discriminador"];								
				$tipo_alquiler=$row["TipoAlquiler"];				
				$maximetro=$row["Maximetro"];								
				$modo=$row["Modo"];
				$observaciones=$row["Observaciones"];				
				$fases=$row["Fases"];				
				$reactiva=$row["Reactiva"];
								
				$p1_activa=$row["P1Activa"];				
				$p1_reactiva=$row["P1Reactiva"];				
				$p1_maximetro=$row["P1Maximetro"];			

				$p2_activa=$row["P2Activa"];				
				$p2_reactiva=$row["P2Reactiva"];				
				$p2_maximetro=$row["P2Maximetro"];			
				
				$p3_activa=$row["P3Activa"];				
				$p3_reactiva=$row["P3Reactiva"];				
				$p3_maximetro=$row["P3Maximetro"];			
				
				$p4_activa=$row["P4Activa"];				
				$p4_reactiva=$row["P4Reactiva"];				
				$p4_maximetro=$row["P4Maximetro"];			
				
				$p5_activa=$row["P5Activa"];				
				$p5_reactiva=$row["P5Reactiva"];				
				$p5_maximetro=$row["P5Maximetro"];			
				
				$p6_activa=$row["P6Activa"];				
				$p6_reactiva=$row["P6Reactiva"];				
				$p6_maximetro=$row["P6Maximetro"];																													
								
//Las modificaciones en realidad no cambian los datos de la tabla DatosRegistrados, si no que dan un alta en la tabla DatosModificados. Aunque se hace la inserccion completa, algunos campos quedaran en blanco, ya que se cambian desde la seccion de oficina_modificacion_datossuministro

//Ahora dependiendo de si el usuario ya tiene un registro o no en la tabla de DatosModificados, se realizara un alta nueva o una actualizacion del registro

					if($tiene_modificaciones_pendientes==0)
					{										
						$solicitud_modificacion="INSERT INTO `DatosModificados` (`CodigoCliente`, `Usuario`, `Password`, `DNI`, `SuministroCiudad`, `SuministroProvincia`, `SuministroCalle`, `SuministroNumero`, `SuministroExtension`, `SuministroExtension2`, `SuministroCP`, `SuministroTelefono1`, `SuministroTelefono2`, `SuministroFax`, `SuministroEmail`, `TitularNombre`, `TitularCalle`, `TitularNumero`, `TitularExtension`, `TitularAclarador`, `TitularPoblacion`, `TitularProvincia`, `TitularTelefono1`, `TitularTelefono2`, `TitularFax`, `TitularEmail`, `TitularDNI`, `FacturaNombre`, `FacturaCalle`, `FacturaNumero`, `FacturaExtension`, `FacturaAclarador`, `FacturaPoblacion`, `FacturaProvincia`, `FacturaTelefono1`, `FacturaTelefono2`, `FacturaFax`, `FacturaEmail`, `FacturaDNI`, `PagadorNombre`, `PagadorCalle`, `PagadorNumero`, `PagadorExtension`, `PagadorAclarador`, `PagadorPoblacion`, `PagadorProvincia`, `PagadorTelefono1`, `PagadorTelefono2`, `PagadorFax`, `PagadorEmail`, `PagadorDNI`, `RepresentanteEmpresa`, `DNIRepresentante`, `CUPS`, `FormaPago`, `BancoDomiciliado`, `NumeroCuenta`,
`IdiomaFactura`,`TarifaContratada`, `PotenciaContratada`, `TipoTension`, `Discriminador`, `TipoAlquiler`, `Maximetro`, `Modo`, `Observaciones`, `FechaRegistro`, `HoraRegistro`, `Fases`, `Reactiva`, `P1Activa`, `P1Reactiva`, `P1Maximetro`, `P2Activa`, `P2Reactiva`, `P2Maximetro`, `P3Activa`, `P3Reactiva`, `P3Maximetro`, `P4Activa`, `P4Reactiva`, `P4Maximetro`, `P5Activa`, `P5Reactiva`, `P5Maximetro`, `P6Activa`, `P6Reactiva`, `P6Maximetro`, `ProcesoT`, `ProcesoB`, `ProcesoS`) VALUES ('".$cod_usuario."', '".$_SESSION['usuario']."', '".$_SESSION['password']."', '".$_SESSION['DNI']."', '".$suministro_poblacion."', '".$suministro_provincia."', '".$suministro_calle."', '".$suministro_numero."', '".$suministro_extension."', '".$suministro_aclarador."', '".$suministro_cp."','".$suministro_telefono."', '".$suministro_movil."', '".$suministro_fax."', '".$suministro_email."', '".$titular_nombre."','".$titular_calle."','".$titular_numero."' , '".$titular_extension."', '".$titular_aclarador."', '".$titular_poblacion."', '".$titular_provincia."', '".$titular_telefono."', '".$titular_movil."', '".$titular_fax."', '".$titular_email."', '".$titular_dni."', '".$factura_nombre."', '".$factura_calle."', '".$factura_numero."', '".$factura_extension."', '".$factura_aclarador."', '".$factura_poblacion."', '".$factura_provincia."', '".$factura_telefono."', '".$factura_movil."', '".$factura_fax."', '".$factura_email."', '".$factura_dni."', '".$pagador_nombre."', '".$pagador_calle."', '".$pagador_numero."', '".$pagador_extension."', '".$pagador_aclarador."', '".$pagador_poblacion."', '".$pagador_provincia."', '".$pagador_telefono."', '".$pagador_movil."', '".$pagador_fax."', '".$pagador_email."', '".$pagador_dni."', '".$representante_nombre."', '".$representante_dni."', '".$_POST["CUPSFijo"]."', '".$forma_pago."', '".$partes_entidad_bancaria[0]."', '".$num_cuenta_completo."','".$idioma_factura."','".$tarifa_contratada."','".$potencia_contratada."', '".$tipo_tension."', '".$discriminador."','".$tipo_alquiler."','".$maximetro."', '".$modo."', '".$observaciones."', '".date("Y-m-d")."', '".date("Y-m-d H:i:s")."','".$fases."', '".$reactiva."', '".$p1_activa."', '".$p1_reactiva."', '".$p1_maximetro."', '".$p2_activa."', '".$p2_reactiva."', '".$p2_maximetro."', '".$p3_activa."', '".$p3_reactiva."', '".$p3_maximetro."', '".$p4_activa."', '".$p4_reactiva."', '".$p4_maximetro."', '".$p5_activa."', '".$p5_reactiva."', '".$p5_maximetro."', '".$p6_activa."', '".$p6_reactiva."', '".$p6_maximetro."', '".$procesoT."', '".$procesoB."', '".$procesoS."');";																												

				}//if($tiene_modificaciones_pendientes==0)

//Si el usuario ya tenia un registro en la base de datos, se realizara una actualizacion del registro				
				else				
				{
					$solicitud_modificacion="UPDATE `DatosModificados` SET `TitularNombre` = '".$titular_nombre."', `TitularCalle` = '".$titular_calle."',`TitularNumero` = '".$titular_numero."', `TitularExtension` = '".$titular_extension."',`TitularAclarador` = '".$titular_aclarador."', `TitularPoblacion` = '".$titular_poblacion."',`TitularProvincia` = '".$titular_provincia."',`TitularTelefono1` = '".$titular_telefono."', `TitularTelefono2` = '".$titular_movil."',`TitularFax` = '".$titular_fax."',`TitularEmail` = '".$titular_email."', `TitularDNI` = '".$titular_dni."',`FacturaNombre` = '".$factura_nombre."',`FacturaCalle` = '".$factura_calle."',`FacturaNumero` = '".$factura_numero."',`FacturaExtension` = '".$factura_extension."',`FacturaAclarador` = '".$factura_aclarador."',`FacturaPoblacion` = '".$factura_poblacion."',`FacturaProvincia` = '".$factura_provincia."',`FacturaTelefono1` = '".$factura_telefono."',`FacturaTelefono2` = '".$factura_movil."',`FacturaFax` = '".$factura_fax."',`FacturaEmail` = '".$factura_email."',`FacturaDNI` = '".$factura_dni."',`PagadorNombre` = '".$pagador_nombre."',`PagadorCalle` = '".$pagador_calle."',`PagadorNumero` = '".$pagador_numero."',`PagadorExtension` = '".$pagador_extension."',`PagadorAclarador` = '".$pagador_aclarador."',`PagadorPoblacion` = '".$pagador_poblacion."',`PagadorProvincia` = '".$pagador_provincia."',`PagadorTelefono1` = '".$pagador_telefono."',`PagadorTelefono2` = '".$pagador_movil."',`PagadorFax` = '".$pagador_fax."',`PagadorEmail` = '".$pagador_email."',`PagadorDNI` = '".$pagador_dni."',`RepresentanteEmpresa` = '".$representante_nombre."',`DNIRepresentante` = '".$representante_dni."',`FormaPago` = '".$forma_pago."',`BancoDomiciliado` = '".$partes_entidad_bancaria[0]."',`NumeroCuenta` = '".$num_cuenta_completo."',`IdiomaFactura` = '".$idioma_factura."',`Observaciones` = '".$observaciones."',`FechaRegistro` = '".date("Y-m-d")."',`HoraRegistro` = '".date("Y-m-d H:i:s")."',`ProcesoB` = '".$procesoB."' WHERE `CodigoCliente` =".$cod_usuario.";";
																													
				}//else($tiene_modificaciones_pendientes==0)

//En cualquier caso ejecutamos la sentencia SQL contruida				
				$ejecutar_solicitud_modificacion=mysql_query($solicitud_modificacion);
															
				MsgBox($modificacion_ok);											
			break;

//Error por no rellenar los campos obligatorios
			case 1:
				MsgBox($error_rellenar);				
			break;	

//Error por caracteres no numericos en telefonos o faxes						
			case 2:
				MsgBox($error_numeros);							
			break;	

//Error en un e-mail			
			case 3:
				MsgBox($error_email);										
			break;	

			case 4:
				MsgBox($error_dni);													
			break;										
							
//Error por no rellenar la entidad bancaria o el numero de cuenta con forma de pago domiciliación			
			case 5:
				MsgBox($error_datos_bancarios);			
			break;										
			
//Error en el numero de cuenta bancaria
			case 6:
				MsgBox($error_num_cuenta);						
			break;						

//Error por rellenar parcialmente los datos de facturacion o los del pagador
			case 7:
				MsgBox($error_datos_incompletos);									
			break;											

//Error por rellenar parcialmente los datos del representante de la empresa
			case 8:
				MsgBox($error_datos_representante);												
			break;																			
			
			case 9:
			    MsgBox($error_condiciones);//"No has aceptado las condiciones");												
			break;	
			
		}//switch($error)
	}//if (isset($_POST["submit"]))
}//else ($_SESSION['usuario'])
		
//En el caso en que se acepte el formulario, como la cuenta bancaria esta compuesta por	el nombre + codigo, habra que subdividirlo para pasarle el paranetro correctamente a la funcion javascript
	$partes_entidad_bancaria=explode("+",$entidad_bancaria);
	
//Llamamos a la funcion javascript que da valor a los datos bancarios. La funcion recibira los valores escritos en el formulario o los que hay en la base de datos dependidendo de la situacion
	echo("<script type='application/javascript' language='javascript'>");	
		echo("dar_valor_datos_bancarios('".$forma_pago."','".$partes_entidad_bancaria[0]."','".$idioma_factura."');");
	echo("</script>");				
	
?>        
    </div><!--<div id="central_oficina"-->
</body>
</html>

