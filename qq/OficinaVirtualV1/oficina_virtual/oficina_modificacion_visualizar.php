<?php
/*
NOTAS:

1- ESTA SECCION TOMA LOS DATOS DE LA TABLA DatosRegistrados

/* NOTA:

 COMO NO SE CIERRA LA CONEXION TRAS CADA CONEXION CON LA BASE DE DATOS, LAS CONSULTAS DEJAN DE FUNCIONAR SI SE ESTABLECE COMO UNICA CONEXION
 LA QUE UTILIZA EL RESTO DE LA WEB, POR LO TANTO HASTA QUE SE HAGA LA MIGRACION COMPLETA DE LOS TEXTOS, COEXISTIRAN LAS DOS CONEXIONES, YA QUE
 SE HA AÑADIDO UNA LLAMADA A LA CONEXION BUENA EN ESTE ARCHIVO.
*/

include("_conexion.php");

//Se incluye el archivo que contiene las verificaciones de datos
include("../includes/verificaciones.php");

//Como tienen textos en comun, para evitar el crecimiento innecesario de la base de datos, se incluye el archivo de textos de las secciones de datos del titular y del alta, ya que la mayoria de los textos son comunes

//include($_SESSION["directorio_raiz"]."idiomas/".$_SESSION["idioma"]."/oficina_datos_titular.php");
//include($_SESSION["directorio_raiz"]."idiomas/".$_SESSION["idioma"]."/oficina_datos_instalacion.php");

//include($_SESSION["directorio_raiz"]."idiomas/".$_SESSION["idioma"]."/oficina_contratacion_alta.php");
//include($_SESSION["directorio_raiz"]."idiomas/".$_SESSION["idioma"]."/oficina_modificacion_datostitular.php");
//include($_SESSION["directorio_raiz"]."idiomas/".$_SESSION["idioma"]."/oficina_modificacion_datossuministro.php");

//include($_SESSION["directorio_raiz"]."idiomas/".$_SESSION["idioma"]."/oficina_datos.php");

include("../idiomas/".$_SESSION["idioma"]."/oficina_datos_titular.php");
include("../idiomas/".$_SESSION["idioma"]."/oficina_datos_instalacion.php");

include("../idiomas/".$_SESSION["idioma"]."/oficina_contratacion_alta.php");
include("../idiomas/".$_SESSION["idioma"]."/oficina_modificacion_datostitular.php");
include("../idiomas/".$_SESSION["idioma"]."/oficina_modificacion_datossuministro.php");

include("../idiomas/".$_SESSION["idioma"]."/oficina_datos.php");
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<title><?=$titulo_web;?></title>

<!--Se añaden las hojas de estilos genericas que utiliza esta seccion-->
<link href="../css/estilos.css" rel="stylesheet" type="text/css" />
<link href="../css/secciones.css" rel="stylesheet" type="text/css" />
<link href="css/oficina.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_vieja.css" rel="stylesheet" type="text/css" />

<link href="css/oficina_modificacion_titular.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_modificacion_suministro.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_datos.css" rel="stylesheet" type="text/css" />

<!--Ahora se añaden las reglas para Internet Explorer. Asi corregiremos las diferencias de visualizacion entre navegadores
Es importante añadirlo despues de las reglas principales, para que se sobreescriban las correcciones pertinentes. En este caso las diferencias
solo son para internet explorer 7-->

<!--[if IE 7]>
	<link href="../css/estilos_ie7.css" rel="stylesheet" type="text/css" />
	<link href="../css/secciones_ie7.css" rel="stylesheet" type="text/css" />    
<![endif]-->







</head>
	
<body>
<!--*************************************************Web realizada por Óscar Rodríguez Calero****************************************-->	
	<div id="central_oficina">
    
<?php
//Se comprueba que el usuario se haya identificado, si no es asi se da un aviso y redirigimos a la seccion de identificacion
if ($_SESSION['usuario']=="")
{	
	MsgBox($error_identificacion);
	redirigir("oficina_index.php?id=".$_SESSION["idioma"]);
	
}//if ($_SESSION['usuario'])

//Si el usuario se ha identificado la seccion se ejecutara con normalidad
else
{
//Añadimos los fragmentos comunes, la cabecera, el menu, y el submenu
		include("oficina_fragmentos_comunes.php");
?>            
       
<div class="contenido_seccion_oficina_datos">
<!--VERSION VIEJA-->
<div class="contenido_ajustado">

             <div class="tipotablacentrado arialblack18Titulo">             
              		<tr align="center"> 
                    <td align="center" height="30" >Datos Modificados</td>
                    </tr>                                                                   		
              </div>                              
               
                <div class="tipotablacentrado" style="width: 600px;">
                  <tr>					
				  <td>
					<fieldset style="border-radius:10px; margin-bottom:10px;">
					    <table border="0">
                        <tr>
						   <td width="100"><img src="../img/oficina/info.png" width="60"/></td>
						   <td  style="font-size:12px; text-align: justify;">
                        Estos datos son los modificados por el usuario a falta de ser validados, en las próximas 48 horas por la Empresa correspondiente
							</td>
						</tr>
						</table>
					</fieldset>
					</td>
                  </tr>
                </div>
                
                <div class="tipotablacentrado">
                <tr>
                  <td width="590" height="10" style="height:10px"></td>
                  <td></td>
                </tr>
                </div>

         
<?php                       
//Esta variable controlara si el cliente tiene o no modificaciones pendientes
					$tiene_modificaciones_pendientes=0;

//Comprobamos si el cliente, tiene modificaciones pendientes
					$consulta_cliente=mysql_query("SELECT * FROM DatosModificados WHERE Usuario  = '".$_SESSION['usuario']."';");
					mysql_query("SET NAMES 'utf8'");
					$hay_cliente = mysql_fetch_array($consulta_cliente);
															
					if($hay_cliente)
					{
						$tiene_modificaciones_pendientes=1;
					}//if($hay_cliente)
					
//Si el cliente tiene modificaciones pendientes se mostrara un mensaje					
					if($tiene_modificaciones_pendientes==1)
					{
?>                  
	                <!--  <tr>
    	              	<td valign="middle" class="texto_advertencia"><?=$advertencia_cambios_pendientes?></td>
        	          </tr>   -->
                      
<?php				}//if($tiene_modificaciones_pendientes==1)

                  							
					$result = mysql_query("SELECT * FROM DatosModificados WHERE CodigoCliente = ".$_SESSION["usuario"]." ");
					$row = mysql_fetch_array($result);
					$codigocliente=$_SESSION["usuario"];					
					$cups=$row["CUPS"];
					$nombre_comercializadora=$row["ComercializadoraNombre"];
					$nombre_distribuidora=$row["EmpresaDistribuidora"];
					$fecha_alta_comercializadora2=fecha_normal($row["ComercializadoraFechaAlta"]);
					$fecha_baja_comercializadora2=fecha_normal($row["ComercializadoraFechaBaja"]);		
					
					
					if ($fecha_baja_comercializadora2==0){$fecha_baja_comercializadora='';} else {$fecha_baja_comercializadora=$fecha_baja_comercializadora2;}
					if ($fecha_alta_comercializadora2==0){$fecha_alta_comercializadora='';} else {$fecha_alta_comercializadora=$fecha_alta_comercializadora2;}

											
											
					
//En este caso como solamente se muestran los datos, las variables siempre deberan de tomar el valor que hay almacenado en la base de datos
					$titular_nombre=$row["TitularNombre"];
					$titular_calle=$row["TitularCalle"];
					$titular_numero2=$row["TitularNumero"];
					if ($titular_numero2==0){$titular_numero='';} else {$titular_numero=$titular_numero2;}

					$titular_extension=$row["TitularExtension"];
					$titular_aclarador=$row["TitularAclarador"];
					$titular_cp=$row["TitularCP"];
					$titular_poblacion=$row["TitularPoblacion"];
					$titular_provincia=$row["TitularProvincia"];
					$titular_telefono=$row["TitularTelefono1"];
					$titular_movil=$row["TitularTelefono2"];
					$titular_fax=$row["TitularFax"];
					$titular_email=$row["TitularEmail"];
					$titular_dni=$row["TitularDNI"];
							
					$factura_nombre=$row["FacturaNombre"];
					$factura_calle=$row["FacturaCalle"];
					$factura_numero2=$row["FacturaNumero"];
					if ($factura_numero2==0){$factura_numero='';} else {$factura_numero=$factura_numero2;}
					$factura_extension=$row["FacturaExtension"];
					$factura_aclarador=$row["FacturaAclarador"];
					$factura_cp=$row["FacturaCP"];
					$factura_poblacion=$row["FacturaPoblacion"];
					$factura_provincia=$row["FacturaProvincia"];
					$factura_telefono=$row["FacturaTelefono1"];
					$factura_movil=$row["FacturaTelefono2"];
					$factura_fax=$row["FacturaFax"];
					$factura_email=$row["FacturaEmail"];
					$factura_dni=$row["FacturaDNI"];
							
					$envio_nombre=$row["EnvioNombre"];
					$envio_calle=$row["EnvioCalle"];
					$envio_numero2=$row["EnvioNumero"];
					if ($envio_numero2==0){$envio_numero='';} else {$envio_numero=$envio_numero2;}
					$envio_extension=$row["EnvioExtension"];
					$envio_aclarador=$row["EnvioAclarador"];
					$envio_cp=$row["EnvioCP"];
					$envio_poblacion=$row["EnvioPoblacion"];
					$envio_provincia=$row["EnvioProvincia"];
					$envio_telefono=$row["EnvioTelefono1"];
					$envio_movil=$row["EnvioTelefono2"];
					$envio_fax=$row["EnvioFax"];
					$envio_email=$row["EnvioEmail"];
					$envio_dni=$row["EnvioDNI"];
									
							
							
							
							
					$pagador_nombre=$row["PagadorNombre"];
					$pagador_calle=$row["PagadorCalle"];
					
					
					$pagador_numero2=$row["PagadorNumero"];
					if ($pagador_numero2==0){$pagador_numero='';} else {$pagador_numero=$pagador_numero2;}
					
					$pagador_extension=$row["PagadorExtension"];
					$pagador_aclarador=$row["PagadorAclarador"];
					$pagador_CP=$row["PagadorCP"];
					$pagador_poblacion=$row["PagadorPoblacion"];
					$pagador_provincia=$row["PagadorProvincia"];
					$pagador_telefono=$row["PagadorTelefono1"];
					$pagador_movil=$row["PagadorTelefono2"];
					$pagador_fax=$row["PagadorFax"];
					$pagador_email=$row["PagadorEmail"];
					$pagador_dni=$row["PagadorDNI"];
							
					$representante_nombre=$row["RepresentanteEmpresa"];
					$representante_dni=$row["DNIRepresentante"];
													
//Ahora subdividiremos el numero de la cuenta de banco para escribir las cifras correspondientes en cada uno de los cuadros
					$num_cuenta_banco2=substr($row["NumeroCuenta"],0,4);
					$num_cuenta_sucursal2=substr($row["NumeroCuenta"],4,4);
					$num_cuenta_digito_control2=substr($row["NumeroCuenta"],8,2);
					$num_cuenta_cuenta2=substr($row["NumeroCuenta"],10,10);
							
					if ($num_cuenta_banco2==0){$num_cuenta_banco='';} else {$num_cuenta_banco=$num_cuenta_banco2;}
					if ($num_cuenta_sucursal2==0){$num_cuenta_sucursal='';} else {$num_cuenta_sucursal=$num_cuenta_sucursal2;}		
					if ($num_cuenta_digito_control2==0){$num_cuenta_digito_control='';} else {$num_cuenta_digito_control=$num_cuenta_digito_control2;}
					if ($num_cuenta_cuenta2==0){$num_cuenta_cuenta='';} else {$num_cuenta_cuenta=$num_cuenta_cuenta2;}
							
					//Ahora subdividiremos el numero del Iban de banco para escribir las cifras correspondientes en cada uno de los cuadros	
					$iban1_2=substr($row["Iban"],0,4);
					$iban2_2=substr($row["Iban"],4,4);
					$iban3_2=substr($row["Iban"],8,4);
					$iban4_2=substr($row["Iban"],12,4);
					$iban5_2=substr($row["Iban"],16,4);
					$iban6_2=substr($row["Iban"],20,4);
					$iban7_2=substr($row["Iban"],24,4);
					$iban8_2=substr($row["Iban"],20,4);
					$iban9_2=substr($row["Iban"],24,4);
					//if ($iban1_2==0){$iban1='';} else {$iban1=$iban1_2;}
					if ($iban2_2==0){$iban2='';} else {$iban2=$iban2_2;}		
					if ($iban3_2==0){$iban3='';} else {$iban3=$iban3_2;}
					if ($iban4_2==0){$iban4='';} else {$iban4=$iban4_2;}	
					if ($iban5_2==0){$iban5='';} else {$iban5=$iban5_2;}
					if ($iban6_2==0){$iban6='';} else {$iban6=$iban6_2;}
					if ($iban7_2==0){$iban7='';} else {$iban7=$iban7_2;}
					if ($iban8_2==0){$iban8='';} else {$iban6=$iban8_2;}
					if ($iban9_2==0){$iban9='';} else {$iban7=$iban9_2;}
					
					$banco_extranjero=$row["BancoExtranjero"];
					if ($banco_extranjero==""){$banco_extranjero='No';} else {$banco_extranjero='Si';}
							
							
					$forma_pago=$row["FormaPago"];
					$idioma_factura=$row["IdiomaFactura"];	
					$entidad_bancaria=$row["BancoDomiciliado"];									
													
					$suministro_poblacion=$row["SuministroCiudad"];
					$suministro_provincia=$row["SuministroProvincia"];				
					$suministro_calle=$row["SuministroCalle"];								
					$suministro_numero=$row["SuministroNumero"];				
					$suministro_extension=$row["SuministroExtension"];								
					$suministro_aclarador=$row["SuministroExtension2"];				
					$suministro_cp=$row["SuministroCP"];				
					$suministro_telefono=$row["SuministroTelefono1"];				
					$suministro_movil=$row["SuministroTelefono2"];								
					$suministro_fax=$row["SuministroFax"];												
					$suministro_email=$row["SuministroEmail"];		
																
					$tipo_tension=$row["TipoTension"];									
					$equipos_medida=$row["TipoAlquiler"];
					$maximetro=$row["Maximetro"];
					$modo=$row["Modo"];
					$fases=$row["Fases"];
					$reactiva=$row["Reactiva"];				
								
					$p1_activa=$row["P1Activa"];				
					$p1_reactiva=$row["P1Reactiva"];				
					$p1_maximetro=$row["P1Maximetro"];			
				
					$p2_activa=$row["P2Activa"];				
					$p2_reactiva=$row["P2Reactiva"];				
					$p2_maximetro=$row["P2Maximetro"];			
								
					$p3_activa=$row["P3Activa"];				
					$p3_reactiva=$row["P3Reactiva"];				
					$p3_maximetro=$row["P3Maximetro"];			
								
					$p4_activa=$row["P4Activa"];				
					$p4_reactiva=$row["P4Reactiva"];				
					$p4_maximetro=$row["P4Maximetro"];			
								
					$p5_activa=$row["P5Activa"];				
					$p5_reactiva=$row["P5Reactiva"];				
					$p5_maximetro=$row["P5Maximetro"];			
							
					$p6_activa=$row["P6Activa"];				
					$p6_reactiva=$row["P6Reactiva"];				
					$p6_maximetro=$row["P6Maximetro"];																									
						
//Cambiaremos el valor del discrminador almacenado en la base de datos						
					if($row["Discriminador"]=="a")
					{
						$discriminador="DHA";				
					}//if($row["Discriminador"]=="a")
?>                        


		
		<div class="tipotabla" style="width: 600px;">
                    
            <fieldset>
                <legend class="arialblack14"> <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 287'); 
			  $rowTexto = mysql_fetch_array($resultTexto);
			  echo($rowTexto['Texto']);?> 
                </legend>
                
           
          
                
              <tr>
             
              <td width="590" height="22" valign="top" class="arial14">
			    <div class="margen_superior_inicial_datos_titular" align="left">
			      <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 287'); 
			  $rowTexto = mysql_fetch_array($resultTexto);
			  echo($rowTexto['Texto']);?> 
			      
			      <input name="CUPS"  value="<?=$cups?>" type="text" class="textfieldLecturaCentrado"  size="25" maxlength="10" style="height:20px" readonly="readonly"/>
			      &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
			      
			      <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 196'); 
			  $rowTexto = mysql_fetch_array($resultTexto);
			  echo($rowTexto['Texto']);?> 
			      
			      
			      <input name="numero"  value="<?=$_SESSION["usuario"]?>" type="text" class="textfieldLecturanum"  size="10" maxlength="10" readonly="readonly"/>
			      
			      
			      
		        </div></td>
                </tr>
                <div class="limpiar"></div>
               <div class="espacio"></div>
            
            <tr>
              <td width="600" height="12"></td>
            </tr>          
                       
            </fieldset>
          </td>
          </tr>
          
       </div>
          
          <tr>
            <td height="18"></td>
          </tr>


    
             	                
<!--SEPARACION ENTRE APARTADOS-->    

<!--SEPARACION ENTRE APARTADOS-->

<div class="tipotabla" style="width: 600px;">
		
        
      <fieldset>
           <legend class="arialblack14"><?=$oficina_datos_titular3?>&nbsp;</legend>
                                       
             <div class="arial14 margen_texto_titular margen_superior_inicial_datos_titular">
               <?=$oficina_datos_titular4?>
           </div>
             <div class="margen_campo_titular margen_superior_inicial_datos_titular">
               <input name="TitularNombre2" value="<?=$titular_nombre?>" type="text" class="textfieldLectura" id="TitularNombre2" size="52" maxlength="50" readonly="readonly" />
           </div>
             
           <div class="limpiar"></div>
             
             <div class="margen_texto_titular arial14">
               <?=$oficina_datos_titular5?>
         </div>
             <div class="margen_campo_titular">
               <input name="TitularCalle2" value="<?=$titular_calle?>" type="text" class="textfieldLectura" id="TitularCalle2" size="52" maxlength="50" readonly="readonly"/>
           </div>

<!--DOS CAMPOS EN LA MISMA LINEA-->
           <div class="limpiar"></div>
             
             <div class="margen_texto_titular arial14">
               <?=$oficina_datos_titular6?>
         </div>
             <div class="margen_campo_titular">
               <input name="TitularNumero2" value="<?=$titular_numero?>" type="text" class="textfieldLecturanum" id="TitularNumero2"  size="5" maxlength="4" readonly="readonly"/>
         </div>                          

             <div class="margen_texto_titular_extension arial14">
               &nbsp;<?=$oficina_datos_titular7?>
         </div>
             <div class="margen_campo_titular">
               <input name="TitularExtension2" type="text" class="textfieldLectura" id="TitularExtension2" value="<?=$titular_extension?>"  size="27" maxlength="50" readonly="readonly"/>
           </div>                                                

<!--DOS CAMPOS EN LA MISMA LINEA-->
             
           <div class="limpiar"></div>                          
                          
             <div class="margen_texto_titular arial14">
               <?=$oficina_datos_titular8?>
         </div>
             <div class="margen_campo_titular">
               <input name="TitularAclarador2" value="<?=$titular_aclarador?>" type="text" class="textfieldLectura" id="TitularAclarador2" size="52" maxlength="50" readonly="readonly"/>
           </div>  
             
             
             
                 <div class="limpiar"></div>                          

             <div class="margen_texto_titular arial14"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 289'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?></div>
             <div class="margen_campo_titular"><input name="TitularCP" type="text" class="textfieldLecturanum" value="<?=$titular_cp?>"  id="TitularCP2"  size="6" maxlength="10" readonly="readonly"/></div>  
             
             
             
           <div class="limpiar"></div>                          
             
             <div class="margen_texto_titular arial14">
               <?=$oficina_datos_titular9?>
         </div>
             <div class="margen_campo_titular">
               <input name="TitularPoblacion2" type="text"  value="<?=$titular_poblacion?>"  class="textfieldLectura" id="TitularPoblacion2" size="52" maxlength="50" readonly="readonly"/>
           </div>  

           <div class="limpiar"></div>                          
             
             <div class="margen_texto_titular arial14">
               <?=$oficina_datos_titular10?>
         </div>
             <div class="margen_campo_titular">
               <input name="TitularProvincia2" type="text"  value="<?=$titular_provincia?>"  class="textfieldLectura" id="TitularProvincia2" size="52" maxlength="50" readonly="readonly"/>
           </div>  

           <div class="limpiar"></div>                          
                          
             <div class="margen_texto_titular arial14">
               <?=$oficina_datos_titular11?>
         </div>
             <div class="margen_campo_titular">
               <input name="TitularTelefono2" type="text" class="textfieldLecturaCentrado" value="<?=$titular_telefono?>"  id="TitularTelefono2" size="10" maxlength="10" readonly="readonly"/>
         </div>                          

             <div class="margen_texto_titular_extension arial14 margen_campo_movil">
               <?=$oficina_datos_titular12?>
         </div>
             <div class="margen_campo_titular_movil">
               <input name="TitularMovil2" value="<?=$titular_movil?>"  type="text" class="textfieldLecturaCentrado" id="TitularMovil2" size="10" maxlength="10" readonly="readonly"/>
           </div>              

           <div class="limpiar"></div>             
                            
             <div class="margen_texto_titular arial14">
               <?=$oficina_datos_titular13?>
         </div>
             <div class="margen_campo_titular">
               <input name="TitularFax2" value="<?=$titular_fax?>"  type="text" class="textfieldLecturaCentrado" id="TitularFax2" size="10" maxlength="10" readonly="readonly"/>
           </div>                 
             
           <div class="limpiar"></div>             
                            
             <div class="margen_texto_titular arial14">
               <?=$oficina_datos_titular14?>
         </div>
             <div class="margen_campo_titular">
               <input name="TitularEmail2" value="<?=$titular_email?>"  type="text" class="textfieldLectura" id="TitularEmail2" size="52" maxlength="50" readonly="readonly"/>
           </div>                              
             
           <div class="limpiar"></div>             
                            
             <div class="margen_texto_titular arial14 margen_inferior_final_datos_titular">
               <?=$oficina_datos_titular15?>
         </div>
             <div class="margen_campo_titular margen_inferior_final_datos_titular">
               <input name="TitularDNI2" value="<?=$titular_dni?>"  type="text" class="textfieldLecturaCentrado" id="TitularDNI2" size="15" maxlength="15" readonly="readonly"/>
           </div>             
               
                                                                 	        
      </fieldset>
            
            
    </div>
         
         
         
         
         
         
         
         
         
         
         
         
         

          
<div class="tipotabla" style="width: 600px;">
		
        
        <fieldset>
             <legend class="arialblack14">Datos Facturación</legend>
                                       
             <div class="arial14 margen_texto_titular margen_superior_inicial_datos_titular"><?=$oficina_datos_titular17?></div>
             <div class="margen_campo_titular margen_superior_inicial_datos_titular"><input name="FacturaNombre" value="<?=$factura_nombre?>" type="text" class="textfieldLectura" id="FacturaNombre" size="52" maxlength="50" readonly="readonly"/></div>
             
             <div class="limpiar"></div>
             
             <div class="margen_texto_titular arial14"><?=$oficina_datos_titular18?></div>
             <div class="margen_campo_titular"><input name="FacturaCalle" value="<?=$factura_calle?>" type="text" class="textfieldLectura" id="FacturaCalle" size="52" maxlength="50" readonly="readonly"/></div>

<!--DOS CAMPOS EN LA MISMA LINEA-->
             <div class="limpiar"></div>
             
             <div class="margen_texto_titular arial14"><?=$oficina_datos_titular19?></div>
             <div class="margen_campo_titular"><input name="FacturaNumero" value="<?=$factura_numero?>" type="text" class="textfieldLecturanum" id="FacturaNumero"  size="5" maxlength="4" readonly="readonly"/></div>                          

             <div class="margen_texto_titular_extension arial14">&nbsp;<?=$oficina_datos_titular20?></div>
             <div class="margen_campo_titular"><input name="FacturaExtension" type="text" class="textfieldLectura" id="FacturaExtension" value="<?=$factura_extension?>"  size="27" maxlength="50" readonly="readonly"/></div>                                                

<!--DOS CAMPOS EN LA MISMA LINEA-->
             
             <div class="limpiar"></div>                          
                          
             <div class="margen_texto_titular arial14"><?=$oficina_datos_titular21?></div>
             <div class="margen_campo_titular"><input name="FacturaAclarador" value="<?=$factura_aclarador?>" type="text" class="textfieldLectura" id="FacturaAclarador" size="52" maxlength="50" readonly="readonly"/></div>  
             
             
             
                 <div class="limpiar"></div>                          

             <div class="margen_texto_titular arial14"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 289'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?></div>
             <div class="margen_campo_titular"><input name="FacturaCP" type="text" class="textfieldLecturanum" value="<?=$factura_cp?>"  id="FacturaCP2"  size="6" maxlength="10" readonly="readonly"/></div>  
             
             
             
             
             <div class="limpiar"></div>                          
             
                          <div class="margen_texto_titular arial14"><?=$oficina_datos_titular22?></div>
             <div class="margen_campo_titular"><input name="FacturaPoblacion" type="text"  value="<?=$factura_poblacion?>"  class="textfieldLectura" id="FacturaPoblacion" size="52" maxlength="50" readonly="readonly"/></div>  

             <div class="limpiar"></div>                          
             
             <div class="margen_texto_titular arial14"><?=$oficina_datos_titular23?></div>
             <div class="margen_campo_titular"><input name="FacturaProvincia" type="text"  value="<?=$factura_provincia?>"  class="textfieldLectura" id="FacturaProvincia" size="52" maxlength="50" readonly="readonly"/></div>  

             <div class="limpiar"></div>                          
                          
             <div class="margen_texto_titular arial14"><?=$oficina_datos_titular24?></div>
             <div class="margen_campo_titular"><input name="FacturaTelefono" type="text" class="textfieldLecturaCentrado" value="<?=$factura_telefono?>"  id="FacturaTelefono" size="10" maxlength="10" readonly="readonly"/></div>                          

             <div class="margen_texto_titular_extension arial14 margen_campo_movil"><?=$oficina_datos_titular25?></div>
             <div class="margen_campo_titular_movil"><input name="FacturaMovil" value="<?=$factura_movil?>"  type="text" class="textfieldLecturaCentrado" id="FacturaMovil" size="10" maxlength="10" readonly="readonly"/></div>              

             <div class="limpiar"></div>             
                            
             <div class="margen_texto_titular arial14"><?=$oficina_datos_titular26?></div>
             <div class="margen_campo_titular"><input name="FacturaFax" value="<?=$factura_fax?>"  type="text" class="textfieldLecturaCentrado" id="FacturaFax" size="10" maxlength="10" readonly="readonly"/></div>                 
             
             <div class="limpiar"></div>             
                            
             <div class="margen_texto_titular arial14"><?=$oficina_datos_titular27?></div>
             <div class="margen_campo_titular"><input name="FacturaEmail" value="<?=$factura_email?>"  type="text" class="textfieldLectura" id="FacturaEmail" size="52" maxlength="50" readonly="readonly"/></div>                              
             
             <div class="limpiar"></div>             
                            
             <div class="margen_texto_titular arial14 margen_inferior_final_datos_titular"><?=$oficina_datos_titular28?></div>
             <div class="margen_campo_titular margen_inferior_final_datos_titular"><input name="FacturaDNI" value="<?=$factura_dni?>"  type="text" class="textfieldLecturaCentrado" id="FacturaDNI" size="15" maxlength="15" readonly="readonly"/></div>             
               
                                                                 	        
            </fieldset>
            
            
    </div>
    
    
    
    
    
    
    
    
    
    
    
<div class="tipotabla" style="width: 600px;">
		
        
        <fieldset>
             <legend class="arialblack14">Datos Envío</legend>
                                       
             <div class="arial14 margen_texto_titular margen_superior_inicial_datos_titular"><?=$oficina_datos_titular17?></div>
             <div class="margen_campo_titular margen_superior_inicial_datos_titular"><input name="EnvioNombre" value="<?=$envio_nombre?>" type="text" class="textfieldLectura" id="envioNombre" size="52" maxlength="50" readonly="readonly"/></div>
             
             <div class="limpiar"></div>
             
             <div class="margen_texto_titular arial14"><?=$oficina_datos_titular18?></div>
             <div class="margen_campo_titular"><input name="EnvioCalle" value="<?=$envio_calle?>" type="text" class="textfieldLectura" id="EnvioCalle" size="52" maxlength="50" readonly="readonly"/></div>

<!--DOS CAMPOS EN LA MISMA LINEA-->
             <div class="limpiar"></div>
             
             <div class="margen_texto_titular arial14"><?=$oficina_datos_titular19?></div>
             <div class="margen_campo_titular"><input name="EnvioNumero" value="<?=$envio_numero?>" type="text" class="textfieldLecturanum" id="EnvioNumero"  size="5" maxlength="4" readonly="readonly"/></div>                          

             <div class="margen_texto_titular_extension arial14">&nbsp;<?=$oficina_datos_titular20?></div>
             <div class="margen_campo_titular"><input name="EnvioExtension" type="text" class="textfieldLectura" id="EnvioExtension" value="<?=$envio_extension?>"  size="27" maxlength="50" readonly="readonly"/></div>                                                

<!--DOS CAMPOS EN LA MISMA LINEA-->
             
             <div class="limpiar"></div>                          
                          
             <div class="margen_texto_titular arial14"><?=$oficina_datos_titular21?></div>
             <div class="margen_campo_titular"><input name="EnvioAclarador" value="<?=$envio_aclarador?>" type="text" class="textfieldLectura" id="EnvioAclarador" size="52" maxlength="50" readonly="readonly"/></div>  
             
             
             
             
             
                 <div class="limpiar"></div>                          

             <div class="margen_texto_titular arial14"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 289'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?></div>
             <div class="margen_campo_titular"><input name="EnvioCP" type="text" class="textfieldLecturanum" value="<?=$envio_cp?>"  id="EnvioCP2"  size="6" maxlength="10" readonly="readonly"/></div>  
             
             
             
             
             
             
             <div class="limpiar"></div>    
             
                                   
             
             <div class="margen_texto_titular arial14"><?=$oficina_datos_titular22?></div>
             <div class="margen_campo_titular"><input name="EnvioPoblacion" type="text"  value="<?=$envio_poblacion?>"  class="textfieldLectura" id="EnvioPoblacion" size="52" maxlength="50" readonly="readonly"/></div>  

             <div class="limpiar"></div>                          
             
             <div class="margen_texto_titular arial14"><?=$oficina_datos_titular23?></div>
             <div class="margen_campo_titular"><input name="EnvioProvincia" type="text"  value="<?=$envio_provincia?>"  class="textfieldLectura" id="EnvioProvincia" size="52" maxlength="50" readonly="readonly"/></div>  

             <div class="limpiar"></div>                          
                          
             <div class="margen_texto_titular arial14"><?=$oficina_datos_titular24?></div>
             <div class="margen_campo_titular"><input name="EnvioTelefono" type="text" class="textfieldLecturaCentrado" value="<?=$envio_telefono?>"  id="EnvioTelefono" size="10" maxlength="10" readonly="readonly"/></div>                          

             <div class="margen_texto_titular_extension arial14 margen_campo_movil"><?=$oficina_datos_titular25?></div>
             <div class="margen_campo_titular_movil"><input name="EnvioMovil" value="<?=$envio_movil?>"  type="text" class="textfieldLecturaCentrado" id="EnvioMovil" size="10" maxlength="10" readonly="readonly"/></div>              

             <div class="limpiar"></div>             
                            
             <div class="margen_texto_titular arial14"><?=$oficina_datos_titular26?></div>
             <div class="margen_campo_titular"><input name="EnvioFax" value="<?=$envio_fax?>"  type="text" class="textfieldLecturaCentrado" id="EnvioFax" size="10" maxlength="10" readonly="readonly"/></div>                 
             
             <div class="limpiar"></div>             
                            
             <div class="margen_texto_titular arial14"><?=$oficina_datos_titular27?></div>
             <div class="margen_campo_titular"><input name="EnvioEmail" value="<?=$envio_email?>"  type="text" class="textfieldLectura" id="EnvioEmail" size="52" maxlength="50" readonly="readonly"/></div>                              
             
             <div class="limpiar"></div>             
                            
             <div class="margen_texto_titular arial14 margen_inferior_final_datos_titular"><?=$oficina_datos_titular28?></div>
             <div class="margen_campo_titular margen_inferior_final_datos_titular"><input name="envioDNI" value="<?=$envio_dni?>"  type="text" class="textfieldLecturaCentrado" id="envioDNI" size="15" maxlength="15" readonly="readonly"/></div>             
               
                                                                 	        
            </fieldset>
            
            
    </div>
    
    
    
    
    
    
    
    
    
    
    
    
    
    
            

<div class="tipotabla" style="width: 600px;">
		
        
        <fieldset>
             <legend class="arialblack14"><?=$oficina_datos_titular29?>&nbsp;</legend>
                                       
             <div class="arial14 margen_texto_titular margen_superior_inicial_datos_titular"><?=$oficina_datos_titular30?></div>
             <div class="margen_campo_titular margen_superior_inicial_datos_titular"><input name="PagadorNombre" value="<?=$pagador_nombre?>" type="text" class="textfieldLectura" id="PagadorNombre" size="52" maxlength="50" readonly="readonly"/></div>
             
             <div class="limpiar"></div>
             
             <div class="margen_texto_titular arial14"><?=$oficina_datos_titular31?></div>
             <div class="margen_campo_titular"><input name="PagadorCalle" value="<?=$pagador_calle?>" type="text" class="textfieldLectura" id="PagadorCalle" size="52" maxlength="50" readonly="readonly"/></div>

<!--DOS CAMPOS EN LA MISMA LINEA-->
             <div class="limpiar"></div>
             
             <div class="margen_texto_titular arial14"><?=$oficina_datos_titular32?></div>
             <div class="margen_campo_titular"><input name="PagadorNumero" value="<?=$pagador_numero?>" type="text" class="textfieldLecturanum" id="PagadorNumero"  size="5" maxlength="4" readonly="readonly"/></div>                          

             <div class="margen_texto_titular_extension arial14">&nbsp;<?=$oficina_datos_titular33?></div>
             <div class="margen_campo_titular"><input name="PagadorExtension" type="text" class="textfieldLectura" id="PagadorExtension" value="<?=$pagador_extension?>"  size="27" maxlength="50" readonly="readonly"/></div>                                                

<!--DOS CAMPOS EN LA MISMA LINEA-->
             
             <div class="limpiar"></div>                          
                          
             <div class="margen_texto_titular arial14"><?=$oficina_datos_titular34?></div>
             <div class="margen_campo_titular"><input name="PagadorAclarador" value="<?=$pagador_aclarador?>" type="text" class="textfieldLectura" id="PagadorAclarador" size="52" maxlength="50" readonly="readonly"/></div>  
             
             
                 <div class="limpiar"></div>                          

             <div class="margen_texto_titular arial14"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 289'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?></div>
             <div class="margen_campo_titular"><input name="PagadorCP" type="text" class="textfieldLecturanum" value="<?=$pagador_cp?>"  id="PagadorCP2"  size="6" maxlength="10" readonly="readonly"/></div>  
             
             
             
             <div class="limpiar"></div>                          
             
             <div class="margen_texto_titular arial14"><?=$oficina_datos_titular35?></div>
             <div class="margen_campo_titular"><input name="PagadorPoblacion" type="text"  value="<?=$pagador_poblacion?>"  class="textfieldLectura" id="PagadorPoblacion" size="52" maxlength="50" readonly="readonly"/></div>  

             <div class="limpiar"></div>                          
             
             <div class="margen_texto_titular arial14"><?=$oficina_datos_titular36?></div>
             <div class="margen_campo_titular"><input name="PagadorProvincia" type="text"  value="<?=$pagador_provincia?>"  class="textfieldLectura" id="PagadorProvincia" size="52" maxlength="50" readonly="readonly"/></div>  

             <div class="limpiar"></div>                          
                          
             <div class="margen_texto_titular arial14"><?=$oficina_datos_titular37?></div>
             <div class="margen_campo_titular"><input name="PagadorTelefono" type="text" class="textfieldLecturaCentrado" value="<?=$pagador_telefono?>"  id="PagadorTelefono" size="10" maxlength="10" readonly="readonly"/></div>                          

             <div class="margen_texto_titular_extension arial14 margen_campo_movil"><?=$oficina_datos_titular38?></div>
             <div class="margen_campo_titular_movil"><input name="PagadorMovil" value="<?=$pagador_movil?>"  type="text" class="textfieldLecturaCentrado" id="PagadorMovil" size="10" maxlength="10" readonly="readonly"/></div>              

			 <div class="limpiar"></div>             
                            
             <div class="margen_texto_titular arial14"><?=$oficina_datos_titular39?></div>
             <div class="margen_campo_titular"><input name="PagadorFax" value="<?=$pagador_fax?>"  type="text" class="textfieldLecturaCentrado" id="PagadorFax" size="10" maxlength="10" readonly="readonly"/></div>                 
             
             <div class="limpiar"></div>             
                            
             <div class="margen_texto_titular arial14"><?=$oficina_datos_titular40?></div>
             <div class="margen_campo_titular"><input name="PagadorEmail" value="<?=$pagador_email?>"  type="text" class="textfieldLectura" id="PagadorEmail" size="52" maxlength="50" readonly="readonly"/></div>                              
             
             <div class="limpiar"></div>             
                            
             <div class="margen_texto_titular arial14 margen_inferior_final_datos_titular"><?=$oficina_datos_titular41?></div>
             <div class="margen_campo_titular margen_inferior_final_datos_titular"><input name="PagadorDNI" value="<?=$pagador_dni?>"  type="text" class="textfieldLecturaCentrado" id="PagadorDNI" size="15" maxlength="15" readonly="readonly"/></div>             
               
                                                                 	        
            </fieldset>
   </div>
         
         
         
         
         
       <div class="tipotabla" style="width: 600px;">
       
       
                
                <fieldset>
                    <legend class="arialblack14">
                    <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 133'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                    </legend>
                    
					
                    
                    <table width="450" align="center">
                      
                      <tr> 
                        <td width="97" height="22" valign="top" class="arial14">
                          <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 134'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                        &nbsp;</td>
                        
                      <td width="341" valign="top"><input name="RepresentanteEmpresa" type="text" class="textfieldLectura" id="RepresentanteEmpresa" value="<?=$representante_nombre?>" size="50" maxlength="50" readonly="readonly"/></td>
                      
                      </tr>
                      
                      <tr> 
                        <td height="22" valign="top" class="arial14">
                          <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 118'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                        &nbsp;</td>
                      <td valign="top"><input name="DNIRepresentante" type="text" class="textfieldLectura" id="DNIRepresentante" value="<?=$representante_dni?>" size="11" maxlength="15" readonly="readonly"/></td>
             </tr>
             
             <div class="limpiar"></div>
         <div class="espacio"></div>
             
             </table>
             <div class="limpiar"></div>
         <div class="espacio"></div>
                    
         </fieldset>
         
         
                
   </div>
           
           
           
           
           
                
   
  <div class="tipotabla" style="width: 600px;">
   
   
   <fieldset  >
                    <legend class="arialblack14">
                    <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 138'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                    </legend>
                    

                    
                    
                    <table width="450" align="center">
                      <tr> 
                        <td width="450" class="arial14">
<!--La forma de pago ya no se lee de ninguna tabla, se ha introducido como textos en las tablas de idiomas, asi que simplemente mostramos el valor de las variables-->                        
                      <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 292'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>&nbsp; &nbsp; &nbsp;&nbsp;
                          
                          <input name="FormaPago" type="text" class="textfieldLectura" id="FormaPago" value="<?=$forma_pago?>" size="40" maxlength="40" readonly="readonly"/>                          
                        </td>
                      </tr>
                                            
					   <tr>
					   <td width="450" class="arial14"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 139'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
					     &nbsp;&nbsp;&nbsp;
                         
                         <input name="EntidadBancaria" type="text" class="textfieldLectura" id="EntidadBancaria" value="<?=$entidad_bancaria?>" size="40" maxlength="40" readonly="readonly"/></td>
					   </tr>                      
                                           
                    </table><br/><br/>
                    <table width="500" border="0" align="center" cellpadding="0" cellspacing="0" class="arial14">
                      <tr> 
                        <td width="450"><fieldset  >
                          <legend class="arialblack14">
                          <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 140'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                          </legend>
                          
                          <table width="400" align="center" border="0">
                          
                            <tr align="center"> 
                                <td width="390" height="10" valign="top">


                                <table width="380" border="0" cellpadding="0" cellspacing="0" height="10">
                                  <tr>
                                    <td width="50" height="10" valign="top"><table width="76" border="0" cellpadding="0" cellspacing="0">
                                      <tr>                                   
                                        <td width="50" height="10" align="center" valign="top" class="arial14">
                                  <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 141'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                                  </td>
                              </tr>
                                    </table>                                    </td>
                                    <td width="4">&nbsp;</td>
                                    <td valign="top"><table width="70" border="0" cellpadding="0" cellspacing="0">

                                      <tr>
                                        <td width="92" height="10" align="center" valign="top" class="arial14">
                                  <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 142'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                                  </td>
                              </tr>
                                    </table>                                    </td>
                                    <td width="120" valign="top"><table width="120" border="0" cellpadding="0" cellspacing="0">

                                      <tr >
                                        <td width="100" height="10" align="center" valign="top" class="arial14">
                                  <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 143'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?> 
                                    </td>
                              <td width="3" >&nbsp;</td>
                                      </tr>
                                    </table>                                    </td>
                                    <td width="97" valign="top"><table width="100" border="0" cellpadding="0" cellspacing="0">

                                      <tr>
                                        <td width="100" height="10" align="center" valign="top" class="arial14">
                                  <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 144'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                                  </td>                                
                            </tr>
                                    </table>                                    </td>
                            </tr>
                                </table>                                </td>
                            </tr>
                            <tr align="center"> 
                              <td height="24" valign="top" class="arial14"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                                
                                <tr>
                                  <td width="55" height="22" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                                    
                                    <tr>
                                      <td width="55" height="22" align="center" valign="top" class="arial14"> <input name="banco" type="text" class="textfieldLecturaCentrado" id="banco" value="<?=$num_cuenta_banco?>" size="6" maxlength="4" readonly="readonly"/></td>
                              </tr>
                                  </table>                                  </td>
                                  <td width="23" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                                    
                                    <tr>
                                      <td width="4" height="22">&nbsp;</td>
                                      <td width="14" valign="top" class="arial14">&#8212;</td>
                              <td width="5">&nbsp;</td>
                                    </tr>
                                  </table>                                  </td>
                                  <td width="67" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                                    
                                    <tr>
                                      <td width="67" height="22" align="center" valign="top" class="arial14"> <input name="sucursal" type="text" class="textfieldLecturaCentrado" id="sucursal" value="<?=$num_cuenta_sucursal?>" size="6" maxlength="4" readonly="readonly"/></td>
                                      
                              </tr>
                                  </table>                                  </td>
                                  <td width="26" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                                    
                                    <tr>
                                      <td width="6" height="22">&nbsp;</td>
                                      <td width="14" valign="top" class="arial14">&#8212;</td>
                              <td width="6">&nbsp;</td>
                                    </tr>
                                  </table>                                  </td>
                                  <td width="76" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                    
                                    <tr>
                                      <td width="76" height="22" align="center" valign="top" class="arial14"> <input name="sucursal2" type="text" class="textfieldLecturaCentrado" id="sucursal2" value="<?=$num_cuenta_digito_control?>" size="4" maxlength="2" readonly="readonly"/></td>
                              </tr>
                                  </table>                                  </td>
                                  <td width="26" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                                    
                                    <tr>
                                      <td width="6" height="22">&nbsp;</td>
                                      <td width="14" valign="top" class="arial14">&#8212;</td>
                              <td width="6">&nbsp;</td>
                                    </tr>
                                  </table>                                  </td>
                                  <td width="119" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                                    
                                    <tr>
                                      <td width="119" height="22" align="center" valign="top" class="arial14"> <input name="sucursal3" type="text" class="textfieldLecturaCentrado" id="sucursal3" value="<?=$num_cuenta_cuenta?>" size="12" maxlength="10" readonly="readonly"/></td>
                            </tr>
                                  </table>                                  </td>
                                  
                            </tr>
                              </table>                                
                                                          </td>   
           
                                                                                                                
                            </tr>
                            
                          </table>

                          <table width="400" align="center" border="0">
                          
                           <!--//////////////////////////////////////////////////////////////////////////////////////////////////////////-->
                            
                            <!--IBAN-->
                             <table width="500" align="center" border="0">
                            <tr align="center"> 
                              <td height="24" valign="top" class="arial14"><table width="100%" border="0" cellpadding="0" cellspacing="0">

                                <tbody><tr><td width="50" height="10" valign="middle" class="arial14" style="padding-right: 5px;">IBAN</td>
                                  <td width="55" height="22" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                                    
                                    <tbody><tr>
                                      <td width="55" height="22" align="center" valign="top" class="arial14"> <input name="iban1" type="text" class="textfieldLecturaCentrado" id="iban1" value="<?=$iban1_2?>" size="3" maxlength="4" readonly="readonly"></td>
                              </tr>
                                  </tbody></table>
                            </td>                                     
                                  <td width="23" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                                    

                                    <tbody><tr>
                                      <td width="4" height="22">&nbsp;</td>
                                      <td width="14" valign="top" class="arial14" style="font-size:8px;padding-top: 5px;"></td>
                              <td width="5">&nbsp;</td>
                                    </tr>
                                  </tbody></table>                                  </td>
                                  
                                  <td width="55" height="22" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                                    
                                    <tbody><tr>
                                      <td width="55" height="22" align="center" valign="top" class="arial14"> <input name="iban2" type="text" class="textfieldLecturaCentrado" id="iban2" value="<?=$iban2?>" size="2" maxlength="4" readonly="readonly"></td>
                              </tr>
                                  </tbody></table>
                            </td>                                     
                                  <td width="23" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                                    

                                    <tbody><tr>
                                      <td width="4" height="22">&nbsp;</td>
                                      <td width="14" valign="top" class="arial14" style="font-size:8px;padding-top: 5px;"></td>
                              <td width="5">&nbsp;</td>
                                    </tr>
                                  </tbody></table>                                  </td>
                                  
                                  
                                  <td width="67" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                                    
                                    <tbody><tr>
                                      <td width="67" height="22" align="center" valign="top" class="arial14"> <input name="iban3" type="text" class="textfieldLecturaCentrado" id="iban3" value="<?=$iban3?>" size="2" maxlength="4" readonly="readonly"></td>
                                      
                              </tr>
                                  </tbody></table>                                  </td>
                                  <td width="26" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                                    
                                    <tbody><tr>
                                      <td width="6" height="22">&nbsp;</td>
                                      <td width="14" valign="top" class="arial14" style="font-size:8px;padding-top: 5px;"></td>
                              <td width="6">&nbsp;</td>
                                    </tr>
                                  </tbody></table>                                  </td>
                                  <td width="76" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                    
                                    <tbody><tr>
                                      <td width="76" height="22" align="center" valign="top" class="arial14"> <input name="iban4" type="text" class="textfieldLecturaCentrado" id="iban4" value="<?=$iban4?>" size="2" maxlength="4" readonly="readonly"></td>
                              </tr>
                                  </tbody></table>                                  </td>
                                  
                                   <td width="26" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                                    
                                    <tbody><tr>
                                      <td width="6" height="22">&nbsp;</td>
                                      <td width="14" valign="top" class="arial14" style="font-size:8px;padding-top: 5px;"></td>
                              <td width="6">&nbsp;</td>
                                    </tr>
                                  </tbody></table>                                  </td>
                                  <td width="76" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                                    
                                    <tbody><tr>
                                      <td width="76" height="22" align="center" valign="top" class="arial14"> <input name="iban5" type="text" class="textfieldLecturaCentrado" id="iban5" value="<?=$iban5?>" size="2" maxlength="4" readonly="readonly"></td>
                            </tr>
                                  </tbody></table>
                                  
                                   <td width="26" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                                    
                                    <tbody><tr>
                                      <td width="6" height="22">&nbsp;</td>
                                      <td width="14" valign="top" class="arial14" style="font-size:8px;padding-top: 5px;"></td>
                              <td width="6">&nbsp;</td>
                                    </tr>
                                  </tbody></table>                                  </td>
                                  <td width="76" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                                    
                                    <tbody><tr>
                                      <td width="76" height="22" align="center" valign="top" class="arial14"> <input name="iban6" type="text" class="textfieldLecturaCentrado" id="iban6" value="<?=$iban6?>" size="2" maxlength="4" readonly="readonly"></td>
                            </tr>
                                  </tbody></table>
											 <td width="26" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                                    
                                    <tbody><tr>
                                      <td width="6" height="22">&nbsp;</td>
                                      <td width="14" valign="top" class="arial14" style="font-size:8px;padding-top: 5px;"></td>
                              <td width="6">&nbsp;</td>
                                    </tr>
                                  </tbody></table>                                  </td>
                                  <td width="76" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                                    
                                    <tbody><tr>
                                   
                                      <td width="76" height="22" align="center" valign="top" class="arial14"> <input name="iban7" type="text" class="textfieldLecturaCentrado"  id="iban7" value="<?=$iban7?>" size="2" maxlength="4" <?=($banco_extranjero=='')?'disabled':''?> readonly="readonly"></td>
                            </tr>
                                  </tbody></table>                                  </td>
								    <td width="50" height="10" valign="middle" class="arial14" style="padding-right: 5px;padding-left: 5px;">CEE </td>
                                   <td width="56" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                                    
                                    <tbody><tr>
                                      <td width="56" height="22" align="center" valign="top" class="arial14"><input id="BancoExtranjero" name="BancoExtranjero" type="text" readonly="readonly" class="textfieldLecturaCentrado" size="1" maxlength="2" value="<?=$banco_extranjero?>"/> </td>
                            </tr>
                                  </tbody></table>
								 
                                 
                                  
                                  
                                  
                            </tr>
                              </tbody></table>          
                            </tr>
                              </tbody></table>             
 

                            <tr> 
                      	<td height="30">&nbsp; </td>
                      </tr>
   					  
                      <tr> 
                      	<td height="10">&nbsp; </td>
                      </tr>
                            
                          </table>
                        </fieldset></td>
                      </tr>
   
   
                      <tr> 
                      	<td height="10">&nbsp; </td>
                      </tr>
                      
                   
                      
                    </table>
                    
                    
                    
                    
                    
         <div class="limpiar"></div>
         <div class="espacio"></div>
                  </fieldset>
                
          </div>      
                                                                                
<!--***************************************************************DATOS TIPO SUMINISTRO************************************************-->
<!--************************************************************************************************************************************-->

<!--SEPARACION ENTRE APARTADOS-->    
<!--SEPARACION ENTRE APARTADOS-->  

  <div class="tipotabla" style="width: 600px;">
                                  

				
                <fieldset>
                      <legend class="arialblack14">
                      <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 145'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                      </legend>
                      <table width="510" align="center" align="center" border="0">


		                  <tr>
        		            <td style="padding-left:80px;">
                            
<!--La primera columna contendra los textos del formulario-->         
                            	<div class="columna_modificacion_suministro separacion_cabecera arial14">
								
									<div class="componente_columna_modificacion_suministro_texto"><?=$oficina_datos1?></div>
									<div class="componente_columna_modificacion_suministro_texto"><?=$oficina_datos2?></div>                            
   									<div class="componente_columna_modificacion_suministro_texto"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 333'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?></div>
									<div class="componente_columna_modificacion_suministro_texto"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 149'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?></div>
									<div class="componente_columna_modificacion_suministro_texto"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 150'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?></div>
									<div class="componente_columna_modificacion_suministro_texto"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 296'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?></div>
                                    <div class="componente_columna_modificacion_suministro_texto"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 563'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?></div>
									<div class="componente_columna_modificacion_suministro_texto"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 151'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?></div>                                    
                                    
                                </div><!--<div class="columna_modificacion_suministro arial14">-->
                                
<!--La segunda columna contendra los campos de texto donde se mostraran los valores actuales de la base de datos-->                               
               	<div class="columna_modificacion_suministro separacion_columnas">
<!--LOLOLO-->                
					<div class="componente_columna_modificacion_suministro_campo_mostrar">                                   
                       	<div class="cabecera_columnas"><?=$oficina_modificacion_datossuministro1?></div>
	                </div><!--<div class="componente_columna_modificacion_suministro_campo_mostrar">-->

					<div class="componente_columna_modificacion_suministro_campo_mostrar">                    
		      	      <input name="PotenciaNormalizada_mostrar" type="text" class="mostrar_datos_lectura" id="PotenciaNormalizada_mostrar" size="25" maxlength="20" readonly="readonly" value="<?=$row["PotenciaContratada"]?>&nbsp;kW / h">                    
	                </div><!--<div class="componente_columna_modificacion_suministro_campo_mostrar">-->       
                    
					<div class="componente_columna_modificacion_suministro_campo_mostrar">                    
		      	      <input name="TarifaContratada_mostrar" type="text" class="mostrar_datos_lectura" id="TarifaContratada_mostrar" size="25" maxlength="20" readonly="readonly" value="<?=$row["TarifaContratada"]?>">                                                                            
	                </div><!--<div class="componente_columna_modificacion_suministro_campo_mostrar">-->                    

			
					<div class="componente_columna_modificacion_suministro_campo_mostrar">                    
<?php
//Se sustituye el valor almacenado en la base de datos por el que se le muestra al usuario en el combo

						if($row["Reactiva"]=="No")
						{
							$resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 170'); 
							$reactiva_mostrar= mysql_fetch_array($resultTexto);
						}//if($row["Reactiva"]=="Si")
						else
						{
							$resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 171'); 
							$reactiva_mostrar=mysql_fetch_array($resultTexto);
						}//else($row["Reactiva"]=="Si")
						
?>                    
		      	      <input name="Reactiva_mostrar" type="text" class="mostrar_datos_lectura" id="Reactiva_mostrar" size="25" maxlength="20" readonly="readonly" value="<?=$reactiva_mostrar["Texto"]?>">
                                                                                  
                    </div><!--<div class="componente_columna_modificacion_suministro_campo_mostrar">-->       
                                        
					<div class="componente_columna_modificacion_suministro_campo_mostrar">                    
<?php
//Se sustituye el valor almacenado en la base de datos por el que se le muestra al usuario en el combo

						if($row["Maximetro"]=="No")
						{
							$resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 170'); 
							$maximetro_mostrar= mysql_fetch_array($resultTexto);
						}//if($row["Discriminador"]=="a")
						else
						{
							$resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 171'); 
							$maximetro_mostrar=mysql_fetch_array($resultTexto);
						}//else($row["Discriminador"]=="a")
						
?>                    
		      	      <input name="Maximetro_mostrar" type="text" class="mostrar_datos_lectura" id="Maximetro_mostrar" size="25" maxlength="20" readonly="readonly" value="<?=$maximetro_mostrar["Texto"]?>">
                                                                                  
                    </div><!--<div class="componente_columna_modificacion_suministro_campo_mostrar">--> 
                      
					<div class="componente_columna_modificacion_suministro_campo_mostrar">                                                            
		      	      <input name="Modo_mostrar" type="text" class="mostrar_datos_lectura" id="Modo_mostrar" size="25" maxlength="20" readonly="readonly" value="<?=$row["Modo"]?>">                      
                                      
	                </div><!--<div class="componente_columna_modificacion_suministro_campo_mostrar">--> 
                                 
					<div class="componente_columna_modificacion_suministro_campo_mostrar">                                                            
		      	      <input name="TipoTension_mostrar" type="text" class="mostrar_datos_lectura" id="TipoTension_mostrar" size="25" maxlength="20" readonly="readonly" value="<?=$row["TipoTension"]?>">                      
                                      
	                </div><!--<div class="componente_columna_modificacion_suministro_campo_mostrar">-->                                  

					<div class="componente_columna_modificacion_suministro_campo_mostrar">                                                            
		      	      <input name="Fases_mostrar" type="text" class="mostrar_datos_lectura" id="Fases_mostrar" size="25" maxlength="20" readonly="readonly" value="<?=$row["Fases"]?>">                      
                                      
	                </div><!--<div class="componente_columna_modificacion_suministro_campo_mostrar">-->                                  
                                  
					<div class="componente_columna_modificacion_suministro_campo_mostrar">                                                            

<?php
//Se sustituye el valor almacenado en la base de datos por el que se le muestra al usuario en el combo

						if($row["TipoAlquiler"]=="Alquiler Empresa")
						{
							$equipo_medida_mostrar=$equipos_medida1;
						}//if($row["TipoAlquiler"]=="Alquiler Empresa")
						else
						{
							$equipo_medida_mostrar=$equipos_medida2;
						}//else($row["TipoAlquiler"]=="Alquiler Empresa")
						
?>   
                    
		      	      <input name="EquiposMedida_mostrar" type="text" class="mostrar_datos_lectura" id="EquiposMedida_mostrar" size="25" maxlength="20" readonly="readonly" value="<?=$equipo_medida_mostrar?>">                      
                                      
	                </div><!--<div class="componente_columna_modificacion_suministro_campo_mostrar">-->                                                      
                                 
                </div><!--<div class="columna_modificacion_suministro separacion_columnas">-->                                
                                                                                                   
               </td>
       		  </tr>


<!--CAMPO NUEVO-->                      

						<tr> 
                          <td height="25" valign="top" class="arial14"><?=$oficina_contratacion_alta5?>:</td>
                        </tr>

					<tr> 
                          <td height="25" valign="top" class="arial14">	               
                			<table border="1" bordercolor="#999999" width="100%" cellpadding="0" cellspacing="0">
							  <tr bordercolor="#999999">
							    <td align="center" width="25%">&nbsp;</td>
							    <td align="center" width="25%"><strong><?=$oficina_contratacion_alta7?></strong></td>
							    <td align="center" width="25%"><strong><?=$oficina_contratacion_alta8?></strong></td>
							    <td align="center" width="25%"><strong><?=$oficina_contratacion_alta9?></strong></td>
							  </tr>
                              
							  <tr bordercolor="#999999">
							    <td align="center" width="25%"><strong><?=$periodo_tarifario1?></strong></td>
                                
                                
                                <?  if ($p1_activa=='No')   {$clase='textfieldCentrado2';} else {$clase='textfieldCentrado';} ?>
                                
                                <td align="center" width="25%"><input id="P1_activa" name="P1_activa" type="text" readonly="readonly" class="<?=$clase?>" size="5" maxlength="2" value="<?=$p1_activa?>"/></td>
                                  <?  if ($p1_reactiva=='No') {$clase='textfieldCentrado2';} else {$clase='textfieldCentrado';} ?>
                                <td align="center" width="25%"><input id="P1_reactiva" name="P1_reactiva" type="text" readonly="readonly" class="<?=$clase?>" size="5" maxlength="2" value="<?=$p1_reactiva?>"/></td>
                                  <?  if ($p1_maximetro=='No') {$clase='textfieldCentrado2';} else {$clase='textfieldCentrado';} ?>
                                <td align="center" width="25%"><input id="P1_maximetro" name="P1_maximetro" type="text" readonly="readonly" class="<?=$clase?>" size="5" maxlength="2" value="<?=$p1_maximetro?>"/></td>
							  </tr>
                              
                              <tr bordercolor="#999999">
                                <td align="center" width="25%"><strong><?=$periodo_tarifario2?></strong></td>
                                
                                <?  if ($p2_activa=='No') {$clase='textfieldCentrado2';} else {$clase='textfieldCentrado';} ?>
                                <td align="center" width="25%"><input id="P2_activa" name="P2_activa" type="text" readonly="readonly" class="<?=$clase?>" size="5" maxlength="2" value="<?=$p2_activa?>"/></td>
                                  <?  if ($p2_reactiva=='No') {$clase='textfieldCentrado2';} else {$clase='textfieldCentrado';} ?>
                                <td align="center" width="25%"><input id="P2_reactiva" name="P2_reactiva" type="text" readonly="readonly" class="<?=$clase?>" size="5" maxlength="2" value="<?=$p2_reactiva?>"/></td>
                                 <?  if ($p2_maximetro=='No') {$clase='textfieldCentrado2';} else {$clase='textfieldCentrado';} ?>
                                <td align="center" width="25%"><input id="P2_maximetro" name="P2_maximetro" type="text" readonly="readonly" class="<?=$clase?>" size="5" maxlength="2" value="<?=$p2_maximetro?>"/></td>
                              </tr>
                              
                              <tr bordercolor="#999999">
                                <td align="center" width="25%"><strong><?=$periodo_tarifario3?></strong></td>
                             
                              <?  if ($p3_activa=='No') {$clase='textfieldCentrado2';} else {$clase='textfieldCentrado';} ?>
                                 <td align="center" width="25%"><input id="P3_activa" name="P3_activa" type="text" readonly="readonly" class="<?=$clase?>" size="5" maxlength="2" value="<?=$p3_activa?>"/></td>
                                 <?  if ($p3_reactiva=='No') {$clase='textfieldCentrado2';} else {$clase='textfieldCentrado';} ?>
                                <td align="center" width="25%"><input id="P3_reactiva" name="P3_reactiva" type="text" readonly="readonly" class="<?=$clase?>" size="5" maxlength="2" value="<?=$p3_reactiva?>"/></td>
                                <?  if ($p3_maximetro=='No') {$clase='textfieldCentrado2';} else {$clase='textfieldCentrado';} ?>
                                <td align="center" width="25%"><input id="P3_maximetro" name="P3_maximetro" type="text" readonly="readonly" class="<?=$clase?>" size="5" maxlength="2" value="<?=$p3_maximetro?>"/></td>
                              </tr>
                              
                              <tr bordercolor="#999999">
                                <td align="center" width="25%"><strong>P4</strong></td>
                                  <?  if ($p4_activa=='No') {$clase='textfieldCentrado2';} else {$clase='textfieldCentrado';} ?>
                                <td align="center" width="25%"><input id="P4_activa" name="P4_activa" type="text" readonly="readonly" class="<?=$clase?>" size="5" maxlength="2" value="<?=$p4_activa?>"/></td>
                                <?  if ($p4_reactiva=='No') {$clase='textfieldCentrado2';} else {$clase='textfieldCentrado';} ?>
                                <td align="center" width="25%"><input id="P4_reactiva" name="P4_reactiva" type="text" readonly="readonly" class="<?=$clase?>" size="5" maxlength="2" value="<?=$p4_reactiva?>"/></td>
                                <?  if ($p4_maximetro=='No') {$clase='textfieldCentrado2';} else {$clase='textfieldCentrado';} ?>
                                <td align="center" width="25%"><input id="P4_maximetro" name="P4_maximetro" type="text" readonly="readonly" class="<?=$clase?>" size="5" maxlength="2" value="<?=$p4_maximetro?>"/></td>
                              </tr>
                              
                              <tr bordercolor="#999999">
                                <td align="center" width="25%"><strong>P5</strong></td>
                                  <?  if ($p5_activa=='No') {$clase='textfieldCentrado2';} else {$clase='textfieldCentrado';} ?>
                                <td align="center" width="25%"><input id="P5_activa" name="P5_activa" type="text" readonly="readonly" class="<?=$clase?>" size="5" maxlength="2" value="<?=$p5_activa?>"/></td>
                                <?  if ($p5_reactiva=='No') {$clase='textfieldCentrado2';} else {$clase='textfieldCentrado';} ?>
                                <td align="center" width="25%"><input id="P5_reactiva" name="P5_reactiva" type="text" readonly="readonly" class="<?=$clase?>" size="5" maxlength="2" value="<?=$p5_reactiva?>"/></td>
                                <?  if ($p5_maximetro=='No') {$clase='textfieldCentrado2';} else {$clase='textfieldCentrado';} ?>
                                <td align="center" width="25%"><input id="P5_maximetro" name="P5_maximetro" type="text" readonly="readonly" class="<?=$clase?>" size="5" maxlength="2" value="<?=$p5_maximetro?>"/></td>
                              </tr>
                              
                              <tr bordercolor="#999999">
                                <td align="center" width="25%"><strong>P6</strong></td>
                                  <?  if ($p6_activa=='No') {$clase='textfieldCentrado2';} else {$clase='textfieldCentrado';} ?>
                                <td align="center" width="25%"><input id="P6_activa" name="P6_activa" type="text" readonly="readonly" class="<?=$clase?>" size="5" maxlength="2" value="<?=$p6_activa?>"/></td>
                                <?  if ($p6_reactiva=='No') {$clase='textfieldCentrado2';} else {$clase='textfieldCentrado';} ?>
                                <td align="center" width="25%"><input id="P6_reactiva" name="P6_reactiva" type="text" readonly="readonly" class="<?=$clase?>" size="5" maxlength="2" value="<?=$p6_reactiva?>"/></td>
                                <?  if ($p6_maximetro=='No') {$clase='textfieldCentrado2';} else {$clase='textfieldCentrado';} ?>
                                <td align="center" width="25%"><input id="P6_maximetro" name="P6_maximetro" type="text" readonly="readonly" class="<?=$clase?>" size="5" maxlength="2" value="<?=$p6_maximetro?>"/></td>
                              </tr>
                              
                            </table>                
                			</td>
                        </tr>                                             
<!--CAMPO NUEVO-->                                            

                    </table>
                    </fieldset>
                    
          </div>



<!--***************************************************************(FIN)DATOS TIPO SUMINISTRO************************************************-->
<!--****************************************************************************************************************************************-->
             <!--********************************************************************CONSUMOS************************************************************-->
<!--****************************************************************************************************************************************-->          <tr>
            
            
            
            
            

     <div class="tipotabla" style="width: 600px;">
                 
            <td></table>
        </table>
<!--VERSION VIEJA-->
          
      </div><!--<div class="contenido_ajustado">--> 
                	
		</div><!--<div class="contenido_seccion_oficina">-->                        
		
        
        <div class="limpiar"></div>
                        	
<!--Por ultimo aparecera el pie de la web-->
<?php 
//	include($_SESSION["directorio_raiz"]."includes/pie.php");


}//else ($_SESSION['usuario'])
		
?>        
    </div><!--<div id="central_oficina"-->
	<?php
    include("../includes/pie.php");
    ?>
    </body>
</html>

