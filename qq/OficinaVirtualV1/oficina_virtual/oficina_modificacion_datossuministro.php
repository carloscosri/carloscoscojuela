<?php
setcookie("potencia", ' ');
include("_conexion.php");

include("../idiomas/".$_SESSION["idioma"]."/oficina_datos_titular.php");
include("../idiomas/".$_SESSION["idioma"]."/oficina_contratacion_alta.php");
include("../idiomas/".$_SESSION["idioma"]."/oficina_modificacion_datostitular.php");
include("../idiomas/".$_SESSION["idioma"]."/oficina_datos.php");
include("../idiomas/".$_SESSION["idioma"]."/oficina_modificacion_datossuministro.php");

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<title><?=$titulo_web;?></title>

<!--Se añaden las hojas de estilos genericas que utiliza esta seccion-->
<link href="../css/estilos.css" rel="stylesheet" type="text/css" />
<link href="../css/secciones.css" rel="stylesheet" type="text/css" />
<link href="css/oficina.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_vieja.css" rel="stylesheet" type="text/css" />

<!--Como los estilos son comunes a la seccion de modificacion del titular, utilizaremos lo mismos estilos-->
<link href="css/oficina_modificacion_titular.css" rel="stylesheet" type="text/css" />
<!--Añadimos la hoja de estilos exclusiva de esta seccion-->
<link href="css/oficina_modificacion_suministro.css" rel="stylesheet" type="text/css" />

<!--[if IE 7]>
	<link href="../css/estilos_ie7.css" rel="stylesheet" type="text/css" />
	<link href="../css/secciones_ie7.css" rel="stylesheet" type="text/css" />    
<![endif]-->


<!--Añadimos el script que define el comportamiento del apartado Datos Tipo Suministo-->

<script language=javascript type=text/javascript>
function stopRKey(evt) {
var evt = (evt) ? evt : ((event) ? event : null);
var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
if ((evt.keyCode == 13) && (node.type=="text")) {return false;}
}
document.onkeypress = stopRKey; 
</script>

<script src="scripts/comportamiento_datos_tipo_suministro.js" type="text/javascript"></script>

<script language="javascript" type="text/javascript">
<!--

//Esta funcion establece el formulario en su estado original, todos los campos deshabilitados salvo las opciones de potencia a contratar
function inicio_formulario()
{	
	//document.form_alta.OpcionesPotencia.selectedIndex=0;
	
	document.form_alta.PotenciaNormalizada.selectedIndex=0;
	document.form_alta.TarifaContratada.selectedIndex=0;
	document.form_alta.Discriminador.selectedIndex=0;
	document.form_alta.PeriodosTarifarios.selectedIndex=0;									
	document.form_alta.Maximetro.selectedIndex=0;												
	document.form_alta.Modo.value="";															
	document.form_alta.TipoTension.selectedIndex=0;		
	document.form_alta.Fases.value="";																		
	document.form_alta.Reactiva.selectedIndex=0;	
	document.form_alta.EquiposMedida.selectedIndex=0;		
			
	document.form_alta.PotenciaNormalizada.disabled = true;
	document.form_alta.TarifaContratada.disabled = true;
	document.form_alta.Discriminador.disabled = true;
	document.form_alta.PeriodosTarifarios.disabled = true;	
	document.form_alta.PeriodosTarifariosb.disabled = true;									
	document.form_alta.Maximetro.disabled = true;												
	document.form_alta.Modo.disabled = true;												
	document.form_alta.TipoTension.disabled = true;		
	document.form_alta.Fases.disabled = true;
	document.form_alta.Reactiva.disabled = true;
	document.form_alta.EquiposMedida.disabled = true;	
	
		
}//function inicio_formulario()

//Esta funcion deshabilita los campos pertinentes cuando se selecciona la opcion que esta vacia en el combo de opciones de potencia a contratar
//En las demas opciones recarga la pagina, estableciendo la seccion activa, de este modo todos los combos toman sus valores
function dar_valor_seccion_activa(idioma)
{
	var opcion_potencia=document.form_alta.OpcionesPotencia.value;
	
	switch(opcion_potencia)	
	{
//Si se selecciona la opcion vacia, se volveran a dehabilitar todos los campos y todos tomaran su valor inicial o se vaciaran segun el caso
		case "-1":
			inicio_formulario();								
		break;

		case "0":
			window.location = "oficina_modificacion_datossuministro.php?id="+idioma+"&s=0#ancla";
		break;
						
		case "1":
			window.location = "oficina_modificacion_datossuministro.php?id="+idioma+"&s=1#ancla";
		break;			
		
		case "2":
			window.location = "oficina_modificacion_datossuministro.php?id="+idioma+"&s=2#ancla";								
		break;
	}//switch(opcion_potencia)
	
}//function dar_valor_seccion_activa(idioma)

//Esta funcion dara el valor oportuno a el campo de las opciones de potencia, ya que al recargar la pagina el combobox se reinicia
function dar_valor_opciones_potencia(seccion)
{	
	document.form_alta.OpcionesPotencia.selectedIndex=parseInt(seccion)+1;
}//function dar_valor_opciones_potencia()

-->
</script>

<?php
if (isset($_POST["submit"]))
{
	$opciones_potencia=$_POST["OpcionesPotencia"];
	$potencia_normalizada=$_POST["PotenciaNormalizada"];	
	$tarifa_contratar=$_POST["TarifaContratada"];	
	$periodos_tarifarios=$_POST["PeriodosTarifarios"];
	$tipo_tension=$_POST["TipoTension"];
	$discriminador=$_POST["Discriminador"];
	$equipos_medida=$_POST["TipoAlquiler"];
	$maximetro=$_POST["Maximetro"];
	$modo=$_POST["Modo"];
	$fases=$_POST["Fases"];
	$reactiva=$_POST["Reactiva"];
	
	
//Esta variable controlara si se produce algun error en las verificaciones
	$error=0;	
	
	if($_POST['Condiciones']=='Yes')  
	{
		$resultTexto = mysql_query('SELECT TitularEmail FROM DatosRegistrados WHERE CodigoCliente = '.$_SESSION["numeroCliente"]);		
	$rowTexto = mysql_fetch_array($resultTexto); 
	$email_cliente = $rowTexto['TitularEmail'];
	
	if($email_cliente==""){
		$error_mail=1;
		}
	} else {$error=9;}


	
//Ahora comenzaremos con las verficaciones. En primer lugar comprobaremos si los campos obligatorios han sido todos escritos
//	if($opciones_potencia==-1 or $potencia_normalizada==-1 or $tarifa_contratar==-1 or $periodos_tarifarios==0)
//	{
//		$error=1;
//	}//if($potencia_normalizada==-1 or $tarifa_contratar==-1 or $periodos_tarifarios==0)

}//if (isset($_POST["submit"]))

?>

</head>
	
<body >
<!--******************************onLoad="document.forms.form_alta.OpcionesPotencia.focus()"*******************Web realizada por ****************************************-->
	<div id="central_oficina">
    
<?php
//Se comprueba que el usuario se haya identificado, si no es asi se da un aviso y redirigimos a la seccion de identificacion
if ($_SESSION['usuario']=="")
{	
	MsgBox($error_identificacion);
	redirigir("oficina_index.php?id=".$_SESSION["idioma"]);
	
}//if ($_SESSION['usuario'])

//Si el usuario se ha identificado la seccion se ejecutara con normalidad
else
{
	if(isset($_GET["s"]) and $_GET["s"]!="")
	{	
		$_SESSION["seccion_origen_modificacion"]=$_GET["s"];
		
//Esta variable controlara si se actualiza o no el combo de opciones de potencia (1=Si, 0=No)
		$_SESSION["actualizar_opciones_potencia"]=1;
		
		switch($_SESSION["seccion_origen_modificacion"])	
		{
//10 kw/h				
			case 0:
				$result2 = mysql_query('SELECT IdTarifa, Tarifa FROM TarifasDetalles where TarifaPeajeOficial = "2.0A ML"');
				$row2 = mysql_fetch_array($result2);
				$t20a = $row2[0];
				$t20a_nombre = $row2[1];
				$array_tarifas_disponibles[0] = $t20a;
				
				$result = mysql_query('SELECT IdTarifa, Tarifa FROM TarifasDetalles where TarifaPeajeOficial = "2.0DHA ML"');
				$row = mysql_fetch_array($result);
				$t20dha = $row[0];
				$t20dha_nombre = $row[1];
				$array_tarifas_disponibles[1] = $t20dha;	

				//$array_tarifas_disponibles[0]=20;									
				//$array_tarifas_disponibles[1]=21;		
															
			break;

//10-15 kw/h					
			case 1:
				$result = mysql_query('SELECT IdTarifa, Tarifa FROM TarifasDetalles where TarifaPeajeOficial ="2.1A ML"');
				$row = mysql_fetch_array($result);
				$t21a = $row[0];
				$t21a_nombre = $row[1];
				$array_tarifas_disponibles[0] = $t21a;
				
				$result = mysql_query('SELECT IdTarifa, Tarifa FROM TarifasDetalles where TarifaPeajeOficial ="2.1DHA ML"');
				$row = mysql_fetch_array($result);
				$t21dha = $row[0];
				$t21dha_nombre = $row[1];
				$array_tarifas_disponibles[1] = $t21dha;
				//$array_tarifas_disponibles[0]=210;									
				//$array_tarifas_disponibles[1]=211;
																			
			break;
									
//>15 kw/h. EN ESTE CASO TAMBIEN SE ESTABLECERAN OTROS VALORES POR DEFECTO

			case 2:
				$result = mysql_query('SELECT IdTarifa, Tarifa FROM TarifasDetalles where TarifaPeajeOficial ="3.0A ML"');
				$row = mysql_fetch_array($result);
				$t30a = $row[0];
				$t30a_nombre = $row[1];
				$array_tarifas_disponibles[0] = $t30a;
				 
				$result = mysql_query('SELECT IdTarifa, Tarifa FROM TarifasDetalles where TarifaPeajeOficial ="3.1A ML"');
				$row = mysql_fetch_array($result);
				$t31a = $row[0];
				$t31a_nombre = $row[1];
				$array_tarifas_disponibles[1] = $t31a;
				
				$result = mysql_query('SELECT IdTarifa, Tarifa FROM TarifasDetalles where TarifaPeajeOficial ="6.1"');
				$row = mysql_fetch_array($result);
				$t61a = $row[0];
				$t61a_nombre = $row[1];
				$array_tarifas_disponibles[2] = $t61a;
				//$array_tarifas_disponibles[0]=30;									
				//$array_tarifas_disponibles[1]=31;																																											
				//$array_tarifas_disponibles[2]=61;																		
			break;

		}//switch($_SESSION["seccion_origen_modificacion"])			
								
	}//if(isset($_GET["s"]) and $_GET["s"]!="")
//Para evitar comportamiento indeseados, si no se ha establecido una opcion de potencia	eliminaremos la variable de sesion que controla el comportamiento del formulario
	else
	{	
		$_SESSION["seccion_origen_modificacion"] = -1;		
	}//else(isset($_GET["s"]) and $_GET["s"]!="")

//Añadimos los fragmentos comunes, la cabecera, el menu, y el submenu
	include("oficina_fragmentos_comunes.php");	
?>                    
		<div class="contenido_seccion_oficina_modificacion_suministro">

<!--VERSION VIEJA-->
		<form action="oficina_modificacion_datossuministro.php?id=<?=$_SESSION["idioma"].$ampersand?>s=<?=$_SESSION["seccion_origen_modificacion"]?>"  method="post" name="form_alta" id="form_alta" onsubmit="habilitar_campos();">
		  <table width="600" border="0" cellspacing="0" cellpadding="0">
            <tr> 
              <td><table width="600" border="0" cellspacing="0" cellpadding="0">
                  <tr align="center"> 
                    <td width="600" height="10" class="arialblack18Titulo" style=" background-image:url(imagenes/MarcoAlta.gif);background-repeat:no-repeat"> 
                      <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 203'); mysql_query("SET NAMES 'utf8'"); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?> </td>
                    <tr>
					<td class="arial14">&nbsp;</td>
					</tr>
					
					<!-- <td width="1">&nbsp;</td> -->
                  </tr>
                 

<?php
//Comprobamos si el cliente, tiene modificaciones pendientes

//Esta variable controlara si el cliente tiene o no modificaciones pendientes
					$tiene_modificaciones_pendientes=0;

					$consulta_cliente=mysql_query("SELECT * FROM DatosModificados WHERE CodigoCliente  = ".$_SESSION["numeroCliente"]); 
					$hay_cliente = mysql_fetch_array($consulta_cliente);
										
					if($hay_cliente)
					{
						$tiene_modificaciones_pendientes=1;
					}//if($hay_cliente)
										
//Si el cliente tiene modificaciones pendientes se mostrara un mensaje					
					if($tiene_modificaciones_pendientes==1)
					{
?>                  
	                <!--  <tr>
    	              	<td valign="middle" class="texto_advertencia"><?=$advertencia_cambios_pendientes?></td>
        	          </tr>  -->
<?php				}//if($tiene_modificaciones_pendientes==1)?>                  

                  <tr> 
                    <td height="94" class="arial14">
					<fieldset>
					<legend class="arialblack14">                    
					<?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 562'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
					</legend>
					<!-- A PARTIR DE AQUI RELLENAMOS LA TABLA -->
                      <table width="469" cellspacing="0" cellpadding="0" border="0" align="center">
                        <?php if (isset($_SESSION['numeroCliente'])){
							$result = mysql_query("SELECT * FROM DatosRegistrados WHERE CodigoCliente = ".$_SESSION['numeroCliente']." ");
							$row = mysql_fetch_array($result);
							 }
							 else
							 {
								$result = mysql_query("SELECT * FROM DatosRegistrados WHERE CodigoCliente = -1");
							//Para que no tome ningun cliente pero no de ningun error en la pagina
								$row = mysql_fetch_array($result);
					 		}																											
?>                                               

<!-- NOMBRE TITULAR -->
					<tr>
                        <td width="500" height="25" colspan="2" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                          <tr>
                            <td width="105" height="3"></td>
                          </tr>
                          <tr>
                            <td height="22" valign="middle" class="arial14"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 119'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?></td>
                        </tr>
                        </table>                        </td>
                        <td valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                          <tr>
                            <td width="375" height="3"></td>
                          </tr>
                          <tr>
                            <td height="22" valign="top"><input name="TitularNombreFijo"   value="<?php echo(str_replace('"','&quot;',$row['TitularNombre']));?>"type="text" class="textfieldLectura" id="TitularNombreFijo" size="50" maxlength="50" readonly="readonly"/></td>							
                        </tr>
                        </table>                        
						</td>
					</tr>
<!-- DNI Y Nª CLIENTE -->
						<tr>
                        <td width="63" height="25" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                          <tr>
                            <td width="68" height="3"></td>
                          </tr>
                          <tr>
                            <td height="22" valign="middle" class="arial14"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 118'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?></td>
                        </tr>
                        </table>                        </td>
                        <td colspan="2" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                          <tr>
                            <td width="412" height="3"></td>
                          </tr>
                          <tr>
                            <td height="22" valign="top" class="arial14">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input value="<?php echo($row['DNI']);?>" name="TitularDNIFijo" type="text" class="textfieldLecturaNum" id="TitularDNIFijo" size="9" maxlength="9" readonly="readonly"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 196'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                              <input name="TitularCodigoFijo"   value="<?php echo($row['CodigoCliente']);?>" type="text" class="textfieldLecturaNum" id="TitularCodigoFijo" size="7" maxlength="10" readonly="readonly"/></td>
                        </tr>                                            
                        	</table>
                           </td>
                        </tr>
<!-- NUMERO POLIZA -->
					<tr>
                        <td height="25" colspan="2" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                          <tr>
                            <td width="105" height="3"></td>
                          </tr>
                          <tr>
                            <td height="22" valign="middle" class="arial14"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 197'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?></td>
                        </tr>
                        </table>                        </td>
                        <td valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                          <tr>
                            <td width="375" height="3"></td>
                          </tr>
                          <tr>
                            <td height="22" valign="top"><input name="TitularPolizaFijo" value="<?php echo($row['NumeroPoliza']);?>"  type="text" class="textfieldLecturaNum" id="TitularPolizaFijo" size="9" maxlength="10" readonly="readonly"/></td>
                        </tr>
                        </table>                        
						</td>
					</tr>
<!-- CUPS -->
					<tr>
                        <td height="25" colspan="2" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                          <tr>
                            <td width="105" height="3"></td>
                          </tr>
                          <tr>
                            <td height="22" valign="middle" class="arial14">CUPS</td>
                        </tr>
                        </table>                        </td>
                        <td valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                          <tr>
                            <td width="375" height="3"></td>
                          </tr>
                          <tr>
                            <td height="22" valign="top"><input name="CUPSFijo" value="<?php echo($row['CUPS']);?>"  type="text" class="textfieldLectura" id="CUPSFijo" size="25" maxlength="25" readonly="readonly"/></td>
                        </tr>
                        </table>                        
						</td>
					</tr>

                    </table>
                    </fieldset>
					</td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr> 
                    <td height="10"></td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>                 
                    <td height="125" valign="top">
						<fieldset>
							<legend class="arialblack14"> <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 130'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?></legend>
                    <table width="469" border="0" cellpadding="0" cellspacing="0" align="center">
                    
                    <tr>
                    	<td height="5">&nbsp;</td>                    
                    </tr>
                    
                      <tr>
                        <td height="22" colspan="2" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                          <tr>
                            <td width="105" height="22" valign="middle" class="arial14"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 121'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?></td>
                        </tr>
                        </table>
						</td>
                        <td width="373" valign="top">
							<table width="100%" border="0" cellpadding="0" cellspacing="0">                          
                          <tr>
                            <td width="375" height="22" valign="top"><input name="SuministroCiudadFijo" type="text" class="textfieldLectura" value="<?php echo(str_replace('"','&quot;',$row['SuministroCiudad']));?>" size="50" maxlength="50" readonly="readonly"></td>
                      </tr>
                        </table>
						</td>
                      </tr>
                      <tr>
                        <td height="25" colspan="2" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                          <tr>
                            <td width="105" height="3"></td>
                          </tr>
                          <tr>
                            <td height="22" valign="middle" class="arial14"><?php $resultTexto=mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto=122');
						$rowTexto=mysql_fetch_array($resultTexto);
						echo($rowTexto['Texto']); ?> </td>
                        </tr>
                        </table>                        </td>
                        <td valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                          <tr>
                            <td width="375" height="3"></td>
                          </tr>
                          <tr>
                            <td height="22" valign="top"><input name="SuministroCPFijo" type="text" class="textfieldLectura" value="<?php echo($row['SuministroCP']);?>" size="5" maxlength="5"readonly="readonly" ></td>
                        </tr>
                        </table>                        </td>
                      </tr>
                      
                      <tr>
                        <td height="25" colspan="2" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                          <tr>
                            <td width="105" height="3"></td>
                          </tr>
                          <tr>
                            <td height="22" valign="middle" class="arial14"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 123'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?></td>
                        </tr>
                        </table>                        </td>
                        <td valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                          <tr>
                            <td width="375" height="3"></td>
                          </tr>
                          <tr>
                            <td height="22" valign="top"><input name="SuministroCalleFijo" type="text" class="textfieldLectura" value="<?php echo(str_replace('"','&quot;',$row['SuministroCalle']));?>" size="50" maxlength="50" readonly="readonly"></td>
                        </tr>
                        </table>                        </td>
                        </tr>
                      
                      <tr>
                        <td width="63" height="25" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                          <tr>
                            <td width="68" height="3"></td>
                          </tr>
                          <tr>
                            <td height="22" valign="middle" class="arial14"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 124'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?></td>
                        </tr>
                        </table>                        </td>
                        <td colspan="2" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                          <tr>
                            <td width="412" height="3"></td>
                          </tr>
                          <tr>
                            <td height="22" valign="top" class="arial14">&nbsp;&nbsp;<input name="SuministroNumeroFijo" type="text" size="10" maxlength="10"class="textfieldLectura" value="<?php echo($row['SuministroNumero']);?>" readonly="readonly">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 125'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                              <input name="SuministroExtensionFijo" type="text" class="textfieldLectura" value="<?php echo(str_replace('"','&quot;',$row['SuministroExtension']));?>" size="17" maxlength="19" readonly="readonly"></td>
                        </tr>
                                                
                        	</table>
                           </td>
                        </tr>
                        
                        <tr>
                            <td width="411" height="3"></td>
                         </tr>
<!--CAMPO NUEVO-->     
 <tr>
                        <td height="25" colspan="2" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                          <tr>
                            <td width="105" height="3"></td>
                          </tr>
                          <tr>
                            <td height="22" valign="middle" class="arial14"><?=$oficina_datos_titular21?></td>
                        </tr>
                        </table>                        </td>
                        <td valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                          <tr>
                            <td width="375" height="3"></td>
                          </tr>
                          <tr>
                            <td height="22" valign="top"><input name="SuministroAclaradorFijo" type="text" class="textfieldLectura" value="<?php echo(str_replace('"','&quot;',$row['SuministroExtension2']));?>" size="50" maxlength="50" readonly="readonly"></td>
                        </tr>
                        </table>                        </td>
                        </tr>

<!--CAMPO NUEVO-->    
                                          
                      <tr>
                        <td height="10"></td>
                        <td width="34"></td>
                        <td></td>
                        </tr>
                    </table>
                    </fieldset>
					</td>
					
                    <td>&nbsp;</td>
                  </tr>
                  
                  <tr>
                    <td height="10"><a name="ancla" id="ancla"></a></td>
                    <td>&nbsp;</td>
                  </tr>
                  
                <tr>
					<td>
					<fieldset style="border-radius:10px;margin-bottom:10px;">
					    <table border="0">
                        <tr>
						   <td width="100"><img src="../img/oficina/info.png" width="60"/></td>
						   <td>
                        Se indican los datos técnicos relativos a contratación.
                        <br><br>
                        Puede introducir cualquier dato en aquel campo que desee modificar y después activar la opción de "Enviar" que se encuentra al final del formulario.
							</td>
						</tr>
						</table>
					</fieldset>
					</td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr> 
                    <td height="10"></td>
                    <td>&nbsp;</td>
                  </tr>
                  
<!--SEPARACION ENTRE APARTADOS-->    
                    <tr class="arial12"> 
                	  <td height="10" class="arial14"></td>
	                  <td class="arial14"></td>
                </tr>
<!--SEPARACION ENTRE APARTADOS-->                                    

                <tr> 
                    <td class="nota_campos_obligatorios"><span class="nota_campos_obligatorios"><?=$nota_campos_obligatorios?></span><br /><br /></td>
                    <td>&nbsp;</td>
                </tr>

				<tr>              
	                <td height="35" valign="top"><fieldset >
                      <legend class="arialblack14">
                      <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 145'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                      </legend>
                      <table width="550">


		                  <tr>
        		            <td style="padding-left:50px;">
                            
<!--La primera columna contendra los textos del formulario-->         
                            	<div class="columna_modificacion_suministro separacion_cabecera arial14">
                                
									<div class="componente_columna_modificacion_suministro_texto"><?=$oficina_modificacion_datossuministro3?></div>								
									<div class="componente_columna_modificacion_suministro_texto"><?=$oficina_datos1?></div>
									<div class="componente_columna_modificacion_suministro_texto"><?=$oficina_datos2?></div>      
   									<div class="componente_columna_modificacion_suministro_texto"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 333'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?></div>
									<div class="componente_columna_modificacion_suministro_texto"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 149'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?></div>
									<div class="componente_columna_modificacion_suministro_texto"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 150'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?></div>
									<div class="componente_columna_modificacion_suministro_texto"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 296'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?></div>
                                    <div class="componente_columna_modificacion_suministro_texto"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 563'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?></div>
									<div class="componente_columna_modificacion_suministro_texto"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 151'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?></div>                                    
                                    
                                </div><!--<div class="columna_modificacion_suministro arial14">-->
                                
<!--La segunda columna contendra los campos de texto donde se mostraran los valores actuales de la base de datos-->                               
               	<div class="columna_modificacion_suministro separacion_columnass">
					<div class="componente_columna_modificacion_suministro_campo_mostrar">                                   
                       	<div class="cabecera_columnas"><?=$oficina_modificacion_datossuministro1?></div>

<?php

				
//Como la opcion de potencia depende de la tarifa, le daremos el valor a mostrar
					if($row["TarifaContratada"]==$t20a_nombre or $row["TarifaContratada"]==$t20dha_nombre)
					{
						$opciones_potencia_mostrar=$opcion_potencia1;
					}//if($row["TarifaContratada"]=="2.0A" or  $row["TarifaContratada"]=="2.0DHA")
					else
					{
						if($row["TarifaContratada"]==$t21a_nombre or $row["TarifaContratada"]==$t21dha_nombre)
						{
							$opciones_potencia_mostrar=$opcion_potencia2;					
						}//if($row["TarifaContratada"]=="2.1A" or  $row["TarifaContratada"]=="2.1DHA")
						else
						{
							if($row["TarifaContratada"]==$t30a_nombre or $row["TarifaContratada"]==$t31a_nombre or $row["TarifaContratada"]==$t61a_nombre)
							{
								$opciones_potencia_mostrar=$opcion_potencia3;					
							}//if($row["TarifaContratada"]=="3.0A" or  $row["TarifaContratada"]=="3.1A" or $row["TarifaContratada"]=="6.1")
						}//if($row["TarifaContratada"]=="2.1A" or  $row["TarifaContratada"]=="2.1DHA")						
					}//else($row["TarifaContratada"]=="2.0A" or  $row["TarifaContratada"]=="2.0DHA")

?>

                    
	      	    	  <input name="OpcionesPotencia_mostrar" type="text" class="mostrar_datos_lectura" id="OpcionesPotencia_mostrar" size="20" maxlength="20" readonly="readonly" value="<?=$opciones_potencia_mostrar?>">					
                </div><!--<div class="componente_columna_modificacion_suministro_campo_mostrar">-->

					<div class="componente_columna_modificacion_suministro_campo_mostrar">                    
		      	      <input name="PotenciaNormalizada_mostrar" type="text" class="mostrar_datos_lectura" id="PotenciaNormalizada_mostrar" size="20" maxlength="20" readonly="readonly" value="<?=$row["PotenciaContratada"]?>">                    
	                </div><!--<div class="componente_columna_modificacion_suministro_campo_mostrar">-->       
                    
					<div class="componente_columna_modificacion_suministro_campo_mostrar">                    
		      	      <input name="TarifaContratada_mostrar" type="text" class="mostrar_datos_lectura" id="TarifaContratada_mostrar" size="20" maxlength="20" readonly="readonly" value="<?=$row["TarifaContratada"]?>">                                                                            
	                </div><!--<div class="componente_columna_modificacion_suministro_campo_mostrar">-->                    

					<div class="componente_columna_modificacion_suministro_campo_mostrar">                    
<?php
//Se sustituye el valor almacenado en la base de datos por el que se le muestra al usuario en el combo
						if($row["Discriminador"]=="a")
						{
							$discriminador_mostrar="DHA";
						}//if($row["Discriminador"]=="a")
?>
                    
		      	      <input style='display:none' name="Discriminador_mostrar" type="text" class="mostrar_datos_lectura" id="Discriminador_mostrar" size="20" maxlength="20" readonly="readonly" value="<?=$discriminador_mostrar?>">                                                                            
	                </div><!--<div class="componente_columna_modificacion_suministro_campo_mostrar">--> 

					<div class="componente_columna_modificacion_suministro_campo_mostrar">                    
<?php
//Se sustituye el valor almacenado en la base de datos por el que se le muestra al usuario en el combo

						if($row["Reactiva"]=="No")
						{
							$resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 170'); 
							$reactiva_mostrar= mysql_fetch_array($resultTexto);
						}//if($row["Reactiva"]=="Si")
						else
						{
							$resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 171'); 
							$reactiva_mostrar=mysql_fetch_array($resultTexto);
						}//else($row["Reactiva"]=="Si")
						
?>                    
		      	      <input name="Reactiva_mostrar" type="text" class="mostrar_datos_lectura" id="Reactiva_mostrar" size="20" maxlength="20" readonly="readonly" value="<?=$reactiva_mostrar["Texto"]?>">
                                                                                  
                    </div><!--<div class="componente_columna_modificacion_suministro_campo_mostrar">-->                     
                                        
					<div class="componente_columna_modificacion_suministro_campo_mostrar">                    
<?php
//Se sustituye el valor almacenado en la base de datos por el que se le muestra al usuario en el combo

						if($row["Maximetro"]=="No")
						{
							$resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 170'); 
							$maximetro_mostrar= mysql_fetch_array($resultTexto);
						}//if($row["Discriminador"]=="a")
						else
						{
							$resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 171'); 
							$maximetro_mostrar=mysql_fetch_array($resultTexto);
						}//else($row["Discriminador"]=="a")
						
?>                    
		      	      <input name="Maximetro_mostrar" type="text" class="mostrar_datos_lectura" id="Maximetro_mostrar" size="20" maxlength="20" readonly="readonly" value="<?=$maximetro_mostrar["Texto"]?>">
                                                                                  
                    </div><!--<div class="componente_columna_modificacion_suministro_campo_mostrar">--> 
                      
					<div class="componente_columna_modificacion_suministro_campo_mostrar">                                                            
		      	      <input name="Modo_mostrar" type="text" class="mostrar_datos_lectura" id="Modo_mostrar" size="20" maxlength="20" readonly="readonly" value="<?=$row["Modo"]?>">                      
                                      
	                </div><!--<div class="componente_columna_modificacion_suministro_campo_mostrar">--> 
                                 
					<div class="componente_columna_modificacion_suministro_campo_mostrar">                                                            
		      	      <input name="TipoTension_mostrar" type="text" class="mostrar_datos_lectura" id="TipoTension_mostrar" size="20" maxlength="20" readonly="readonly" value="<?=$row["TipoTension"]?>">                      
                                      
	                </div><!--<div class="componente_columna_modificacion_suministro_campo_mostrar">-->                                  

					<div class="componente_columna_modificacion_suministro_campo_mostrar">                                                            
		      	      <input name="Fases_mostrar" type="text" class="mostrar_datos_lectura" id="Fases_mostrar" size="20" maxlength="20" readonly="readonly" value="<?=$row["Fases"]?>">                      
                                      
	                </div><!--<div class="componente_columna_modificacion_suministro_campo_mostrar">-->                                                      

					<div class="componente_columna_modificacion_suministro_campo_mostrar">                                                            

<?php
//Se sustituye el valor almacenado en la base de datos por el que se le muestra al usuario en el combo


						if($row["TipoAlquiler"]=="Alquiler Empresa")
						{
							$equipo_medida_mostrar=$equipos_medida1;
						}//if($row["TipoAlquiler"]=="Alquiler Empresa")
						else
						{
							$equipo_medida_mostrar=$equipos_medida2;
						}//else($row["TipoAlquiler"]=="Alquiler Empresa")
						
?>   
                    
		      	      <input name="EquiposMedida_mostrar" type="text" class="mostrar_datos_lectura" id="EquiposMedida_mostrar" size="20" maxlength="20" readonly="readonly" value="<?=$equipo_medida_mostrar?>">                      
                                      
	                </div><!--<div class="componente_columna_modificacion_suministro_campo_mostrar">-->                                                      
                                 
                </div><!--<div class="columna_modificacion_suministro separacion_columnas">-->                                

<!----------------------------------------------------------------------------------------------------------------------------------------------->
<!----------------------------------------------------------------------------------------------------------------------------------------------->
<!--------------------------------------------- Los datos a Modificar: ----------------------------------------------------------------->
        
	                           	<div class="columna_modificacion_suministro separacion_columnass">
									<div class="cabecera_columnas"><?=$oficina_modificacion_datossuministro2?></div>

                                
									<div class="componente_columna_modificacion_suministro_campo">                                
<?php
//Este array contendrá los posibles estados en los cuales se puede solicitar el bono social
									$array_opciones_potencia[0]=$opcion_potencia1;
									$array_opciones_potencia[1]=$opcion_potencia2;
									$array_opciones_potencia[2]=$opcion_potencia3;

//Esta variable controlara el numero de idiomas que se han mostrado en el combobox de idiomas de facturacion	
									$opciones_potencia_mostradas=0;								
?>                   																	                                
								  	<select name="OpcionesPotencia" id="OpcionesPotencia" onchange="dar_valor_seccion_activa('<?=$_SESSION["idioma"]?>');" value="<?=$opciones_potencia_mostrar?>">
								    	<option value="-1">&nbsp;</option>
<?php 
//Recorremos el array de idiomas y los mostramos en el combobox correspondiente
										/*while($opciones_potencia_mostradas<count($array_opciones_potencia))
										{
?>											
											<option value="<?=$opciones_potencia_mostradas;?>"><?=$array_opciones_potencia[$opciones_potencia_mostradas];?></option>
<?php 		
											$opciones_potencia_mostradas++;
										}//while($opciones_potencia_mostradas<count($array_opciones_potencia)) */
										
										switch($_SESSION["seccion_origen_modificacion"]){
											case 0:
												echo '<option value="0" selected="selected">'.$array_opciones_potencia[0].'</option>';
												echo '<option value="1">'.$array_opciones_potencia[1].'</option>';
												echo '<option value="2">'.$array_opciones_potencia[2].'</option>';
											break;
											case 1:
												echo '<option value="0">'.$array_opciones_potencia[0].'</option>';
												echo '<option value="1" selected="selected">'.$array_opciones_potencia[1].'</option>';
												echo '<option value="2">'.$array_opciones_potencia[2].'</option>';
											break;
											case 2:
												echo '<option value="0">'.$array_opciones_potencia[0].'</option>';
												echo '<option value="1">'.$array_opciones_potencia[1].'</option>';
												echo '<option value="2" selected="selected">'.$array_opciones_potencia[2].'</option>';
											break;
											default:
												echo '<option value="0">'.$array_opciones_potencia[0].'</option>';
												echo '<option value="1">'.$array_opciones_potencia[1].'</option>';
												echo '<option value="2">'.$array_opciones_potencia[2].'</option>';
											break;
										}
?>
									  </select>                                                                         
								</div><!--<div class="componente_columna_modificacion_suministro_campo>-->

								<div class="componente_columna_modificacion_suministro_campo">                                
<script>
							function pasar_variable(){	
								var miVariable = document.form_alta.PotenciaNormalizada.value;
								document.cookie ='potencia='+miVariable+';';
								location.reload();
							}
							
							actualizar_tarifa(<?=$_SESSION["seccion_origen_modificacion"]?>, <?php if(isset($t20a)){echo $t20a;} else { echo "0";}?>, <?php if(isset($t20dha)){echo $t20dha;} else { echo "0";}?>, <?php if(isset($t21a)){echo $t21a;} else { echo "0";}?>, <?php if(isset($t21dha)){echo $t21dha;} else { echo "0";}?>, <?php if(isset($t30a)){echo $t30a;} else { echo "0";}?>, <?php if(isset($t31a)){echo $t31a;} else { echo "0";}?>, <?php if(isset($t61a)){echo $t61a;} else { echo "0";}?>);
							</script> 
<!--Cuando se selecciona una potencia, se asignara automaticamente la tarifa correspondiente-->
							<select name="PotenciaNormalizada" id="PotenciaNormalizada" onchange="pasar_variable()">
							  <option value="-1">&nbsp;</option>
                               
                            <?php
								$potenciaElegida =  $_COOKIE["potencia"];
								$potenciaSeparada = explode(" ",$potenciaElegida);
							?>
<?php
//Consultamos en la tabla PotenciasNormalizadas las potencias disponibles. NO se puede utilizar las funciones de consulta, porque se cerraria la conexion al estar empleando el metodo antiguo. Las potencias se ordenaran por su valor y por la tarifa a la que pertenecen, ya que son unicas
							$rs_potencias_normalizadas = mysql_query('SELECT * FROM PotenciasNormalizadas ORDER BY Potencia,idTarifa');
							?>
                                <option value="<?=$potenciaSeparada[0];?>" selected="selected"><?php echo $potenciaSeparada[0];?></option>
                                <?php
							while($registro_potencias_normalizadas=mysql_fetch_array($rs_potencias_normalizadas))
							{						
//Esta variable almacenara el numero de tarifas que se ha comprobado								
								$potencias_comprobadas=0;
																
								while($potencias_comprobadas<count($array_tarifas_disponibles))
								{	
//Ahora comprobamos que la tarifa en la que nos encontramos es una de las que está disponible para la potencia en la que nos encontremos (que 	dependerá a la seccion a la que se haya accedido). Solo se mostraran las tarifas disponibles dependiendo de la poencia seleccionada								
									if($array_tarifas_disponibles[$potencias_comprobadas]==$registro_potencias_normalizadas["idTarifa"])
									{																		
//Asignamos el valor a cada opcion como una concatenacion de la potencia, un simbolo "+" y el ID de la tarifa que le corresponde, de esta manera no hay que repetir consultas a la base de datos para saber que tarifa se le asigna a cada potencia
?>
										<option value="<?=$registro_potencias_normalizadas["Potencia"]."+".$registro_potencias_normalizadas["idTarifa"]."+".$registro_potencias_normalizadas["Tension"]."+".$registro_potencias_normalizadas["Fases"]?>"><?=$registro_potencias_normalizadas["Potencia"]?></option>
                    	
<?php            
									}//if($array_tarifas_disponibles[$potencias_comprobadas]==$registro_potencias_normalizadas["idTarifa"])
							$potencias_comprobadas++;										
						}//while($potencias_comprobadas<count($array_tarifas_disponibles))
					}//while($registro_potencias_normalizadas=mysql_fetch_array($rs_potencias_normalizadas))                              
?>                                                            
						</select>&nbsp;&nbsp;&nbsp;kW
						</div><!--<div class="componente_columna_modificacion_suministro_campo>-->

<!--La tercera columna contendra los combos con los nuevos valores y el comportamiento pertinente-->                                                                                       
					<div class="componente_columna_modificacion_suministro_campo">                                                    
<?php
//Consultamos en la tabla TarifasDetalles las tarifas disponibles. NO se puede utilizar las funciones de consulta, porque se cerraria la conexion al estar empleando el metodo antiguo
						$rs_tarifas= mysql_query('SELECT * FROM TarifasDetalles');		
?>                   
						  <select name="TarifaContratada" id="TarifaContratada" onchange="actualizar_periodos_tarifa(<?=$_SESSION["seccion_origen_modificacion"]?>, <?php if(isset($t20a)){echo $t20a;} else { echo "0";}?>, <?php if(isset($t20dha)){echo $t20dha;} else { echo "0";}?>, <?php if(isset($t21a)){echo $t21a;} else { echo "0";}?>, <?php if(isset($t21dha)){echo $t21dha;} else { echo "0";}?>, <?php if(isset($t30a)){echo $t30a;} else { echo "0";}?>, <?php if(isset($t31a)){echo $t31a;} else { echo "0";}?>, <?php if(isset($t61a)){echo $t61a;} else { echo "0";}?>); ">
				    		<option value="-1">&nbsp;</option>
                                                         
<?php 
//Vamos mostrando todas las tarifas encontradas
							while($registro_tarifas=mysql_fetch_array($rs_tarifas))
							{
//Esta variable almacenara el numero de tarifas que se ha comprobado								
								$tarifas_comprobadas=0;
									
								while($tarifas_comprobadas<count($array_tarifas_disponibles))
								{
//Ahora comprobamos que la tarifa en la que nos encontramos es una de las que está disponible para la potencia en la que nos encontremos (que 	dependerá a la seccion a la que se haya accedido). Solo se mostraran las tarifas disponibles dependiendo de la poencia seleccionada								
									if($array_tarifas_disponibles[$tarifas_comprobadas]==$registro_tarifas["IdTarifa"])
									{
										$tarifacontratada2=$registro_tarifas["Tarifa"]	
?>
										<option value="<?=$registro_tarifas["IdTarifa"];?>"><?=$registro_tarifas["Tarifa"]?></option>
<?php										
									}//if($array_tarifas_disponibles[$tarifas_comprobadas]==$registro_tarifas["IdTarifa"])
																														
									$tarifas_comprobadas++;
								}//while($tarifas_comprobadas<count($array_tarifas_disponibles))
							}//while($registro_tarifas=mysql_fetch_array($rs_tarifas))
?>
						  </select>	
                                            
                                        
					</div><!--<div class="componente_columna_modificacion_suministro_campo">-->
                    
					<div class="componente_columna_modificacion_suministro_campo">                                
	                    <select style='display:none' name="Discriminador" id="Discriminador" onchange="cambiar_periodos_tarifarios_discriminador(<?=$_SESSION["seccion_origen_modificacion"]?>);">
    		              <option value="<?=$_SESSION["seccion_origen_modificacion"];?>" selected="selected">&nbsp;</option>              
                    
<!--Como las funcionalidades cambian dependiendo de la seccion que se encuentre activa, añadimos el indice de la seccion al atributo value del discriminador, de esta forma podremos condicionar las acciones-->
            		      <option value="<?=$_SESSION["seccion_origen_modificacion"];?>">DHA</option>
		                </select>
					</div><!--<div class="componente_columna_modificacion_suministro_campo">-->                   

					<div class="componente_columna_modificacion_suministro_campo">                                
	                    <select name="Reactiva" id="Reactiva"  onchange="asignar_periodos_reactiva()">
                          <option value="No" selected="selected">
                            <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 170'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                            </option>
                          <option value="Si">
                            <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 171'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                            </option>
                        </select> 
					</div><!--<div class="componente_columna_modificacion_suministro_campo">-->                   

					<div class="componente_columna_modificacion_suministro_campo">                                
                        <select name="Maximetro" id="Maximetro"  onchange="asignar_modo();">
                          <option value="No" selected="selected">
                          <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 170'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                          </option>
                          <option value="Si">
                          <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 171'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                          </option>
                        </select>                    
					</div><!--<div class="componente_columna_modificacion_suministro_campo">-->                      


					<div class="componente_columna_modificacion_suministro_campo">                                
	                    <input name="Modo" type="text" class="textfieldCentrado3" id="Modo" size="2" maxlength="1" readonly="readonly" value="1">
					</div><!--<div class="componente_columna_modificacion_suministro_campo">-->                      

					<div class="componente_columna_modificacion_suministro_campo">                                
						<select name="TipoTension" size="1" id="TipoTension">
							<option value="" selected="selected">&nbsp;</option>
                                                                                                                                
<?php
//Consultamos todas laas tensiones distintas que se encuentran en la tabla de potencias normalizadas
					
					switch($_SESSION["seccion_origen_modificacion"])	
					{
						//10 kw/h				
						case 0:
							$rs_tensiones = mysql_query('SELECT DISTINCT(Tension) FROM PotenciasNormalizadas where idTarifa ='.$t20a.' or idTarifa ='.$t20dha.' ORDER BY Tension');	
							break;
						case 1:
							$rs_tensiones = mysql_query('SELECT DISTINCT(Tension) FROM PotenciasNormalizadas where idTarifa ='.$t21a.' or idTarifa ='.$t21dha.' ORDER BY Tension');	
							break;
						case 2:
							$rs_tensiones = mysql_query('SELECT DISTINCT(Tension) FROM PotenciasNormalizadas where idTarifa ='.$t30a.' or idTarifa ='.$t31a.' or idTarifa ='.$t61a.' ORDER BY Tension');	
							break;
					}
					
					
//Ahora mostramos las tensiones utilizando el array construcido cuando se ha realizado la consulta de las potencias normalizadas
							while($registro_tensiones=mysql_fetch_array($rs_tensiones))
							{
?>
								<option value="<?=$registro_tensiones["Tension"]?>" selected="selected"><?=$potenciaSeparada[2]?></option>
<?php
	
							}//while($registro_potencias_normalizadas=mysql_fetch_array($rs_potencias_normalizadas))*/
?>
						</select>                    
					</div><!--<div class="componente_columna_modificacion_suministro_campo">-->                      

					<div class="componente_columna_modificacion_suministro_campo">                                
	                    <input name="Fases" type="text" class="textfieldCentrado3" id="Fases" size="2" maxlength="20" readonly="readonly" value="<?=$potenciaSeparada[3]?>">
					</div><!--<div class="componente_columna_modificacion_suministro_campo">-->                   

					<div class="componente_columna_modificacion_suministro_campo">                                
						<select name="EquiposMedida" id="EquiposMedida">
							<option value="1" selected="selected"><?=$equipos_medida1?></option>
                            <option value="2"><?=$equipos_medida2?></option>   
                                                 
                        </select>                    
					</div><!--<div class="componente_columna_modificacion_suministro_campo">-->
                    
                </div><!--<div class="columna_modificacion_suministro separacion_columnas">-->
                                
                                                                
               </td>
       		  </tr>


<!--CAMPO NUEVO      -->                

						<tr> 
                          <td height="30" valign="top" class="arial14">

		

                       <select style='display:none' name="PeriodosTarifarios" size="1" id="PeriodosTarifarios" onchange="actualizar_tabla(<?=$_SESSION["seccion_origen_modificacion"]?>);">
                              <option value="0" selected="selected">&nbsp;</option>
                                  <option value="1"><?=$opcion_periodo_tarifario1?></option>
                                  <option value="2"><?=$opcion_periodo_tarifario2?></option>                      
	                              <option value="3"><?=$opcion_periodo_tarifario3?></option>
                                  <option value="4"><?=$opcion_periodo_tarifario4?></option>                       </select>
                            
                          
               			  </td>
                        </tr>   
                        
					<tr> 
                          <td height="25" valign="top" class="arial14">	               
                			<table border="1" bordercolor="#ffffff" width="500" cellpadding="0" cellspacing="0">
							  <tr bordercolor="#ffffff">
							    <td align="center" width="25%">&nbsp;</td>
							    <td align="center" width="25%"><strong><?=$oficina_contratacion_alta7?></strong></td>
							    <td align="center" width="25%"><strong><?=$oficina_contratacion_alta8?></strong></td>
							    <td align="center" width="25%"><strong><?=$oficina_contratacion_alta9?></strong></td>
                                
							  </tr>
                              
							  <tr bordercolor="#ffffff">
							    <td align="center" width="25%"><strong><?=$periodo_tarifario1?></strong></td>
                               
      
                               
                                      
									  <?php  if (document.form_alta.P1_maximetro.value=='No') {$clase='textfieldCentrado2';} else {$clase='textfieldCentrado';} 
                                      
                                      echo("<script type='text/javascript' language='javascript'>");
			if (document.form_alta.P1_maximetro.value=='No') {$clase='textfieldCentrado2';} else {$clase='textfieldCentrado';}
		echo("</script>");
                                      ?>
															
                                <?php $P1_activa='No'; if ($P1_activa=='No') {$clase='textfieldCentrado2';} else {$clase='textfieldCentrado';} ?>  
                                <td align="center" width="25%"><input id="P1_activa" name="P1_activa" type="text" readonly="readonly" class="<?php echo $clase; ?>" size="5" maxlength="2" value="<?php echo $P1_activa; ?>"/></td>
                                <?php $P1_reactiva='No'; if ($P1_reactiva=='No') {$clase='textfieldCentrado2';} else {$clase='textfieldCentrado';} ?>                                  
								<td align="center" width="25%"><input id="P1_reactiva" name="P1_reactiva" type="text" readonly="readonly" class="<?php echo $clase; ?>" size="5" maxlength="2" value="<?php echo $P1_reactiva; ?>"/></td>
                                <?php $P1_maximetro='No'; if ($P1_maximetro=='No') {$clase='textfieldCentrado2';} else {$clase='textfieldCentrado';} ?>  
								<td align="center" width="25%"><input id="P1_maximetro" name="P1_maximetro" type="text" readonly="readonly" class="<?php echo $clase; ?>" size="5" maxlength="2" value="<?php echo $P1_maximetro; ?>"/></td>
							  </tr>
                              
                              <tr bordercolor="#ffffff">
                                <td align="center" width="25%"><strong><?=$periodo_tarifario2?></strong></td>
                                <?php $P2_activa='No'; if ($P2_activa=='No') {$clase='textfieldCentrado2';} else {$clase='textfieldCentrado';} ?>
                                <td align="center" width="25%"><input id="P2_activa" name="P2_activa" type="text" readonly="readonly" class="<?php echo $clase; ?>" size="5" maxlength="2" value="<?php echo $P2_activa; ?>"/></td>
                                <?php $P2_reactiva='No'; if ($P2_reactiva=='No') {$clase='textfieldCentrado2';} else {$clase='textfieldCentrado';} ?>
								<td align="center" width="25%"><input id="P2_reactiva" name="P2_reactiva" type="text" readonly="readonly" class="<?php echo $clase; ?>" size="5" maxlength="2" value="<?php echo $P2_reactiva; ?>"/></td>
								<?php $P2_maximetro='No'; if ($P2_maximetro=='No') {$clase='textfieldCentrado2';} else {$clase='textfieldCentrado';} ?>
                                <td align="center" width="25%"><input id="P2_maximetro" name="P2_maximetro" type="text" readonly="readonly" class="<?php echo $clase; ?>" size="5" maxlength="2" value="<?php echo $P2_maximetro; ?>"/></td>
                              </tr>
                              
                              <tr bordercolor="#ffffff">
                                <td align="center" width="25%"><strong><?=$periodo_tarifario3?></strong></td>
								<?php $P3_activa='No'; if ($P3_activa=='No') {$clase='textfieldCentrado2';} else {$clase='textfieldCentrado';} ?>
                                <td align="center" width="25%"><input id="P3_activa" name="P3_activa" type="text" readonly="readonly" class="<?php echo $clase; ?>" size="5" maxlength="2" value="<?php echo $P3_activa; ?>"/></td>
                                <?php $P3_reactiva='No'; if ($P3_reactiva=='No') {$clase='textfieldCentrado2';} else {$clase='textfieldCentrado';} ?>
								<td align="center" width="25%"><input id="P3_reactiva" name="P3_reactiva" type="text" readonly="readonly" class="<?php echo $clase; ?>" size="5" maxlength="2" value="<?php echo $P3_reactiva; ?>"/></td>
								<?php $P3_maximetro='No'; if ($P3_maximetro=='No') {$clase='textfieldCentrado2';} else {$clase='textfieldCentrado';} ?>                                
								<td align="center" width="25%"><input id="P3_maximetro" name="P3_maximetro" type="text" readonly="readonly" class="<?php echo $clase; ?>" size="5" maxlength="2" value="<?php echo $P3_maximetro; ?>"/></td>
                              </tr>
                              
                              <tr bordercolor="#ffffff">
                                <td align="center" width="25%"><strong>P4</strong></td>
                                <?php $P4_activa='No'; if ($P4_activa=='No') {$clase='textfieldCentrado2';} else {$clase='textfieldCentrado';} ?>
								<td align="center" width="25%"><input id="P4_activa" name="P4_activa" type="text" readonly="readonly" class="<?php echo $clase; ?>" size="5" maxlength="2" value="<?php echo $P4_activa; ?>"/></td>
                                <?php $P4_reactiva='No'; if ($P4_reactiva=='No') {$clase='textfieldCentrado2';} else {$clase='textfieldCentrado';} ?>
                                <td align="center" width="25%"><input id="P4_reactiva" name="P4_reactiva" type="text" readonly="readonly" class="<?php echo $clase; ?>" size="5" maxlength="2" value="<?php echo $P4_reactiva; ?>"/></td>
								<?php $P4_maximetro='No'; if ($P4_maximetro=='No') {$clase='textfieldCentrado2';} else {$clase='textfieldCentrado';} ?>                                
                                <td align="center" width="25%"><input id="P4_maximetro" name="P4_maximetro" type="text" readonly="readonly" class="<?php echo $clase; ?>" size="5" maxlength="2" value="<?php echo $P4_maximetro; ?>"/></td>
                              </tr>
                              
                              <tr bordercolor="#ffffff">
                                <td align="center" width="25%"><strong>P5</strong></td>
                                <?php $P5_activa='No'; if ($P5_activa=='No') {$clase='textfieldCentrado2';} else {$clase='textfieldCentrado';} ?>                                
								<td align="center" width="25%"><input id="P5_activa" name="P5_activa" type="text" readonly="readonly" class="<?php echo $clase; ?>" size="5" maxlength="2" value="<?php echo $P5_activa; ?>"/></td>
                                <?php $P5_reactiva='No'; if ($P5_reactiva=='No') {$clase='textfieldCentrado2';} else {$clase='textfieldCentrado';} ?>                                
								<td align="center" width="25%"><input id="P5_reactiva" name="P5_reactiva" type="text" readonly="readonly" class="<?php echo $clase; ?>" size="5" maxlength="2" value="<?php echo $P5_reactiva; ?>"/></td>
								<?php $P5_maximetro='No'; if ($P5_maximetro=='No') {$clase='textfieldCentrado2';} else {$clase='textfieldCentrado';} ?>                                                                
								<td align="center" width="25%"><input id="P5_maximetro" name="P5_maximetro" type="text" readonly="readonly" class="<?php echo $clase; ?>" size="5" maxlength="2" value="<?php echo $P5_maximetro; ?>"/></td>
						  </td>
                              </tr>
                              
                              <tr bordercolor="#ffffff">
                                <td align="center" width="25%"><strong>P6</strong></td>
                                <?php $P6_activa='No'; if ($P6_activa=='No') {$clase='textfieldCentrado2';} else {$clase='textfieldCentrado';} ?>                                								
                                <td align="center" width="25%"><input id="P6_activa" name="P6_activa" type="text" readonly="readonly" class="<?php echo $clase; ?>" size="5" maxlength="2" value="<?php echo $P6_activa; ?>"/></td>
                                <?php $P6_reactiva='No'; if ($P6_reactiva=='No') {$clase='textfieldCentrado2';} else {$clase='textfieldCentrado';} ?>                                                                
								<td align="center" width="25%"><input id="P6_reactiva" name="P6_reactiva" type="text" readonly="readonly" class="<?php echo $clase; ?>" size="5" maxlength="2" value="<?php echo $P6_reactiva; ?>"/></td>
								<?php $P6_maximetro='No'; if ($P6_maximetro=='No') {$clase='textfieldCentrado2';} else {$clase='textfieldCentrado';} ?>
                                <td align="center" width="25%"><input id="P6_maximetro" name="P6_maximetro" type="text" readonly="readonly" class="<?php echo $clase; ?>" size="5" maxlength="2" value="<?php echo $P6_maximetro; ?>"/></td>
                              </tr>
                              
                            </table>                
                			</td>
                        </tr>
                     
       					<tr>
		                    <td height="10">&nbsp;</td>	
    	                </tr>
                        

<!--CAMPO NUEVO-->                                            

                    </table>
                    </fieldset></td>
	                <td>&nbsp;</td>
                </tr>


                <tr class="arial12"> 
                  <td height="10" class="arial14"></td>
                  <td class="arial14"></td>
                </tr>
               

                <tr class="arial12"> 
                  <td height="20" class="arial14">&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                       
                       
                <tr>
	                <td height="30" align="center" class="arial14">
                    <input name="Condiciones" type="checkbox" value="Yes" id="Condiciones"/>He leído y Acepto las Condiciones de Modificación<br/>
                    <td>&nbsp;</td>
                </tr>
                                      
				<tr>
	                <td height="30" align="center" class="arial14">
                    <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 153'); $rowTexto = mysql_fetch_array($resultTexto);?>
                    <input name="submit" type="submit" id="submit" value="<?php echo($rowTexto['Texto']);?>"> 
                    <td>&nbsp;</td>
				</tr>
                      
                <tr class="arial12"> 
                  <td height="20" class="arial14">&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
               
              </table></td>
            </tr>
          </table>
          
          </form>



<!--VERSION VIEJA-->
                	
		</div><!--<div class="contenido_seccion_oficina">-->                        
		
        
        <div class="limpiar"></div>
                        	
<!--Por ultimo aparecera el pie de la web-->
<?php 
	include("../includes/pie.php");

//NOTA: EN ESTA OCASION, EL ERROR SE MOSTRARA DESPUES DE LA CARGA DE LA PAGINA, SI NO POR ALGUN MOTIVO EN OCASIONES DESPUES DE DAR EL ERROR CESA LA CARGA DE LA PAGINA
	if(isset($_POST["submit"]))
	{		
//Por ultimo mostraremos el error correspondiente, en el caso de que haya fallado alguna de las verificaciones, o continuaremos con el proceso		
		switch($error)
		{		
//Proceso OK	
			case 0:						
//Antes de realizar el alta en la base de datos, deberemos de preparar los datos que no nos da el formulario

//Como en este caso el cliente ya existe, el codigo del cliente no hace falta calcularlo. Lo mismo sucede con el CUPS
				
				$cod_usuario=$row["CodigoCliente"];
				$cambiosecciondatostecnicos="Si";

//Como previamente se ha realizado una consulta de los datos del usuario, ahora simplemente asignamos valor a las variables antes de hacer la inserccion o modificacion en la base de datos
				
				$suministro_poblacion=str_replace("'","\'",$row["SuministroPoblacion"]);
				$suministro_provincia=$row["SuministroProvincia"];				
				$suministro_calle=$row["SuministroCalle"];								
				$suministro_numero=$row["SuministroNumero"];				
				$suministro_extension=$row["SuministroExtension"];								
				$suministro_aclarador=$row["SuministroExtension2"];				
				$suministro_cp=$row["SuministroCP"];	
				$suministro_pais=$row["SuministroPais"];			
				$suministro_telefono=$row["SuministroTelefono1"];				
				$suministro_movil=$row["SuministroTelefono2"];								
				$suministro_fax=$row["SuministroFax"];												
				$suministro_email=$row["SuministroEmail"];																
				
				$titular_nombre=str_replace("'","\'",$row["TitularNombre"]);
				$titular_calle=str_replace("'","\'",$row["TitularCalle"]);
				$titular_numero=str_replace("'","\'",$row["TitularNumero"]);
				$titular_extension=str_replace("'","\'",$row["TitularExtension"]);
				$titular_aclarador=str_replace("'","\'",$row["TitularAclarador"]);
				$titular_poblacion=str_replace("'","\'",$row["TitularPoblacion"]);
				$titular_provincia=str_replace("'","\'",$row["TitularProvincia"]);
				$titular_CP=$row["TitularCP"];
				$titular_pais=$row["TitularPais"];
				$titular_telefono=$row["TitularTelefono1"];
				$titular_movil=$row["TitularTelefono2"];
				$titular_fax=$row["TitularFax"];
				$titular_email=$row["TitularEmail"];
				$titular_dni=$row["TitularDNI"];
							
				$factura_nombre=str_replace("'","\'",$row["FacturaNombre"]);
				$factura_calle=str_replace("'","\'",$row["FacturaCalle"]);
				$factura_numero=str_replace("'","\'",$row["FacturaNumero"]);
				$factura_extension=str_replace("'","\'",$row["FacturaExtension"]);
				$factura_aclarador=str_replace("'","\'",$row["FacturaAclarador"]);
				$factura_poblacion=str_replace("'","\'",$row["FacturaPoblacion"]);
				$factura_provincia=str_replace("'","\'",$row["FacturaProvincia"]);
				$factura_CP=$row["FacturaCP"];
				$factura_pais=str_replace("'","\'",$row["FacturaPais"]);
				$factura_telefono=$row["FacturaTelefono1"];
				$factura_movil=$row["FacturaTelefono2"];
				$factura_fax=$row["FacturaFax"];
				$factura_email=str_replace("'","\'",$row["FacturaEmail"]);
				$factura_dni=$row["FacturaDNI"];
				
				
				$envio_nombre=str_replace("'","\'",$row["EnvioNombre"]);
				$envio_calle=str_replace("'","\'",$row["EnvioCalle"]);
				$envio_numero=str_replace("'","\'",$row["EnvioNumero"]);
				$envio_extension=str_replace("'","\'",$row["EnvioExtension"]);
				$envio_aclarador=str_replace("'","\'",$row["EnvioAclarador"]);
				$envio_poblacion=str_replace("'","\'",$row["EnvioPoblacion"]);
				$envio_provincia=str_replace("'","\'",$row["EnvioProvincia"]);
				$envio_CP=$row["EnvioCP"];
				$envio_pais=$row["EnvioPais"];
				$envio_telefono=$row["EnvioTelefono1"];
				$envio_movil=$row["EnvioTelefono2"];
				$envio_fax=$row["EnvioFax"];
				$envio_email=str_replace("'","\'",$row["EnvioEmail"]);
				$envio_dni=$row["EnvioDNI"];
				
				
							
				$pagador_nombre=str_replace("'","\'",$row["PagadorNombre"]);
				$pagador_calle=str_replace("'","\'",$row["PagadorCalle"]);
				$pagador_numero=str_replace("'","\'",$row["PagadorNumero"]);
				$pagador_extension=str_replace("'","\'",$row["PagadorExtension"]);
				$pagador_aclarador=str_replace("'","\'",$row["PagadorAclarador"]);
				$pagador_poblacion=str_replace("'","\'",$row["PagadorPoblacion"]);
				$pagador_provincia=str_replace("'","\'",$row["PagadorProvincia"]);
				$pagador_CP=$row["PagadorCP"];
				$pagador_pais=$row["PagadorPais"];
				$pagador_telefono=$row["PagadorTelefono1"];
				$pagador_movil=$row["PagadorTelefono2"];
				$pagador_fax=$row["PagadorFax"];
				$pagador_email=str_replace("'","\'",$row["PagadorEmail"]);
				$pagador_dni=$row["PagadorDNI"];
		
				$representante_nombre=str_replace("'","\'",$row["RepresentanteEmpresa"]);
				$representante_dni=str_replace("'","\'",$row["DNIRepresentante"]);
													
				$num_cuenta_completo=$row["NumeroCuenta"];
							
				$forma_pago=$row["FormaPago"];
				$idioma_factura=$row["IdiomaFactura"];	
				$nombrebanco=$row["BancoDomiciliado"];

//Por otro lado las variables que no han requerido identificacion no las hemos recogido, por lo tanto lo haremos ahora antes de la inserccion o modificacion
				$potencia_normalizada=$_POST["PotenciaNormalizada"];
				$opciones_potencia=$_POST["OpcionesPotencia"];
				$tipo_tension=$_POST["TipoTension"];	
				$discriminador=$_POST["Discriminador"];
				$equipos_medida=$_POST["EquiposMedida"];
				$maximetro=$_POST["Maximetro"];
				$modo=$_POST["Modo"];
				$fases=$_POST["Fases"];
				$reactiva=$_POST["Reactiva"];				
				$observaciones=$_POST["Observaciones"];
				
				$p1_activa=$_POST["P1_activa"];				
				$p1_reactiva=$_POST["P1_reactiva"];				
				$p1_maximetro=$_POST["P1_maximetro"];			

				$p2_activa=$_POST["P2_activa"];				
				$p2_reactiva=$_POST["P2_reactiva"];				
				$p2_maximetro=$_POST["P2_maximetro"];			
				
				$p3_activa=$_POST["P3_activa"];				
				$p3_reactiva=$_POST["P3_reactiva"];				
				$p3_maximetro=$_POST["P3_maximetro"];			
				
				$p4_activa=$_POST["P4_activa"];				
				$p4_reactiva=$_POST["P4_reactiva"];				
				$p4_maximetro=$_POST["P4_maximetro"];			
				
				$p5_activa=$_POST["P5_activa"];				
				$p5_reactiva=$_POST["P5_reactiva"];				
				$p5_maximetro=$_POST["P5_maximetro"];			
				
				$p6_activa=$_POST["P6_activa"];				
				$p6_reactiva=$_POST["P6_reactiva"];				
				$p6_maximetro=$_POST["P6_maximetro"];																			
				

//Los equipos de medida envia un indice y no la descripcion del equipo de medida, ahora lo sustituiremos antes de dar el alta
				switch($equipos_medida)
				{
					case 1:
						$equipos_medida="Alquiler Empresa";
					break;
					
					case 2:
						$equipos_medida="Propiedad Cliente";					
					break;					
				}//switch($equipos_medida)
				
				if($_POST["Discriminador"]!="-1")
				{
					$discriminador="a";				
				}//if($_POST["Discriminador"]!="-1")
				
				$result = mysql_query('SELECT IdTarifa FROM TarifasDetalles where Tarifa LIKE "%DH%" or Tarifa ="20DHA" or Tarifa ="2.0.DHA"');
				$row = mysql_fetch_array($result);
				$row[0];					
												
				switch($tarifa_contratar)
				{
					case $t20a:	$discriminador="-";	break;
					case $t20dha:	$discriminador="D";	break;
					case $t21a:	$discriminador="-";	break;
					case $t21dha:	$discriminador="D";	break;
					case $t30a:	$discriminador="-";	break;
					case $t31a:	$discriminador="-";	break;
					case $t61a:	$discriminador="-";	break;					
				}//switch($equipos_medida)
												
								
							
						$rs_tarifas= mysql_query('SELECT * FROM TarifasDetalles');	
//Vamos mostrando todas las tarifas encontradas
							while($registro_tarifas=mysql_fetch_array($rs_tarifas))
							{
//Esta variable almacenara el numero de tarifas que se ha comprobado								
								$tarifas_comprobadas=0;
									
								while($tarifas_comprobadas<count($array_tarifas_disponibles))
								{
//Ahora comprobamos que la tarifa en la que nos encontramos es una de las que está disponible para la potencia en la que nos encontremos (que 	dependerá a la seccion a la que se haya accedido). Solo se mostraran las tarifas disponibles dependiendo de la poencia seleccionada								
									if($tarifa_contratar==$registro_tarifas["IdTarifa"])
									{
									$tarifa_contratar2=$registro_tarifas["Tarifa"];
									}//if($array_tarifas_disponibles[$tarifas_comprobadas]==$registro_tarifas["IdTarifa"])
																														
									$tarifas_comprobadas++;
								}//while($tarifas_comprobadas<count($array_tarifas_disponibles))
							}//while($registro_tarifas=mysql_fetch_array($rs_tarifas))

																
//Las modificaciones en realidad no cambian los datos de la tabla DatosRegistrados, si no que dan un alta en la tabla DatosModificados. Aunque se hace la inserccion completa, algunos campos quedaran en blanco, ya que se cambian desde la seccion de oficina_modificacion_datossuministro

//Ahora dependiendo de si el usuario ya tiene un registro o no en la tabla de DatosModificados, se realizara un alta nueva o una actualizacion del registro

					if($tiene_modificaciones_pendientes==0)
					{						
					
					$nombrebanco=str_replace("'","\"",$nombrebanco);
					
					$potenciaSeparada = explode("+",$potencianormalizada);
					$potencianormalizada = $potenciaSeparada[0];
								
													
						$solicitud_modificacion="INSERT INTO `datosmodificados` (`CodigoCliente`, `Usuario`, `Password`, `DNI`, `SuministroCiudad`, `SuministroProvincia`, `SuministroCalle`, `SuministroNumero`, `SuministroExtension`, `SuministroExtension2`, `SuministroCP`, `SuministroTelefono1`, `SuministroTelefono2`, `SuministroFax`, `SuministroEmail`, `TitularNombre`, `TitularCalle`, `TitularNumero`, `TitularExtension`, `TitularAclarador`, `TitularCP`, `TitularPoblacion`, `TitularProvincia`, `TitularPais`, `TitularTelefono1`, `TitularTelefono2`, `TitularFax`, `TitularEmail`, `TitularDNI`, `FacturaNombre`, `FacturaCalle`, `FacturaNumero`, `FacturaExtension`, `FacturaAclarador`,`FacturaCP`, `FacturaPoblacion`, `FacturaProvincia`,`FacturaPais`, `FacturaTelefono1`, `FacturaTelefono2`, `FacturaFax`, `FacturaEmail`, `FacturaDNI`, `EnvioNombre`, `EnvioCalle`, `EnvioNumero`, `EnvioExtension`, `EnvioAclarador`,`EnvioCP`, `EnvioPoblacion`, `EnvioProvincia`,`EnvioPais`, `EnvioTelefono1`, `EnvioTelefono2`, `EnvioFax`, `EnvioEmail`, `EnvioDNI`, `PagadorNombre`, `PagadorCalle`, `PagadorNumero`, `PagadorExtension`, `PagadorAclarador`, `PagadorCP`,`PagadorPoblacion`, `PagadorProvincia`, `PagadorPais`,`PagadorTelefono1`, `PagadorTelefono2`, `PagadorFax`, `PagadorEmail`, `PagadorDNI`, `RepresentanteEmpresa`, `DNIRepresentante`, `CUPS`, `FormaPago`, `BancoDomiciliado`, `NumeroCuenta`, `IdiomaFactura`,`TarifaContratada`, `PotenciaContratada`, `TipoTension`, `Discriminador`, `TipoAlquiler`, `Maximetro`, `Modo`, `Observaciones`, `FechaRegistro`, `HoraRegistro`, `Fases`, `Reactiva`, `P1Activa`, `P1Reactiva`, `P1Maximetro`, `P2Activa`, `P2Reactiva`, `P2Maximetro`, `P3Activa`, `P3Reactiva`, `P3Maximetro`, `P4Activa`, `P4Reactiva`, `P4Maximetro`, `P5Activa`, `P5Reactiva`, `P5Maximetro`, `P6Activa`, `P6Reactiva`, `P6Maximetro`, `CambioSeccionBanco`, `CambioSeccionDatosTecnicos`, `cambiotitular`, `cambiofactura`, `cambioenvio`, `cambiopagador`, `cambiodomiciliacion`, `cambiorepresentante`)
						
						 VALUES
						 
						  ('".$cod_usuario."', '".$_SESSION['usuario']."', '".$_SESSION['password']."', '".$_SESSION['DNI']."', '".$suministro_poblacion."', '".$suministro_provincia."', '".$suministro_calle."', '".$suministro_numero."', '".$suministro_extension."', '".$suministro_aclarador."', '".$suministro_cp."','".$suministro_telefono."', '".$suministro_movil."', '".$suministro_fax."', '".$suministro_email."', '".$titular_nombre."','".$titular_calle."','".$titular_numero."' , '".$titular_extension."', '".$titular_aclarador."', '".$titular_cp."','".$titular_poblacion."', '".$titular_provincia."', '".$titular_pais."', '".$titular_telefono."', '".$titular_movil."', '".$titular_fax."', '".$titular_email."', '".$titular_dni."', '".$factura_nombre."', '".$factura_calle."', '".$factura_numero."', '".$factura_extension."', '".$factura_aclarador."', '".$factura_cp."' , '".$factura_poblacion."', '".$factura_provincia."', '".$factura_pais."', '".$factura_telefono."', '".$factura_movil."', '".$factura_fax."', '".$factura_email."', '".$factura_dni."', '".$envio_nombre."', '".$envio_calle."', '".$envio_numero."', '".$envio_extension."', '".$envio_aclarador."', '".$envio_cp."' , '".$envio_poblacion."', '".$envio_provincia."', '".$envio_pais."', '".$envio_telefono."', '".$envio_movil."', '".$envio_fax."', '".$envio_email."', '".$envio_dni."', '".$pagador_nombre."', '".$pagador_calle."', '".$pagador_numero."', '".$pagador_extension."', '".$pagador_aclarador."', '".$pagador_cp."', '".$pagador_poblacion."', '".$pagador_provincia."', '".$pagador_pais."', '".$pagador_telefono."', '".$pagador_movil."', '".$pagador_fax."', '".$pagador_email."', '".$pagador_dni."', '".$representante_nombre."', '".$representante_dni."', '".$_POST["CUPSFijo"]."', '".$forma_pago."', '".$nombrebanco."', '".$num_cuenta_completo."','".$idioma_factura."','".$tarifa_contratar2."','".$potencia_normalizada."', '".$tipo_tension."', '".$discriminador."','".$equipos_medida."','".$maximetro."', '".$modo."', '".$Observaciones."', '".date("Y-m-d")."', '".date("Y-m-d H:i:s")."','".$fases."', '".$reactiva."', '".$p1_activa."', '".$p1_reactiva."', '".$p1_maximetro."', '".$p2_activa."', '".$p2_reactiva."', '".$p2_maximetro."', '".$p3_activa."', '".$p3_reactiva."', '".$p3_maximetro."', '".$p4_activa."', '".$p4_reactiva."', '".$p4_maximetro."', '".$p5_activa."', '".$p5_reactiva."', '".$p5_maximetro."', '".$p6_activa."', '".$p6_reactiva."', '".$p6_maximetro."', '".$cambioseccionbanco."', '".$cambiosecciondatostecnicos."' , '".$_POST['cambiotitular']."' , '".$_POST['cambiofactura']."' , '".$_POST['cambioenvio']."' , '".$_POST['cambiopagador']."' , '".$_POST['cambiodomiciliacion']."' , '".$_POST['cambiorepresentante']."');";										
				

				}//if($tiene_modificaciones_pendientes==0)

//Si el usuario ya tenia un registro en la base de datos, se realizara una actualizacion del registro				
				else				
				{									
					$solicitud_modificacion="UPDATE `datosmodificados` SET `PotenciaContratada` = '".$potencia_normalizada."', `TarifaContratada` = '".$tarifa_contratar2."',`Discriminador` = '".$discriminador."', `Maximetro` = '".$maximetro."',`Modo` = '".$modo."', `TipoTension` = '".$tipo_tension."',`Fases` = '".$fases."',`TipoAlquiler` = '".$equipos_medida."', `Reactiva` = '".$reactiva."',`Observaciones` = '".$observaciones."',`P1Activa` = '".$p1_activa."', `P1Reactiva` = '".$p1_reactiva."',`P1Maximetro` = '".$p1_maximetro."',`P2Activa` = '".$p2_activa."',`P2Reactiva` = '".$p2_reactiva."',`P2Maximetro` = '".$p2_maximetro."',`P3Activa` = '".$p3_activa."',`P3Reactiva` = '".$p3_reactiva."',`P3Maximetro` = '".$p3_maximetro."',`P4Activa` = '".$p4_activa."',`P4Reactiva` = '".$p4_reactiva."',`P4Maximetro` = '".$p4_maximetro."',`P5Activa` = '".$p5_activa."',`P5Reactiva` = '".$p5_reactiva."',`P5Maximetro` = '".$p5_maximetro."',`P6Activa` = '".$p6_activa."',`P6Reactiva` = '".$p6_reactiva."',`P6Maximetro` = '".$p6_maximetro."',`CambioSeccionDatosTecnicos` = '".$cambiosecciondatostecnicos."' WHERE `CodigoCliente` =".$cod_usuario.";";
																												
				}//else($tiene_modificaciones_pendientes==0)
								
//En cualquier caso ejecutamos la sentencia SQL contruida	

				$seccion_modificaciones="Datos Suministro";
				
				include("../includes/email_modificaciones.php");			
				
				//echo $solicitud_modificacion;
				$enlace =  mysql_connect('89.248.100.44', 'aguasbarbastro', 'oscar');	
				mysql_select_db('aguasbarbastro',$enlace);	
				$ejecutar_solicitud_modificacion=mysql_query($solicitud_modificacion);
															
				MsgBox($modificacion_ok);
				if($error_mail==1 or $error_dni==1 or $error_telefono==1){
					?><script type="text/javascript">
					var opciones = "width=650,height=400,scrollbars=NO,left=400,top=300";
					window.open("../includes/datos_importantes.php?mail=<?=$error_mail;?>&dni=<?=$error_dni;?>&telefono=<?=$error_telefono;?>&seccion=<?=$seccion_modificaciones;?>","Importante", opciones);
					</script>	
					<?php 
				}

//Establecemos la seccion activa como 0, de esta manera se mantendrá el valor del campo Opciones de potencia				
				$_SESSION["seccion_origen_modificacion"]=0;
				
//El campo de opciones de potencia no se debera de actualizar cuando se recarge la pagina tras la inserccion o modificacion
				$_SESSION["actualizar_opciones_potencia"]=0;
				
//Por utlimo reiniciaremos el formulario
			
				
				if($_SESSION["actualizar_opciones_potencia"]==1)
	{
		echo("<script type='application/javascript' language='javascript'>");	
			echo("dar_valor_opciones_potencia('".$_SESSION["seccion_origen_modificacion"]."');");
		echo("</script>");				
	}//if($_SESSION["actualizar_opciones_potencia"]==1)

//En el caso de que se haya accedido a la seccion de > 15kw/h, le damos valor a las demas variables que vendrán preestablecidas.
//Para ello llamaremos a la funcion javascript que actualiza los campos pertinentes. No se puede realizar la llamada al mismo tiempo que se realizan el resto de operaciones que dependen de la seccion activa, porque si no los valores por defecto de los componentes sustituyen a los dado por la funcion
	if($_SESSION["seccion_origen_modificacion"]==2)
	{	
		echo("<script type='text/javascript' language='javascript'>");
			echo("valores_mas_15kw();");
		echo("</script>");	
	}//if($_SESSION["seccion_origen_modificacion"]==2)		
					
										
			break;
				
//Error por no rellenar los campos obligatorios
			case 1:
				MsgBox($error_rellenar);
//El campo de opciones de potencia se debera de actualizar cuando se recarge la pagina tras la inserccion o modificacion								
				$_SESSION["actualizar_opciones_potencia"]=1;				
			break;									
			
			case 9:
			MsgBox($error_condiciones);//"No has aceptado las condiciones");												
			break;	

			
		}//switch($error)
	}//if (isset($_POST["submit"]))
}//else ($_SESSION['usuario'])
	
//Esto debe ocurrir cuando no se haya realizado una inserccion o modificacion correctamente	
//Llamamos a la funcion javascript que da valor a los datos bancarios. La funcion recibira los valores escritos en el formulario o los que hay en la base de datos dependidendo de la situacion	

//Si no se ha realizado una inserccion o modificacion correctamente, deberemos de actualizar el combo de opciones de potencia	
	if($_SESSION["actualizar_opciones_potencia"]==1)
	{
		echo("<script type='application/javascript' language='javascript'>");	
			echo("dar_valor_opciones_potencia('".$_SESSION["seccion_origen_modificacion"]."');");
		echo("</script>");				
	}//if($_SESSION["actualizar_opciones_potencia"]==1)

//En el caso de que se haya accedido a la seccion de > 15kw/h, le damos valor a las demas variables que vendrán preestablecidas.
//Para ello llamaremos a la funcion javascript que actualiza los campos pertinentes. No se puede realizar la llamada al mismo tiempo que se realizan el resto de operaciones que dependen de la seccion activa, porque si no los valores por defecto de los componentes sustituyen a los dado por la funcion
	if($_SESSION["seccion_origen_modificacion"]==2)
	{	
		echo("<script type='text/javascript' language='javascript'>");
			echo("valores_mas_15kw();");
		echo("</script>");	
	}//if($_SESSION["seccion_origen_modificacion"]==2)			
	
//Si la seccion no estaba establecedida mostraremos el formulario en su estado inicial, todos los campos dehabilitados salvo el de opciones de potencia a contratar. Tambien deberemos hacerlo en el caso de que la variable que indica la sesion este vacia, ya que al pulsar el boton de enviar el formulario se establece la varibale aunque sin valor.
	if(!isset($_GET["s"]) or $_GET["s"]=="")
	{
		
	}//if(!isset($_GET["s"]) or $_GET["s"]=="")
?>        
    </div><!--<div id="central_oficina"-->
</body>
</html>

