<html></html>
<head>
<title>Multienerg&iacute;a Verde - Actualidad</title>
<link rel="stylesheet" href="css/styles.css">
<link rel="stylesheet" href="css/stylesEmpresa.css">
<link rel="stylesheet" href="css/flexslider.css">
 <!-- Javascripts -->
	 <script type="text/javascript" src="js/jquery-1.7.min.js"></script>
	 <script type="text/javascript" src="js/jquery.flexslider.js"></script>
	 <script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
	 <script type="text/javascript">
		  $(function() {
				$('.flexslider').flexslider({
				  animation: "slide"
				});
			 });
	 </script>
<style type="text/css">
.style1 {
	text-decoration: underline;
}
</style>
</head>
<body>
<div id="contenedor">
	<?php include ('includes/header_secciones.php'); ?>
<div id="contenido">
          	<div id="slide">
				<div id="imagenContenido">
                	<img src="images/secciones/negocio.png" alt="Imagen Empresa" />
                </div>
              <div id="textoContenido">
               	<h2> Multienerg&iacute;a Verde</h2>
                  <p>Las Comercializadoras de energ&iacute;a tienen como funci&oacute;n la labor comercial del suministro de gas y/o electricidad a su hogar o lugar de trabajo. </p>
                  <p>Ejercen de intermediarias entre el cliente final y la empresa productora. Para ello utilizan la red de distribuci&oacute;n nacional.</p>
                  <p>Aqu&iacute; le ofrecemos un listado de todas las comercializadoras de energ&iacute;a en Espa&ntilde;a. Desde la liberaci&oacute;n del mercado de energ&iacute;a, usted puede elegir su comercializadora de energ&iacute;a. Puede ser una comercializadora de gas y de electricidad, pero tambi&eacute;n pueden ser 2 empresas diferentes: 1 para el suministro de gas y 1 para la electricidad.</p>
                  <p>Las Comercializadoras de energ&iacute;a tienen como funci&oacute;n la labor comercial del suministro de gas y/o electricidad a su hogar o lugar de trabajo.</p>
                </div>
                
			</div>
	</div>    
    <div id="clean"></div>
	<?php include ('includes/pie_secciones.php'); ?>
</div>

</body>