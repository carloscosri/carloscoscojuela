<?php 
session_start();
include("_conexion.php");

//Se comprueba que el usuario se haya identificado, si no es asi se da un aviso y redirigimos a la seccion de identificacion
if ($_SESSION['usuario']=="")
{	
	MsgBox($error_identificacion);
	redirigir("oficina_index.php?id=".$_SESSION["idioma"]);
	
}//if ($_SESSION['usuario'])

//Si el usuario se ha identificado la seccion se ejecutara con normalidad
else
{
	$sql = "SELECT * FROM DatosRegistrados WHERE CodigoCliente = ".$_SESSION['numeroCliente']."";
	$result = mysql_query($sql);
	$row = mysql_fetch_array($result);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?=$titulo_web;?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
<link href="css/oficina_vieja.css" rel="stylesheet" type="text/css" />

<link href="css/imprimir_certificado_consumos.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_informacion_certificadoconsumos.css" rel="stylesheet" type="text/css" />

<script language="javascript" type="text/javascript">
//<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
//-->

//Esta funcion hace que al cargar la pagina se habra la ventana de dialogo de impresion
function imprimir()
{
	window.print();
}//function dimensiona()

//-->
</script>
</head>

<body onload="javascript:imprimir()">
<div id="Layer1" style="position:absolute; left:447px; top:106px; width:243px; height:92px; z-index:1" class="arial11black"><?php echo($row['TitularNombre'])?><br/>
  <?php echo($row['SuministroCalle'])?>&nbsp;&nbsp;<?php echo($row['SuministroNumero'])?>&nbsp;&nbsp;<?php echo($row['SuministroExtension'])?>&nbsp;&nbsp;<?php echo($row['SuministroExtension2'])?><br/>
<?php echo($row['SuministroCP'])?>&nbsp;&nbsp;&nbsp;<?php echo($row['SuministroCiudad'])?></div>



<div id="Layer3" style="position:absolute; align:center; left:26px; top:142px; width:200px; height:20px; z-index:1; font-size:16px; text-decoration:underline" class="arial11black">Certificado de Consumos</div>

<div id="Layer2" style="position:absolute; left:29px; top:155px; width:600px; height:381px; z-index:2" class="arial12Negro"> 
  <?php 


 
 // $rowTexto = //mysql_fetch_array($resultTexto);$rowTexto[1]); 
$Monate = array('','','','','','','','','','','','');
$i=0;
$m=610;
while($i!=12){
	$resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto='.$m);
	$rowTexto= mysql_fetch_array($resultTexto);
	$Monate[$i]=$rowTexto[1];
	$i++;
	$m++;

	
}


//$Monate = array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'); 
  $mes = $Monate[date("m")-1];
 

  echo("<br /><br /><br />");
  echo($poblacion_empresa.", ");
  echo(date("d "));
  echo("de ");
  echo($mes);
  echo(" del ");
  echo(date("Y"));
   echo(".");
  ?>
  <p>
    <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto =310'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
  </p>
  <p>
    <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto =575'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?></p>
	
	



  <table width="650" border="0" cellspacing="0" cellpadding="0">

    <tr>
  <td height="57" colspan="3">
   
    <table width="700" border="1" cellspacing="0" cellpadding="0">
      <tr>
        <td width="25%" bgcolor="#CCCCCC"><div align="center"><strong>CUPS</strong>:</div></td>
        <td width="17%" bgcolor="#CCCCCC"><div align="center"><strong>N&uacute;mero Cliente:</strong></div></td>
        <td width="13%" bgcolor="#CCCCCC"><div align="center"><strong>DNI:</strong></div></td>
        <td width="45%" bgcolor="#CCCCCC"><div align="center"><strong>Nombre Titular</strong>:</div></td>
      </tr>
      <tr>
        <td><div align="center"><?php echo(strtoupper($row['CUPS']));?></div></td>
        <td><div align="center"><?php echo(strtoupper($row['CodigoCliente']));?></div></td>
        <td><div align="center"><?php echo(strtoupper($row['DNI']));?></div></td>
        <td><div align="center"><?php echo(strtoupper($row['TitularNombre']));?></div></td>
      </tr>
    </table>
  <br />

    <table width="700" border="1" cellspacing="0" cellpadding="0">
      <tr>
        <td width="33%" bgcolor="#CCCCCC"><div align="center"><strong>Direcci&oacute;n</strong></div></td>
        <td width="5%" bgcolor="#CCCCCC"><div align="center"><strong>N&uacute;m:</strong></div></td>
        <td width="34%" bgcolor="#CCCCCC"><div align="center"><strong>Extensi&oacute;n:</strong></div></td>
        <td width="8%" bgcolor="#CCCCCC"><div align="center"><strong>C.P.:</strong></div></td>
        <td width="20%" bgcolor="#CCCCCC"><div align="center"><strong>Poblaci&oacute;n</strong></div></td>
      </tr>
      <tr>
        <td><div align="center"><?php echo(strtoupper($row['SuministroCalle']));?></div></td>
        <td><div align="center"><?php echo(strtoupper($row['SuministroNumero']));?></div></td>
        <td><div align="center"><?php echo(strtoupper($row['SuministroExtension']));?></div></td>
        <td><div align="center"><?php echo(strtoupper($row['SuministroCP']));?></div></td>
        <td><div align="center"><?php echo(strtoupper($row['SuministroCiudad']));?></div></td>
      </tr>
    </table>
    <br />
<br />



  <table height="20" width="700" border="0" cellspacing="0" cellpadding="0">
	<tr>
	  <td width="701" height="20" colspan="2" valign="top"><table height="20" width="700" border="1" bgcolor="#CCCCCC">
	    <tr>
	      <td width="690" height="20" valign="top" class="arial12black2">&nbsp;&nbsp;&nbsp;
	        <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto =582'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
	        <?php echo($_GET['FechaInicial']); $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto =239'); $rowTexto = mysql_fetch_array($resultTexto); echo(' '.$rowTexto['Texto'].' ');    echo($_GET['FechaFinal']);?></td>
          </tr>
      </table></td>
      
  </tr>
</table>

<br />



    
            
<?php   
				$fechaIni=substr($_GET['FechaInicial'],6,4).'-'.substr($_GET['FechaInicial'],3,2).'-'.substr($_GET['FechaInicial'],0,2);
				$fechaFin=substr($_GET['FechaFinal'],6,4).'-'.substr($_GET['FechaFinal'],3,2).'-'.substr($_GET['FechaFinal'],0,2);
				
		   		$consulta_consumo = "SELECT * FROM CertificadoConsumo WHERE NumeroCliente = ".$_SESSION['numeroCliente']." AND FechaFactura >= '".$fechaIni."' AND FechaFactura <= '".$fechaFin."' ORDER BY Anio DESC,Bimestre DESC,FechaFactura DESC";
					         
		            $resultado_consumos = mysql_query($consulta_consumo);

//Estas variables iran acumulando los valores de los dos tipos de consumos. Las utilizaremos para mostrar los totales
					$total_activa=0;
					$total_reactiva=0;					
																		
					if($registro_consumo = mysql_fetch_array($resultado_consumos))
					{
?>

              <table width="790" border="1" cellspacing="0" cellpadding="0" class="estructura_consumo2">
  <tr>
    <td width="98" class="per colfondo times12Azul4">Periodo</td>
    <td width="98" class="con colfondo times12Azul4">F.Factura</td>
    <td width="98" class="con colfondo times12Azul4">Tipo consumo</td>
    <td width="68" class="px colfondo times12Azul4">P1 Punta</td>
    <td width="68" class="px colfondo times12Azul4">P2 LLano</td>
    <td width="68" class="px colfondo times12Azul4">P3 Valle</td>
    <td width="68" class="px colfondo times12Azul4">P4</td>
    <td width="68" class="px colfondo times12Azul4">P5</td>
    <td width="68" class="px colfondo times12Azul4">P6</td>
    <td width="66" class="px colfondo times12Azul4">SubTotal</td>
  </tr>
  
</table>   

 <div class="limpiar"></div>


                            
                       
                            
<?php 						do
							{
							
								$total_activa=$total_activa+$registro_consumo["TotalActiva"];
								$total_reactiva=$total_reactiva+$registro_consumo["TotalReactiva"];;					
 ?>
   
<table width="750" border="1" cellspacing="0" cellpadding="0" class="estructura_consumo2">
                <tr>
    <td rowspan="2" class="per2 times12Azul4"><span class="posicion_datos_periodo_certificado_consumo" >
      <?=$registro_consumo["PeriodoTexto"]?>
    </span></td>
    <td width="103" rowspan="2" class="con times12Azul4"><span class="posicion_datos_periodo_certificado_consumo">
      <?=$registro_consumo["FechaFactura"]?>
    </span></td>
    <td width="103" class="con times12Azul4"><span class="alto posicion_datos_tipo_certificado_consumo">Activa</span></td>
    <td width="61" class="px times12Azul4"><div align="right"><span class="alto posicion_datos_periodo_certificado_consumo">
      <? if (number_format($registro_consumo["P1Activa"],0,",",".")==0){echo("-");}else{echo(number_format($registro_consumo["P1Activa"],0,",","."));}?>
    </span></div></td>
    <td width="61" class="px times12Azul4"><div align="right"><span class="alto posicion_datos_periodo_certificado_consumo">
      <? if (number_format($registro_consumo["P2Activa"],0,",",".")==0){echo("-");}else{echo(number_format($registro_consumo["P2Activa"],0,",","."));}?>
    </span></div></td>
    <td width="61" class="px times12Azul4"><div align="right"><span class="alto posicion_datos_periodo_certificado_consumo">
      <? if (number_format($registro_consumo["P3Activa"],0,",",".")==0){echo("-");}else{echo(number_format($registro_consumo["P3Activa"],0,",","."));}?>
    </span></div></td>
    <td width="61" class="px times12Azul4"><div align="right"><span class="alto posicion_datos_periodo_certificado_consumo">
      <? if (number_format($registro_consumo["P4Activa"],0,",",".")==0){echo("-");}else{echo(number_format($registro_consumo["P4Activa"],0,",","."));}?>
    </span></div></td>
    <td width="61" class="px times12Azul4"><div align="right"><span class="alto posicion_datos_periodo_certificado_consumo">
      <? if (number_format($registro_consumo["P5Activa"],0,",",".")==0){echo("-");}else{echo(number_format($registro_consumo["P5Activa"],0,",","."));}?>
    </span></div></td>
    <td width="61" class="px times12Azul4"><div align="right"><span class="alto posicion_datos_periodo_certificado_consumo">
      <? if (number_format($registro_consumo["P6Activa"],0,",",".")==0){echo("-");}else{echo(number_format($registro_consumo["P6Activa"],0,",","."));}?>
    </span></div></td>
      <td width="56" class="px times12Azul4 colfondo"><div align="right"><span class="alto posicion_datos_periodo_certificado_consumo">
        <? 
	  
	  $subtotalactiva=$registro_consumo["P1Activa"]+$registro_consumo["P2Activa"]+$registro_consumo["P3Activa"]+$registro_consumo["P4Activa"]+$registro_consumo["P5Activa"]+$registro_consumo["P6Activa"];
 if (number_format($subtotalactiva,0,",",".")==0){echo("-");}else{echo(number_format($subtotalactiva,0,",","."));}?>
      </span></div></td>
  </tr>
  <tr>
    <td class="con times12Azul4"><span class="alto posicion_datos_tipo_certificado_consumo">Reactiva</span></td>
    <td class="px times12Azul4"><div align="right"><span class="alto posicion_datos_periodo_certificado_consumo">
      <? if (number_format($registro_consumo["P1Reactiva"],0,",",".")==0){echo("-");}else{echo(number_format($registro_consumo["P1Reactiva"],0,",","."));}?>
    </span></div></td>
    <td class="px times12Azul4"><div align="right"><span class="alto posicion_datos_periodo_certificado_consumo">
      <? if (number_format($registro_consumo["P2Reactiva"],0,",",".")==0){echo("-");}else{echo(number_format($registro_consumo["P2Reactiva"],0,",","."));}?>
    </span></div></td>
    <td class="px times12Azul4"><div align="right"><span class="alto posicion_datos_periodo_certificado_consumo">
      <? if (number_format($registro_consumo["P3Reactiva"],0,",",".")==0){echo("-");}else{echo(number_format($registro_consumo["P3Reactiva"],0,",","."));}?>
    </span></div></td>
    <td class="px times12Azul4"><div align="right"><span class="alto posicion_datos_periodo_certificado_consumo">
      <? if (number_format($registro_consumo["P4Reactiva"],0,",",".")==0){echo("-");}else{echo(number_format($registro_consumo["P4Reactiva"],0,",","."));}?>
    </span></div></td>
    <td class="px times12Azul4"><div align="right"><span class="alto posicion_datos_periodo_certificado_consumo">
      <? if (number_format($registro_consumo["P5Reactiva"],0,",",".")==0){echo("-");}else{echo(number_format($registro_consumo["P5Reactiva"],0,",","."));}?>
    </span></div></td>
    <td class="px times12Azul4"><div align="right"><span class="alto posicion_datos_periodo_certificado_consumo">
      <? if (number_format($registro_consumo["P6Reactiva"],0,",",".")==0){echo("-");}else{echo(number_format($registro_consumo["P6Reactiva"],0,",","."));}?>
    </span></div></td>
    
     <td class="px times12Azul4 colfondo"><div align="right"><span class="alto posicion_datos_periodo_certificado_consumo">
       <? 
	  
	  $subtotalreactiva=$registro_consumo["P1Reactiva"]+$registro_consumo["P2Reactiva"]+$registro_consumo["P3Reactiva"]+$registro_consumo["P4Reactiva"]+$registro_consumo["P5Reactiva"]+$registro_consumo["P6Reactiva"];
 if (number_format($subtotalreactiva,0,",",".")==0){echo("-");}else{echo(number_format($subtotalreactiva,0,",","."));}?>
     </span></div></td>
    
    
  </tr>
  </table>       
                
                
                
                
                  <?php 
				  
				$totalactivaP1=  $totalactivaP1 + $registro_consumo["P1Activa"];
				$totalactivaP2=  $totalactivaP2 + $registro_consumo["P2Activa"];
				$totalactivaP3=  $totalactivaP3 + $registro_consumo["P3Activa"];
				$totalactivaP4=  $totalactivaP4 + $registro_consumo["P4Activa"];
				$totalactivaP5=  $totalactivaP5 + $registro_consumo["P5Activa"];
				$totalactivaP6=  $totalactivaP6 + $registro_consumo["P6Activa"];
				
				$totalreactivaP1=  $totalreactivaP1 + $registro_consumo["P1Reactiva"];
				$totalreactivaP2=  $totalreactivaP2 + $registro_consumo["P2Reactiva"];
				$totalreactivaP3=  $totalreactivaP3 + $registro_consumo["P3Reactiva"];
				$totalreactivaP4=  $totalreactivaP4 + $registro_consumo["P4Reactiva"];
				$totalreactivaP5=  $totalreactivaP5 + $registro_consumo["P5Reactiva"];
				$totalreactivaP6=  $totalreactivaP6 + $registro_consumo["P6Reactiva"];
				
				$totalsubtotalactiva=  $totalsubtotalactiva + $subtotalactiva;
				$totalsubtotalreactiva=  $totalsubtotalreactiva + $subtotalreactiva;
			
				  
						}while($registro_consumo = mysql_fetch_array($resultado_consumos));
?>
              
                  <!--Se añade la fila de los totales-->
                  <br />


                  

                  <table width="750" border="1" cellspacing="0" cellpadding="0" class="estructura_consumo2">
  <tr>
    <td rowspan="2" class="per2 colfondo times12Azul4">Totales</td>
    <td rowspan="2" class="con colfondo times12Azul4">&nbsp;</td>
    <td class="con colfondo times12Azul4">Activa</td>
    <td class="px colfondo times12Azul4"><div align="right">
      <? if (number_format($totalactivaP1,0,",",".")==0){echo("-");}else{echo(number_format($totalactivaP1,0,",","."));}?>&nbsp;&nbsp;
    </div></td>
    <td class="px colfondo times12Azul4"><div align="right">
      <? if (number_format($totalactivaP2,0,",",".")==0){echo("-");}else{echo(number_format($totalactivaP2,0,",","."));}?>&nbsp;&nbsp;
    </div></td>
    <td class="px colfondo times12Azul4"><div align="right">
      <? if (number_format($totalactivaP3,0,",",".")==0){echo("-");}else{echo(number_format($totalactivaP3,0,",","."));}?>&nbsp;&nbsp;&nbsp;
    </div></td>
    <td class="px colfondo times12Azul4"><div align="right">
      <? if (number_format($totalactivaP4,0,",",".")==0){echo("-");}else{echo(number_format($totalactivaP4,0,",","."));}?>&nbsp;&nbsp;&nbsp;
    </div></td>
    <td class="px colfondo times12Azul4"><div align="right">
      <? if (number_format($totalactivaP5,0,",",".")==0){echo("-");}else{echo(number_format($totalactivaP5,0,",","."));}?>&nbsp;&nbsp;&nbsp;
    </div></td>
    <td class="px colfondo times12Azul4"><div align="right">
      <? if (number_format($totalactivaP6,0,",",".")==0){echo("-");}else{echo(number_format($totalactivaP6,0,",","."));}?>&nbsp;&nbsp;&nbsp;
    </div></td>
    <td class="px colfondo times12Azul4"><div align="right">
      <? if (number_format($totalsubtotalactiva,0,",",".")==0){echo("-");}else{echo(number_format($totalsubtotalactiva,0,",","."));}?>&nbsp;&nbsp;&nbsp;
    </div></td>
  </tr>
  
   <tr>
    <td class="con colfondo times12Azul4">Reactiva</td>
    <td class="px colfondo times12Azul4"><div align="right">
      <? if (number_format($totalreactivaP1,0,",",".")==0){echo("-");}else{echo(number_format($totalreactivaP1,0,",","."));}?>&nbsp;&nbsp;
    </div></td>
    <td class="px colfondo times12Azul4"><div align="right">
      <? if (number_format($totalreactivaP2,0,",",".")==0){echo("-");}else{echo(number_format($totalreactivaP2,0,",","."));}?>&nbsp;&nbsp;
    </div></td>
    <td class="px colfondo times12Azul4"><div align="right">
      <? if (number_format($totalreactivaP3,0,",",".")==0){echo("-");}else{echo(number_format($totalreactivaP3,0,",","."));}?>&nbsp;&nbsp;&nbsp;
    </div></td>
    <td class="px colfondo times12Azul4"><div align="right">
      <? if (number_format($totalreactivaP4,0,",",".")==0){echo("-");}else{echo(number_format($totalreactivaP4,0,",","."));}?>&nbsp;&nbsp;&nbsp;
    </div></td>
    <td class="px colfondo times12Azul4"><div align="right">
      <? if (number_format($totalreactivaP5,0,",",".")==0){echo("-");}else{echo(number_format($totalreactivaP5,0,",","."));}?>&nbsp;&nbsp;&nbsp;
    </div></td>
    <td class="px colfondo times12Azul4"><div align="right">
      <? if (number_format($totalreactivaP6,0,",",".")==0){echo("-");}else{echo(number_format($totalreactivaP6,0,",","."));}?>&nbsp;&nbsp;&nbsp;
    </div></td>
    <td class="px colfondo times12Azul4"><div align="right">
      <? if (number_format($totalsubtotalreactiva,0,",",".")==0){echo("-");}else{echo(number_format($totalsubtotalreactiva,0,",","."));}?>&nbsp;&nbsp;&nbsp;
    </div></td>
  </tr>
  
</table>
                  

        <div class="limpiar"></div>
                     
                    
<?php					
					}//if($registro_consumo = mysql_fetch_array($resultado_consumos))
?>
					<div class="limpiar"></div>

	  </td>
	</tr>
  </table>
 
<p>
    <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto =313'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>.<br />
<img src="../img/oficina/firma.jpg" width="210" height="120" /></span><br /><br />
    <?=$nombre_empresa?></div>
<img src="../img/oficina/logo_datos.gif" alt="<?=$nombre_empresa_completo?>" title="<?=$nombre_empresa_completo?>" width="427" height="95" />
  </p>
</div>
</body>
</html>
<?php
}//else ($_SESSION['usuario'])
?>