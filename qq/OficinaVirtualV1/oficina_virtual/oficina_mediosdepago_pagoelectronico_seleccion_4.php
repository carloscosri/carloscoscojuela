<?php
/*
NOTAS:

1- ESTA SECCION TOMA LOS DATOS DE LA TABLA FacturasPDF

- ESCRIBE EN LA TABLA SolicitudFacturas

*/

/* NOTA:

 COMO NO SE CIERRA LA CONEXION TRAS CADA CONEXION CON LA BASE DE DATOS, LAS CONSULTAS DEJAN DE FUNCIONAR SI SE ESTABLECE COMO UNICA CONEXION
 LA QUE UTILIZA EL RESTO DE LA WEB, POR LO TANTO HASTA QUE SE HAGA LA MIGRACION COMPLETA DE LOS TEXTOS, COEXISTIRAN LAS DOS CONEXIONES, YA QUE
 SE HA AÑADIDO UNA LLAMADA A LA CONEXION BUENA EN ESTE ARCHIVO.
*/

include("_conexion.php");

//Se incluye el archivo que contiene las verificaciones de datos
include("../includes/verificaciones.php");

//Se incluye el archivo con los textos especifico de esta seccion en el idioma oportuno
//include($_SESSION["directorio_raiz"]."idiomas/".$_SESSION["idioma"]."/oficina_informacion_facturas.php");
include("../idiomas/".$_SESSION["idioma"]."/oficina_datos_titular.php");
include("../idiomas/".$_SESSION["idioma"]."/oficina_informacion_facturas.php");

include("../idiomas/".$_SESSION["idioma"]."/oficina_informacion_certificadofacturas.php");

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<title><?=$titulo_web;?></title>

<!--Se añaden las hojas de estilos genericas que utiliza esta seccion-->
<link href="../css/estilos.css" rel="stylesheet" type="text/css" />
<link href="../css/secciones.css" rel="stylesheet" type="text/css" />
<link href="css/oficina.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_vieja.css" rel="stylesheet" type="text/css" />

<link href="css/oficina_informacion_contratos_y_facturas.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_informacion_facturas.css" rel="stylesheet" type="text/css" />

<link href="css/oficina_informacion_contratos_y_facturas.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_informacion_certificadoconsumos.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_informacion_certificadofacturas.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" media="all" href="scripts/calendar/skins/aqua/theme.css" title="Aqua" />


<!-- librería principal del calendario -->
<script type="text/javascript" src="scripts/calendar/calendar.js"></script>

<!-- librería para cargar el lenguaje deseado -->
<script type="text/javascript" src="scripts/calendar/lang/calendar-es.js"></script>

<!-- librería que declara la función Calendar.setup, que ayuda a generar un calendario en unas pocas líneas de código -->
<script type="text/javascript" src="scripts/calendar/calendar-setup.js"></script>




<!--Ahora se añaden las reglas para Internet Explorer. Asi corregiremos las diferencias de visualizacion entre navegadores
Es importante añadirlo despues de las reglas principales, para que se sobreescriban las correcciones pertinentes. En este caso las diferencias
solo son para internet explorer 7-->

<!--[if IE 7]>
	<link href="../css/estilos_ie7.css" rel="stylesheet" type="text/css" />
	<link href="../css/secciones_ie7.css" rel="stylesheet" type="text/css" />    
<![endif]-->

<!--[if IE]>
	<link href="css/tablas_informacionIE.css" rel="stylesheet" type="text/css" />
<![endif]-->

<!--[if IE 7]>
	<link href="css/tablas_informacionIE7.css" rel="stylesheet" type="text/css" />
<![endif]-->

<script language="javascript" type="text/javascript">
function avisa()
{
	alert("OPCION NO DISPONIBLE");
}//function avisa()
//-->
</script>

<?php

//Asignamos los valores por defecto a la fecha de inicio y la fecha de fin, estos cambiaran si el usuario pulsa el boton del formulario habiendo escrito algo
$fecha_inicio="dd-mm-aaaa";
$fecha_fin="dd-mm-aaaa";

if(isset($_POST["Submit"]))
{	
	$fecha_inicio=$_POST["DesdeFecha"];
	$fecha_fin=$_POST["HastaFecha"];
	$observaciones=$_POST["observaciones"];	
	
	$error=comparar_fechas($fecha_inicio,$fecha_fin);		
}//if(isset($_POST["Submit"]))

?>






<?php
if (isset($_POST["submit"]))
{
//Recogemos los valores de las variables
	//Recogemos los valores de las variables
	
	//$titular_nombre=str_replace("'","&quot;",$_POST["TitularNombre"]);
	

	$factura_nombre=$_POST["FacturaNombre"];							
	$factura_calle=$_POST["FacturaCalle"];								
	$factura_numero2=$_POST["FacturaNumero"];
	if ($factura_numero2=='0') {$factura_numero='';} else {$factura_numero=$factura_numero2;};
	
	$factura_extension=$_POST["FacturaExtension"];
	$factura_aclarador=$_POST["FacturaAclarador"];
	$factura_cp=$_POST["FacturaCP"];		
	$factura_poblacion=$_POST["FacturaPoblacion"];		
	$factura_provincia=$_POST["FacturaProvincia"];
	
	$factura_telefono=$_POST["FacturaTelefono"];		
	$factura_movil=$_POST["FacturaMovil"];			
	$factura_fax=$_POST["FacturaFax"];	
	$factura_email=$_POST["FacturaEmail"];	
	$factura_dni=$_POST["FacturaDNI"];
	
}
?>


</head>
	
<body>




<script> 

function addCommas(nStr)
{
	nStr += '';
	x = nStr.split('.');
	x1 = x[0];
	x2 = x.length > 1 ? ',' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + '.' + '$2');
	}
	return x1 + x2;
}



function sumaCheck(num){ var suma=0; 
for(i=1;i<num+1;i++){ 
if(document.getElementById("check"+i).checked) 
suma = suma + parseFloat(document.getElementById("check"+i).value)
suma2 = suma.toFixed(2)
suma3= addCommas(suma2)
} 
document.getElementById("seleccionada").value='S';
document.getElementById("importeseleccionado").value=suma3
return (suma3); 
} 
</script> 





<!--*************************************************Web realizada por ****************************************-->
	<div id="central_oficina">
    
<?php
//Se comprueba que el usuario se haya identificado, si no es asi se da un aviso y redirigimos a la seccion de identificacion
if ($_SESSION['usuario']=="")
{	
	MsgBox($error_identificacion);
	redirigir("oficina_index.php?id=".$_SESSION["idioma"]);
	
}//if ($_SESSION['usuario'])

//Si el usuario se ha identificado la seccion se ejecutara con normalidad
else
{
//Añadimos los fragmentos comunes, la cabecera, el menu, y el submenu
	include("oficina_fragmentos_comunes.php");
?>    
	  <div class="contenido_seccion_oficina_mediospago">                                         
       	
  <form action="oficina_mediosdepago_pagoelectronico_seleccion.php?id=<?=$_SESSION["idioma"]?>"  method="post" name="form_factura_seleccion" id="form_factura_seleccion">

        <table width="600" border="0" cellspacing="0" cellpadding="0">
		  <?php if (isset($_SESSION['numeroCliente'])){
					 		$result = mysql_query("SELECT * FROM DatosRegistrados WHERE CodigoCliente = ".$_SESSION['numeroCliente']." ");
							$row = mysql_fetch_array($result);
					 }else{
					 		$result = mysql_query("SELECT * FROM DatosRegistrados WHERE CodigoCliente = -1");
							//Para que no tome ningun cliente pero no de ningun error en la pagina
							$row = mysql_fetch_array($result);
					 }
					 
					 
					 
				if (!isset($_POST["submit"]))	
							{								
//Si el formulario no se ha aceptado, se mostraran los valores almacenados en la base de datos
							    $factura_nombre=$row["FacturaNombre"];
								$factura_calle=$row["FacturaCalle"];
								$factura_numero2=$row["FacturaNumero"];
								if ($factura_numero2=='0') {$factura_numero='';} else {$factura_numero=$factura_numero2;};
								$factura_extension=$row["FacturaExtension"];
								$factura_aclarador=$row["FacturaAclarador"];
								$factura_cp=$row["FacturaCP"];
								$factura_poblacion=$row["FacturaPoblacion"];
								$factura_provincia=$row["FacturaProvincia"];
								$factura_telefono=$row["FacturaTelefono1"];
								$factura_movil=$row["FacturaTelefono2"];
								$factura_fax=$row["FacturaFax"];
								$factura_email=$row["FacturaEmail"];
								$factura_dni=$row["FacturaDNI"];
								
								
															
							}
					 ?>
            <tr> 
              <td valign="top"><div align="center">
             
                
                <table width="640" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td height="47"><div align="center"><span class="arialblack18Titulo">Medio Electr&oacute;nico Pago Facturas Pendientes</span></div></td>
                  </tr>
                  <tr>
                    <td height="20"><div align="center"></div></td>
                  </tr>
                 
                  <tr>
                    <td><div align="center">
                      <table width="633" border="0" cellpadding="0" cellspacing="0" >
                        <tr>
                          <td width="633" class="arial14"><fieldset >
                            <legend class="arialblack14">
                              <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 130'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                              </legend>
                            <table width="500" align="center">
                              <tr>
                                <td width="148" height="22" valign="top" class="arial14"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 196'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                                  &nbsp;</td>
                                <td width="57" valign="top" class="arial14"><input name="CodigoCliente" type="text" class="textfieldLecturaNum" id="CodigoCliente" value="<?php echo($row['CodigoCliente']);?>" size="5" maxlength="5"  readonly="readonly" /></td>
                                <td width="20" valign="top" class="arial14">&nbsp;</td>
                                <td width="47" valign="top" class="arial14">CUPS</td>
                                <td width="204" valign="top" class="arial14">&nbsp;&nbsp;&nbsp;
                                  <input name="CUPSFijo" value="<?php echo($row['CUPS']);?>"  type="text" class="textfieldLectura" id="CUPSFijo" size="25" maxlength="25" /></td>
                                </tr>
                              </table>
                            </fieldset></td>
                        </tr>
                      </table>
                    </div>
                    <div align="center"></div></td>
                  </tr>
                  <tr>
                    <td height="25">&nbsp;</td>
                  </tr>
                  <tr>
                    <td><div align="center"><span class="arialblack16Titulo">Relaci&oacute;n de Facturas Seleccionadas para el Pago</span> <br />
                    </div></td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td><div align="center">
                      
                        <?php 
		   
		
			$importetotal=0;
			$i=1;
			$seleccionada='N';
			$consulta_medio_pago_seleccion="SELECT * FROM RecibosPendientes WHERE  NumeroCliente=".$_SESSION['numeroCliente']." AND Seleccionada='S' ORDER BY `FechaFactura` DESC";
			
			$resultados_medio_pago_seleccion = mysql_query($consulta_medio_pago_seleccion);
			$num_medio_pago_seleccion=mysql_num_rows($resultados_medio_pago_seleccion);
			$n2=$num_medio_pago_seleccion;
			
			//$registro_medio_pago = mysql_fetch_array($resultados_medio_pago);
			//echo($registro_medio_pago);
			//echo("<br/>");


//Si hay facturas se mostrara un resumen de los datos de cada una de ellas			
			if($num_medio_pago_seleccion != 0)
			{
				
				while($registro_medio_pago_seleccion = mysql_fetch_array($resultados_medio_pago_seleccion))
				{
					
			//$consulta_medio_pago="SELECT * FROM FacturasPDF WHERE (NumeroCliente=".$_SESSION['numeroCliente']." AND FechaFactura = '".$registro_medio_pago["FechaFactura"]."')";
			//$resultados_medio_pago = mysql_query($consulta_medio_pago);			
			//$num_medio_pago=mysql_num_rows($resultados_medio_pago);
			//$registro_medio_pago = mysql_fetch_array($resultados_medio_pago);


		
?>
                      
                    </div>
                      <div >
                        <div class="columnas_fila4_contrato">
                          <div class="color_fondo_cabecera_contrato times12Azul4">Nº Factura</div>
                          <div class="color_fondo_datos_contrato alto_factura_periodo arial12">
                            <?=$registro_medio_pago_seleccion["NumeroFactura"]?>
                          </div>
                        </div>
                        <!--<div class="columnas_fila3_contrato">-->
                        <div class="columnas_fila4_contrato">
                          <div class="color_fondo_cabecera_contrato times12Azul4">Fecha </div>
                          <div class="color_fondo_datos_contrato alto_factura_periodo arial12">
                            <?=fecha_normal($registro_medio_pago_seleccion["FechaFactura"])?>
                          </div>
                        </div>
                        <!--<div class="columnas_fila3_contrato">-->
                        <div class="columnas_fila4_contrato">
                          <div class="color_fondo_cabecera_contrato times12Azul4">N&ordm; Recibo </div>
                          <div class="color_fondo_datos_contrato alto_factura_periodo arial12">
                            <?=$registro_medio_pago_seleccion["NumeroRecibo"]?>
                          </div>
                        </div>
                        <!--<div class="columnas_fila3_contrato">-->
                        <div class="columnas_fila4_contrato">
                          <div class="color_fondo_cabecera_contrato times12Azul4">Origen Fact </div>
                          <div class="color_fondo_datos_contrato alto_factura_periodo arial12">
                            <?
						
						switch ($registro_medio_pago_seleccion["TipoFactura"])
						{
						case 'E':						{$tipofactura="Energía";}						break;
						case 'R':						{$tipofactura="Reconexión";}					break;
						case 'D':						{$tipofactura="Derechos";}						break;
						case 'V':						{$tipofactura="Varios";}						break;
						default :						{$tipofactura="------";}      					break;
						}
						echo($tipofactura);
						
						
						?>
                          </div>
                        </div>
                        <!--<div class="columnas_fila3_contrato columna_factura_periodo">-->
                        <div class="columnas_fila4_contrato">
                          <div class="color_fondo_cabecera_contrato times12Azul4">Tipo Recibo </div>
                          <div class="color_fondo_datos_contrato alto_factura_periodo arial12">
                            <?
						
						switch ($registro_medio_pago_seleccion["TipoRecibo"])
						{
						case 'F':				{$tiporecibo="Facturado";}					break;
						case 'I':				{$tiporecibo="Impagado";}					break;
						default :				{$tiporecibo="------";}     				break;
						}
						echo($tiporecibo);
									
						?>
                          </div>
                        </div>
                        <!--<div class="columnas_fila3_contrato">-->
                        <div class="columnas_fila4_contrato">
                          <div class="color_fondo_cabecera_contrato times12Azul4">Est Impagado </div>
                          <div class="color_fondo_datos_contrato alto_factura_periodo arial12">
                            <?
						
						switch ($registro_medio_pago_seleccion["EstadoImpagado"])
						{
						case '1':				{$tipoestadoimpagado="Pendiente";}					break;
						case 'C':				{$tipoestadoimpagado="Carta de Corte";}				break;
						case 'O':				{$tipoestadoimpagado="Cortado";}					break;
						default :				{$tipoestadoimpagado="------";}     				break;
						}
						echo($tipoestadoimpagado);
									
						?>
                          </div>
                        </div>
                        <!--<div class="columnas_fila3_contrato">-->
                        <div class="columnas_fila5_contrato">
                          <div class="color_fondo_cabecera_contrato times12Azul4">Importe</div>
                          <div class="color_fondo_datos_contrato alto_factura_periodo arial12">
                            <?=$registro_medio_pago_seleccion["ImportePendiente"]?>
                            <? echo(" ");?></div>
                        </div>
                        <!--<div class="columnas_fila3_contrato">-->
                      </div>
                      <!--<div class="columnas_fila1_larga_contrato">-->
                      <div class="limpiar"></div>
                      <div class="columnas_fila6_contrato">
                        <div class="color_fondo_cabecera_contrato times12Azul4">Nombre Titular </div>
                        <div class="color_fondo_datos_contrato arial12"> <?php echo($row['TitularNombre']);?> </div>
                      </div>
                      <!--<div class="columnas_consumos_informacion_factura">-->
                      <div class="columnas_fila6_contrato">
                        <div class="color_fondo_cabecera_contrato times12Azul4">Direcci&oacute;n Punto Suministro</div>
                        <div class="color_fondo_datos_contrato arial12"> <?php echo($row['SuministroCalle']);?> </div>
                      </div>
                      <!--<div class="columnas_consumos_informacion_factura">-->
                      <div class="columnas_fila6_contrato">
                        <div class="color_fondo_cabecera_contrato times12Azul4">Poblaci&oacute;n Punto Suministro</div>
                        <div class="color_fondo_datos_contrato arial12"> <?php echo($row['SuministroCiudad']);?> </div>
                      </div>
                      <!--<div class="columnas_consumos_informacion_factura">-->
                      <div class="limpiar"> </div>
                      <!--<div class="posicion_boton_imprimir_certificado_consumo">-->
                      <!--<div class="posicion_boton_imprimir_certificado_consumo">-->
                      <div class="limpiar"></div>
                      <input type="text" style="display:none;" name="seleccionada" id="seleccionada" value="<?=$seleccionada?>"/>
                      <br />
                      <?php 
			$importetotal=$importetotal+$registro_medio_pago_seleccion["ImportePendiente"];
			$i=$i+1;

			
			
		
	
			
			
			
			
			
			
				}//while($row = mysql_fetch_array($result))				  
		   }//if($num_facturas != 0)
		   
//Si no hay facturas en la base de datos mostraremos un mensaje al usuario
		   else
		   {
?>
                     
                      <div class="error_no_registros">
                        <?=$error_no_facturas?>
                      </div>
                    <?php		   
		   }//else($num_facturas != 0)
$i=$i-1;

?>
                    <br />
                    <br /></td>
                  </tr>
                  <tr>
                    <td>
                      <fieldset>
                        <table width="650" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td height="31"><div align="center">El Importe a pagar seg&uacute;n las Facturas seleccionadas es de: </div>
                              <div align="right"></div></td>
                          </tr>
                          <tr>
                            <td><div align="center">
                              <input size="10" maxlength="10" name="importe" type="text" class="textfieldLecturaImporteSeleccionado" id="importe" value="<?php echo(number_format($importetotal,2,',','.'));?>"/>
                            <? echo(" &euro;");?></div></td>
                          </tr> 
                        </table>
                        
                      </fieldset>
                   </td>
                  </tr>
                  
<!--CIERRE DEL APARTADO-->                      

                  <tr>
                    <td>
                      
                      </td>
                  </tr>
                  <tr>
                    <td height="27">&nbsp;</td>
                  </tr>
                  <tr>
                    <td height="27">
                    
                    
                    
		<div align="center">
		  <table width="550" border="0" cellpadding="0" cellspacing="0">
		    <tr class="arial12"> 
		      <td colspan="2" class="arial14" width="550">
		        
		        <fieldset   >
		          <legend class="arialblack14">Datos  Personales 
		            
		          
		            
		            </legend>
		          
		          <table width="450" align="center">
		            
		            <tr> 
		              <td width="73" height="5" valign="top"></td>
		              </tr>
		            
		            
		            <tr> 
		              <td height="24" valign="top">
		                
		                <table width="100%" border="0" cellpadding="0" cellspacing="0">
		                  
		                  <tr>
		                    <td width="75" height="20" valign="middle" class="arial14"><?=$oficina_datos_titular17?></td>
		                    </tr>
		                  </table>
		                
		                </td>
		              
		              
		              <td valign="top"><input name="FacturaNombre" value="<?=$factura_nombre?>" type="text" class="textfield" id="FacturaNombre" size="52" maxlength="50" /></td>
		              </tr>
		            <tr> 
		              <td height="24" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
		                
		                <tr>
		                  <td width="75" height="20" valign="middle" class="arial14"><?=$oficina_datos_titular18?></td>
		                  </tr>
		                </table>                        </td>
		              <td valign="top"><input name="FacturaCalle" value="<?=$factura_calle?>" type="text" class="textfield" id="FacturaCalle" size="52" maxlength="50" /></td>
		              </tr>
		            <tr>                                           
		            <td height="24" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
		              <tr>
		                <td width="75" height="20" valign="middle" class="arial14"><?=$oficina_datos_titular19?></td>
		                </tr>                        
		              
		              </table>                        </td>
		            
		            <td valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
		              <tr>                                                  
		                <td width="105" height="20" valign="top" class="arial14"><input name="FacturaNumero" value="<?=$factura_numero?>" type="text" class="textfieldnum" id="FacturaNumero"  size="5" maxlength="4" /></td>
		                
		                <td width="75" bordercolor="#FF0000" height="22" valign="middle" class="arial14">&nbsp;<?=$oficina_datos_titular20?></td>
		                <td width="363" height="22" valign="top"><input name="FacturaExtension" type="text" class="textfield" id="FacturaExtension" value="<?=$factura_extension?>"  size="29" maxlength="50" /></td>                            
		                
		                </td>
		              </tr>
		              </table>                                                                                                               
		            
		            <tr> 
		              <td height="24" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
		                
		                <tr>
		                  <td width="75" height="20" valign="middle" class="arial14"><?=$oficina_datos_titular21?></td>
		                  </tr>
		                </table>                        </td>
		              <td valign="top"><input name="FacturaAclarador" value="<?=$factura_aclarador?>" type="text" class="textfield" id="FacturaAclarador" size="52" maxlength="50" /></td>
		              </tr>
		            <tr>
		                <td height="24" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
		                  
		                  <tr>
		                    <td width="73" height="20" valign="middle" class="arial14"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 289'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?></td>
		                    </tr>
		                  </table>                        </td>
		                <td valign="top"><input name="FacturaCP" type="text"  value="<?=$factura_cp?>"  class="textfieldnum" id="FacturaCP" size="10" maxlength="10" /></td>
		                </tr>
		            
		            
		            
		            
		            <tr>
		              <td height="24" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
		                
		                <tr>
		                  <td width="73" height="20" valign="middle" class="arial14"><?=$oficina_datos_titular22?></td>
		                  </tr>
		                </table>                        </td>
		              <td valign="top"><input name="FacturaPoblacion" type="text"  value="<?=$factura_poblacion?>"  class="textfield" id="FacturaPoblacion" size="52" maxlength="50" /></td>
		              </tr>
		            
		            
		            <tr>
		              <td height="24" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
		                
		                <tr>
		                  <td width="73" height="20" valign="middle" class="arial14"><?=$oficina_datos_titular23?></td>
		                  </tr>
		                </table>                        </td>
		              <td valign="top"><input name="FacturaProvincia" type="text"  value="<?=$factura_provincia?>"  class="textfield" id="FacturaProvincia" size="52" maxlength="50" /></td>
		              </tr>
		            
		            
		            
		            <tr>
		              <td height="2" colspan="2"></td>
		              </tr>
		            <tr> 
		              <td height="24" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
		                
		                <tr>
		                  <td width="75" height="20" valign="middle" class="arial14"><?=$oficina_datos_titular24?> (*)</td>
		                  </tr>
		                </table>                        </td>
		              <td valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
		                
		                <tr>
		                  <td width="363" height="20" valign="top" class="arial14"><input name="FacturaTelefono" type="text" class="textfieldCentrado" value="<?=$factura_telefono?>"  id="FacturaTelefono" size="15" maxlength="15" />
		                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		                    
		                    
		                    <?=$oficina_datos_titular25?>
		                    &nbsp; 
		                    <input name="FacturaMovil" value="<?=$factura_movil?>"  type="text" class="textfieldCentrado" id="FacturaMovil" size="9" maxlength="9" /></td>
		                  </tr>
		                </table>                        </td>
		              </tr>
		            
		            
		            
		            
		            <tr>
		              <td height="25" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
		                
		                <tr>
		                  <td width="75" height="21" valign="middle" class="arial14"><?=$oficina_datos_titular26?></td>
		                  </tr>
		                </table>                        </td>
		              <td valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
		                
		                <tr>
		                  <td width="363" height="21" valign="top" class="arial14"><input name="FacturaFax" value="<?=$factura_fax?>"  type="text" class="textfieldCentrado" id="FacturaFax" size="15" maxlength="15" /></td>
		                  </tr>
		                </table>                        </td>
		              </tr>
		            
		            
		            
		            
		            <tr> 
		              <td height="24" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
		                
		                <tr>
		                  <td width="75" height="20" valign="middle" class="arial14"><?=$oficina_datos_titular27?></td>
		                  </tr>
		                </table>                        </td>
		              <td valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
		                
		                <tr>
		                  <td width="363" height="20" valign="top"><input name="FacturaEmail" value="<?=$factura_email?>"  type="text" class="textfield" id="FacturaEmail" size="52" maxlength="50" /></td>
		                  </tr>
		                </table>                        </td>
		              </tr>
		            
		            
		            <tr>
		              <td height="25" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
		                
		                <tr>
		                  <td width="75" height="21" valign="middle" class="arial14"><?=$oficina_datos_titular28?></td>
		                  </tr>
		                </table>                        </td>
		              <td valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
		                
		                <tr>
		                  <td width="363" height="21" valign="top" class="arial14"><input name="FacturaDNI" value="<?=$factura_dni?>"  type="text" class="textfieldCentrado" id="FacturaDNI" size="15" maxlength="15" /></td>
		                  </tr>
		                </table>                        </td>
		              </tr>
                      
                      
                      <tr>
		              <td height="24" colspan="2" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
		                
		                
		                </table>
		                (*) Campos Obligatorios</td>
		              </tr>
                      
                      
		            
		            <!--CIERRE DEL APARTADO-->                      
		            </table>
		          </fieldset></td>
		      </tr>
		    <!--CIERRE DEL APARTADO-->                      
		    </table>
		  </div>
<!--APARRADO NUEVO-->


                    
                    
                    
                    
                    
                    </td>
                  </tr>
                  <tr>
                    <td height="27">&nbsp;</td>
                  </tr>
                  <tr>
                    <td height="48"><span class="arial12black">&nbsp;&lt;&lt;&nbsp;<a href="oficina_mediosdepago_pagoelectronico.php"> VOLVER a SELECCION de Facturas</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        Pasar al Pago de las Facturas Seleccionadas &nbsp;<img src="../img/oficina/cesta-compra.png" width="32" height="32" align="middle" />&nbsp;  &gt;&gt; &nbsp;&nbsp;
                        <input type="submit" name="submit" id="submit" value="Pagar" />
                    </span></td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                  </tr>
                </table>
              </div>
           </tr>
             
<!--CIERRE DEL APARTADO-->                      

	      </td>
          </tr>
           </table>
        <!--VERSION VIEJA-->        	
       </form> 	
  </div><!--<div class="contenido_seccion_oficina">-->                        
		
        
        <div class="limpiar"></div>
                        	
<!--Por ultimo aparecera el pie de la web-->
<?php 
	include("../includes/pie.php");


		if(isset($_POST["submit"]))
	{
//Por ultimo mostraremos el error correspondiente, en el caso de que haya fallado alguna de las verificaciones, o continuaremos con el proceso		
		
				$insertar_facturas_seleccionadas="INSERT INTO `PagoRemesas` (`NumFactura1`, `NumFactura2`, `NumFactura3`, `NumFactura4`, `NumFactura5`, `ImporteFactura1`, `ImporteFactura2`, `ImporteFactura3`, `ImporteFactura4`, `ImporteFactura5`, `ImporteTotal`, `FechaPago`) VALUES ('1','2','3','4','5','100,20','57,30','654,20','65,30','25,10','".$importetotal."', '".date("Y-m-d H:i:s")."'); ";
				
				//echo($insertar_facturas_seleccionadas);
				$ejecutar_solicitud_facturas=mysql_query($insertar_facturas_seleccionadas);
				//echo($ejecutar_solicitud_facturas);											
				
				redirigir("oficina_mediosdepago_pagoelectronico_pago.php?id=".$_SESSION["idioma"]);

										
			
		
	}//if(isset($_POST["Submit"]))	
		



}//else ($_SESSION['usuario'])
?>        
    </div><!--<div id="central_oficina"-->
</body>
</html>
