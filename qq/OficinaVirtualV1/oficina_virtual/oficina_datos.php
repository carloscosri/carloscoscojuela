<?php
/*
NOTAS:
1- ESTA SECCION TOMA LOS DATOS DE LA TABLA DatosRegistrados

/* NOTA:
 COMO NO SE CIERRA LA CONEXION TRAS CADA CONEXION CON LA BASE DE DATOS, LAS CONSULTAS DEJAN DE FUNCIONAR SI SE ESTABLECE COMO UNICA CONEXION
 LA QUE UTILIZA EL RESTO DE LA WEB, POR LO TANTO HASTA QUE SE HAGA LA MIGRACION COMPLETA DE LOS TEXTOS, COEXISTIRAN LAS DOS CONEXIONES, YA QUE
 SE HA AÑADIDO UNA LLAMADA A LA CONEXION BUENA EN ESTE ARCHIVO.
*/

include("_conexion.php");

//Se incluye el archivo que contiene las verificaciones de datos
include("../includes/verificaciones.php");

//Como tienen textos en comun, para evitar el crecimiento innecesario de la base de datos, se incluye el archivo de textos de las secciones de datos del titular y del alta, ya que la mayoria de los textos son comunes
include("../idiomas/".$_SESSION["idioma"]."/oficina_datos_titular.php");
include("../idiomas/".$_SESSION["idioma"]."/oficina_datos_instalacion.php");
include("../idiomas/".$_SESSION["idioma"]."/oficina_contratacion_alta.php");
include("../idiomas/".$_SESSION["idioma"]."/oficina_modificacion_datostitular.php");
include("../idiomas/".$_SESSION["idioma"]."/oficina_modificacion_datossuministro.php");
include("../idiomas/".$_SESSION["idioma"]."/oficina_datos.php");
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script src="../includes/mootools-1.2.2-core-reducido.js" type="text/javascript"></script>
<script>
var idsPestanas = ["p1", "p2", "p3", "otrapestana", "p4"];
var idsContenedores = ["cont1", "cont2", "cont3", "cont4", "cont5"];

function cambiaPestana(id){
	idsContenedores.each(function(miItem){
		$(miItem).addClass("nover");
	});
	//borro la clase activa de todas las pestañas
	idsPestanas.each(function(item){
		$(item).removeClass("activa");
	});
	//coloco la clase activa a la pestaña que se recibe por parámetro
	$(idsPestanas[id]).addClass("activa");
	//oculto el contenido de todos los contenedores asociados a las pestañas
	

	//en el contenido que está asociado con la pestaña activa, quito la clase nover
	$(idsContenedores[id]).removeClass("nover");
}
</script>
<style>
.nover{display:none;}
#pestanas{margin:auto;}
#pestanas ul {width:600px; text-align:center; margin-left:-65px;}
#pestanas ul li {display:inline; text-align:center; padding:5px 10px 5px 10px; background-color:#DDD; border-radius:7px 7px 7px 7px; 
border-top: #FFF 1px solid;
border-left: #FFF 1px solid;
border-right: #999 1px solid;
border-bottom: #999 1px solid;}
#pestanas ul li:hover {background-color:#CCC; 
border-top: #999 1px solid;
border-left: #999 1px solid;
border-right: #FFF 1px solid;
border-bottom: #FFF 1px solid;}
#pestanas ul li a{color:#330D0C; font-weight:bold; text-decoration:none;}
#pestanas ul li a:hover{color:#000;}

</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?=$titulo_web;?></title>

<!--Se añaden las hojas de estilos genericas que utiliza esta seccion-->
<link href="../css/estilos.css" rel="stylesheet" type="text/css" />
<link href="../css/secciones.css" rel="stylesheet" type="text/css" />
<link href="css/oficina.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_vieja.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_modificacion_titular.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_modificacion_suministro.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_datos.css" rel="stylesheet" type="text/css" />

<!--[if IE 7]>
	<link href="../css/estilos_ie7.css" rel="stylesheet" type="text/css" />
	<link href="../css/secciones_ie7.css" rel="stylesheet" type="text/css" />    
<![endif]-->
</head>
	
<body>
	<div id="central_oficina">
    
<?php
//Se comprueba que el usuario se haya identificado, si no es asi se da un aviso y redirigimos a la seccion de identificacion
if ($_SESSION['usuario']=="")
{	
	MsgBox($error_identificacion);
	redirigir("oficina_index.php?id=".$_SESSION["idioma"]);
	
}//if ($_SESSION['usuario'])

//Si el usuario se ha identificado la seccion se ejecutara con normalidad
else
{
//Añadimos los fragmentos comunes, la cabecera, el menu, y el submenu
		include("oficina_fragmentos_comunes.php");
?>            
       
<div class="contenido_seccion_oficina_datos">
    <div class="contenido_ajustado">
             <div class="tipotablacentrado arialblack18Titulo"><?=$oficina_datos_titulo?></div>

			<div id="pestanas">
        <ul>
        <li id="p1"><a href="javascript: void(0);" onclick="cambiaPestana(0)">Inicio</a></li>
        <li id="otrapestana"><a href="javascript: void(0);" onclick="cambiaPestana(2)">Titular</a></li>
        <li id="p2"><a href="javascript: void(0);" onclick="cambiaPestana(1)">Suministro</a></li>
        <li id="p3"><a href="javascript: void(0);" onclick="cambiaPestana(3)">Pagos y datos Bancarios</a></li>
        <li id="p4"><a href="javascript: void(0);" onclick="cambiaPestana(4)">Consumos</a></li>
        
        </ul>
        </div>

		<div id="cont1">
              <div class="tipotablacentrado">
                  <div align="justify" class="arial12Importanteb padtot avisillo"><?=$textoLOPD?></div>   
              </div>
                
              <div class="tipotablacentrado"></div>
<?php                       
//Esta variable controlara si el cliente tiene o no modificaciones pendientes
					$tiene_modificaciones_pendientes=0;

//Comprobamos si el cliente, tiene modificaciones pendientes
					$consulta_cliente=mysql_query("SELECT * FROM DatosModificados WHERE Usuario  = '".$_SESSION["usuario"]."';");
					mysql_query("SET NAMES 'utf8'");
					$hay_cliente = mysql_fetch_array($consulta_cliente);
															
					if($hay_cliente) $tiene_modificaciones_pendientes=1;
					
//Si el cliente tiene modificaciones pendientes se mostrara un mensaje					
					if($tiene_modificaciones_pendientes==1)
					{  
			?>                  
	                 <!--<span class="arial12Importanteb"><?=$advertencia_cambios_pendientes?></span>-->
                      
			<?php	}//if($tiene_modificaciones_pendientes==1)
					
					$result = mysql_query("SELECT * FROM DatosRegistrados WHERE CodigoCliente = ".$_SESSION['numeroCliente']." ");
					$row = mysql_fetch_array($result);
					$codigocliente=$_SESSION['numeroCliente'];					
					$cups=$row["CUPS"];
					$nombre_comercializadora=$row["ComercializadoraNombre"];
					$nombre_distribuidora=$row["EmpresaDistribuidora"];
					$fecha_alta_comercializadora2=fecha_normal($row["ComercializadoraFechaAlta"]);
					$fecha_baja_comercializadora2=fecha_normal($row["ComercializadoraFechaBaja"]);		
					
					if ($fecha_baja_comercializadora2==0){$fecha_baja_comercializadora='';} else {$fecha_baja_comercializadora=$fecha_baja_comercializadora2;}
					if ($fecha_alta_comercializadora2==0){$fecha_alta_comercializadora='';} else {$fecha_alta_comercializadora=$fecha_alta_comercializadora2;}
					
//En este caso como solamente se muestran los datos, las variables siempre deberan de tomar el valor que hay almacenado en la base de datos
					$titular_nombre=$row["TitularNombre"];
					$titular_calle=$row["TitularCalle"];
					$titular_numero2=$row["TitularNumero"];
					if ($titular_numero2==0){$titular_numero='';} else {$titular_numero=$titular_numero2;}

					$titular_extension=$row["TitularExtension"];
					$titular_aclarador=$row["TitularAclarador"];
					$titular_cp=$row["TitularCP"];
					$titular_poblacion=$row["TitularPoblacion"];
					$titular_provincia=$row["TitularProvincia"];
					$titular_telefono=$row["TitularTelefono1"];
					$titular_movil=$row["TitularTelefono2"];
					$titular_fax=$row["TitularFax"];
					$titular_email=$row["TitularEmail"];
					$titular_dni=$row["TitularDNI"];
							
					$factura_nombre=$row["FacturaNombre"];
					$factura_calle=$row["FacturaCalle"];
					$factura_numero2=$row["FacturaNumero"];
					if ($factura_numero2==0){$factura_numero='';} else {$factura_numero=$factura_numero2;}
					$factura_extension=$row["FacturaExtension"];
					$factura_aclarador=$row["FacturaAclarador"];
					$factura_cp=$row["FacturaCP"];
					$factura_poblacion=$row["FacturaPoblacion"];
					$factura_provincia=$row["FacturaProvincia"];
					$factura_telefono=$row["FacturaTelefono1"];
					$factura_movil=$row["FacturaTelefono2"];
					$factura_fax=$row["FacturaFax"];
					$factura_email=$row["FacturaEmail"];
					$factura_dni=$row["FacturaDNI"];
							
					$envio_nombre=$row["EnvioNombre"];
					$envio_calle=$row["EnvioCalle"];
					$envio_numero2=$row["EnvioNumero"];
					if ($envio_numero2==0){$envio_numero='';} else {$envio_numero=$envio_numero2;}
					$envio_extension=$row["EnvioExtension"];
					$envio_aclarador=$row["EnvioAclarador"];
					$envio_cp=$row["EnvioCP"];
					$envio_poblacion=$row["EnvioPoblacion"];
					$envio_provincia=$row["EnvioProvincia"];
					$envio_telefono=$row["EnvioTelefono1"];
					$envio_movil=$row["EnvioTelefono2"];
					$envio_fax=$row["EnvioFax"];
					$envio_email=$row["EnvioEmail"];
					$envio_dni=$row["EnvioDNI"];
							
					$pagador_nombre=$row["PagadorNombre"];
					$pagador_calle=$row["PagadorCalle"];
					
					$pagador_numero2=$row["PagadorNumero"];
					if ($pagador_numero2==0){$pagador_numero='';} else {$pagador_numero=$pagador_numero2;}
					
					$pagador_extension=$row["PagadorExtension"];
					$pagador_aclarador=$row["PagadorAclarador"];
					$pagador_CP=$row["PagadorCP"];
					$pagador_poblacion=$row["PagadorPoblacion"];
					$pagador_provincia=$row["PagadorProvincia"];
					$pagador_telefono=$row["PagadorTelefono1"];
					$pagador_movil=$row["PagadorTelefono2"];
					$pagador_fax=$row["PagadorFax"];
					$pagador_email=$row["PagadorEmail"];
					$pagador_dni=$row["PagadorDNI"];
							
					$representante_nombre=$row["RepresentanteEmpresa"];
					$representante_dni=$row["DNIRepresentante"];
													
//Ahora subdividiremos el numero de la cuenta de banco para escribir las cifras correspondientes en cada uno de los cuadros
					$num_cuenta_banco2=substr($row["NumeroCuenta"],0,4);
					$num_cuenta_sucursal2=substr($row["NumeroCuenta"],4,4);
					$num_cuenta_digito_control2=substr($row["NumeroCuenta"],8,2);
					$num_cuenta_cuenta2=substr($row["NumeroCuenta"],10,10);
					
					if ($num_cuenta_banco2==0){$num_cuenta_banco='';} else {$num_cuenta_banco=$num_cuenta_banco2;}
					if ($num_cuenta_sucursal2==0){$num_cuenta_sucursal='';} else {$num_cuenta_sucursal=$num_cuenta_sucursal2;}		
					if ($num_cuenta_digito_control2==0){$num_cuenta_digito_control='';} else {$num_cuenta_digito_control=$num_cuenta_digito_control2;}
					if ($num_cuenta_cuenta2==0){$num_cuenta_cuenta='';} else {$num_cuenta_cuenta=$num_cuenta_cuenta2;}
	//Ahora subdividiremos el numero del Iban de banco para escribir las cifras correspondientes en cada uno de los cuadros	
					$iban1_2=substr($row["Iban"],0,4);
					$iban2_2=substr($row["Iban"],4,4);
					$iban3_2=substr($row["Iban"],8,4);
					$iban4_2=substr($row["Iban"],12,4);
					$iban5_2=substr($row["Iban"],16,4);
					$iban6_2=substr($row["Iban"],20,4);
					$iban7_2=substr($row["Iban"],24,4);
					$iban8_2=substr($row["Iban"],20,4);
					$iban9_2=substr($row["Iban"],24,4);
					//if ($iban1_2==0){$iban1='';} else {$iban1=$iban1_2;}
					if ($iban2_2==0){$iban2='';} else {$iban2=$iban2_2;}		
					if ($iban3_2==0){$iban3='';} else {$iban3=$iban3_2;}
					if ($iban4_2==0){$iban4='';} else {$iban4=$iban4_2;}	
					if ($iban5_2==0){$iban5='';} else {$iban5=$iban5_2;}
					if ($iban6_2==0){$iban6='';} else {$iban6=$iban6_2;}
					if ($iban7_2==0){$iban7='';} else {$iban7=$iban7_2;}
					if ($iban8_2==0){$iban8='';} else {$iban6=$iban8_2;}
					if ($iban9_2==0){$iban9='';} else {$iban7=$iban9_2;}
					
					
					$banco_extranjero["BancoExtranjero"];
					if ($banco_extranjero==""){$banco_extranjero='No';} else {$banco_extranjero='Si';}	
					
					$forma_pago=$row["FormaPago"];
					$idioma_factura=$row["IdiomaFactura"];	
					$entidad_bancaria=$row["BancoDomiciliado"];									
													
					$suministro_poblacion=$row["SuministroCiudad"];
					$suministro_provincia=$row["SuministroProvincia"];				
					$suministro_calle=$row["SuministroCalle"];								
					$suministro_numero=$row["SuministroNumero"];				
					$suministro_extension=$row["SuministroExtension"];								
					$suministro_aclarador=$row["SuministroExtension2"];				
					$suministro_cp=$row["SuministroCP"];				
					$suministro_telefono=$row["SuministroTelefono1"];				
					$suministro_movil=$row["SuministroTelefono2"];								
					$suministro_fax=$row["SuministroFax"];												
					$suministro_email=$row["SuministroEmail"];		
																
					$tipo_tension=$row["TipoTension"];									
					$equipos_medida=$row["TipoAlquiler"];
					$maximetro=$row["Maximetro"];
					$modo=$row["Modo"];
					$fases=$row["Fases"];
					$reactiva=$row["Reactiva"];				
								
					$p1_activa=$row["P1Activa"];				
					$p1_reactiva=$row["P1Reactiva"];				
					$p1_maximetro=$row["P1Maximetro"];			
				
					$p2_activa=$row["P2Activa"];				
					$p2_reactiva=$row["P2Reactiva"];				
					$p2_maximetro=$row["P2Maximetro"];			
								
					$p3_activa=$row["P3Activa"];				
					$p3_reactiva=$row["P3Reactiva"];				
					$p3_maximetro=$row["P3Maximetro"];			
								
					$p4_activa=$row["P4Activa"];				
					$p4_reactiva=$row["P4Reactiva"];				
					$p4_maximetro=$row["P4Maximetro"];			
								
					$p5_activa=$row["P5Activa"];				
					$p5_reactiva=$row["P5Reactiva"];				
					$p5_maximetro=$row["P5Maximetro"];			
							
					$p6_activa=$row["P6Activa"];				
					$p6_reactiva=$row["P6Reactiva"];				
					$p6_maximetro=$row["P6Maximetro"];																									
						
//Cambiaremos el valor del discrminador almacenado en la base de datos						
					if($row["Discriminador"]=="a") $discriminador="DHA";			
?>                        

		<fieldset class="tipotabla">
            <legend class="arialblack14"> CUPS </legend>
            <div class="margen_superior_inicial_datos_titular" align="left">  <strong>CUPS:</strong>
				<?=$cups?>
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                      
                <strong><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 196');
			  	$rowTexto = mysql_fetch_array($resultTexto);
			  	echo($rowTexto['Texto']);?> </strong>
			      
			     <?=$_SESSION['numeroCliente']?>
		     </div>
       </fieldset>
         
    <fieldset class="tipotabla">
        <legend class="arialblack14"><?=$oficina_datos_instalacion38?></legend>
		<div class="margen_superior_inicial_datos_titular" align="left">	
		<p>
			<div style="font-weight:bold; float:left; width:200px;"><?=$oficina_datos_instalacion39?> <?=$oficina_datos_instalacion38?></div>
			<?=$nombre_comercializadora?>
          <br />
			<div style="font-weight:bold; float:left; width:200px;"><?=$oficina_datos_instalacion39?> <?=$oficina_datos_titular_distribuidora?></div>
            <?=$nombre_distribuidora?>
          <br />
            <div style="font-weight:bold; float:left; width:150px;"><?=$oficina_datos_instalacion40?></div>
            <div style="float:left"><?=$fecha_alta_comercializadora?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
			<div style="font-weight:bold; float:left; width:150px;"><?=$oficina_datos_instalacion41?></div>
			<?=$fecha_baja_comercializadora?>
         </p>
   		 </div>
	</fieldset>
    <fieldset class="tipotabla">
           <legend class="arialblack14">
           <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 133'); 
		   $rowTexto = mysql_fetch_array($resultTexto); 
		   echo($rowTexto['Texto']);?>
           </legend>
					
           <p>
               <div style="font-weight:bold; width:100px; float:left"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 134'); 
			   $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?></div>
			   <?=$representante_nombre?>&nbsp;
               <br />
               <div style="font-weight:bold; width:100px; float:left"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 118'); 
			   $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                </div>
				<?=$representante_dni?>&nbsp;
            </p>
   		</fieldset>
</div>

	<!-- SUMINISTRO -->
    <div id="cont2" class="nover">
          <fieldset class="tipotabla">
              <legend class="arialblack14">
                 <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 130'); 
                       $rowTexto = mysql_fetch_array($resultTexto); 
                       echo($rowTexto['Texto']);?>
              </legend>
              <p>
                   <div style="font-weight:bold; width:100px; float:left "><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 216'); 
                   $rowTexto = mysql_fetch_array($resultTexto); 
                   echo($rowTexto['Texto']);?></div>
        
                   <?php echo $suministro_calle?>&nbsp;
                  <br />
                    <div style="font-weight:bold; width:100px; float:left"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 124'); 
                    $rowTexto = mysql_fetch_array($resultTexto); 
                    echo($rowTexto['Texto']);?></div>
                    <?php echo $suministro_numero?>&nbsp;
                  <br />       
                    <div style="font-weight:bold; width:100px; float:left"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 125'); 
                    $rowTexto = mysql_fetch_array($resultTexto); 
                    echo($rowTexto['Texto']);?></div>
                    <?php echo $suministro_extension?>&nbsp;
                  <br />           
                    <div style="font-weight:bold; width:100px; float:left"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 288'); 
                    $rowTexto = mysql_fetch_array($resultTexto); 
                    echo($rowTexto['Texto']);?></div>
                    <?php echo $suministro_aclarador?>&nbsp;
                  <br />
                    <div style="font-weight:bold; width:100px; float:left"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 289'); 
                    $rowTexto = mysql_fetch_array($resultTexto); 
                    echo($rowTexto['Texto']);?></div>
                    <?php echo $suministro_cp?>&nbsp;
                  <br />
                    <div style="font-weight:bold; width:100px; float:left"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 217'); 
                    $rowTexto = mysql_fetch_array($resultTexto); 
                    echo($rowTexto['Texto']);?></div>
                    <?php echo $suministro_poblacion?>&nbsp;
                  <br />
                    <div style="font-weight:bold; width:100px; float:left"><?php echo $oficina_datos_titular2?></div>
                    <?php echo $suministro_provincia?>&nbsp;
                  <br />
                    <div style="font-weight:bold; width:100px; float:left"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 126'); 
                    $rowTexto = mysql_fetch_array($resultTexto); 
                    echo($rowTexto['Texto']);?></div>
                    <div style="float:left"><?php echo $suministro_telefono?>&nbsp;&nbsp;&nbsp;</div>
                  <br />
                    <div style="font-weight:bold; width:100px; float:left"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 127'); 
                    $rowTexto = mysql_fetch_array($resultTexto); 
                    echo($rowTexto['Texto']);?></div>
                    <?php echo $suministro_movil?>&nbsp;
                  <br />
                    <div style="font-weight:bold; width:100px; float:left"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 128'); 
                    $rowTexto = mysql_fetch_array($resultTexto); 
                    echo($rowTexto['Texto']);?></div>
                    <?php echo $suministro_fax?>&nbsp;
                  <br />
                    <div style="font-weight:bold; width:100px; float:left"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 129'); 
                    $rowTexto = mysql_fetch_array($resultTexto); 
                    echo($rowTexto['Texto']);?></div>
                    <?php echo $suministro_email?>&nbsp;     
                </p>               
        </fieldset>

        <fieldset class="tipotabla">
        <legend class="arialblack14">
		   <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 145'); 
           $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
        </legend>
                      <table width="510" align="center" align="center" border="0" style="margin-left:0px;margin-top: -20px;">
		                  <tr>
        		            <td style="padding-left:0px;">
                            	<div class="columna_modificacion_suministro separacion_cabecera" style="margin-top: 33px; font-size: 12px;">
									<div style="font-weight:bold;"><?php echo $oficina_datos1?></div>
									<div style="font-weight:bold;"><?php echo $oficina_datos2?></div>                            
   									<div style="font-weight:bold;">
									<?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 333'); 
									$rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?></div>
									<div style="font-weight:bold;">
									<?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 149'); 
									$rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?></div>
									<div style="font-weight:bold;">
									<?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 150'); 
									$rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?></div>
									<div style="font-weight:bold;">
									<?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 296'); 
									$rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?></div>
                                    <div style="font-weight:bold;">
									<?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 563'); 
									$rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?></div>
									<div style="font-weight:bold;">
									<?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 151'); 
									$rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?></div>                                    
                                    
                                </div><!--<div class="columna_modificacion_suministro arial14">-->
                                                              
            <div class="columna_modificacion_suministro separacion_columnas" style="font-size:12px">
				<div>                                   
                <div class="cabecera_columnas"><!--<?php echo $oficina_modificacion_datossuministro1?>--> <br /></fieldset>
	        </div><!--<div class="componente_columna_modificacion_suministro_campo_mostrar">-->

			<div><?php echo $row["PotenciaContratada"]?>                    
	        </div><!--<div class="componente_columna_modificacion_suministro_campo_mostrar">-->       
                    
			<div>                    
		      	 <?php echo $row["TarifaContratada"]?>                                                                            
	        </div><!--<div class="componente_columna_modificacion_suministro_campo_mostrar">-->                    
			
			
			<div>                    
<?php
//Se sustituye el valor almacenado en la base de datos por el que se le muestra al usuario en el combo
						if($row["Reactiva"]=="No")
						{
							$resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 170'); 
							$reactiva_mostrar= mysql_fetch_array($resultTexto);
						}//if($row["Reactiva"]=="Si")
						else

						{
							$resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 171'); 
							$reactiva_mostrar=mysql_fetch_array($resultTexto);
						}//else($row["Reactiva"]=="Si")
						
?>                    
		      	      <?php echo $reactiva_mostrar["Texto"]?>
                                                                                  
            </div><!--<div class="componente_columna_modificacion_suministro_campo_mostrar">-->       
                                        
			<div>                    
<?php
//Se sustituye el valor almacenado en la base de datos por el que se le muestra al usuario en el combo

						if($row["Maximetro"]=="No")
						{
							$resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 170'); 
							$maximetro_mostrar= mysql_fetch_array($resultTexto);
						}//if($row["Discriminador"]=="a")
						else
						{
							$resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 171'); 
							$maximetro_mostrar=mysql_fetch_array($resultTexto);
						}//else($row["Discriminador"]=="a")
						
?>                    
		      	      <?php echo $maximetro_mostrar["Texto"]?>
                                                                                  
                    </div><!--<div class="componente_columna_modificacion_suministro_campo_mostrar">--> 
                      
					<div>                                                            
		      	      <?php echo $row["Modo"]?>                      
                                      
	                </div><!--<div class="componente_columna_modificacion_suministro_campo_mostrar">--> 
                                 
					<div>                                                            
		      	      <?php echo $row["TipoTension"]?>  &nbsp;                   
                                      
	                </div><!--<div class="componente_columna_modificacion_suministro_campo_mostrar">-->                                  

					<div>                                                            
		      	      <?php if ($row["Fases"]==""){
						  echo '<br>';
					  } else {
						 echo $row["Fases"];
					  }
					  ?>                      
                                      
	                </div><!--<div class="componente_columna_modificacion_suministro_campo_mostrar">-->                                  
                                  
					<div>                                                            

<?php
//Se sustituye el valor almacenado en la base de datos por el que se le muestra al usuario en el combo

						if($row["TipoAlquiler"]=="Alquiler Empresa")
						{
							$equipo_medida_mostrar=$equipos_medida1;
						}//if($row["TipoAlquiler"]=="Alquiler Empresa")
						else
						{
							$equipo_medida_mostrar=$equipos_medida2;
						}//else($row["TipoAlquiler"]=="Alquiler Empresa")
						
?>   
		      	      <?php echo $equipo_medida_mostrar?>                    
                                      
	                </div><!--<div class="componente_columna_modificacion_suministro_campo_mostrar">-->                                                                                    
                </div><!--<div class="columna_modificacion_suministro separacion_columnas">-->                                
                                                                                                   
               </td>
       		  </tr>
						<tr> 
                          <td height="25" valign="top" class="" style="font-weight:bold;font-size:12px"><?php echo $oficina_contratacion_alta5?>:</td>
                        </tr>

					<tr> 
                          <td height="25" valign="top" class="arial14">	               
                			<table style="font-size:12px" border="1" bordercolor="#999999" width="100%" cellpadding="0" cellspacing="0">
							  <tr bordercolor="#999999">
							    <td align="center" width="25%">&nbsp;</td>
							    <td align="center" width="25%"><strong><?php echo $oficina_contratacion_alta7?></strong></td>
							    <td align="center" width="25%"><strong><?php echo $oficina_contratacion_alta8?></strong></td>
							    <td align="center" width="25%"><strong><?php echo $oficina_contratacion_alta9?></strong></td>
							  </tr>
                              
							  <tr bordercolor="#999999">
							    <td align="center" width="25%"><strong><?php echo $periodo_tarifario1?></strong></td>
                                
                                
                                <?php  if ($p1_activa=='No') {$clase='textfieldCentrado2';} else {$clase='textfieldCentrado';} ?>
                                
                                <td align="center" width="25%"><input id="P1_activa" name="P1_activa" type="text" readonly="readonly" class="<?php echo $clase?>" size="5" maxlength="2" value="<?php echo $p1_activa?>"/></td>
                                  <?php  if ($p1_reactiva=='No') {$clase='textfieldCentrado2';} else {$clase='textfieldCentrado';} ?>
                                <td align="center" width="25%"><input id="P1_reactiva" name="P1_reactiva" type="text" readonly="readonly" class="<?php echo $clase?>" size="5" maxlength="2" value="<?php echo $p1_reactiva?>"/></td>
                                  <?php  if ($p1_maximetro=='No') {$clase='textfieldCentrado2';} else {$clase='textfieldCentrado';} ?>
                                <td align="center" width="25%"><input id="P1_maximetro" name="P1_maximetro" type="text" readonly="readonly" class="<?php echo $clase?>" size="5" maxlength="2" value="<?php echo $p1_maximetro?>"/></td>
							  </tr>
                              
                              <tr bordercolor="#999999">
                                <td align="center" width="25%"><strong><?php echo $periodo_tarifario2?></strong></td>
                                
                                <?php  if ($p2_activa=='No') {$clase='textfieldCentrado2';} else {$clase='textfieldCentrado';} ?>
                                <td align="center" width="25%"><input id="P2_activa" name="P2_activa" type="text" readonly="readonly" class="<?php echo $clase?>" size="5" maxlength="2" value="<?php echo $p2_activa?>"/></td>
                                  <?php if ($p2_reactiva=='No') {$clase='textfieldCentrado2';} else {$clase='textfieldCentrado';} ?>
                                <td align="center" width="25%"><input id="P2_reactiva" name="P2_reactiva" type="text" readonly="readonly" class="<?php echo $clase?>" size="5" maxlength="2" value="<?php echo $p2_reactiva?>"/></td>
                                 <?php if ($p2_maximetro=='No') {$clase='textfieldCentrado2';} else {$clase='textfieldCentrado';} ?>
                                <td align="center" width="25%"><input id="P2_maximetro" name="P2_maximetro" type="text" readonly="readonly" class="<?php echo $clase?>" size="5" maxlength="2" value="<?php echo $p2_maximetro?>"/></td>
                              </tr>
                              
                              <tr bordercolor="#999999">
                                <td align="center" width="25%"><strong><?php echo $periodo_tarifario3?></strong></td>
                             
                              <?php if ($p3_activa=='No') {$clase='textfieldCentrado2';} else {$clase='textfieldCentrado';} ?>
                                 <td align="center" width="25%"><input id="P3_activa" name="P3_activa" type="text" readonly="readonly" class="<?php echo $clase?>" size="5" maxlength="2" value="<?php echo $p3_activa?>"/></td>
                              <?php if ($p3_reactiva=='No') {$clase='textfieldCentrado2';} else {$clase='textfieldCentrado';} ?>
                                <td align="center" width="25%"><input id="P3_reactiva" name="P3_reactiva" type="text" readonly="readonly" class="<?php echo $clase?>" size="5" maxlength="2" value="<?php echo $p3_reactiva?>"/></td>
                              <?php if ($p3_maximetro=='No') {$clase='textfieldCentrado2';} else {$clase='textfieldCentrado';} ?>
                                <td align="center" width="25%"><input id="P3_maximetro" name="P3_maximetro" type="text" readonly="readonly" class="<?php echo $clase?>" size="5" maxlength="2" value="<?php echo $p3_maximetro?>"/></td>
                              </tr>
                              
                              <tr bordercolor="#999999">
                                <td align="center" width="25%"><strong>P4</strong></td>
                                <?php if ($p4_activa=='No') {$clase='textfieldCentrado2';} else {$clase='textfieldCentrado';} ?>
                                <td align="center" width="25%"><input id="P4_activa" name="P4_activa" type="text" readonly="readonly" class="<?php echo $clase?>" size="5" maxlength="2" value="<?php echo $p4_activa?>"/></td>
                                <?php if ($p4_reactiva=='No') {$clase='textfieldCentrado2';} else {$clase='textfieldCentrado';} ?>
                                <td align="center" width="25%"><input id="P4_reactiva" name="P4_reactiva" type="text" readonly="readonly" class="<?php echo $clase?>" size="5" maxlength="2" value="<?php echo $p4_reactiva?>"/></td>
                                <?php if ($p4_maximetro=='No') {$clase='textfieldCentrado2';} else {$clase='textfieldCentrado';} ?>
                                <td align="center" width="25%"><input id="P4_maximetro" name="P4_maximetro" type="text" readonly="readonly" class="<?php echo $clase?>" size="5" maxlength="2" value="<?php echo $p4_maximetro?>"/></td>
                              </tr>
                              
                              <tr bordercolor="#999999">
                                <td align="center" width="25%"><strong>P5</strong></td>
                                  <?php if ($p5_activa=='No') {$clase='textfieldCentrado2';} else {$clase='textfieldCentrado';} ?>
                                <td align="center" width="25%"><input id="P5_activa" name="P5_activa" type="text" readonly="readonly" class="<?php echo $clase?>" size="5" maxlength="2" value="<?php echo $p5_activa?>"/></td>
                                <?php if ($p5_reactiva=='No') {$clase='textfieldCentrado2';} else {$clase='textfieldCentrado';} ?>
                                <td align="center" width="25%"><input id="P5_reactiva" name="P5_reactiva" type="text" readonly="readonly" class="<?php echo $clase?>" size="5" maxlength="2" value="<?php echo $p5_reactiva?>"/></td>
                                <?php if ($p5_maximetro=='No') {$clase='textfieldCentrado2';} else {$clase='textfieldCentrado';} ?>
                                <td align="center" width="25%"><input id="P5_maximetro" name="P5_maximetro" type="text" readonly="readonly" class="<?php echo $clase?>" size="5" maxlength="2" value="<?php echo $p5_maximetro?>"/></td>
                              </tr>
                              
                              <tr bordercolor="#999999">
                                <td align="center" width="25%"><strong>P6</strong></td>
                                  <?php if ($p6_activa=='No') {$clase='textfieldCentrado2';} else {$clase='textfieldCentrado';} ?>
                                <td align="center" width="25%"><input id="P6_activa" name="P6_activa" type="text" readonly="readonly" class="<?php echo $clase?>" size="5" maxlength="2" value="<?php echo $p6_activa?>"/></td>
                                <?php if ($p6_reactiva=='No') {$clase='textfieldCentrado2';} else {$clase='textfieldCentrado';} ?>
                                <td align="center" width="25%"><input id="P6_reactiva" name="P6_reactiva" type="text" readonly="readonly" class="<?php echo $clase?>" size="5" maxlength="2" value="<?php echo $p6_reactiva?>"/></td>
                                <?php if ($p6_maximetro=='No') {$clase='textfieldCentrado2';} else {$clase='textfieldCentrado';} ?>
                                <td align="center" width="25%"><input id="P6_maximetro" name="P6_maximetro" type="text" readonly="readonly" class="<?php echo $clase?>" size="5" maxlength="2" value="<?php echo $p6_maximetro?>"/></td>
                              </tr>
                              
                            </table>                
                			</td>
                        </tr>                                             
                    </table>
                </fieldset>
          </div>
</div>

    
     <!-- FACTURA -->
     <div id="cont4" class="nover">
	 <fieldset class="tipotabla">
		 <legend class="arialblack14">Datos Facturación</legend>
         <p>
               <div style="font-weight:bold; width:100px; float:left"><?=$oficina_datos_titular17?></div>
			   <?=$factura_nombre?>&nbsp;
             <br />
               <div style="font-weight:bold; width:100px; float:left"><?=$oficina_datos_titular18?></div>
               <?=$factura_calle?>&nbsp;
             <br />
               <div style="font-weight:bold; width:100px; float:left"><?=$oficina_datos_titular19?></div>
               <?=$factura_numero?>&nbsp;
             <br />
               <div style="font-weight:bold; width:100px; float:left"><?=$oficina_datos_titular20?></div>
               <?=$factura_extension?>&nbsp;
             <br />
               <div style="font-weight:bold; width:100px; float:left"><?=$oficina_datos_titular21?></div>
               <?=$factura_aclarador?>&nbsp;
             <br />
               <div style="font-weight:bold; width:100px; float:left"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 289'); 
			   $rowTexto = mysql_fetch_array($resultTexto); 
			   echo($rowTexto['Texto']);?></div>
               <?=$factura_cp?>&nbsp;
             <br />
               <div style="font-weight:bold; width:100px; float:left"><?=$oficina_datos_titular22?></div>
               <?=$factura_poblacion?>&nbsp;
             <br />
               <div style="font-weight:bold; width:100px; float:left"><?=$oficina_datos_titular23?></div>
               <?=$factura_provincia?>&nbsp;
             <br />
               <div style="font-weight:bold; width:100px; float:left"><?=$oficina_datos_titular24?></div>
               <?=$factura_telefono?>&nbsp;
             <br />
               <div style="font-weight:bold; width:100px; float:left"><?=$oficina_datos_titular25?></div>
               <?=$factura_movil?>&nbsp;
             <br />
               <div style="font-weight:bold; width:100px; float:left"><?=$oficina_datos_titular26?></div>
               <?=$factura_fax?>&nbsp;
             <br />
               <div style="font-weight:bold; width:100px; float:left"><?=$oficina_datos_titular27?></div>
               <?=$factura_email?>&nbsp;
             <br />
               <div style="font-weight:bold; width:100px; float:left"><?=$oficina_datos_titular28?></div>
               <?=$factura_dni?>&nbsp;
        </p>             
    </fieldset>
    
    <!-- PAGADOR -->
    <fieldset class="tipotabla">
		<legend class="arialblack14"><?=$oficina_datos_titular29?>&nbsp;</legend>
        <p>
           <div style="font-weight:bold; width:100px; float:left"><?=$oficina_datos_titular30?></div>
           <?=$pagador_nombre?>&nbsp;
         <br />
           <div style="font-weight:bold; width:100px; float:left"><?=$oficina_datos_titular31?></div>
           <?=$pagador_calle?>&nbsp;
         <br />
           <div style="font-weight:bold; width:100px; float:left"><?=$oficina_datos_titular32?></div>
         <br /><?=$pagador_numero?>&nbsp;                      
           <div style="font-weight:bold; width:100px; float:left"><?=$oficina_datos_titular33?></div>
           <?=$pagador_extension?>&nbsp;
         <br />
           <div style="font-weight:bold; width:100px; float:left"><?=$oficina_datos_titular34?></div>
           <?=$pagador_aclarador?>&nbsp;
         <br />
           <div style="font-weight:bold; width:100px; float:left">
           <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 289'); 
		   $rowTexto = mysql_fetch_array($resultTexto); 
		   echo($rowTexto['Texto']);?></div>
           <?=$pagador_cp?>&nbsp;
         <br />
           <div style="font-weight:bold; width:100px; float:left"><?=$oficina_datos_titular35?></div>
           <?=$pagador_poblacion?>&nbsp;
         <br />
           <div style="font-weight:bold; width:100px; float:left"><?=$oficina_datos_titular36?></div>
           <?=$pagador_provincia?>&nbsp;
         <br />
           <div style="font-weight:bold; width:100px; float:left"><?=$oficina_datos_titular37?></div>
           <?=$pagador_telefono?> &nbsp;                    
		 <br />
           <div style="font-weight:bold; width:100px; float:left"><?=$oficina_datos_titular38?></div>
           <?=$pagador_movil?>&nbsp;
         <br />
            <div style="font-weight:bold; width:100px; float:left"><?=$oficina_datos_titular39?></div>
            <?=$pagador_fax?>&nbsp;
         <br />
           <div style="font-weight:bold; width:100px; float:left"><?=$oficina_datos_titular40?></div>
           <?=$pagador_email?>&nbsp;
         <br />
            <div style="font-weight:bold; width:100px; float:left"><?=$oficina_datos_titular41?></div>
            <?=$pagador_dni?>&nbsp;
      </p>             
  </fieldset>
  <p> </p>
    <fieldset class="tipotabla">
		 <legend class="arialblack14">
         <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 138'); 
		 $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
         </legend>           

		 <p>
           <div style="font-weight:bold; width:120px; float:left"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 292'); 
		   $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?></div> 
           <?=$forma_pago?> &nbsp;                      
           <br />
           <div style="font-weight:bold; width:120px; float:left"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 139'); 
		   $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?></div>
           <?=$entidad_bancaria?>&nbsp;
         </p>
              <fieldset>
                    <table width="500" border="0" align="center" cellpadding="0" cellspacing="0" class="arial14">
                      <tr> 
                        <td width="450">
                          <span class="arialblack14">
                          <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 140'); 
						  $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                          </span>
                          <table width="400" align="center" border="0" style="font-size:12px">
                          
                            <tr align="center"> 
                                <td valign="top">
                                <strong><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 141'); 
								$rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?></strong>
                                  </td>
                                  <td width="10">  </td>
                              <td><strong><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 142'); 
							  $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?></strong>
                                  </td>
                                  <td width="10">  </td>
                              <td><strong><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 143'); 
							  $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?> </strong>
                                    </td>
                                    <td width="10">  </td>
                              <td><strong><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 144'); 
							  $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?></strong>
                                  </td>                                
                            </tr>
                                    <tr>
                                    <td style="text-align:center"><?=$num_cuenta_banco?></td>
                                    <td width="10"> - </td>
                                    <td style="text-align:center"><?=$num_cuenta_sucursal?></td>
                                    <td width="10"> - </td>
                                    <td style="text-align:center"><?=$num_cuenta_digito_control?></td>
                                    <td width="10"> - </td>
                                    <td style="text-align:center"><?=$num_cuenta_cuenta?></td>
                            </tr>
                                  </table>
                           </td></tr>
                      </table> 
                       <table width="500" border="0" align="center" cellpadding="0" cellspacing="0" class="arial14">
                      <tr> 
                        <td width="450">
                          <table width="400" align="center" border="0" style="margin-left: 0px; font-size:12px;">
                                    <tr>
                                    <td style="text-align:center;width: 47px;padding-right: 16px;"><strong>IBAN</strong></td>
                                    <td style="text-align:center"><?=$iban1_2?></td>
                                    <td width="10"> - </td>
                                    <td style="text-align:center"><?=$iban2?></td>
                                    <td width="10"> - </td>
                                    <td style="text-align:center"><?=$iban3?></td>
                                    <td width="10"> - </td>
                                    <td style="text-align:center"><?=$iban4?></td>
                                    <td width="10"> - </td>
                                    <td style="text-align:center"><?=$iban5?></td>
                                     <td width="10"> - </td>
                                    <td style="text-align:center"><?=$iban6?></td>
                                    <?php
										if ($iban7!=''){
											?>
											 <td width="10"> - </td>
                                    		<td style="text-align:center"><?=$iban7?></td>
                                             <td width="10">  </td>
                                            <td width="10"><strong>CEE: </strong></td>
                                            <td style="text-align:center"><?=$banco_extranjero?></td>
											<?php
										}else{
											?>
											<td width="10">  </td>
                                    <td width="10"><strong style="padding-left: 60px;">CEE: </strong></td>
                                    <td style="text-align:center"><?=$banco_extranjero?></td>
											<?php
											}
									?>
                            </tr>
                                  </table>
                           </td></tr>
                      </table> 
                    </fieldset>
         <div class="limpiar"></div>
         <div class="espacio"></div>
     </fieldset>      
</div>
    <!-- TITULAR-->
    <div id="cont3" class="nover">
    <fieldset class="tipotabla">
		<legend class="arialblack14"><?=$oficina_datos_titular3?>&nbsp;</legend>
        <p>
			 <div style="font-weight:bold; width:100px; float:left"><?=$oficina_datos_titular4?></div>
             <?=$titular_nombre?>&nbsp;
           <br />
             <div style="font-weight:bold; width:100px; float:left;"> <?=$oficina_datos_titular5?></div>
             <?=$titular_calle?>&nbsp;
           <br />
             <div style="font-weight:bold; width:100px; float:left"><?=$oficina_datos_titular6?></div>
             <?=$titular_numero?>&nbsp;
           <br />
             <div style="font-weight:bold; width:100px; float:left"><?=$oficina_datos_titular7?></div>
             <?=$titular_extension?>&nbsp;
           <br />
             <div style="font-weight:bold; width:100px; float:left"><?=$oficina_datos_titular8?></div>
             <?=$titular_aclarador?>&nbsp;
           <br />
           	 <div style="font-weight:bold; width:100px; float:left"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 289');
			 $rowTexto = mysql_fetch_array($resultTexto); 
			 echo($rowTexto['Texto']);?></div>
			 <?=$titular_cp?>&nbsp;
            <br />
             <div style="font-weight:bold; width:100px; float:left"> <?=$oficina_datos_titular9?></div>
             <?=$titular_poblacion?>&nbsp;
           <br />
             <div style="font-weight:bold; width:100px; float:left"><?=$oficina_datos_titular10?></div>
             <?=$titular_provincia?>&nbsp;
           <br />
             <div style="font-weight:bold; width:100px; float:left"><?=$oficina_datos_titular11?></div>
             <div style="float:left"><?=$titular_telefono?>&nbsp;&nbsp;&nbsp;</div>
           <br />
             <div style="font-weight:bold; width:100px; float:left"><?=$oficina_datos_titular12?></div>
         	 <?=$titular_movil?>&nbsp;
           <br />
             <div style="font-weight:bold; width:100px; float:left"><?=$oficina_datos_titular13?></div>
             <?=$titular_fax?>&nbsp;
           <br />
              <div style="font-weight:bold; width:100px; float:left"><?=$oficina_datos_titular14?></div>
         	  <?=$titular_email?>&nbsp;
           <br />
              <div style="font-weight:bold; width:100px; float:left"><?=$oficina_datos_titular15?></div>
         	   <?=$titular_dni?>&nbsp;
           </p>             
    </fieldset>

    <fieldset class="tipotabla">
		<legend class="arialblack14">Datos Envío</legend>                         
        <p>
            <div style="font-weight:bold; width:100px; float:left"><?=$oficina_datos_titular17?></div>
            <?=$envio_nombre?>&nbsp;
           <br />
            <div style="font-weight:bold; width:100px; float:left"><?=$oficina_datos_titular18?></div>
            <?=$envio_calle?>&nbsp;
           <br />
             <div style="font-weight:bold; width:100px; float:left"><?=$oficina_datos_titular19?></div>
           <br /><?=$envio_numero?>&nbsp;                      
             <div style="font-weight:bold; width:100px; float:left"><?=$oficina_datos_titular20?></div>
             <?=$envio_extension?>&nbsp;
           <br />
             <div style="font-weight:bold; width:100px; float:left"><?=$oficina_datos_titular21?></div>
             <?=$envio_aclarador?>&nbsp;
           <br />
             <div style="font-weight:bold; width:100px; float:left"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 289'); 
			 $rowTexto = mysql_fetch_array($resultTexto); 
			 echo($rowTexto['Texto']);?></div>
             <?=$envio_cp?>&nbsp;
           <br />
             <div style="font-weight:bold; width:100px; float:left"><?=$oficina_datos_titular22?></div>
             <?=$envio_poblacion?>&nbsp;
           <br />
             <div style="font-weight:bold; width:100px; float:left"><?=$oficina_datos_titular23?></div>
             <?=$envio_provincia?>&nbsp;
           <br />
            <div style="font-weight:bold; width:100px; float:left"><?=$oficina_datos_titular24?></div>
            <?=$envio_telefono?> &nbsp;                       
		   <br />
              <div style="font-weight:bold; width:100px; float:left"><?=$oficina_datos_titular25?></div>
              <?=$envio_movil?>&nbsp;
           <br />
             <div style="font-weight:bold; width:100px; float:left"><?=$oficina_datos_titular26?></div>
             <?=$envio_fax?>&nbsp;
           <br />
             <div style="font-weight:bold; width:100px; float:left"><?=$oficina_datos_titular27?></div>
             <?=$envio_email?>&nbsp;
           <br />
             <div style="font-weight:bold; width:100px; float:left"><?=$oficina_datos_titular28?></div>
             <?=$envio_dni?>&nbsp;</p>                  
       </fieldset>
  </div>     
 
<div id="cont5" class="nover">
     <div class="tipotabla" style="float:left; width:90%">
	          <table width="626px" border="0">
    	      <tr>
                <td width="100%" height="18" valign="top" class="arialblack14"><?php 
                $resulttabla = mysql_query('SELECT * FROM ConsumosRD WHERE Usuario='.$_SESSION['usuario']);
                  $rowtabla=mysql_fetch_array($resulttabla);
                $resultTexto=mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 470');
                   $rowTexto = mysql_fetch_array($resultTexto);
                  echo($rowTexto['Texto']); 
                  echo($rowtabla['AnioActual']);
                  ?></td>
              </tr>              
              <tr>
                <td height="282" valign="top">               
                  <table width="100%" border="0" cellpadding="0" cellspacing="0" style="color:#FFF;">
                     <tr style="color:#000; background-color:#FFF;">
                      <td width="78" height="20">&nbsp;</td>
                      <td width="62" align="center" valign="middle" class="arial12black2"><?php $resultTexto=mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 460');
                   $rowTexto = mysql_fetch_array($resultTexto);
                  echo($rowTexto['Texto']); ?></td>
                      <td width="62" align="center" valign="middle" class="arial12black2"><?php $resultTexto=mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 462');
                   $rowTexto = mysql_fetch_array($resultTexto);
                  echo($rowTexto['Texto']); ?></td>
                      <td width="62" align="center" valign="middle" class="arial12black2"><?php $resultTexto=mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 461');
                   $rowTexto = mysql_fetch_array($resultTexto);
                  echo($rowTexto['Texto']); ?></td>
                      <td width="62" align="center" valign="middle" class="arial12black2">&nbsp;</td>
                      <td width="62" align="center" valign="middle" class="arial12black2"><!--DWLayoutEmptyCell-->&nbsp;</td>
                      <td width="68" align="center" valign="middle" class="arial12black2"><!--DWLayoutEmptyCell-->&nbsp;</td>
                    </tr>
                     <tr style="color:#000; background-color:#FFF;">
                      <td height="20" valign="top" class="arial12black2"><!--DWLayoutEmptyCell-->&nbsp;</td>
                      <td align="center" valign="middle" class="arial12black2">P1</td>
                      <td align="center" valign="middle" class="arial12black2">P2</td>
                      <td align="center" valign="middle" class="arial12black2"><span class="arial12black2">P3</span></td>
                      <td align="center" valign="middle" class="arial12black2">P4</td>
                      <td align="center" valign="middle" class="arial12black2"><span class="arial12black2">P5</span></td>
                      <td align="center" valign="middle" class="arial12black2">P6</td>
                    </tr>
                     <tr>
                      <td height="20" valign="middle" style="color:#000; background-color:#FFF;" class="arial12black2">
					  	<?php $resultTexto=mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 471');
                   		$rowTexto = mysql_fetch_array($resultTexto);
                  		echo("&nbsp;" . $rowTexto['Texto']); ?>
                      </td>
                      <td colspan="6" rowspan="12" valign="top">
                      	<table width="100%" border="1" cellpadding="0" cellspacing="0" style="background-color:#EBF0F5">
                          <tr>
                          <td width="62" height="20" align="right" valign="middle" class="arial12Gris"><?php 
                          if(isset($rowtabla['EneroP1']) && $rowtabla['EneroP1']!=0){
                          echo(number_format($rowtabla['EneroP1'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                          ?></td>
                      <td width="62" align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['EneroP2']) && $rowtabla['EneroP2']!=0){
                          echo(number_format($rowtabla['EneroP2'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                     ?></td>
                      <td width="62" align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['EneroP3']) && $rowtabla['EneroP3']!=0){
                          echo(number_format($rowtabla['EneroP3'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                          ?></td>
                      <td width="62" align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['EneroP4']) && $rowtabla['EneroP4']!=0){
                          echo(number_format($rowtabla['EneroP4'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                     ?></td>
                      <td width="62" align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['EneroP5']) && $rowtabla['EneroP5']!=0){
                          echo(number_format($rowtabla['EneroP5'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                      ?></td>
                      <td width="68" align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['EneroP6']) && $rowtabla['EneroP6']!=0){
                          echo(number_format($rowtabla['EneroP6'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                    ?></td>
                    </tr>
                          <tr>
                          <td height="20" align="right" valign="middle" class="arial12Gris"><?php 
                          if(isset($rowtabla['FebreroP1']) && $rowtabla['FebreroP1']!=0){
                          echo(number_format($rowtabla['FebreroP1'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                        ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['FebreroP2']) && $rowtabla['FebreroP2']!=0){
                          echo(number_format($rowtabla['FebreroP2'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
    				  ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['FebreroP3']) && $rowtabla['FebreroP3']!=0){
                          echo(number_format($rowtabla['FebreroP3'],0,",","."));
                          }else{
                          echo('&nbsp;');
                      }
                      ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['FebreroP4']) && $rowtabla['FebreroP4']!=0){
                          echo(number_format($rowtabla['FebreroP4'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                      
                    ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['FebreroP5']) && $rowtabla['FebreroP5']!=0){
                          echo(number_format($rowtabla['FebreroP5'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                      ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['FebreroP6']) && $rowtabla['FebreroP6']!=0){
                          echo(number_format($rowtabla['FebreroP6'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                      ?></td>
                    </tr>
                          <tr>
                          <td height="20" align="right" valign="middle" class="arial12Gris"><?php 
                          if(isset($rowtabla['MarzoP1']) && $rowtabla['MarzoP1']!=0){
                          echo(number_format($rowtabla['MarzoP1'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                    ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['MarzoP2']) && $rowtabla['MarzoP2']!=0){
                          echo(number_format($rowtabla['MarzoP2'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                      ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['MarzoP3']) && $rowtabla['MarzoP3']!=0){
                          echo(number_format($rowtabla['MarzoP3'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                    ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php
                      if(isset($rowtabla['MarzoP4']) && $rowtabla['MarzoP4']!=0){
                          echo(number_format($rowtabla['MarzoP4'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                      ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['MarzoP5']) && $rowtabla['MarzoP5']!=0){
                          echo(number_format($rowtabla['MarzoP5'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                      ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['MarzoP6']) && $rowtabla['MarzoP6']!=0){
                          echo(number_format($rowtabla['MarzoP6'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                ?></td>
                    </tr>
                          <tr>
                          <td height="20" align="right" valign="middle" class="arial12Gris"><?php 
                          if(isset($rowtabla['AbrilP1']) && $rowtabla['AbrilP1']!=0){
                          echo(number_format($rowtabla['AbrilP1'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                         ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['AbrilP2']) && $rowtabla['AbrilP2']!=0){
                          echo(number_format($rowtabla['AbrilP2'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                      ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['AbrilP3']) && $rowtabla['AbrilP3']!=0){
                          echo(number_format($rowtabla['AbrilP3'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                    ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['AbrilP4']) && $rowtabla['AbrilP4']!=0){
                          echo(number_format($rowtabla['AbrilP4'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                     ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['AbrilP5']) && $rowtabla['AbrilP5']!=0){
                          echo(number_format($rowtabla['AbrilP5'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                       ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['AbrilP6']) && $rowtabla['AbrilP6']!=0){
                          echo(number_format($rowtabla['AbrilP6'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                    ?></td>
                    </tr>
                          <tr>
                          <td height="20" align="right" valign="middle" class="arial12Gris"><?php 
                          if(isset($rowtabla['MayoP1']) && $rowtabla['MayoP1']!=0){
                          echo(number_format($rowtabla['MayoP1'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                         ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['MayoP2']) && $rowtabla['MayoP2']!=0){
                          echo(number_format($rowtabla['MayoP2'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                      ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['MayoP3']) && $rowtabla['MayoP3']!=0){
                          echo(number_format($rowtabla['MayoP3'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                      ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['MayoP4']) && $rowtabla['MayoP4']!=0){
                          echo(number_format($rowtabla['MayoP4'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                      ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['MayoP5']) && $rowtabla['MayoP5']!=0){
                          echo(number_format($rowtabla['MayoP5'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                     ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['MayoP6']) && $rowtabla['MayoP6']!=0){
                          echo(number_format($rowtabla['MayoP6'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                     ?></td>
                    </tr>
                        <tr>
                          <td height="20" align="right" valign="middle" class="arial12Gris"><?php 
                          if(isset($rowtabla['JunioP1']) && $rowtabla['JunioP1']!=0){
                          echo(number_format($rowtabla['JunioP1'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                         ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['JunioP2']) && $rowtabla['JunioP2']!=0){
                          echo(number_format($rowtabla['JunioP2'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                     ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['JunioP3']) && $rowtabla['JunioP3']!=0){
                          echo(number_format($rowtabla['JunioP3'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                      ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['JunioP4']) && $rowtabla['JunioP4']!=0){
                          echo(number_format($rowtabla['JunioP4'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                    ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['JunioP5']) && $rowtabla['JunioP5']!=0){
                          echo(number_format($rowtabla['JunioP5'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                    ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['JunioP6']) && $rowtabla['JunioP6']!=0){
                          echo(number_format($rowtabla['JunioP6'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                     ?></td>
                    </tr>
                          <tr>
                          <td height="20" align="right" valign="middle" class="arial12Gris"><?php 
                          if(isset($rowtabla['JulioP1']) && $rowtabla['JulioP1']!=0){
                          echo(number_format($rowtabla['JulioP1'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                          ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['JulioP2']) && $rowtabla['JulioP2']!=0){
                          echo(number_format($rowtabla['JulioP2'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                     ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['JulioP3']) && $rowtabla['JulioP3']!=0){
                          echo(number_format($rowtabla['JulioP3'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                     ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['JulioP4']) && $rowtabla['JulioP4']!=0){
                          echo(number_format($rowtabla['JulioP4'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                      
                     ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['JulioP5']) && $rowtabla['JulioP5']!=0){
                          echo(number_format($rowtabla['JulioP5'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                    ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['JulioP6']) && $rowtabla['JulioP6']!=0){
                          echo(number_format($rowtabla['JulioP6'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                    ?></td>
                    </tr>
                          <tr>
                          <td height="20" align="right" valign="middle" class="arial12Gris"><?php 
                          if(isset($rowtabla['AgostoP1']) && $rowtabla['AgostoP1']!=0){
                          echo(number_format($rowtabla['AgostoP1'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                          ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['AgostoP2']) && $rowtabla['AgostoP2']!=0){
                          echo(number_format($rowtabla['AgostoP2'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                    ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['AgostoP3']) && $rowtabla['AgostoP3']!=0){
                          echo(number_format($rowtabla['AgostoP3'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                    ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['AgostoP4']) && $rowtabla['AgostoP4']!=0){
                          echo(number_format($rowtabla['AgostoP4'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                      ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['AgostoP5']) && $rowtabla['AgostoP5']!=0){
                          echo(number_format($rowtabla['AgostoP5'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                     ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['AgostoP6']) && $rowtabla['AgostoP6']!=0){
                          echo(number_format($rowtabla['AgostoP6'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }?></td>
                       </tr>
                          <tr>
                          <td height="20" align="right" valign="middle" class="arial12Gris"><?php 
                          if(isset($rowtabla['SeptiembreP1']) && $rowtabla['SeptiembreP1']!=0){
                          echo(number_format($rowtabla['SeptiembreP1'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['SeptiembreP2']) && $rowtabla['SeptiembreP2']!=0){
                          echo(number_format($rowtabla['SeptiembreP2'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['SeptiembreP3']) && $rowtabla['SeptiembreP3']!=0){
                          echo(number_format($rowtabla['SeptiembreP3'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['SeptiembreP4']) && $rowtabla['SeptiembreP4']!=0){
                          echo(number_format($rowtabla['SeptiembreP4'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['SeptiembreP5']) && $rowtabla['SeptiembreP5']!=0){
                          echo(number_format($rowtabla['SeptiembreP5'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['SeptiembreP6']) && $rowtabla['SeptiembreP6']!=0){
                          echo(number_format($rowtabla['SeptiembreP6'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }?></td>
                    </tr>
                          <tr>
                          <td height="20" align="right" valign="middle" class="arial12Gris"><?php 
                           if(isset($rowtabla['OctubreP1']) && $rowtabla['OctubreP1']!=0){
                          echo(number_format($rowtabla['OctubreP1'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                          ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                       if(isset($rowtabla['OctubreP2']) && $rowtabla['OctubreP2']!=0){
                          echo(number_format($rowtabla['OctubreP2'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                      ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                       if(isset($rowtabla['OctubreP3']) && $rowtabla['OctubreP3']!=0){
                          echo(number_format($rowtabla['OctubreP3'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                      ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                       if(isset($rowtabla['OctubreP4']) && $rowtabla['OctubreP4']!=0){
                          echo(number_format($rowtabla['OctubreP4'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                     ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                       if(isset($rowtabla['OctubreP5']) && $rowtabla['OctubreP5']!=0){
                          echo(number_format($rowtabla['OctubreP5'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                      ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                       if(isset($rowtabla['OctubreP6']) && $rowtabla['OctubreP6']!=0){
                          echo(number_format($rowtabla['OctubreP6'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                    ?></td>
                    </tr>
                           <tr>
                          <td height="20" align="right" valign="middle" class="arial12Gris"><?php 
                          if(isset($rowtabla['NoviembreP1']) && $rowtabla['NoviembreP1']!=0){
                          echo(number_format($rowtabla['NoviembreP1'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                        ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['NoviembreP2']) && $rowtabla['NoviembreP2']!=0){
                          echo(number_format($rowtabla['NoviembreP2'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                      ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['NoviembreP3']) && $rowtabla['NoviembreP3']!=0){
                          echo(number_format($rowtabla['NoviembreP3'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                      ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['NoviembreP4']) && $rowtabla['NoviembreP4']!=0){
                          echo(number_format($rowtabla['NoviembreP4'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                     ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['NoviembreP5']) && $rowtabla['NoviembreP5']!=0){
                          echo(number_format($rowtabla['NoviembreP5'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                     ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['NoviembreP6']) && $rowtabla['NoviembreP6']!=0){
                          echo(number_format($rowtabla['NoviembreP6'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                      ?></td>
                    </tr>
                          <tr>
                          <td height="20" align="right" valign="middle" class="arial12Gris"><?php 
                          if(isset($rowtabla['DiciembreP1']) && $rowtabla['DiciembreP1']!=0){
                          echo(number_format($rowtabla['DiciembreP1'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                         ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php
                      if(isset($rowtabla['DiciembreP2']) && $rowtabla['DiciembreP2']!=0){
                          echo(number_format($rowtabla['DiciembreP2'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                       ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['DiciembreP3']) && $rowtabla['DiciembreP3']!=0){
                          echo(number_format($rowtabla['DiciembreP3'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                    ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['DiciembreP4']) && $rowtabla['DiciembreP4']!=0){
                          echo(number_format($rowtabla['DiciembreP4'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                     ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['DiciembreP5']) && $rowtabla['DiciembreP5']!=0){
                          echo(number_format($rowtabla['DiciembreP5'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                      ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['DiciembreP6']) && $rowtabla['DiciembreP6']!=0){
                          echo(number_format($rowtabla['DiciembreP6'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                      ?></td>
                    </tr>
                      </table>		          
                      </td>
             		</tr>
                     <tr>
                      <td height="20" valign="middle" style="color:#000; background-color:#FFF;" class="arial12black2">&nbsp;<?php $resultTexto=mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 472');
                   $rowTexto = mysql_fetch_array($resultTexto);
                  echo($rowTexto['Texto']); ?></td>
                    </tr> 
                     <tr>
                      <td height="20" valign="middle" style="color:#000; background-color:#FFF;" class="arial12black2">&nbsp;<?php $resultTexto=mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 473');
                   $rowTexto = mysql_fetch_array($resultTexto);
                  echo($rowTexto['Texto']); ?></td>
                    </tr>
                     <tr>
                      <td height="20" valign="middle" style="color:#000; background-color:#FFF;" class="arial12black2">&nbsp;<?php $resultTexto=mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 474');
                   $rowTexto = mysql_fetch_array($resultTexto);
                  echo($rowTexto['Texto']); ?></td>
                    </tr>
                     <tr>
                      <td height="20" valign="middle" style="color:#000; background-color:#FFF;" class="arial12black2">&nbsp;<?php $resultTexto=mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 475');
                   $rowTexto = mysql_fetch_array($resultTexto);
                  echo($rowTexto['Texto']); ?></td>
                    </tr>
                     <tr>
                      <td height="20" valign="middle" style="color:#000; background-color:#FFF;" class="arial12black2">&nbsp;<?php $resultTexto=mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 476');
                   $rowTexto = mysql_fetch_array($resultTexto);
                  echo($rowTexto['Texto']); ?></td>
                    </tr>
                     <tr>
                      <td height="20" valign="middle" style="color:#000; background-color:#FFF;" class="arial12black2">&nbsp;<?php $resultTexto=mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 477');
                   $rowTexto = mysql_fetch_array($resultTexto);
                  echo($rowTexto['Texto']); ?></td>
                    </tr>
                     <tr>
                      <td height="20" valign="middle" style="color:#000; background-color:#FFF;" class="arial12black2">&nbsp;<?php $resultTexto=mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 478');
                   $rowTexto = mysql_fetch_array($resultTexto);
                  echo($rowTexto['Texto']); ?></td>
                    </tr>
                     <tr>
                      <td height="20" valign="middle" style="color:#000; background-color:#FFF;" class="arial12black2">&nbsp;<?php $resultTexto=mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 479');
                   $rowTexto = mysql_fetch_array($resultTexto);
                  echo($rowTexto['Texto']); ?></td>
                    </tr>
                     <tr>
                      <td height="20" valign="middle" style="color:#000; background-color:#FFF;" class="arial12black2">&nbsp;<?php $resultTexto=mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 480');
                   $rowTexto = mysql_fetch_array($resultTexto);
                  echo($rowTexto['Texto']); ?></td>
                    </tr>
                     <tr>
                      <td height="20" valign="middle" style="color:#000; background-color:#FFF;" class="arial12black2">&nbsp;<?php $resultTexto=mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 481');
                   $rowTexto = mysql_fetch_array($resultTexto);
                  echo($rowTexto['Texto']); ?></td>
                    </tr>
                     <tr>
                      <td height="20" valign="middle" style="color:#000; background-color:#FFF;" class="arial12black2">&nbsp;<?php $resultTexto=mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 482');
                   $rowTexto = mysql_fetch_array($resultTexto);
                  echo($rowTexto['Texto']); ?></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
               <td height="18">&nbsp;</td>
          	  </tr>
              <tr>
                <td height="18" valign="top" class="arialblack14"><?php $resultTexto=mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 470');
                   $rowTexto = mysql_fetch_array($resultTexto);
                  echo($rowTexto['Texto']); ?>&nbsp;<?php 
                        
                        echo($rowtabla['AnioAnterior']);?> </td>
              </tr>
                       
              <tr>
                <td height="282" valign="top">
                <table border="0" cellpadding="0" cellspacing="0" width="100%"  style="color:#FFF;">
                    <tr style="color:#000; background-color:#FFF;">
                       <td width="76" height="20" valign="top" class="arial12black2"><!--DWLayoutEmptyCell-->&nbsp;</td>
                      <td width="63" align="center" valign="middle" class="arial12black2"><?php $resultTexto=mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 460');
                   $rowTexto = mysql_fetch_array($resultTexto);
                  echo($rowTexto['Texto']); ?></td>
                       <td width="63" align="center" valign="middle" class="arial12black2"><?php $resultTexto=mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 462');
                   $rowTexto = mysql_fetch_array($resultTexto);
                  echo($rowTexto['Texto']); ?></td>
                       <td width="63" align="center" valign="middle" class="arial12black2"><?php $resultTexto=mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 461');
                   $rowTexto = mysql_fetch_array($resultTexto);
                  echo($rowTexto['Texto']); ?></td>
                       <td width="63" align="center" valign="middle" class="arial12black2">&nbsp;</td>
                       <td width="63" align="center" valign="middle" class="arial12black2"><!--DWLayoutEmptyCell-->&nbsp;</td>
                       <td width="65" align="center" valign="middle" class="arial12black2"><!--DWLayoutEmptyCell-->&nbsp;</td>
                    </tr>
                    <tr style="color:#000; background-color:#FFF;">
                      <td height="20">&nbsp;</td>
                      <td align="center" valign="middle" class="arial12black2">P1</td>
                      <td align="center" valign="middle"><span class="arial12black2">P2</span></td>
                      <td align="center" valign="middle" class="arial12black2">P3</td>
                      <td align="center" valign="middle"><span class="arial12black2">P4</span></td>
                      <td align="center" valign="middle"><span class="arial12black2">P5</span></td>
                      <td align="center" valign="middle"><span class="arial12black2">P6</span></td>
                    </tr>
                    <tr>
                      <td height="20" valign="middle" style="color:#000; background-color:#FFF;" class="arial12black2" >&nbsp;<?php $resultTexto=mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 471');
                   $rowTexto = mysql_fetch_array($resultTexto);
                  echo($rowTexto['Texto']); ?></td>
                      <td colspan="6" rowspan="12" valign="top"><table width="100%" border="1" cellpadding="0" cellspacing="0" style="background-color:#EBF0F5">
                                            <tr>
                          <td width="63" height="20" align="right" valign="middle" class="arial12Gris"><?php if(isset($rowtabla['EneroP1N']) && $rowtabla['EneroP1N']!=0){
                          echo(number_format($rowtabla['EneroP1N'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                          ?></td>
                      <td width="63" align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['EneroP2N']) && $rowtabla['EneroP2N']!=0){
                          echo(number_format($rowtabla['EneroP2N'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                     ?></td>
                      <td width="63" align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['EneroP3N']) && $rowtabla['EneroP3N']!=0){
                          echo(number_format($rowtabla['EneroP3N'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                      
                      ?></td>
                      <td width="63" align="right" valign="middle" class="arial12Gris"><?php 
                        if(isset($rowtabla['EneroP4N']) && $rowtabla['EneroP4N']!=0){
                          echo(number_format($rowtabla['EneroP4N'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                    ?></td>
                      <td width="63" align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['EneroP5N']) && $rowtabla['EneroP5N']!=0){
                          echo(number_format($rowtabla['EneroP5N'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
    
                      ?></td>
                      <td width="65" align="right" valign="middle" class="arial12Gris"><?php 
                        if(isset($rowtabla['EneroP6N']) && $rowtabla['EneroP6N']!=0){
                          echo(number_format($rowtabla['EneroP6N'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                      ?></td>
                    </tr>
                    <tr>
                          <td height="20" align="right" valign="middle" class="arial12Gris"><?php 
                          if(isset($rowtabla['FebreroP1N']) && $rowtabla['FebreroP1N']!=0){
                          echo(number_format($rowtabla['FebreroP1N'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                          ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                        if(isset($rowtabla['FebreroP2N']) && $rowtabla['FebreroP2N']!=0){
                          echo(number_format($rowtabla['FebreroP2N'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                      
                      ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['FebreroP3N']) && $rowtabla['FebreroP3N']!=0){
                          echo(number_format($rowtabla['FebreroP3N'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                      
                     ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                        if(isset($rowtabla['FebreroP4N']) && $rowtabla['FebreroP4N']!=0){
                          echo(number_format($rowtabla['FebreroP4N'],0,",","."));
    
                          }else{
                          echo('&nbsp;');
                          }
                      ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['FebreroP5N']) && $rowtabla['FebreroP5N']!=0){
                          echo(number_format($rowtabla['FebreroP5N'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                     ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                       if(isset($rowtabla['FebreroP5N']) && $rowtabla['FebreroP5N']!=0){
                          echo(number_format($rowtabla['FebreroP6N'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                      ?></td>
                    </tr>
                    <tr>
                          <td height="20" align="right" valign="middle" class="arial12Gris"><?php 
                           if(isset($rowtabla['MarzoP1N']) && $rowtabla['MarzoP1N']!=0){
                          echo(number_format($rowtabla['MarzoP1N'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                          ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                       if(isset($rowtabla['MarzoP2N']) && $rowtabla['MarzoP2N']!=0){
                          echo(number_format($rowtabla['MarzoP2N'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                     ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                       if(isset($rowtabla['MarzoP3N']) && $rowtabla['MarzoP3N']!=0){
                          echo(number_format($rowtabla['MarzoP3N'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                    ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                       if(isset($rowtabla['MarzoP4N']) && $rowtabla['MarzoP4N']!=0){
                          echo(number_format($rowtabla['MarzoP4N'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                      ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                       if(isset($rowtabla['MarzoP5N']) && $rowtabla['MarzoP5N']!=0){
                          echo(number_format($rowtabla['MarzoP5N'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                     ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                       if(isset($rowtabla['MarzoP6N']) && $rowtabla['MarzoP6N']!=0){
                          echo(number_format($rowtabla['MarzoP6N'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                     ?></td>
                    </tr>
                    <tr>
                          <td height="20" align="right" valign="middle" class="arial12Gris"><?php 
                          if(isset($rowtabla['AbrilP1N']) && $rowtabla['AbrilP1N']!=0){
                          echo(number_format($rowtabla['AbrilP1N'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                         ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['AbrilP2N']) && $rowtabla['AbrilP2N']!=0){
                          echo(number_format($rowtabla['AbrilP2N'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                      ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['AbrilP3N']) && $rowtabla['AbrilP3N']!=0){
                          echo(number_format($rowtabla['AbrilP3N'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                      
                      ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['AbrilP4N']) && $rowtabla['AbrilP4N']!=0){
                          echo(number_format($rowtabla['AbrilP4N'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                     ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['AbrilP5N']) && $rowtabla['AbrilP5N']!=0){
                          echo(number_format($rowtabla['AbrilP5N'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                      ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['AbrilP6N']) && $rowtabla['AbrilP6N']!=0){
                          echo(number_format($rowtabla['AbrilP6N'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                      ?></td>
                    </tr>
                    <tr>
                          <td height="20" align="right" valign="middle" class="arial12Gris"><?php 
                           if(isset($rowtabla['MayoP1N']) && $rowtabla['MayoP1N']!=0){
                          echo(number_format($rowtabla['MayoP1N'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                          ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                       if(isset($rowtabla['MayoP2N']) && $rowtabla['MayoP2N']!=0){
                          echo(number_format($rowtabla['MayoP2N'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                      ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                       if(isset($rowtabla['MayoP3N']) && $rowtabla['MayoP3N']!=0){
                          echo(number_format($rowtabla['MayoP3N'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                    ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                       if(isset($rowtabla['MayoP4N']) && $rowtabla['MayoP4N']!=0){
                          echo(number_format($rowtabla['MayoP4N'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                     ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                       if(isset($rowtabla['MayoP5N']) && $rowtabla['MayoP5N']!=0){
                          echo(number_format($rowtabla['MayoP5N'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                     ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                       if(isset($rowtabla['MayoP6N']) && $rowtabla['MayoP6N']!=0){
                          echo(number_format($rowtabla['MayoP6N'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                      ?></td>
                    </tr>
                    <tr>
                          <td height="20" align="right" valign="middle" class="arial12Gris"><?php 
                           if(isset($rowtabla['JunioP1N']) && $rowtabla['JunioP1N']!=0){
                          echo(number_format($rowtabla['JunioP1N'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                         ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                       if(isset($rowtabla['JunioP2N']) && $rowtabla['JunioP2N']!=0){
                          echo(number_format($rowtabla['JunioP2N'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                      ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                       if(isset($rowtabla['JunioP3N']) && $rowtabla['JunioP3N']!=0){
                          echo(number_format($rowtabla['JunioP3N'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                     ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                       if(isset($rowtabla['JunioP4N']) && $rowtabla['JunioP4N']!=0){
                          echo(number_format($rowtabla['JunioP4N'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                      ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                       if(isset($rowtabla['JunioP5N']) && $rowtabla['JunioP5N']!=0){
                          echo(number_format($rowtabla['JunioP5N'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                    ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php
                       if(isset($rowtabla['JunioP6N']) && $rowtabla['JunioP6N']!=0){
                          echo(number_format($rowtabla['JunioP6N'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                     ?></td>
                    </tr>
                    <tr>
                          <td height="20" align="right" valign="middle" class="arial12Gris"><?php 
                          if(isset($rowtabla['JulioP1N']) && $rowtabla['JulioP1N']!=0){
                          echo(number_format($rowtabla['JulioP1N'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                          ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['JulioP2N']) && $rowtabla['JulioP2N']!=0){
                          echo(number_format($rowtabla['JulioP2N'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                     ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['JulioP3N']) && $rowtabla['JulioP3N']!=0){
                          echo(number_format($rowtabla['JulioP3N'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                     ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['JulioP4N']) && $rowtabla['JulioP4N']!=0){
                          echo(number_format($rowtabla['JulioP4N'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                     ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['JulioP5N']) && $rowtabla['JulioP5N']!=0){
                          echo(number_format($rowtabla['JulioP5N'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                      ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['JulioP6N']) && $rowtabla['JulioP6N']!=0){
                          echo(number_format($rowtabla['JulioP6N'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                      ?></td>
                    </tr>
                    <tr>
                          <td height="20" align="right" valign="middle" class="arial12Gris"><?php
                          if(isset($rowtabla['AgostoP1N']) && $rowtabla['AgostoP1N']!=0){
                          echo(number_format($rowtabla['AgostoP1N'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                           ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['AgostoP2N']) && $rowtabla['AgostoP2N']!=0){
                          echo(number_format($rowtabla['AgostoP2N'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                     ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['AgostoP3N']) && $rowtabla['AgostoP3N']!=0){
                          echo(number_format($rowtabla['AgostoP3N'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                     ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['AgostoP4N']) && $rowtabla['AgostoP4N']!=0){
                          echo(number_format($rowtabla['AgostoP4N'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                      ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['AgostoP5N']) && $rowtabla['AgostoP5N']!=0){
                          echo(number_format($rowtabla['AgostoP5N'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                      ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['AgostoP6N']) && $rowtabla['AgostoP6N']!=0){
                          echo(number_format($rowtabla['AgostoP6N'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                     ?></td>
                    </tr>
                    <tr>
                          <td height="20" align="right" valign="middle" class="arial12Gris"><?php 
                          if(isset($rowtabla['SeptiembreP1N']) && $rowtabla['SeptiembreP1N']!=0){
                          echo(number_format($rowtabla['SeptiembreP1N'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                         ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['SeptiembreP2N']) && $rowtabla['SeptiembreP2N']!=0){
                          echo(number_format($rowtabla['SeptiembreP2N'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                     ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['SeptiembreP3N']) && $rowtabla['SeptiembreP3N']!=0){
                          echo(number_format($rowtabla['SeptiembreP3N'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                      ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['SeptiembreP4N']) && $rowtabla['SeptiembreP4N']!=0){
                          echo(number_format($rowtabla['SeptiembreP4N'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                      ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['SeptiembreP5N']) && $rowtabla['SeptiembreP5N']!=0){
                          echo(number_format($rowtabla['SeptiembreP5N'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                     ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['SeptiembreP6N']) && $rowtabla['SeptiembreP6N']!=0){
                          echo(number_format($rowtabla['SeptiembreP6N'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                     ?></td>
                    </tr>
                    <tr>
                          <td height="20" align="right" valign="middle" class="arial12Gris"><?php 
                          if(isset($rowtabla['OctubreP1N']) && $rowtabla['OctubreP1N']!=0){
                          echo(number_format($rowtabla['OctubreP1N'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                          ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['OctubreP2N']) && $rowtabla['OctubreP2N']!=0){
                          echo(number_format($rowtabla['OctubreP2N'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                     ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['OctubreP3N']) && $rowtabla['OctubreP3N']!=0){
                          echo(number_format($rowtabla['OctubreP3N'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                     ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['OctubreP4N']) && $rowtabla['OctubreP4N']!=0){
                          echo(number_format($rowtabla['OctubreP4N'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                     ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['OctubreP5N']) && $rowtabla['OctubreP5N']!=0){
                          echo(number_format($rowtabla['OctubreP5N'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                     ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['OctubreP6N']) && $rowtabla['OctubreP6N']!=0){
                          echo(number_format($rowtabla['OctubreP6N'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                      ?></td>
                    </tr>
                    <tr>
                          <td height="20" align="right" valign="middle" class="arial12Gris"><?php 
                          
                          if(isset($rowtabla['NoviembreP1N']) && $rowtabla['NoviembreP1N']!=0){
                          echo(number_format($rowtabla['NoviembreP1N'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                          ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['NoviembreP2N']) && $rowtabla['NoviembreP2N']!=0){
                          echo(number_format($rowtabla['NoviembreP2N'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                      ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['NoviembreP3N']) && $rowtabla['NoviembreP3N']!=0){
                          echo(number_format($rowtabla['NoviembreP3N'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                     ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php
                      if(isset($rowtabla['NoviembreP4N']) && $rowtabla['NoviembreP4N']!=0){
                          echo(number_format($rowtabla['NoviembreP4N'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                       ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['NoviembreP5N']) && $rowtabla['NoviembreP5N']!=0){
                          echo(number_format($rowtabla['NoviembreP5N'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                      ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                      if(isset($rowtabla['NoviembreP6N']) && $rowtabla['NoviembreP6N']!=0){
                          echo(number_format($rowtabla['NoviembreP6N'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                      ?></td>
                    </tr>
                    <tr>
                          <td height="20" align="right" valign="middle" class="arial12Gris"><?php
                           if(isset($rowtabla['DiciembreP1N']) && $rowtabla['DiciembreP1N']!=0){
                          echo(number_format($rowtabla['DiciembreP1N'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                        ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php
                       if(isset($rowtabla['DiciembreP2N']) && $rowtabla['DiciembreP2N']!=0){
                          echo(number_format($rowtabla['DiciembreP2N'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                       ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php
                       if(isset($rowtabla['DiciembreP3N']) && $rowtabla['DiciembreP3N']!=0){
                          echo(number_format($rowtabla['DiciembreP3N'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                       ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                       if(isset($rowtabla['DiciembreP4N']) && $rowtabla['DiciembreP4N']!=0){
                          echo(number_format($rowtabla['DiciembreP4N'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                     ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                       if(isset($rowtabla['DiciembreP5N']) && $rowtabla['DiciembreP5N']!=0){
                          echo(number_format($rowtabla['DiciembreP5N'],0,",","."));

                          }else{
                          echo('&nbsp;');
                          }
                      ?></td>
                      <td align="right" valign="middle" class="arial12Gris"><?php 
                       if(isset($rowtabla['DiciembreP6N']) && $rowtabla['DiciembreP6N']!=0){
                          echo(number_format($rowtabla['DiciembreP6N'],0,",","."));
                          }else{
                          echo('&nbsp;');
                          }
                     ?></td>
                    </tr>
                  </table>		          
               </td>
              </tr>
              <tr>
                      <td height="20" valign="middle" style="color:#000; background-color:#FFF;" class="arial12black2">&nbsp;<?php $resultTexto=mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 472');
                   $rowTexto = mysql_fetch_array($resultTexto);
                  echo($rowTexto['Texto']); ?></td>
                    </tr>
              <tr>
                      <td height="20" valign="middle" style="color:#000; background-color:#FFF;" class="arial12black2">&nbsp;<?php $resultTexto=mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 473');
                   $rowTexto = mysql_fetch_array($resultTexto);
                  echo($rowTexto['Texto']); ?></td>
                    </tr>
              <tr>
                      <td height="20" valign="middle" style="color:#000"; background-color:#FFF; class="arial12black2" >&nbsp;<?php $resultTexto=mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 474');
                   $rowTexto = mysql_fetch_array($resultTexto);
                  echo($rowTexto['Texto']); ?></td>
                    </tr>
              <tr>
                      <td height="20" valign="middle" style="color:#000; background-color:#FFF;" class="arial12black2" >&nbsp;<?php $resultTexto=mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 475');
                   $rowTexto = mysql_fetch_array($resultTexto);
                  echo($rowTexto['Texto']); ?></td>
                    </tr>
              <tr>
                      <td height="20" valign="middle" style="color:#000; background-color:#FFF;" class="arial12black2"  >&nbsp;<?php $resultTexto=mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 476');
                   $rowTexto = mysql_fetch_array($resultTexto);
                  echo($rowTexto['Texto']); ?></td>
                    </tr>
              <tr>
                      <td height="20" valign="middle" style="color:#000; background-color:#FFF;" class="arial12black2">&nbsp;<?php $resultTexto=mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 477');
                   $rowTexto = mysql_fetch_array($resultTexto);
                  echo($rowTexto['Texto']); ?></td>
                    </tr>
              <tr>
                      <td height="20" valign="middle" style="color:#000; background-color:#FFF;" class="arial12black2" >&nbsp;<?php $resultTexto=mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 478');
                   $rowTexto = mysql_fetch_array($resultTexto);
                  echo($rowTexto['Texto']); ?></td>
                    </tr>
              <tr>
                      <td height="20" valign="middle" style="color:#000; background-color:#FFF;" class="arial12black2" >&nbsp;<?php $resultTexto=mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 479');
                   $rowTexto = mysql_fetch_array($resultTexto);
                  echo($rowTexto['Texto']); ?></td>
                    </tr>
              <tr>
                      <td height="20" valign="middle" style="color:#000; background-color:#FFF;" class="arial12black2" >&nbsp;<?php $resultTexto=mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 480');
                   $rowTexto = mysql_fetch_array($resultTexto);
                  echo($rowTexto['Texto']); ?></td>
                    </tr>
              <tr>
                      <td height="20" valign="middle" style="color:#000; background-color:#FFF;" class="arial12black2" >&nbsp;<?php $resultTexto=mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 481');
                   $rowTexto = mysql_fetch_array($resultTexto);
                  echo($rowTexto['Texto']); ?></td>
                    </tr>
              <tr>
                      <td height="20" valign="middle" style="color:#000; background-color:#FFF;" class="arial12black2" >&nbsp;<?php $resultTexto=mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 482');
                   $rowTexto = mysql_fetch_array($resultTexto);
                  echo($rowTexto['Texto']); ?></td>
                    </tr>
              </table>
<!--VERSION VIEJA-->
          </td></tr></table>
          </div>
          </div><!--<div class="contenido_ajustado">--> 
                	
		</div><!--<div class="contenido_seccion_oficina">-->                        
		
        
        <div class="limpiar"></div>
                        	
<!--Por ultimo aparecera el pie de la web-->
<?php 
//	include($_SESSION["directorio_raiz"]."includes/pie.php");


}//else ($_SESSION['usuario'])
		
?>        
    
	<?php
    include("../includes/pie.php");
    ?>
    </div><!--<div id="central_oficina"-->
    </body>
</html>

