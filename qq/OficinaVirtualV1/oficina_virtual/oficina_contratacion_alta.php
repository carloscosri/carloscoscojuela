<?php
/* NOTA:

 COMO NO SE CIERRA LA CONEXION TRAS CADA CONEXION CON LA BASE DE DATOS, LAS CONSULTAS DEJAN DE FUNCIONAR SI SE ESTABLECE COMO UNICA CONEXION
 LA QUE UTILIZA EL RESTO DE LA WEB, POR LO TANTO HASTA QUE SE HAGA LA MIGRACION COMPLETA DE LOS TEXTOS, COEXISTIRAN LAS DOS CONEXIONES, YA QUE
 SE HA AÑADIDO UNA LLAMADA A LA CONEXION BUENA EN ESTE ARCHIVO.
*/

include("_conexion.php");

//Se incluye el archivo que contiene las verificaciones de datos
include("../includes/verificaciones.php");

//Se incluye el archivo con los textos especifico de esta seccion en el idioma oportuno
//include($_SESSION["directorio_raiz"]."idiomas/".$_SESSION["idioma"]."/oficina_contratacion_alta.php");

//LALALA Como el formulario es similar el de la seccion de datos del titular, añadimos su archivo de textos a la seccion, para no duplicarlos
//include($_SESSION["directorio_raiz"]."idiomas/".$_SESSION["idioma"]."/oficina_datos_titular.php");

//include($_SESSION["directorio_raiz"]."idiomas/".$_SESSION["idioma"]."/oficina_contratacion_alta.php");

include("../idiomas/".$_SESSION["idioma"]."/oficina_datos_titular.php");

include("../idiomas/".$_SESSION["idioma"]."/oficina_contratacion_alta.php");

//Esta variable controlara si la contratacion la esta haciendo un usuario sin registrar en la web (0) o un usuario registrado (1), se empleara para determinar el tipo de insert que hace el archivo "comprobaciones_inserccion_contratacion.php"
$_SESSION["contratacion_sin_registro"]=0;
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<title><?=$titulo_web;?></title>

<!--Se añaden las hojas de estilos genericas que utiliza esta seccion-->
<link href="../css/estilos.css" rel="stylesheet" type="text/css" />
<link href="../css/secciones.css" rel="stylesheet" type="text/css" />
<link href="css/oficina.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_vieja.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_contratacion_alta.css" rel="stylesheet" type="text/css" />

<!--Ahora se añaden las reglas para Internet Explorer. Asi corregiremos las diferencias de visualizacion entre navegadores
Es importante añadirlo despues de las reglas principales, para que se sobreescriban las correcciones pertinentes. En este caso las diferencias
solo son para internet explorer 7-->

<!--[if IE 7]>
	<link href="../css/estilos_ie7.css" rel="stylesheet" type="text/css" />
	<link href="../css/secciones_ie7.css" rel="stylesheet" type="text/css" />    
<![endif]-->


<!--Añadimos el script que define el comportamiento del apartado Datos Tipo Suministo-->
<script src="scripts/comportamiento_datos_tipo_suministro.js" type="text/javascript"></script>

<!--Añadimos el script que define el comportamiento de las altas de contratacion, tanto de las de oficina virtual como las de los usuarios no registrados-->
<script src="scripts/comportamiento_altas_contratacion.js" type="text/javascript"></script>

<?php
	include("scripts/verificaciones_contratacion.php");
?>

</head>
	
<body>
<!--*************************************************Web realizada por ****************************************-->
	<div id="central_oficina">    
<?php
//Se comprueba que el usuario se haya identificado, si no es asi se da un aviso y redirigimos a la seccion de identificacion
if ($_SESSION['usuario']=="")
{	
	MsgBox($error_identificacion);
	redirigir("oficina_index.php?id=".$_SESSION["idioma"]);
	
}//if ($_SESSION['usuario'])

//Si el usuario se ha identificado la seccion se ejecutara con normalidad
else
{

//Capturaremos la variable que nos indica desde que enlace se ha llegado a la seccion. De esta manera mostraremos un titulo u otro y se producirán los demás cambios de comportamiento

//0->10 Kw/h
//1->10 a 15 Kw/h
//2-> >15 Kw/h

if($_GET["s"]!="")
{
	$_SESSION["seccion_origen_contratacion"]=$_GET["s"];
}//if($_GET["s"]!="")

//Añadimos los fragmentos comunes, la cabecera, el menu, y el submenu
	include("oficina_fragmentos_comunes.php");
?>                    
		<div class="contenido_seccion_oficina_contratacion_alta">        
			<div class="titulo_seccion_oficina_contratacion_alta arialblack18Titulo">
<?php            
//Dependiendo desde que enlace se haya accedido mostraremos un titulo u otro.
//Además las tarifas a contratar que se muestran seran diferentes, se almacenaran en array_tarifas_disponibles
//También variaran las potencias normalizadas disponibles, se almacenaran los identificadores de las potencias a mostrar en array_tarifas en la segunda posicion

				switch($_SESSION["seccion_origen_contratacion"])	
				{
//10 kw/h				
					case 0:
						echo($oficina_contratacion_alta1);
						$array_tarifas_disponibles[0]=20;									
						$array_tarifas_disponibles[1]=21;															
					break;

//10-15 kw/h					
					case 1:
						echo($oficina_contratacion_alta2);																											
						$array_tarifas_disponibles[0]=210;									
						$array_tarifas_disponibles[1]=211;															
					break;
									
//>15 kw/h. EN ESTE CASO TAMBIEN SE ESTABLECERAN OTROS VALORES POR DEFECTO

					case 2:
						echo($oficina_contratacion_alta3);									
						$array_tarifas_disponibles[0]=30;									
						
/*NOTA: DE MOMENTO NO SE TIENEN EN CUENTA ESTAS TARIFAS						
						$array_tarifas_disponibles[1]=31;																																											
						$array_tarifas_disponibles[2]=61;
*/																		
					break;

//LOLOLO POSIBLE CAMBIO FUNCIOMIENTO BONOSOCIAL
					case 4:
						echo($oficina_contratacion_alta10);
						$array_tarifas_disponibles[0]=10;			
					break;
				}//switch($_SESSION["seccion_origen_contratacion"])	
?>                
            </div><!--<div class="titulo_seccion_oficina_contratacion_alta arialblack18Titulo">-->
            
            <div class="texto_seccion_oficina_contratacion_alta arial12">
				<?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 670'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
            </div>
            
            <div class="nota_campos_obligatorios"><?=$nota_campos_obligatorios?></div>
                     
<!--VERSION VIEJA-->          

			<form action="oficina_contratacion_alta.php?id=<?=$_SESSION["idioma"].$ampersand?>s=<?=$_SESSION["seccion_origen_contratacion"]?>" method="post" name="form_alta" id="form_alta" onsubmit="habilitar_campos();">
            
<?php
				include("scripts/formulario_contratacion.php");
?>	

			</form>
<!--LOLOLO-->              
                                                                    
<!--VERSION VIEJA-->



            <div class="volver_seccion_oficina_contratacion_alta"><a href="oficina_contratacion.php?id=<?=$_SESSION["idioma"]?>" class="enlace"><?=$oficina_contratacion_alta6?></a></div>                    	
        	
		</div><!--<div class="contenido_seccion_oficina">-->                        
		        
        <div class="limpiar"></div>
        
<!--Por ultimo aparecera el pie de la web-->
<?php 
//	include($_SESSION["directorio_raiz"]."includes/pie.php");
	include("../includes/pie.php");
        
}//else ($_SESSION['usuario'])

include("scripts/comprobaciones_inserccion_contratacion.php");
	
?>
    </div><!--<div id="central_oficina"-->
</body>
</html>
