<?php
/* NOTA:

 COMO NO SE CIERRA LA CONEXION TRAS CADA CONEXION CON LA BASE DE DATOS, LAS CONSULTAS DEJAN DE FUNCIONAR SI SE ESTABLECE COMO UNICA CONEXION
 LA QUE UTILIZA EL RESTO DE LA WEB, POR LO TANTO HASTA QUE SE HAGA LA MIGRACION COMPLETA DE LOS TEXTOS, COEXISTIRAN LAS DOS CONEXIONES, YA QUE
 SE HA A�ADIDO UNA LLAMADA A LA CONEXION BUENA EN ESTE ARCHIVO.
*/

include("_conexion.php");
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title><?=$titulo_web;?></title>
<!--CSS-->
<link rel="stylesheet" href="css/estilos.css">
<script src="external/jquery-1.8.0.min.js" type="text/javascript"></script>
<script src="external/jquery-ui-1.8.23.custom.min.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" media="all" href="scripts/calendar/skins/aqua/theme.css" title="Aqua" />
<script type="text/javascript" src="scripts/calendar/calendar.js"></script>

<!-- librer�a para cargar el lenguaje deseado -->
<script type="text/javascript" src="scripts/calendar/lang/calendar-es.js"></script>

<!-- librer�a que declara la funci�n Calendar.setup, que ayuda a generar un calendario en unas pocas l�neas de c�digo -->
<script type="text/javascript" src="scripts/calendar/calendar-setup.js"></script>


    
     <script type="text/javascript" src="https://www.google.com/jsapi"></script>
      
 <style>
	 input{height:inherit}
	 </style>
</head>

<body>
<div id="contenido">
<?php
//Se comprueba que el usuario se haya identificado, si no es asi se da un aviso y redirigimos a la seccion de identificacion
if ($_SESSION['usuario']=="")
{	
	MsgBox($error_identificacion);
	redirigir("graficos_index.php");
	
}//if ($_SESSION['usuario'])

//Si el usuario se ha identificado la seccion se ejecutara con normalidad
else
{
?>    
	<div id="cabecera">
    	<a href="graficos_index.php" id="logo"><img src="img/logo.png" style="margin: 12px;"></a>
         <div id="enlacesMenu">
          <div class="menu_oficina"><?php include("menu_oficina.php");?></div>
        </div>  
    </div>
    <div id="menu" style="min-height:500px;">
 	<p style="font-size: 14px;font-weight: bold;height: 20px;;padding:5px;margin: 0px;background-color: #C0C1C3;">Tipos de gr&aacute;ficos</p>
    <ul class="menu">
        <li class="item1"><a href="graficos_consumos.php">Consumos El&eacute;ctricos</a></li>
        <li class="item2" ><a href="graficos_curvas.php">Curvas de Carga</a></li>
        <li class="item3"><a href="graficos_importes.php">Importes</a></li>
        <?php /*?><li class="item4"><a href="graficos_maximetros.php">Max&iacute;metro</a></li><?php */?>
        <li class="item5"><a href="graficos_kwh.php">�/KWH</a></li>
    </ul>
 
</div>
    <div id="cuerpo">
    	<section class="ib-container" id="ib-container" style="text-align:center">


<script src="js/highcharts.js"></script>
<script src="js/highcharts-3d.js"></script>
<script src="js/modules/exporting.js"></script>
<script src="js/modules/data.js"></script>
<script src="js/modules/drilldown.js"></script>

	<div class="contenido_ajustado"> 
    	
    <div style="text-align:center">
 
   
    <div class="limpiar"></div>
    
    
   
    </div>
</div>
<?php
}
	include ('includes/footer.php');
?>
</div></div>

<div class="limpiar"></div>

</body>
</html>   
