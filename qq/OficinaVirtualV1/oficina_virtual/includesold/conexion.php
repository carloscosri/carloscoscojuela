<?php


/********************************************FUNCIONES DE CONEXION A LA BASE DE DATOS Y VARIABLES GENERICAS*****************************************/
/************************************************************************************************************************************************/

//Se incluye el archivo que contiene todas las variables de ambito global de la web
if($_SESSION['oficina_virtual']!=1)
{		
	include($_SESSION["directorio_raiz"]."includes/variables.php");
}//if($_SESSION['oficina_virtual']!=1)
else
{
	include("../includes/variables.php");
}//else($_SESSION['oficina_virtual']!=1)

//MUESTRA TODAS LAS VARIABLES DE SERVIDOR - PARA DEPURACION
/*
	echo "<pre>";
		print_r($_SERVER);
	echo "</pre>";
*/

//Se captura de la URL el idioma que esta activo en cada momento
$_SESSION["idioma"]=$_GET["id"];

//El idioma por defecto sera el catalan	
if ($_SESSION["idioma"] == "")
{
	$_SESSION["idioma"]=$idioma_defecto;
}//if ($_SESSION["idioma"] == "")	


//************************************************************************SISTEMA DE IDIOMAS VIEJO**********************************************

if($_SESSION['idioma']=="es")
{
	$_SESSION['tablaIdioma'] = "palabras";
	$_SESSION['noticiaIdioma']="Noticias";
}//if($_SESSION['idioma']=="es")

else if($_SESSION['idioma']=="en")
{
	$_SESSION['tablaIdioma'] = "palabras_en";
	$_SESSION['noticiaIdioma']="Noticias_en";
}//else if($_SESSION['idioma']=="en")

else if($_SESSION['idioma']=="ca")
{
	$_SESSION['tablaIdioma'] = "palabras_cat";
	$_SESSION['noticiaIdioma']="Noticias_cat";
}//else if($_SESSION['idioma']=="ca")

else if($_SESSION['idioma']=="ga")
{
	$_SESSION['tablaIdioma'] = "palabras_gal";
	$_SESSION['noticiaIdioma']="Noticias_gal";
}//else if($_SESSION['idioma']=="ga")

else if($_SESSION['idioma']=='eu')
{
	$_SESSION['tablaIdioma']="palabras_eus";
	$_SESSION['noticiaIdioma']="Noticias_eus";
}//else if($_SESSION['idioma']=='eu')

//Se establece como idioma por defecto el espa�ol
else
{
	$_SESSION['tablaIdioma'] = "palabras";
	$_SESSION['noticiaIdioma']="Noticias";
}//else

//************************************************************************SISTEMA DE IDIOMAS VIEJO**********************************************

//Ahora que tenemos el idioma activo, se incluye el archivo con los textos que contienen todas las secciones, en el idioma oportuno
//Se incluye el archivo que contiene todas las variables de ambito global de la web
if($_SESSION['oficina_virtual']!=1)
{		
	include($_SESSION["directorio_raiz"]."idiomas/".$_SESSION["idioma"]."/comun.php");
}//if($_SESSION['oficina_virtual']!=1)
else
{
	include("../idiomas/".$_SESSION["idioma"]."/comun.php");
}//else($_SESSION['oficina_virtual']!=1)
		
//Esta funcion conecta con una base de datos mySQL
function conectar($servidor,$usuario,$contrase�a,$nombre_bd)
{		
   if (!($db=mysql_connect($servidor,$usuario,$contrase�a)))
   {
//Dependiendo del idioma activo, se mostrara un texto u otro en el caso de un error de conexion.
//ESTOS TEXTOS SE INCLUYEN AQUI, DEBIDO A QUE SI NO HABRIA QUE PASAR COMO PARAMETRO EL TEXTO DEL ERROR SIEMPRE QUE SE CONECTARA A LA BASE DE DATOS
	switch ($_SESSION["idioma"])
	{
		case "es":
			$error_bd_conexion="No se puede conectar a la base de datos: ";
		break;
		
		case "ca":
			$error_bd_conexion="No es pot connectar a la base de dades: ";
		break;		
		
		case "eu":
			$error_bd_conexion="Ez konektatu ahal datu-baseari: ";
		break;		
		
		case "ga":
			$error_bd_conexion="Non lle pode conectar � base de datos: ";
		break;	

		case "en":
			$error_bd_conexion="Can�t connect to the database: ";
		break;									
	}//switch ($_SESSION["idioma"])
	
      die($error_bd_conexion.mysql_error());
      exit();
   }//if (!($db=mysql_connect($ruta_sevidor_bd,$usuario_bd,$contrasena_bd)))   
      
   if (!mysql_select_db($nombre_bd,$db))
   {
//LALALA A�ADIR MENSAJES DE ERROR A LA BASE DE DATOS DE IDIOMAS!!!!!!!!!!!!!!!!!!!!!!!   
      die ($error_bd_conexion.mysql_error());
      exit();
   }//if (!mysql_select_db($usuario_bd,$db))
   $db_selected = mysql_select_db($nombre_bd, $db);
   return $db;
}//function conectar()

//Esta funcion selecciona todos los registros de una base de datos mySQL de la tabla que recibe como parametro:
//1-El nombre de la tabla
//2-El nombre del campo por el que se desee ordenar
//3- Si el orden es ascendente(0) o descendente (1)
//4-Nombre del servidor
//5-Nombre usuario de la base de datos con priviligeios
//6-Clave del usuario de la base de datos con privilegios
//7-Nombre de la base de datos a la que se desea conectar

function seleccion ($tabla,$orden,$ascendente,$servidor,$usuario,$contrase�a,$nombre_bd)
{
	$con=conectar($servidor,$usuario,$contrase�a,$nombre_bd);
	
//Dependiendo del parametro recibido los registros devueltos se ordenaran ascendentemente o descendentemente	
	if($ascendente!=0)
	{
		$orden_ascendente="DESC";	
	}//if($ascendente!=0)
	else
	{
		$orden_ascendente="ASC";
	}//if($ascendente!=0)
	
	$result=mysql_query("SELECT * FROM $tabla ORDER BY $orden $orden_ascendente",$con);		
	//CerrarConexion();
	return $result;
}//function seleccion ($tabla,$orden,$ascendente,$servidor,$usuario,$contrase�a,$nombre_bd)

//Esta funcion selecciona todos los registros de una base de datos mySQL de la tabla que recibe como parametro y con la sentencia "WHERE" que tambien recibe como parametro:
//1-El nombre de la tabla
//2-Contenido de la clausura WHERE de la consulta a realizar
//3-El nombre del campo por el que se desee ordenar
//4- Si el orden es ascendente(0) o descendente (1)
//5-Nombre del servidor
//6-Nombre usuario de la base de datos con priviligeios
//7-Clave del usuario de la base de datos con privilegios
//8-Nombre de la base de datos a la que se desea conectar

function seleccion_condicional ($tabla, $condicion,$orden,$ascendente,$servidor,$usuario,$contrase�a,$nombre_bd)
{
	$con=conectar($servidor,$usuario,$contrase�a,$nombre_bd);
//Dependiendo del parametro recibido los registros devueltos se ordenaran ascendentemente o descendentemente	
	if($ascendente!=0)
	{
		$orden_ascendente="DESC";		
	}//if($ascendente!=0)
	else
	{
		$orden_ascendente="ASC";
	}//if($ascendente!=0)
	
	$result=mysql_query("SELECT * FROM $tabla WHERE $condicion ORDER BY $orden $orden_ascendente",$con);
			
	CerrarConexion();
	return $result;
}//function seleccion_condicional ($tabla, $condicion,$orden,$ascendente,$servidor,$usuario,$contrase�a,$nombre_bd)


//Esta funcion selecciona todos los registros de una base de datos mySQL de la tabla que recibe como parametro y con la sentencia "WHERE" que tambien recibe como parametro. Aunque en esta ocasion solo con los campos que recibe como parametro:
//1-El nombre de la tabla
//2-Campos de la tabla que se quieren consultar
//3-Contenido de la clausura WHERE de la consulta a realizar
//4-El nombre del campo por el que se desee ordenar
//5- Si el orden es ascendente(0) o descendente (1)
//6-Nombre del servidor
//7-Nombre usuario de la base de datos con priviligeios
//8-Clave del usuario de la base de datos con privilegios
//9-Nombre de la base de datos a la que se desea conectar

function seleccion_condicional_campos($tabla, $campos, $condicion,$orden,$ascendente,$servidor,$usuario,$contrase�a,$nombre_bd)
{
	$con=conectar($servidor,$usuario,$contrase�a,$nombre_bd);
	
//Dependiendo del parametro recibido los registros devueltos se ordenaran ascendentemente o descendentemente	
	if($ascendente!=0)
	{
		$orden_ascendente="DESC";		
	}//if($ascendente!=0)
	else
	{
		$orden_ascendente="ASC";
	}//if($ascendente!=0)
	
	$result=mysql_query("SELECT ".$campos." FROM $tabla WHERE $condicion ORDER BY $orden $orden_ascendente",$con);			
	CerrarConexion();
	return $result;
}//function seleccion_condicional_campos($tabla, $campos, $condicion,$orden,$ascendente,$servidor,$usuario,$contrase�a,$nombre_bd)

//Esta funcion selecciona un numero determinado de registros, que recibe como parametro de una base de datos mySQL de la tabla que recibe como parametro:

//1-El nombre de la tabla
//2-Limite de registros que se desea que la consulta devuelva
//3-Nombre del servidor
//4-Nombre usuario de la base de datos con priviligeios
//5-Clave del usuario de la base de datos con privilegios
//6-Nombre de la base de datos a la que se desea conectar

//LALALA SE UTILIZA???
function SeleccionLimit ($tabla, $limite,$servidor,$usuario,$contrase�a,$nombre_bd)
{
	$con=conectar($servidor,$usuario,$contrase�a,$nombre_bd);	
	$result=mysql_query("SELECT * FROM $tabla LIMIT $limite",$con);
	CerrarConexion();
	return $result;
}//function SeleccionLimit ($tabla, $limite)

//Esta funcion selecciona un registro por cada valor que se repite del campo recibido como parametro de una base de datos mySQL de la tabla que recibe como parametro:

//1-El nombre de la tabla
//2-Campo del que se quieren conocer los distintos valores
//3-El nombre del campo por el que se desee ordenar
//4- Si el orden es ascendente(0) o descendente (1)
//5-Nombre del servidor
//6-Nombre usuario de la base de datos con priviligeios
//7-Clave del usuario de la base de datos con privilegios
//8-Nombre de la base de datos a la que se desea conectar

function seleccion_unica ($tabla,$campo,$orden,$ascendente,$servidor,$usuario,$contrase�a,$nombre_bd)
{
	$con=conectar($servidor,$usuario,$contrase�a,$nombre_bd);
//Dependiendo del parametro recibido los registros devueltos se ordenaran ascendentemente o descendentemente	
	if($ascendente!=0)
	{
		$orden_ascendente="DESC";	
	}//if($ascendente!=0)
	else
	{
		$orden_ascendente="ASC";
	}//if($ascendente!=0)
	
	$result=mysql_query("SELECT DISTINCT($campo) FROM $tabla ORDER BY $orden $orden_ascendente",$con);		
	CerrarConexion();
	return $result;
}//function Seleccion ($tabla)

//Esta funcion cierra la conexion con la base de datos
function CerrarConexion()
{
	mysql_close();
}//function CerrarConexion($db)

//Esta funcion convierte una fecha en formato mySQL en formato normal para mostrar por pantalla
function fecha_normal($fecha)
{
    list($anio,$mes,$dia)=explode("-",$fecha);
    return $dia."-".$mes."-".$anio;
}//function fecha_normal($fecha)  

//Esta funcion devuelve el nombre del script en ejecucion. Es decir la ultima componente de la URL, sin extension.
//Por ejeplo si se ejecuta el script "idiomas/exportar_idiomas.php" se devolver�a "exportar_idiomas"
function nombre_script_en_ejecucion()
{	
/*	
LALALA PRUEBAS PARA DEFINIR LA CARPETA EN EL SERVIDOR DEL CLIENTE!!!!!!	
	echo("SERVER['SCRIPT_FILENAME']: ".$_SERVER['SCRIPT_FILENAME']."<br />");
*/
	
//Esta variable almacena la ruta completa del script que se esta ejecutando actualmente
	//echo($_SERVER['SCRIPT_FILENAME']);
	$ruta_origen=explode("/",$_SERVER['SCRIPT_FILENAME']);	
	//echo(sizeof($ruta_origen));	
	//echo($ruta_origen[0]."_1_".$ruta_origen[1]."_2_".$ruta_origen[2]."_".$ruta_origen[3]."_".$ruta_origen[4]."_".$ruta_origen[5]."_5_".$ruta_origen[6]."_6_".$ruta_origen[7]."_".$ruta_origen[8]."_".$ruta_origen[9]);
//Como la longitud del array "$ruta_origen" se deja de contar en la carpeta donde se alojan los archivos html, hay que sumarle
//el numero de carpetas que nos faltan para llegar a la ruta del archivo actual, si no los enlaces no funcionaran

//Para no tener que preocuparnos por que la ruta sea correcta cuando la pagina se ejecute desde una ubicacion u otra, hacemos condicional el numero de carpetas que hay que sumarle al array Ruta de origen, para que nos de siempre el nombre del script en ejecucion
	switch($_SERVER["HTTP_HOST"])
	{
//Si el servidor es localhost (Ejecucion en servidor local)
//		case "localhost":		
//			$num_carpetas_hasta_raiz_web=1;		
//		break;
//Si el servidor es el de AUDINFOR
//		case "www.audinforsystem.es":

//Si se accede desde la oficina virtual, al estar en un subdirectorio, habra que sumarle uno a las carpetas		
//			if($_SESSION['oficina_virtual']!=1)
//			{		
//				$num_carpetas_hasta_raiz_web=5;						
//			}//if($_SESSION['oficina_virtual']!=1)
//			else
//			{
//				$num_carpetas_hasta_raiz_web=6;									
//			}//else($_SESSION['oficina_virtual']!=1)
//		break;
	
		default:
//LALALA CAMBIAR DESPUES DE COMPROBACION EN SERVDOR DEL CLIENTE		
			if($_SESSION['oficina_virtual']!=1)
			{	
				$num_carpetas_hasta_raiz_web=1;
			}//if($_SESSION['oficina_virtual']!=1)
			else
			{
				$num_carpetas_hasta_raiz_web=1;						
			}//if($_SESSION['oficina_virtual']!=1)
			
		
		break;	
	}//switch($_SERVER["HTTP_HOST"])
	
//Esta variable almacenara el nombre del archivo php que se esta ejecutando en este momento (contiene variables get, "?id=es...")
	$indice_ruta=sizeof($ruta_origen)-$num_carpetas_hasta_raiz_web;
	//echo($indice_ruta);
	$pagina_actual=$ruta_origen[$indice_ruta];						
	
//	echo($pagina_actual);
//LALALA PRUEBAS PARA DEFINIR LA CARPETA EN EL SERVIDOR DEL CLIENTE!!!!!!		
//	echo("SCRIPT_FILENAME: ".$_SERVER['SCRIPT_FILENAME']."<br />");
//	echo("pagina_actual: ".$pagina_actual."<br />");	
//	echo("num_carpetas_hasta_raiz_web: ".$num_carpetas_hasta_raiz_web."<br />");
	
//PARA QUITAR LA EXTENSION SI ES NECESARIO!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

/*	
//Esta variable almacenara la posicion del punto en la cadena de texto que almacena el nombre de la pagina de origen
	$posicion_punto=strrpos($pagina_actual,".");							
//Ahora la variable almacena solamente el nombre de la pagina de origen, sin extension y sin parametros GET	
	$pagina_actual=substr($pagina_actual,0,$posicion_punto);			
*/
	
//Devolvemos el nombre del script en ejecucion	
	return $pagina_actual;
}//function nombre_script_en_ejecucion()

//Esta funcion muestra una ventana modal informativa con el mansaje que recibe como parametro
function MsgBox($frase)
{
	$codigo="<script type='text/javascript'>\n";
	$codigo.=" alert('$frase');\n";
	$codigo.="</script>\n";		
	echo($codigo);
}//function MsgBox($frase)

//Esta funcion redirige a la URL que recibe como parametro
function redirigir($url)
{
	$codigo="<script type='text/javascript'>\n";
	$codigo.=" location.href='$url';\n";
	$codigo.="</script>\n";		
	echo($codigo);
}//function redirigir($url)

//Como la funcion strtoupper, no convierte a mayusculas ni las vocales acentuadas, ni la � y otros carac�cteres y en este caso particular el nombre de la empresa que se muestra en mayusculas, ha sido necesario realizar esta funcion para corregir este problema. En este caso habr� que sustituir las llamadas a la funcion strtoupper por llamadas a esta funcion
function pasar_mayusculas($cadena)
{
   return strtr($cadena,
   "abcdefghijklmnopqrstuvwxyz".
   "\x9C\x9A\xE0\xE1\xE2\xE3".
   "\xE4\xE5\xE6\xE7\xE8\xE9".
   "\xEA\xEB\xEC\xED\xEE\xEF".
   "\xF0\xF1\xF2\xF3\xF4\xF5".
   "\xF6\xF8\xF9\xFA\xFB\xFC".
   "\xFD\xFE\xFF",
   "ABCDEFGHIJKLMNOPQRSTUVWXYZ".
   "\x8C\x8A\xC0\xC1\xC2\xC3\xC4".
   "\xC5\xC6\xC7\xC8\xC9\xCA\xCB".
   "\xCC\xCD\xCE\xCF\xD0\xD1\xD2".
   "\xD3\xD4\xD5\xD6\xD8\xD9\xDA".
   "\xDB\xDC\xDD\xDE\x9F");
}//function pasar_mayusculas($cadena)
echo($con);
/********************************************(FIN)FUNCIONES DE CONEXION A LA BASE DE DATOS Y VARIABLES GENERICAS******************************/
/********************************************************************************************************************************************/