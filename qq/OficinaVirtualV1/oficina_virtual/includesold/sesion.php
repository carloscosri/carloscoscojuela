<?php
//Como hay que comenzar la session en cada script que use las variables de sesion, para poder acceder a ellas, añadimos la intruccion en este archivo, que estara incluido en todas las paginas

session_start();	

//Esta variable almacenara el tiempo de expiracion de la sesion en segundos
$tiempo_expiracion_sesion=600;

// Cuando el tiempo que lleva activa sea menor que 1 minuto 
 if (!empty($_SESSION['usuario']) and !empty($_SESSION["contrasena"])) 
 { 
 	$antes = $_SESSION["tiempo_sesion"];
//Si ha trascurrido el tiempo maximo de la sesion, la destruiremos. El resto de las paginas ya estan preparadas para redirigir al usuario si no se encuentra identificado			 
	 if (time()-$antes > $tiempo_expiracion_sesion) 
	 { 
	 	echo("SE DESTRUYE LA SESION!!!!!<BR />");
		session_unset();
		session_destroy();
	 }//if (time()-$antes > $tiempo_expiracion_sesion) 
	 
	 else
	 {
//Para que no caduque la sesion, siempre que pase el tiempo maximo, y sea solamente por inactividad, cuando no haya finalizado el tiempo maximo renovaremos la variable del tiempo de sesion
 		$_SESSION["tiempo_sesion"]=time();
	 }//else (time()-$antes > $tiempo_expiracion_sesion) 
 }//if (!empty($_SESSION['usuario']) and !empty($_SESSION["contrasena"])) 

//Esta variable almacena la ruta del servidor en la que se encuentra el directorio raiz del sitio web	
$directorio_raiz_web=$_SERVER["DOCUMENT_ROOT"]."/";

//Para no tener que preocuparnos por la conexion, hacemos condicional el directorio raiz de la web
switch($_SERVER["HTTP_HOST"]){
	
//Si el servidor es el de AUDINFOR

/*
	default:		
		// Esta variable almacena la ruta del servidor en la que se encuentra el directorio raiz del sitio web	
		$directorio_raiz_web=$directorio_raiz_web."/";
		// Esta variable definira la URL del servidor de la base de datos
		$ruta_sevidor_bd="82.223.114.64";
		// Esta variable almacena el nombre de usuario para la identificacion en la base de datos
		$usuario_bd="Electrica";
		// Esta variable almacena la clavepara la identificacion en la base de datos
		$contrasena_bd="Ele2015";
		// Esta variable definira el nombre de la base de datos que contiene la informacion para la web, en este caso coincide con el nombre de usuario para entrar a la base de datos, aunqur no tendría porque ser asi
		$nombre_bd="ElectricaDirecta";
	break;
	*/
   default:		
		// Esta variable almacena la ruta del servidor en la que se encuentra el directorio raiz del sitio web	
		$directorio_raiz_web=$directorio_raiz_web."/";
		// Esta variable definira la URL del servidor de la base de datos
		$ruta_sevidor_bd="212.97.161.140";
		// Esta variable almacena el nombre de usuario para la identificacion en la base de datos
		$usuario_bd="ovdemos";
		// Esta variable almacena la clavepara la identificacion en la base de datos
		$contrasena_bd="Audin349728";
		// Esta variable definira el nombre de la base de datos que contiene la informacion para la web, en este caso coincide con el nombre de usuario para entrar a la base de datos, aunqur no tendría porque ser asi
		$nombre_bd="ov_demos";
	break;
	/*$directorio_raiz_web = $directorio_raiz_web."/";
	$ruta_sevidor_bd = "212.97.161.140";
	$usuario_bd = "ovdemos";
	$contrasena_bd = "Audin349728";
	$nombre_bd = "ov_demos";*/
}//switch($_SERVER["HTTP_HOST"])
	
$_SESSION["directorio_raiz"]=$directorio_raiz_web;	

//echo("ORIGEN: ".$_SERVER["DOCUMENT_ROOT"]."<br />");
//echo("RAIZ: ".$_SESSION["directorio_raiz"]."<br />");
?>