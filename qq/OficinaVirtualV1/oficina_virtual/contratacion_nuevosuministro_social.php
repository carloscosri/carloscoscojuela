<?php
/*
NOTAS:

1- ESTA SECCION TOMA LOS DATOS DE LA TABLA ComercializadorasUR, tabla creada expresamente para esta seccion

2 - ESTA SECCION PERTENECE A LA PARTE DE LA WEB PUBLICA, POR ESO NO SE REQUIERE IDENTIFICACION PARA ACCEDER A ELLA. ESTA EN LA CARPETA DE OFICINA VIRTUAL POR NO TENER QUE HACER NUEVOS COMPONENTES Y CAMBIAR RUTAS.


*/

/* NOTA:

 COMO NO SE CIERRA LA CONEXION TRAS CADA CONEXION CON LA BASE DE DATOS, LAS CONSULTAS DEJAN DE FUNCIONAR SI SE ESTABLECE COMO UNICA CONEXION
 LA QUE UTILIZA EL RESTO DE LA WEB, POR LO TANTO HASTA QUE SE HAGA LA MIGRACION COMPLETA DE LOS TEXTOS, COEXISTIRAN LAS DOS CONEXIONES, YA QUE
 SE HA AÑADIDO UNA LLAMADA A LA CONEXION BUENA EN ESTE ARCHIVO.
*/

include("_conexion.php");

//Se incluye el archivo con los textos especifico de esta seccion en el idioma oportuno
//include($_SESSION["directorio_raiz"]."idiomas/".$_SESSION["idioma"]."/oficina_contratacion_social.php");
include("../idiomas/".$_SESSION["idioma"]."/oficina_contratacion_social.php");

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<title><?=$titulo_web;?></title>

<!--Se añaden las hojas de estilos genericas que utiliza esta seccion-->
<link href="../css/estilos.css" rel="stylesheet" type="text/css" />
<link href="../css/secciones.css" rel="stylesheet" type="text/css" />
<link href="css/oficina.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_vieja.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_contratacion_social.css" rel="stylesheet" type="text/css" />

<!--Ahora se añaden las reglas para Internet Explorer. Asi corregiremos las diferencias de visualizacion entre navegadores
Es importante añadirlo despues de las reglas principales, para que se sobreescriban las correcciones pertinentes. En este caso las diferencias
solo son para internet explorer 7-->

<!--[if IE 7]>
	<link href="../css/estilos_ie7.css" rel="stylesheet" type="text/css" />
	<link href="../css/secciones_ie7.css" rel="stylesheet" type="text/css" />    
<![endif]-->

</head>
	
<body>
<!--*************************************************Web realizada por ****************************************-->
	<div id="central_oficina">
    
		<div>
<?php
			//include($_SESSION["directorio_raiz"]."includes/cabecera.php");
			include("../includes/cabecera.php");            
?>
		</div>                               

		<div class="posicion_menu">
<?php        
			//include($_SESSION["directorio_raiz"]."includes/menu.php");
			include("../includes/menu.php");
?>			            
		</div><!--<div class="posicion_menu">-->
           
		<div class="posicion_submenu">
<?php
			//include($_SESSION["directorio_raiz"]."includes/submenu.php");
			include("../includes/submenu.php");
?>
        </div> <!--<div class="posicion_menu">-->                 
        
		<div class="contenido_contratacion_social_sin_registrar">

			<div class="titulo_seccion_oficina_contratacion_social arialblack18Titulo"><?=$oficina_contratacion_social1?></div>
            
            <div class="texto_seccion_oficina_contratacion_social arial12">
				<?=$oficina_contratacion_social2?>
   				<a href="contratacion_nuevosuministro_alta.php?id=<?=$_SESSION["idioma"].$ampersand?>s=4" class="enlace_bono_social"><?=$oficina_contratacion_social4?></a>
                <br />
	            <?=$oficina_contratacion_social5?>
            </div><!--<div class="texto_seccion_oficina_contratacion_social arial12">-->
            
<?php
//Como las comercializadoras de último recurso no van a ser algo dinámico, se añaden en este array. Se ha decidido no añadirlo a las tablas de idiomas porque al no variar los textos dependiendo del idioma, omitimos la repeticion de estos textos en la base de datos
						
			include("comercializadoras_ultimo_recurso.php");
			
//Esta variable almacenra el numero de comercializadoras que se han mostrado																																			
			$num_comercializadoras_mostradas=0;

//Ahora se muestran todas las comercializadoras de ultimo reccurso.

			while($num_comercializadoras_mostradas<count($array_comercializadoras))
			{				
?>
				<div class="comercializadoras_seccion_oficina_contratacion_social"><strong>
					<?=$array_comercializadoras[$num_comercializadoras_mostradas][0]?></strong>&nbsp;-&nbsp;
					<?=$array_comercializadoras[$num_comercializadoras_mostradas][1]?>&nbsp;-&nbsp;
					<a href="http://<?=$array_comercializadoras[$num_comercializadoras_mostradas][2]?>" class="enlace" target="_blank"><?=$array_comercializadoras[$num_comercializadoras_mostradas][2]?></a>
                </div><!--<div class="comercializadoras_seccion_oficina_contratacion_social"><strong>-->   

<?php				
				$num_comercializadoras_mostradas++;
			}//while($num_comercializadoras mostradas<count($array_comercializadoras))
			
																																	//$array_comercializadoras[0][0]
																																	
?>
                                    
            <div class="volver_seccion_oficina_contratacion_social"><a href="contratacion_nuevosuministro.php?id=<?=$_SESSION["idioma"]?>" class="enlace"><?=$oficina_contratacion_social3?></a></div>            
<!--VERSION VIEJA-->



        	
        	
		</div><!--<div class="contenido_contratacion_social_sin_registrar">-->                        
		
        
        <div class="limpiar"></div>
		
        <br /><br /><br /><br /><br />
                        	
<!--Por ultimo aparecera el pie de la web-->
<?php 
//	include($_SESSION["directorio_raiz"]."includes/pie.php");
	include("../includes/pie.php");
?>        
    </div><!--<div id="central_oficina"-->
</body>
</html>
