<?php
//El array que almacena las comercializadoras de ultimo recurso tiene la siguiente estructura:

//0 -> Razon social de la comercializadora de ultimo recurso
//1 -> Telefono de la comercializadora de ultimo recurso
//2 -> Direccion web de la comercializadora de ultimo recurso

$array_comercializadoras[0][0]="Endesa Energía XXI, S.L.";
$array_comercializadoras[0][1]="902 508 850";
$array_comercializadoras[0][2]="www.endesaonline.com";

$array_comercializadoras[1][0]="Iberdrola Comercialización de Último Recurso, S.A.";
$array_comercializadoras[1][1]="901 202 020";
$array_comercializadoras[1][2]="www.iberdrola.es";

$array_comercializadoras[2][0]="Unión Fenosa Metra, S.L.";
$array_comercializadoras[2][1]="901 220 380";
$array_comercializadoras[2][2]="www.unionfenosa.es";

$array_comercializadoras[3][0]="Hidrocantábrico Energía Último Recurso S.A.U.";
$array_comercializadoras[3][1]="902 860 860";
$array_comercializadoras[3][2]="www.hcenergia.com";

$array_comercializadoras[4][0]="E.ON Comercializadora de Último Recurso, S.L.";
$array_comercializadoras[4][1]="902 222 838";
$array_comercializadoras[4][2]="www.eon-espana.com";

?>