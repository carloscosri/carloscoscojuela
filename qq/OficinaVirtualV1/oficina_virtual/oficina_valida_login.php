<?php
/*
NOTAS:

1- CONSULTA DE LA TABLA "DatosRegistrados" PARA COMPROBAR SI EXISTE EL USUARIO

2- DESAPARARECE EL CAMBIO DE COMPORTAMIENTO DE LA APLICACIÓN CUANDO SE IDENTIFICA UN CLIENTE O UNA COMERCIALIZADORA

autorizaLOPD -> LEY OFICIAL DE PROTECCION DE DATOS
autorizaRD ->???

*/
///Añadimos el archivo que contiene el inicio de la sesion y las variables de sesion.  Es importante añadir
//este archivo antes de las cabeceras del documento o dara error
include("../includes/sesion.php");

//Añadimos la libreria de conexion a base de datos, que a su vez contiene las referencias a todos los archivos necesarios
include("../includes/conexion.php");

//Se incluye el archivo con los textos especifico de esta seccion en el idioma oportuno
//include($_SESSION["directorio_raiz"]."idiomas/".$_SESSION["idioma"]."/oficina_virtual_login.php");
include("../idiomas/".$_SESSION["idioma"]."/oficina_virtual_login.php");


//SESION -> USO??
$_SESSION['nombreUsuarioL'] = $_POST["usuario"];




//Esta funcion quita una serie de caracteres de la cadena de texto que recibe como parametro
function quitar($mensaje)
{
	$mensaje = str_replace("<","&lt;",$mensaje);
	$mensaje = str_replace(">","&gt;",$mensaje);
	$mensaje = str_replace("\'","&#39;",$mensaje);
	$mensaje = str_replace('\"',"&quot;",$mensaje);
	$mensaje = str_replace("\\\\","&#92;",$mensaje);
	
	return $mensaje;
}//function quitar($mensaje)

$_SESSION['info_login']="";	

//Comprobacion de si los campos estan vacios, si no es asi se avisa al usuario y se redirige a la pantalla de identificacion
	if(trim($_POST["usuario"])=="" or trim($_POST["password"])=="")
	{
		

		
		MsgBox($error_rellenar);	
		redirigir("oficina_index.php?id=".$_SESSION["idioma"]);
		
	}//if(trim($_POST["usuario"])=="" or trim($_POST["password"])=="")
	
//Si el usuario ha escrito todos los datos pasaremos a comprobar si el usuario existe
	else
	{
//Estas variables recojen los valores recibidos por el formulario de identificacion. Se aprovecha para quitar los catracteres problematicos			
		
		if($_POST["usuario"])
		{
		$nombre_usuario = quitar($_POST["usuario"]);
		$password = quitar($_POST["password"]);
		}
		
		else {
		$nombre_usuario=$_GET['CodigoCliente'];
		$password=$_GET['Password'];
		
		}
//Consultamos en la tabla DatosRegistrados si existe el usuario
		$rs_usuario = seleccion_condicional_campos("DatosRegistrados","Usuario,DNI,Password,CodigoCliente,TitularNombre","Usuario='".$nombre_usuario."' AND Password='".$password."'","CodigoCliente",0,$ruta_sevidor_bd,$usuario_bd,$contrasena_bd,$nombre_bd);				

		$registro_usuario=mysql_fetch_array($rs_usuario);
		
		$rs_usuario_mp = seleccion_condicional_campos("ContraseñasMultipuntos","CIF,Contraseña","CIF='".$nombre_usuario."' AND Contraseña='".$password."'","CIF",0,$ruta_sevidor_bd,$usuario_bd,$contrasena_bd,$nombre_bd);
		
		$registro_usuario_mp=mysql_fetch_array($rs_usuario_mp);
		
		$rs_usuario_rep = seleccion_condicional_campos("Comisionistas","Nombre,DNI","Nombre='".$nombre_usuario."' AND DNI='".$password."'","DNI",0,$ruta_sevidor_bd,$usuario_bd,$contrasena_bd,$nombre_bd);
		
		$registro_usuario_rep=mysql_fetch_array($rs_usuario_rep);
		
		//Comprobacion de Usuario Adminsitrador
		$administracion = mysql_num_rows(mysql_query("SELECT * FROM Administracion WHERE Usuario='" .$nombre_usuario."' AND Contrasena='".$password."'"));
		
		if($administracion!=0){ //Solo accede si el usuario es el administrador
			$_SESSION['usuario_mp']= $_POST["usuario"];
			$_SESSION['password_mp'] = $password;
			$_SESSION['administracion'] = 1;
			redirigir("oficina_presentacion_call.php?id=".$_SESSION["idioma"]);
		}
		
//Si no obtenemos resultados en la consulta se dara un mensaje de error y se redirige a la pantalla de identificacion		
		if((!$registro_usuario) AND (!$registro_usuario_mp) AND (!$registro_usuario_rep))
		{
			MsgBox($error_no_usuario);		
			redirigir("oficina_index.php?id=".$_SESSION["idioma"]);
		}//if(!$registro_usuario)
		
//Si existe el usuario continuaremos con el proceso
		else
		{			
				if($registro_usuario_mp)
				{
				$_SESSION['usuario_mp']= $_POST["usuario"];
				$_SESSION['password_mp'] = $password;
				
				redirigir("oficina_presentacion_mp.php?id=".$_SESSION["idioma"]);
				}
				else if ($registro_usuario_rep) {
					$_SESSION['usuario_mp']= $_POST["usuario"];
					$_SESSION['password_mp'] = $password;
					$_SESSION['comercial'] = 1;
					redirigir("oficina_presentacion_comercial.php?id=".$_SESSION["idioma"]);
				}
				else {
		
//Se da valor a las variables de sesion viejas			
			$_SESSION['usuario'] = $_POST["usuario"];							
			$_SESSION['nombre_usuario'] = $registro_usuario['TitularNombre'];
			$_SESSION['login'] = 1;
			$_SESSION['password'] = $password;
			$_SESSION['numeroCliente'] = $registro_usuario['CodigoCliente'];	
			$_SESSION['DNI']=$registro_usuario['DNI'];
						
//Redirigimos al primer apartado de la oficina virtual, los datos personales, asi se evita la redireccion al index que se hacia en la versión vieja						
			redirigir("oficina_presentacion.php?id=".$_SESSION["idioma"]);
			}//if($registro_usuario_mp)
		}//if((!$registro_usuario) AND (!$registro_usuario_mp))				
	}//if(trim($_POST["usuario"])=="" or trim($_POST["password"])=="")	

		/*echo("POST['usuario']: ".$_POST["usuario"]);
		echo("POST['password']: ".$_POST["password"]);	*/
?>