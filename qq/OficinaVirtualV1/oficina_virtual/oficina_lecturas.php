<?php
/* NOTA:

 COMO NO SE CIERRA LA CONEXION TRAS CADA CONEXION CON LA BASE DE DATOS, LAS CONSULTAS DEJAN DE FUNCIONAR SI SE ESTABLECE COMO UNICA CONEXION
 LA QUE UTILIZA EL RESTO DE LA WEB, POR LO TANTO HASTA QUE SE HAGA LA MIGRACION COMPLETA DE LOS TEXTOS, COEXISTIRAN LAS DOS CONEXIONES, YA QUE
 SE HA AÑADIDO UNA LLAMADA A LA CONEXION BUENA EN ESTE ARCHIVO.
*/

include("_conexion.php");

//Se incluye el archivo que contiene las verificaciones de datos
include("../includes/verificaciones.php");

//Se incluye el archivo con los textos especifico de esta seccion en el idioma oportuno
//include($_SESSION["directorio_raiz"]."idiomas/".$_SESSION["idioma"]."/oficina_lecturas.php");

include("../idiomas/".$_SESSION["idioma"]."/oficina_lecturas.php");
include("../idiomas/".$_SESSION["idioma"]."/oficina_modificacion_datossuministro.php");
include("../idiomas/".$_SESSION["idioma"]."/oficina_datos.php");
//Asignamos el valor por defecto a la fecha, este cambiara si el usuario pulsa el boton del formulario habiendo escrito algo
$fecha_lectura=date("d-m-Y");

//Cuando se pulse el boton aceptar del formulario, recogeremos el valor del boton
if (isset($_POST['Submit'])) 
{	
//Recogemos los valores de las variables del formulario
	$fecha_lectura=$_POST['FechaLectura'];
	$num_contador=$_POST['NumeroContador'];
		
	$lectura_unica=$_POST['lectura_unica'];

//Al las lecturas les sustituimos el punto por la coma, ya que es el caracter que emplea MYSQL para los numero sdecimales			
//SUSTITUIR COMAS!!!!!	
	$p1_activa=str_replace(".",",",$_POST['P1_activa']);	
	$p1_reactiva=str_replace(".",",",$_POST['P1_reactiva']);			
	$p1_maximetro=str_replace(".",",",$_POST['P1_maximetro']);
		
	$p2_activa=str_replace(".",",",$_POST['P2_activa']);	
	$p2_reactiva=str_replace(".",",",$_POST['P2_reactiva']);	
	$p2_maximetro=str_replace(".",",",$_POST['P2_maximetro']);	
			
	$p3_activa=str_replace(".",",",$_POST['P3_activa']);
	$p3_reactiva=str_replace(".",",",$_POST['P3_reactiva']);	
	$p3_maximetro=str_replace(".",",",$_POST['P3_maximetro']);		
	
	$p4_activa=str_replace(".",",",$_POST['P4_activa']);
	$p4_reactiva=str_replace(".",",",$_POST['P4_reactiva']);	
	$p4_maximetro=str_replace(".",",",$_POST['P4_maximetro']);		
	
	$p5_activa=str_replace(".",",",$_POST['P5_activa']);
	$p5_reactiva=str_replace(".",",",$_POST['P5_reactiva']);	
	$p5_maximetro=str_replace(".",",",$_POST['P5_maximetro']);		
	
	$p6_activa=str_replace(".",",",$_POST['P6_activa']);
	$p6_reactiva=str_replace(".",",",$_POST['P6_reactiva']);	
	$p6_maximetro=str_replace(".",",",$_POST['P6_maximetro']);		
	
	$observaciones=$_POST['Observaciones'];
							
//Con esta variable se comprobara si hay algun tipo de error en el formulario	
	$error=0;

	if($fecha_lectura=="" or $num_contador=="")
	{
	//	$error=1;
	}//if($fecha_lectura=="" or $num_contador=="")

//Comprobamos que la fecha se ha escrito correctamente	
	if($error==0 and (comprobar_fecha($fecha_lectura)))
	{
		$error=2;
	}//if($error==0 and (comprobar_fecha($fecha_lectura)))

//Si no se ha producido un error anteriormente pasaremos a comprobar las fechas
	if ($error==0)
	{
//Esta variable capturara la fecha actual del sistema, la usaremos para comparar con las fechas escritas por el usuario	
		$fecha_actual=date("d-m-Y");
				
//Esta variable almacenara los dias trascurridos desde la fecha de lectura a la fecha actual, respectivamente
		$diferencia_fecha=dias_trascurridos($fecha_lectura,$fecha_actual);									
		
//Comprobamos que las fechas introducidas por el usuario no sean posteriores a la fecha actual
		if($diferencia_fecha<0)
		{
			$error=6;			
		}//if($diferencia_fecha<0)
	}//if ($error==0)
				
//Ahora se comprueba que al menos se haya rellenado al menos una lectura	
	if($error==0 and $lectura_unica=="" and $p1_activa=="" and $p1_reactiva=="" and $p1_maximetro=="" and $p2_activa=="" and $p2_reactiva=="" and $p2_maximetro=="" and $p3_activa=="" and $p3_reactiva=="" and $p3_maximetro=="" and $p4_activa=="" and $p4_reactiva=="" and $p4_maximetro=="" and $p5_activa=="" and $p5_reactiva=="" and $p5_maximetro=="" and $p6_activa=="" and $p6_reactiva=="" and $p6_maximetro=="")
	{
		$error=3;
	}//if($error==0 and $lectura_unica=="" and $p1_activa=="" and $p1_reactiva=="" and $p1_maximetro=="" and $p2_activa=="" and $p2_reactiva=="" and $p2_maximetro=="" and $p3_activa=="" and $p3_reactiva=="" and $p3_maximetro=="" and $p4_activa=="" and $p4_reactiva=="" and $p4_maximetro=="" and $p5_activa=="" and $p5_reactiva=="" and $p5_maximetro=="" and $p6_activa=="" and $p6_reactiva=="" and $p6_maximetro=="")

				
//Si se ha rellenado un valor de lectura de un solo periodo, no puede haber nada en los campos de lecturas con varios periodos tarifarios
	if($error==0 and $lectura_unica!="" and ($p1_activa!="" or $p1_reactiva!="" or $p1_maximetro!="" or $p2_activa!="" or $p2_reactiva!="" or $p2_maximetro!="" or $p3_activa!="" or $p3_reactiva!="" or $p3_maximetro!="" or $p4_activa!="" or $p4_reactiva!="" or $p4_maximetro!="" or $p5_activa!="" or $p5_reactiva!="" or $p5_maximetro!="" or $p6_activa!="" or $p6_reactiva!="" or $p6_maximetro!=""))
	{
		$error=4;		
	}//if($error==0 and $lectura_unica!="" and ($p1_activa!="" or $p1_reactiva!="" or $p1_maximetro!="" or $p2_activa!="" or $p2_reactiva!="" or $p2_maximetro!="" or $p3_activa!="" or $p3_reactiva!="" or $p3_maximetro!="" or $p4_activa!="" or $p4_reactiva!="" or $p4_maximetro!="" or $p5_activa!="" or $p5_reactiva!="" or $p5_maximetro!="" or $p6_activa!="" or $p6_reactiva!="" or $p6_maximetro!=""))


//Ahora se comprueba el formato de los numeros de las lecturas, que deben ser como maximo numeros decimales de 8 digitos la parte entera y 3 la decimal, aunque tambien se aceptan numeros enteros

//Estas variables controlaran las caracteristicas de los numeros de las lecturas
	$longitud_parte_entera=8;
	$longitud_parte_decimal=3;	

//Si se ha escrito una lectura unica comprobaremos el formato de su numero
	if($error==0 and ($lectura_unica!=""))
	{
		if(comprobar_decimal($lectura_unica,$longitud_parte_entera,$longitud_parte_decimal))
		{
			$error=5;	
		}//if(comprobar_decimal($lectura_unica,$longitud_parte_entera,$longitud_parte_decimal))
		
//Si no se ha detectado ningun error estableceremos el valor que se grabara en la base de datos para el campo LecturaDomestica de la tabla de lecturas, ya que lo que almacena a cambiado, ahora simplemente almacenara "si" o "no"		
		else
		{
			$lectura_domestica="si";
			
//Como la lectura domestica equivale al periodo tarifario p2_activa, igualamos los valores para que se escriba en el campo correspondiente
			$p2_activa=$lectura_unica;
						
		}//if(comprobar_decimal($lectura_unica,$longitud_parte_entera,$longitud_parte_decimal))
	}//if(comprobar_decimal($lectura_unica,$longitud_parte_entera....

	else
	{				
//Si no se ha escrito ninguna lectura unica, deberemos de comprobar el resto de las lecturas, en el caso de que no se haya  producido un error previamnete
		if ($error==0)
		{
//En este caso se establecera la lectura domestica a no, ya que el usuario ha rellena varios periodos tarifarios	
			$lectura_domestica="no";			
		
//Almacenaremos en un array todas las lecturas que tengan contenido	del apartado "Lecturas con Varios Periodos Tarifarios"
			$array_lecturas[0]=$p1_activa;	
			$array_lecturas[1]=$p1_reactiva;
			$array_lecturas[2]=$p1_maximetro;
				
			$array_lecturas[3]=$p2_activa;
			$array_lecturas[4]=$p2_reactiva;
			$array_lecturas[5]=$p2_maximetro;
					
			$array_lecturas[6]=$p3_activa;
			$array_lecturas[7]=$p3_reactiva;
			$array_lecturas[8]=$p3_maximetro;
			
			$array_lecturas[9]=$p4_activa;
			$array_lecturas[10]=$p4_reactiva;
			$array_lecturas[11]=$p4_maximetro;
			
			$array_lecturas[12]=$p5_activa;
			$array_lecturas[13]=$p5_reactiva;
			$array_lecturas[14]=$p5_maximetro;
			
			$array_lecturas[15]=$p6_activa;
			$array_lecturas[16]=$p6_reactiva;
			$array_lecturas[17]=$p6_maximetro;		

//Esta variable almacenara el numero de lecturas que se han comprobado			
			$num_lecturas_comprobadas=0;

//Ahora comprobaremos el formato de los numeros de las lecturas
			while($num_lecturas_comprobadas<count($array_lecturas))
			{		
//Solo se comprobaran las lecturas que no se hayan dejado vacias			
				if($array_lecturas[$num_lecturas_comprobadas]!="")			
				{				
//Si se detecta algun numero con formato invalido se dara error y se saldra del bucle			
					if(comprobar_decimal($array_lecturas[$num_lecturas_comprobadas],$longitud_parte_entera,$longitud_parte_decimal))
					{
						$error=5;				
						break;
					}//if(comprobar_decimal($array_lecturas[$num_lecturas_comprobadas],$longitud_parte_entera,$longitud_parte_decimal))
				}//if($array_lecturas[$num_lecturas_comprobadas]!="")					
			
				$num_lecturas_comprobadas++;		
			}//while($num_lecturas_comprobadas<count($array_lecturas[17]))
		}//if ($error==0)			
	}//else($error==0 and ($lectura_unica!=""))
										
}//if (isset($_POST['Submit']))
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<title><?=$titulo_web;?></title>

<!--Se añaden las hojas de estilos genericas que utiliza esta seccion-->
<link href="../css/estilos.css" rel="stylesheet" type="text/css" />
<link href="../css/secciones.css" rel="stylesheet" type="text/css" />
<link href="css/oficina.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_vieja.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_lecturas.css" rel="stylesheet" type="text/css" />

<!--Ahora se añaden las reglas para Internet Explorer. Asi corregiremos las diferencias de visualizacion entre navegadores
Es importante añadirlo despues de las reglas principales, para que se sobreescriban las correcciones pertinentes. En este caso las diferencias
solo son para internet explorer 7-->

<script language=javascript type=text/javascript>
function stopRKey(evt) {
var evt = (evt) ? evt : ((event) ? event : null);
var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
if ((evt.keyCode == 13) && (node.type=="text")) {return false;}
}
document.onkeypress = stopRKey; 
</script>

<!--[if IE 7]>
	<link href="../css/estilos_ie7.css" rel="stylesheet" type="text/css" />
	<link href="../css/secciones_ie7.css" rel="stylesheet" type="text/css" />    
<![endif]-->

</head>
	
<body>
<!--*************************************************Web realizada por ****************************************-->
	<div id="central_oficina">
    
<?php
//Se comprueba que el usuario se haya identificado, si no es asi se da un aviso y redirigimos a la seccion de identificacion
if ($_SESSION['usuario']=="")
{	
	MsgBox($error_identificacion);
	redirigir("oficina_index.php?id=".$_SESSION["idioma"]);
	
}//if ($_SESSION['usuario'])

//Si el usuario se ha identificado la seccion se ejecutara con normalidad
else
{
//Añadimos los fragmentos comunes, la cabecera, el menu, y el submenu
	include("oficina_fragmentos_comunes.php");
?>                    
		<div class="contenido_seccion_oficina_lecturas">

<!--VERSION VIEJA-->

      <form action="oficina_lecturas.php?id=<?=$_SESSION["idioma"]?>"  method="post" name="lectura_contadores">		 
		  <table width="520" border="0" cellspacing="0" cellpadding="0">
            <tr> 
              <td><table width="520" border="0" cellspacing="0" cellpadding="0">
                  <?php $result=mysql_query("SELECT * FROM ".$_SESSION['tablaIdioma']." WHERE idTexto=112");
		  $row = mysql_fetch_array($result);?>
                  <tr align="center"> 
                    <td height="30" class="arialblack18Titulo" style=" background-image:url(imagenes/MarcoAlta.gif);background-repeat:no-repeat"><?php echo($row[1]);?></td>
                  </tr>
                  <tr> 
                    <td class="arial14">&nbsp;</td>
                  <tr> 
                    <td class="arial14"><fieldset >
                      <legend class="arialblack14">
                      <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 130'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                      </legend>
                      <table width="520" align="center">
                        <!--DWLayoutTable-->
                        <tr> 
                          <td width="121" height="22" valign="middle" class="arial14"> 
                            <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 196'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
&nbsp;                          </td>
                        <td width="327" valign="top" class="arial14"><?php if (isset($_SESSION['numeroCliente'])){
										$result = mysql_query("SELECT * FROM DatosRegistrados WHERE CodigoCliente = ".$_SESSION['numeroCliente']." ");
										$row = mysql_fetch_array($result);
										 }else{
										$result = mysql_query("SELECT * FROM DatosRegistrados WHERE CodigoCliente = -1");
										//Para que no tome ningun cliente pero no de ningun error en la pagina
										$row = mysql_fetch_array($result);
								 		}?>
                            <input name="CodigoCliente" type="text"  class="textfieldLecturaNum" id="CodigoCliente" value="<?php echo($row['CodigoCliente']);?>" size="5" maxlength="5" readonly="true">
                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 118'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                            &nbsp; 
                          <input name="SuministroDNI" type="text" class="textfieldLectura" id="SuministroDNI" value="<?php echo($row['DNI']);?>" size="12" maxlength="15"  readonly="true"></td>
                        </tr>
                        <tr> 
                          <td height="22" valign="middle" class="arial14">
                            <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 119'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
&nbsp;&nbsp;</td>
                        <td valign="top"><input name="SuministroNombre" type="text" class="textfieldLectura" id="SuministroNombre" value="<?php echo(str_replace('"','&quot;',$row['TitularNombre']));?>" size="50" maxlength="50"  readonly="true"></td>
                        </tr>
                        <tr> 
                          <td height="22" valign="middle" class="arial14">
                          <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 120'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?></td>
                        <td valign="top"><input name="SuministroExtensionNombre" type="text" class="textfieldLectura" id="SuministroExtensionNombre2" value="<?php echo(str_replace('"','&quot;',$row['SuministroExtension']));?>" size="50" maxlength="50"  readonly="true"></td>
                        </tr>
                        <tr> 
                          <td height="20" valign="middle" class="arial14">
                            <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 121'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                          &nbsp;</td>
                        <td valign="top"><input  name="SuministroExtension2" type="text" class="textfieldLectura" id="SuministroExtension3" value="<?php echo(str_replace('"','&quot;',$row['SuministroCiudad']));?>" size="50" maxlength="50" readonly="true"></td>
                        </tr>
                        <tr>
                          <td height="20" valign="middle" class="arial14"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 122'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?></td>
                          <td valign="top"><input  readonly="true" name="SuministroCP2" type="text" class="textfieldLecturaNum" id="SuministroCP3" value="<?php echo($row['SuministroCP']);?>" size="6" maxlength="10"></td>
                        </tr>
                        <tr> 
                          <td height="22" valign="middle" class="arial14">
                            <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 123'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td valign="top"><input name="SuministroCalle" type="text"  class="textfieldLectura" id="SuministroExtensionNombre3" value="<?php echo(str_replace('"','&quot;',$row['SuministroCalle']));?>" size="50" maxlength="50" readonly="true"></td>
                        </tr>
                        <tr> 
                          <td height="22" valign="middle" class="arial14">
                            <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 124'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                          &nbsp;</td>
                        <td valign="top" class="arial14"><input  readonly="true"  name="SuministroNumero" type="text" class="textfieldLecturaNum" id="SuministroNumero" value="<?php echo($row['SuministroNumero']);?>" size="5" maxlength="4">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 125'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
&nbsp;
<input  name="SuministroExtension" type="text" class="textfieldLectura" id="SuministroExtension" value="<?php echo(str_replace('"','&quot;',$row['SuministroExtension']));?>" size="20" maxlength="20"  readonly="true"></td>
                        </tr>
                        <tr>
                          <td height="22" valign="middle" class="arial14"><?=$oficina_datos2?>
                            &nbsp;&nbsp;&nbsp;&nbsp;</td>
                          <td valign="top"><input name="SuministroExtensionNombre2" type="text"  class="textfieldLectura" id="SuministroExtensionNombre" value="<?=$row["TarifaContratada"]?>" size="10" maxlength="10" readonly="readonly" /></td>
                        </tr>
                      </table>
                      </fieldset></td>
                  <tr> 
                    <td>&nbsp;</td>
                  </tr>
                  <tr class="arial12"> 
                    <td height="10" class="arial14"></td>
                  </tr>
                  <tr > 
                    <td class="arial12black">
                      <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 327'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>                    </td>
                  </tr>
                  <tr class="arial12"> 
                    <td class="arial14">&nbsp;</td>
                  </tr>
                  
                  
                  
                  
                  
                  
                  
                  <tr> 
					<td width="451" class="nota_campos_obligatorios"><span class="nota_campos_obligatorios"><?=$nota_campos_obligatorios?></span><br /><br /></td>
				</tr>
                  
                  <tr class="arial12"> 
                    <td class="arial14"><fieldset>
                      <legend class="arialblack14">
                      <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 328'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                      </legend>
                      <table width="520">
                        <!--DWLayoutTable-->
                        <tr> 
                          <td height="24" valign="top" class="arial14"> 
                            <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 329'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>*
&nbsp;&nbsp;
                          
                           
						  <?php 
						  
						  
						 switch ($row["TarifaContratada"])
						 {
						 case '2.0A ML':
						      $readonly20="";
							  $class20="textfieldCentrado";
							  
							  $P1Activa_readonly="readonly=\"true\" ";
							  $P2Activa_readonly="readonly=\"true\" ";
							  $P3Activa_readonly="readonly=\"true\" ";
							  $P4Activa_readonly="readonly=\"true\" ";
							  $P5Activa_readonly="readonly=\"true\" ";
							  $P6Activa_readonly="readonly=\"true\" ";
							  
							  $P1Activa_class="textfieldLectura";
							  $P2Activa_class="textfieldLectura";
							  $P3Activa_class="textfieldLectura";
							  $P4Activa_class="textfieldLectura";
							  $P5Activa_class="textfieldLectura";
							  $P6Activa_class="textfieldLectura";
							  
					     break;
						 
						 case '2.1A ML':
						      $readonly20="";
							  $class20="textfieldCentrado";
							  
							  $P1Activa_readonly="readonly=\"true\" ";
							  $P2Activa_readonly="readonly=\"true\" ";
							  $P3Activa_readonly="readonly=\"true\" ";
							  $P4Activa_readonly="readonly=\"true\" ";
							  $P5Activa_readonly="readonly=\"true\" ";
							  $P6Activa_readonly="readonly=\"true\" ";
							  
							  $P1Activa_class="textfieldLectura";
							  $P2Activa_class="textfieldLectura";
							  $P3Activa_class="textfieldLectura";
							  $P4Activa_class="textfieldLectura";
							  $P5Activa_class="textfieldLectura";
							  $P6Activa_class="textfieldLectura";
						 
						 
						 break;
						 
						 
						 
						 
						 case '2.0.DHA ML' :
						      $readonly20="readonly=\"true\" ";
							  $class20="textfieldLectura";
							  
							  $P1Activa_readonly="";
							  $P2Activa_readonly="readonly=\"true\" ";
							  $P3Activa_readonly="";
							  $P4Activa_readonly="readonly=\"true\" ";
							  $P5Activa_readonly="readonly=\"true\" ";
							  $P6Activa_readonly="readonly=\"true\" ";
							  
							  $P1Activa_class="textfieldCentrado";
							  $P2Activa_class="textfieldLectura";
							  $P3Activa_class="textfieldCentrado";
							  $P4Activa_class="textfieldLectura";
							  $P5Activa_class="textfieldLectura";
							  $P6Activa_class="textfieldLectura";
						 break;
						 
						 
						  case '2.1.DHA ML' :
						      $readonly20="readonly=\"true\" ";
							  $class20="textfieldLectura";
							  
							  $P1Activa_readonly="";
							  $P2Activa_readonly="readonly=\"true\" ";
							  $P3Activa_readonly="";
							  $P4Activa_readonly="readonly=\"true\" ";
							  $P5Activa_readonly="readonly=\"true\" ";
							  $P6Activa_readonly="readonly=\"true\" ";
							  
							  $P1Activa_class="textfieldCentrado";
							  $P2Activa_class="textfieldLectura";
							  $P3Activa_class="textfieldCentrado";
							  $P4Activa_class="textfieldLectura";
							  $P5Activa_class="textfieldLectura";
							  $P6Activa_class="textfieldLectura";
						 break;
						 
						 
						 case '3.0A ML':
						      $readonly20="readonly=\"true\" ";
							  $class20="textfieldLectura";
							  
							  $P1Activa_readonly="";
							  $P2Activa_readonly="";
							  $P3Activa_readonly="";
							  $P4Activa_readonly="readonly=\"true\" ";
							  $P5Activa_readonly="readonly=\"true\" ";
							  $P6Activa_readonly="readonly=\"true\" ";
							  
							  $P1Activa_class="textfieldCentrado";
							  $P2Activa_class="textfieldCentrado";
							  $P3Activa_class="textfieldCentrado";
							  $P4Activa_class="textfieldLectura";
							  $P5Activa_class="textfieldLectura";
							  $P6Activa_class="textfieldLectura";
							  
							  
						 break;
						 
						 	 case '3.1A ML':
						      $readonly20="readonly=\"true\" ";
							  $class20="textfieldLectura";
							  
							  $P1Activa_readonly="";
							  $P2Activa_readonly="";
							  $P3Activa_readonly="";
							  $P4Activa_readonly="";
							  $P5Activa_readonly="";
							  $P6Activa_readonly="";
							  
							  $P1Activa_class="textfieldCentrado";
							  $P2Activa_class="textfieldCentrado";
							  $P3Activa_class="textfieldCentrado";
							  $P4Activa_class="textfieldCentrado";
							  $P5Activa_class="textfieldCentrado";
							  $P6Activa_class="textfieldCentrado";
							  
							  
						 break;
						 
						 default:
						 
						 { 
						 
						      $readonly20="readonly=\"true\" ";
							  $class20="textfieldLectura";
							  
							  $P1Activa_readonly="readonly=\"true\" ";
							  $P2Activa_readonly="readonly=\"true\" ";
							  $P3Activa_readonly="readonly=\"true\" ";
							  $P4Activa_readonly="readonly=\"true\" ";
							  $P5Activa_readonly="readonly=\"true\" ";
							  $P6Activa_readonly="readonly=\"true\" ";
							  
							  $P1Activa_class="textfieldLectura";
							  $P2Activa_class="textfieldLectura";
							  $P3Activa_class="textfieldLectura";
							  $P4Activa_class="textfieldLectura";
							  $P5Activa_class="textfieldLectura";
							  $P6Activa_class="textfieldLectura";
						 
						 }
						 
						 }
							  ?>

                          <input name="FechaLectura" type="text" class="textfield" id="fecha" value="<?=$fecha_lectura?>" size="12" maxlength="10"></td>
                          <td valign="top" class="arial14">
                            <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 330'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>&nbsp;
                            <input name="NumeroContador" type="text" class="textfield" id="NumeroContador" size="12" maxlength="20" value="<?=$num_contador?>"></td>
                        </tr>
                      </table>
                      </fieldset></td>
                  </tr>  				  

				  <tr class="arial12"> 
                    <td height="10" class="arial14"></td>
                  </tr>

<!--LELELE APARTADO NUEVO-->                  

 <tr class="arial12"> 
                    <td class="arial14"><fieldset>
                      <legend class="arialblack14"><?=$oficina_lecturas1?></legend>
                      
	                  <div class="columna_formularios_lectura_simple separacion_columnas_lecturas_simple"><?=$oficina_lecturas5?></div>
    	              <div class="columna_formularios_lectura_simple">
                      	&nbsp;&nbsp;<input type="text" name="lectura_unica" size="12" maxlength="12" <?=$readonly20?> class="<?=$class20?>" value="<?=$lectura_unica?>" />
                      </div>                      
                                           
<!--LELELE APARTADO NUEVO-->                  
              
	          <tr class="arial12"> 
	          	<td height="10" class="arial14"></td>
	          </tr>
          
<!--APARTADO NUEVO-->                  
				<tr>
<!--LOLOLO-->                
				   <td width="540" height="145" valign="top"class="arial14"><fieldset >
					 <legend class="arialblack14"><?=$oficina_lecturas2?></legend>
                  
			   <table width="100%" border="0" cellpadding="0" cellspacing="0">
		       <tr>		       
            	    <td width="83" height="18"></td>
                	<td width="164"></td>
			        <td width="59"></td>
			        <td width="152"></td>
		        </tr>
                        
            <div class="margen_primera_columna_lecturas_multiples columna_formulario_lecturas_multiples separacion_columnas_lecturas_multiples">
            	<div class="posicion_cabeceras_lecturas_multiples"><strong><?=$oficina_lecturas3?></strong></div>
    			<div class="separacion_textos_lecturas_multiples"><?=$oficina_lecturas4?></div>                
       			<div class="separacion_textos_lecturas_multiples"><?=$oficina_lecturas5?></div>
       			<div class="separacion_textos_lecturas_multiples"><?=$oficina_lecturas6?></div>                
                
<!--NOTA: COMO ESTOS TEXTOS SON FIJOS, NO SE HAN INTRODUCIDO EN LA TABLA DE IDIOMAS-->                
       			<div class="separacion_textos_lecturas_multiples">P4</div>                                
       			<div class="separacion_textos_lecturas_multiples">P5</div>                                
       			<div>P6</div                                                                
             ></div><!--<div class="columna_formulario_lecturas_multiples separacion_columnas_lecturas_multiples">-->
            
            <div class="columna_formulario_lecturas_multiples separacion_columnas_lecturas_multiples">
            	<div class="posicion_cabeceras_lecturas_multiples"><strong><?=$oficina_lecturas7?></strong></div>
	  			<div class="separacion_campos_lecturas_multiples"><input type="text" name="P1_activa" size="12" maxlength="12" <?=$P1Activa_readonly?> class="<?=$P1Activa_class?>" value="<?=$p1_activa?>" /></div>
                <div class="separacion_campos_lecturas_multiples"><input type="text" name="P2_activa" size="12" maxlength="12" <?=$P2Activa_readonly?> class="<?=$P2Activa_class?>" value="<?=$p2_activa?>" /></div>
                <div class="separacion_campos_lecturas_multiples"><input type="text" name="P3_activa" size="12" maxlength="12" <?=$P3Activa_readonly?> class="<?=$P3Activa_class?>" value="<?=$p3_activa?>" /></div>                
                <div class="separacion_campos_lecturas_multiples"><input type="text" name="P4_activa" size="12" maxlength="12" <?=$P4Activa_readonly?> class="<?=$P4Activa_class?>" value="<?=$p4_activa?>" /></div>
                <div class="separacion_campos_lecturas_multiples"><input type="text" name="P5_activa" size="12" maxlength="12" <?=$P5Activa_readonly?> class="<?=$P5Activa_class?>" value="<?=$p5_activa?>" /></div>
                <div><input type="text" name="P6_activa" size="12" maxlength="12" <?=$P6Activa_readonly?> class="<?=$P6Activa_class?>" value="<?=$p6_activa?>" /></div>             
             </div><!--<div class="columna_formulario_lecturas_multiples separacion_columnas_lecturas_multiples">-->
                
            <div class="columna_formulario_lecturas_multiples separacion_columnas_lecturas_multiples">
            	<div class="posicion_cabeceras_lecturas_multiples"><strong><?=$oficina_lecturas8?></strong></div>
	  			<div class="separacion_campos_lecturas_multiples"><input type="text" name="P1_reactiva" size="12" maxlength="12" <?=$P1Activa_readonly?> class="<?=$P1Activa_class?>" value="<?=$p1_reactiva?>" /></div>
                <div class="separacion_campos_lecturas_multiples"><input type="text" name="P2_reactiva" size="12" maxlength="12" <?=$P2Activa_readonly?> class="<?=$P2Activa_class?>" value="<?=$p2_reactiva?>" /></div>
                <div class="separacion_campos_lecturas_multiples"><input type="text" name="P3_reactiva" size="12" maxlength="12" <?=$P3Activa_readonly?> class="<?=$P3Activa_class?>" value="<?=$p3_reactiva?>" /></div>
                <div class="separacion_campos_lecturas_multiples"><input type="text" name="P4_reactiva" size="12" maxlength="12" <?=$P4Activa_readonly?> class="<?=$P4Activa_class?>" value="<?=$p4_reactiva?>" /></div>
                <div class="separacion_campos_lecturas_multiples"><input type="text" name="P5_reactiva" size="12" maxlength="12" <?=$P5Activa_readonly?> class="<?=$P5Activa_class?>" value="<?=$p5_reactiva?>" /></div>
                <div><input type="text" name="P6_reactiva" size="12" maxlength="12" <?=$P6Activa_readonly?> class="<?=$P6Activa_class?>" value="<?=$p6_reactiva?>" /></div>             
             </div><!--<div class="columna_formulario_lecturas_multiples separacion_columnas_lecturas_multiples">-->               
                
			<div class="columna_formulario_lecturas_multiples">
            	<div class="posicion_cabeceras_lecturas_multiples"><strong><?=$oficina_lecturas9?></strong></div>
	  			<div class="separacion_campos_lecturas_multiples"><input type="text" name="P1_maximetro" size="12" maxlength="12" <?=$P1Activa_readonly?> class="<?=$P1Activa_class?>" value="<?=$p1_maximetro?>" /></div>
                <div class="separacion_campos_lecturas_multiples"><input type="text" name="P2_maximetro" size="12" maxlength="12" <?=$P2Activa_readonly?> class="<?=$P2Activa_class?>" value="<?=$p2_maximetro?>" /></div>
                <div class="separacion_campos_lecturas_multiples"><input type="text" name="P3_maximetro" size="12" maxlength="12" <?=$P3Activa_readonly?> class="<?=$P3Activa_class?>" value="<?=$p3_maximetro?>" /></div>
                <div class="separacion_campos_lecturas_multiples"><input type="text" name="P4_maximetro" size="12" maxlength="12" <?=$P4Activa_readonly?> class="<?=$P4Activa_class?>" value="<?=$p4_maximetro?>" /></div>
                <div class="separacion_campos_lecturas_multiples"><input type="text" name="P5_maximetro" size="12" maxlength="12" <?=$P5Activa_readonly?> class="<?=$P5Activa_class?>" value="<?=$p5_maximetro?>" /></div>
                <div><input type="text" name="P6_maximetro" size="12" maxlength="12" <?=$P6Activa_readonly?> class="<?=$P6Activa_class?>" value="<?=$p6_maximetro?>" /></div>
            </div><!--<div class="columna_formulario_lecturas_multiples">-->              
            
            <div class="limpiar"></div>                                    		
                        	                		    
	        </table>
		   </fieldset>			</td>
          </tr>
		 <tr>
		   <td height="10"></td>
          </tr>
                                    
<!--APARTADO NUEVO-->                   
                   
				  <tr><td><fieldset>
                      <legend class="arialblack14"> 
                      <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto =152'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                       </legend>
                    <table width="469" align="center">
                        <tr> 
                          <td width="461" align="center" class="arial12"> <textarea name="Observaciones" cols="55" rows="5" id="Observaciones" class="arial14"></textarea> 
                          </td>
                        </tr>
                      </table>
                      </fieldset></td></tr>
                  <tr class="arial12"> 
                    <td height="10" class="arial14"></td>
                  </tr>
                  <tr class="arial12"> <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 153'); $rowTexto = mysql_fetch_array($resultTexto);?>
                 
                    <td height="30" align="center" class="arial14"><input name="Submit" type="Submit" id="Submit" value="<?php echo($rowTexto['Texto']);?>"> 
                    <br /><br />

                    </td>
                  </tr>
                </table></td>
            </tr>
          </table>
		  </form>

<!--VERSION VIEJA-->

        	
		</div><!--<div class="contenido_seccion_oficina">-->                        
		
        
        <div class="limpiar"></div>
                        	
<!--Por ultimo aparecera el pie de la web-->
<?php 
	include("../includes/pie.php");
    
//Cuando se pulse el boton aceptar del formulario, recogeremos el valor del boton
	if (isset($_POST['Submit'])) 
	{	
//Mostraremos el mensaje correspondiente dependiendo de si el proceso ha ido bien o se ha encontrado algun problema		
		switch($error)
		{
//Si todo ha ido bien se informara al usuario		
			case 0:
		
//LALALA SE ESCRIBE EN LA TABLA DE LECTURAS!!!!

//Almacenamos en la tabla de incidencias la informacion facilitada por el usuario		
				$insertar_lectura="INSERT INTO LecturaClientes (
`NumeroCliente`,`FechaLectura`,`NumeroContador`,`LecturaDomestica`,`P1Activa`,`P1Reactiva`,`P1Maximetro`,`P2Activa`,`P2Reactiva`,`P2Maximetro`,`P3Activa`,`P3Reactiva`,`P3Maximetro`,`P4Activa`,`P4Reactiva`,`P4Maximetro`,`P5Activa`,`P5Reactiva`,`P5Maximetro`,`P6Activa`,`P6Reactiva`,`P6Maximetro`,`FechaRegistro`,`HoraRegistro`,`Observaciones`)
				
			VALUES ('".$_POST["CodigoCliente"]."', '".fecha_mysql($fecha_lectura)."', '".$num_contador."', '".$lectura_domestica."', '".$p1_activa."', '".$p1_reactiva."', '".$p1_maximetro."', '".$p2_activa."', '".$p2_reactiva."', '".$p2_maximetro."', '".$p3_activa."', '".$p3_reactiva."', '".$p3_maximetro."', '".$p4_activa."', '".$p4_reactiva."', '".$p4_maximetro."', '".$p5_activa."', '".$p5_reactiva."', '".$p5_maximetro."', '".$p6_activa."', '".$p6_reactiva."', '".$p6_maximetro."', '".date("Y-m-d")."', '".date("Y-m-d H:i:s")."', '".$observaciones."')";				
												
//Ejecutamos la insercion del registro
				$result=mysql_query($insertar_lectura);

//LALALA PASAR LOS TEXTOS A LA TABLA DE IDIOMAS Y EJECUTAR TODOS LOS ERRORES EN LOS 5 IDIOMAS
		
//Reiniciamos las variables, para que cuando se haga la redireccion el formulario aparezca vacio de nuevo			
				$fecha_lectura="";		
				$num_contador="";
				
				$p1_activa="";
				$p1_reactiva="";
				$p1_maximetro="";
		
				$p2_activa="";
				$p2_reactiva="";
				$p2_maximetro="";
						
				$p3_activa="";
				$p3_reactiva="";
				$p3_maximetro="";
				
				$p4_activa="";
				$p4_reactiva="";
				$p4_maximetro="";
				
				$p5_activa="";
				$p5_reactiva="";
				$p5_maximetro="";
				
				$p6_activa="";
				$p6_reactiva="";
				$p6_maximetro="";
			
				$observaciones="";
			
//Se informa al usuario del exito del envio de los datos				
				MsgBox($lectura_ok);
				include("../includes/email_lecturas.php");
				redirigir("oficina_lecturas.php?id=".$_SESSION["idioma"]);								
												
			break;

//Error por no rellenar la fecha o el numero de contador
			case 1:
				MsgBox($error_rellenar);							
			break;
//Error en la fecha
			case 2:
				MsgBox($error_fecha);					
			break;
		
//Error de no escribir al menos una lectura		
			case 3:
				MsgBox($error_lectura);
			break;								

//Error de escribir lecturas en un solo periodo y multiples periodos
			case 4:
				MsgBox($error_varias_lecturas);									
			break;										
		
//Error de escribir lecturas en un solo periodo y multiples periodos
			case 5:
				MsgBox($error_numero_lectura);									
			break;												

//Error fecha posterior a la fecha actual
			case 6:
				MsgBox($error_fecha_posterior);									
			break;	
										
		}//switch($error)		
	}//if (isset($_POST['Submit'])) 	    
}//else ($_SESSION['usuario'])
?>        
    </div><!--<div id="central_oficina"-->
</body>
</html>
