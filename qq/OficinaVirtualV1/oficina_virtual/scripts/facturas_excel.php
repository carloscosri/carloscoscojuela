<?php
include("../../includes/sesion.php");
//Conexion a la db
$conEmp = mysql_connect($ruta_sevidor_bd, $usuario_bd, $contrasena_bd);
mysql_select_db($nombre_bd, $conEmp);

//Recogida de parametros
$nif=$_GET['nif'];
$fechaInicio=$_GET['FechaInicial'];
$fechaFin=$_GET['FechaFinal'];

//Validacion fechas
if ($fechaInicio > $fechaFin){
	echo "La fecha inicial no puede ser posterior que la fecha final";
	?>
    <!-- Redireccion en caso de error -->
	<script type="text/javascript">
		function redirection(){ window.location ="../oficina_presentacion_mp.php"; }  
		setTimeout ("redirection()", 3000); //tiempo en milisegundos
	</script>
<?php
} else {
	
//Consulta BBDD
$sql ="SELECT * FROM FacturasImprimir where Nif='".$nif."' and Fecha>'".$fechaInicio."' and Fecha<'".$fechaFin."' ";
 
$r = mysql_query( $sql ) or trigger_error( mysql_error($conn), E_USER_ERROR );
$return = '';
if( mysql_num_rows($r)>0){
    $return .= '<table border=1>';
    $cols = 0;
    while($rs = mysql_fetch_row($r)){
        $return .= '<tr>';
        if($cols==0){
            $cols = sizeof($rs);
            $cols_names = array();
            for($i=0; $i<$cols; $i++){
                $col_name = mysql_field_name($r,$i);
                $return .= '<th>'.htmlspecialchars($col_name).'</th>';
                $cols_names[$i] = $col_name;
            }
            $return .= '</tr><tr>';
        }

        for ($i=0; $i<$cols; $i++){
            if($cols_names[$i] == 'fechaAlta'){ 
                $return .= '<td>'.htmlspecialchars(date('d/m/Y H:i:s',$rs[$i])).'</td>';
            }else if($cols_names[$i] == 'activo'){ 
                $return .= '<td>'.htmlspecialchars( $rs[$i]==1? 'SI':'NO' ).'</td>';
            }else{
                $return .= '<td>'.htmlspecialchars($rs[$i]).'</td>';
            }
        }
        $return .= '</tr>';
    }
    $return .= '</table>';
    mysql_free_result($r);
}

//Generar archivo excel
header("Content-type: application/vnd-ms-excel; charset=iso-8859-1");
header("Content-Disposition: attachment; filename=ResumenFacturas_".$nif."_".date('d-m-Y').".xls");
echo $return; 
}
?>