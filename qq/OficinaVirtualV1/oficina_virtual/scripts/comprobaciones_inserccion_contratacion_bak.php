<?php
//NOTA: EN ESTA OCASION, EL ERROR SE MOSTRARA DESPUES DE LA CARGA DE LA PAGINA, SI NO POR ALGUN MOTIVO EN OCASIONES DESPUES DE DAR EL ERROR CESA LA CARGA DE LA PAGINA
	if(isset($_POST["submit"]))
	{				
//Por ultimo mostraremos el error correspondiente, en el caso de que haya fallado alguna de las verificaciones, o continuaremos con el proceso		
		switch($error)
		{
//Proceso OK	
			case 0:
//Antes de realizar el alta en la base de datos, deberemos de preparar los datos que no nos da el formulario

//En primer lugar el codigo de cliente. Se obtiene leyendo de la tabla ReferenciaAltas el ultimo codigo escrito y sumandole 1
				$rs_ultima_alta = mysql_query('SELECT max(Referencia) as ultima_referencia FROM ReferenciaAltas');								

//Si la tabla tiene contentenido se calculara el nuevo codigo de usuario				
				if($registro_ultima_alta=mysql_fetch_array($rs_ultima_alta))
				{
					$cod_usuario=intval($registro_ultima_alta["ultima_referencia"])+1;										
				}//if($registro_ultima_alta=mysql_fetch_array($rs_ultima_alta))
				
//Si la tabla esta vacia el codigo de usuario sera el 1
				else
				{
					$cod_usuario=1;
				}//else ($registro_ultima_alta=mysql_fetch_array($rs_ultima_alta))
								
//En cualquiera de los casos escribiremos en la tabla la nueva referencia
				$insertar_referencia="INSERT INTO ReferenciaAltas (`Referencia`) VALUES ('".$cod_usuario."');";
//Ejecutamos la insercion del registro
				$resultado_inserccion=mysql_query($insertar_referencia);									

//La forma de pago envia un indice y no la descripcion de la forma de pago, ahora lo sustituiremos antes de dar el alta
				switch($forma_pago)
				{
					case 1:
						$forma_pago="Domiciliacion";
					break;
					
					case 2:
						$forma_pago="No domiciliado";					
					break;					
				}//switch($forma_pago)

//El idioma de facturacion envia un indice y no la descripcion del idioma, ahora lo sustituiremos antes de dar el alta
				switch($idioma_factura)
				{
					case 0:
						$idioma_factura="Castellano";
					break;
					
					case 1:
						$idioma_factura="Catalan";					
					break;	

					case 2:
						$idioma_factura="Euskera";					
					break;	

					case 3:
						$idioma_factura="Gallego";					
					break;	

					case 4:
						$idioma_factura="Bable (Asturias)";					
					break;																				
				}//switch($idioma_factura)

//Los equipos de medida envia un indice y no la descripcion del equipo de medida, ahora lo sustituiremos antes de dar el alta
				switch($equipos_medida)
				{
					case 1:
						$equipos_medida="Alquiler Empresa";
					break;
					
					case 2:
						$equipos_medida="Propiedad Cliente";					
					break;					
				}//switch($equipos_medida)
								
//Tambien preparamos el numero de la cuenta que sera la concatenacion de todos los campos que lo forman
				$num_cuenta_completo=$num_cuenta_banco.$num_cuenta_sucursal.$num_cuenta_digito_control.$num_cuenta_cuenta;

//Recogemos el campo de tarifa contratada. Como el value del combo tiene el codigo de la tarifa y no la descripcion, la tendremos que consultar				
				$cod_tarifa_contratada=$_POST['TarifaContratada'];
				
				$rs_tarifas = mysql_query("SELECT Tarifa FROM TarifasDetalles WHERE IdTarifa='".$cod_tarifa_contratada."';");				
				$registro_tarifas=mysql_fetch_array($rs_tarifas);

//Como la potencia normalizada, tiene el codigo de su tarifa concatenado con un signo de "+" habrá que separarlos
				
				$potencia_normalizada_depurada=explode("+",$potencia_normalizada);
								
//Esta variable almacenara el valor que se grabara en la base de datos
				$discriminador="";
								
				if($_POST["Discriminador"]!="-1")
				{
					$discriminador="a";				
				}//if($_POST["Discriminador"]!="-1")
				
//Como el valor que da la entidad bancaria es una composicion del nombre de la entidad + su codigo deberemos de separarlo
				$partes_entidad_bancaria=explode("+",$entidad_bancaria);																																		

//Si se ha accedido a la seccion de Bono Social
				if ($_SESSION["seccion_origen_contratacion"]==4)
				{
//El estado del bono social de facturacion envia un indice y no la descripcion del estado, ahora lo sustituiremos antes de dar el alta
					switch($estado_bono_social)
					{					
						case 0:
							$estado_bono_social="Jubilado";					
						break;	

						case 1:
							$estado_bono_social="Desempleado";					
						break;	

						case 2:
							$estado_bono_social="Minusvalido";					
						break;	

						case 3:
							$estado_bono_social="Familia Numerosa";					
						break;																				
					}//switch($estado_bono_social)
				}//if ($_SESSION["seccion_origen_contratacion"]==4)

//Grabaremos la fecha del bono social, solo si se ha seleccionado este
			if ($_SESSION["seccion_origen_contratacion"]==4)
			{
				$fecha_bono_social=date("Y-m-d");
			}//if ($_SESSION["seccion_origen_contratacion"]==4)
			
//Ahora consultaremos la tabla de tarifas para saber la descripcion de la tarifa				
//El campo CUPS, en principio no se elimina de la tabla, pero se deja vacio

//Dependiendo de si el usuario esta o no esta registrado en la web, se hara un tipo de inserccion u otra
				if($_SESSION["contratacion_sin_registro"]==1)												
				{				
//EN ESTA OCASION, COMO EL CLIENTE NO ESTA REGISTRADO EN LA WEB NO TIENE UN NOMBRE DE USUARIO, NI PASSWORD, NI DNI ASIGANDOS POR LO TANTO ESOS CAMPOS QUEDARAN VACIOS												
					$insertar_alta="INSERT INTO `AltaNueva` (`CodigoCliente`,`SuministroCiudad`, `SuministroProvincia`, `SuministroCalle`, `SuministroNumero`, `SuministroExtension`, `SuministroExtension2`, `SuministroCP`, `SuministroTelefono1`, `SuministroTelefono2`, `SuministroFax`, `SuministroEmail`, `TitularNombre`, `TitularCalle`, `TitularNumero`, `TitularExtension`, `TitularAclarador`, `TitularPoblacion`, `TitularProvincia`, `TitularTelefono1`, `TitularTelefono2`, `TitularFax`, `TitularEmail`, `TitularDNI`, `FacturaNombre`, `FacturaCalle`, `FacturaNumero`, `FacturaExtension`, `FacturaAclarador`, `FacturaPoblacion`, `FacturaProvincia`, `FacturaTelefono1`, `FacturaTelefono2`, `FacturaFax`, `FacturaEmail`, `FacturaDNI`, `PagadorNombre`, `PagadorCalle`, `PagadorNumero`, `PagadorExtension`, `PagadorAclarador`, `PagadorPoblacion`, `PagadorProvincia`, `PagadorTelefono1`, `PagadorTelefono2`, `PagadorFax`, `PagadorEmail`, `PagadorDNI`, `RepresentanteEmpresa`, `DNIRepresentante`, `CUPS`, `FormaPago`, `BancoDomiciliado`, `NumeroCuenta`,
`IdiomaFactura`,`TarifaContratada`, `PotenciaContratada`, `TipoTension`, `Discriminador`, `TipoAlquiler`, `Maximetro`, `Modo`, `Observaciones`, `FechaRegistro`, `HoraRegistro`, `Fases`, `Reactiva`, `P1Activa`, `P1Reactiva`, `P1Maximetro`, `P2Activa`, `P2Reactiva`, `P2Maximetro`, `P3Activa`, `P3Reactiva`, `P3Maximetro`, `P4Activa`, `P4Reactiva`, `P4Maximetro`, `P5Activa`, `P5Reactiva`, `P5Maximetro`, `P6Activa`, `P6Reactiva`, `P6Maximetro`,`EstadoBonoSocial`,`FechaVigenciaBonoSocial`,`Comecializadora`,`Distribuidora`) VALUES ('".$cod_usuario."', '".$suministro_poblacion."', '".$suministro_provincia."', '".$suministro_calle."', '".$suministro_numero."', '".$suministro_extension."', '".$suministro_aclarador."', '".$suministro_cp."','".$suministro_telefono."', '".$suministro_movil."', '".$suministro_fax."', '".$suministro_email."', '".$titular_nombre."','".$titular_calle."','".$titular_numero."' , '".$titular_extension."', '".$titular_aclarador."', '".$titular_poblacion."', '".$titular_provincia."', '".$titular_telefono."', '".$titular_movil."', '".$titular_fax."', '".$titular_email."', '".$titular_dni."', '".$factura_nombre."', '".$factura_calle."', '".$factura_numero."', '".$factura_extension."', '".$factura_aclarador."', '".$factura_poblacion."', '".$factura_provincia."', '".$factura_telefono."', '".$factura_movil."', '".$factura_fax."', '".$factura_email."', '".$factura_dni."', '".$pagador_nombre."', '".$pagador_calle."', '".$pagador_numero."', '".$pagador_extension."', '".$pagador_aclarador."', '".$pagador_poblacion."', '".$pagador_provincia."', '".$pagador_telefono."', '".$pagador_movil."', '".$pagador_fax."', '".$pagador_email."', '".$pagador_dni."', '".$representante_nombre."', '".$representante_dni."', '".$cups."', '".$forma_pago."', '".$partes_entidad_bancaria[0]."', '".$num_cuenta_completo."','".$idioma_factura."','".$registro_tarifas['Tarifa']."','".$potencia_normalizada_depurada[0]."', '".$_POST["TipoTension"]."', '".$discriminador."','".$equipos_medida."','".$_POST["Maximetro"]."', '".$_POST["Modo"]."', '".$_POST["Observaciones"]."', '".date("Y-m-d")."', '".date("Y-m-d H:i:s")."','".$_POST["Fases"]."', '".$_POST["Reactiva"]."', '".$_POST["P1_activa"]."', '".$_POST["P1_reactiva"]."', '".$_POST["P1_maximetro"]."', '".$_POST["P2_activa"]."', '".$_POST["P2_reactiva"]."', '".$_POST["P2_maximetro"]."', '".$_POST["P3_activa"]."', '".$_POST["P3_reactiva"]."', '".$_POST["P3_maximetro"]."', '".$_POST["P4_activa"]."', '".$_POST["P4_reactiva"]."', '".$_POST["P4_maximetro"]."', '".$_POST["P5_activa"]."', '".$_POST["P5_reactiva"]."', '".$_POST["P5_maximetro"]."', '".$_POST["P6_activa"]."', '".$_POST["P6_reactiva"]."', '".$_POST["P6_maximetro"]."', '".$estado_bono_social."', '".$fecha_bono_social."','".$comercializadora."','".$distribuidora."');";				


//Llamamos a la funcion javascript que borra todos los datos del formulario
				echo("<script type='application/javascript' language='javascript'>");
					echo("reiniciar_campos_no_registrado();");
				echo("</script>");				
								
				}//if($_SESSION["contratacion_sin_registro"]==1)												
				else
				{
					$insertar_alta="INSERT INTO `AltaNueva` (`CodigoCliente`, `Usuario`, `Password`, `DNI`, `SuministroCiudad`, `SuministroProvincia`, `SuministroCalle`, `SuministroNumero`, `SuministroExtension`, `SuministroExtension2`, `SuministroCP`, `SuministroTelefono1`, `SuministroTelefono2`, `SuministroFax`, `SuministroEmail`, `TitularNombre`, `TitularCalle`, `TitularNumero`, `TitularExtension`, `TitularAclarador`, `TitularPoblacion`, `TitularProvincia`, `TitularTelefono1`, `TitularTelefono2`, `TitularFax`, `TitularEmail`, `TitularDNI`, `FacturaNombre`, `FacturaCalle`, `FacturaNumero`, `FacturaExtension`, `FacturaAclarador`, `FacturaPoblacion`, `FacturaProvincia`, `FacturaTelefono1`, `FacturaTelefono2`, `FacturaFax`, `FacturaEmail`, `FacturaDNI`, `PagadorNombre`, `PagadorCalle`, `PagadorNumero`, `PagadorExtension`, `PagadorAclarador`, `PagadorPoblacion`, `PagadorProvincia`, `PagadorTelefono1`, `PagadorTelefono2`, `PagadorFax`, `PagadorEmail`, `PagadorDNI`, `RepresentanteEmpresa`, `DNIRepresentante`, `CUPS`, `FormaPago`, `BancoDomiciliado`, `NumeroCuenta`,
`IdiomaFactura`,`TarifaContratada`, `PotenciaContratada`, `TipoTension`, `Discriminador`, `TipoAlquiler`, `Maximetro`, `Modo`, `Observaciones`, `FechaRegistro`, `HoraRegistro`, `Fases`, `Reactiva`, `P1Activa`, `P1Reactiva`, `P1Maximetro`, `P2Activa`, `P2Reactiva`, `P2Maximetro`, `P3Activa`, `P3Reactiva`, `P3Maximetro`, `P4Activa`, `P4Reactiva`, `P4Maximetro`, `P5Activa`, `P5Reactiva`, `P5Maximetro`, `P6Activa`, `P6Reactiva`, `P6Maximetro`,`EstadoBonoSocial`,`FechaVigenciaBonoSocial`) VALUES ('".$cod_usuario."', '".$_SESSION['usuario']."', '".$_SESSION['password']."', '".$_SESSION['DNI']."', '".$suministro_poblacion."', '".$suministro_provincia."', '".$suministro_calle."', '".$suministro_numero."', '".$suministro_extension."', '".$suministro_aclarador."', '".$suministro_cp."','".$suministro_telefono."', '".$suministro_movil."', '".$suministro_fax."', '".$suministro_email."', '".$titular_nombre."','".$titular_calle."','".$titular_numero."' , '".$titular_extension."', '".$titular_aclarador."', '".$titular_poblacion."', '".$titular_provincia."', '".$titular_telefono."', '".$titular_movil."', '".$titular_fax."', '".$titular_email."', '".$titular_dni."', '".$factura_nombre."', '".$factura_calle."', '".$factura_numero."', '".$factura_extension."', '".$factura_aclarador."', '".$factura_poblacion."', '".$factura_provincia."', '".$factura_telefono."', '".$factura_movil."', '".$factura_fax."', '".$factura_email."', '".$factura_dni."', '".$pagador_nombre."', '".$pagador_calle."', '".$pagador_numero."', '".$pagador_extension."', '".$pagador_aclarador."', '".$pagador_poblacion."', '".$pagador_provincia."', '".$pagador_telefono."', '".$pagador_movil."', '".$pagador_fax."', '".$pagador_email."', '".$pagador_dni."', '".$representante_nombre."', '".$representante_dni."', '', '".$forma_pago."', '".$partes_entidad_bancaria[0]."', '".$num_cuenta_completo."','".$idioma_factura."','".$registro_tarifas['Tarifa']."','".$potencia_normalizada_depurada[0]."', '".$_POST["TipoTension"]."', '".$discriminador."','".$equipos_medida."','".$_POST["Maximetro"]."', '".$_POST["Modo"]."', '".$_POST["Observaciones"]."', '".date("Y-m-d")."', '".date("Y-m-d H:i:s")."','".$_POST["Fases"]."', '".$_POST["Reactiva"]."', '".$_POST["P1_activa"]."', '".$_POST["P1_reactiva"]."', '".$_POST["P1_maximetro"]."', '".$_POST["P2_activa"]."', '".$_POST["P2_reactiva"]."', '".$_POST["P2_maximetro"]."', '".$_POST["P3_activa"]."', '".$_POST["P3_reactiva"]."', '".$_POST["P3_maximetro"]."', '".$_POST["P4_activa"]."', '".$_POST["P4_reactiva"]."', '".$_POST["P4_maximetro"]."', '".$_POST["P5_activa"]."', '".$_POST["P5_reactiva"]."', '".$_POST["P5_maximetro"]."', '".$_POST["P6_activa"]."', '".$_POST["P6_reactiva"]."', '".$_POST["P6_maximetro"]."', '".$estado_bono_social."', '".$fecha_bono_social."');";
				}//else($_SESSION["contratacion_sin_registro"]==1)												
				
				$ejecutar_insercion_alta=mysql_query($insertar_alta);

															
//Llamamos a la funcion javascript que borra todos los datos del formulario
				echo("<script type='application/javascript' language='javascript'>");
					echo("reiniciar_formulario(".$_SESSION["seccion_origen_contratacion"].");");
				echo("</script>");				

				
												
				MsgBox($alta_ok);											
			break;

//Error por no rellenar los campos obligatorios
			case 1:
				MsgBox($error_rellenar);				
			break;	

//Error por caracteres no numericos en telefonos o faxes						
			case 2:
				MsgBox($error_numeros);							
			break;	

//Error en un e-mail			
			case 3:
				MsgBox($error_email);										
			break;	

			case 4:
				MsgBox($error_dni);													
			break;										
							
//Error por no rellenar la entidad bancaria o el numero de cuenta con forma de pago domiciliación			
			case 5:
				MsgBox($error_datos_bancarios);			
			break;										
			
//Error en el numero de cuenta bancaria
			case 6:
				MsgBox($error_num_cuenta);						
			break;						

//Error por rellenar parcialmente los datos de facturacion o los del pagador
			case 7:
				MsgBox($error_datos_incompletos);									
			break;											

//Error por rellenar parcialmente los datos del representante de la empresa
			case 8:
				MsgBox($error_datos_representante);												

//Error por escribir el CUPS de forma erronea
			case 9:
				MsgBox($error_cups);																
			break;																			
			
		}//switch($error)
	}//if (isset($_POST["submit"]))

//En el caso de que se haya accedido a la seccion tarifa social, le damos valor a las demas variables que vendrán preestablecidas.
//Para ello llamaremos a la funcion javascript que actualiza los campos pertinentes. No se puede realizar la llamada al mismo tiempo que se realizan el resto de operaciones que dependen de la seccion activa, porque si no los valores por defecto de los componentes sustituyen a los dado por la funcion
	if($_SESSION["seccion_origen_contratacion"]==4)
	{	
		echo("<script type='text/javascript' language='javascript'>");
			echo("valores_bono_social();");
		echo("</script>");	
	}//if($_SESSION["seccion_origen_contratacion"]==2)		
	
//En el caso de que se haya accedido a la seccion de > 15kw/h, le damos valor a las demas variables que vendrán preestablecidas.
//Para ello llamaremos a la funcion javascript que actualiza los campos pertinentes. No se puede realizar la llamada al mismo tiempo que se realizan el resto de operaciones que dependen de la seccion activa, porque si no los valores por defecto de los componentes sustituyen a los dado por la funcion
	if($_SESSION["seccion_origen_contratacion"]==2)
	{	
		echo("<script type='text/javascript' language='javascript'>");
			echo("valores_mas_15kw();");
		echo("</script>");	
	}//if($_SESSION["seccion_origen_contratacion"]==2)				
		
?>        
