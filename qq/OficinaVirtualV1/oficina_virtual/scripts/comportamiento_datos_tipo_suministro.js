//Esta funcion actualiza los periodos tarifarios segun la tarifa escogida. Actualmente solo surte efecto cuando estamos en la seccion >15kw/hs
function actualizar_periodos_tarifa(seccion_activa, t20a, t20dha, t21a, t21dha, t30a, t31a, t61a)
{		
	var tarifa_actual=document.form_alta.TarifaContratada.value;
	
	//alert("entrando en actualizar...");
	//alert("la tarifa es "+tarifa_actual+" y se compara con " + t20dha);
	
	if (tarifa_actual == t20a){
		document.form_alta.Discriminador.selectedIndex=0;
		document.form_alta.PeriodosTarifarios.selectedIndex=1;
		document.form_alta.TarifaContratada.selectedIndex=1;
		actualizar_tabla(seccion_activa);						
		document.form_alta.Reactiva.disabled = true;
		document.form_alta.Maximetro.disabled = true;										
		document.form_alta.PeriodosTarifariosb.value="Punta LLANO VALLE 20";
		
	}
	
	if (tarifa_actual == t20dha){
		document.form_alta.Discriminador.selectedIndex=1;
		document.form_alta.PeriodosTarifarios.selectedIndex=2;
		document.form_alta.TarifaContratada.selectedIndex=2;
		actualizar_tabla(seccion_activa);
		//Como cambian los periodos tarifarios habrá que habilitar los campos de rectiva y maximetro
		document.form_alta.Reactiva.disabled = false;
		document.form_alta.Maximetro.disabled = false;	
		document.form_alta.PeriodosTarifariosb.value="Punta LLANO VALLE 21";
				
	}
	
	if (tarifa_actual == t21a){
		document.form_alta.Discriminador.selectedIndex=0;
		document.form_alta.TarifaContratada.selectedIndex=1;
		document.form_alta.PeriodosTarifarios.selectedIndex=1
		actualizar_tabla(seccion_activa);					
		document.form_alta.Reactiva.disabled = true;
		document.form_alta.Maximetro.disabled = true;			
		document.form_alta.PeriodosTarifariosb.value="Punta LLANO VALLE 30";	
		
		//reiniciar_periodos_tarifarios();		
		document.form_alta.PeriodosTarifariosb.value="Punta LLANO VALLE 210";	
	}
	
	if (tarifa_actual==t21dha){
		document.form_alta.Discriminador.selectedIndex=1;	
		document.form_alta.PeriodosTarifarios.selectedIndex=2;
		document.form_alta.TarifaContratada.selectedIndex=2;
		actualizar_tabla(seccion_activa);
		//Como cambian los periodos tarifarios habrá que habilitar los campos de rectiva y maximetro				
		document.form_alta.Reactiva.disabled = false;
		document.form_alta.Maximetro.disabled = false;	
		document.form_alta.PeriodosTarifariosb.value="Punta LLANO VALLE 211";	
	}
	
	if (tarifa_actual == t30a){	
		document.form_alta.PeriodosTarifarios.selectedIndex=3;
		actualizar_tabla(seccion_activa);
		document.form_alta.PeriodosTarifariosb.value="Punta LLANO VALLE 30";	
	}
	
	if (tarifa_actual == t31a || tarifa_actual == t61a){
		document.form_alta.PeriodosTarifarios.selectedIndex=4;		
		actualizar_tabla(seccion_activa);
		document.form_alta.PeriodosTarifariosb.text="Punta LLANO VALLE 31";	
	}
	/*switch(tarifa_actual)
	{
		case "-1":
			document.form_alta.Discriminador.selectedIndex=0;
			reiniciar_periodos_tarifarios();
			document.form_alta.Reactiva.disabled = true;
			document.form_alta.Maximetro.disabled = true;											
		break;

//En los casos en los que el discriminador este activo, si se cambia la tarifa, también habrá que cambiar el valor del discriminador
//Tarifa 2.0A -> Discriminador No
		case t20a:		
			document.form_alta.Discriminador.selectedIndex=0;					
			document.form_alta.Reactiva.disabled = true;
			document.form_alta.Maximetro.disabled = true;										
			document.form_alta.PeriodosTarifarios.selectedIndex=1;
			document.form_alta.PeriodosTarifariosb.value="Punta LLANO VALLE 20";
			//Cambiamos la tarifa a la tarifa normal
            reiniciar_periodos_tarifarios();
			document.form_alta.TarifaContratada.selectedIndex=1;
			actualizar_tabla(seccion_activa);					
		break;

//Tarifa 2.0DHA -> Discriminador Si
		case t20dha:
			alert("entrando en case de actualizar...");	
			document.form_alta.Discriminador.selectedIndex=1;
			document.form_alta.PeriodosTarifarios.selectedIndex=2;
			actualizar_tabla(seccion_activa);
			//Como cambian los periodos tarifarios habrá que habilitar los campos de rectiva y maximetro
			document.form_alta.Reactiva.disabled = false;
			document.form_alta.Maximetro.disabled = false;	
			document.form_alta.PeriodosTarifariosb.value="Punta LLANO VALLE 21";
			document.form_alta.TarifaContratada.selectedIndex=2;										
		break;		

//Tarifa 2.1A -> Discriminador No
		case t21a:
			document.form_alta.Discriminador.selectedIndex=0;					
			document.form_alta.Reactiva.disabled = true;
			document.form_alta.Maximetro.disabled = true;			
			document.form_alta.PeriodosTarifariosb.value="Punta LLANO VALLE 30";	
			document.form_alta.PeriodosTarifarios.selectedIndex=1
			reiniciar_periodos_tarifarios();;
			document.form_alta.TarifaContratada.selectedIndex=1;
			actualizar_tabla(seccion_activa);		
			document.form_alta.PeriodosTarifariosb.value="Punta LLANO VALLE 210";			
		break;

//Tarifa 2.1DHA -> Discriminador Si
		case t21dha:
			document.form_alta.Discriminador.selectedIndex=1;	
			document.form_alta.PeriodosTarifarios.selectedIndex=2;
			actualizar_tabla(seccion_activa);
//Como cambian los periodos tarifarios habrá que habilitar los campos de rectiva y maximetro				
			document.form_alta.Reactiva.disabled = false;
			document.form_alta.Maximetro.disabled = false;	
			document.form_alta.TarifaContratada.selectedIndex=2;
			document.form_alta.PeriodosTarifariosb.value="Punta LLANO VALLE 211";			
		break;		
		
//Tarifa 3.0A -> DE P1 A P3
		case t30a:
		    document.form_alta.PeriodosTarifariosb.value="Punta LLANO VALLE 30";	
			document.form_alta.PeriodosTarifarios.selectedIndex=3;
			actualizar_tabla(seccion_activa);			
			document.form_alta.PeriodosTarifariosb.value="Punta LLANO VALLE 30";		
		break;
		
//Tarifa 3.1A y 6.1 -> DE P1 A P6
		case t31a || t61a:
			document.form_alta.PeriodosTarifarios.selectedIndex=4;		
			actualizar_tabla(seccion_activa);
			document.form_alta.PeriodosTarifariosb.text="Punta LLANO VALLE 31";
		break;
		
		default:
			alert('default');
		
	}//switch(tarifa_actual)*/
				
}//function actualizar_periodos_tarifa(seccion_activa)


//Esta funcion da valor al combobox de la tarifa, dependiendo de la potencia seleccionada. Recibe como parametro el id de la tarifa que le corresponde a la potencia

function asignar_tarifa(seccion_activa, t20a, t20dha, t21a, t21dha, t30a, t31a, t61a)
{	
	//alert("entrando en asignar tarifa...");
	//alert(t20dha);
//Esta variable almacenara el valor de la potencia seleccionado por el usuario	
	var potencia=document.form_alta.PotenciaNormalizada.value;	

//Esta variable almacenara el valor que tenga el discriminador
	var discriminador=document.form_alta.Discriminador.selectedIndex;
			
//Si se deja en blanco la potencia se ponen en blanco tambien el combo de la tarifa, el discriminador y se reinician los periodos tarifarios			
	if(potencia=="-1")	
	{			
		document.form_alta.Discriminador.selectedIndex=0;
		document.form_alta.TarifaContratada.selectedIndex=0;
		document.form_alta.TipoTension.selectedIndex=0;
		reiniciar_periodos_tarifarios();		
		
		document.form_alta.Reactiva.disabled = true;
		document.form_alta.Maximetro.disabled = true;										

	}//if(potencia=="-1")	
	else
	{			
//Dividimos el valor recibido de la potencia por el simbolo mas. Esta variable contendra en la primera posicion la potencia, en la segunda el ID de la tarifa que hay que asignarle, en la tercera la tension y en la cuarta las fases
		var partes_potencia = potencia.split('+');

//Esta variable controlara el id del array que contiene las tarifas que se esta comprobando	
		var id_tarifa=0;
	
		var num_tarifas_disponibles=document.form_alta.TarifaContratada.options.length;
	
//Ahora recorremos el combo de tarifas buscando la tarifa que hay que asignar	
		while (id_tarifa<num_tarifas_disponibles)
		{		
//Cuando encontramos la tarifa buscada, se selecciona en el combo de tarifas
			if(document.form_alta.TarifaContratada.options[id_tarifa].value==partes_potencia[1])
			{
				document.form_alta.TarifaContratada.selectedIndex=id_tarifa;
//Esta variable almacenara el ID de la tarifa que se ha seleccionado automaticamente, lo utilizaremos para asignar los demas valores			
				id_tarifa_seleccionada=document.form_alta.TarifaContratada.options[id_tarifa].value;
				//alert("id tarifa es "+ id_tarifa_seleccionada + " y se compara con "+ t20dha);
//Ahora dependiendo de la tarifa se actualizaran algunos de los valores del apartado Datos Tipo de Suministro	
				
				if (id_tarifa_seleccionada == t20a){
					if(discriminador==1){document.form_alta.TarifaContratada.selectedIndex=2;}//if(discriminador==1)
					document.form_alta.Maximetro.selectedIndex=0; //no
					document.form_alta.Modo.value="1";						
					document.form_alta.Reactiva.selectedIndex=0; //no	
				}
				
				if (id_tarifa_seleccionada == t20dha){
					//alert ("con if funciona")
					document.form_alta.Maximetro.selectedIndex=1; //si
					document.form_alta.Modo.value="2";
					document.form_alta.Reactiva.selectedIndex=1; //si
				}	
				
				if (id_tarifa_seleccionada == t30a){						
						document.form_alta.Maximetro.selectedIndex=1; //si
						document.form_alta.Modo.value="2";
						document.form_alta.Reactiva.selectedIndex=1; //si				
				}												

				if (id_tarifa_seleccionada == t31a){											
						document.form_alta.Maximetro.selectedIndex=1; //si
						document.form_alta.Modo.value="2";
						document.form_alta.Reactiva.selectedIndex=1; //si				
				}																

				if (id_tarifa_seleccionada == t21a){
												
						if(discriminador==1){document.form_alta.TarifaContratada.selectedIndex=2;}//if(discriminador==1)						
						document.form_alta.Maximetro.selectedIndex=0; //no
						document.form_alta.Modo.value="1";
						document.form_alta.Reactiva.selectedIndex=0; //no							
				}																				

				if (id_tarifa_seleccionada == t21dha){						
						document.form_alta.Maximetro.selectedIndex=1; //si
						document.form_alta.Modo.value="2";
						document.form_alta.Reactiva.selectedIndex=1; //si		
				}																							
				
				if (id_tarifa_seleccionada == t61a){					
						document.form_alta.Maximetro.selectedIndex=1; //si
						document.form_alta.Modo.value="2";
						document.form_alta.Reactiva.selectedIndex=1; //si	
				}
				
				/*switch(id_tarifa_seleccionada)
				{
//1.0			
/*
				case "10":
					document.form_alta.Maximetro.selectedIndex=0; //no
					document.form_alta.Modo.value="1";
					document.form_alta.TipoTension.selectedIndex=1; //2x220/380
					document.form_alta.Reactiva.selectedIndex=0; //no							
				break;
*/

//2.0A			
					/*case t20a:				
//Si el discriminador esta activado, deberemos de cambiar a la tarifa con discriminador 
						alert("entrando en el case...");																
						if(discriminador==1)
						{
							document.form_alta.TarifaContratada.selectedIndex=2;
						}//if(discriminador==1)
											
						document.form_alta.Maximetro.selectedIndex=0; //no
						document.form_alta.Modo.value="1";						
						document.form_alta.Reactiva.selectedIndex=0; //no												
					break;				

//2.0DHA			
					case t20dha:						
						document.form_alta.Maximetro.selectedIndex=1; //si
						document.form_alta.Modo.value="2";
						document.form_alta.Reactiva.selectedIndex=1; //si		
					break;	

//3.0A. En este caso tambien se actualizaran los periodos tarifarios DE P1 A P3
					case t30a:						
						document.form_alta.Maximetro.selectedIndex=1; //si
						document.form_alta.Modo.value="2";
						document.form_alta.Reactiva.selectedIndex=1; //si				
					break;												

//3.1A. En este caso tambien se actualizaran los periodos tarifarios DE P1 A P6
					case t31a:											
						document.form_alta.Maximetro.selectedIndex=1; //si
						document.form_alta.Modo.value="2";
						document.form_alta.Reactiva.selectedIndex=1; //si				
					break;																

//2.1A
					case t21a:
//Si el discriminador esta activado, deberemos de cambiar a la tarifa con discriminador 																
						if(discriminador==1)
						{
							document.form_alta.TarifaContratada.selectedIndex=2;
						}//if(discriminador==1)
												
						document.form_alta.Maximetro.selectedIndex=0; //no
						document.form_alta.Modo.value="1";
						document.form_alta.Reactiva.selectedIndex=0; //no							
					break;																				

//2.1DHA
					case t21dha:						
						document.form_alta.Maximetro.selectedIndex=1; //si
						document.form_alta.Modo.value="2";
						document.form_alta.Reactiva.selectedIndex=1; //si		
					break;																								
				
//6.1 En este caso tambien se actualizaran los periodos tarifarios DE P1 A P6
					case t61a:						
						document.form_alta.Maximetro.selectedIndex=1; //si
						document.form_alta.Modo.value="2";
						document.form_alta.Reactiva.selectedIndex=1; //si		
					break;																															
				}//switch(id_tarifa_seleccionada)*/
				
				break;
			}//if(document.form_alta.TarifaContratada.options[id_tarifa].value==partes_potencia[1])				
					
			id_tarifa++;				
		}//while (id_tarifa<num_tarifas_disponibles)

//Ahora se le dara valor al campo de la tension

//Esta variable controlara el id del array que contiene las tensiones que se esta comprobando	
		var id_tension=0;
			
		var num_tensiones_disponibles=document.form_alta.TipoTension.options.length;
				
//Ahora recorremos el combo de tarifas buscando la tarifa que hay que asignar	
		while (id_tension<num_tensiones_disponibles)
		{		
//Cuando encontramos la tension buscada, se selecciona en el combo de tensiones
			if(document.form_alta.TipoTension.options[id_tension].value==partes_potencia[2])
			{
				document.form_alta.TipoTension.selectedIndex=id_tension;
				break;
			}//if(document.form_alta.TipoTension.options[id_tension].value==partes_potencia[2])
			id_tension++;
		}//while (id_tension<num_tensiones_disponibles)

//Le damos el valor a las fases, que es uno de los valores que obtenemos al subdividir la potencia
		document.form_alta.Fases.value=partes_potencia[3];
		
//Por ultimo llamamos a la funcion que actualiza los periodos tarifarios en los casos en los que es necesario
		actualizar_periodos_tarifa(seccion_activa, t20a, t20dha, t21a, t21dha, t30a, t31a, t61a);
	}//else(potencia=="-1")	
}//function asignar_tarifa(seccion_activa)

//Esta funcion da valor al campo Modo dependiendo del valor del maximetro. Tambien actualiza los periodos tarifarios
function asignar_modo()
{
//Esta variable almacenara la opcion de periodos tarifarios que esta activa

//1-> P2 Llano
//2-> P1 Punta y P3 Valle
//3-> De P1 a P3
//4-> De P1 a P6
	var periodo_tarifario_activo=document.form_alta.PeriodosTarifarios.selectedIndex;
	
//Maximetro no -> Modo 1.
	if (document.form_alta.Maximetro.selectedIndex==0)
	{
		document.form_alta.Modo.value="1";
		
//Ahora dependiendo del periodo tarifario activo se actualizara la tabla de periodos tarifarios
		switch(periodo_tarifario_activo)
		{
			case 1:
				document.form_alta.P2_maximetro.value="No";
			break;
			
			case 2:
				document.form_alta.P1_maximetro.value="No";
				document.form_alta.P3_maximetro.value="No";			
			break;
			
			case 3:
				document.form_alta.P1_maximetro.value="No";
				document.form_alta.P2_maximetro.value="No";							
				document.form_alta.P3_maximetro.value="No";			
			break;
			
			case 4:
				document.form_alta.P1_maximetro.value="No";
				document.form_alta.P2_maximetro.value="No";							
				document.form_alta.P3_maximetro.value="No";
				document.form_alta.P4_maximetro.value="No";
				document.form_alta.P5_maximetro.value="No";							
				document.form_alta.P6_maximetro.value="No";							
			break;						
			
		}//switch(periodo_tarifario_activo)		
						
	}//if (document.form_alta.Maximetro.selectedIndex==0)
	
//Maximetro si -> Modo 1	
	else
	{
		document.form_alta.Modo.value="2";					
		
//Ahora dependiendo del periodo tarifario activo se actualizara la tabla de periodos tarifarios
		switch(periodo_tarifario_activo)
		{
			case 1:
				document.form_alta.P2_maximetro.value="Si";
			break;
			
			case 2:
				document.form_alta.P1_maximetro.value="Si";
				document.form_alta.P3_maximetro.value="Si";			
			break;
			
			case 3:
				document.form_alta.P1_maximetro.value="Si";
				document.form_alta.P2_maximetro.value="Si";							
				document.form_alta.P3_maximetro.value="Si";			
			break;
			
			case 4:
				document.form_alta.P1_maximetro.value="Si";
				document.form_alta.P2_maximetro.value="Si";							
				document.form_alta.P3_maximetro.value="Si";
				document.form_alta.P4_maximetro.value="Si";
				document.form_alta.P5_maximetro.value="Si";							
				document.form_alta.P6_maximetro.value="Si";							
			break;						
			
		}//switch(periodo_tarifario_activo)				
							
	}//else (document.form_alta.Maximetro.selectedIndex==0)	
}//function asignar_modo()

//Esta funcion cambia el valor de los periodos tarifarios cuando se cambia el valor de la reactiva
function asignar_periodos_reactiva()
{
//1-> P2 Llano
//2-> P1 Punta y P3 Valle
//3-> De P1 a P3
//4-> De P1 a P6
	var periodo_tarifario_activo=document.form_alta.PeriodosTarifarios.selectedIndex;
	
//Reactiva no
	if (document.form_alta.Reactiva.selectedIndex==0)
	{				
//Ahora dependiendo del periodo tarifario activo se actualizara la tabla de periodos tarifarios		
		switch(periodo_tarifario_activo)
		{
			case 1:
				document.form_alta.P2_reactiva.value="No";
			break;
			
			case 2:
				document.form_alta.P1_reactiva.value="No";
				document.form_alta.P3_reactiva.value="No";			
			break;
			
			case 3:
				document.form_alta.P1_reactiva.value="No";
				document.form_alta.P2_reactiva.value="No";							
				document.form_alta.P3_reactiva.value="No";			
			break;
			
			case 4:
				document.form_alta.P1_reactiva.value="No";
				document.form_alta.P2_reactiva.value="No";							
				document.form_alta.P3_reactiva.value="No";
				document.form_alta.P4_reactiva.value="No";
				document.form_alta.P5_reactiva.value="No";							
				document.form_alta.P6_reactiva.value="No";							
			break;
		}//switch(periodo_tarifario_activo)		
	}//if (document.form_alta.Reactiva.selectedIndex==0)
	
//Reactiva si
	else
	{
	//Ahora dependiendo del periodo tarifario activo se actualizara la tabla de periodos tarifarios		
		switch(periodo_tarifario_activo)
		{
			case 1:
				document.form_alta.P2_reactiva.value="Si";
			break;
			
			case 2:
				document.form_alta.P1_reactiva.value="Si";
				document.form_alta.P3_reactiva.value="Si";			
			break;
			
			case 3:
				document.form_alta.P1_reactiva.value="Si";
				document.form_alta.P2_reactiva.value="Si";							
				document.form_alta.P3_reactiva.value="Si";			
			break;
			
			case 4:
				document.form_alta.P1_reactiva.value="Si";
				document.form_alta.P2_reactiva.value="Si";							
				document.form_alta.P3_reactiva.value="Si";
				document.form_alta.P4_reactiva.value="Si";
				document.form_alta.P5_reactiva.value="Si";							
				document.form_alta.P6_reactiva.value="Si";							
			break;
		}//switch(periodo_tarifario_activo)		
		
	}//else (document.form_alta.Reactiva.selectedIndex==0)	
}//function asignar_periodos_reactiva()

//Esta funcion cambia los valores de algunos campos dependidendo del valor que se le haya dado al discriminador
function cambiar_periodos_tarifarios_discriminador(seccion_activa)
{
//0 ->No
//1 ->Si	
	var discriminador=document.form_alta.Discriminador.selectedIndex;

//10 kw SIN DISCIMINADOR O 10-15 kw
	if (seccion_activa==0 || seccion_activa==1)
	{	
//SIN DISCRIMINADOR -> P2 ACTIVA	
		if(discriminador==0)
		{		
			reiniciar_periodos_tarifarios();
			
			document.form_alta.Reactiva.disabled = true;
			document.form_alta.Maximetro.disabled = true;
													
//Cambiamos la tarifa a la tarifa normal
			document.form_alta.TarifaContratada.selectedIndex=1;
		}//if(discriminador==0)		
		
////10 kw SIN DISCIMINADOR O 10-15 kw CON DISCRIMINADOR -> P1 Y P3
		else
		{			
			document.form_alta.PeriodosTarifarios.selectedIndex=2;
			actualizar_tabla(seccion_activa);		
			
			document.form_alta.Reactiva.disabled = false;
			document.form_alta.Maximetro.disabled = false;										
			
//Cambiamos la tarifa a la tarifa DHA
			document.form_alta.TarifaContratada.selectedIndex=2;			
		}//else(discriminador==0)		
	}//if (seccion_activa==0 || seccion_activa==1)		
}//function cambiar_periodos_tarifarios_discriminador(seccion_activa)

//Esta funcion establece los valores por defecto de los periodos tarifarios
function reiniciar_periodos_tarifarios()
{	
	document.form_alta.PeriodosTarifarios.selectedIndex=0;
	
	document.form_alta.P1_activa.value="No";
	document.form_alta.P1_reactiva.value="No";											
	document.form_alta.P1_maximetro.value="No";																									

	document.form_alta.P2_activa.value="No";
	document.form_alta.P2_reactiva.value="No";											
	document.form_alta.P2_maximetro.value="No";
	
	document.form_alta.P3_activa.value="No";
	document.form_alta.P3_reactiva.value="No";											
	document.form_alta.P3_maximetro.value="No";					

	document.form_alta.P4_activa.value="No";
	document.form_alta.P4_reactiva.value="No";											
	document.form_alta.P4_maximetro.value="No";

	document.form_alta.P5_activa.value="No";
	document.form_alta.P5_reactiva.value="No";											
	document.form_alta.P5_maximetro.value="No";

	document.form_alta.P6_activa.value="No";
	document.form_alta.P6_reactiva.value="No";											
	document.form_alta.P6_maximetro.value="No";
		
}//function reiniciar_periodos_tarifarios()



//Esta funcion actualiza la tabla de periodos tarifarios cuando cambia el cobobox correspondiente
function actualizar_tabla(seccion_activa)
{		
	//alert("entrando en ULTIMO actualizar...");
//Estas variables almacenaran las opciones escogidas en periodos tarifarios, maximetro y reactiva, respectivamente
	var periodos_tarifarios=document.form_alta.PeriodosTarifarios.value;
	
//0 -> No 1-> Si		
	var maximetro=document.form_alta.Maximetro.selectedIndex;
	
//0 -> No 1-> Si		
	var reactiva=document.form_alta.Reactiva.selectedIndex;	
	
	switch(periodos_tarifarios)	
	{
//Se reinicia la tabla de periodos tarifarios, poniendo todo a "No", salvo P2_activa que es el valor por defecto.

		case "0":
			document.form_alta.P1_activa.value="No";
			document.form_alta.P1_activa.class="textfieldcentrado2";
			document.form_alta.P1_reactiva.value="No";
			document.form_alta.P1_maximetro.value="No";						
			
			document.form_alta.P2_activa.value="Si";
			document.form_alta.P2_activa.class="textfieldcentrado";
			document.form_alta.P2_reactiva.value="No";
			document.form_alta.P2_maximetro.value="No";						
			
			document.form_alta.P3_activa.value="No";
			document.form_alta.P1_activa.class="textfieldcentrado2";
			document.form_alta.P3_reactiva.value="No";
			document.form_alta.P3_maximetro.value="No";

			document.form_alta.P4_activa.value="No";
			document.form_alta.P4_reactiva.value="No";
			document.form_alta.P4_maximetro.value="No";						
			
			document.form_alta.P5_activa.value="No";
			document.form_alta.P5_reactiva.value="No";
			document.form_alta.P5_maximetro.value="No";						
			
			document.form_alta.P6_activa.value="No";
			document.form_alta.P6_reactiva.value="No";
			document.form_alta.P6_maximetro.value="No";

//En la seccion de >15kw estos campos no se pueden modificar asi que no se habilitaran ni se cambiaran los valores			
			if (seccion_activa!=2)
			{									
				document.form_alta.Reactiva.selectedIndex=0;	
				document.form_alta.Maximetro.selectedIndex=0;
				
				document.form_alta.Reactiva.disabled = true;
				document.form_alta.Maximetro.disabled = true;
			}//if (seccion_activa!=2)
			
		break;
//P2 Llano	
		case "1":
			document.form_alta.P1_activa.value="No";
			document.form_alta.P1_reactiva.value="No";			
			document.form_alta.P1_maximetro.value="No";						
			
			document.form_alta.P2_activa.value="Si";
			
			document.form_alta.P3_activa.value="No";
			document.form_alta.P3_reactiva.value="No";
			document.form_alta.P3_maximetro.value="No";

			document.form_alta.P4_activa.value="No";
			document.form_alta.P4_reactiva.value="No";
			document.form_alta.P4_maximetro.value="No";						
			
			document.form_alta.P5_activa.value="No";
			document.form_alta.P5_reactiva.value="No";
			document.form_alta.P5_maximetro.value="No";						
			
			document.form_alta.P6_activa.value="No";
			document.form_alta.P6_reactiva.value="No";
			document.form_alta.P6_maximetro.value="No";	

//En la seccion de >15kw estos campos no se pueden modificar asi que no se habilitaran
			if (seccion_activa!=2)
			{						
				document.form_alta.Reactiva.disabled = false;
				document.form_alta.Maximetro.disabled = false;						
			}//if (seccion_activa!=2)

//Si el maximetro ha sido activado se pondran a "Si" los periodos tarifarios pertinentes
			if(maximetro==1)
			{
				document.form_alta.P2_maximetro.value="Si";						
			}//if(maximetro==1)
			
//Si la reactiva ha sido activada se pondran a "Si" los periodos tarifarios pertinentes
			if(reactiva==1)
			{
				document.form_alta.P2_reactiva.value="Si";						
			}//if(reactiva==1)
														
		break;

//P1 Punta y P3 Valle                                                                                                    		
		case "2":
			document.form_alta.P1_activa.value="Si";
			
			document.form_alta.P2_activa.value="No";
			document.form_alta.P2_reactiva.value="No";
			document.form_alta.P2_maximetro.value="No";						
			
			document.form_alta.P3_activa.value="Si";

			document.form_alta.P4_activa.value="No";
			document.form_alta.P4_reactiva.value="No";
			document.form_alta.P4_maximetro.value="No";						
			
			document.form_alta.P5_activa.value="No";
			document.form_alta.P5_reactiva.value="No";
			document.form_alta.P5_maximetro.value="No";						
			
			document.form_alta.P6_activa.value="No";

			document.form_alta.P6_reactiva.value="No";
			document.form_alta.P6_maximetro.value="No";		
																			
//En la seccion de >15kw estos campos no se pueden modificar asi que no se habilitaran
			if (seccion_activa!=2)
			{						
				document.form_alta.Reactiva.disabled = false;
				document.form_alta.Maximetro.disabled = false;						
			}//if (seccion_activa!=2)	

//Si el maximetro ha sido activado se pondran a "Si" los periodos tarifarios pertinentes
			if(maximetro==1)
			{
				document.form_alta.P1_maximetro.value="Si";		
				document.form_alta.P3_maximetro.value="Si";											
			}//if(maximetro==1)			
			
//Si la reactiva ha sido activada se pondran a "Si" los periodos tarifarios pertinentes
			if(reactiva==1)
			{
				document.form_alta.P1_reactiva.value="Si";	
				document.form_alta.P3_reactiva.value="Si";																
			}//if(reactiva==1)
						
		break;
//De P1 a P3
		case "3":
			document.form_alta.P1_activa.value="Si";
			
			document.form_alta.P2_activa.value="Si";
			
			document.form_alta.P3_activa.value="Si";

			document.form_alta.P4_activa.value="No";
			document.form_alta.P4_reactiva.value="No";
			document.form_alta.P4_maximetro.value="No";						
			
			document.form_alta.P5_activa.value="No";
			document.form_alta.P5_reactiva.value="No";
			document.form_alta.P5_maximetro.value="No";						
			
			document.form_alta.P6_activa.value="No";
			document.form_alta.P6_reactiva.value="No";	
			document.form_alta.P6_maximetro.value="No";		

//En la seccion de >15kw estos campos no se pueden modificar asi que no se habilitaran			
			if (seccion_activa!=2)
			{						
				document.form_alta.Reactiva.disabled = false;
				document.form_alta.Maximetro.disabled = false;						
			}//if (seccion_activa!=2)												

//Si el maximetro ha sido activado se pondran a "Si" los periodos tarifarios pertinentes
			if(maximetro==1)
			{
				document.form_alta.P1_maximetro.value="Si";		
				document.form_alta.P2_maximetro.value="Si";										
				document.form_alta.P3_maximetro.value="Si";											
			}//if(maximetro==1)			

//Si la reactiva ha sido activada se pondran a "Si" los periodos tarifarios pertinentes
			if(reactiva==1)
			{
				document.form_alta.P1_reactiva.value="Si";				
				document.form_alta.P2_reactiva.value="Si";	
				document.form_alta.P3_reactiva.value="Si";																
			}//if(reactiva==1)
																						
		break;		
//De P1 a P6		
		case "4":
			document.form_alta.P1_activa.value="Si";			
			document.form_alta.P2_activa.value="Si";			
			document.form_alta.P3_activa.value="Si";
			document.form_alta.P4_activa.value="Si";			
			document.form_alta.P5_activa.value="Si";			
			document.form_alta.P6_activa.value="Si";
								
//En la seccion de >15kw estos campos no se pueden modificar asi que no se habilitaran			
			if (seccion_activa!=2)
			{						
				document.form_alta.Reactiva.disabled = false;
				document.form_alta.Maximetro.disabled = false;						
			}//if (seccion_activa!=2)																				

//Si el maximetro ha sido activado se pondran a "Si" los periodos tarifarios pertinentes
			if(maximetro==1)
			{
				document.form_alta.P1_maximetro.value="Si";		
				document.form_alta.P2_maximetro.value="Si";										
				document.form_alta.P3_maximetro.value="Si";
				document.form_alta.P4_maximetro.value="Si";													
				document.form_alta.P5_maximetro.value="Si";									
				document.form_alta.P6_maximetro.value="Si";																
			}//if(maximetro==1)						

//Si la reactiva ha sido activada se pondran a "Si" los periodos tarifarios pertinentes
			if(reactiva==1)
			{
				document.form_alta.P1_reactiva.value="Si";				
				document.form_alta.P2_reactiva.value="Si";	
				document.form_alta.P3_reactiva.value="Si";				
				document.form_alta.P4_reactiva.value="Si";				
				document.form_alta.P5_reactiva.value="Si";	
				document.form_alta.P6_reactiva.value="Si";																
			}//if(reactiva==1)
						
		break;
	}//switch(forma_pago)		

}//function actualizar_tabla(seccion_activa)

//Esta funcion establecera los valores de los campos del apartado Datos Tipo de Suministro, cuando se acceda a la opcion de más de 15 kw/h
function valores_mas_15kw()
{	
//Discriminador= En blanco
	document.form_alta.Discriminador.selectedIndex=0; 	
//Maximetro= si
	document.form_alta.Maximetro.selectedIndex=1; 		
//Modo = 2
	document.form_alta.Modo.value="2";
//Reactiva = si	
	document.form_alta.Reactiva.selectedIndex=1;
//Tambien se bloqueara le discriminador	
	document.form_alta.Discriminador.disabled = true;
}//function valores_mas_15kw()

//Esta funcion habilitara los campos que se dehabilitan para que envien el valor correctamente. Se ejecutara siempre que el usuario pulse el boton Enviar del formulario
function habilitar_campos()
{
	document.form_alta.Discriminador.disabled = false;
	document.form_alta.Maximetro.disabled = false;
	document.form_alta.Modo.disabled = false;
	document.form_alta.TipoTension.disabled = false;
	document.form_alta.Reactiva.disabled = false;
	document.form_alta.PeriodosTarifarios.disabled = false;
	document.form_alta.Fases.disabled = false;	
}//function habilitar_campos()