<?php
if (isset($_POST["submit"]))
{
//Recogemos los valores de las variables
	$suministro_calle=$_POST["SuministroCalle"];
	$suministro_numero=$_POST["SuministroNumero"];	
	$suministro_extension=$_POST["SuministroExtension"];		
	$suministro_aclarador=$_POST["SuministroExtension2"];			
	$suministro_cp=$_POST["SuministroCP"];		
	$suministro_poblacion=$_POST["SuministroCiudad"];		
	$suministro_provincia=$_POST["SuministroProvincia"];					
	$suministro_telefono=$_POST["SuministroTelefono1"];					
	$suministro_movil=$_POST["SuministroTelefono2"];						
	$suministro_fax=$_POST["SuministroFax"];							
	$suministro_email=$_POST["SuministroEmail"];							

	$titular_nombre=$_POST["TitularNombre"];							
	$titular_calle=$_POST["TitularCalle"];								
	$titular_numero=$_POST["TitularNumero"];
	$titular_extension=$_POST["TitularExtension"];	
	$titular_aclarador=$_POST["TitularAclarador"];		
	$titular_poblacion=$_POST["TitularPoblacion"];		
	$titular_provincia=$_POST["TitularProvincia"];
	
	$titular_telefono=$_POST["TitularTelefono"];					
	$titular_movil=$_POST["TitularMovil"];						
	$titular_fax=$_POST["TitularFax"];								
	$titular_email=$_POST["TitularEmail"];	
	$titular_dni=$_POST["TitularDNI"];

	$factura_nombre=$_POST["FacturaNombre"];							
	$factura_calle=$_POST["FacturaCalle"];								
	if ($_POST["FacturaNumero"] != "")
	{
	$factura_numero=$_POST["FacturaNumero"];
	}
	else
	{
	$factura_numero=0;
	}
	$factura_extension=$_POST["FacturaExtension"];
	$factura_aclarador=$_POST["FacturaAclarador"];		
	$factura_poblacion=$_POST["FacturaPoblacion"];		
	$factura_provincia=$_POST["FacturaProvincia"];
	$factura_telefono=$_POST["FacturaTelefono"];		
	$factura_movil=$_POST["FacturaMovil"];			
	$factura_fax=$_POST["FacturaFax"];	
	$factura_email=$_POST["FacturaEmail"];	
	$factura_dni=$_POST["FacturaDNI"];
		
	$pagador_nombre=$_POST["PagadorNombre"];							
	$pagador_calle=$_POST["PagadorCalle"];								
	$pagador_numero=$_POST["PagadorNumero"];
	$pagador_poblacion=$_POST["PagadorPoblacion"];		
	$pagador_provincia=$_POST["PagadorProvincia"];	
	if ($_POST["PagadorNumero"] != "")
	{
	$pagador_numero=$_POST["PagadorNumero"];			
	}
	else
	{
	$pagador_numero=0;
	}
	$pagador_extension=$_POST["PagadorExtension"];
	$pagador_aclarador=$_POST["PagadorAclarador"];	
	$pagador_telefono=$_POST["PagadorTelefono"];		
	$pagador_movil=$_POST["PagadorMovil"];			
	$pagador_fax=$_POST["PagadorFax"];						
	$pagador_email=$_POST["PagadorEmail"];
	$pagador_dni=$_POST["PagadorDNI"];	

	$representante_nombre=$_POST["RepresentanteEmpresa"];
	$representante_dni=$_POST["DNIRepresentante"];
	
	$forma_pago=$_POST["FormaPago"];
	$idioma_factura=$_POST["IdiomaFactura"];	

//Este campo sera obligatorio si la forma de pago es "Domiciliacion"
	$entidad_bancaria=$_POST["EntidadBancaria"];	
	
	$num_cuenta_banco=$_POST["banco"];
	$num_cuenta_sucursal=$_POST["sucursal"];	
	$num_cuenta_digito_control=$_POST["sucursal2"];		
	$num_cuenta_cuenta=$_POST["sucursal3"];		
	
	$potencia_normalizada=$_POST["PotenciaNormalizada"];	
	$tarifa_contratar=$_POST["TarifaContratada"];	
	$periodos_tarifarios=$_POST["PeriodosTarifarios"];	
	$equipos_medida=$_POST["EquiposMedida"];

	$estado_bono_social=$_POST["EstadoBonoSocial"];
		
/*
VERIFICACIONES PENDIENTES:

//COMPROBAR	SI DATOS DE FACTURACION SON IGUALES QUE LOS DEL TITULAR??? -> 

//COMPROBAR	SI DATOS DEL PAGADOR SON IGUALES QUE LOS DEL TITULAR???

*/
	
//Esta variable controlara si se produce algun error en las verificaciones
	$error=0;
			
//Ahora comenzaremos con las verficaciones. En primer lugar comprobaremos si los campos obligatorios han sido todos escritos
	if($suministro_calle=="" or $suministro_numero=="" or $suministro_cp=="" or $suministro_poblacion=="" or $suministro_provincia=="" or $titular_nombre=="" or $titular_calle=="" or $titular_numero=="" or $titular_poblacion=="" or $titular_provincia=="" or $titular_dni=="" or $forma_pago==-1 or $potencia_normalizada==-1 or $tarifa_contratar==-1 or $periodos_tarifarios==0 or ($_SESSION["seccion_origen_contratacion"]==4 and $estado_bono_social==-1))
	{
		$error=1;
	}//if($suministro_calle=="" or $suministro_numero=="" or $suministro_cp=="" or $suministro_poblacion=="" or $suministro_provincia=="" or $titular_nombre=="" or $titular_calle=="" or $titular_numero=="" or $titular_poblacion=="" or $titular_provincia=="" or $potencia_normalizada==""or $tarifa_contratar=="" or $periodos_tarifarios==0)

//Si no se ha producido ningun error, ahora se comprueban todos los numeros de telefono y faxes que se hayan escrito		
	if($error==0 and ($suministro_telefono!="" and solo_numero($suministro_telefono) or ($suministro_movil!="" and solo_numero($suministro_movil)) or ($suministro_fax!="" and solo_numero($suministro_fax)) or ($titular_telefono!="" and solo_numero($titular_telefono)) or ($titular_movil!="" and solo_numero($titular_movil)) or ($titular_fax!="" and solo_numero($titular_fax)) or ($factura_telefono!="" and solo_numero($factura_telefono)) or ($factura_movil!="" and solo_numero($factura_movil)) or ($factura_fax!="" and solo_numero($factura_fax)) or ($pagador_telefono!="" and solo_numero($pagador_telefono)) or ($pagador_movil!="" and solo_numero($pagador_movil)) or ($pagador_fax!="" and solo_numero($pagador_fax)) or ($suministro_cp!="" and solo_numero($suministro_cp))))
	{
		$error=2;	
	}//if($error==0 and ($suministro_telefono!="" and solo_numero($suministro_telefono) or...
				
//A continuacion se haran las comprobaciones de las direcciones de correo que se hayan escrito
	if($error==0 and ($suministro_email!="" and !comprobar_email($suministro_email)) or ($titular_email!="" and !comprobar_email($titular_email)) or ($factura_email!="" and !comprobar_email($factura_email)) or ($pagador_email!="" and !comprobar_email($pagador_email)))
	{
		$error=3;
	}//if($error==0 and ($suministro_email!="" and !comprobar_email($suministro_email)) or ($titular_email!="" and !comprobar_email($titular_email)) or ($factura_email!="" and !comprobar_email($factura_email)))

//Ahora comprobaremos los DNI que se hayan escrito
	if($error==0 and ($titular_dni!="" and comprobar_dni($titular_dni)) or ($factura_dni!="" and comprobar_dni($factura_dni)) or ($pagador_dni!="" and comprobar_dni($pagador_dni)) or ($representante_dni!="" and comprobar_dni($representante_dni)))
	{
		$error=4;
	}//if($error==0 and ($titular_dni!="" and comprobar_dni($titular_dni)) or ($factura_dni!="" and comprobar_dni($factura_dni)) or ($pagador_dni!="" and comprobar_dni($pagador_dni)) or ($representante_dni!="" and comprobar_dni($representante_dni)))

//Ahora pasaremos a comprobar los datos bancarios. Si la forma de pago es "Domiciliada" los campos de Entidad Bancaria y numero de cuenta se vuelven obligatorios
	if($error==0 and $forma_pago==1)
	{				
//Esta variable almacenara el numero de la cuenta bancaria completo		
		$num_cuenta_completo=$num_cuenta_banco.$num_cuenta_sucursal.$num_cuenta_digito_control.$num_cuenta_cuenta;		
//Esta variable almacenara true si el digito de control es correcto y false si no lo es, será otra restriccion mas para qeu el nuemro de cuenta bancaria sea correcto
		$comprobacion=comprobacion_digito_control($num_cuenta_completo);

//Ahora sbdividimos el valor recibido del campo de la entidad bancaria por el signo "+", en la primera posicion se almacenara el nombre de la entidad bancaria y en la segunda el codigo. Lo utilizaremos para comprobar que el usuario no haya cambiado el codigo de entidad bancaria asignado automaticamente
		$partes_entidad_bancaria=explode("+",$entidad_bancaria);

//En las comprobaciones añadimos la comprobacion de si el primer numero de la cuenta bacanria coincide con el codigo de la identidad bancaria seleccionada								
		if($entidad_bancaria==-1 or $num_cuenta_banco=="" or $num_cuenta_sucursal=="" or $num_cuenta_digito_control=="" or $num_cuenta_cuenta==""or $partes_entidad_bancaria[1]!=$num_cuenta_banco or !$comprobacion)
		{
			$error=5;
		}//	if($entidad_bancaria==-1 or $num_cuenta_banco=="" or $num_cuenta_sucursal=="" or $num_cuenta_digito_control=="" or $num_cuenta_cuenta==""or $partes_entidad_bancaria[1]!=$num_cuenta_banco or !$comprobacion)
		
//Si se ha rellenado el numero de la cuenta comprobaremos que la cuenta bancaria solo contenga caracteres numericos y cada tramo del numero de la cuenta debera de tener la longitud correcta de caracteres
		else
		{							
			if($error==0 and (strlen($num_cuenta_banco)<4 or solo_numero($num_cuenta_banco)) or (strlen($num_cuenta_sucursal)<4 or solo_numero($num_cuenta_sucursal)) or (strlen($num_cuenta_digito_control)<2 or solo_numero($num_cuenta_digito_control)) or (strlen($num_cuenta_cuenta)<10 or solo_numero($num_cuenta_cuenta)))
			{
				$error=6;			
			}//if((strlen($num_cuenta_banco)<4 or solo_numero($num_cuenta_banco)) or (strlen($num_cuenta_sucursal)<4 or solo_numero($num_cuenta_sucursal)) or (strlen($num_cuenta_digito_control)<2 or solo_numero($num_cuenta_digito_control)) or (strlen($num_cuenta_cuenta)<10 or solo_numero($num_cuenta_cuenta)))
			
		}//else($entidad_bancaria==-1 or $num_cuenta_banco=="" or $num_cuenta_sucursal=="" or $num_cuenta_digito_control=="" or $num_cuenta_digito_control=="")
		
	}//if($error==0 and $forma_pago==1)

//Ahora se comprobara si se ha escrito algún dato en los apartados Facturacion, Pagador y Representante empresa. En el caso de que se haya escrito se debera de comprobar que los campos marcados como obligatorios en los demas apartados esten llenos
			
//Comprobacion de los datos del facturacion
	if($factura_nombre!="" or $factura_calle!="" or $factura_numero!="" or $factura_poblacion!="" or $factura_provincia!="" or $factura_dni!="" or $factura_extension!="" or $factura_aclarador!="" or $factura_telefono!="" or $factura_movil!="" or $factura_fax!="" or $factura_email!="" or $factura_dni!="")
	{		
		if($error==0 and ($factura_nombre=="" or $factura_calle=="" or $factura_numero=="" or $factura_poblacion=="" or $factura_provincia=="" or $factura_dni==""))
		{
			$error=7;
		}//if($error==0 and ($factura_nombre=="" or $factura_calle=="" or $factura_numero=="" or $factura_poblacion=="" or $factura_provincia=="" or $factura_dni==""))
	}//	if($factura_nombre!="" or $factura_calle!="" or $factura_numero!="" or $factura_poblacion!="" or $factura_provincia!="" or $factura_dni!="" or $factura_extension!="" or $factura_aclarador!="" or $factura_telefono!="" or $factura_movil!="" or $factura_fax!="" or $factura_email!="" or $factura_dni!="")

	
//Comprobacion de los datos del pagador
	if($pagador_nombre!="" or $pagador_calle!="" or $pagador_numero!="" or $pagador_poblacion!="" or $pagador_provincia!="" or $pagador_dni!="" or $pagador_extension!="" or $pagador_aclarador!="" or $pagador_telefono!="" or $pagador_movil!="" or $pagador_fax!="" or $pagador_email!="" or $pagador_dni!="")
	{		
		if($error==0 and ($pagador_nombre=="" or $pagador_calle=="" or $pagador_numero=="" or $pagador_poblacion=="" or $pagador_provincia=="" or $pagador_dni==""))
		{
			$error=7;
		}//if($pagador_nombre=="" or $pagador_calle=="" or $pagador_numero=="" or $pagador_poblacion=="" or $pagador_provincia=="" or $pagador_dni=="")
	}//if($error==0 and ($pagador_nombre!="" or $pagador_calle!="" or $pagador_numero!="" or $pagador_poblacion!="" or $pagador_provincia!="" or $pagador_dni!="")	

//Comprobacion de los datos del representante de la empresa
	if($representante_nombre!="" or $representante_dni!="")
	{		
		if($error==0 and ($representante_nombre=="" or $representante_dni==""))
		{
			$error=8;
		}//if($error==0 and ($representante_nombre=="" or $representante_dni==""))
	}//if($representante_nombre!="" or $representante_dni!="")

//Por seguridad, en cualquier caso se borran los datos bancarios
	$_POST["banco"]="";
	$_POST["sucursal"]="";	
	$_POST["sucursal2"]="";		
	$_POST["sucursal3"]="";		
			
}//if (isset($_POST["submit"]))
?>