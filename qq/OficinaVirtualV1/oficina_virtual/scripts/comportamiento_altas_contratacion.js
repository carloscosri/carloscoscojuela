<!--
//Esta funcion se ejecuta cuando se cambia el valor de la forma de pago, deshabilitara los campos de banco domiciliado y los componentes del numero de la cuenta, cuando se seleccione la opcion de No domiciliado y los habilitara cuando se seleccione la opcion de Domiciliacion
function datos_bancarios()
{
//Esta variable almacenara un indice que representara la forma de pago seleccionada:
//1 = Domiciliacion
//2 = No Domiciliado

	var forma_pago;

	forma_pago=document.form_alta.FormaPago.value;
	
	switch(forma_pago)	
	{
//Si la forma de pago es domiciliacion bancaria, se habilitaran los campos correspondientes	
		case "1":					
			document.form_alta.EntidadBancaria.disabled = false;
			document.form_alta.banco.disabled = false;			
			document.form_alta.sucursal.disabled = false;						
			document.form_alta.sucursal2.disabled = false;						
			document.form_alta.sucursal3.disabled = false;														
		break;

//Si la forma de pago es no domicialiacion, se dehabilitaran los campos correspondientes y se eliminaran sus valores
		case "2":
			document.form_alta.EntidadBancaria.selectedIndex=0; 
			document.form_alta.banco.value = "";			
			document.form_alta.sucursal.value = "";						
			document.form_alta.sucursal2.value = "";						
			document.form_alta.sucursal3.value = "";
					
			document.form_alta.EntidadBancaria.disabled = true;
			document.form_alta.banco.disabled = true;			
			document.form_alta.sucursal.disabled = true;						
			document.form_alta.sucursal2.disabled = true;						
			document.form_alta.sucursal3.disabled = true;					
		break;				
	}//switch(forma_pago)			
}//function datos_bancarios()

//Esta funcion establece el codigo de la entidad bancaria en el numero de cuenta
function asignar_codigo_entidad_bancaria()
{
	var entidad_bancaria=document.form_alta.EntidadBancaria.value;
//Dividimos el valor recibido de la entidad bancaria por el simbolo mas. Esta variable contendra en la primera posicion el nombre de la entidad bancaria y en la segunda el codigo
	var partes_entidad_bancaria= entidad_bancaria.split('+');
		
	document.form_alta.banco.value=partes_entidad_bancaria[1];
}//function asignar_codigo_entidad_bancaria()

//Esta funcion vacia los campos exclusivos de la solicitud de alta de un usuario no registrado en la oficina virtual
function reiniciar_campos_no_registrado()
{
	document.form_alta.CUPS.value="";
	document.form_alta.Comercializadora.value="";
	document.form_alta.Distribuidora.value="";	
}//function reiniciar_campos_no_registrado()

//Esta funcion devuelve el formulario a su estado actual
function reiniciar_formulario(seccion_activa)
{	
	document.form_alta.SuministroCalle.value="";
	document.form_alta.SuministroNumero.value="";	
	document.form_alta.SuministroExtension.value="";		
	document.form_alta.SuministroExtension2.value="";			
	document.form_alta.SuministroCP.value="";				
	document.form_alta.SuministroCiudad.value="";					
	document.form_alta.SuministroProvincia.value="";						
	document.form_alta.SuministroTelefono1.value="";							
	document.form_alta.SuministroTelefono2.value="";								
	document.form_alta.SuministroFax.value="";									
	document.form_alta.SuministroEmail.value="";
											
	document.form_alta.TitularNombre.value="";											
	document.form_alta.TitularCalle.value="";												
	document.form_alta.TitularNumero.value="";													
	document.form_alta.TitularExtension.value="";														
	document.form_alta.TitularAclarador.value="";															
	document.form_alta.TitularPoblacion.value="";																
	document.form_alta.TitularProvincia.value="";																	
	document.form_alta.TitularTelefono.value="";																		
	document.form_alta.TitularMovil.value="";																			
	document.form_alta.TitularFax.value="";																				
	document.form_alta.TitularEmail.value="";																					
	document.form_alta.TitularDNI.value="";																						

	document.form_alta.FacturaNombre.value="";																							
	document.form_alta.FacturaCalle.value="";
	document.form_alta.FacturaNumero.value="";	
	document.form_alta.FacturaExtension.value="";		
	document.form_alta.FacturaAclarador.value="";			
	document.form_alta.FacturaPoblacion.value="";				
	document.form_alta.FacturaProvincia.value="";					
	document.form_alta.FacturaTelefono.value="";						
	document.form_alta.FacturaMovil.value="";							
	document.form_alta.FacturaFax.value="";								
	document.form_alta.FacturaEmail.value="";									
	document.form_alta.FacturaDNI.value="";										
	
	document.form_alta.PagadorNombre.value="";											
	document.form_alta.PagadorCalle.value="";												
	document.form_alta.PagadorNumero.value="";													
	document.form_alta.PagadorExtension.value="";														
	document.form_alta.PagadorAclarador.value="";															
	document.form_alta.PagadorPoblacion.value="";																
	document.form_alta.PagadorProvincia.value="";																	
	document.form_alta.PagadorTelefono.value="";																		
	document.form_alta.PagadorMovil.value="";																			
	document.form_alta.PagadorFax.value="";																				
	document.form_alta.PagadorEmail.value="";																					
	document.form_alta.PagadorEmail.value="";
	document.form_alta.PagadorDNI.value="";																																																			
	
	document.form_alta.RepresentanteEmpresa.value="";																																																			
	document.form_alta.DNIRepresentante.value="";																																																			
	
	document.form_alta.FormaPago.selectedIndex=0;
	document.form_alta.EntidadBancaria.selectedIndex=0;
	
	document.form_alta.banco.value="";
	document.form_alta.sucursal.value="";
	document.form_alta.sucursal2.value="";
	document.form_alta.sucursal3.value="";
	
	document.form_alta.TarifaContratada.selectedIndex=0;
	document.form_alta.Discriminador.selectedIndex=0;
			
	document.form_alta.Maximetro.selectedIndex=0;	
	document.form_alta.Reactiva.selectedIndex=0;
				
	document.form_alta.Modo.value="1";
	
	document.form_alta.Fases.value="";
		
	document.form_alta.EquiposMedida.selectedIndex=0;				

	document.form_alta.Observaciones.value="";						

//Llamamos a la funcion que reinicia los periodos tarifarios	
	reiniciar_periodos_tarifarios();
		
	if (seccion_activa!=2)
	{
		document.form_alta.Reactiva.selectedIndex=0;	
		document.form_alta.Maximetro.selectedIndex=0;						
		
		document.form_alta.Reactiva.disabled = true;
		document.form_alta.Maximetro.disabled = true;						
	}//if (seccion_activa!=2)
			
}//function reiniciar_formulario(seccion_activa)


//LOLOLO ESTA FUNCION DEBERIA DESAPARECER!!!!
//Esta funcion establecera los valores de los campos del apartado Datos Tipo de Suministro, cuando se acceda a la opcion de tarifa social
function valores_bono_social()
{
//Discriminador= En blanco
	document.form_alta.Discriminador.selectedIndex=0; 	
//Maximetro= no
	document.form_alta.Maximetro.selectedIndex=0; 		
//Modo = ""
	document.form_alta.Modo.value="";
//Tipo tension = 2x220
	document.form_alta.TipoTension.selectedIndex=1;	
//Reactiva = no	
	document.form_alta.Reactiva.selectedIndex=0;	

//Periodos Tarifarios = P2 Llano
	document.form_alta.PeriodosTarifarios.selectedIndex=1;	
	document.form_alta.P2_activa.value="SI";

//Fases=2
	document.form_alta.Fases.value="2";		
		
//Los valores que se han dado han de ser fijos, por lo tanto a continuacion deshabilitaremos los campos necesarios
	document.form_alta.Discriminador.disabled = true;
	document.form_alta.Maximetro.disabled = true;
	document.form_alta.Modo.disabled = true;
	document.form_alta.TipoTension.disabled = true;
	document.form_alta.Reactiva.disabled = true;
	document.form_alta.PeriodosTarifarios.disabled = true;
	document.form_alta.Fases.disabled = true;
	
}//function valores_bono_social()
//-->

/*NOTA: LOS CAMPOS DESHABILITADOS NO ENVIAN EL VALOR POST!!!!!!!!!!!*/