<?php
/*
NOTAS:

1- ESTA SECCION TOMA LOS DATOS DE LA TABLA DatosRegistrados

*/

/* NOTA:

 COMO NO SE CIERRA LA CONEXION TRAS CADA CONEXION CON LA BASE DE DATOS, LAS CONSULTAS DEJAN DE FUNCIONAR SI SE ESTABLECE COMO UNICA CONEXION
 LA QUE UTILIZA EL RESTO DE LA WEB, POR LO TANTO HASTA QUE SE HAGA LA MIGRACION COMPLETA DE LOS TEXTOS, COEXISTIRAN LAS DOS CONEXIONES, YA QUE
 SE HA AÑADIDO UNA LLAMADA A LA CONEXION BUENA EN ESTE ARCHIVO.
*/

include("_conexion.php");


//Se incluye el archivo con los textos especifico de esta seccion en el idioma oportuno
//include($_SESSION["directorio_raiz"]."idiomas/".$_SESSION["idioma"]."/oficina_contratacion.php");
include("../idiomas/".$_SESSION["idioma"]."/oficina_avisos.php");

include("../idiomas/".$_SESSION["idioma"]."/oficina_informacion_facturas.php");

include("../idiomas/".$_SESSION["idioma"]."/oficina_informacion_certificadoconsumos.php");
include("../idiomas/".$_SESSION["idioma"]."/oficina_contratacion.php");

include("../idiomas/".$_SESSION["idioma"]."/oficina_datos_titular.php");
include("../idiomas/".$_SESSION["idioma"]."/oficina_contratacion_alta.php");

include("../idiomas/".$_SESSION["idioma"]."/oficina_modificacion_datostitular.php");

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<title><?=$titulo_web;?></title>

<!--Se añaden las hojas de estilos genericas que utiliza esta seccion-->
<link href="../css/estilos.css" rel="stylesheet" type="text/css" />
<link href="../css/secciones.css" rel="stylesheet" type="text/css" />
<link href="css/oficina.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_vieja.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_contratacion.css" rel="stylesheet" type="text/css" />

<link href="css/oficina_avisos_noticias.css" rel="stylesheet" type="text/css" />

<!--Ahora se añaden las reglas para Internet Explorer. Asi corregiremos las diferencias de visualizacion entre navegadores
Es importante añadirlo despues de las reglas principales, para que se sobreescriban las correcciones pertinentes. En este caso las diferencias
solo son para internet explorer 7-->

<script language=javascript type=text/javascript>
function stopRKey(evt) {
var evt = (evt) ? evt : ((event) ? event : null);
var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
if ((evt.keyCode == 13) && (node.type=="text")) {return false;}
}
document.onkeypress = stopRKey; 
</script>
<!--[if IE 7]>
	<link href="../css/estilos_ie7.css" rel="stylesheet" type="text/css" />
	<link href="../css/secciones_ie7.css" rel="stylesheet" type="text/css" />    
<![endif]-->

<?php

?>


</head>
	
<body>
<!--*************************************************Web realizada por ****************************************-->
	<div id="central_oficina">
    
<?php

//Se comprueba que el usuario se haya identificado, si no es asi se da un aviso y redirigimos a la seccion de identificacion
if ($_SESSION['usuario']=="")
{	
	MsgBox($error_identificacion);
	redirigir("oficina_index.php?id=".$_SESSION["idioma"]);
	
}//if ($_SESSION['usuario'])

//Si el usuario se ha identificado la seccion se ejecutara con normalidad
else
{
//Añadimos los fragmentos comunes, la cabecera, el menu, y el submenu
		
		
		if (isset($_POST["Submit"]))
{


	$suministro_email=$_POST["SuministroEmail"];	
	
}

		
		include("oficina_fragmentos_comunes.php");
?>                   
		<div class="contenido_seccion_oficina_cortes">

<!--VERSION VIEJA-->

	 <table width="600" border="0" cellspacing="0" cellpadding="0">
            		 
                <tr align="center">
                  <td height="60" valign="top" class="arialblack18Titulo"><?=$recibir_avisos?> </td>
                </tr>
                   
                   
                  <br />
<br />

     
                <tr>
                  <td height="50" colspan="1" valign="top" class="arial14">
                  	<div align="left" class="arialblack14"><?=$descripcion_avisos?></div>
                  </td>
                <br /><br />


                </tr>
                
				<tr align="center">
                  <form action="oficina_avisos_cortes.php?id=<?=$_SESSION["idioma"]?>" method="post" name="avisos_cortes" id="avisos_cortes">
                </tr>
                  
                <tr align="left">
                   
                              
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><?php      
	
	
	                $result = mysql_query("SELECT * FROM DatosRegistrados WHERE CodigoCliente = ".$_SESSION['numeroCliente']." ");
					$row = mysql_fetch_array($result);
					
				if (!isset($_POST["Submit"]))	
				{
					
					$suministro_email=$row["SuministroEmail"];	
				} else {	$suministro_email=$_POST["SuministroEmail"];
				
				
				
				
				}
					
					$resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 129');
					$rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?></td>
    <td><input name="SuministroEmail" value="<?=$suministro_email?>"  type="text" class="textfield" id="SuministroEmail" size="22" maxlength="22" /></td>
    <td colspan="1" align="left" valign="top" class="arial14">
    
    
					  <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 153'); $rowTexto = mysql_fetch_array($resultTexto);?>
                       
                        
                       <input type="submit" name="Submit" value="<?php echo($rowTexto['Texto']);?>" />
    </td>
  </tr>
</table>

               
             	</tr>
          </table>
 </form>
<!--VERSION VIEJA-->

        	
            
      
        <div class="posicion_cuadro_ventajas">
                <div class="cuadro_ventajas_cabecera"></div>            
                                
              <div class="cuadro_ventajas_cuerpo">
                    
             

<?php

//$cons="SELECT * FROM DatosRegistrados where CodigoCliente= ".$_SESSION['usuario']." ";
//echo($cons);
//$resultTexto=mysql_query($cons);
//echo($resultTexto);
//$rowTexto = mysql_fetch_array($resultTexto);
//echo($rowTexto["SuministroEmail"]);      



$result = mysql_query("SELECT * FROM DatosRegistrados WHERE CodigoCliente = ".$_SESSION['numeroCliente']." ");
$row = mysql_fetch_array($result);
$suministro_email=$row["SuministroEmail"];	
	
$resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 129');
$rowTexto = mysql_fetch_array($resultTexto);


$resultTexto2 = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 153'); $rowTexto2 = mysql_fetch_array($resultTexto2);

//este recordset es con diferencia de idioma
//$rs_cortes=seleccion_condicional("cortes_luz", "idioma='".$_SESSION["idioma"]."'","id",0,$ruta_sevidor_bd,$usuario_bd,$contrasena_bd,$nombre_bd);

//este recordset es sin diferencia de idioma


//$rs_cortes=seleccion("cortes_luz","id",0,$ruta_sevidor_bd,$usuario_bd,$contrasena_bd,$nombre_bd);

$rs_cortesq="SELECT * FROM cortes_luz order by id asc";
$rs_cortes=mysql_query($rs_cortesq);

//$rs_cortes2=seleccion("DatosRegistrados","id",0,$ruta_sevidor_bd,$usuario_bd,$contrasena_bd,$nombre_bd);


//$rs_cortes2=seleccion_condicional("DatosRegistrados", "(idioma='".$_SESSION["idioma"]." AND CodigoCliente='".$_SESSION['numeroCliente']."')","id",0,$ruta_sevidor_bd,$usuario_bd,$contrasena_bd,$nombre_bd);
//echo($rs_cortes2);
//echo($rs_cortes);
$cont=0;

$registros = mysql_num_rows($rs_cortes);
$registros2 = mysql_num_rows($resultTexto);
//$registros2= mysql_num_rows($rs_cortes2);
//echo($registros);
//$row = mysql_fetch_array( $rs_cortes2);

if ($registros > 0){


$titulo_columna1="Día";
$titulo_columna2="Horas";
$titulo_columna4="Ver Detalles";


?>
<span class="centrar">
<table width="100%" border="0" cellspacing="0" cellpadding="5">
  <tr  class="cabecera_cuadro_comparativo pequenno menu">
    <td><?=$titulo_columna1?></td>
    <td><?=$titulo_columna2?></td>
    <td><?=$titulo_columna4?></td>
   </tr>
<?php


	while($registro_cortes = mysql_fetch_array($rs_cortes))
		
		{
		$idi=$_SESSION["idioma"];
		if($idi==en){
		setlocale(LC_TIME, 'es_ES','sp',$idi);
		$fecha = strftime('%d de %B de %Y', strtotime($registro_cortes["dia"]));
		}
		else
		{
		setlocale(LC_TIME, 'es_ES','sp',$idi);
		$fecha = strftime('%d de %B de %Y', strtotime($registro_cortes["dia"]));
		}
		
			if ($cont==0){
			 $estilo_tabla="td_fondo_amarillo";
			 $cont=1;
			}
			else
			{
			 $estilo_tabla="td_fondo_blanco";
			 $cont=0;
			}
		?>
		  <tr>
			<td width="200" class="<?=$estilo_tabla?> centrar pequenno"><div align="center">
			  <?=$fecha?>
			  </div></td>
			<td width="100" class="<?=$estilo_tabla?> centrar pequenno"><div align="center">
			  <?=$registro_cortes["horas"]?>
			  </div></td>
			<td width="300"class="<?=$estilo_tabla?> centrar pequenno"><div align="center"><a href="oficina_avisos_cortes_detalles.php?id=<?=$_SESSION["idioma"]?><?=$ampersand?>det=<?=$registro_cortes["id"]?>"  class="arial11">
			  <?=$registro_cortes["municipio"]?>
			  </a></div></td>
		 </tr>
		<?php
	}
	?>
	</table></span>
    <?php
}
	else
{
 ?>
 
                <div class="centrar  menu"><?=$informacion_cortes4?></div>
  
  <?php 
}



?>

     </div> <!-- <div class="cuadro_ventajas_cuerpo">  -->
     
      
        <div class="cuadro_ventajas_pie">  </div>
        <div class="limpiar"></div>
        
 </div>   <!-- <div class="posicion_cuadro_ventajas">-->

      
            
		</div><!--<div class="contenido_seccion_oficina">-->                        
		
        
        <div class="limpiar"></div>
                        	
<!--Por ultimo aparecera el pie de la web-->


<?php 
	include("../includes/pie.php");

//NOTA: EN ESTA OCASION, EL ERROR SE MOSTRARA DESPUES DE LA CARGA DE LA PAGINA, SI NO POR ALGUN MOTIVO EN OCASIONES DESPUES DE DAR EL ERROR CESA LA CARGA DE LA PAGINA
	if(isset($_POST["Submit"]))
	{
//Por ultimo mostraremos el error correspondiente, en el caso de que haya fallado alguna de las verificaciones, o continuaremos con el proceso		
		switch($error)
		{				
			case 0:
//Si no se ha producido ningun error en las verificaciones de las fechas, en el caso de haber pulsado el boton enviar se dara el alta en la tabla CertificadoConsumo
				
				
                 $suministro_email=$_POST["SuministroEmail"];

//LOLOLO		
				$insertar_solicitud_informacion_cortes="INSERT INTO `SolicitudInformacionCortes` (`NumeroCliente` ,`FechaRegistro` ,`HoraRegistro` ,`Email` ,`Observaciones`) VALUES (
'".$_SESSION['numeroCliente']."', '".date("Y-m-d")."', '".date("Y-m-d H:i:s")."', '".$suministro_email."', 'Solicita Información Cortes');";
	
				$ejecutar_solicitud_informacion_cortes=mysql_query($insertar_solicitud_informacion_cortes);
								
				MsgBox($solicitud_ok);
				//if(isset($_POST["Submit"]))
				
			break;
//Error por no rellenar los campos obligatorios
			case 1:
				MsgBox($error_rellenar);
			break;								
			
//Error por escribir la fecha incorrectamente
			case 2:
				MsgBox($error_fecha);
			break;									
		
//Error por fechas escritas posterior a la actual
			case 3:
				MsgBox($error_fecha_posterior);
			break;									

//Error por que la fecha de inicio es mayor que la fecha final
			case 4:
				MsgBox($error_fecha_inicio_mayor);
			break;											
			
		}//switch($error)
	}//if(isset($_POST["Submit"]))	
        
}//else ($_SESSION['usuario'])
?>   


    </div><!--<div id="central_oficina"-->
    
</body>
</html>
