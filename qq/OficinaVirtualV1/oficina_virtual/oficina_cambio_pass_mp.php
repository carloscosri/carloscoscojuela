<?php
include("_conexion.php");

//Se incluye el archivo con los textos especifico de esta seccion en el idioma oportuno
include("../idiomas/".$_SESSION["idioma"]."/oficina_datos_instalacion.php");

//Se incluye el archivo con los textos especifico de esta seccion en el idioma oportuno
include("../idiomas/".$_SESSION["idioma"]."/oficina_cambio_pass.php");

//Cuando se pulse el boton aceptar del formulario, recogeremos el valor del boton
if ((isset($_POST['btn_aceptar'])) && $_POST['btn_aceptar']=="Aceptar") 
{	
//Recogemos los valores de los campos de texto del formulario escritas por el usuario
	$pass_vieja=$_POST['pass_vieja'];
	$pass_nueva=$_POST['pass_nueva'];
	$pass_nueva2=$_POST['pass_nueva2'];
	
//Con esta variable se comprobara si hay algun tipo de error en el formulario	
	$error=0;	
	
//Primero se comprueba si el usuario ha escrito todos los campos	
	if($pass_vieja=="" or $pass_nueva=="" or $pass_nueva2=="")
	{
		$error=1;
	}//if($pass_vieja=="" or $pass_nueva=="" or $pass_nueva2=="")		
}//if ((isset($_POST['btn_aceptar'])) && $_POST['btn_aceptar']=="Aceptar") 
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<title><?=$titulo_web;?></title>

<!--Se añaden las hojas de estilos genericas que utiliza esta seccion-->
<link href="../css/estilos.css" rel="stylesheet" type="text/css" />
<link href="../css/secciones.css" rel="stylesheet" type="text/css" />
<link href="css/oficina.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_cambio_pass.css" rel="stylesheet" type="text/css" />
<link href="css/colores.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_datos.css" rel="stylesheet" type="text/css" />

<!--Ahora se añaden las reglas para Internet Explorer. Asi corregiremos las diferencias de visualizacion entre navegadores
Es importante añadirlo despues de las reglas principales, para que se sobreescriban las correcciones pertinentes. En este caso las diferencias
solo son para internet explorer 7-->

<!--[if IE 7]>
	<link href="../css/estilos_ie7.css" rel="stylesheet" type="text/css" />
	<link href="../css/secciones_ie7.css" rel="stylesheet" type="text/css" />    
<![endif]-->
<style>
#formCambioPass{
	color:#fff;text-shadow:0 -1px 0 rgba(0,0,0,0.25);
	background-color:#999;
	*background-color:#444;
	background-image:-moz-linear-gradient(top,#999,#444);
	background-image:-webkit-gradient(linear,0 0,0 100%,from(#999),to(#444));
	background-image:-webkit-linear-gradient(top,#999,#444);
	background-image:-o-linear-gradient(top,#999,#444);
	background-image:linear-gradient(to bottom,#999,#444);
	background-repeat:repeat-x;border-color:#bd362f #bd362f #802420;
	border-color:rgba(0,0,0,0.1) rgba(0,0,0,0.1) rgba(0,0,0,0.25);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffee5f5b',endColorstr='#ffbd362f',GradientType=0);
	filter:progid:DXImageTransform.Microsoft.gradient(enabled=false);
	border-right:1px #000 solid; border-bottom:1px #000 solid; border-radius:10px; width:230px; margin-top:150px; padding-top:10px
}
</style>
</head>
	
<body>
	<div id="central_oficina">
    	<nav>
            <ul>
            	<li class="ovirtual ovirtualActivo"><a href="#">AREA CLIENTES</a></li>
                <li class="contacto"><a href="../contacto.php">CONTACTO</a></li>
                <li class="tarifas"><a href="../tarifas.php">TARIFAS</a></li>
                <li class="servicios"><a href="../servicios.php">SERVICIOS</a></li>
                <li class="somos"><a href="../aracan.php">ARACAN</a></li>
            </ul>
        </nav>
    
<?php
//Se comprueba que el usuario se haya identificado, si no es asi se da un aviso y redirigimos a la seccion de identificacion

if ($_SESSION['usuario_mp']=="")
{	
	MsgBox($error_identificacion);
	redirigir("oficina_index.php?id=".$_SESSION["idioma"]);
	
}//if ($_SESSION['usuario'])

//Si el usuario se ha identificado la seccion se ejecutara con normalidad
else
{


//Si el usuario se ha identificado la seccion se ejecutara con normalidad

//Añadimos los fragmentos comunes, la cabecera, el menu, y el submenu

	
	?>
			<div>
<?php
        	//include($_SESSION["directorio_raiz"]."includes/cabecera.php");
			include("../includes/cabecera.php");            
?>
        </div>                     
          
   		<div class="posicion_menu">
<?php        
		 	//include($_SESSION["directorio_raiz"]."includes/menu.php");
			include("../includes/menu.php");
			
			
?>			            
        </div><!--<div class="posicion_menu">-->
	
           
<div class="contenido_seccion_oficina_cambio_pass">
        
<div class="posicion_cambio_pass_mp" id="formCambioPass">
      
                <form id="login" name="login" action="oficina_cambio_pass_mp.php?id=<?=$_SESSION["idioma"]?>" method="post">
                
                    <div class="componente_cambio_pass"><?=$oficina_cambio_pass1?></div>
                    
                  <div class="componente_cambio_pass">	                    
                    <input id="pass_vieja" name="pass_vieja" type="text" size="26" maxlength="15">
                  </div>
                    
                    <div class="componente_cambio_pass"><?=$oficina_cambio_pass2?></div>
                     
                     <div class="componente_cambio_pass">
                         <input id="pass_nueva" name="pass_nueva" type="password" size="26" maxlength="15">
                    </div>                
    
                    <div class="componente_cambio_pass"><?=$oficina_cambio_pass3?></div>
                     
                     <div class="componente_cambio_pass">
                         <input id="pass_nueva2" name="pass_nueva2" type="password" size="26" maxlength="15">
                    </div>                
                                    
                    <div class="boton_cambio_pass">   
                        <input type="submit" name="btn_aceptar" value="Aceptar" />
                    </div>
                                                                    
		        </form>
		</div><!--<div class="posicion_cambio_pass">-->


  </div><!--<div class="contenido_seccion_oficina">-->                        
		
        
        <div class="limpiar"></div>
                 <p>&nbsp;</p>
      <p>&nbsp;</p>
      <p>&nbsp;</p>             	
      <p>
  <!--Por ultimo aparecera el pie de la web-->
  <?php 
		include("../includes/pie.php");
		
//Cuando se pulse el boton aceptar del formulario se ejecutaran las consecuencias de las verificaciones
	if ((isset($_POST['btn_aceptar'])) && $_POST['btn_aceptar']=="Aceptar") 
	{	
//Mostraremos el mensaje correspondiente dependiendo de si el proceso ha ido bien o se ha encontrado algun problema		
		switch($error)
		{
			case 0:
//Primero comprobamos que la contraseña del usuario es la correcta					
				$rs_password = seleccion_condicional_campos("ContraseñasMultipuntos","Contraseña","CIF='".$_SESSION['usuario_mp']."'","Contraseña",0,$ruta_sevidor_bd,$usuario_bd,$contrasena_bd,$nombre_bd);			
		
				$registro_password = mysql_fetch_array($rs_password);
					
				if($registro_password['Contraseña']!=$pass_vieja)
				{					
					MsgBox($error_pass_invalida);		
				}//if($registro_password['Password']!=$pass_vieja)
				else
				{
					if($pass_nueva!=$pass_nueva2)
					{			
						MsgBox($pass_no_coincide);		
					}//if($pass_nueva!=$pass_nueva2)
					else
					{			
						$con=conectar($ruta_sevidor_bd,$usuario_bd,$contrasena_bd,$nombre_bd);																								
						mysql_query("UPDATE ContraseñasMultipuntos SET Contraseña ='".$pass_nueva."' WHERE CIF ='".$_SESSION['usuario_mp']."'",$con);
						
							
					$tiene_modificacion_contrasena_pendientes=0;

//Comprobamos si el cliente, tiene modificaciones pendientes
					$consulta_cliente=mysql_query("SELECT * FROM Cambio_Contrasena_MP WHERE CIF='".$_SESSION['usuario_mp']."'"); 
					
					$hay_cliente = mysql_fetch_array($consulta_cliente);
							
										
					if($hay_cliente)
					{$tiene_modificacion_contrasena_pendientes=1;} else {$tiene_modificacion_contrasena_pendientes=0;}
					
						if($tiene_modificacion_contrasena_pendientes==1)
						{
						$solicitud_insertar_contrasena="UPDATE `Cambio_Contrasena_MP` SET `PasswordAnt` = '".$pass_vieja."',`PasswordNuevo` = '".$pass_nueva."',`FechaRegistro` = '".date("Y-m-d H:i:s")."' WHERE `CIF` =".$_SESSION['usuario_mp'].";";
						
						}
						else{
						$solicitud_insertar_contrasena="INSERT INTO `Cambio_Contrasena_MP` (`CIF`,`PasswordAnt`,`PasswordNuevo`,`FechaRegistro`) VALUES ('".$_SESSION['usuario_mp']."','".$pass_vieja."','".$pass_nueva."','".date("Y-m-d H:i:s")."');";
					
						
						}
						mysql_query($solicitud_insertar_contrasena,$con);										
						MsgBox($mensaje_actualizar_pass);
						redirigir("oficina_presentacion_mp.php?id=".$_SESSION["idioma"]);				
					}//else($pass_nueva!=$pass_nueva2)			
				}//else($registro_password['Password']!=$pass_vieja)	
			break;
			
			case 1:
				MsgBox($error_rellenar);		
			break;
		}//switch($error)
	}//if ((isset($_POST['btn_aceptar'])) && $_POST['btn_aceptar']=="Aceptar")         
}//else ($_SESSION['usuario'])
?>        
      </p>
      


	</div><!--<div id="central_oficina"-->
</body>
</html>
