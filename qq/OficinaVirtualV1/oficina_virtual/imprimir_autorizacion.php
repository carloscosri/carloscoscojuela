<?php
/*
NOTAS:

1- ESTA SECCION TOMA LOS DATOS DE LA TABLA ContratosPDF

2- LA MAQUETACION DE LOS DATOS SE HA TENIDO QUE HACER CON TABLAS, YA QUE CON CAPAS ASIGNABA VALORES DIFERENCTES EN LA WEB, EN LA IMPRESION EN PAPEL Y EN LA IMPRESION PDF.

*/

///Añadimos el archivo que contiene el inicio de la sesion y las variables de sesion.  Es importante añadir
//este archivo antes de las cabeceras del documento o dara error
include("../includes/sesion.php");

//Añadimos la libreria de conexion a base de datos, que a su vez contiene las referencias a todos los archivos necesarios
include("../includes/conexion.php");




//Se comprueba que el usuario se haya identificado, si no es asi se da un aviso y redirigimos a la seccion de identificacion
if ($_SESSION['usuario']=="")
{	
	MsgBox($error_identificacion);
	redirigir("oficina_index.php?id=".$_SESSION["idioma"]);
	
}//if ($_SESSION['usuario'])

//Si el usuario se ha identificado la seccion se ejecutara con normalidad
else
{
//Se incluye el archivo con los textos especifico de esta seccion en el idioma oportuno
//	include($_SESSION["directorio_raiz"]."idiomas/".$_SESSION["idioma"]."/imprimir_contrato.php");
	include("../idiomas/".$_SESSION["idioma"]."/imprimir_contrato.php");
	include("../idiomas/".$_SESSION["idioma"]."/empresa_contacto.php");
		
//Recogemos los parametros que nos diran el contrato que se ha de leer. El contrato queda identificado con el numero del cliente y la fecha de alta
	$numero_cliente=$_SESSION['numeroCliente'];
	$fecha_alta=$_GET["fe"];

//Realizamos a consulta de los datos del titular
	$rs_cliente = seleccion_condicional ("DatosRegistrados", "CodigoCliente = '".$_SESSION['numeroCliente']."'","CodigoCliente",0,$ruta_sevidor_bd,$usuario_bd,$contrasena_bd,$nombre_bd);
	$registro_cliente= mysql_fetch_array($rs_cliente);
	
	$modificados_cliente = seleccion_condicional ("DatosModificados", "CodigoCliente = '".$_SESSION['numeroCliente']."'","CodigoCliente",0,$ruta_sevidor_bd,$usuario_bd,$contrasena_bd,$nombre_bd);
	$registro_modificado_cliente= mysql_fetch_array($modificados_cliente);
	
//Realizamos la consulta de los datos del contrato
	$rs_contrato = seleccion_condicional ("ContratosPDF", "NumeroCliente = '".$_SESSION['numeroCliente']."' AND FechaAlta='".$fecha_alta."'","FechaAlta",0,$ruta_sevidor_bd,$usuario_bd,$contrasena_bd,$nombre_bd);
	$registro_contrato = mysql_fetch_array($rs_contrato);

//Tambien consultamos los precios de las tarifas
	$rs_precios= seleccion_condicional ("TarifasDetalles", "Tarifa = '".$registro_cliente["TarifaContratada"]."'","Tarifa",0,$ruta_sevidor_bd,$usuario_bd,$contrasena_bd,$nombre_bd);
	$registro_precios = mysql_fetch_array($rs_precios);
	
//Y los precios de la energia reactiva	
	$rs_precios_reactiva= seleccion ("TarifasReactiva","PrecioReactiva1",0,$ruta_sevidor_bd,$usuario_bd,$contrasena_bd,$nombre_bd);
	$registro_precios_reactiva = mysql_fetch_array($rs_precios_reactiva);	
	
//Para mostrar por separado los distintos digitos de la cuenta bancaria los separamos
	$num_cuenta_banco=substr($registro_cliente["NumeroCuenta"],0,4);
	$num_cuenta_sucursal=substr($registro_cliente["NumeroCuenta"],4,4);
	$num_cuenta_digito_control=substr($registro_cliente["NumeroCuenta"],8,2);
	$num_cuenta_cuenta=substr($registro_cliente["NumeroCuenta"],10,10);
	
	
	$num_cuenta_banco2=substr($registro_modificado_cliente["NumeroCuenta"],0,4);
	$num_cuenta_sucursal2=substr($registro_modificado_cliente["NumeroCuenta"],4,4);
	$num_cuenta_digito_control2=substr($registro_modificado_cliente["NumeroCuenta"],8,2);
	$num_cuenta_cuenta2=substr($registro_modificado_cliente["NumeroCuenta"],10,10);
	
//Tambien capturamos la fecha de alta de contratacion y la pasamos a "formato texto"	

	//$dia_actual=substr($registro_contrato["FechaAlta"],8,2);	
	//$mes_actual=substr($registro_contrato["FechaAlta"],5,2);
	//$ano_actual=substr($registro_contrato["FechaAlta"],0,4);
	
	$mes_actual=date("m");
	
	
//Si el primer carácter del dia o del mes es 0, lo suprimiremos
	if(substr($dia_actual,0,1)==0)
	{
		$dia_actual=str_replace("0","",$dia_actual);
	}//if(substr($dia_actual,0,1)==0)

	if(substr($mes_actual,0,1)==0)
	{
		$mes_actual=str_replace("0","",$mes_actual);
	}//if(substr($mes_actual,0,1)==0)

//Por último el hay que pasar el mes al nombre del mes en vez de formato numerico
	switch($mes_actual)
	{
		case 1:
			$mes_actual_texto=$enero;
		break;
		
		case 2:
			$mes_actual_texto=$febrero;
		break;
		
		case 3:
			$mes_actual_texto=$marzo;
		break;
		
		case 4:
			$mes_actual_texto=$abril;
		break;
		
		case 5:
			$mes_actual_texto=$mayo;
		break;
		
		case 6:
			$mes_actual_texto=$junio;
		break;
		
		case 7:
			$mes_actual_texto=$julio;
		break;
		
		case 8:
			$mes_actual_texto=$agosto;
		break;
		
		case 9:
			$mes_actual_texto=$septiembre;
		break;
																		
		case 10:
			$mes_actual_texto=$octubre;
		break;
		
		case 11:
			$mes_actual_texto=$noviembre;
		break;	
		
		case 12:
			$mes_actual_texto=$diciembre;
		break;						
	}//switch($mes_actual)
	
?>
<!--**************************************SECCION NUEVA INTEGRAMENTE DESARROLLADA POR OSCAR RODRIGUEZ CALERO**********************-->
<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' />
<title><?=$titulo_web;?></title>

<link href="../css/estilos.css" rel="stylesheet" type="text/css" />
<link href="../css/secciones.css" rel="stylesheet" type="text/css" />

<!--Añadimos los estilos especificos de esta seccion-->
<link href="css/imprimir_contrato.css" rel="stylesheet" type="text/css" />

<!--La visualizacion de la impresion del contrato es completamente distinta para IE, por lo tanto le asignaremos su propia hoja de estilos-->
<!--[if IE]>
	<link href="css/imprimir_contratoIE.css" rel="stylesheet" type="text/css" />
<![endif]-->

<script language="javascript" type="text/javascript">
//<!--
//Esta funcion hace que al cargar la pagina se habra la ventana de dialogo de impresion
function imprimir()
{
	window.print();
}//function dimensiona()

//-->
</script>
</head>

<body onLoad="javascript:imprimir()">
<!--*************************************************Web realizada por ****************************************-->
    <div class="dimensiones_contrato">
        
    	<div class="margenes_impresion_contrato">

			
		    <div class="titulo_apartado_impresion_contrato"><br />
		      <br />
		      <br />
		      <br />
            </div>
    		
            <table width="200" border="0" cellpadding="0" cellspacing="0" class="tabla_impresion_contrato">
            	<tr><td width="200" colspan="2" class="celda_larga borde_contrato texto_contrato"> <br />
            	  <?=$registro_cliente["TitularNombre"]?>
           	  <br /></td></tr>
              	<tr>
              	  <td class="celda_larga borde_contrato texto_contrato" colspan="2"> <?=$registro_cliente["TitularCalle"]."   ".$registro_cliente["TitularNumero"]?>
           	      <br /></td></tr>
              	<tr>
              	  <td class="celda_larga borde_contrato texto_contrato" colspan="2"> <?=$registro_cliente["TitularCP"]."   ".$registro_cliente["TitularPoblacion"]?>
           	      <br />
           	     
           	      <br /></td></tr>
</table>
        
                   
		    <div class="titulo_apartado_impresion_contrato"></div>
<table width="600" border="1" cellspacing="0" cellpadding="0">
               <tr>
                 <td class="texto_contrato gris" colspan="4"><div align="center">Datos del punto de Suministro</div></td>
               </tr>
             </table>
             
             <table width="600" border="1" cellspacing="0" cellpadding="0" align="left">
               <tr align="center" class="texto_contrato gris">
                 <td width="300" ><strong><?=$nombre_razon_social?></strong></td>
                 <td width="300" ><strong><?=$direccion?></strong></td>
               
               </tr>
               <tr class="texto_contrato" align="center">
                 <td><?=$registro_cliente["TitularNombre"]?></td>
                 <td><?=$registro_cliente["SuministroCalle"]?></td>

               </tr>
             </table><br /><br />
             
             
             
          <br />
          <table width="600" border="1" cellpadding="0" cellspacing="0">
<tr align="center" class="texto_contrato gris">
        <td width="50" ><strong><?=$num?></strong></td>
        <td width="200" ><strong>Extensi&oacute;n</strong></td>
        <td width="100" ><strong>Aclarador</strong></td>
        <td width="100" ><strong>Número Cliente</strong></td>
                 <td width="150" ><strong>CUPS</strong></td>
      </tr>
               
               <tr class="texto_contrato" align="center">
                 <td><?=$registro_cliente["SuministroNumero"]?></td>
                 <td><?=$registro_cliente["SuministroExtension"]?></td>
                 <td><?=$registro_cliente["SuministroExtension2"]?></td>
                 <td><?=$registro_cliente["CodigoCliente"]?></td>
                 <td><?=$registro_cliente["CUPS"]?></td>
               </tr>
               
               
               
          </table>
      
<td class="celda_mitad borde_contrato texto_contrato" colspan="2"> 



 <?
 
//$Monate = array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'); 

  echo("<br />");
  echo($poblacion_empresa.", ");
  echo(date("d "));
  echo("de ");
  echo($mes_actual_texto);
  echo(" del ");
  echo(date("Y"));
  echo(".");
?>

</td>

         
          <p><br />
            Por la presente autorizo a la empresa:</p>
          
            <table class="padl2 texto_contrato12" width="430" border="0" align="left" cellpadding="0" cellspacing="0">
              <tr>
                <td width="596" colspan="2" ><strong>
                <?=$nombre_empresa_completo?>
                </strong></td>
              </tr>
              <tr>
                <td ><strong>
                <?=$direccion_empresa?>
                </strong></td>
              </tr>
              <br/>
              <tr>
                <td ><strong>
                <?=$cp_empresa?>
                ,<?=$poblacion_empresa?> 
                </strong></td>
              </tr>
              </table>
            <p><br />
              <br />
              
            </p>
<p>&nbsp;</p>
          <p>a efectuar el cargo de los recibos correspondientes al suministro de energ&iacute;a el&eacute;ctrica,<br />
a la cuenta bancaria que a continuaci&oacute;n se detalla:<br />
        </p>
         
                <br/><br/>
                <table class="padl2 texto_contrato" width="500" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="161"><strong><?=$entidad_financiera?>:</strong></td>
                    <td width="339"><div align="left"><?= $registro_modificado_cliente["BancoDomiciliado"] ?></div></td>
                  </tr>
                                
                  
                  <tr>
                    <td><strong><?=$num_cuenta?>:</strong></td>
                    <td><div class="arial14b" align="left">
                      <?=$num_cuenta_banco2?>
                      -
                      <?=$num_cuenta_sucursal2?>
                      -
                      <?=$num_cuenta_digito_control2?>
                      -
                      <?=$num_cuenta_cuenta2?>
                   </div></td>
                  </tr>
                </table>
                <tr>                
                  <td ><br />
                  <br /></td>
                </tr>
                
         
          
          
<table class="texto_contrato" border="0" cellpadding="0" cellspacing="0" >
               <tr>
                    <td>
                    
                    Atentamente,
                    
                    </td>
                  </tr>
            </table>
          <p>&nbsp;</p>
          <p>&nbsp;</p>
          <p><br />
            <br />
            
            
            
          </p>
          <table class="texto_contrato" border="0" cellpadding="0" cellspacing="0" >
<tr>
                    <td colspan="2">
                     <em> <?=$registro_cliente["TitularNombre"]?></em>
  </td>
              </tr>
                  <tr>
                    <td >
                      
                      <?=$cif_nif?>
                      :<em>
                      <?=$registro_cliente["DNI"]?>
                      </em></td>
                  </tr>
            </table>
                
        </div>
    </div>
  
<!--*****************************************************************(FIN)HOJA 4********************************************************-->
<!--************************************************************************************************************************************-->

</body>
</html>
<?php
}//else($_SESSION['usuario'])
?>
<!--**************************************SECCION NUEVA INTEGRAMENTE DESARROLLADA POR OSCAR RODRIGUEZ CALERO**********************-->