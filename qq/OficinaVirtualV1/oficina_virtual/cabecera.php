<?php	
//Llamamos a la funcion que nos devuelve el nombre del script en ejecucion, para hacer el enlace a los idiomas correcto
	$pagina_actual=nombre_script_en_ejecucion();	

//Los nombres de las subsecciones deberan de ir precedidos del nombre de la seccion separado por un guion bajo (empresa_descripcion.php). De esta forma podemos automatizar el cambio de foto de la cabecera. Como hay una fotografia por seccion esta se llamara igual que la seccion
//cabecera		
	$nombre_seccion=explode("_",$pagina_actual);
	
//La cabecera mostrara una fotografía u otra dependiendo de la seccion en la que nos encontremos en cada momento	
?>   

<div class="logo_cabecera_seccion">
<?php	
//La ruta de la imagen de la cabecera cambiara si se accede desde la oficina virtual
	if($_SESSION['oficina_virtual']!=1)
	{
?>
		<a href="index.php?id=<?=$_SESSION["idioma"]?>"><img src="img/comun/logo1.jpg" alt="<?=$titulo_web;?>" border="0" title="<?=$titulo_web;?>" /></a>
<?php
	}//if($_SESSION['oficina_virtual']!=1)
	else
	{	
?>
		<a href="../index.php?id=<?=$_SESSION["idioma"]?>"><img src="../img/comun/logo1.jpg" alt="<?=$titulo_web;?>" border="0" title="<?=$titulo_web;?>" /></a>
<?php	
	}//else($_SESSION['oficina_virtual']!=1)
?>    
</div><!--<div class="logo_cabecera_seccion">-->


<div class="foto_cabecera_seccion">
<?php	
//La ruta de la imagen de la cabecera cambiara si se accede desde la oficina virtual
	if($_SESSION['oficina_virtual']!=1)
	{
?>
		<img src="img/comun/logo2.jpg" alt="" />
        
<?php
	}//if($_SESSION['oficina_virtual']!=1)
	else
	{	
?>
		<img src="../img/comun/logo2.jpg" alt="" />		
<?php	
	}//else($_SESSION['oficina_virtual']!=1)
?>

<style>
/* Contact Bar */
.themeblvd-contact-bar ul { margin:0; }
.themeblvd-contact-bar li { float:left; list-style:none; padding:1px; }
.themeblvd-contact-bar li a { background-image:url(../images/parts/social-media-grey.png); display:block; width:24px; height:24px; text-indent:-9999px; }
.themeblvd-contact-bar .dark li a { background-image:url(../images/parts/social-media-dark.png); }
.themeblvd-contact-bar .grey li a { background-image:url(../images/parts/social-media-grey.png); }
.themeblvd-contact-bar .light li a { background-image:url(../images/parts/social-media-light.png); }
.themeblvd-contact-bar li .amazon { background-position:0 0; }
.themeblvd-contact-bar li .delicious { background-position:0 -24px; }
.themeblvd-contact-bar li .deviantart { background-position:0 -48px; }
.themeblvd-contact-bar li .digg { background-position:0 -72px; }
.themeblvd-contact-bar li .dribbble { background-position:0 -96px; }
.themeblvd-contact-bar li .ebay { background-position:0 -120px; }
.themeblvd-contact-bar li .email { background-position:0 -144px; }
.themeblvd-contact-bar li .facebook { background-position:0 -168px; }
.themeblvd-contact-bar li .feedburner { background-position:0 -192px; }
.themeblvd-contact-bar li .flickr { background-position:0 -216px; }
.themeblvd-contact-bar li .forrst { background-position:0 -240px; }
.themeblvd-contact-bar li .foursquare { background-position:0 -264px; }
.themeblvd-contact-bar li .github { background-position:0 -288px; }
.themeblvd-contact-bar li .google { background-position:0 -312px; }
.themeblvd-contact-bar li .instagram { background-position:0 -336px; }
.themeblvd-contact-bar li .linkedin { background-position:0 -360px; }
.themeblvd-contact-bar li .myspace { background-position:0 -384px; }
.themeblvd-contact-bar li .paypal { background-position:0 -408px; }
.themeblvd-contact-bar li .picasa { background-position:0 -432px; }
.themeblvd-contact-bar li .pinterest { background-position:0 -456px; }
.themeblvd-contact-bar li .reddit { background-position:0 -480px; }
.themeblvd-contact-bar li .rss { background-position:0 -504px; }
.themeblvd-contact-bar li .scribd { background-position:0 -528px; }
.themeblvd-contact-bar li .squidoo { background-position:0 -552px; }
.themeblvd-contact-bar li .technorati { background-position:0 -576px; }
.themeblvd-contact-bar li .tumblr { background-position:0 -600px; }
.themeblvd-contact-bar li .twitter { background-position:0 -624px; }
.themeblvd-contact-bar li .vimeo { background-position:0 -648px; }
.themeblvd-contact-bar li .xbox { background-position:0 -672px; }
.themeblvd-contact-bar li .yahoo { background-position:0 -696px; }
.themeblvd-contact-bar li .youtube { background-position:0 -720px; }
</style>   

<div class="social-media">
	<div class="themeblvd-contact-bar">
    	<ul class="grey"><li><a href="https://www.facebook.com/pages/Integra-Energ%C3%ADa/491914580869088" title="Facebook" class="facebook" target="_blank">Facebook</a></li><li><a href="http://www.linkedin.com" title="Linkedin" class="linkedin" target="_blank">Linkedin</a></li><li><a href="https://twitter.com/IntegraEnergia" title="Twitter" class="twitter" target="_blank">Twitter</a></li><li><a href="http://www.integraenergia.es/feed/rss/" title="Rss" class="rss" target="_blank">Rss</a></li></ul><div class="clear"></div></div><!-- .themeblvd-contact-bar (end) -->			</div><!-- .social-media (end) -->

 
</div><!--<div class="foto_cabecera_seccion">-->
