<?php
/* NOTA:
 COMO NO SE CIERRA LA CONEXION TRAS CADA CONEXION CON LA BASE DE DATOS, LAS CONSULTAS DEJAN DE FUNCIONAR SI SE ESTABLECE COMO UNICA CONEXION
 LA QUE UTILIZA EL RESTO DE LA WEB, POR LO TANTO HASTA QUE SE HAGA LA MIGRACION COMPLETA DE LOS TEXTOS, COEXISTIRAN LAS DOS CONEXIONES, YA QUE
 SE HA AÑADIDO UNA LLAMADA A LA CONEXION BUENA EN ESTE ARCHIVO.
*/

include("_conexion.php");

//Se incluye el archivo con los textos especifico de esta seccion en el idioma oportuno
include("../idiomas/".$_SESSION["idioma"]."/oficina_presentacion.php");
include("../idiomas/".$_SESSION["idioma"]."/oficina_submenu.php");

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<title><?=$titulo_web;?></title>
<script language="JavaScript" type="text/javascript" src="ajax.js"></script>
<!--Se añaden las hojas de estilos genericas que utiliza esta seccion-->
<link href="../css/estilos.css" rel="stylesheet" type="text/css" />
<link href="../css/secciones.css" rel="stylesheet" type="text/css" />
<link href="css/oficina.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_presentacion_mp.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_presentacion.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_vieja.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_informacion_contratos_y_facturas.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_informacion_certificadofacturas.css" rel="stylesheet" type="text/css" />

<link rel="stylesheet" type="text/css" media="all" href="scripts/calendar/skins/aqua/theme.css" title="Aqua" />
<link rel="stylesheet" type="text/css" media="all" href="css/colores.css" title="Aqua" />

<script type="text/javascript" src="scripts/calendar/calendar.js"></script>
<!-- librería para cargar el lenguaje deseado -->
<script type="text/javascript" src="scripts/calendar/lang/calendar-es.js"></script>
<!-- librería que declara la función Calendar.setup, que ayuda a generar un calendario en unas pocas líneas de código -->
<script type="text/javascript" src="scripts/calendar/calendar-setup.js"></script>

<!--Ahora se añaden las reglas para Internet Explorer. Asi corregiremos las diferencias de visualizacion entre navegadores
Es importante añadirlo despues de las reglas principales, para que se sobreescriban las correcciones pertinentes. En este caso las diferencias
solo son para internet explorer 7-->

<!--[if IE 7]>
	<link href="../css/estilos_ie7.css" rel="stylesheet" type="text/css" />
	<link href="../css/secciones_ie7.css" rel="stylesheet" type="text/css" />    
<![endif]-->

<style>
#fechasExcel, #fechasPDF, #comisiones{
	position:fixed;
	left:50%;
	top:50%;
	margin-left:-250px; 
	margin-top:-190px;        
    z-index:2;
    /*overflow:auto;*/
    display:none;               
	background:#f0f0f0;
	border: 2px solid #CCC;                   
	width:500px;
	padding:10px 10px 10px 10px;
	height:420px;
}
#comisiones{ height:240px;}
input.submit1{width:70px; height:70px; background-image:url(../img/oficina/excel.png); text-indent:-5000px}
input.submit2{width:70px; height:70px; background-image:url(../img/oficina/pdf.png); text-indent:-5000px}
#fechasExcel table{ font-weight:normal;}

table.resultados{width:950px;border-collapse:collapse; text-align:center}
tr.cabecera{font-size:15px; font-style:italic; border-bottom:2px solid #FFF; font-weight:bold;}
tr.cabecera td{padding:7px; color:#003C58}
tr.linea td{ border-bottom: 1px solid rgba(0, 60, 88, 0.25);
    height: 15px;
    padding: 3px 3px 8px;}
	
tr.inputs{ background-color: #003c58;
    height: 10px;
    padding: 10px 0;}
tr.inputs input { font-weight:bold; border-radius:5px;  box-shadow:none }
.calendar{z-index:10000;}
</style>

</head>

<body>
<?php
$hoy = date("Y-m-d");
$hoy = explode("-",$hoy);

$fecha_inicio_ver = '2013-'.$hoy[1].'-01';
$fecha_fin_ver=date("Y-m-d");
?>

<div id="fechasExcel">
<a href="#" onclick="muestra_oculta('fechasExcel')"><?=$cerrar;?></a>
<h3 align="center" style="color:#000"><?=$resumenFacturas;?></h3>
<form action="obtenerfacturasAgentes.php" method="POST" onsubmit="muestra_oculta('fechasExcel')">
    <input type="text" value="<?=$_SESSION['password_mp'];?>" name="nif" style="display:none;" />
	<input type="text" value="<?=$_SESSION['usuario_mp'];?>" name="dni" style="display:none;" />
    
    <table borde="0" align="center" width="480" style="color:#000">
    <tr>
    	<td colspan="2" align="center"><?=$resumenFacturasIntro;?> <i></i><br /><br /></td>
    </tr>
    <tr>
    	<td><?=$fechaInicio;?></td><td><?=$fechaFin;?></td>
    </tr>
    <tr>	
        <td>
        <input name="FechaInicial"  id="sel1" value="<?=$fecha_inicio_ver?>"  type="text" class="textfield" size="15" maxlength="10"  onchange="MostrarConsultaAgentes(this.value, this.form.FechaFinal.value, this.form.nif.value)"> 
                            &nbsp;                    
        <input type="button" id="lanzador" value="..." />
        <!-- script que define y configura el calendario-->
        <script type="text/javascript">
			Calendar.setup({
				inputField : "sel1", // id del campo de texto
				ifFormat : "%Y-%m-%d", // formato de la fecha que se escriba en el campo de texto
				button : "lanzador" // el id del botón que lanzará el calendario
			});
        </script>
        </td><td>
        <input name="FechaFinal" id="sel2" value="<?=$fecha_fin_ver?>"type="text" class="textfield" size="15" maxlength="10"  onchange="MostrarConsultaAgentes(this.form.FechaInicial.value, this.value, this.form.nif.value); return false">&nbsp;
        
        <input type="button" id="lanzador2" value="..." />
        <!-- script que define y configura el calendario-->
        <script type="text/javascript">
			Calendar.setup({
				inputField : "sel2", // id del campo de texto
				ifFormat : "%Y-%m-%d", // formato de la fecha que se escriba en el campo de texto
				button : "lanzador2" // el id del botón que lanzará el calendario
			});
        </script></td>
    </tr>
    <tr>
        <td colspan="2" align="center">
        <br />
		<!-- DEJAMOS COMENTADA LA LINEA DEL EXCEL A LA ESPERA DE INSTRUCCIONES -->
        <!--<input type="submit" name="submit" class="submit1" value="GENERAR EXCEL" />&nbsp;&nbsp;-->
        <!--<input type="submit" name="submit" class="submit2" value="GENERAR PDF" />-->
		<input type="submit" name="submit" class="submit2" value="DESCARGAR PDF" />
        </td>
    </tr>
	<tr>
        <td colspan="2" align="center">
        <br />
        <!-- <span>GENERAR EXCEL</span> -->
		&nbsp;&nbsp;
        <!-- <input type="submit" name="submit" class="submit2" value="GENERAR PDF" /> -->
        <span>DESCARGAR PDF</span>
        </td>
    </tr>
    </table>
    <div id="resultado" style="padding-top:10px;min-height:200px; max-height:300px;"></div>
    
</form>
</div>

<div id="comisiones">
<a href="#" onclick="muestra_oculta('comisiones')">Cerrar</a>
<h3 align="center">Resumen de Comisiones </h3>
<form action="" method="POST" onsubmit="muestra_oculta('comisiones')">
    <input type="text" value="<?=$_SESSION['usuario_mp'];?>" name="nif" style="display:none;" />
    
    <table borde="0" align="center" width="480">
    <tr>
    	<td colspan="2" align="center">Seleccione el rango de fechas de las Comisiones que desee consultar<i></i><br /><br /></td>
    </tr>
    <tr>
    	<td><?=$fechaInicio;?></td><td><?=$fechaFin;?></td>
    </tr>
    <tr>	
        <td>
        <input name="fechainicio"  id="sel3" value="<?=$fecha_inicio_ver?>"  type="text" class="textfield" size="15" maxlength="10" onchange="MostrarConsultaAgentes(this.value, this.form.FechaFinal.value, this.form.nif.value)"> 
                            &nbsp;                    
        <input type="button" id="lanzador3" value="..." />
        <!-- script que define y configura el calendario-->
        <script type="text/javascript">
			Calendar.setup({
				inputField : "sel3", // id del campo de texto
				ifFormat : "%Y-%m-%d", // formato de la fecha que se escriba en el campo de texto
				button : "lanzador3" // el id del botón que lanzará el calendario
			});
        </script>
        </td><td>
        <input name="fechafin" id="sel4" value="<?=$fecha_fin_ver?>"type="text" class="textfield" size="15" maxlength="10" onchange="MostrarConsultaAgentes(this.form.FechaInicial.value, this.value, this.form.nif.value)" >&nbsp;
        
        <input type="button" id="lanzador4" value="..." />
        <!-- script que define y configura el calendario-->
        <script type="text/javascript">
			Calendar.setup({
				inputField : "sel4", // id del campo de texto
				ifFormat : "%Y-%m-%d", // formato de la fecha que se escriba en el campo de texto
				button : "lanzador4" // el id del botón que lanzará el calendario
			});
        </script></td>
    </tr>
    <tr>
        <td colspan="2" align="center">
        <br />
        <input type="submit" name="submit" value="VER COMISIONES" />
        </td>
    </tr>
    </table>
    <!--<div id="resultado" style="padding-top:10px;min-height:200px; max-height:300px;"></div>-->
    
</form>
</div>

	<div id="central_oficina" style="top:0px">

<?php
//Se comprueba que el usuario se haya identificado, si no es asi se da un aviso y redirigimos a la seccion de identificacion
if ($_SESSION['usuario_mp']=="")
{	
	MsgBox($error_identificacion);
	redirigir("oficina_index.php?id=".$_SESSION["idioma"]);
	
} else {
include("../includes/cabecera.php");
//Añadimos los fragmentos comunes, la cabecera, el menu, y el submenu
	?>

<div class="posicion_menu">
<?php        
		 	//include($_SESSION["directorio_raiz"]."includes/menu.php");
			include("../includes/menu.php");
			
			
?>			            
        </div>

<div class="contenido_seccion_oficina_presentacion">
<script>
function muestra_oculta(id){
	if (document.getElementById){ 
	var el = document.getElementById(id); 
	el.style.display = (el.style.display == 'none') ? 'block' : 'none'; 
	}
}
window.onload = function(){
	muestra_oculta('fechasExcel');
	muestra_oculta('comisiones');
	muestra_oculta('fechasPDF');
}
</script>

<!--Como el texto de oficina virtual ya lo tenemos añadido en el menu, en vez de duplicar el texto en la seccion correspondiente llamamos a la misma variable que el menu ($menu4)-->        
	<div class="titulo_seccion" style="height:25px;"><?=$menu4?> - Portal de Agente</div>
    <div class="menu_oficina" style="text-align:right;margin-top: 5px;"><a href="logout.php?id=<?=$_SESSION["idioma"]?>" class="enlace"><?=$cerrar_sesion?></a></div>		      
			<div>
                   <strong>A continuacion se muestran todos los puntos de suministro. </strong><br />
                   Puede filtrar introduciendo el valor deseado en cada columna y pulsando 'ENTER'<br />
                   </div>
              <form name="cambios">
                   <div align="center">
                   
                      <br />					  
                      <p align="right" style="padding-right:10px;">
                        <a href="#" onclick="muestra_oculta('fechasExcel')" style="text-decoration:none; color:#FFF">
						Resumen Facturas &nbsp; <img src="../img/oficina/facturas.png" border="0" align="absmiddle" width="35" /></a>
                        <br />
                        <!--<a href="#"  onclick="muestra_oculta('comisiones')" style="text-decoration:none">
                        Resumen Comisiones &nbsp; <img src="../img/comun/moneybag_dollar.png" border="0" align="absmiddle" width="25" /></a> -->						
                      </p>
						
                      <div id="resultadoAgente" style="padding-top:10px;">
                      <?php
                        $consulta_clientes="select * from DatosRegistrados where Agente IS NOT NULL and Agente IN (select Numero from Comisionistas where Nombre like '".$_SESSION['usuario_mp']."%')";
						$resultados_clientes = mysql_query($consulta_clientes);	
						?>
                        <table class="resultados">
                            <tr class="cabecera">
                                <td>Cliente</td>
                                <td>DNI</td>
                                <td>Titular</td>
                                <td>Direccion</td>
                                <td>Numero</td>
                                <td>Provincia</td>
                                <td>CUPS</td>
                                <td>Acceso</td>
                            </tr>
                            
                           <tr class="inputs">
                                <td><input name="cliente" type="text" style="width:50px;margin: 5px;" onchange="MostrarConsulta2(this.value, document.forms.cambios.dni.value,document.forms.cambios.titular.value,document.forms.cambios.direccion.value,document.forms.cambios.numero.value,document.forms.cambios.provincia.value,document.forms.cambios.cups.value,'<?php echo $_SESSION['usuario_mp'];?>')" /></td>
                                
                                <td><input name="dni" type="text" style="width:72px;margin: 5px;" onchange="MostrarConsulta2(document.forms.cambios.cliente.value, this.value,document.forms.cambios.titular.value,document.forms.cambios.direccion.value,document.forms.cambios.numero.value,document.forms.cambios.provincia.value,document.forms.cambios.cups.value,'<?=$_SESSION['usuario_mp'];?>')"/></td>
                                
                                <td><input name="titular" type="text" style="width:145px;margin: 5px;" onchange="MostrarConsulta2(document.forms.cambios.cliente.value, document.forms.cambios.dni.value,this.value,document.forms.cambios.direccion.value,document.forms.cambios.numero.value,document.forms.cambios.provincia.value,document.forms.cambios.cups.value,'<?=$_SESSION['usuario_mp'];?>')" /></td>
                                
                                <td><input name="direccion" type="text" style="width:120px;margin: 5px;" onchange="MostrarConsulta2(document.forms.cambios.cliente.value, document.forms.cambios.dni.value,document.forms.cambios.titular.value,this.value,document.forms.cambios.numero.value,document.forms.cambios.provincia.value,document.forms.cambios.cups.value,'<?=$_SESSION['usuario_mp'];?>')" /></td>
                                
                                <td><input name="numero" type="text" style="width:50px;margin: 5px;" onchange="MostrarConsulta2(document.forms.cambios.cliente.value, document.forms.cambios.dni.value,document.forms.cambios.titular.value,document.forms.cambios.direccion.value,this.value,document.forms.cambios.provincia.value,document.forms.cambios.cups.value,'<?=$_SESSION['usuario_mp'];?>')" /></td>
                                
                                <td><input name="provincia" type="text" style="width:80px;margin: 5px;" onchange="MostrarConsulta2(document.forms.cambios.cliente.value, document.forms.cambios.dni.value,document.forms.cambios.titular.value,document.forms.cambios.direccion.value,document.forms.cambios.numero.value,this.value,document.forms.cambios.cups.value,'<?=$_SESSION['usuario_mp'];?>')" /></td>
                                
                                <td><input name="cups" type="text" style="width:155px" onchange="MostrarConsulta2(document.forms.cambios.cliente.value, document.forms.cambios.dni.value,document.forms.cambios.titular.value,document.forms.cambios.direccion.value,document.forms.cambios.numero.value,document.forms.cambios.provincia.value,this.value,'<?=$_SESSION['usuario_mp'];?>')" /></td>
                                <script>
								function borrar(){
									location.href='oficina_presentacion_comercial.php';
								}
								</script>
								<td><input type="reset" value="Limpiar" onclick="javascript:borrar()" /></td>
                            </tr>
                            
                            <?php
                            $i=1;
                            //muestra los datos consultados
                            while($registro_clientes = mysql_fetch_array($resultados_clientes)){
                                ?>
                                    
                                  <tr class="linea">
                                    <td><?=$registro_clientes['CodigoCliente'];?></td>
                                    <td><?=$registro_clientes['DNI'];?></td>
                                    <td><?=utf8_decode($registro_clientes['TitularNombre']);?></td>
                                    <td><?=$registro_clientes['TitularCalle'];?></td>
                                    <td><?=utf8_decode($registro_clientes['TitularNumero']);?></td>
                                    <td><?=utf8_decode($registro_clientes['TitularProvincia']);?></td>
                                    <td><?=$registro_clientes['CUPS'];?></td>
                                    <td>
                                        <a href="oficina_valida_login_mp.php?CodigoCliente=<?=$registro_clientes["CodigoCliente"]?>&amp;Password=<?=$registro_clientes["Password"]?>"> 
                                            <img src="../img/comun/entrar.gif" border="0" /> 
                                        </a>
                                    </td>
                                 </tr>   
                                <?php
                                $i++;
                            }
                        ?>
                       </table>
                      </div>
           			</div></form>
                    <p style="margin-top:50px;"> </p>
                    <!--<?=$i;?>-->
                    <div class="limpiar"></div>
                    <br />
                    <!--Por ultimo aparecera el pie de la web-->
                 </div>
<?php 
	include("../includes/pie.php");
}
?>
	<p>&nbsp;</p>
</div>
</body>
</html>