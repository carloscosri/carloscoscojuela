<?php
/*
NOTAS:

1- ESTA SECCION TOMA LOS DATOS DE LA TABLA DatosRegistrados

*/

/* NOTA:

 COMO NO SE CIERRA LA CONEXION TRAS CADA CONEXION CON LA BASE DE DATOS, LAS CONSULTAS DEJAN DE FUNCIONAR SI SE ESTABLECE COMO UNICA CONEXION
 LA QUE UTILIZA EL RESTO DE LA WEB, POR LO TANTO HASTA QUE SE HAGA LA MIGRACION COMPLETA DE LOS TEXTOS, COEXISTIRAN LAS DOS CONEXIONES, YA QUE
 SE HA AÑADIDO UNA LLAMADA A LA CONEXION BUENA EN ESTE ARCHIVO.
*/

include("_conexion.php");

//Se incluye el archivo que contiene las verificaciones de datos
include("../includes/verificaciones.php");

include("../idiomas/".$_SESSION["idioma"]."/oficina_informacion_facturas.php");


include("../idiomas/".$_SESSION["idioma"]."/oficina_informacion_certificadofacturas.php");

//Se incluye el archivo con los textos especifico de esta seccion en el idioma oportuno
//include($_SESSION["directorio_raiz"]."idiomas/".$_SESSION["idioma"]."/oficina_incidencias.php");

include("../idiomas/".$_SESSION["idioma"]."/oficina_incidencias.php");

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<title><?=$titulo_web;?></title>

<!--Se añaden las hojas de estilos genericas que utiliza esta seccion-->
<link href="../css/estilos.css" rel="stylesheet" type="text/css" />
<link href="../css/secciones.css" rel="stylesheet" type="text/css" />
<link href="css/oficina.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_vieja.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_incidencias.css" rel="stylesheet" type="text/css" />

<!--Ahora se añaden las reglas para Internet Explorer. Asi corregiremos las diferencias de visualizacion entre navegadores
Es importante añadirlo despues de las reglas principales, para que se sobreescriban las correcciones pertinentes. En este caso las diferencias
solo son para internet explorer 7-->
<script language=javascript type=text/javascript>
function stopRKey(evt) {
var evt = (evt) ? evt : ((event) ? event : null);
var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
if ((evt.keyCode == 13) && (node.type=="text")) {return false;}
}
document.onkeypress = stopRKey;
</script>
<!--[if IE 7]>
	<link href="../css/estilos_ie7.css" rel="stylesheet" type="text/css" />
	<link href="../css/secciones_ie7.css" rel="stylesheet" type="text/css" />
<![endif]-->

</head>

<?php

//Asignamos el valor por defecto a la fecha, este cambiara si el usuario pulsa el boton del formulario habiendo escrito algo
$fecha=date("d-m-Y");
//En este caso tambien asignamos el valor por defecto a la fecha y la hora
$hora=date("H");
$minutos=date("i");

if(isset($_POST["Submit"]))
{


	$fecha="";
	$hora="";
	$minutos="";

//En el caso de que el usuaro acepte el formulario se relaizaran las comprobaciones

//Se recogen los datos del informante
	$nombre_informante="";
	$direccion_informante="";
	$poblacion_informante="";
	$telefono_informante="";
	$movil_informante="";
	$fax_informante="";
	$email_informante="";

//Se recogen los datos de la incidencia
	$direccion_incidencia="";
	$poblacion_incidencia="";
	$descripcion_incidencia="";

//-------------------------------------------------------------------------------------
//En el caso de que el usuaro acepte el formulario se relaizaran las comprobaciones
	$fecha=$_POST["FechaIncidencia"];
	$hora=$_POST["HoraIncidencia"];
	$minutos=$_POST["MinutosIncidencia"];



//Se recogen los datos del informante
	$nombre_informante=$_POST["NombreInformante"];
	$direccion_informante=$_POST["DireccionInformante"];
	$poblacion_informante=$_POST["PoblacionInformante"];
	$telefono_informante=$_POST["Telefono1"];
	$movil_informante=$_POST["Telefono2"];
	$fax_informante=$_POST["FaxInformante"];
	$email_informante=$_POST["EmailInformante"];

//Se recogen los datos de la incidencia
	$direccion_incidencia=$_POST["DireccionIncidencia"];
	$poblacion_incidencia=$_POST["PoblacionIncidencia"];
	$descripcion_incidencia=$_POST["Descripcion"];

//Esta variable controlara si se produce algun error en las verificaciones
	$error=0;

//Comprobamos que se han escrito todos los campos obligatorios
	if($fecha=="" or $hora=="" or $minutos=="" or $nombre_informante=="" or $direccion_informante=="" or $poblacion_informante=="" or $telefono_informante=="" or $direccion_incidencia=="" or $poblacion_incidencia=="" or $descripcion_incidencia=="")
	{
		$error=1;
	}//if($fecha=="" or $hora=="" or $minutos=="" or $nombre_informante=="" or $direccion_informante=="" or $poblacion_informante=="" or $telefono_informante=="" or $direccion_incidencia=="" or $poblacion_incidencia=="" or $descripcion_incidencia=="")

//Comprobamos que la fecha se ha escrito correctamente
	if($error==0 and (comprobar_fecha($fecha)))
	{
		$error=2;
	}//if($error==0 and (comprobar_fecha($fecha)))

//Si no se ha producido un error anteriormente pasaremos a comprobar las fechas
	if ($error==0)
	{
//Esta variable capturara la fecha actual del sistema, la usaremos para comparar con las fechas escritas por el usuario
		$fecha_actual=date("d-m-Y");

//Esta variable almacenara los dias trascurridos desde la fecha de lectura a la fecha actual, respectivamente
		$diferencia_fecha=dias_trascurridos($fecha,$fecha_actual);

//Comprobamos que las fechas introducidas por el usuario no sean posteriores a la fecha actual
		if($diferencia_fecha<0)
		{
			$error=6;
		}//if($diferencia_fecha<0)
	}//if ($error==0)

//Una vez comprobada la fecha se comprueba que la hora se haya escrito correctamente. Para ello solo deben contener 2 caracteres numericos entre 00-23 para la hora y entre 00-59 para los minutos
	if($error==0 and (strlen($hora)!=2 or solo_numero($hora)!=0 or intval($hora)<0 or intval($hora)>23 or strlen($minutos)!=2 or solo_numero($minutos)!=0 or intval($minutos)<0 or intval($minutos)>59))
	{
		$error=3;
	}//if($error==0 and (strlen($hora)!=2 or solo_numero($hora)!=0 or intval($hora)<0 or intval($hora)>23 or strlen($minutos)!=2 or solo_numero($minutos)!=0 or intval($minutos)<0 or intval($minutos)>59))

//Ahora comprobaremos los campos numericos
	if($error==0 and (solo_numero($telefono_informante) or solo_numero($movil_informante) or solo_numero($fax_informante)))
	{
		$error=4;
	}//if($error==0 and (solo_numero($telefono_informante) or solo_numero($movil_informante) or solo_numero($fax_informante)))

//Por último nos queda comprobar si el correo electronico es correcto, en el caso en el que se haya escrito algo en el campo
	if($error==0 and $email_informante!="")
	{
		if(!comprobar_email($email_informante))
		{
			$error=5;
		}//if(!comprobar_email($email_informante))
	}//if($error==0 and $email_informante!="")
}//}//if(isset($_POST["Submit"]))

?>

<body>
<!--*************************************************Web realizada por ****************************************-->
	<div id="central_oficina">

<?php
//Se comprueba que el usuario se haya identificado, si no es asi se da un aviso y redirigimos a la seccion de identificacion
if ($_SESSION['usuario']=="")
{
	MsgBox($error_identificacion);
	redirigir("oficina_index.php?id=".$_SESSION["idioma"]);

}//if ($_SESSION['usuario'])

//Si el usuario se ha identificado la seccion se ejecutara con normalidad
else
{
//Añadimos los fragmentos comunes, la cabecera, el menu, y el submenu
	include("oficina_fragmentos_comunes.php");
?>
		<div class="contenido_seccion_oficina_incidencias">

<!--VERSION VIEJA-->

		<form action="oficina_incidencias.php?id=<?=$_SESSION["idioma"]?>" method="post" name="Incidencias" id="Incidencias">

        <table width="650" border="0" cellspacing="0" cellpadding="0" >
          <!--DWLayoutTable-->
          <?php $result=mysql_query("SELECT * FROM ".$_SESSION['tablaIdioma']." WHERE idTexto=113");
		  mysql_query("SET NAMES 'utf8'");
		  $row = mysql_fetch_array($result);?>
          <tr align="center">
            <td height="30" colspan="2" class="arialblack18Titulo"  style=" background-image:url(imagenes/MarcoInfo.gif);background-repeat:no-repeat"><?php echo($row[1]);?></td>
          </tr>
          <tr>
            <td width="650" height="140" valign="middle"><div align="justify"  class="arial12Importanteb" style="border: 1px solid rgb(201, 43, 43); padding: 5px; background-color: rgb(245, 220, 220); text-decoration: none;">
			<!-- CAMBIAR EN LA BBDD 669 (Nombre Empresa) y 671 (Empresa + Dirección + ',' al final) -->
			<?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 668'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
			<?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 669'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
			<?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 670'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
			<?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 671'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
			<?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 672'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
            </div></td>
            <td width="3"></td>
          </tr>
          <tr>
            <td colspan="2" class="arial14" align="center"> <fieldset >
                <legend class="arialblack14">
                <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 130'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                </legend>
                <table width="500" align="center">
                  <!--DWLayoutTable-->
			  <?php if (isset($_SESSION['numeroCliente'])){
					 		$result = mysql_query("SELECT * FROM DatosRegistrados WHERE CodigoCliente = ".$_SESSION['numeroCliente']." ");
							$row = mysql_fetch_array($result);
					 }else{
					 		$result = mysql_query("SELECT * FROM DatosRegistrados WHERE CodigoCliente = -1");
							//Para que no tome ningun cliente pero no de ningun error en la pagina
							$row = mysql_fetch_array($result);
					 }
					 ?>

                <tr>

                    <td width="117" height="22" valign="middle" class="arial14">
						<input name="numeroCliente" type="hidden" value="<?= $_SESSION['numeroCliente'] ?>" />
						<input name="volver" type="hidden" value="oficina" />
                      <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 196'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?></td>
                <td width="331" valign="top" class="arial14"><input name="CodigoCliente" type="text" class="textfieldLecturaNum" readonly="true"  value="<?php echo($row['CodigoCliente']);?>" id="CodigoCliente" size="10" maxlength="10" />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 118'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
&nbsp;
<input name="SuministroDNI" type="text" readonly="true" class="textfieldLectura" id="SuministroDNI" value="<?php echo($row['DNI']);?>" size="12" maxlength="15" /></td>
                </tr>

                <tr>
                    <td height="22" valign="middle" class="arial14">CUPS</td>
                <td valign="top"><input name="CUPS" type="text" class="textfieldLectura" readonly="true" id="CUPS" value="<?php echo(str_replace('"','&quot;',$row['CUPS']));?>" size="25" maxlength="25" /></td>
                </tr>

                <tr>
                    <td height="22" valign="middle" class="arial14">
                      <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 119'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
&nbsp;&nbsp;</td>
                <td valign="top"><input name="SuministroNombre" type="text" class="textfieldLectura" readonly="true" id="SuministroNombre" value="<?php echo(str_replace('"','&quot;',$row['TitularNombre']));?>" size="50" maxlength="50" /></td>
                </tr>
                <tr>
                    <td height="20" valign="middle" class="arial14">
                      <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 121'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?></td>
                <td valign="top"><input name="SuministroCiudad" readonly="true" type="text" class="textfieldLectura" id="SuministroCiudad" value="<?php echo(str_replace('"','&quot;',$row['SuministroCiudad']));?>" size="50" maxlength="50" /></td>
                </tr>
                <tr>
                  <td height="24" valign="middle" class="arial14"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 122'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?></td>
                  <td valign="top"><input name="SuministroCP2" type="text" readonly="true" class="textfieldLecturaNum" id="SuministroCP3" value="<?php echo($row['SuministroCP']);?>" size="6" maxlength="10" /></td>
                </tr>
                <tr>
                    <td height="22" valign="middle" class="arial14">
                      <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 123'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
&nbsp;&nbsp;&nbsp;</td>
                <td valign="top"><input name="SuministroCalle" type="text" class="textfieldLectura" readonly="true" id="SuministroExtensionNombre3" value="<?php echo(str_replace('"','&quot;',$row['SuministroCalle']));?>" size="50" maxlength="50" /></td>
                </tr>
                <tr>
                    <td height="22" valign="middle" class="arial14">
                      <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 124'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                    &nbsp;</td>
                <td valign="top" class="arial14"><input name="SuministroNumero" readonly="true" type="text" class="textfieldLecturaNum" id="SuministroNumero" value="<?php echo($row['SuministroNumero']);?>" size="5" maxlength="4" />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 125'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
&nbsp;
<input name="SuministroExtension" readonly="true" type="text" class="textfieldLectura" id="SuministroExtension" value="<?php echo(str_replace('"','&quot;',$row['SuministroExtension']));?>" size="20" maxlength="20" /></td>
                </tr>
              </table>
              </fieldset></td>
          </tr>
          <tr>
            <td height="18"></td>
            <td></td>
          </tr>
          <tr>
            <td height="18" valign="top" class="arial12black">
                <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 182'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
              .</td>
          <td></td>
          </tr>
          <tr>
            <td height="16"></td>
            <td></td>
          </tr>
          <tr>
            <td height="22" valign="top" class="arial14">
                <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 183'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                *&nbsp;


                <input name="FechaIncidencia" type="text" class="textfield" id="FechaIncidencia"  value="<?=$fecha?>" size="12" maxlength="10" />
&nbsp;&nbsp;
                <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 215'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                *&nbsp;
                <input name="HoraIncidencia" type="text" class="textfieldCentrado" id="HoraIncidencia"  value="<?=$hora?>" size="2" maxlength="2" />
                h.
                <input name="MinutosIncidencia" type="text" class="textfieldCentrado" id="MinutosIncidencia"  value="<?=$minutos?>" size="2" maxlength="2" />
              min. </td>
          <td></td>
          </tr>
          <tr>
            <td height="16"></td>
            <td></td>
          </tr>

            <tr>
                <td width="451" class="nota_campos_obligatorios"><span class="nota_campos_obligatorios"><?=$nota_campos_obligatorios?></span><br /><br /></td>
            </tr>

          <tr class="arial12">
            <td colspan="2" class="arial14"><fieldset >
                <legend class="arialblack14">
                <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 184'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                </legend>
                <table width="500" align="center">
                  <!--DWLayoutTable-->
                  <tr>
                    <td width="91" height="22" valign="top" class="arial14">
                      <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 134'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                    *&nbsp;</td>
                  <td width="337" valign="top"><input name="NombreInformante" type="text" class="textfield" id="NombreInformante" size="50" maxlength="50" value="<?=$nombre_informante?>" /></td>
                  </tr>
                  <tr>
                    <td height="24" valign="top" class="arial14">
                      <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto =216'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                    *&nbsp;</td>
                  <td valign="top"><input name="DireccionInformante" type="text" class="textfield" id="DireccionInformante" size="50" maxlength="50" value="<?=$direccion_informante?>" /></td>
                  </tr>
                  <tr>
                    <td height="24" valign="top" class="arial14">
                      <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 217'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                    *                      </td>
                  <td valign="top"><input name="PoblacionInformante" type="text" class="textfield" id="PoblacionInformante" size="50" maxlength="50" value="<?=$poblacion_informante?>"/></td>
                  </tr>
                  <tr>
                    <td height="25" valign="middle" class="arial14">
                      <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 345'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                    *&nbsp;</td>
                  <td valign="top" class="arial14"><input name="Telefono1" type="text" class="textfield" id="Telefono1" size="15" maxlength="15" value="<?=$telefono_informante?>"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 583'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                    <input name="Telefono2"  type="text" class="textfield" id="Telefono2" size="15" maxlength="15" value="<?=$movil_informante?>" />
&nbsp;</td>
                  </tr>
                  <tr>
                    <td height="24" valign="middle" class="arial14"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 128'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
&nbsp;</td>
                  <td valign="top"><input name="FaxInformante"  type="text"  class="textfield" id="FaxInformante" size="15" maxlength="15" value="<?=$fax_informante?>"/></td>
                  </tr>
                  <tr>
                    <td height="24" valign="top" class="arial14">
                    <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 129'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?></td>
                  <td valign="top"><input name="EmailInformante" type="text" class="textfield" id="EmailInformante" size="50" maxlength="50" value="<?=$email_informante?>"/></td>
                  </tr>
                </table>
              </fieldset></td>
          </tr>
          <tr >
            <td height="10" ></td>
            <td ></td>
          </tr>
          <tr class="arial12">
            <td colspan="2" class="arial14"><fieldset >
                <legend class="arialblack14">
                <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 185'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                </legend>
                <table width="500" align="center">
                  <!--DWLayoutTable-->
                  <tr>
                    <td width="93" height="21" valign="top" class="arial14">
                      <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 216'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                    *&nbsp;</td>
                  <td width="335" valign="top"><input name="DireccionIncidencia" type="text" class="textfield" id="DireccionIncidencia" size="50" maxlength="50" value="<?=$direccion_incidencia?>"/></td>
                  </tr>
                  <tr>
                    <td height="22" valign="top" class="arial14">
                      <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto =217'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                    *                      </td>
                  <td valign="top"><input name="PoblacionIncidencia" type="text" class="textfield" id="PoblacionIncidencia" size="50" maxlength="50" value="<?=$poblacion_incidencia?>"/></td>
                  </tr>
                </table>
              </fieldset></td>
          </tr>

          <tr class="arial12">
            <td height="10" class="arial14"></td>
            <td class="arial14"></td>
          </tr>
          <tr class="arial12">
            <td colspan="2" class="arial14"><fieldset>
                <legend class="arialblack14">
                <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 186'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                *</legend>
                <table width="500" align="center">
                <tr>
                  <td width="432" align="center" class="arial14"> <textarea name="Descripcion" cols="55" rows="5" id="Descripcion" class="arial14"><?=$descripcion_incidencia?></textarea>                  </td>
                </tr>
              </table>
              </fieldset></td>
          </tr>
          <tr class="arial12">
            <td height="16"></td>
            <td class="arial14"></td>
          </tr>
          <tr class="arial12">
            <td height="24" align="center" valign="top" class="arial14"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 153'); $rowTexto = mysql_fetch_array($resultTexto);?>
              <input type="submit" name="Submit" value="<?php echo($rowTexto['Texto']);?>" />            </td>
          <td class="arial14"></td>
          </tr>
        </table>
	    </form>
          <table width="458" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td>&nbsp;</td>
            </tr>
          </table>

























<!--VERSION VIEJA-->











		</div><!--<div class="contenido_seccion_oficina">-->


        <div class="limpiar"></div>

<!--Por ultimo aparecera el pie de la web-->
<?php
	include("../includes/pie.php");

//Cuando se pulse el boton aceptar del formulario se ejecutaran las consecuencias de las verificaciones
	if(isset($_POST["Submit"]))
	{
//Ahora dependiendo del resultado de las verificaciones, se mostrara el mensaje de error oportuno o se realizara el alta y envio de la incidencia
		switch($error)
		{
			case 0:
//Pasadas todas las verificaciones si el proceso ha sido correcto realizaremos las siguientes acciones

//Almacenamos en la tabla de incidencias la informacion facilitada por el usuario
				$insertar_incidencia="INSERT INTO Incidencias (
				`NumeroCliente`,`CUPS`,`FechaIncidencia`,`HoraIncidencia`,`NombreInformante`,`DireccionInformante`,`PoblacionInformante`,
				`Telefono1`,`Telefono2`,`FaxInformante`,`EmailInformante`,`DireccionIncidencia`,`PoblacionIncidencia`,`Descripcion`,
				`FechaRegistro`,`HoraRegistro`)
				VALUES ('".$_POST["CodigoCliente"]."','".$_POST["CUPS"]."','".fecha_mysql($fecha)."','".$hora.":".$minutos."','".$nombre_informante."','".$direccion_informante."','".$poblacion_informante."','".$telefono_informante."','".$movil_informante."','".$fax_informante."','".$email_informante."','".$direccion_incidencia."','".$poblacion_incidencia."','".$descripcion_incidencia."','".date("Y-m-d")."','".date("Y-m-d H:i:s")."');";

//Ejecutamos la insercion del registro
				$ejecutar_insercion_incidencia=mysql_query($insertar_incidencia);

//Ahora enviamos por correo electronico a todas las direcciones almacenadas en la base de datos la incidencia
 				//sendmail
				//include("../phpmailer/envio_mail_incidencia.php");

				//phpmailer
				//include("../phpmailer/enviar_incidencia.php");

				//Reiniciamos las variables, para que cuando se haga la redireccion el formulario aparezca vacio de nuevo






					$asunto=$consulta2." Web ".$nombre_empresa." - Incidencia";

//Preparamos la cabecera para el envío en formato HTML
				$headers = "MIME-Version: 1.0\r\n";
				$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";

//Establecemos la direccion del remitente
				$headers .= "From: Web ".$nombre_empresa." <".$mail_empresa.">\r\n";

//Esta variable almacenara el contenido del correo electrónico que reciben en la empresa cliente avisandoles de la solicitud de la contraseña
				$cuerpo="Incidencia desde la Web: <strong>";

				$cuerpo.=$fecha." ";
			    $cuerpo.=$hora.":";
				$cuerpo.=$minutos."</strong><br /><br />";
//Primero escribimos todos los campos de contacto del usuario
				$cuerpo.="<strong>Nombre:</strong> ".$nombre_informante."<br /><br />";
				$cuerpo.="<strong>Telefono:</strong> ".$telefono_informante."<br /><br />";

//Los campos no obligatorios solo apareceran si el usuario ha escrito algo en ellos
				if($direccion_informante!="")
				{
					$cuerpo.="<strong>Dirección Informante:</strong> ".$direccion_informante."<br /><br />";
				}//if($email!="")

				if($poblacion_informante!="")
				{
					$cuerpo.="<strong>Poblacion:</strong> ".$poblacion_informante."<br /><br />";
				}//if($email!="")


				if($email_informante!="")
				{
					$cuerpo.="<strong>Email:</strong> ".$email_informante."<br /><br />";
				}//if($email!="")

					if($movil_informante!="")
				{
					$cuerpo.="<strong>Movil:</strong> ".$movil_informante."<br /><br />";
				}//if($email!="")

				if($fax_informante!="")
				{
					$cuerpo.="<strong>Fax:</strong> ".$fax_informante."<br /><br />";
				}//if($fax!="")

//Despues de los datos de contacto se añadira lo referente a la sugerencia

				if($direccion_incidencia!="")
				{
					$cuerpo.="<strong>Direccion Incidencia:</strong> ".$direccion_incidencia."<br /><br />";
				}//if($tema!="")

				if($poblacion_incidencia!="")
				{
					$cuerpo.="<strong>Poblacion Incidencia:</strong> ".$poblacion_incidencia."<br /><br />";
				}//if($otras!="")

//Por ultimo se añade el comentario del usuario
				$cuerpo.="<strong>Descripción Incidencia:</strong> ".$descripcion_incidencia."<br /><br />";

//Consultamos en la tabla DireccionesEmail las direcciones a las que la empresa cliente quiere enviar los correos
				$rs_destinatarios = seleccion("DireccionesEmail","IdEmail",0,$ruta_sevidor_bd,$usuario_bd,$contrasena_bd,$nombre_bd);

//Se envian mails a todas las direcciones que haya dadas de alta en la tabla. Aunque la direccion no exista no se produce un error, es decir que el usuario no será consciente del error
				while($destinatario=mysql_fetch_array($rs_destinatarios))
				{
					mail($destinatario["Email"],$asunto,$cuerpo,$headers);
				}//while($destinatario=mysql_fetch_array($rs_destinatarios))

//Reiniciamos las variables, para que cuando se haga la redireccion el formulario aparezca vacio de nuevo
				$nombre_informante="";
				$direccion_informante="";
				$poblacion_informante="";
				$telefono_informante="";
				$movil_informante="";
				$fax_informante="";
				$email_informante="";

//Se recogen los datos de la incidencia
				$direccion_incidencia="";
				$poblacion_incidencia="";
				$descripcion_incidencia="";



				MsgBox($solicitud_ok);

				redirigir("oficina_incidencias.php?id=".$_SESSION["idioma"]);
			break;

//Error por no rellenar datos obligatorios
			case 1:
				MsgBox($error_rellenar);
			break;

//Error en la fecha
			case 2:
				MsgBox($error_fecha);
			break;

//Error en la hora
			case 3:
				MsgBox($error_hora);
			break;

//Error los telefonos o el fax
			case 4:
				MsgBox($error_telefonos);
			break;

//Error en el correo electronico
			case 5:
				MsgBox($error_mail);
			break;

//Error fecha posterior a la fecha actual
			case 6:
				MsgBox($error_fecha_posterior);
			break;
		}//switch($error)
	}//if(isset($_POST["Submit"]))
}//else ($_SESSION['usuario'])
?>
    </div><!--<div id="central_oficina"-->
</body>
</html>



