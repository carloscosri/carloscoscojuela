<?php

/* NOTA:

 COMO NO SE CIERRA LA CONEXION TRAS CADA CONEXION CON LA BASE DE DATOS, LAS CONSULTAS DEJAN DE FUNCIONAR SI SE ESTABLECE COMO UNICA CONEXION
 LA QUE UTILIZA EL RESTO DE LA WEB, POR LO TANTO HASTA QUE SE HAGA LA MIGRACION COMPLETA DE LOS TEXTOS, COEXISTIRAN LAS DOS CONEXIONES, YA QUE
 SE HA AÑADIDO UNA LLAMADA A LA CONEXION BUENA EN ESTE ARCHIVO.
*/

include("_conexion.php");

//Se incluye el archivo que contiene las verificaciones de datos
include("../includes/verificaciones.php");

//Se incluye el archivo con los textos especifico de esta seccion en el idioma oportuno
include("../idiomas/".$_SESSION["idioma"]."/oficina_sugerencias.php");

//Cuando se pulse el boton aceptar del formulario, recogeremos el valor del boton
if (isset($_POST['Submit']))
{
	$comentario="";
	$tema="";
	$otras="";
	$nombre_usuario_formulario="";
	$email="";
	$telefono="";
	$fax="";
	$observaciones="";

//Recogemos los valores de los campos de texto del formulario escritas por el usuario
	$comentario=$_POST['comentario'];
	$tema=$_POST['tema'];
	$otras=$_POST['otras'];
	$nombre_usuario_formulario=$_POST['nombre'];
	$email=$_POST['email'];
	$telefono=$_POST['telefono'];
	$fax=$_POST['fax'];
	$observaciones=$_POST['observaciones'];






//Como en la fucnion "comprobar()" solo se encuentran las verificaciones de si los campos están o no vacio, a continuacion haremos las demas
//Con esta variable se comprobara si hay algun tipo de error en el formulario
	$error=0;

	if($nombre_usuario_formulario=="" or $telefono=="" or $observaciones=="")
	{
		$error=1;
	}//if($nombre_usuario_formulario=="" or $telefono=="" or $observaciones=="")


//Al ser obligatorio, comprobamos en primer lugar el telefono
	if($error==0 and $telefono!="" and solo_numero($telefono))
	{
		$error=2;
	}//if($telefono!="" and solo_numero($telefono))

//En el caso en que no se haya producido un error en comprobaciones anteriores y se haya escrito algo en el campo del fax, lo comprobaremos si el
//usuario ha escrito algo en el (no es obligatorio)
	if($error==0 and $fax!="" and solo_numero($fax))
	{
		$error=2;
	}//if($error==0 and $fax!="" and solo_numero($fax))

//En el caso en que no se haya producido un error en comprobaciones anteriores y se haya escrito algo en el campo del email, lo comprobaremos si el
//usuario ha escrito algo en el (no es obligatorio)
	if($error==0 and $email!="" and !comprobar_email($email))
	{
		$error=3;
	}//if($error==0 and $email!="" and !comprobar_email($email))
}//if (isset($_POST['Submit']))

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<title><?=$titulo_web;?></title>

<!--Se añaden las hojas de estilos genericas que utiliza esta seccion-->
<link href="../css/estilos.css" rel="stylesheet" type="text/css" />
<link href="../css/secciones.css" rel="stylesheet" type="text/css" />
<link href="css/oficina.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_vieja.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_sugerencias.css" rel="stylesheet" type="text/css" />

<!--Ahora se añaden las reglas para Internet Explorer. Asi corregiremos las diferencias de visualizacion entre navegadores
Es importante añadirlo despues de las reglas principales, para que se sobreescriban las correcciones pertinentes. En este caso las diferencias
solo son para internet explorer 7-->
<script language=javascript type=text/javascript>
function stopRKey(evt) {
var evt = (evt) ? evt : ((event) ? event : null);
var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
if ((evt.keyCode == 13) && (node.type=="text")) {return false;}
}
document.onkeypress = stopRKey;
</script>

<!--[if IE 7]>
	<link href="../css/estilos_ie7.css" rel="stylesheet" type="text/css" />
	<link href="../css/secciones_ie7.css" rel="stylesheet" type="text/css" />
<![endif]-->

</head>

<body>
<!--*************************************************Web realizada por ****************************************-->
	<div id="central_oficina">

<?php
//Se comprueba que el usuario se haya identificado, si no es asi se da un aviso y redirigimos a la seccion de identificacion
if ($_SESSION['usuario']=="")
{
	MsgBox($error_identificacion);
	redirigir("oficina_index.php?id=".$_SESSION["idioma"]);

}//if ($_SESSION['usuario'])

//Si el usuario se ha identificado la seccion se ejecutara con normalidad
else
{
//Añadimos los fragmentos comunes, la cabecera, el menu, y el submenu
		include("oficina_fragmentos_comunes.php");
?>
		<div class="contenido_seccion_oficina_sugerencias">

<!--VERSION VIEJA-->



		 <table width="575" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td>
			  <form action="oficina_sugerencias.php?id=<?=$_SESSION["idioma"]?>" method="post" name="sugerencia" id="sugerencia">
			  <table width="650" border="0" cellspacing="0" cellpadding="0">
                  <?php $result=mysql_query("SELECT * FROM ".$_SESSION['tablaIdioma']." WHERE idTexto=114");
				  mysql_query("SET NAMES 'utf8'");
				  $row = mysql_fetch_array($result);?>
                  <tr align="center">

                  <td height="30" class="arialblack18Titulo" style=" background-image:url(imagenes/MarcoAlta.gif);background-repeat:no-repeat"><?php echo($row[1]);?></td>
                  </tr>
                  <tr>
                  <td width="650" height="136"valign="middle"><div align="justify" class="arial12Importanteb" style="border: 1px solid rgb(201, 43, 43); padding: 5px; background-color: rgb(245, 220, 220); text-decoration: none;">
					<!-- CAMBIAR EN LA BBDD 669 (Nombre Empresa) y 671 (Empresa + Dirección + ',' al final) -->
					<?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 668'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
					<?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 669'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
					<?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 670'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
					<?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 671'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
					<?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 672'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
					<?php $row = mysql_fetch_array($result); echo($row[1]);?></div></td>
                  </tr>


                  <tr class="arial12">
                    <td class="arial14"> <fieldset>
                    <legend class="arialblack14">
                    <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 337'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                    </legend>
                    <table width="500" align="center">

                        <tr class="arial14">
                          <td width="461" align="center"><input name="volver" type="hidden" value="oficina" /> <label>
                            <input type="radio" name="comentario" value="Problema" />
                            </label>
                          <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 338'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                          <label>
                            <input name="comentario" type="radio" value="Consulta" checked="checked" />
                            </label>
                          <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 339'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                          <label>
                            <input type="radio" name="comentario" value="Sugerencia" />
                            </label>
                          <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 340'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                          <label>
                            <input type="radio" name="comentario" value="Ruego" />
                            </label>
                          <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 341'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                        </td>
                        </tr>
                      </table>
                      </fieldset></td>
                  </tr>
                  <tr class="arial12">
                    <td height="10" class="arial14"> </td>
                  </tr>
                  <tr class="arial12">
                    <td class="arial14"><fieldset>
                    <legend class="arialblack14">
                    <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 342'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                    </legend>
                    <table width="500" border="0" align="center">

                        <tr>
                          <td width="550" height="26" align="center" valign="middle" class="arial14"> <select name="tema" id="tema">
						  <option selected="selected"></option>
                            <option value="<?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto =584'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto =584'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?></option>
                            <option value="<?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto =585'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto =585'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?></option>
                            <option value="<?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto =586'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto =586'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?></option>
							 <option value="<?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto =587'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto =587'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?></option>
                            <option value="<?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto =588'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto =588'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?></option>
                            <option value="<?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto =589'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto =589'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?></option>
                            <option value="<?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto =343'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto =343'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?></option>
                          </select>
                            &nbsp;
                            <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto =343'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                            <input name="otras" type="text" class="textfield" id="otras" size="40" maxlength="50" value="<?=$otras?>" /></td>

                        </tr>
                      </table>
                    </fieldset></td>
                  </tr>
                  <tr class="arial12">
                    <td height="10" class="arial14"></td>
                  </tr>
                  <tr class="arial12">
                    <td class="arial14"> <fieldset>
                    <legend class="arialblack14">
                    <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 344'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                    </legend>
                    <table width="500" align="center">
                      <tr>

                          <td width="98" height="22" class="arial14">
                            <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 134'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                            * </td>
                          <td height="22" colspan="3" class="arial14"> <input name="nombre" type="text" class="textfield" id="nombre" size="40" maxlength="50" value="<?=$nombre_usuario_formulario?>" /></td>
                        </tr>
                        <tr>

                        <td width="98" class="arial14">
                          <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 129'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>                        </td>
                          <td colspan="3" class="arial14"> <input name="email" type="text" class="textfield" id="email" size="40" maxlength="50" value="<?=$email?>" />
                          </td>
                        </tr>
                        <tr>

                          <td class="arial14">
                            <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 345'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                            * </td>
                          <td width="85" class="arial14"><input name="telefono" type="text" class="textfield" id="telefono" onKeyPress="return LP_data(event)" value="<?=$telefono?>" size="13" maxlength="15" /></td>

                        <td width="44" align="left" class="arial14">
                          <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 128'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>                        </td>
                          <td width="115" align="left" class="arial14"><input name="fax" type="text" class="textfield" id="fax" value="<?=$fax?>" size="13" maxlength="15" /></td>
                        </tr>
                      </table>
                      </fieldset></td>
                  </tr>
                  <tr class="arial12">
                    <td height="10" class="arial14"></td>
                  </tr>
                  <tr class="arial12">
                    <td class="arial14"><fieldset>
                      <legend class="arialblack14">
                      <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto =346'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                      * </legend>
                    <table width="469" align="center">
                        <tr>
                          <td width="461" align="center" class="arial12"> <textarea name="observaciones" cols="55" rows="5" id="observaciones" class="arial14"><?=$observaciones?></textarea>
                          </td>
                        </tr>
                      </table>
                      </fieldset></td>
                  </tr>

                  <tr >
	                  <td height="10"></td>
                  </tr>

                  <tr class="arial12">
				  <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 153'); $rowTexto = mysql_fetch_array($resultTexto);?>

                    <td align="center" class="arial14"> <input type="submit" name="Submit" value="<?php echo($rowTexto['Texto']);?>" />
                  </td>
                  </tr>

	              <tr >
	                  <td height="10"></td>
                  </tr>

                </table></form></td>
            </tr>
          </table>

<!--VERSION VIEJA-->



		</div><!--<div class="contenido_seccion_oficina_sugerencias">-->


        <div class="limpiar"></div>

<!--Por ultimo aparecera el pie de la web-->
<?php
//	include($_SESSION["directorio_raiz"]."includes/pie.php");
	include("../includes/pie.php");

//Cuando se pulse el boton aceptar del formulario se ejecutaran las consecuencias de las verificaciones
	if (isset($_POST['Submit']))
	{
//Mostraremos el mensaje correspondiente dependiendo de si el proceso ha ido bien o se ha encontrado algun problema
		switch($error)
		{
//Si todo ha ido bien se informara al usuario
			case 0:



//Establecemos los parametros de envio del correo electronico, en este caso el destinatario no se define, ya que lo leeremos de la base de datos
				$asunto=$consulta2." Web ".$nombre_empresa." - Sugerencia";

//Preparamos la cabecera para el envío en formato HTML
				$headers = "MIME-Version: 1.0\r\n";
				$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";

//Establecemos la direccion del remitente
				$headers .= "From: Web ".$nombre_empresa." <".$mail_empresa.">\r\n";

//Esta variable almacenara el contenido del correo electrónico que reciben en la empresa cliente avisandoles de la solicitud de la contraseña
				$cuerpo="Sugerencia desde la Web:<br /><br />";

//Primero escribimos todos los campos de contacto del usuario
				$cuerpo.="<strong>Nombre:</strong> ".$nombre_usuario_formulario."<br /><br />";
				$cuerpo.="<strong>Telefono:</strong> ".$telefono."<br /><br />";

//Los campos no obligatorios solo apareceran si el usuario ha escrito algo en ellos
				if($email!="")
				{
					$cuerpo.="<strong>Email:</strong> ".$email."<br /><br />";
				}//if($email!="")

				if($fax!="")
				{
					$cuerpo.="<strong>Fax:</strong> ".$fax."<br /><br />";
				}//if($fax!="")

//Despues de los datos de contacto se añadira lo referente a la sugerencia
				$cuerpo.="<strong>Tipo de comentario:</strong> ".$comentario."<br /><br />";

				if($tema!="")
				{
					$cuerpo.="<strong>Tema:</strong> ".$tema."<br /><br />";
				}//if($tema!="")

				if($otras!="")
				{
					$cuerpo.="<strong>Otros temas:</strong> ".$otras."<br /><br />";
				}//if($otras!="")

//Por ultimo se añade el comentario del usuario
				$cuerpo.="<strong>Observaciones:</strong> ".$observaciones."<br /><br />";

//Consultamos en la tabla DireccionesEmail las direcciones a las que la empresa cliente quiere enviar los correos
				$rs_destinatarios = seleccion("DireccionesEmail","IdEmail",0,$ruta_sevidor_bd,$usuario_bd,$contrasena_bd,$nombre_bd);

//Se envian mails a todas las direcciones que haya dadas de alta en la tabla. Aunque la direccion no exista no se produce un error, es decir que el usuario no será consciente del error
				while($destinatario=mysql_fetch_array($rs_destinatarios))
				{
					mail($destinatario["Email"],$asunto,$cuerpo,$headers);
				}//while($destinatario=mysql_fetch_array($rs_destinatarios))

//Reiniciamos las variables, para que cuando se haga la redireccion el formulario aparezca vacio de nuevo
				$comentario="";
				$tema="";
				$otras="";
				$nombre_usuario_formulario="";
				$email="";
				$telefono="";
				$fax="";
				$observaciones="";

//Se informa al usuario del exito del envio de los datos
				MsgBox($sugerencia_ok);





			//Ahora enviamos por correo electronico a todas las direcciones almacenadas en la base de datos
 				//sendmail
				//include("../phpmailer/envio_mail_sugerencia.php");

				//phpmailer
				//include("../phpmailer/enviar_sugerencia.php");


//Reiniciamos las variables, para que cuando se haga la redireccion el formulario aparezca vacio de nuevo


				redirigir("oficina_sugerencias.php?id=".$_SESSION["idioma"]);
			break;

//Error por no rellenar los campos
			case 1:
				MsgBox($error_rellenar);
			break;
//Error por telefono o fax
			case 2:
				MsgBox($error_telefono);
			break;

//Error por email
			case 3:
				MsgBox($error_email);
			break;

		}//switch($error)
	}//if (isset($_POST['Submit']))
}//else ($_SESSION['usuario'])
?>
    </div><!--<div id="central_oficina"-->
</body>
</html>
