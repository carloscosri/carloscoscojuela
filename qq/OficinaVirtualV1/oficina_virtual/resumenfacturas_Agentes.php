<?php
session_start();
include('_conexion.php');
require('fpdf/rotation.php');
header("Content-Type: text/html; charset=UTF-8 ");
class PDF extends PDF_Rotate
{
	function Header()
	{
		$this->SetFont('Arial','B',15);
		$this->Ln(20);
	}
	
	function Footer()
	{
		$this->SetY(-15); 
		$this->SetFont('Arial','I',6); 
		$this->Ln(3);
	}
	
	function SeccionTitle($x, $y, $width, $height, $label)
	{
		$this->SetY($y);
		$this->SetX($x);
		$this->SetFont('Arial','B',8); 
		$this->SetFillColor(190,190,190); 
		$this->Cell($width,$height,$label,1,1,'C',true);		
		$this->Ln(4);
	}

	function SeccionBody($x, $y, $width, $text, $height_cont)
	{
		$this->SetY($y+5);
		$this->SetX($x);
		$this->SetFont('Arial','B',8);
		$this->SetFillColor(220,220,220);
		$this->MultiCell($width,$height_cont,$text,1);
		$this->Ln(); 
	}
	
	function SeccionBodyBorder($x, $y, $width, $text, $height_cont,$r,$g,$b)
	{
		$this->SetY($y+5);
		$this->SetX($x);
		$this->SetFont('Arial','B',8);
		$this->SetDrawColor($r, $g, $b);
		$this->MultiCell($width,$height_cont,$text,1);
		$this->Ln(); 
	}
	
	function SeccionBodyBG($x, $y, $width, $text, $height_cont,$r,$g,$b)
	{
		$this->SetY($y+5);
		$this->SetX($x);
		$this->SetFont('Arial','B',8);
		$this->MultiCell($width,$height_cont,$text);
		$this->SetFillColor($r,$g,$b);
		$this->Ln(); 
	}

	function PrintSeccion($x, $y, $width, $height, $title, $text, $height_cont)
	{
		$this->SeccionTitle($x, $y, $width, $height, $title);
		$this->SeccionBody($x, $y, $width, $text, $height_cont);
	}
	
	function PrintSeccion2($x, $y, $width, $height, $title, $text, $height_cont)
	{
		$this->SeccionTitle($x, $y, $width, $height, $title);
		$this->SetY($y+5);
		$this->SetX($x);
		$this->SetFont('Arial','',8);
		$this->SetFillColor(220,220,220);
		$this->MultiCell($width,$height_cont,$text,1);
		$this->Ln(); 
	}
	
	function CreaCelda($x, $y, $width, $label, $r, $g, $b, $fuente, $especial, $tamano){
		$this->SetY($y);
		$this->SetX($x);
		$this->SetFont($fuente, $especial, $tamano);
		$this->SetFillColor($r,$g,$b); 
		$this->Cell($width,3,$label,0,0,'L',true);
	}
	
	function CreaCelda2($x, $y, $width, $label, $r, $g, $b){
		$this->SetY($y);
		$this->SetX($x);
		$this->SetFont('Arial','B',8);
		$this->SetFillColor($r,$g,$b);
		$this->Cell($width,4,$label,0,0,'C',true);
	}
	
	function CreaCeldaCentrada($x, $y, $width, $label, $r, $g, $b){
		$this->SetY($y);
		$this->SetX($x);
		$this->SetFont('Arial','',8);
		$this->SetFillColor($r,$g,$b); 
		$this->Cell($width,3,$label,0,0,'C',true);
	}
	function CreaCeldaAlDer($x, $y, $width, $label, $r, $g, $b){
		$this->SetY($y);
		$this->SetX($x);
		$this->SetFont('Arial','',8);
		$this->SetFillColor($r,$g,$b);
		$this->Cell($width,3,$label,0,0,'R',true);
	}
	
	function CreaCeldaAlDer2($x, $y, $width, $label, $r, $g, $b){
		$this->SetY($y);
		$this->SetX($x);
		$this->SetFont('Arial','',6);
		$this->SetFillColor($r,$g,$b);
		$this->Cell($width,3,$label,0,0,'R',true);
	}
	
	function CreaCeldaAlDer3($x, $y, $width, $label, $r, $g, $b, $fuente, $especial, $tamano){
		$this->SetY($y);
		$this->SetX($x);
		$this->SetFont($fuente, $especial, $tamano);
		$this->SetFillColor($r,$g,$b); 
		$this->Cell($width,3,$label,0,0,'R',true);
	}
	
	function RotatedText($x, $y, $txt, $angle)
	{
		//Text rotated around its origin
		$this->Rotate($angle, $x, $y);
		$this->SetFont('Arial','',7);
		$this->Text($x, $y, $txt);
		$this->Rotate(0);
	}
}

$nif=$_GET['nif'];
$fechaInicio=$_GET['FechaInicial'];
$fechaFin=$_GET['FechaFinal'];
$fechafactura = $fecha." 00:00:00";
 
include("../includes/sesion.php");

$link = mysql_connect($ruta_sevidor_bd, $usuario_bd, $contrasena_bd);
if (!$link) {die('Could not connect: ' . mysql_error());}

$db_selected = mysql_select_db($nombre_bd, $link);
if (!$db_selected){die ('Can\'t use foo : ' . mysql_error());}

$consulta_facturas="SELECT * FROM FacturasImprimir where Fecha > '".$fechaInicio."' and Fecha < '".$fechaFin."' and CodAbonado IN (Select Usuario from DatosRegistrados where Agente = (SELECT Numero from Comisionistas where DNI = '".$nif."'))";
$resultados_facturas = mysql_query($consulta_facturas);	


$pdf=new PDF();
$pdf->SetDisplayMode('real');

while($registro_factura = mysql_fetch_array($resultados_facturas)){
$meses = array("ENERO", "FEBRERO", "MARZO", "ABRIL", "MAYO", "JUNIO", "JULIO", "AGOSTO", "SEPTIEMBRE", "OCTUBRE", "NOVIEMBRE", "DICIEMBRE");

$fecha_secciones = explode('-',$fechafactura);
$mes=$registro_factura["NumeroPeriodo"];
	
$meses_factura = array();
$anios = array();
	
$anio = substr($registro_factura["PeriodoTexto"], -4);

for ($i=0; $i<12; $i++){
	$mes = $mes-1;
		
	if ($mes == 0){
		$mes = 12;
		$anio = $anio-1;
	}
	$meses_factura[$i] = $mes;
	$anios[$i] = $anio;
}
	
// Datos de la factura	
$fecha = explode(' ',$registro_factura["Fecha"]);
$periodo_factura = $registro_factura["periodo"];
$num_factura = $registro_factura["NumFactura"];
$importe_total=number_format($registro_factura["importe"],2,",",".");
$cliente = $registro_factura['CodAbonadoGrafico'];
$poliza = $registro_factura['CodAbonadoGrafico'];

// Datos de titular
$nombre_titular = $registro_factura['NombreAbonado'];
$direccion_titular = $registro_factura['Ndomicilio1'];
$direccion_titular2 = $registro_factura['CodPostal1']." - ".$registro_factura['texto-ciudad'];
$dni_titular = $registro_factura['Nif'];
$cod_barras = $registro_factura['CodigoBarras'];
$titularCP=$registro_factura['CodPostal1'];
$titularPob=$registro_factura['texto-ciudad1'];

// Datos de suministro
if ($registro_factura['Nombre2Abonado']!="" or !is_null($registro_factura['Nombre2Abonado'])){
	$nombre_suministro = $registro_factura['Nombre2Abonado'];
} else {
	$nombre_suministro = $registro_factura['NombreAbonado']	;
}
if ($registro_factura['Ndomicilio2']!="" or !is_null($registro_factura['Ndomicilio2'])){
	$direccion_suministro = $registro_factura['Nombrecalle'].' '.$registro_factura['numdomicilio'].' '.$registro_factura['extnumerodomici1'];
} else {
	$direccion_suministro = $registro_factura['Ndomicilio1'];
}
$poblacion_suministro = $registro_factura['texto-ciudad'];
$suministro_CodPos=$registro_factura['CodPostal2'];
$suministro_ciudad = $registro_factura['texto-ciudad'];
$cups = $registro_factura['CUPS'];
$cnae = $registro_factura['cnae'];
$potencia_contratada=$registro_factura['pckw_ab'];
if($registro_factura['PotContP1']==0){$p1="";} else {$p1="P1: ".$registro_factura['PotContP1'];}
if($registro_factura['PotContP2']==0){$p2="";} else {$p2="P2: ".$registro_factura['PotContP2'];}
if($registro_factura['PotContP3']==0){$p3="";} else {$p3="P3: ".$registro_factura['PotContP3'];}
if($registro_factura['PotContP4']==0){$p4="";} else {$p4="P4: ".$registro_factura['PotContP4'];}
if($registro_factura['PotContP5']==0){$p5="";} else {$p5="P5: ".$registro_factura['PotContP5'];}
if($registro_factura['PotContP6']==0){$p6="";} else {$p6="P6: ".$registro_factura['PotContP6'];}
$tarifa = $registro_factura['nombretarifa'];
if($registro_factura['TensionSuministro']==""){ $tension = " "; } else { $tension = $registro_factura['TensionSuministro']; }
if($registro_factura['contadoractiva']==""){ $contador=" "; } else { $contador=$registro_factura['contadoractiva']; }
$ref_catastral = $registro_factura['ReferenciaCatastral'];
$discHorario = $registro_factura['textodisriminador'];

// Datos de pago
$forma_pago = $registro_factura['plazo1'];
$banco = $registro_factura['ban2-nom'];
$num_cuenta_banco = substr($registro_factura["numcuenta"],0,4);
$num_cuenta_sucursal = substr($registro_factura["numcuenta"],4,4);
$num_cuenta_digito_control = substr($registro_factura["numcuenta"],8,2);
$num_cuenta_cuenta = substr($registro_factura["numcuenta"],10,10);
$num_cuenta = $num_cuenta_banco.' - '. $num_cuenta_sucursal.' - '.$num_cuenta_digito_control.' - '.substr($num_cuenta_cuenta,1,5).'*****';
if($num_cuenta!="" and $banco!=""){$forma_pago="BANCO";}
$base_imponible = number_format($registro_factura["BaseImponible"],2,",",".");
$iva = number_format($registro_factura["CuotaIva"],2,",",".");
$total_factura = number_format($registro_factura["Total"],2,",",".");
$termino_pago = $registro_factura['plazo1'];
$plazo = $registro_factura['plazo2'];

$base1=$registro_factura['Base1'];
$base2=$registro_factura['Base2'];
$iva1=$registro_factura['Iva1'];
$iva2=$registro_factura['Iva2'];
$cuota1=$registro_factura['Cuota1'];
$cuota2=$registro_factura['Cuota2'];

// Lecturas
$fechalectura = explode(" ",$registro_factura['fechalectura']);
$fechalecturaant = explode(" ",$registro_factura['FechaLecturaAnt']);
if($registro_factura['LecturaP1Ant']==0){$lecturaP1ant=' ';}else{$lecturaP1ant=number_format($registro_factura['LecturaP1Ant'],0,",",".");}
if($registro_factura['LecturaP2Ant']==0){$lecturaP2ant=' ';}else{$lecturaP2ant=number_format($registro_factura['LecturaP2Ant'],0,",",".");}
if($registro_factura['LecturaP3Ant']==0){$lecturaP3ant=' ';}else{$lecturaP3ant=number_format($registro_factura['LecturaP3Ant'],0,",",".");}
if($registro_factura['LecturaP4Ant']==0){$lecturaP4ant=' ';}else{$lecturaP4ant=number_format($registro_factura['LecturaP4Ant'],0,",",".");}
if($registro_factura['LecturaP5Ant']==0){$lecturaP5ant=' ';}else{$lecturaP5ant=number_format($registro_factura['LecturaP5Ant'],0,",",".");}
if($registro_factura['LecturaP6Ant']==0){$lecturaP6ant=' ';}else{$lecturaP6ant=number_format($registro_factura['LecturaP6Ant'],0,",",".");}

if($registro_factura['LecturaP1Act']==0){$lecturaP1act=' ';}else{$lecturaP1act=number_format($registro_factura['LecturaP1Act'],0,",",".");}
if($registro_factura['LecturaP2Act']==0){$lecturaP2act=' ';}else{$lecturaP2act=number_format($registro_factura['LecturaP2Act'],0,",",".");}
if($registro_factura['LecturaP3Act']==0){$lecturaP3act=' ';}else{$lecturaP3act=number_format($registro_factura['LecturaP3Act'],0,",",".");}
if($registro_factura['LecturaP4Act']==0){$lecturaP4act=' ';}else{$lecturaP4act=number_format($registro_factura['LecturaP4Act'],0,",",".");}
if($registro_factura['LecturaP5Act']==0){$lecturaP5act=' ';}else{$lecturaP5act=number_format($registro_factura['LecturaP5Act'],0,",",".");}
if($registro_factura['LecturaP6Act']==0){$lecturaP6act=' ';}else{$lecturaP6act=number_format($registro_factura['LecturaP6Act'],0,",",".");}

if($registro_factura['LecturaReacP1Ant']==0){$lecturaReacP1ant=' ';}else{$lecturaReacP1ant=number_format($registro_factura['LecturaReacP1Ant'],0,",",".");}
if($registro_factura['LecturaReacP2Ant']==0){$lecturaReacP2ant=' ';}else{$lecturaReacP2ant=number_format($registro_factura['LecturaReacP2Ant'],0,",",".");}
if($registro_factura['LecturaReacP3Ant']==0){$lecturaReacP3ant=' ';}else{$lecturaReacP3ant=number_format($registro_factura['LecturaReacP3Ant'],0,",",".");}
if($registro_factura['LecturaReacP4Ant']==0){$lecturaReacP4ant=' ';}else{$lecturaReacP4ant=number_format($registro_factura['LecturaReacP4Ant'],0,",",".");}
if($registro_factura['LecturaReacP5Ant']==0){$lecturaReacP5ant=' ';}else{$lecturaReacP5ant=number_format($registro_factura['LecturaReacP5Ant'],0,",",".");}
if($registro_factura['LecturaReacP6Ant']==0){$lecturaReacP6ant=' ';}else{$lecturaReacP6ant=number_format($registro_factura['LecturaReacP6Ant'],0,",",".");}

if($registro_factura['LecturaReacP1Act']==0){$lecturaReacP1act=' ';}else{$lecturaReacP1act=number_format($registro_factura['LecturaReacP1Act'],0,",",".");}
if($registro_factura['LecturaReacP2Act']==0){$lecturaReacP2act=' ';}else{$lecturaReacP2act=number_format($registro_factura['LecturaReacP2Act'],0,",",".");}
if($registro_factura['LecturaReacP3Act']==0){$lecturaReacP3act=' ';}else{$lecturaReacP3act=number_format($registro_factura['LecturaReacP3Act'],0,",",".");}
if($registro_factura['LecturaReacP4Act']==0){$lecturaReacP4act=' ';}else{$lecturaReacP4act=number_format($registro_factura['LecturaReacP4Act'],0,",",".");}
if($registro_factura['LecturaReacP5Act']==0){$lecturaReacP5act=' ';}else{$lecturaReacP5act=number_format($registro_factura['LecturaReacP5Act'],0,",",".");}
if($registro_factura['LecturaReacP6Act']==0){$lecturaReacP6act=' ';}else{$lecturaReacP6act=number_format($registro_factura['LecturaReacP6Act'],0,",",".");}

if($registro_factura['LecturaMaxP1']==0){$lecturaMaxP1=' ';}else{$lecturaMaxP1=number_format($registro_factura['LecturaMaxP1'],2,",",".");}
if($registro_factura['LecturaMaxP2']==0){$lecturaMaxP2=' ';}else{$lecturaMaxP2=number_format($registro_factura['LecturaMaxP2'],2,",",".");}
if($registro_factura['LecturaMaxP3']==0){$lecturaMaxP3=' ';}else{$lecturaMaxP3=number_format($registro_factura['LecturaMaxP3'],2,",",".");}
if($registro_factura['LecturaMaxP4']==0){$lecturaMaxP4=' ';}else{$lecturaMaxP4=number_format($registro_factura['LecturaMaxP4'],2,",",".");}
if($registro_factura['LecturaMaxP5']==0){$lecturaMaxP5=' ';}else{$lecturaMaxP5=number_format($registro_factura['LecturaMaxP5'],2,",",".");}
if($registro_factura['LecturaMaxP6']==0){$lecturaMaxP6=' ';}else{$lecturaMaxP6=number_format($registro_factura['LecturaMaxP6'],2,",",".");}

if($registro_factura['ConsumoReacP1']==0){$ConsumoReacP1=' ';}else{$ConsumoReacP1=number_format($registro_factura['ConsumoReacP1'],0,",",".");}
if($registro_factura['ConsumoReacP2']==0){$ConsumoReacP2=' ';}else{$ConsumoReacP2=number_format($registro_factura['ConsumoReacP2'],0,",",".");}
if($registro_factura['ConsumoReacP3']==0){$ConsumoReacP3=' ';}else{$ConsumoReacP3=number_format($registro_factura['ConsumoReacP3'],0,",",".");}
if($registro_factura['ConsumoReacP4']==0){$ConsumoReacP4=' ';}else{$ConsumoReacP4=number_format($registro_factura['ConsumoReacP4'],0,",",".");}
if($registro_factura['ConsumoReacP6']==0){$ConsumoReacP5=' ';}else{$ConsumoReacP5=number_format($registro_factura['ConsumoReacP5'],0,",",".");}
if($registro_factura['ConsumoReacP5']==0){$ConsumoReacP6=' ';}else{$ConsumoReacP6=number_format($registro_factura['ConsumoReacP6'],0,",",".");}

if($registro_factura['ConsumoP1']==0){$ConsumoP1=' ';}else{$ConsumoP1=number_format($registro_factura['ConsumoP1'],0,",",".");}
if($registro_factura['ConsumoP2']==0){$ConsumoP2=' ';}else{$ConsumoP2=number_format($registro_factura['ConsumoP2'],0,",",".");}
if($registro_factura['ConsumoP3']==0){$ConsumoP3=' ';}else{$ConsumoP3=number_format($registro_factura['ConsumoP3'],0,",",".");}
if($registro_factura['ConsumoP4']==0){$ConsumoP4=' ';}else{$ConsumoP4=number_format($registro_factura['ConsumoP4'],0,",",".");}
if($registro_factura['ConsumoP6']==0){$ConsumoP5=' ';}else{$ConsumoP5=number_format($registro_factura['ConsumoP5'],0,",",".");}
if($registro_factura['ConsumoP5']==0){$ConsumoP6=' ';}else{$ConsumoP6=number_format($registro_factura['ConsumoP6'],0,",",".");}

$total_actual_reactiva = $lecturaReacP1act+$lecturaReacP2act+$lecturaReacP3act+$lecturaReacP4act+$lecturaReacP5act+$lecturaReacP6act;
$total_anterior_reactiva = $lecturaReacP1ant+$lecturaReacP2ant+$lecturaReacP3ant+$lecturaReacP4ant+$lecturaReacP5ant+$lecturaReacP6ant;
$consumo_reactiva = ($total_actual_reactiva - $total_anterior_reactiva)*1000;

$total_actual_activa = $lecturaP1act+$lecturaP2act+$lecturaP3act+$lecturaP4act+$lecturaP5act+$lecturaP6act;
$total_anterior_activa = $lecturaP1ant+$lecturaP2ant+$lecturaP3ant+$lecturaP4ant+$lecturaP5ant+$lecturaP6ant;
$consumo_activa = ($total_actual_activa - $total_anterior_activa)*1000;

$señal_actual = $registro_factura['SeñalActual'];

// Calculo de factura
$textoPotP1 = $registro_factura['TextoPotP1'];
$textoPotP2 = $registro_factura['TextoPotP2'];
$textoPotP3 = $registro_factura['TextoPotP3'];
$textoPotP4 = $registro_factura['TextoPotP4'];
$textoPotP5 = $registro_factura['TextoPotP5'];
$textoPotP6 = $registro_factura['TextoPotP6'];

$importePotP1 = number_format($registro_factura['ImportePotP1'],2,",",".");
$importePotP2 = number_format($registro_factura['ImportePotP2'],2,",",".");
$importePotP3 = number_format($registro_factura['ImportePotP3'],2,",",".");
$importePotP4 = number_format($registro_factura['ImportePotP4'],2,",",".");
$importePotP5 = number_format($registro_factura['ImportePotP5'],2,",",".");
$importePotP6 = number_format($registro_factura['ImportePotP6'],2,",",".");

$textoDtoPotP1 = $registro_factura['TextoDtoPotP1'];
$textoDtoPotP2 = $registro_factura['TextoDtoPotP2'];
$textoDtoPotP3 = $registro_factura['TextoDtoPotP3'];
$textoDtoPotP4 = $registro_factura['TextoDtoPotP4'];
$textoDtoPotP5 = $registro_factura['TextoDtoPotP5'];
$textoDtoPotP6 = $registro_factura['TextoDtoPotP6'];

$importeDtoPotP1 = number_format($registro_factura['ImporteDtoPotP1'],2,",",".");
$importeDtoPotP2 = number_format($registro_factura['ImporteDtoPotP2'],2,",",".");
$importeDtoPotP3 = number_format($registro_factura['ImporteDtoPotP3'],2,",",".");
$importeDtoPotP4 = number_format($registro_factura['ImporteDtoPotP4'],2,",",".");
$importeDtoPotP5 = number_format($registro_factura['ImporteDtoPotP5'],2,",",".");
$importeDtoPotP6 = number_format($registro_factura['ImporteDtoPotP6'],2,",",".");


$textoDtoEnerP1 = $registro_factura['TextoDtoEnerP1'];
$textoDtoEnerP2 = $registro_factura['TextoDtoEnerP2'];
$textoDtoEnerP3 = $registro_factura['TextoDtoEnerP3'];
$textoDtoEnerP4 = $registro_factura['TextoDtoEnerP4'];
$textoDtoEnerP5 = $registro_factura['TextoDtoEnerP5'];
$textoDtoEnerP6 = $registro_factura['TextoDtoEnerP6'];

$importeDtoEnerP1 = number_format($registro_factura['ImporteDtoEnerP1'],2,",",".");
$importeDtoEnerP2 = number_format($registro_factura['ImporteDtoEnerP2'],2,",",".");
$importeDtoEnerP3 = number_format($registro_factura['ImporteDtoEnerP3'],2,",",".");
$importeDtoEnerP4 = number_format($registro_factura['ImporteDtoEnerP4'],2,",",".");
$importDtoeEnerP5 = number_format($registro_factura['ImporteDtoEnerP5'],2,",",".");
$importeDtoEnerP6 = number_format($registro_factura['ImporteDtoEnerP6'],2,",",".");

$textoEnerP1 = $registro_factura['TextoEnerP1'];
$textoEnerP2 = $registro_factura['TextoEnerP2'];
$textoEnerP3 = $registro_factura['TextoEnerP3'];
$textoEnerP4 = $registro_factura['TextoEnerP4'];
$textoEnerP5 = $registro_factura['TextoEnerP5'];
$textoEnerP6 = $registro_factura['TextoEnerP6'];

$importeEnerP1 = number_format($registro_factura['ImporteEnerP1'],2,",",".");
$importeEnerP2 = number_format($registro_factura['ImporteEnerP2'],2,",",".");
$importeEnerP3 = number_format($registro_factura['ImporteEnerP3'],2,",",".");
$importeEnerP4 = number_format($registro_factura['ImporteEnerP4'],2,",",".");
$importeEnerP5 = number_format($registro_factura['ImporteEnerP5'],2,",",".");
$importeEnerP6 = number_format($registro_factura['ImporteEnerP6'],2,",",".");

$textoReacP1 = $registro_factura['TextoReacP1'];
$textoReacP2 = $registro_factura['TextoReacP2'];
$textoReacP3 = $registro_factura['TextoReacP3'];
$textoReacP4 = $registro_factura['TextoReacP4'];
$textoReacP5 = $registro_factura['TextoReacP5'];
$textoReacP6 = $registro_factura['TextoReacP6'];

$importeReacP1 = number_format($registro_factura['ImporteReacP1'],2,",",".");
$importeReacP2 = number_format($registro_factura['ImporteReacP2'],2,",",".");
$importeReacP3 = number_format($registro_factura['ImporteReacP3'],2,",",".");
$importeReacP4 = number_format($registro_factura['ImporteReacP4'],2,",",".");
$importeReacP5 = number_format($registro_factura['ImporteReacP5'],2,",",".");
$importeReacP6 = number_format($registro_factura['ImporteReacP6'],2,",",".");

if($registro_factura['ConsumoReacP1']==0){$ConsumoReacP1=' ';}else{$ConsumoReacP1=number_format($registro_factura['ConsumoReacP1'],0,",",".");}
if($registro_factura['ConsumoReacP2']==0){$ConsumoReacP2=' ';}else{$ConsumoReacP2=number_format($registro_factura['ConsumoReacP2'],0,",",".");}
if($registro_factura['ConsumoReacP3']==0){$ConsumoReacP3=' ';}else{$ConsumoReacP3=number_format($registro_factura['ConsumoReacP3'],0,",",".");}
if($registro_factura['ConsumoReacP4']==0){$ConsumoReacP4=' ';}else{$ConsumoReacP4=number_format($registro_factura['ConsumoReacP4'],0,",",".");}
if($registro_factura['ConsumoReacP6']==0){$ConsumoReacP5=' ';}else{$ConsumoReacP5=number_format($registro_factura['ConsumoReacP5'],0,",",".");}
if($registro_factura['ConsumoReacP5']==0){$ConsumoReacP6=' ';}else{$ConsumoReacP6=number_format($registro_factura['ConsumoReacP6'],0,",",".");}

if($registro_factura['ConsumoP1']==0){$ConsumoP1=' ';}else{$ConsumoP1=number_format($registro_factura['ConsumoP1'],0,",",".");}
if($registro_factura['ConsumoP2']==0){$ConsumoP2=' ';}else{$ConsumoP2=number_format($registro_factura['ConsumoP2'],0,",",".");}
if($registro_factura['ConsumoP3']==0){$ConsumoP3=' ';}else{$ConsumoP3=number_format($registro_factura['ConsumoP3'],0,",",".");}
if($registro_factura['ConsumoP4']==0){$ConsumoP4=' ';}else{$ConsumoP4=number_format($registro_factura['ConsumoP4'],0,",",".");}
if($registro_factura['ConsumoP6']==0){$ConsumoP5=' ';}else{$ConsumoP5=number_format($registro_factura['ConsumoP5'],0,",",".");}
if($registro_factura['ConsumoP5']==0){$ConsumoP6=' ';}else{$ConsumoP6=number_format($registro_factura['ConsumoP6'],0,",",".");}

$impuesto = $registro_factura['impuesto'];
$sinimpuesto = $registro_factura['simpuesto'];
$coeficiente = $registro_factura['coeficiente'];
$total_impuesto = number_format($registro_factura['totalimpuesto'],2,",",".");

$alquiler = $registro_factura['talqreac'];

$boe2012texto42011 = $registro_factura['BOE2012Texto42011'];
$boe2012texto12012 = $registro_factura['BOE2012Texto12012'];
$boe2012textototal = $registro_factura['BOE2012TextoTotal'];
$boe2012consumo42011 = $registro_factura['BOE2012Consumo42011'];
$boe2012consumo12012 = $registro_factura['BOE2012Consumo12012'];
$boe2012consumototal = $registro_factura['BOE2012ConsumoTotal'];
$boe2012importe42011 = $registro_factura['BOE2012Importe42011'];
$boe2012importe12012 = $registro_factura['BOE2012Importe12012'];
$boe2012importetotal = $registro_factura['BOE2012ImporteTotal'];
$boe2012textoabril = $registro_factura['BOE2012TextoAbril'];
$boe2012consumoabril = $registro_factura['BOE2012ConsumoAbril'];
$boe2012importeabril = $registro_factura['BOE2012ImporteAbril'];

//Historial consumos
$mes1 = $registro_factura['Mes1'];
$mes2 = $registro_factura['Mes2'];
$mes3 = $registro_factura['Mes3'];
$mes4 = $registro_factura['Mes4'];
$mes5 = $registro_factura['Mes5'];
$mes6 = $registro_factura['Mes6'];
$mes7 = $registro_factura['Mes7'];
$mes8 = $registro_factura['Mes8'];
$mes9 = $registro_factura['Mes9'];
$mes10 = $registro_factura['Mes10'];
$mes11 = $registro_factura['Mes11'];
$mes12 = $registro_factura['Mes12'];

$anio1 = $registro_factura['An1'];
$anio2 = $registro_factura['An2'];
$anio3 = $registro_factura['An3'];
$anio4 = $registro_factura['An4'];
$anio5 = $registro_factura['An5'];
$anio6 = $registro_factura['An6'];
$anio7 = $registro_factura['An7'];
$anio8 = $registro_factura['An8'];
$anio9 = $registro_factura['An9'];
$anio10 = $registro_factura['An10'];
$anio11 = $registro_factura['An11'];
$anio12 = $registro_factura['An12'];

$con1 = $registro_factura['Con1'];
$con2 = $registro_factura['Con2'];
$con3 = $registro_factura['Con3'];
$con4 = $registro_factura['Con4'];
$con5 = $registro_factura['Con5'];
$con6 = $registro_factura['Con6'];
$con7 = $registro_factura['Con7'];
$con8 = $registro_factura['Con8'];
$con9 = $registro_factura['Con9'];
$con10 = $registro_factura['Con10'];
$con11 = $registro_factura['Con11'];
$con12 = $registro_factura['Con12'];

$i=0;
if($mes1=="") {} else {$meses[$i]=$mes1; $i+=1;}
if($mes2=="") {} else {$meses[$i]=$mes2; $i+=1;}
if($mes3=="") {} else {$meses[$i]=$mes3; $i+=1;}
if($mes4=="") {} else {$meses[$i]=$mes4; $i+=1;}
if($mes5=="") {} else {$meses[$i]=$mes5; $i+=1;}
if($mes6=="") {} else {$meses[$i]=$mes6; $i+=1;}
if($mes7=="") {} else {$meses[$i]=$mes7; $i+=1;}
if($mes8=="") {} else {$meses[$i]=$mes8; $i+=1;}
if($mes9=="") {} else {$meses[$i]=$mes9; $i+=1;}
if($mes10=="") {} else {$meses[$i]=$mes10; $i+=1;}
if($mes11=="") {} else {$meses[$i]=$mes11; $i+=1;}
if($mes12=="") {} else {$meses[$i]=$mes12; $i+=1;}

$i=0;
if($anio1=="") {} else {$anios[$i]=$anio1; $i+=1;}
if($anio2=="") {} else {$anios[$i]=$anio2; $i+=1;}
if($anio3=="") {} else {$anios[$i]=$anio3; $i+=1;}
if($anio4=="") {} else {$anios[$i]=$anio4; $i+=1;}
if($anio5=="") {} else {$anios[$i]=$anio5; $i+=1;}
if($anio6=="") {} else {$anios[$i]=$anio6; $i+=1;}
if($anio7=="") {} else {$anios[$i]=$anio7; $i+=1;}
if($anio8=="") {} else {$anios[$i]=$anio8; $i+=1;}
if($anio9=="") {} else {$anios[$i]=$anio9; $i+=1;}
if($anio10=="") {} else {$anios[$i]=$anio10; $i+=1;}
if($anio11=="") {} else {$anios[$i]=$anio11; $i+=1;}
if($anio12=="") {} else {$anios[$i]=$anio12; $i+=1;}

$i=0;
if($con1=="") {} else {$consu[$i]=$con1; $i+=1;}
if($con2=="") {} else {$consu[$i]=$con2; $i+=1;}
if($con3=="") {} else {$consu[$i]=$con3; $i+=1;}
if($con4=="") {} else {$consu[$i]=$con4; $i+=1;}
if($con5=="") {} else {$consu[$i]=$con5; $i+=1;}
if($con6=="") {} else {$consu[$i]=$con6; $i+=1;}
if($con7=="") {} else {$consu[$i]=$con7; $i+=1;}
if($con8=="") {} else {$consu[$i]=$con8; $i+=1;}
if($con9=="") {} else {$consu[$i]=$con9; $i+=1;}
if($con10=="") {} else {$consu[$i]=$con10; $i+=1;}
if($con11=="") {} else {$consu[$i]=$con11; $i+=1;}
if($con12=="") {} else {$consu[$i]=$con12; $i+=1;}

$emisora=$registro_factura['NIFEmisora'];
$referencia=$registro_factura['Referencia'];
$identificacion=$registro_factura['Identi'];
$meses_nombres=array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
$fecha_partes=explode('-',$fecha[0]);
$importe_total=number_format($registro_factura["Importe"],2,",",".");

$pdf->AddPage();
$pdf->SetFont('Arial');

$pdf->Image('fpdf/front.png', 2, 5);

$pdf->CreaCelda(22,253.5,15,'Fecha',255,255,255,'Arial','B',8);
$pdf->CreaCelda(22,257.5,15,'P1 Punta',255,255,255,'Arial','B',8);
$pdf->CreaCelda(22,260.5,15,'P2 Llano',255,255,255,'Arial','B',8);
$pdf->CreaCelda(22,263.5,15,'P3 Valle',255,255,255,'Arial','B',8);
$pdf->CreaCelda(22,267,15,'P4',255,255,255,'Arial','B',8);
$pdf->CreaCelda(22,270,15,'P5',255,255,255,'Arial','B',8);
$pdf->CreaCelda(22,273.5,15,'P6',255,255,255,'Arial','B',8);

/* INFORMACION DE FACTURA */
$pdf->CreaCelda(18,43.5,25,utf8_decode('Nº FACTURA'),255,255,255,'Arial','B',8);
$pdf->CreaCelda(18,47.5,25,utf8_decode('FECHA FACTURA'),255,255,255,'Arial','B',8);
$pdf->CreaCelda(18,51.5,25,utf8_decode('PERIODO FACTURACION'),255,255,255,'Arial','B',8);
$pdf->CreaCelda(18,56,25,utf8_decode('TOTAL FACTURA'),255,255,255,'Arial','B',9);
$pdf->CreaCelda(18,62,25,utf8_decode('FORMA DE PAGO'),255,255,255,'Arial','B',8);
$pdf->CreaCelda(18,67,25,utf8_decode('ENTIDAD BANCARIA'),255,255,255,'Arial','B',8);
$pdf->CreaCelda(18,71,25,utf8_decode('Nº DE CUENTA'),255,255,255,'Arial','B',8);

$pdf->CreaCelda(58,43.5,25,$num_factura,255,255,255,'Arial','',8);
$pdf->CreaCelda(58,47.5,25,$fecha_partes[2].' de '.$meses_nombres[number_format($fecha_partes[1]-1)].' de '.$fecha_partes[0],255,255,255,'Arial','',8);
$pdf->CreaCelda(58,51.5,25,$periodo_factura,255,255,255,'Arial','',8);
$pdf->CreaCelda(58,56,25,$importe_total.' Euros',255,255,255,'Arial','b',10);
$pdf->CreaCelda(58,62,25,$registro_factura['Energia-Cal'],255,255,255,'Arial','',8);
$pdf->CreaCelda(58,67,25,$banco,255,255,255,'Arial','',8);
$pdf->CreaCelda(58,71,25,$num_cuenta_banco.''.$num_cuenta_sucursal.''.$num_cuenta_digito_control.''.substr($num_cuenta_cuenta,1,5).'*****',255,255,255,'Arial','',8);

$pdf->CreaCelda(110,55,25,$nombre_suministro,255,255,255,'Arial','B',10);
$pdf->CreaCelda(110,60,25,$direccion_suministro,255,255,255,'Arial','B',10);
$pdf->CreaCelda(110,65,25,$suministro_CodPos.'     '.$suministro_ciudad,255,255,255,'Arial','B',10);

/*$pdf->CreaCelda(110,68,25,'Codigo Cliente',255,255,255,'Arial','',8);
$pdf->CreaCelda(150,78,25,$_SESSION['numeroCliente'],255,255,255,'Arial','',8);*/

/* DATOS DE SUMINISTRO */
$pdf->CreaCelda(18,84,25,utf8_decode('TITULAR'),255,255,255,'Arial','B',8);
$pdf->CreaCelda(18,87.5,25,utf8_decode('DIR SUMINISTRO'),255,255,255,'Arial','B',8);
$pdf->CreaCelda(18,91,25,utf8_decode('POBLACION'),255,255,255,'Arial','B',8);
$pdf->CreaCelda(18,94.5,25,utf8_decode('DNI/CIF'),255,255,255,'Arial','B',8);
$pdf->CreaCelda(18,98,25,utf8_decode('COD. CLIENTE'),255,255,255,'Arial','B',8);
$pdf->CreaCelda(18,101.5,25,utf8_decode('TARIFA'),255,255,255,'Arial','B',8);

$pdf->CreaCelda(52,84,25,$nombre_suministro,255,255,255,'Arial','',8);
$pdf->CreaCelda(52,87.5,25,$direccion_titular,255,255,255,'Arial','',8);
$pdf->CreaCelda(52,91,25,$titularCP.'   '.$titularPob,255,255,255,'Arial','',8);
$pdf->CreaCelda(52,94.5,25,$dni_titular,255,255,255,'Arial','',8);
$pdf->CreaCelda(52,98,25,$_SESSION['numeroCliente'],255,255,255,'Arial','',8);
$pdf->CreaCelda(52,101.5,25,$tarifa,255,255,255,'Arial','',8);

$pdf->CreaCelda(120,84.5,20,utf8_decode('Nº Cliente'),255,255,255,'Arial','B',8);
//$pdf->CreaCelda(120,98,20,utf8_decode('CNAE'),255,255,255,'Arial','B',8);
$pdf->CreaCelda(120,95.5,20,utf8_decode('CUPS'),255,255,255,'Arial','B',8);
$pdf->CreaCelda(120,100,20,utf8_decode('POTENCIA'),255,255,255,'Arial','B',8);
//$pdf->CreaCelda(120,108.5,20,utf8_decode('DISCRIM'),255,255,255,'Arial','B',8);

$pdf->CreaCelda(140,84.5,25,$_SESSION['usuario'],255,255,255,'Arial','',8);
//$pdf->CreaCelda(140,98,25,$cnae,255,255,255,'Arial','',8);
$pdf->CreaCelda(140,95.5,25,$cups,255,255,255,'Arial','',8);
$pdf->CreaCelda(140,100,25,$registro_factura['pckw_ab'],255,255,255,'Arial','',8);
//$pdf->CreaCelda(140,108.5,25,$discHorario,255,255,255,'Arial','',8);


/* LECTURAS */
$fechalecturaant = explode('-',$fechalecturaant[0]);
$fechalectura = explode('-',$fechalectura[0]);

$pdf->CreaCeldaAlDer3(45,250,10,'Anterior',255,255,255,'Arial','B',8);
$pdf->CreaCeldaAlDer(45,253.5,10,$fechalecturaant[2].'/'.$fechalecturaant[1].'/'.$fechalecturaant[0],255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(45,257.5,10,$lecturaP1ant,255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(45,260.5,10,$lecturaP2ant,255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(45,263.5,10,$lecturaP3ant,255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(45,267,10,$lecturaP4ant,255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(45,270.5,10,$lecturaP5ant,255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(45,273.5,10,$lecturaP6ant,255,255,255,'Arial','',8);

$pdf->CreaCeldaAlDer3(60,250,10,'Actual',255,255,255,'Arial','B',8);
$pdf->CreaCeldaAlDer(60,253.5,10,$fechalectura[2].'/'.$fechalectura[1].'/'.$fechalectura[0],255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(60,257.5,10,$lecturaP1act,255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(60,260.5,10,$lecturaP2act,255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(60,263.5,10,$lecturaP3act,255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(60,267.5,10,$lecturaP4act,255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(60,270.5,10,$lecturaP5act,255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(60,273.5,10,$lecturaP6act,255,255,255,'Arial','',8);

$pdf->CreaCeldaAlDer3(75,250,10,'Consumo',255,255,255,'Arial','B',8);
$pdf->CreaCeldaAlDer(75,253.5,10,'REAL',255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(75,257.5,10,$ConsumoP1,255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(75,260.5,10,$ConsumoP2,255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(75,263.5,10,$ConsumoP3,255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(75,267,10,$ConsumoP4,255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(75,270.5,10,$ConsumoP5,255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(75,273.5,10,$ConsumoP6,255,255,255,'Arial','',8);

$pdf->CreaCeldaAlDer3(95,250,10,'Anterior',255,255,255,'Arial','B',8);
$pdf->CreaCeldaAlDer(95,253.5,10,$fechalecturaant[2].'/'.$fechalecturaant[1].'/'.$fechalecturaant[0],255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(95,257.5,10,$lecturaReacP1ant,255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(95,260.5,10,$lecturaReacP2ant,255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(95,263.5,10,$lecturaReacP3ant,255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(95,267,10,$lecturaReacP4ant,255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(95,270.5,10,$lecturaReacP5ant,255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(95,273.5,10,$lecturaReacP6ant,255,255,255,'Arial','',8);

$pdf->CreaCeldaAlDer3(110,250,10,'Actual',255,255,255,'Arial','B',8);
$pdf->CreaCeldaAlDer(110,253.5,10,$fechalectura[2].'/'.$fechalectura[1].'/'.$fechalectura[0],255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(110,257.5,10,$lecturaReacP1act,255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(110,260.5,10,$lecturaReacP2act,255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(110,263.5,10,$lecturaReacP3act,255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(110,267,10,$lecturaReacP4act,255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(110,270.5,10,$lecturaReacP5act,255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(110,273.5,10,$lecturaReacP6act,255,255,255,'Arial','',8);

$pdf->CreaCeldaAlDer3(125,250,10,'Consumo',255,255,255,'Arial','B',8);
$pdf->CreaCeldaAlDer(125,253.5,10,'REAL',255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(125,257.5,10,$ConsumoReacP1,255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(125,260.5,10,$ConsumoReacP2,255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(125,263.5,10,$ConsumoReacP3,255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(125,267,10,$ConsumoReacP4,255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(125,270.5,10,$ConsumoReacP5,255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(125,273.5,10,$ConsumoReacP6,255,255,255,'Arial','',8);

$pdf->CreaCeldaAlDer(140,253.5,10,'REAL',255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(140,257.5,10,$lecturaMaxP1,255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(140,260.5,10,$lecturaMaxP2,255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(140,263.5,10,$lecturaMaxP3,255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(140,267,10,$lecturaMaxP4,255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(140,270.5,10,$lecturaMaxP5,255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(140,273.5,10,$lecturaMaxP6,255,255,255,'Arial','',8);

/* CALCULO DE LA FACTURA */
$posy=131;
$lineas=0;

$pdf->CreaCelda(18,$posy-4,40,'Termino de Potencia:',255,255,255,'Arial','B',8);

//Potencia P1
if($importePotP1==0 and $textoPotP1==""){} else {
	$pdf->CreaCelda(18,$posy,70,$textoPotP1,255,255,255,'Arial','',8);
	$pdf->CreaCeldaAlDer(190,$posy,6,$importePotP1,255,255,255);
	$lineas+=1; 
	$posy+=3;
}
if($importeDtoPotP1==0 and $textoDtoPotP1==""){} else {
	$pdf->CreaCelda(18,$posy,70,$textoDtoPotP1,255,255,255,'Arial','',8);
	$pdf->CreaCeldaAlDer(190,$posy,6,$importeDtoPotP1,255,255,255);
	$lineas+=1; 
	$posy+=3;
}

//Potencia P2
if($importePotP2==0 and $textoPotP2==""){} else {
	$pdf->CreaCelda(18,$posy,70,$textoPotP2,255,255,255,'Arial','',8);
	$pdf->CreaCeldaAlDer(190,$posy,6,$importePotP2,255,255,255);
	$lineas+=1; 
	$posy+=3;
}
if($importeDtoPotP2==0 and $textoDtoPotP2==""){} else {
	$pdf->CreaCelda(18,$posy,70,$textoDtoPotP2,255,255,255,'Arial','',8);
	$pdf->CreaCeldaAlDer(190,$posy,6,$importeDtoPotP2,255,255,255);
	$lineas+=1; 
	$posy+=3;
}

//Potencia P3
if($importePotP3==0 and $textoPotP3==""){} else {
	$pdf->CreaCelda(18,$posy,70,$textoPotP3,255,255,255,'Arial','',8);
	$pdf->CreaCeldaAlDer(190,$posy,6,$importePotP3,255,255,255);
	$lineas+=1; 
	$posy+=3;
}
if($importeDtoPotP3==0 and $textoDtoPotP3==""){} else {
	$pdf->CreaCelda(18,$posy,70,$textoDtoPotP3,255,255,255,'Arial','',8);
	$pdf->CreaCeldaAlDer(190,$posy,6,$importeDtoPotP3,255,255,255);
	$lineas+=1; 
	$posy+=3;
}

//Potencia P4
if($importePotP4==0 and $textoPotP4==""){} else {
	$pdf->CreaCelda(18,$posy,70,$textoPotP4,255,255,255,'Arial','',8);
	$pdf->CreaCeldaAlDer(190,$posy,6,$importePotP4,255,255,255);
	$lineas+=1; 
	$posy+=3;
}
if($importeDtoPotP4==0 and $textoDtoPotP4==""){} else {
	$pdf->CreaCelda(18,$posy,70,$textoDtoPotP4,255,255,255,'Arial','',8);
	$pdf->CreaCeldaAlDer(190,$posy,6,$importeDtoPotP4,255,255,255);
	$lineas+=1; 
	$posy+=3;
}

//Potencia P5
if($importePotP5==0 and $textoPotP5==""){} else {
	$pdf->CreaCelda(18,$posy,70,$textoPotP5,255,255,255,'Arial','',8);
	$pdf->CreaCeldaAlDer(190,$posy,6,$importePotP5,255,255,255);
	$lineas+=1; 
	$posy+=3;
}
if($importeDtoPotP5==0 and $textoDtoPotP5==""){} else {
	$pdf->CreaCelda(18,$posy,70,$textoDtoPotP5,255,255,255,'Arial','',8);
	$pdf->CreaCeldaAlDer(190,$posy,6,$importeDtoPotP5,255,255,255);
	$lineas+=1; 
	$posy+=3;
}

//Potencia P6
if($importePotP6==0 and $textoPotP6==""){} else {
	$pdf->CreaCelda(18,$posy,70,$textoPotP6,255,255,255,'Arial','',8);
	$pdf->CreaCeldaAlDer(190,$posy,6,$importePotP6,255,255,255);
	$lineas+=1; 
	$posy+=3;
}
if($importeDtoPotP6==0 and $textoDtoPotP6==""){} else {
	$pdf->CreaCelda(18,$posy,70,$textoDtoPotP6,255,255,255,'Arial','',8);
	$pdf->CreaCeldaAlDer(190,$posy,6,$importeDtoPotP6,255,255,255);
	$lineas+=1; 
	$posy+=3;
}


$pdf->CreaCelda(18,$posy+1,40,'Termino de Energia (Mercado Electrico) ',255,255,255,'Arial','B',8);
$posy+=5;
// Energia P1
if($importeEnerP1==0 and $textoEnerP1==""){} else {
	$pdf->CreaCelda(18,$posy,70,$textoEnerP1,255,255,255,'Arial','',8);
	$pdf->CreaCeldaAlDer(190,$posy,6,$importeEnerP1,255,255,255);
	$lineas+=1; 
	$posy+=3;
}
if($importeDtoEnerP1==0 and $textoDtoEnerP1==""){} else {
	$pdf->CreaCelda(18,$posy,70,$textoDtoEnerP1,255,255,255,'Arial','',8);
	$pdf->CreaCeldaAlDer(190,$posy,6,$importeDtoEnerP1,255,255,255);
	$lineas+=1; 
	$posy+=3;
}
// Energia P2
if($importeEnerP2==0 and $textoEnerP2==""){} else {
	$pdf->CreaCelda(18,$posy,70,$textoEnerP2,255,255,255,'Arial','',8);
	$pdf->CreaCeldaAlDer(190,$posy,6,$importeEnerP2,255,255,255);
	$lineas+=1; 
	$posy+=3;
}
if($importeDtoEnerP2==0 and $textoDtoEnerP2==""){} else {
	$pdf->CreaCelda(18,$posy,70,$textoDtoEnerP2,255,255,255,'Arial','',8);
	$pdf->CreaCeldaAlDer(190,$posy,6,$importeDtoEnerP2,255,255,255);
	$lineas+=1; 
	$posy+=3;
}

//Energia P3
if($importeEnerP3==0 and $textoEnerP3==""){} else {
	$pdf->CreaCelda(18,$posy,70,$textoEnerP3,255,255,255,'Arial','',8);
	$pdf->CreaCeldaAlDer(190,$posy,6,$importeEnerP3,255,255,255);
	$lineas+=1; 
	$posy+=3;
}
if($importeDtoEnerP3==0 and $textoDtoEnerP3==""){} else {
	$pdf->CreaCelda(18,$posy,70,$textoDtoEnerP3,255,255,255,'Arial','',8);
	$pdf->CreaCeldaAlDer(190,$posy,6,$importeDtoEnerP3,255,255,255);
	$lineas+=1; 
	$posy+=3;
}

// Energia P4
if($importeEnerP4==0 and $textoEnerP4==""){} else {
	$pdf->CreaCelda(18,$posy,70,$textoEnerP4,255,255,255,'Arial','',8);
	$pdf->CreaCeldaAlDer(190,$posy,6,$importeEnerP4,255,255,255);
	$lineas+=1; 
	$posy+=3;
}
if($importeDtoEnerP4==0 and $textoDtoEnerP4==""){} else {
	$pdf->CreaCelda(18,$posy,70,$textoDtoEnerP4,255,255,255,'Arial','',8);
	$pdf->CreaCeldaAlDer(190,$posy,6,$importeDtoEnerP4,255,255,255);
	$lineas+=1; 
	$posy+=3;
}

//Energia P5
if($importeEnerP5==0 and $textoEnerP5==""){} else {
	$pdf->CreaCelda(18,$posy,70,$textoEnerP5,255,255,255,'Arial','',8);
	$pdf->CreaCeldaAlDer(190,$posy,6,$importeEnerP5,255,255,255);
	$lineas+=1; 
	$posy+=3;
}

if($importeDtoEnerP5==0 and $textoDtoEnerP5==""){} else {
	$pdf->CreaCelda(18,$posy,70,$textoDtoEnerP5,255,255,255,'Arial','',8);
	$pdf->CreaCeldaAlDer(190,$posy,6,$importeDtoEnerP5,255,255,255);
	$lineas+=1; 
	$posy+=3;
}

//Energia P6
if($importeEnerP6==0 and $textoEnerP6==""){} else {
	$pdf->CreaCelda(18,$posy,70,$textoEnerP6,255,255,255,'Arial','',8);
	$pdf->CreaCeldaAlDer(190,$posy,6,$importeEnerP6,255,255,255);
	$lineas+=1; 
	$posy+=3;
}

if($importeDtoEnerP6==0 and $textoDtoEnerP6==""){} else {
	$pdf->CreaCelda(18,$posy,70,$textoDtoEnerP6,255,255,255,'Arial','',8);
	$pdf->CreaCeldaAlDer(190,$posy,6,$importeDtoEnerP6,255,255,255);
	$lineas+=1; 
	$posy+=3;
}

$pdf->CreaCelda(18,$posy+1,40,'Termino de Reactiva ',255,255,255,'Arial','B',8);
$posy+=5;
if($importeReacP1==0 and $textoReacP1==""){ } else {
	$pdf->CreaCelda(18,$posy,70,$textoReacP1,255,255,255,'Arial','',8);
	$pdf->CreaCeldaAlDer(190,$posy,6,$importeReacP1,255,255,255);
	$lineas+=1; 
	$posy+=3;
}

if($importeReacP2==0 and $textoReacP2==""){ } else {
	$pdf->CreaCelda(18,$posy,70,$textoReacP2,255,255,255,'Arial','',8);
	$pdf->CreaCeldaAlDer(190,$posy,6,$importeReacP2,255,255,255);
	$lineas+=1; 
	$posy+=3;
}

if($importeReacP3==0 and $textoReacP3==""){ } else {
	$pdf->CreaCelda(18,$posy,70,$textoReacP3,255,255,255,'Arial','',8);
	$pdf->CreaCeldaAlDer(190,$posy,6,$importeReacP3,255,255,255);
	$lineas+=1; 
	$posy+=3;
}

if($importeReacP4==0 and $textoReacP4==""){ } else {
	$pdf->CreaCelda(18,$posy,70,$textoReacP4,255,255,255,'Arial','',8);
	$pdf->CreaCeldaAlDer(190,$posy,6,$importeReacP4,255,255,255);
	$lineas+=1; 
	$posy+=3;
}

if($importeReacP5==0 and $textoReacP5==""){ } else {
	$pdf->CreaCelda(18,$posy,70,$textoReacP5,255,255,255,'Arial','',8);
	$pdf->CreaCeldaAlDer(190,$posy,6,$importeReacP5,255,255,255);
	$lineas+=1; 
	$posy+=3;
}

if($importeReacP6==0 and $textoReacP6==""){ } else {
	$pdf->CreaCelda(18,$posy,70,$textoReacP6,255,255,255,'Arial','',8);
	$pdf->CreaCeldaAlDer(190,$posy,6,$importeReacP6,255,255,255);
	$lineas+=1; 
	$posy+=3;
}

/*maximetros*/
$pdf->CreaCelda(18,$posy+1,40,'Excesos de Potencia (Maximetros) ',255,255,255,'Arial','B',8);
$posy+=5;
if($importeMaxP1==0 and $textoMaxP1==""){ } else {
	$pdf->CreaCelda(18,$posy,70,$textoMaxP1,255,255,255,'Arial','',8);
	$pdf->CreaCeldaAlDer(190,$posy,6,$importeMaxP1.' '.$euro,255,255,255);
	$lineas+=1; 
	$posy+=3;
}
if($importeMaxP2==0 and $textoMaxP2==""){ } else {
	$pdf->CreaCelda(18,$posy,70,$textoMaxP2,255,255,255,'Arial','',8);
	$pdf->CreaCeldaAlDer(190,$posy,6,$importeMaxP2.' '.$euro,255,255,255);
	$lineas+=1; 
	$posy+=3;
}
if($importeMaxP3==0 and $textoMaxP3==""){ } else {
	$pdf->CreaCelda(18,$posy,70,$textoMaxP3,255,255,255,'Arial','',8);
	$pdf->CreaCeldaAlDer(190,$posy,6,$importeMaxP3.' '.$euro,255,255,255);
	$lineas+=1; 
	$posy+=3;
}
if($importeMaxP4==0 and $textoMaxP4==""){ } else {
	$pdf->CreaCelda(18,$posy,70,$textoMaxP4,255,255,255,'Arial','',8);
	$pdf->CreaCeldaAlDer(190,$posy,6,$importeMaxP4.' '.$euro,255,255,255);
	$lineas+=1; 
	$posy+=3;
}
if($importeMaxP5==0 and $textoMaxP5==""){ } else {
	$pdf->CreaCelda(18,$posy,70,$textoMaxP5,255,255,255,'Arial','',8);
	$pdf->CreaCeldaAlDer(190,$posy,6,$importeMaxP5.' '.$euro,255,255,255);
	$lineas+=1; 
	$posy+=3;
}
if($importeMaxP6==0 and $textoMaxP6==""){ } else {
	$pdf->CreaCelda(18,$posy,70,$textoMaxP6,255,255,255,'Arial','',8);
	$pdf->CreaCeldaAlDer(190,$posy,6,$importeMaxP6.' '.$euro,255,255,255);
	$lineas+=1; 
	$posy+=3;
}



if($registro_factura['ImporteAdicional']==0){} else {
	$pdf->CreaCelda(18,204,40,$registro_factura['ConceptoAdicional'],255,255,255,'Arial','',8);
	$pdf->CreaCeldaAlDer(170,204,6,$registro_factura['ImporteAdicional'],255,255,255,'Arial','',8);
}

if ($boe2012consumo42011==0 and $boe2012importe42011 == 0 and $boe2012texto42011=="") {} else {
	$pdf->CreaCelda(18,210,40,$boe2012texto42011,255,255,255, 'Arial','',8);
	$pdf->CreaCeldaAlDer(170,210,6,$boe2012importe42011,255,255,255);
	$posy+=3;
}

if ($boe2012consumo12012==0 and $boe2012importe12012 == 0 and $boe2012texto12012=="") {} else {
	$pdf->CreaCelda(18,210,40,$boe2012texto12012,255,255,255, 'Arial','',8);
	$pdf->CreaCeldaAlDer(170,210,6,$boe2012importe12012,255,255,255);
	$posy+=3;
}

if ($boe2012consumototal==0 and $boe2012importetotal == 0 and $boe2012textototal=="") {} else {
	$pdf->CreaCelda(18,$posy,80,$boe2012textototal,255,255,255, 'Arial','',8);
	$pdf->CreaCeldaAlDer(190,$posy,7,$boe2012importetotal,255,255,255);
	$posy+=3;
}

$pdf->CreaCelda(18,214,30,'Impuesto Electrico',255,255,255,'Arial','B',8);
$pdf->CreaCelda(50,214,70,$impuesto.'   % sobre   '.$simpuesto.'  eur.  x   '.$coeficiente,255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(190,214,6,$total_impuesto,255,255,255,'Arial','',8);

if($alquiler==0){} else {
	$pdf->CreaCelda(18,217,40,'Importe alquiler',255,255,255,'Arial','',8);
	$pdf->CreaCeldaAlDer(190,217,6,number_format($alquiler,2,",","."),255,255,255,'Arial','',8);
}

/* OBSERVACIONES */
//$pdf->CreaCelda(10.5,230.5,25,$registro_factura['textolibre'],255,255,255,'Arial','B',8);
//$pdf->SeccionBodyBG2(116,227,83,$registro_factura['textolibre'],4,255,255,255);

/* BASE IMPONIBLE... */
//$pdf->CreaCelda(18,225,11,'Base Imponible',255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer3(143,224,11,$base1,255,255,255,'Arial','B',7);
$pdf->CreaCeldaAlDer3(159,224,11,number_format($iva1,2).' %',255,255,255,'Arial','',7);
$pdf->CreaCeldaAlDer3(184,224,11,$cuota1,255,255,255,'Arial','',7);
/*$pdf->CreaCeldaAlDer3(147,229,11,$base2,255,255,255,'Arial','B',7);
if ($iva2!= 0){
	$pdf->CreaCeldaAlDer3(165,229,11,number_format($iva2,2).' %',255,255,255,'Arial','',7);
}*/
//$pdf->CreaCeldaAlDer3(190,228.5,11,$cuota2,255,255,255,'Arial','',7);

//$pdf->CreaCelda(118,233,15,'TOTAL FACTURA',255,255,255,'Arial','B',9);
$pdf->CreaCeldaAlDer3(171,233,20,$importe_total,255,255,255,'Arial','B',9);


// Siguiente pagina
$pdf->AddPage();
$pdf->Image('fpdf/back.png', 5, 8, 197);


/*if($tarifa=="2.0A ML"){$pdf->Image('fpdf/back20.png', 5, 8, 197);} else {}
if($tarifa=="2.0DHA ML"){$pdf->Image('fpdf/back20DHA.png', 5, 8, 197);} else {}
if($tarifa=="2.1A ML"){$pdf->Image('fpdf/back21.png', 5, 8, 197);} else {}
if($tarifa=="2.1DHA ML"){$pdf->Image('fpdf/back21DHA.png', 5, 8, 197);} else {}
if($tarifa=="3.0A ML"){$pdf->Image('fpdf/back30A.png', 5, 8, 197);} else {}
if($tarifa=="6.1A ML"){$pdf->Image('fpdf/back61A.png', 5, 8, 197);} else {}
if($tarifa=="3.1A ML"){$pdf->Image('fpdf/back31A.png', 5, 8, 197);} else {}*/



$pdf->CreaCelda(68,14,25,$mes1,255,255,255,'Arial','',8);
$pdf->CreaCelda(68,18,25,$mes2,255,255,255,'Arial','',8);
$pdf->CreaCelda(68,22,25,$mes3,255,255,255,'Arial','',8);
$pdf->CreaCelda(68,26,25,$mes4,255,255,255,'Arial','',8);
$pdf->CreaCelda(68,30,25,$mes5,255,255,255,'Arial','',8);
$pdf->CreaCelda(68,34,25,$mes6,255,255,255,'Arial','',8);
$pdf->CreaCelda(68,38,25,$mes7,255,255,255,'Arial','',8);
$pdf->CreaCelda(68,42,25,$mes8,255,255,255,'Arial','',8);
$pdf->CreaCelda(68,46,25,$mes9,255,255,255,'Arial','',8);
$pdf->CreaCelda(68,50,25,$mes10,255,255,255,'Arial','',8);
$pdf->CreaCelda(68,54,25,$mes11,255,255,255,'Arial','',8);
$pdf->CreaCelda(68,58,25,$mes12,255,255,255,'Arial','',8);

$pdf->CreaCelda(105,14,25,$anio1,255,255,255,'Arial','',8);
$pdf->CreaCelda(105,18,25,$anio2,255,255,255,'Arial','',8);
$pdf->CreaCelda(105,22,25,$anio3,255,255,255,'Arial','',8);
$pdf->CreaCelda(105,26,25,$anio4,255,255,255,'Arial','',8);
$pdf->CreaCelda(105,30,25,$anio5,255,255,255,'Arial','',8);
$pdf->CreaCelda(105,34,25,$anio6,255,255,255,'Arial','',8);
$pdf->CreaCelda(105,38,25,$anio7,255,255,255,'Arial','',8);
$pdf->CreaCelda(105,42,25,$anio8,255,255,255,'Arial','',8);
$pdf->CreaCelda(105,46,25,$anio9,255,255,255,'Arial','',8);
$pdf->CreaCelda(105,50,25,$anio10,255,255,255,'Arial','',8);
$pdf->CreaCelda(105,54,25,$anio11,255,255,255,'Arial','',8);
$pdf->CreaCelda(105,58,25,$anio12,255,255,255,'Arial','',8);

$pdf->CreaCelda(120,14,5,$con1,255,255,255,'Arial','',8);
$pdf->CreaCelda(120,18,5,$con2,255,255,255,'Arial','',8);
$pdf->CreaCelda(120,22,5,$con3,255,255,255,'Arial','',8);
$pdf->CreaCelda(120,26,5,$con4,255,255,255,'Arial','',8);
$pdf->CreaCelda(120,30,5,$con5,255,255,255,'Arial','',8);
$pdf->CreaCelda(120,34,5,$con6,255,255,255,'Arial','',8);
$pdf->CreaCelda(120,38,5,$con7,255,255,255,'Arial','',8);
$pdf->CreaCelda(120,42,5,$con8,255,255,255,'Arial','',8);
$pdf->CreaCelda(120,46,5,$con9,255,255,255,'Arial','',8);
$pdf->CreaCelda(120,50,5,$con10,255,255,255,'Arial','',8);
$pdf->CreaCelda(120,54,5,$con11,255,255,255,'Arial','',8);
$pdf->CreaCelda(120,58,5,$con12,255,255,255,'Arial','',8);
}

$pdf->Output("ResumenFacturas_".$nif."_".date('d-m-Y').".pdf", 'D');

?>