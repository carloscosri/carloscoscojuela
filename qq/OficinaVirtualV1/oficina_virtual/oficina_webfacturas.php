<?php
/*
NOTAS:

1- ESTA SECCION TOMA LOS DATOS DE LA TABLA FacturasPDF

- ESCRIBE EN LA TABLA SolicitudFacturas

*/

/* NOTA:

 COMO NO SE CIERRA LA CONEXION TRAS CADA CONEXION CON LA BASE DE DATOS, LAS CONSULTAS DEJAN DE FUNCIONAR SI SE ESTABLECE COMO UNICA CONEXION
 LA QUE UTILIZA EL RESTO DE LA WEB, POR LO TANTO HASTA QUE SE HAGA LA MIGRACION COMPLETA DE LOS TEXTOS, COEXISTIRAN LAS DOS CONEXIONES, YA QUE
 SE HA AÑADIDO UNA LLAMADA A LA CONEXION BUENA EN ESTE ARCHIVO.
*/

include("_conexion.php");

//Se incluye el archivo que contiene las verificaciones de datos
include("../includes/verificaciones.php");

//Se incluye el archivo con los textos especifico de esta seccion en el idioma oportuno
//include($_SESSION["directorio_raiz"]."idiomas/".$_SESSION["idioma"]."/oficina_informacion_facturas.php");
include("../idiomas/".$_SESSION["idioma"]."/oficina_datos_titular.php");
include("../idiomas/".$_SESSION["idioma"]."/oficina_informacion_facturas.php");

include("../idiomas/".$_SESSION["idioma"]."/oficina_informacion_certificadofacturas.php");

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<title><?=$titulo_web;?></title>

<!--Se añaden las hojas de estilos genericas que utiliza esta seccion-->
<link href="../css/estilos.css" rel="stylesheet" type="text/css" />
<link href="../css/secciones.css" rel="stylesheet" type="text/css" />
<link href="css/oficina.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_vieja.css" rel="stylesheet" type="text/css" />

<link href="css/oficina_informacion_contratos_y_facturas.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_informacion_facturas.css" rel="stylesheet" type="text/css" />

<link href="css/oficina_informacion_contratos_y_facturas.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_informacion_certificadoconsumos.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_informacion_certificadofacturas.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" media="all" href="scripts/calendar/skins/aqua/theme.css" title="Aqua" />


<!-- librería principal del calendario -->
<script type="text/javascript" src="scripts/calendar/calendar.js"></script>

<!-- librería para cargar el lenguaje deseado -->
<script type="text/javascript" src="scripts/calendar/lang/calendar-es.js"></script>

<!-- librería que declara la función Calendar.setup, que ayuda a generar un calendario en unas pocas líneas de código -->
<script type="text/javascript" src="scripts/calendar/calendar-setup.js"></script>




<!--Ahora se añaden las reglas para Internet Explorer. Asi corregiremos las diferencias de visualizacion entre navegadores
Es importante añadirlo despues de las reglas principales, para que se sobreescriban las correcciones pertinentes. En este caso las diferencias
solo son para internet explorer 7-->

<!--[if IE 7]>
	<link href="../css/estilos_ie7.css" rel="stylesheet" type="text/css" />
	<link href="../css/secciones_ie7.css" rel="stylesheet" type="text/css" />    
<![endif]-->

<!--[if IE]>
	<link href="css/tablas_informacionIE.css" rel="stylesheet" type="text/css" />
<![endif]-->

<!--[if IE 7]>
	<link href="css/tablas_informacionIE7.css" rel="stylesheet" type="text/css" />
<![endif]-->

<script language="javascript" type="text/javascript">
function avisa()
{
	alert("OPCION NO DISPONIBLE");
}//function avisa()
//-->
</script>

<?php

//Asignamos los valores por defecto a la fecha de inicio y la fecha de fin, estos cambiaran si el usuario pulsa el boton del formulario habiendo escrito algo
$fecha_inicio="dd-mm-aaaa";
$fecha_fin="dd-mm-aaaa";

if(isset($_POST["Submit"]))
{	
	$fecha_inicio=$_POST["DesdeFecha"];
	$fecha_fin=$_POST["HastaFecha"];
	$observaciones=$_POST["observaciones"];	
	
	$error=comparar_fechas($fecha_inicio,$fecha_fin);		
}//if(isset($_POST["Submit"]))

?>

</head>
	
<body>
<!--*************************************************Web realizada por ****************************************-->
	<div id="central_oficina">
    
<?php
//Se comprueba que el usuario se haya identificado, si no es asi se da un aviso y redirigimos a la seccion de identificacion
if ($_SESSION['usuario']=="")
{	
	MsgBox($error_identificacion);
	redirigir("oficina_index.php?id=".$_SESSION["idioma"]);
	
}//if ($_SESSION['usuario'])

//Si el usuario se ha identificado la seccion se ejecutara con normalidad
else
{
	
	$tipo=$_GET['s'];
	
//Añadimos los fragmentos comunes, la cabecera, el menu, y el submenu
	include("oficina_fragmentos_comunes.php");
?>    
	  <div class="contenido_seccion_oficina_informacion_factura_web">                                         
<!--VERSION VIEJA-->        	
<!--VERSION VIEJA-->        	
        <table width="800" border="0" cellspacing="0" cellpadding="0">
		  <?php if (isset($_SESSION['numeroCliente'])){
					 		$result = mysql_query("SELECT * FROM DatosRegistrados WHERE CodigoCliente = ".$_SESSION['numeroCliente']." ");
							$row = mysql_fetch_array($result);
					 }else{
					 		$result = mysql_query("SELECT * FROM DatosRegistrados WHERE CodigoCliente = -1");
							//Para que no tome ningun cliente pero no de ningun error en la pagina
							$row = mysql_fetch_array($result);
					 }
					 ?>
            <tr> 
              <td valign="top"><table class="tabla_datos_facturas_web" border="0" cellspacing="0" cellpadding="0">
                           
               
                <tr> 
                   
                  <td height="30" align="center" class="arialblack18Titulo">
                  Facturas WEB   </td>
			    </tr> 
				    <tr> 
				   <td class="arial14">&nbsp;</td>
				    </tr> 
                <tr>
                  <td  class="arial14"><fieldset >
                    <legend class="arialblack14">
                    <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 130'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                    </legend>
                    <table width="tabla_datos_contratos_y_facturas" align="center">
                      <tr>
                        <td height="3">&nbsp;</td>
                      </tr>
                      <tr>
                        <td height="22" valign="top" class="arial14"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 196'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                          &nbsp;</td>
                        <td valign="top" class="arial14"><input name="CodigoCliente" type="text" class="textfieldLecturaNum" id="CodigoCliente" value="<?php echo($row['CodigoCliente']);?>" size="5" maxlength="5"  readonly="readonly" />
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                          <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 118'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                          &nbsp;
                          <input name="SuministroDNI"  type="text" class="textfieldLecturaNum" id="SuministroDNI" value="<?php echo($row['DNI']);?>" size="9" maxlength="15" readonly="readonly" />
                          &nbsp;</td>
                      </tr>
                      <tr>
                        <td height="22" class="arial14">CUPS&nbsp;</td>
                        <td valign="top" class="arial14"><input name="CUPSFijo" value="<?php echo($row['CUPS']);?>"  type="text" class="textfieldLectura" id="CUPSFijo" size="25" maxlength="25" /></td>
                      </tr>
                      <tr>
                        <td height="22" valign="top" class="arial14"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 119'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                          &nbsp;&nbsp;</td>
                        <td valign="top"><input name="SuministroNombre" type="text"  class="textfieldLectura" id="SuministroNombre" value="<?php echo(str_replace('"','&quot;',$row['TitularNombre']));?>" size="50" maxlength="50" readonly="readonly" /></td>
                      </tr>
                      <tr>
                        <td height="22" valign="top" class="arial14"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 123'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                          &nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td valign="top"><input name="SuministroCalle" type="text"  class="textfieldLectura" id="SuministroExtensionNombre3" value="<?php echo(str_replace('"','&quot;',$row['SuministroCalle']));?>" size="50" maxlength="50" readonly="readonly" /></td>
                      </tr>
                      <tr>
                        <td height="22" valign="top" class="arial14"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 124'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                          &nbsp;</td>
                        <td valign="top" class="arial14"><input readonly="readonly"  name="SuministroNumero" type="text" class="textfieldLecturaNum" id="SuministroNumero" value="<?php echo($row['SuministroNumero']);?>" size="5" maxlength="5" />
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                          <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 125'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                          &nbsp;
                          <input name="SuministroExtension" type="text" class="textfieldLectura" id="SuministroExtension" value="<?php echo(str_replace('"','&quot;',$row['SuministroExtension']));?>" size="20" maxlength="20" readonly="readonly" /></td>
                      </tr>
                      <tr>
                        <td height="22" valign="top" class="arial14"><?=$oficina_datos_titular21?></td>
                        <td valign="top"><input name="SuministroAclaradorFijo" type="text" class="textfieldLectura" value="<?php echo(str_replace('"','&quot;',$row['SuministroExtension2']));?>" size="50" maxlength="50" /></td>
                      </tr>
                      <tr>
                        <td height="24" valign="middle" class="arial14"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 122'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?></td>
                        <td valign="top"><input readonly="readonly"  name="SuministroCP2" type="text" class="textfieldLecturaNum" id="SuministroCP3" value="<?php echo($row['SuministroCP']);?>" size="5" maxlength="5" /></td>
                      </tr>
                      
                      
                      <tr>
                        <td height="21" valign="middle" class="arial14"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 121'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?></td>
                        <td valign="top"><input  name="SuministroExtension2" type="text" class="textfieldLectura" id="SuministroExtension3" value="<?php echo(str_replace('"','&quot;',$row['SuministroCiudad']));?>" size="50" maxlength="50" readonly="readonly" /></td>
                      </tr>
                    </table>
                  </fieldset></td>
                </tr>
                <tr align="center"> 
                  <td class="arialblack16">&nbsp;</td>
                </tr>
                <tr align="center"> 
                  <td align="center" class="arial12Importanteb">
					En este apartado puede consultar las facturas que han sido emitidas a su nombre y que no han sido enviadas por correo, sino que puede descargarlas en formato PDF o imprimirlas directamente.<br />
<br />
En este sistema el periodo de tiempo que puede consultar o imprimir las facturas es de doce meses. Si necesita de alguna fecha anterior debe de solicitarla directamente a traves del apartado correspondiente de "Solicitud de Facturas" que se encuentra en esta misma Oficina Virtual.</td>
                </tr>
                <tr align="center"> 
                  <td height="20" class="arialblack16">&nbsp;</td>
                </tr>
                  <tr align="center"> 
                  <td class="arialblack16Titulo">Relación de Facturas existentes en Web.Facturas</td>
                </tr>
                   <tr align="center"> 
                  <td height="20" class="arialblack16">&nbsp;</td>
                </tr>
                   <tr align="center"> 
                <td align="center" class="arial12Importanteb"><div align="center">
				  Seleccione la Factura que desee descargar</div></td>
        </tr>
        <tr align="center"> 
                  <td class="arialblack16">&nbsp;</td>
                </tr>
                  <tr align="left"> 
                  <td class="arialblack16"><div align="center">
                    <table width="600" border="0" cellpadding="0" cellspacing="0">
                      <tr>
                        <td width="33%"><div align="center"><img src="../img/oficina/icono_pdf.gif" border="0" /><a href="oficina_webfacturas.php?s=1" class="submenu"> Pendientes</a></div></td>
                        <td width="37%"><div align="center"><img src="../img/oficina/icono_pdf_descargado.gif" border="0" /><a href="oficina_webfacturas.php?s=2" class="submenu"> Descargadas</a></div></td>
                        <td width="30%"><div align="center"><img src="../img/oficina/icono_pdf_todas.gif" border="0" /><a href="oficina_webfacturas.php?s=0" class="submenu"> Todas</a></div></td>
                      </tr>
                    </table>
                    <p>&nbsp;</p>
                  </div></td>
                </tr>
                
              </table>
<?php 
	   
		switch ($tipo)
		
		{
			
		case 1:
			
			$consulta_facturas_web="SELECT * FROM FacturasWeb WHERE (Cliente=".$_SESSION['numeroCliente'].") AND Disponible='S' AND Descargada='N' ORDER BY `FechaFactura` DESC";
			$resultados_facturas_web = mysql_query($consulta_facturas_web);			
			$num_facturas_web=mysql_num_rows($resultados_facturas_web);
			
		break;
		
		case 2:
			
			$consulta_facturas_web="SELECT * FROM FacturasWeb WHERE (Cliente=".$_SESSION['numeroCliente'].") AND Descargada='S' AND Disponible='S' ORDER BY `FechaFactura` DESC";
			$resultados_facturas_web = mysql_query($consulta_facturas_web);			
			$num_facturas_web=mysql_num_rows($resultados_facturas_web);
			
		break;
				
		default:
			
			$consulta_facturas_web="SELECT * FROM FacturasWeb WHERE Cliente=".$_SESSION['numeroCliente']." ORDER BY `FechaFactura` DESC";
			$resultados_facturas_web = mysql_query($consulta_facturas_web);			
			$num_facturas_web=mysql_num_rows($resultados_facturas_web);
			
		break;
		
		
			
		}


			
//Si hay facturas se mostrara un resumen de los datos de cada una de ellas			
			if($num_facturas_web != 0)
			{
				
				while($registro_factura_web = mysql_fetch_array($resultados_facturas_web))
				{
					
					
			$consulta_facturas="SELECT * FROM FacturasPDF WHERE (NumeroCliente=".$_SESSION['numeroCliente']." AND FechaFactura = '".$registro_factura_web["FechaFactura"]."')";
			$resultados_facturas = mysql_query($consulta_facturas);			
			$num_facturas=mysql_num_rows($resultados_facturas);
			$registro_factura = mysql_fetch_array($resultados_facturas);


		
?>
                 <br />

                <div >
                
			  <div class="columnas_fila4_contrato">
                    	<div class="color_fondo_cabecera_contrato times12Azul4">Nº Factura</div>
                       	<div class="color_fondo_datos_contrato alto_factura_periodo arial12"><?=$registro_factura_web["NumeroFactura"]?></div>                                         
                  </div><!--<div class="columnas_fila3_contrato">-->

			  <div class="columnas_fila4_contrato">
                    	<div class="color_fondo_cabecera_contrato times12Azul4">Fecha	</div>
                       	<div class="color_fondo_datos_contrato alto_factura_periodo arial12"><?=fecha_normal($registro_factura_web["FechaFactura"])?></div>                                           
                  </div><!--<div class="columnas_fila3_contrato">-->

			  <div class="columnas_fila4_contrato">
                    	<div class="color_fondo_cabecera_contrato times12Azul4">MES/BIM	</div>
                       	<div class="color_fondo_datos_contrato alto_factura_periodo arial12"><?=$registro_factura_web["Periodo"]?></div>                                                
                  </div><!--<div class="columnas_fila3_contrato">-->

			  <div class="columnas_fila4_contrato">
                    	<div class="color_fondo_cabecera_contrato times12Azul4">Tipo Fact</div>
                       	<div class="color_fondo_datos_contrato alto_factura_periodo arial12"><?
						
						switch ($registro_factura_web["TipoFactura"])
						{
						case 'E':						{$tipofactura="Energía";}						break;
						case 'D':						{$tipofactura="Derechos";}						break;
						case 'V':						{$tipofactura="Varios";}						break;
						default :						{$tipofactura="------";}      					break;
						}
						echo($tipofactura);
						
						
						?></div>
                  </div><!--<div class="columnas_fila3_contrato columna_factura_periodo">-->                    

			  <div class="columnas_fila4_contrato">
                    	<div class="color_fondo_cabecera_contrato times12Azul4">A&ntilde;o</div>
                       	<div class="color_fondo_datos_contrato alto_factura_periodo arial12"><?=$registro_factura_web["Año"]?></div>                                                                     
                  </div><!--<div class="columnas_fila3_contrato">-->                    

			  <div class="columnas_fila4_contrato">
                    	<div class="color_fondo_cabecera_contrato times12Azul4">Tipo Periodo</div>
                       	<div class="color_fondo_datos_contrato alto_factura_periodo arial12">
                        
                        <?
						
						switch ($registro_factura_web["TipoPeriodo"])
						{
						case 'Mens':					{$tipoperiodo="Mensual";}						break;
						case 'Bime':					{$tipoperiodo="Bimensual";}						break;
						case 'Bi1':						{$tipoperiodo="Bimensual";}						break;
						default :						{$tipoperiodo="---------";}      					break;
						}
						echo($tipoperiodo);
						
						
						?>
                        
                        
                        
                        </div>                                                
                  </div><!--<div class="columnas_fila3_contrato">-->                    

					<div class="columnas_fila5_contrato">
                    	<div class="color_fondo_cabecera_contrato times12Azul4">Periodo Lectura</div>
                        <div class="color_fondo_datos_contrato alto_factura_periodo arial12"><?=$registro_factura_web["PeriodoLectura"]?></div>                                                                         
                    </div><!--<div class="columnas_fila3_contrato">-->                                        

              <div class="columnas_fila4_contrato">
                    	<div class="color_fondo_cabecera_contrato times12Azul4">Importe</div>
                        <div class="color_fondo_datos_contrato alto_factura_periodo arial12"><?=$registro_factura_web["Importe"]?><? echo(" ");?></div>                                                                         
                  </div>


                </div><!--<div class="columnas_fila1_larga_contrato">--> 

                    <div class="limpiar"></div>
                    
                   

                                        
          <div class="columnas_fila5_contrato">
                    	<div class="color_fondo_cabecera_contrato times12Azul4">Nombre Titular	</div>
                       	<div class="color_fondo_datos_contrato arial12">
						
						<?php echo($row['TitularNombre']);?>
						
						</div>                                         
                </div><!--<div class="columnas_consumos_informacion_factura">-->
                    
		  <div class="columnas_fila5_contrato">
                    	<div class="color_fondo_cabecera_contrato times12Azul4">Direcci&oacute;n Punto Suministro</div>
                       	<div class="color_fondo_datos_contrato arial12">
						
						<?php echo($row['SuministroCalle']);?>
                        
	</div>                                         
                </div><!--<div class="columnas_consumos_informacion_factura">-->

		  <div class="columnas_fila5_contrato">
                    	<div class="color_fondo_cabecera_contrato times12Azul4">Poblaci&oacute;n Punto Suministro</div>
                       	<div class="color_fondo_datos_contrato arial12">
						
					    <?php echo($row['SuministroCiudad']);?>
                        
    </div>                                         
                </div><!--<div class="columnas_consumos_informacion_factura">-->
                    
		  <div class="columnas_fila5_contrato">
                    	<div class="color_fondo_cabecera_contrato times12Azul4">Fecha Hora Descarga</div>
                       	<div class="color_fondo_datos_contrato arial12">
                        <?php echo($registro_factura_web["FechaDescarga"]);?>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        
						 <?php
						 
											 
						 if(strlen($registro_factura_web["HoraDescarga"])==1)
						 	{echo("0");
							 echo($registro_factura_web["HoraDescarga"]);
							 }
							 else
							 {
						     echo($registro_factura_web["HoraDescarga"]);
							 } 
						 
						 
						
						 
						 if ($registro_factura_web["HoraDescarga"]<>NULL){echo(":");} else {};
						 
						   if(strlen($registro_factura_web["MinutoDescarga"])==1)
						 	{echo("0");
							 echo($registro_factura_web["MinutoDescarga"]);
							 }
							 else
							 {
						    echo($registro_factura_web["MinutoDescarga"]);
							 } 
						 ?>
                         
                        </div>                                         
                </div><!--<div class="columnas_consumos_informacion_factura">-->
                    
   					
                    <div class="limpiar"> </div>
                    
                    <?php 
											
						if (($registro_factura_web["Disponible"])=='N')
						{					
						?>
                        <div class="posicion_boton_imprimir_certificado_consumo">
   	   	  			  <a><img src="../img/oficina/icono_pdf_no.gif" alt="<?= 
					  $oficina_informacion_certificadofacturas4?>" title="<?=$oficina_informacion_certificadofacturas4?>" border="0" /></a>
        			    </div><!--<div class="posicion_boton_imprimir_certificado_consumo">-->
                       
                        
                        	<div class="posicion_texto_imprimir_certificado_consumo">		
                    		<a class="arial12bu">Factura no disponible</a>
                        </div><!--<div class="posicion_boton_imprimir_certificado_consumo">-->
                    
                        
                        
                        <?php 
						
						}
						else {
								if (($registro_factura_web["Descargada"])=='N')
								{
						
						 ?>
                         
							<div class="posicion_boton_imprimir_certificado_consumo">
   	   	  			  <a><img src="../img/oficina/icono_pdf.gif" alt="<?= 
					  $oficina_informacion_certificadofacturas4?>" title="<?=$oficina_informacion_certificadofacturas4?>" border="0" /></a>
        			    </div><!--<div class="posicion_boton_imprimir_certificado_consumo">-->
                       
						<div class="posicion_texto_imprimir_certificado_consumo">		
                    		<a href="facturaweb.php?Registro_Factura_Fecha=<?=$registro_factura_web["FechaFactura"]?>" target="_blank" class="enlace">Imprimir Factura</a>
                        </div><!--<div class="posicion_boton_imprimir_certificado_consumo">-->
                    
     				<?php }
						 else{
					?>
                    
                    	<div class="posicion_boton_imprimir_certificado_consumo">
   	   	  			  <a href="facturaweb.php?Registro_Factura_Fecha=<?=$registro_factura_web["FechaFactura"]?>" target="_blank" class="enlace"><img src="../img/oficina/icono_pdf_descargado.gif" alt="<?= 
					  $oficina_informacion_certificadofacturas4?>" title="<?=$oficina_informacion_certificadofacturas4?>" border="0" /></a>
        			    </div><!--<div class="posicion_boton_imprimir_certificado_consumo">-->
                       
                    
               	<div class="posicion_texto_imprimir_certificado_consumo">		
                   		  <a  href="facturaweb.php?Registro_Factura_Fecha=<?=$registro_factura_web["FechaFactura"]?>" target="_blank" class="arial12b">Factura ya descargada anteriormente</a>
                </div><!--<div class="posicion_boton_imprimir_certificado_consumo">-->
                    <?php
						}
						}?>
     
     
     
                     
<div class="limpiar"></div>
                   <br />
<br />


<?php 
				}//while($row = mysql_fetch_array($result))				  
		   }//if($num_facturas != 0)
		   
//Si no hay facturas en la base de datos mostraremos un mensaje al usuario
		   else
		   {
?>
				<div class="error_no_registros"><?=$error_no_facturas?></div>
<?php		   
		   }//else($num_facturas != 0)
?>		   
              
          </tr>                                                
            
            </table>
	
		    </td>
          </tr>
        </table>
		 
         
     
         
        <div align="center"><br />
  <br />
        </div>

         
                
<!--VERSION VIEJA-->        	
        	
		</div><!--<div class="contenido_seccion_oficina">-->                        
		
        
        <div class="limpiar"></div>
                        	
<!--Por ultimo aparecera el pie de la web-->
<?php 
	include("../includes/pie.php");

//NOTA: EN ESTA OCASION, EL ERROR SE MOSTRARA DESPUES DE LA CARGA DE LA PAGINA, SI NO POR ALGUN MOTIVO EN OCASIONES DESPUES DE DAR EL ERROR CESA LA CARGA DE LA PAGINA
	if(isset($_POST["Submit"]))
	{
//Por ultimo mostraremos el error correspondiente, en el caso de que haya fallado alguna de las verificaciones, o continuaremos con el proceso		
		switch($error)
		{		
//Proceso OK	
			case 0:						
//Antes de realizar el alta en la base de datos, deberemos de preparar los datos que no nos da el formulario
			
																	
				MsgBox($solicitud_ok);
														
			break;
				
//Error por no rellenar los campos obligatorios
			case 1:
				MsgBox($error_rellenar);
			break;								
			
//Error por escribir la fecha incorrectamente
			case 2:
				MsgBox($error_fecha);
			break;									
		
//Error por fechas escritas posterior a la actual
			case 3:
				MsgBox($error_fecha_posterior);
			break;									

//Error por que la fecha de inicio es mayor que la fecha final
			case 4:
				MsgBox($error_fecha_inicio_mayor);
			break;											
			
		}//switch($error)
	}//if(isset($_POST["Submit"]))	
}//else ($_SESSION['usuario'])
?>        
    </div><!--<div id="central_oficina"-->
</body>
</html>
