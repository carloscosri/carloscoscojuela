<?php
/*
NOTAS:

1- ESTA SECCION TOMA LOS DATOS DE LA TABLA FacturasPDF

- ESCRIBE EN LA TABLA SolicitudFacturas

*/

/* NOTA:

 COMO NO SE CIERRA LA CONEXION TRAS CADA CONEXION CON LA BASE DE DATOS, LAS CONSULTAS DEJAN DE FUNCIONAR SI SE ESTABLECE COMO UNICA CONEXION
 LA QUE UTILIZA EL RESTO DE LA WEB, POR LO TANTO HASTA QUE SE HAGA LA MIGRACION COMPLETA DE LOS TEXTOS, COEXISTIRAN LAS DOS CONEXIONES, YA QUE
 SE HA AÑADIDO UNA LLAMADA A LA CONEXION BUENA EN ESTE ARCHIVO.
*/

include("_conexion.php");

//Se incluye el archivo que contiene las verificaciones de datos
include("../includes/verificaciones.php");

//Se incluye el archivo con los textos especifico de esta seccion en el idioma oportuno
//include($_SESSION["directorio_raiz"]."idiomas/".$_SESSION["idioma"]."/oficina_informacion_facturas.php");
include("../idiomas/".$_SESSION["idioma"]."/oficina_datos_titular.php");
include("../idiomas/".$_SESSION["idioma"]."/oficina_informacion_facturas.php");

include("../idiomas/".$_SESSION["idioma"]."/oficina_informacion_certificadofacturas.php");

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<title><?=$titulo_web;?></title>

<!--Se añaden las hojas de estilos genericas que utiliza esta seccion-->
<link href="../css/estilos.css" rel="stylesheet" type="text/css" />
<link href="../css/secciones.css" rel="stylesheet" type="text/css" />
<link href="css/oficina.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_vieja.css" rel="stylesheet" type="text/css" />

<link href="css/oficina_informacion_contratos_y_facturas.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_informacion_facturas.css" rel="stylesheet" type="text/css" />

<link href="css/oficina_informacion_contratos_y_facturas.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_informacion_certificadoconsumos.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_informacion_certificadofacturas.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" media="all" href="scripts/calendar/skins/aqua/theme.css" title="Aqua" />

<script language=javascript type=text/javascript>
function stopRKey(evt) {
var evt = (evt) ? evt : ((event) ? event : null);
var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
if ((evt.keyCode == 13) && (node.type=="text")) {return false;}
}
document.onkeypress = stopRKey; 
</script>

<!-- librería principal del calendario -->
<script type="text/javascript" src="scripts/calendar/calendar.js"></script>

<!-- librería para cargar el lenguaje deseado -->
<script type="text/javascript" src="scripts/calendar/lang/calendar-es.js"></script>

<!-- librería que declara la función Calendar.setup, que ayuda a generar un calendario en unas pocas líneas de código -->
<script type="text/javascript" src="scripts/calendar/calendar-setup.js"></script>




<!--Ahora se añaden las reglas para Internet Explorer. Asi corregiremos las diferencias de visualizacion entre navegadores
Es importante añadirlo despues de las reglas principales, para que se sobreescriban las correcciones pertinentes. En este caso las diferencias
solo son para internet explorer 7-->

<!--[if IE 7]>
	<link href="../css/estilos_ie7.css" rel="stylesheet" type="text/css" />
	<link href="../css/secciones_ie7.css" rel="stylesheet" type="text/css" />    
<![endif]-->

<!--[if IE]>
	<link href="css/tablas_informacionIE.css" rel="stylesheet" type="text/css" />
<![endif]-->

<!--[if IE 7]>
	<link href="css/tablas_informacionIE7.css" rel="stylesheet" type="text/css" />
<![endif]-->

<script language="javascript" type="text/javascript">
function avisa()
{
	alert("OPCION NO DISPONIBLE");
}//function avisa()
//-->
</script>

<?php

//Asignamos los valores por defecto a la fecha de inicio y la fecha de fin, estos cambiaran si el usuario pulsa el boton del formulario habiendo escrito algo
$fecha_inicio="dd-mm-aaaa";
$fecha_fin="dd-mm-aaaa";

if(isset($_POST["Submit"]))
{	
	$fecha_inicio=$_POST["DesdeFecha"];
	$fecha_fin=$_POST["HastaFecha"];
	$email_suministro=$_POST["suministroemail"];
	$observaciones=$_POST["observaciones"];	
	
	$error=comparar_fechas($fecha_inicio,$fecha_fin);		
}//if(isset($_POST["Submit"]))

?>

</head>
	
<body>
	<div id="central_oficina">
    
<?php
//Se comprueba que el usuario se haya identificado, si no es asi se da un aviso y redirigimos a la seccion de identificacion
if ($_SESSION['usuario']=="")
{	
	MsgBox($error_identificacion);
	redirigir("oficina_index.php?id=".$_SESSION["idioma"]);
	
}//if ($_SESSION['usuario'])

//Si el usuario se ha identificado la seccion se ejecutara con normalidad
else
{
//Añadimos los fragmentos comunes, la cabecera, el menu, y el submenu
	include("oficina_fragmentos_comunes.php");
?>
<div class="contenido_seccion_oficina_informacion_contratos_y_facturas">
  <?php if (isset($_SESSION['numeroCliente'])){
					 		$result = mysql_query("SELECT * FROM DatosRegistrados WHERE CodigoCliente = ".$_SESSION['numeroCliente']." ");
							$row = mysql_fetch_array($result);
					 }else{
					 		$result = mysql_query("SELECT * FROM DatosRegistrados WHERE CodigoCliente = -1");
							//Para que no tome ningun cliente pero no de ningun error en la pagina
							$row = mysql_fetch_array($result);
					 }
			?>
  <div class="tipotablacentrado arialblack18Titulo p4">
    <?php 
                $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 325'); 
                $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);
                ?>
  </div>
  <fieldset style="padding-left:0px; padding-top:15px; border-radius:10px;">
    <div> <img src="../img/oficina/info.png" align="left" height="65" />
      <?php 
					$resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 301'); 
					$rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
      <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 572'); 
					$rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
      <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 307'); 
					$rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
    </div>
  </fieldset>
  <!--<div style="margin:auto; text-align:center; margin-top:20px;">
            Seleccione el tipo de Facturas que desee visualizar: &nbsp;
            	<select name="" onchange="MostrarFactura(this.value, '<?=$_SESSION['numeroCliente'];?>', '<?=$_SESSION['idioma'];?>')">
                	<option name="TODAS" value="TODAS">TODAS</option>
                    <option name="ENERGIA" value="ENERGIA">ENERGIA</option>
                    <option name="VARIOS" value="VARIOS">DERECHOS / VARIOS</option>
                </select>
            </div>-->
  <div id="resumenFacturas"> <br />
    <br />
    <?php 
			$consulta_facturas="SELECT * FROM FacturasPDF WHERE NumeroCliente = ".$_SESSION['numeroCliente']." ORDER BY `FechaFactura` DESC LIMIT 0,11";
			$resultados_facturas = mysql_query($consulta_facturas);			
			$num_facturas=mysql_num_rows($resultados_facturas);
			/*if ($num_facturas==0){ echo '<br><br>'; } else {
				if ($num_facturas==1){
					echo ' <p class="p3 top-3 clr-4 aligncenter">A continuacion se muestra la ultima factura: </p>';
				} else {
					echo ' <p class="p3 top-3 clr-4 aligncenter">A continuacion se muestran las ultimas '.$num_facturas.' facturas: </p>';
				}
			}*/
			
			//Si hay facturas se mostrara un resumen de los datos de cada una de ellas		
			//echo 'Facturas Energia	';		
			if($num_facturas != 0)
			{
				while($registro_factura = mysql_fetch_array($resultados_facturas))
				{
?>
    <table style="font-size:10px; border-radius:6px; margin:auto; margin-top:10px; width:620px; text-align:center; background-color: #F2F2F2;border: 1px solid #999999; border-radius: 5px 5px 5px 5px;box-shadow: 0 3px 2px #999999; padding-left: 10px; padding-right: 5px;" class="tablaContratos">
      <tr class="clr-4 aligncenter" style="text-decoration:underline">
        <td><strong>
          <?php 
						   		// Num Factura
                                $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 252'); 
                                $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);
							?>
        </strong></td>
        <td><strong>
          <?php 
								// Fecha
								$resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 254'); 
								$rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);
							?>
        </strong></td>
        <td><strong>
          <?php 
								// Periodo
								$resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 253'); 
								$rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);
							?>
        </strong></td>
        <td><!-- IMPORTE BASE -->
          <strong>
            <?=$oficina_informacion_facturas12?>
          </strong></td>
        <td><!-- IMPORTE IVA -->
          <strong>
            <?=$oficina_informacion_facturas13?>
          </strong></td>
        <td><!-- IMPORTE -->
          <strong>
            <?=$oficina_informacion_facturas14;?>
          </strong></td>
        <td rowspan="2" class="aligncenter" style="padding-left:10px; padding-right:10px;"><a href="imprimir_factura.php?Registro_Factura_Fecha=<?=$registro_factura["FechaFactura"];?>&amp;NumFactura=<?=$registro_factura["NumeroFactura"];?>" title="Descargar" target="_blank"><img src="../img/oficina/descargaFactura.png" /></a></td>
      </tr>
      <tr class="aligncenter">
        <td><?=$registro_factura["NumeroFactura"]?></td>
        <td><?=fecha_normal($registro_factura["FechaFactura"])?></td>
        <td><?=$registro_factura["PeriodoTexto"]?></td>
        <td><?=number_format($registro_factura["ImporteBase"],2,",",".")." &euro;"?></td>
        <td><?=number_format($registro_factura["ImporteIVA"],2,",",".")." &euro;"?></td>
        <td><?=number_format($registro_factura["ImporteFactura"],2,",",".")." &euro;";?></td>
      </tr>
    </table>
    <!--<div class="limpiar"></div>-->
    <?php 
				}//while($row = mysql_fetch_array($result))				  
		   }//if($num_facturas != 0)
		   
		   
		    
			//$consulta_facturas2="SELECT * FROM FacturasVarios WHERE CodCliente = ".$_SESSION['numeroCliente']." ORDER BY `Fecha` DESC LIMIT 0,11";
			//$resultados_facturas2 = mysql_query($consulta_facturas2);			
			//$num_facturas2=mysql_num_rows($resultados_facturas2);
			/*if ($num_facturas2==0){ echo '<br><br>'; } else {
				if ($num_facturas2==1){
					echo ' <p class="p3 top-3 clr-4 aligncenter">A continuacion se muestra la ultima factura: </p>';
				} else {
					echo ' <p class="p3 top-3 clr-4 aligncenter">A continuacion se muestran las ultimas '.$num_facturas2.' facturas: </p>';
				}
			}*/
			
			//Si hay facturas se mostrara un resumen de los datos de cada una de ellas	
			//echo '<br><br>Facturas Varios / Derechos	';	
			if($num_facturas2 != 0)
			{
				while($registro_factura2 = mysql_fetch_array($resultados_facturas2))
				{
?>
    <!--<table style="font-size:10px; border-radius:6px; margin:auto; margin-top:10px; width:650px; text-align:center" class="tablaContratos">
                      <tr class="clr-4 aligncenter" style="text-decoration:underline">
                        <td width="75">
						   <strong>N&ordm; Factura</strong>
                         </td>
                         <td width="75">
                        	<strong>N&ordm; Serie</strong>
                        </td>
                         <td width="75">
                        	<strong>Fecha</strong>
                        </td>
                        <td width="85">
                        	<strong>Tipo</strong>
                         </td>
                         <td width="75">
                         	<strong><?=$oficina_informacion_facturas12?></strong>
                         </td>
                         <td width="75">
                            <strong><?=$oficina_informacion_facturas13?></strong>
                         </td>
                         <td width="75">
                         	<strong><?=$oficina_informacion_facturas14;?></strong>
                         </td>
                         <td rowspan="2" class="aligncenter" style="padding-left:10px; padding-right:10px;">
                         <?php
						 $FechaFactura = explode(' ',$registro_factura2["Fecha"]);
						 ?>
                         	<a href="imprimir_factura_varios.php?Registro_Factura_Fecha=<?=$FechaFactura[0];?>&NumFactura=<?=$registro_factura2["NumFactura"];?>" title="Descargar" target="_blank"><img src="../img/oficina/descargaFactura.png"></a>
                         </td>
                      </tr>   
                           
            		<tr class="aligncenter">
                    	<td>
                       	 	<?=$registro_factura2["NumFactura"]?>
                        </td>
                        <td>
                       	 	<?=$registro_factura2["NumSerie"]?>
                        </td>
                        <td>
                        	<?php
                            	$fechaPartes = explode ('-', $registro_factura2["Fecha"]);
								$dia = explode(' ',$fechaPartes[2]);
								echo $dia[0].'-'.$fechaPartes[1].'-'.$fechaPartes[0];
							?>
                     	</td>
                        <td>
                        	<?=$registro_factura2["Tipo"]?>
                        </td>
                        <td>
                        	<?=number_format($registro_factura2["ImporteBase"],2,",",".")." &euro;"?>
                        </td>
                        <td>
                        	<?=number_format($registro_factura2["Iva1"]+$registro_factura2["Iva2"]+$registro_factura2["Iva3"],2,",",".")." &euro;"?>
                        </td>
                        <td>
                        	<?=number_format($registro_factura2["TotalFactura"],2,",",".")." &euro;";?>
                        </td>
                    </tr>
                    </table>

            <!--<div class="limpiar"></div>-->
    <?php 
				}//while($row = mysql_fetch_array($result))				  
		   }//if($num_facturas != 0)
		   
		   
//Si no hay facturas en la base de datos mostraremos un mensaje al usuario
		   else
		   {
?>
    <!--<span class="error_no_registros"><?=$error_no_facturas?></span>-->
    <?php		   
		   }//else($num_facturas != 0)
?>
    <div class="limpiar"></div>
    <!--      
<form action="oficina_informacion_facturas.php?id=<?=$_SESSION["idioma"]?>" method="post" name="copias_certificados" id="copias_certificados" class="top-3 p4">
    <legend class="arialblack14">
       <?php 
	   $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 241'); 
	   $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);
	   ?>
    </legend>
<table class="tabla_formulario_solicitudes aligncenter">
	<tr>
    	<td colspan="4" class="arial12">
        	<div align="justify">
               <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 306'); 
			   $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
               <br /><br />
            </div>
        </td>
    </tr>
    <tr>
    	<td width="25%">
        	<?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 243'); 
			$rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>*
        </td>
        <td width="25%">
        	<input id="sel3" name="DesdeFecha"  type="text" class="textfield" value="<?=$fecha_inicio?>" style="width:126px" />
            <input type="button" id="lanzador3" value="..." />
           
            <script type="text/javascript">
            Calendar.setup({
            inputField : "sel3", // id del campo de texto
            ifFormat : "%d-%m-%Y", // formato de la fecha que se escriba en el campo de texto
            button : "lanzador3" // el id del bot&oacute;n que lanzar&aacute; el calendario

            });
            </script>
        </td>
        <td width="25%">
        	<?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 244'); 
			$rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>*
        </td>
        <td width="25%">
        <input id="sel4" name="HastaFecha" type="text" class="textfield" value="<?=$fecha_fin?>" style="width:126px" />
        <input type="button" id="lanzador4" value="..." />
        
        <script type="text/javascript">
        Calendar.setup({
        inputField : "sel4", // id del campo de texto
        ifFormat : "%d-%m-%Y", // formato de la fecha que se escriba en el campo de texto
        button : "lanzador4" // el id del bot&oacute;n que lanzar&aacute; el calendario
        });
        </script>
        </td>
    </tr>
    <tr><td colspan="4"><br></td></tr>
    <tr>
    	<td>
			<?php
                $result = mysql_query("SELECT * FROM DatosRegistrados WHERE CodigoCliente = ".$_SESSION['numeroCliente']." ");
                $row = mysql_fetch_array($result);
                $suministroemail=$row["SuministroEmail"];	
                        
                $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 129'); 
                $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);
            ?>
        </td>
    	<td colspan="3">
            <input name="suministroemail" value="<?=$suministroemail?>"  type="text" class="textfield" id="suministroemail" style="width:500px"/>
        </td>
    </tr>
    <tr><td colspan="4"><br></td></tr>
    <tr>
    	<td colspan="4">
        	
                <legend class="arialblack14"> 
                  <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto =152'); 
				  $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                </legend>
                <textarea name="observaciones" cols="54" rows="5" id="observaciones" class="arial14"></textarea>                          
          
        </td>
    </tr>
</table>
</form> -->
    <br />
    <br />
  </div>
  <!--<div class="contenido_seccion_oficina">-->
</div>
<!--<div class="contenido_seccion_oficina">-->                        
		
        
      <div class="limpiar"></div>
                        	
<!--Por ultimo aparecera el pie de la web-->
<?php 
	include("../includes/pie.php");

//NOTA: EN ESTA OCASION, EL ERROR SE MOSTRARA DESPUES DE LA CARGA DE LA PAGINA, SI NO POR ALGUN MOTIVO EN OCASIONES DESPUES DE DAR EL ERROR CESA LA CARGA DE LA PAGINA
	if(isset($_POST["Submit"]))
	{
//Por ultimo mostraremos el error correspondiente, en el caso de que haya fallado alguna de las verificaciones, o continuaremos con el proceso		
		switch($error)
		{		
//Proceso OK	
			case 0:						
//Antes de realizar el alta en la base de datos, deberemos de preparar los datos que no nos da el formulario
			
				$insertar_solicitud_factura="INSERT INTO `SolicitudFacturas` (`NumeroCliente` ,`FechaInicial` ,`FechaFinal` ,`FechaRegistro` ,
`HoraRegistro`,`email` ,`Observaciones`)VALUES ('".$_SESSION['numeroCliente']."', '".fecha_mysql($fecha_inicio)."', '".fecha_mysql($fecha_fin)."', '".date("Y-m-d")."', '".date("Y-m-d H:i:s")."', '".$email_suministro."', '".$observaciones."')";
								
				$ejecutar_solicitud_facturas=mysql_query($insertar_solicitud_factura);
															
				MsgBox($solicitud_ok);
				include("../includes/email_solicitud.php");						
			break;
				
//Error por no rellenar los campos obligatorios
			case 1:
				MsgBox($error_rellenar);
			break;								
			
//Error por escribir la fecha incorrectamente
			case 2:
				MsgBox($error_fecha);
			break;									
		
//Error por fechas escritas posterior a la actual
			case 3:
				MsgBox($error_fecha_posterior);
			break;									

//Error por que la fecha de inicio es mayor que la fecha final
			case 4:
				MsgBox($error_fecha_inicio_mayor);
			break;											
			
		}//switch($error)
	}//if(isset($_POST["Submit"]))	
}//else ($_SESSION['usuario'])
?>        
    </div><!--<div id="central_oficina"-->
</body>
</html>
