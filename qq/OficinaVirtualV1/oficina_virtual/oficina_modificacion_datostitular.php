<?php
/*
NOTAS:

1- ESTA SECCION TOMA LOS DATOS DE LA TABLA DatosRegistrados

2- ESCRIBE EN LA TABLA DatosModificados

*/

/* NOTA:

 COMO NO SE CIERRA LA CONEXION TRAS CADA CONEXION CON LA BASE DE DATOS, LAS CONSULTAS DEJAN DE FUNCIONAR SI SE ESTABLECE COMO UNICA CONEXION
 LA QUE UTILIZA EL RESTO DE LA WEB, POR LO TANTO HASTA QUE SE HAGA LA MIGRACION COMPLETA DE LOS TEXTOS, COEXISTIRAN LAS DOS CONEXIONES, YA QUE
 SE HA AÑADIDO UNA LLAMADA A LA CONEXION BUENA EN ESTE ARCHIVO.
*/

include("_conexion.php");

//Se incluye el archivo que contiene las verificaciones de datos
include("../includes/verificaciones.php");

//Como tienen textos en comun, para evitar el crecimiento innecesario de la base de datos, se incluye el archivo de textos de las secciones de datos del titular y del alta, ya que la mayoria de los textos son comunes
//include($_SESSION["directorio_raiz"]."idiomas/".$_SESSION["idioma"]."/oficina_datos_titular.php");
//include($_SESSION["directorio_raiz"]."idiomas/".$_SESSION["idioma"]."/oficina_contratacion_alta.php");

//include($_SESSION["directorio_raiz"]."idiomas/".$_SESSION["idioma"]."/oficina_modificacion_datostitular.php");

include("../idiomas/".$_SESSION["idioma"]."/oficina_datos_titular.php");
include("../idiomas/".$_SESSION["idioma"]."/oficina_contratacion_alta.php");

include("../idiomas/".$_SESSION["idioma"]."/oficina_modificacion_datostitular.php");

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<title><?=$titulo_web;?></title>
<script type="text/javascript" src="../includes/jquery.min.js"></script> <!-- JQuery -->
<script type="text/javascript" src="../includes/jQuery.validity.js"></script> <!-- Validity -->
<!--Se añaden las hojas de estilos genericas que utiliza esta seccion-->
<link href="../css/estilos.css" rel="stylesheet" type="text/css" />
<link href="../css/secciones.css" rel="stylesheet" type="text/css" />
<link href="css/validacion.css" rel="stylesheet" type="text/css" />
<link href="css/oficina.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_vieja.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_modificacion_titular.css" rel="stylesheet" type="text/css" />

<!--Ahora se añaden las reglas para Internet Explorer. Asi corregiremos las diferencias de visualizacion entre navegadores
Es importante añadirlo despues de las reglas principales, para que se sobreescriban las correcciones pertinentes. En este caso las diferencias
solo son para internet explorer 7-->

<!--[if IE 7]>
	<link href="../css/estilos_ie7.css" rel="stylesheet" type="text/css" />
	<link href="../css/secciones_ie7.css" rel="stylesheet" type="text/css" />
<![endif]-->
<script language=javascript type=text/javascript>
function stopRKey(evt) {
var evt = (evt) ? evt : ((event) ? event : null);
var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
if ((evt.keyCode == 13) && (node.type=="text")) {return false;}
}
document.onkeypress = stopRKey;
</script>
<style>
input:-moz-read-only { /* For Firefox */
    background-color: #E3E3E3;
}

input:read-only {
    background-color: #E3E3E3;
}
</style>
<script language="javascript" type="text/javascript">


function datos_bancarios()
{
//Esta variable almacenara un indice que representara la forma de pago seleccionada:
//1 = Domiciliacion
//2 = No Domiciliado

	var forma_pago;

	forma_pago=document.form_cambios_datos_titular.FormaPago.value;

	switch(forma_pago)
	{
//Si la forma de pago es domiciliacion bancaria, se habilitaran los campos correspondientes
		case "1":
			document.form_cambios_datos_titular.EntidadBancaria.readOnly = false;

			document.form_cambios_datos_titular.banco.readOnly = true;
			document.form_cambios_datos_titular.sucursal.readOnly = true;
			document.form_cambios_datos_titular.sucursal2.readOnly = true;
			document.form_cambios_datos_titular.sucursal3.readOnly = true;

			document.form_cambios_datos_titular.iban1.readOnly = false;
			document.form_cambios_datos_titular.iban2.readOnly = false;
			document.form_cambios_datos_titular.iban3.readOnly = false;
			document.form_cambios_datos_titular.iban4.readOnly = false;
			document.form_cambios_datos_titular.iban5.readOnly = false;
			document.form_cambios_datos_titular.iban6.readOnly = false;

			document.form_cambios_datos_titular.iban7.readOnly = true;
		break;

//Si la forma de pago es no domicialiacion, se dehabilitaran los campos correspondientes y se eliminaran sus valores
		case "2":
			document.form_cambios_datos_titular.EntidadBancaria.selectedIndex=1;
			document.form_cambios_datos_titular.banco.value = "";
			document.form_cambios_datos_titular.sucursal.value = "";
			document.form_cambios_datos_titular.sucursal2.value = "";
			document.form_cambios_datos_titular.sucursal3.value = "";
			document.form_cambios_datos_titular.entidad3.value = "";
			document.form_cambios_datos_titular.iban1.value = "";
			document.form_cambios_datos_titular.iban2.value = "";
			document.form_cambios_datos_titular.iban3.value = "";
			document.form_cambios_datos_titular.iban4.value = "";
			document.form_cambios_datos_titular.iban5.value = "";
			document.form_cambios_datos_titular.iban6.value = "";
			document.form_cambios_datos_titular.iban7.value = "";


			document.form_cambios_datos_titular.EntidadBancaria.readOnly = true;
			document.form_cambios_datos_titular.banco.readOnly = true;
			document.form_cambios_datos_titular.sucursal.readOnly = true;
			document.form_cambios_datos_titular.sucursal2.readOnly = true;
			document.form_cambios_datos_titular.sucursal3.readOnly = true;
			document.form_cambios_datos_titular.iban1.readOnly = true;
			document.form_cambios_datos_titular.iban2.readOnly = true;
			document.form_cambios_datos_titular.iban3.readOnly = true;
			document.form_cambios_datos_titular.iban4.readOnly = true;
			document.form_cambios_datos_titular.iban5.readOnly = true;
			document.form_cambios_datos_titular.iban6.readOnly = true;
			document.form_cambios_datos_titular.iban7.readOnly = true;
		break;
	}//switch(forma_pago)
}//function datos_bancarios()

//Esta funcion establece el codigo de la entidad bancaria en el numero de cuenta
function asignar_codigo_entidad_bancaria()
{
	var entidad_bancaria=document.form_cambios_datos_titular.EntidadBancaria.value;
//Dividimos el valor recibido de la entidad bancaria por el simbolo mas. Esta variable contendra en la primera posicion el nombre de la entidad bancaria y en la segunda el codigo
	var partes_entidad_bancaria= entidad_bancaria.split('+');

	document.form_cambios_datos_titular.banco.value=partes_entidad_bancaria[1];
	document.form_cambios_datos_titular.sucursal.value = "";
	document.form_cambios_datos_titular.sucursal2.value = "";
	document.form_cambios_datos_titular.sucursal3.value = "";
	document.form_cambios_datos_titular.iban2.value=partes_entidad_bancaria[1];
	document.form_cambios_datos_titular.iban1.value = "";
	document.form_cambios_datos_titular.iban3.value = "";
	document.form_cambios_datos_titular.iban4.value = "";
	document.form_cambios_datos_titular.iban5.value = "";
	document.form_cambios_datos_titular.iban6.value = "";
	document.form_cambios_datos_titular.iban7.value = "";
}//function asignar_codigo_entidad_bancaria()

function Load()
{

	document.form_cambios_datos_titular.EntidadBancaria.readOnly = false;

			document.form_cambios_datos_titular.banco.readOnly = true;
			document.form_cambios_datos_titular.sucursal.readOnly = true;
			document.form_cambios_datos_titular.sucursal2.readOnly = true;
			document.form_cambios_datos_titular.sucursal3.readOnly = true;

			document.form_cambios_datos_titular.iban1.readOnly = false;
			document.form_cambios_datos_titular.iban2.readOnly = false;
			document.form_cambios_datos_titular.iban3.readOnly = false;
			document.form_cambios_datos_titular.iban4.readOnly = false;
			document.form_cambios_datos_titular.iban5.readOnly = false;
			document.form_cambios_datos_titular.iban6.readOnly = false;

			document.form_cambios_datos_titular.iban7.readOnly = true;

	var numero_banco=document.form_cambios_datos_titular.banco.value;
	var numero_banco2=document.form_cambios_datos_titular.iban2.value;
	var num_bancos_disponibles=document.form_cambios_datos_titular.EntidadBancaria.options.length;

//Esta variable almacenara el numero de entidades bancarias que se han comprobado
			var num_bancos_comprobados=0;

//Ahora seleccionaremos la entidad bancaria en el combobox. Como los nombres de las entidades bancarias no cambian con los idiomas, podemos realizar la busqueda directamente en los valores del combobox
			while (num_bancos_comprobados<num_bancos_disponibles)
			{
//Como el valor que se pasa desde el combo de bancos es el nombre de la entidad y el codigo de la entidad bancaria separados por un signo "+", para realizar la comparacion, deberemos separar ambas partes
				var partes_banco= document.form_cambios_datos_titular.EntidadBancaria.options[num_bancos_comprobados].value.split('+');

//Cuando encontramos la tarifa buscada, se selecciona en el combo de tarifas y salimos de la funcion
				if(partes_banco[1]==numero_banco && partes_banco[1]==numero_banco2)
				{
					document.form_cambios_datos_titular.entidad3.value=partes_banco[0];

				}//if(document.form_cambios_datos_titular.EntidadBancaria.options[num_bancos_comprobados].value==banco)

				num_bancos_comprobados++;
			}//while (num_bancos_comprobados<num_bancos_disponibles)<br />

			cambiar_iban();
}

function asignar_banco2()
{

	var numero_banco=document.form_cambios_datos_titular.banco.value;
	var numero_banco2=document.form_cambios_datos_titular.iban2.value;
	var num_bancos_disponibles=document.form_cambios_datos_titular.EntidadBancaria.options.length;

//Esta variable almacenara el numero de entidades bancarias que se han comprobado
			var num_bancos_comprobados=0;

//Ahora seleccionaremos la entidad bancaria en el combobox. Como los nombres de las entidades bancarias no cambian con los idiomas, podemos realizar la busqueda directamente en los valores del combobox
			while (num_bancos_comprobados<num_bancos_disponibles)
			{
//Como el valor que se pasa desde el combo de bancos es el nombre de la entidad y el codigo de la entidad bancaria separados por un signo "+", para realizar la comparacion, deberemos separar ambas partes
				var partes_banco= document.form_cambios_datos_titular.EntidadBancaria.options[num_bancos_comprobados].value.split('+');

//Cuando encontramos la tarifa buscada, se selecciona en el combo de tarifas y salimos de la funcion
				if(partes_banco[1]==numero_banco && partes_banco[1]==numero_banco2)
				{
					document.form_cambios_datos_titular.entidad3.value=partes_banco[0];

				}//if(document.form_cambios_datos_titular.EntidadBancaria.options[num_bancos_comprobados].value==banco)

				num_bancos_comprobados++;
			}//while (num_bancos_comprobados<num_bancos_disponibles)<br />

			cambios('cambiodomiciliacion');
}
// creamos la funcion para convertir la cuenta bancaria a formato iban
function calcular_iban(numero, modulo)
{
   var VoyPor = 0;
   var resto = 0;
   var dividendo;
  /* while($VoyPor <= strlen($numero)){
		$dividendo = $resto.substr($numero,$VoyPor,1);
		$resto = $dividendo % $modulo;
		$VoyPor = $VoyPor + 1;
   }*/
   do{	dividendo = String(resto) + String(numero.substr(VoyPor,1));
   //alert(dividendo);
		resto = dividendo % modulo;
		VoyPor = VoyPor + 1;
	}while(VoyPor <= numero.length);
   return resto;
} ;

//Funcion para cambiar el iban cuando cambias la cuenta bancaria
function cambiar_iban()
{
//contatenamos la cuenta bancaria
	var cuenta_bancaria=document.form_cambios_datos_titular.banco.value + document.form_cambios_datos_titular.sucursal.value + document.form_cambios_datos_titular.sucursal2.value + document.form_cambios_datos_titular.sucursal3.value;

	if ((cuenta_bancaria != "") && (cuenta_bancaria.length ==20)){

		var DigControl = 98 - calcular_iban(cuenta_bancaria + "142800",97);
		if(DigControl<10){
		DigControl = '0' + DigControl;
}
		var Iban= "ES"+DigControl+cuenta_bancaria;
			//alert(Iban);
	}
	cuenta_bancaria1 = cuenta_bancaria.substr(0,4);
	cuenta_bancaria2 = cuenta_bancaria.substr(4,4);
	cuenta_bancaria3 = cuenta_bancaria.substr(8,4);
	cuenta_bancaria4 = cuenta_bancaria.substr(12,4);
	cuenta_bancaria5 = cuenta_bancaria.substr(16,4);
	document.form_cambios_datos_titular.iban1.value="ES"+DigControl;
	document.form_cambios_datos_titular.iban2.value= cuenta_bancaria1;
	document.form_cambios_datos_titular.iban3.value= cuenta_bancaria2;
	document.form_cambios_datos_titular.iban4.value= cuenta_bancaria3;
	document.form_cambios_datos_titular.iban5.value= cuenta_bancaria4;
	document.form_cambios_datos_titular.iban6.value= cuenta_bancaria5;
	cambios('cambiodomiciliacion');
}

function cambiar_iban2()
{
//contatenamos la cuenta bancaria
	var cuenta_bancaria=document.form_cambios_datos_titular.banco.value + document.form_cambios_datos_titular.sucursal.value + document.form_cambios_datos_titular.sucursal2.value + document.form_cambios_datos_titular.sucursal3.value;

	if ((cuenta_bancaria != "") && (cuenta_bancaria.length ==20)){

		var DigControl = 98 - calcular_iban(cuenta_bancaria + "142800",97);
		if(DigControl<10){
		DigControl = '0' + DigControl;
}
		var Iban= "ES"+DigControl+cuenta_bancaria;
			//alert(Iban);
	}
	cuenta_bancaria1 = cuenta_bancaria.substr(0,4);
	cuenta_bancaria2 = cuenta_bancaria.substr(4,4);
	cuenta_bancaria3 = cuenta_bancaria.substr(8,4);
	cuenta_bancaria4 = cuenta_bancaria.substr(12,4);
	cuenta_bancaria5 = cuenta_bancaria.substr(16,4);
	document.form_cambios_datos_titular.iban1.value="ES"+DigControl;
	document.form_cambios_datos_titular.iban2.value= cuenta_bancaria1;
	document.form_cambios_datos_titular.iban3.value= cuenta_bancaria2;
	document.form_cambios_datos_titular.iban4.value= cuenta_bancaria3;
	document.form_cambios_datos_titular.iban5.value= cuenta_bancaria4;
	document.form_cambios_datos_titular.iban6.value= cuenta_bancaria5;
	asignar_banco2();
}

function cambiar_cb2()
{
	var cee=document.form_cambios_datos_titular.BancoExtranjero.value;
	if (cee!="Si"){
//contatenamos la cuenta bancaria
	var iban=document.form_cambios_datos_titular.iban2.value + document.form_cambios_datos_titular.iban3.value + document.form_cambios_datos_titular.iban4.value + document.form_cambios_datos_titular.iban5.value + document.form_cambios_datos_titular.iban6.value;

	iban1 = iban.substr(0,4);
	iban2 = iban.substr(4,4);
	iban3 = iban.substr(8,2);
	iban4 = iban.substr(10,10);
	document.form_cambios_datos_titular.banco.value=iban1;
	document.form_cambios_datos_titular.sucursal.value= iban2;
	document.form_cambios_datos_titular.sucursal2.value= iban3;
	document.form_cambios_datos_titular.sucursal3.value= iban4;
	asignar_banco2();
	;}else
	{
		};
}
function cambiar_cb()
{
	var cee=document.form_cambios_datos_titular.BancoExtranjero.value;
	//Si el banco no es extranjero
	if (cee!="Si"){
//contatenamos la cuenta bancaria
	var iban=document.form_cambios_datos_titular.iban2.value + document.form_cambios_datos_titular.iban3.value + document.form_cambios_datos_titular.iban4.value + document.form_cambios_datos_titular.iban5.value + document.form_cambios_datos_titular.iban6.value;

	iban1 = iban.substr(0,4);
	iban2 = iban.substr(4,4);
	iban3 = iban.substr(8,2);
	iban4 = iban.substr(10,10);
	document.form_cambios_datos_titular.banco.value=iban1;
	document.form_cambios_datos_titular.sucursal.value= iban2;
	document.form_cambios_datos_titular.sucursal2.value= iban3;
	document.form_cambios_datos_titular.sucursal3.value= iban4;
	cambios('cambiodomiciliacion');
	;}else
	{
		};
}

function tipo()
{
	var cee=document.form_cambios_datos_titular.BancoExtranjero.value;
	var Iban = document.getElementById("radioIban").checked;
	if (Iban && cee!="Si"){
			document.form_cambios_datos_titular.iban1.readOnly = false;
			document.form_cambios_datos_titular.iban2.readOnly = false;
			document.form_cambios_datos_titular.iban3.readOnly = false;
			document.form_cambios_datos_titular.iban4.readOnly = false;
			document.form_cambios_datos_titular.iban5.readOnly = false;
			document.form_cambios_datos_titular.iban6.readOnly = false;
			document.form_cambios_datos_titular.iban7.readOnly = true;
			document.form_cambios_datos_titular.banco.readOnly = true;
			document.form_cambios_datos_titular.sucursal.readOnly = true;
			document.form_cambios_datos_titular.sucursal2.readOnly = true;
			document.form_cambios_datos_titular.sucursal3.readOnly = true;
			document.form_cambios_datos_titular.BancoExtranjero.disabled = false;
	}else if (Iban && cee=="Si"){
			document.form_cambios_datos_titular.iban1.readOnly = false;
			document.form_cambios_datos_titular.iban2.readOnly = false;
			document.form_cambios_datos_titular.iban3.readOnly = false;
			document.form_cambios_datos_titular.iban4.readOnly = false;
			document.form_cambios_datos_titular.iban5.readOnly = false;
			document.form_cambios_datos_titular.iban6.readOnly = false;
			document.form_cambios_datos_titular.iban7.readOnly = false;
			document.form_cambios_datos_titular.banco.readOnly = true;
			document.form_cambios_datos_titular.sucursal.readOnly = true;
			document.form_cambios_datos_titular.sucursal2.readOnly = true;
			document.form_cambios_datos_titular.sucursal3.readOnly = true;
			document.form_cambios_datos_titular.BancoExtranjero.disabled = false;
	}else{
			document.form_cambios_datos_titular.iban1.readOnly = true;
			document.form_cambios_datos_titular.iban2.readOnly = true;
			document.form_cambios_datos_titular.iban3.readOnly = true;
			document.form_cambios_datos_titular.iban4.readOnly = true;
			document.form_cambios_datos_titular.iban5.readOnly = true;
			document.form_cambios_datos_titular.iban6.readOnly = true;
			document.form_cambios_datos_titular.iban7.readOnly = true;
		document.form_cambios_datos_titular.banco.readOnly = false;
			document.form_cambios_datos_titular.sucursal.readOnly = false;
			document.form_cambios_datos_titular.sucursal2.readOnly = false;
			document.form_cambios_datos_titular.sucursal3.readOnly = false;
			document.form_cambios_datos_titular.BancoExtranjero.disabled = true;
	}
	}

function cee()
{
	var cee=document.form_cambios_datos_titular.BancoExtranjero.value;

	if (cee=="Si"){

			document.form_cambios_datos_titular.iban7.readOnly = false;
			document.form_cambios_datos_titular.banco.readOnly = true;
			document.form_cambios_datos_titular.sucursal.readOnly = true;
			document.form_cambios_datos_titular.sucursal2.readOnly = true;
			document.form_cambios_datos_titular.sucursal3.readOnly = true;
			document.form_cambios_datos_titular.iban1.value = "";
			document.form_cambios_datos_titular.iban2.value = "";
			document.form_cambios_datos_titular.iban3.value = "";
			document.form_cambios_datos_titular.iban4.value = "";
			document.form_cambios_datos_titular.iban5.value = "";
			document.form_cambios_datos_titular.iban6.value = "";
			document.form_cambios_datos_titular.iban7.value = "";
			document.form_cambios_datos_titular.iban1.focus();

	}else{

		document.form_cambios_datos_titular.iban7.readOnly = true;
		document.form_cambios_datos_titular.banco.readOnly = true;
			document.form_cambios_datos_titular.sucursal.readOnly = true;
			document.form_cambios_datos_titular.sucursal2.readOnly = true;
			document.form_cambios_datos_titular.sucursal3.readOnly = true;

	}
		cambios('cambiodomiciliacion');
	}

//Esta funcion hace que los textos almacenados en la base de datos para los datos bancarios marquen la opcion oportuna en los combos
function dar_valor_datos_bancarios(forma_pago, banco, idioma) //forma_pago
{
	switch(forma_pago)
	{
//Si la forma de pago es la domicialiacion la seleccionaremos en el combobox y habilitaremos los campos de entidad bancaria y la cuenta
		case "Domiciliacion Bancaria":
			document.form_cambios_datos_titular.FormaPago.selectedIndex=1;
			document.form_cambios_datos_titular.EntidadBancaria.readOnly = false;
			document.form_cambios_datos_titular.banco.readOnly = false;
			document.form_cambios_datos_titular.sucursal.readOnly = false;
			document.form_cambios_datos_titular.sucursal2.readOnly = false;
			document.form_cambios_datos_titular.sucursal3.readOnly = false;
			document.form_cambios_datos_titular.iban1.readOnly = false;
			document.form_cambios_datos_titular.iban2.readOnly = false;
			document.form_cambios_datos_titular.iban3.readOnly = false;
			document.form_cambios_datos_titular.iban4.readOnly = false;
			document.form_cambios_datos_titular.iban5.readOnly = false;
			document.form_cambios_datos_titular.iban6.readOnly = false;
			document.form_cambios_datos_titular.iban7.readOnly = false;

//Esta variable almacenara el numero total de entidades bancarias disponibles
			var num_bancos_disponibles=document.form_cambios_datos_titular.EntidadBancaria.options.length;

//Esta variable almacenara el numero de entidades bancarias que se han comprobado
			var num_bancos_comprobados=0;

//Ahora seleccionaremos la entidad bancaria en el combobox. Como los nombres de las entidades bancarias no cambian con los idiomas, podemos realizar la busqueda directamente en los valores del combobox
			while (num_bancos_comprobados<num_bancos_disponibles)
			{
//Como el valor que se pasa desde el combo de bancos es el nombre de la entidad y el codigo de la entidad bancaria separados por un signo "+", para realizar la comparacion, deberemos separar ambas partes
				var partes_banco= document.form_cambios_datos_titular.EntidadBancaria.options[num_bancos_comprobados].value.split('+');

//Cuando encontramos la tarifa buscada, se selecciona en el combo de tarifas y salimos de la funcion
				if(partes_banco[0]==banco)
				{
					document.form_cambios_datos_titular.EntidadBancaria.selectedIndex=num_bancos_comprobados;
				}//if(document.form_cambios_datos_titular.EntidadBancaria.options[num_bancos_comprobados].value==banco)

				num_bancos_comprobados++;
			}//while (num_bancos_comprobados<num_bancos_disponibles)

		break;

//Si la forma de pago es domicializacion, simplemente la seleccionaremos en el combobox
		case "No Domiciliado":
			document.form_cambios_datos_titular.FormaPago.selectedIndex=1;
		break;
	}//switch(forma_pago)

//En cualquiera de los casos le damos valor al idioma de la factura
	switch(idioma)
	{
		case "Castellano":
			document.form_cambios_datos_titular.IdiomaFactura.selectedIndex=0;
		break;

		case "Catalán":
			document.form_cambios_datos_titular.IdiomaFactura.selectedIndex=1;
		break;

		case "Euskera":
			document.form_cambios_datos_titular.IdiomaFactura.selectedIndex=2;
		break;

		case "Gallego":
			document.form_cambios_datos_titular.IdiomaFactura.selectedIndex=3;
		break;

		case "Bable (Asturias)":
			document.form_cambios_datos_titular.IdiomaFactura.selectedIndex=4;
		break;

	}//switch(idioma)

}//function dar_valor_datos_bancarios()

function cambios(campo){
	eval("document.form_cambios_datos_titular."+campo+".checked=1");
}
-->

</script>
<script type="text/javascript">
var control;


$(function() {
        $("form#form_cambios_datos_titular").validity(function() {
            $("#banco")
				.require("El campo BANCO es obligatorio")
				.nonHtml ("Codigo HTML no permitido")
				.maxLength(4, "El campo BANCO debe tener 4 digitos")
				.minLength( 4, "El campo BANCO debe tener 4 digitos")

			$("#sucursal")
				.require("El campo SUCURSAL es obligatorio")
				.nonHtml ("Codigo HTML no permitido")
				.maxLength(4, "El campo SUCURSAL debe tener 4 digitos")
				.minLength( 4, "El campo SUCURSAL debe tener 4 digitos")

			$("#sucursal2")
				.require("El CAMPO DE CONTROL es obligatorio")
				.nonHtml ("Codigo HTML no permitido")
				.maxLength(2, "El CAMPO DE CONTROL debe tener 2 digitos")
				.minLength( 2, "El CAMPO DE CONTROL debe tener 2 digitos")

			$("#sucursal3")
				.require("El campo CUENTA es obligatorio")
				.nonHtml ("Codigo HTML no permitido")
				.maxLength(10, "El campo CUENTA debe tener 10 digitos")
				.minLength( 10, "El campo CUENTA debe tener 10 digitos")
			var cee=document.form_cambios_datos_bancarios.BancoExtranjero.value;
	if (cee!="Si"){
			$("#iban1")
				.require("El campo IBAN es obligatorio")
				.match(/^ES\d{2}$/, "El primer campo del IBAN debe tener formato ESXX.")
				.nonHtml ("Codigo HTML no permitido")
				.maxLength(4, "El primer campo del IBAN debe tener 4 digitos")
				.minLength( 4, "El primer campo del IBAN debe tener 4 digitos");


			$("#iban2")
				.require("El campo IBAN es obligatorio")
				.match("number","El segundo campo del IBAN debe ser numerico")
				.nonHtml ("Codigo HTML no permitido")
				.maxLength(4, "El segundo campo del IBAN debe tener 4 digitos")
				.minLength( 4, "El segundo campo del IBAN debe tener 4 digitos");

			$("#iban3")
				.require("El campo IBAN es obligatorio")
				.match("number","El tercer campo del IBAN debe ser numerico")
				.nonHtml ("Codigo HTML no permitido")
				.maxLength(4, "El tercer campo del IBAN debe tener 4 digitos")
				.minLength( 4, "El tercer campo del IBAN debe tener 4 digitos");

			$("#iban4")
				.require("El campo IBAN es obligatorio")
				.match("number","El cuarto campo del IBAN debe ser numerico")
				.nonHtml ("Codigo HTML no permitido")
				.maxLength(4, "El cuarto campo del IBAN debe tener 4 digitos")
				.minLength( 4, "El cuarto campo del IBAN debe tener 4 digitos");

			$("#iban5")
				.require("El campo IBAN es obligatorio")
				.match("number","El quinto campo del IBAN debe ser numerico")
				.nonHtml ("Codigo HTML no permitido")
				.maxLength(4, "El quinto campo del IBAN debe tener 4 digitos")
				.minLength( 4, "El quinto campo del IBAN debe tener 4 digitos");

			$("#iban6")
				.require("El campo IBAN es obligatorio")
				.match("number","El sexto campo del IBAN debe ser numerico")
				.nonHtml ("Codigo HTML no permitido")
				.maxLength(4, "El sexto campo del IBAN debe tener 4 digitos")
				.minLength( 4, "El sexto campo del IBAN debe tener 4 digitos");
	}
			$("#entidad2")
				.require("El campo BANCO no es correcto")

		});
    });
</script>

<?php


/*
if (($num_cuenta_completo != "") and (strlen($num_cuenta_completo)==20)){
	$DigControl = 98 - calcular_iban($num_cuenta_completo . "142800",97);
	$Iban= "ES".$DigControl.$cuenta;
}*/


if (isset($_POST["submit"]))
{
//Recogemos los valores de las variables
	//Recogemos los valores de las variables

	//$titular_nombre=str_replace("'","&quot;",$_POST["TitularNombre"]);
	$titular_nombre=$_POST["TitularNombre"];


	$titular_calle=$_POST["TitularCalle"];
	$titular_numero2=$_POST["TitularNumero"];
	if ($titular_numero2=='0') {$titular_numero='';} else {$titular_numero=$titular_numero2;};


	$titular_extension=$_POST["TitularExtension"];
	$titular_aclarador=$_POST["TitularAclarador"];
	$titular_cp=$_POST["TitularCP"];
	//$titular_poblacion=str_replace("'","&quot;",$_POST["TitularPoblacion"]);
	$titular_poblacion=$_POST["TitularPoblacion"];
	$titular_provincia=$_POST["TitularProvincia"];

	$titular_telefono=$_POST["TitularTelefono"];
	$titular_movil=$_POST["TitularMovil"];
	$titular_fax=$_POST["TitularFax"];
	$titular_email=$_POST["TitularEmail"];
	$titular_dni=$_POST["TitularDNI"];

	$factura_nombre=$_POST["FacturaNombre"];
	$factura_calle=$_POST["FacturaCalle"];
	$factura_numero2=$_POST["FacturaNumero"];
	if ($factura_numero2=='0') {$factura_numero='';} else {$factura_numero=$factura_numero2;};

	$factura_extension=$_POST["FacturaExtension"];
	$factura_aclarador=$_POST["FacturaAclarador"];
	$factura_cp=$_POST["FacturaCP"];
	$factura_poblacion=$_POST["FacturaPoblacion"];
	$factura_provincia=$_POST["FacturaProvincia"];

	$factura_telefono=$_POST["FacturaTelefono"];
	$factura_movil=$_POST["FacturaMovil"];
	$factura_fax=$_POST["FacturaFax"];
	$factura_email=$_POST["FacturaEmail"];
	$factura_dni=$_POST["FacturaDNI"];




	$envio_nombre=$_POST["EnvioNombre"];
	$envio_calle=$_POST["EnvioCalle"];
	$envio_numero2=$_POST["EnvioNumero"];
	if ($envio_numero2=='0') {$envio_numero='';} else {$envio_numero=$envio_numero2;};
	$envio_extension=$_POST["EnvioExtension"];
	$envio_aclarador=$_POST["EnvioAclarador"];

	$envio_cp=$_POST["EnvioCP"];
	$envio_poblacion=$_POST["EnvioPoblacion"];
	$envio_provincia=$_POST["EnvioProvincia"];

	$envio_telefono=$_POST["EnvioTelefono"];
	$envio_movil=$_POST["EnvioMovil"];
	$envio_fax=$_POST["EnvioFax"];
	$envio_email=$_POST["EnvioEmail"];
	$envio_dni=$_POST["EnvioDNI"];



	$pagador_nombre=$_POST["PagadorNombre"];
	$pagador_calle=$_POST["PagadorCalle"];
	//$pagador_numero=$_POST["PagadorNumero"];

	$pagador_cp=$_POST["PagadorCP"];

	$pagador_poblacion=$_POST["PagadorPoblacion"];
	$pagador_provincia=$_POST["PagadorProvincia"];
	$pagador_numero2=$_POST["PagadorNumero"];
	if ($pagador_numero2=='0') {$pagador_numero='';} else {$pagador_numero=$pagador_numero2;};

	$pagador_extension=$_POST["PagadorExtension"];
	$pagador_aclarador=$_POST["PagadorAclarador"];

	$pagador_telefono=$_POST["PagadorTelefono"];
	$pagador_movil=$_POST["PagadorMovil"];
	$pagador_fax=$_POST["PagadorFax"];
	$pagador_email=$_POST["PagadorEmail"];
	$pagador_dni=$_POST["PagadorDNI"];

	$representante_nombre=$_POST["RepresentanteEmpresa"];
	$representante_dni=$_POST["DNIRepresentante"];

	$forma_pago=$_POST["FormaPago"];
	$metodopago=$_POST["metodopago"];
	$num_cuenta_banco=$_POST["banco"];
	$num_cuenta_sucursal=$_POST["sucursal"];
	$num_cuenta_digito_control=$_POST["sucursal2"];
	$num_cuenta_cuenta=$_POST["sucursal3"];
	$iban1=$_POST["iban1"];
	$iban2=$_POST["iban2"];
	$iban3=$_POST["iban3"];
	$iban4=$_POST["iban4"];
	$iban5=$_POST["iban5"];
	$iban6=$_POST["iban6"];
	$iban7=$_POST["iban7"];
	$iban8=$_POST["iban8"];
	$iban9=$_POST["iban9"];
	$BancoExtranjero=$_POST["BancoExtranjero"];
	$entidad2=$_POST["entidad3"];
	$Observaciones=$_POST["Observaciones"];

	$cambiotitular2=$_POST["cambiotitular"];
	if ($cambiotitular2==TRUE) {$pagador_numero='';} else {$pagador_numero=$pagador_numero2;};


//Estos campos son combos y tendrán un tratamiento especial

	$idioma_factura=$_POST["IdiomaFactura"];



//Esta variable controlara si se produce algun error en las verificaciones
	$error=0;


	if($error==0 and $forma_pago==1)
	{
//Esta variable almacenara el numero de la cuenta bancaria completo
		$num_cuenta_completo=$num_cuenta_banco.$num_cuenta_sucursal.$num_cuenta_digito_control.$num_cuenta_cuenta;
		$iban_completo=$iban1.$iban2.$iban3.$iban4.$iban5.$iban6.$iban7;
//Esta variable almacenara true si el digito de control es correcto y false si no lo es, será otra restriccion mas para qeu el nuemro de cuenta bancaria sea correcto
		$comprobacion=comprobacion_digito_control($num_cuenta_completo);

//Ahora sbdividimos el valor recibido del campo de la entidad bancaria por el signo "+", en la primera posicion se almacenara el nombre de la entidad bancaria y en la segunda el codigo. Lo utilizaremos para comprobar que el usuario no haya cambiado el codigo de entidad bancaria asignado automaticamente
		$partes_entidad_bancaria=explode("+",$entidad_bancaria);

//En las comprobaciones añadimos la comprobacion de si el primer numero de la cuenta bancaria coincide con el codigo de la identidad bancaria seleccionada
		if($entidad_bancaria==-1 or $num_cuenta_banco=="" or $num_cuenta_sucursal=="" or $num_cuenta_digito_control=="" or $num_cuenta_cuenta==""or $partes_entidad_bancaria[1]!=$num_cuenta_banco or !$comprobacion)
		{
			//$error=5;
		}//	if($entidad_bancaria==-1 or $num_cuenta_banco=="" or $num_cuenta_sucursal=="" or $num_cuenta_digito_control=="" or $num_cuenta_cuenta==""or $partes_entidad_bancaria[1]!=$num_cuenta_banco or !$comprobacion)

//Si se ha rellenado el numero de la cuenta comprobaremos que la cuenta bancaria solo contenga caracteres numericos y cada tramo del numero de la cuenta debera de tener la longitud correcta de caracteres
		else
		{
			if($error==0 and (strlen($num_cuenta_banco)<4 or solo_numero($num_cuenta_banco)) or (strlen($num_cuenta_sucursal)<4 or solo_numero($num_cuenta_sucursal)) or (strlen($num_cuenta_digito_control)<2 or solo_numero($num_cuenta_digito_control)) or (strlen($num_cuenta_cuenta)<10 or solo_numero($num_cuenta_cuenta)))
			{
			//	$error=6;
			}//if((strlen($num_cuenta_banco)<4 or solo_numero($num_cuenta_banco)) or (strlen($num_cuenta_sucursal)<4 or solo_numero($num_cuenta_sucursal)) or (strlen($num_cuenta_digito_control)<2 or solo_numero($num_cuenta_digito_control)) or (strlen($num_cuenta_cuenta)<10 or solo_numero($num_cuenta_cuenta)))

		}//else($entidad_bancaria==-1 or $num_cuenta_banco=="" or $num_cuenta_sucursal=="" or $num_cuenta_digito_control=="" or $num_cuenta_digito_control=="")

	}//if($error==0 and $forma_pago==1)



//Ahora comenzaremos con las verficaciones. En primer lugar comprobaremos si los campos obligatorios han sido todos escritos
	if($titular_nombre=="" or $titular_calle=="" or $titular_numero=="" or $titular_poblacion=="" or $titular_provincia=="" or $titular_dni=="")
	{
	//	$error=1;
	}//if($titular_nombre=="" or $titular_calle=="" or $titular_numero=="" or $titular_poblacion=="" or $titular_provincia=="" or $titular_dni=="" or $forma_pago==-1)

//Si no se ha producido ningun error, ahora se comprueban todos los numeros de telefono y faxes que se hayan escrito
//	if($error==0 and ($titular_telefono!="" and solo_numero($titular_telefono)) or ($titular_movil!="" and solo_numero($titular_movil)) or ($titular_fax!="" and solo_numero($titular_fax)) or // ($factura_telefono!="" and solo_numero($factura_telefono)) or ($factura_movil!="" and solo_numero($factura_movil)) or ($factura_fax!="" and solo_numero($factura_fax)) or
// ($pagador_telefono!="" and solo_numero($pagador_telefono)) or ($pagador_movil!="" and solo_numero($pagador_movil)) or ($pagador_fax!="" and solo_numero($pagador_fax)))
//	{
//		$error=2;
//	}//if($error==0 and ($titular_telefono!="" and solo_numero($titular_telefono)) or...

//A continuacion se haran las comprobaciones de las direcciones de correo que se hayan escrito
//	if($error==0 and ($titular_email!="" and !comprobar_email($titular_email)) or ($factura_email!="" and !comprobar_email($factura_email)) or ($pagador_email!="" and
// !comprobar_email($pagador_email)))
//	{
//		$error=3;
//	}//if($error==0 and ($titular_email!="" and !comprobar_email($titular_email)) or ($factura_email!="" and !comprobar_email($factura_email)) or ($pagador_email!="" and !comprobar_email($pagador_email)))

//Ahora comprobaremos los DNI que se hayan escrito
	//if($error==0 and ($titular_dni!="" and comprobar_dni($titular_dni)) or ($factura_dni!="" and comprobar_dni($factura_dni)) or ($pagador_dni!="" and comprobar_dni($pagador_dni)) or ($representante_dni!="" and comprobar_dni($representante_dni)))
//	{
//		$error=4;
//	}//if($error==0 and ($titular_dni!="" and comprobar_dni($titular_dni)) or ($factura_dni!="" and comprobar_dni($factura_dni)) or ($pagador_dni!="" and comprobar_dni($pagador_dni)) or ($representante_dni!="" and comprobar_dni($representante_dni)))

//Ahora pasaremos a comprobar los datos bancarios. Si la forma de pago es "Domiciliada" los campos de Entidad Bancaria y numero de cuenta se vuelven obligatorios
//	if($error==0 and $forma_pago==1)
//	{
//Esta variable almacenara el numero de la cuenta bancaria completo
//		$num_cuenta_completo=$num_cuenta_banco.$num_cuenta_sucursal.$num_cuenta_digito_control.$num_cuenta_cuenta;
//Esta variable almacenara true si el digito de control es correcto y false si no lo es, será otra restriccion mas para qeu el nuemro de cuenta bancaria sea correcto
//		$comprobacion=comprobacion_digito_control($num_cuenta_completo);

//Ahora sbdividimos el valor recibido del campo de la entidad bancaria por el signo "+", en la primera posicion se almacenara el nombre de la entidad bancaria y en la segunda el codigo. Lo utilizaremos para comprobar que el usuario no haya cambiado el codigo de entidad bancaria asignado automaticamente
//		$partes_entidad_bancaria=explode("+",$entidad_bancaria);

//En las comprobaciones añadimos la comprobacion de si el primer numero de la cuenta bancaria coincide con el codigo de la identidad bancaria seleccionada
//		if($entidad_bancaria==-1 or $num_cuenta_banco=="" or $num_cuenta_sucursal=="" or $num_cuenta_digito_control=="" or $num_cuenta_cuenta==""or
//$partes_entidad_bancaria[1]!=$num_cuenta_banco or !$comprobacion)
//		{
//			$error=5;
//		}//	if($entidad_bancaria==-1 or $num_cuenta_banco=="" or $num_cuenta_sucursal=="" or $num_cuenta_digito_control=="" or $num_cuenta_cuenta==""or $partes_entidad_bancaria[1]!=$num_cuenta_banco or !$comprobacion)

//Si se ha rellenado el numero de la cuenta comprobaremos que la cuenta bancaria solo contenga caracteres numericos y cada tramo del numero de la cuenta debera de tener la longitud correcta de caracteres
//		else
//		{
//			if($error==0 and (strlen($num_cuenta_banco)<4 or solo_numero($num_cuenta_banco)) or (strlen($num_cuenta_sucursal)<4 or solo_numero($num_cuenta_sucursal)) or (strlen($num_cuenta_digito_control)<2 or solo_numero($num_cuenta_digito_control)) or (strlen($num_cuenta_cuenta)<10 or solo_numero($num_cuenta_cuenta)))
//			{
//				$error=6;
//			}//if((strlen($num_cuenta_banco)<4 or solo_numero($num_cuenta_banco)) or (strlen($num_cuenta_sucursal)<4 or solo_numero($num_cuenta_sucursal)) or (strlen($num_cuenta_digito_control)<2 or solo_numero($num_cuenta_digito_control)) or (strlen($num_cuenta_cuenta)<10 or solo_numero($num_cuenta_cuenta)))

//		}//else($entidad_bancaria==-1 or $num_cuenta_banco=="" or $num_cuenta_sucursal=="" or $num_cuenta_digito_control=="" or $num_cuenta_digito_control=="")

//	}//if($error==0 and $forma_pago==1)

//Ahora se comprobara si se ha escrito algún dato en los apartados Facturacion, Pagador y Representante empresa. En el caso de que se haya escrito se debera de comprobar que los campos marcados como obligatorios en los demas apartados esten llenos

//Comprobacion de los datos del facturacion
//	if($factura_nombre!="" or $factura_calle!="" or $factura_numero!="" or $factura_poblacion!="" or $factura_provincia!="" or $factura_dni!="" or $factura_extension!="" or $factura_aclarador!="" or $factura_telefono!="" or $factura_movil!="" or ///$factura_fax!="" or $factura_email!="" or $factura_dni!="")
//	{
//		if($error==0 and ($factura_nombre=="" or $factura_calle=="" or $factura_numero=="" or $factura_poblacion=="" or $factura_provincia=="" or $factura_dni==""))
//		{
//			$error=7;
//		}//if($error==0 and ($factura_nombre=="" or $factura_calle=="" or $factura_numero=="" or $factura_poblacion=="" or $factura_provincia=="" or $factura_dni==""))
//	}//	if($factura_nombre!="" or $factura_calle!="" or $factura_numero!="" or $factura_poblacion!="" or $factura_provincia!="" or $factura_dni!="" or $factura_extension!="" or
//$factura_aclarador!="" or $factura_telefono!="" or $factura_movil!="" or $factura_fax!="" or $factura_email!="" or $factura_dni!="")

//Comprobacion de los datos del pagador
//	if($pagador_nombre!="" or $pagador_calle!="" or $pagador_numero!="" or $pagador_poblacion!="" or $pagador_provincia!="" or $pagador_dni!="" or $pagador_extension!="" or //$pagador_aclarador!="" or $pagador_telefono!="" or $pagador_movil!="" or $pagador_fax!="" or $pagador_email!="" or $pagador_dni!="")
//	{
//		if($error==0 and ($pagador_nombre=="" or $pagador_calle=="" or $pagador_numero=="" or $pagador_poblacion=="" or $pagador_provincia=="" or $pagador_dni==""))
//		{
//			$error=7;
//		}//if($pagador_nombre=="" or $pagador_calle=="" or $pagador_numero=="" or $pagador_poblacion=="" or $pagador_provincia=="" or $pagador_dni=="")
//	}//if($error==0 and ($pagador_nombre!="" or $pagador_calle!="" or $pagador_numero!="" or $pagador_poblacion!="" or $pagador_provincia!="" or $pagador_dni!="")

//Comprobacion de los datos del representante de la empresa
//	if($representante_nombre!="" or $representante_dni!="")
//	{
//		if($error==0 and ($representante_nombre=="" or $representante_dni==""))
//		{
//			$error=8;
//		}//if($error==0 and ($representante_nombre=="" or $representante_dni==""))
//	}//if($representante_nombre!="" or $representante_dni!="")






if($_POST['Condiciones']=='Yes')
{
	$resultTexto = mysql_query('SELECT * FROM DatosRegistrados WHERE CodigoCliente = '.$_SESSION["numeroCliente"]);
	$rowTexto = mysql_fetch_array($resultTexto);
	$email_cliente = $rowTexto['TitularEmail'];
	$dni_cliente = $rowTexto['TitularDNI'];
	$telefono_cliente = $rowTexto['TitularTelefono1'];
	$num_cuenta_banco=$_POST["banco"];
	$num_cuenta_sucursal=$_POST["sucursal"];
	$num_cuenta_digito_control=$_POST["sucursal2"];
	$num_cuenta_cuenta=$_POST["sucursal3"];
	$num_cuenta_completo=$num_cuenta_banco.$num_cuenta_sucursal.$num_cuenta_digito_control.$num_cuenta_cuenta;
	$error_dni=0;
	$error_mail=0;
	$error_telefono=0;
	if($email_cliente==""){$error_mail=1;}
	if($dni_cliente==""){$error_dni=1;}
	if($telefono_cliente==""){$error_telefono=1;}

 } else {$error=9;}
/*
echo strlen($num_cuenta_completo);

if (strlen($num_cuenta_completo)!=20){
	$error =11;
}*/

	$iban1=$_POST["iban1"];
	$iban2=$_POST["iban2"];
	$iban3=$_POST["iban3"];
	$iban4=$_POST["iban4"];
	$iban5=$_POST["iban5"];
	$iban6=$_POST["iban6"];
	$iban7=$_POST["iban7"];
	$iban8=$_POST["iban8"];
	$iban9=$_POST["iban9"];

$iban_completo = $iban1.$iban2.$iban3.$iban4.$iban5.$iban6.$iban7;
if ($_POST["BancoExtranjero"] ==""){
if (strlen($iban_completo)==24){

}else{$error =12;	}

}


if(  ($_POST['cambiotitular']=='Si')
 or ($_POST['cambiofactura']=='Si')
 or ($_POST['cambioenvio']=='Si')
 or ($_POST['cambiopagador']=='Si')
 or ($_POST['cambiodomiciliacion']=='Si')
 or ($_POST['cambiorepresentante']=='Si')   )

 {} else {$error=10;}


}//if (isset($_POST["submit"]))

?>

</head>

<body onload="Load()">
	<div id="central_oficina">

<?php
//Se comprueba que el usuario se haya identificado, si no es asi se da un aviso y redirigimos a la seccion de identificacion
if ($_SESSION['usuario']=="")
{
	MsgBox($error_identificacion);
	redirigir("oficina_index.php?id=".$_SESSION["idioma"]);

}//if ($_SESSION['usuario'])

//Si el usuario se ha identificado la seccion se ejecutara con normalidad
else
{
//Añadimos los fragmentos comunes, la cabecera, el menu, y el submenu
		include("oficina_fragmentos_comunes.php");
?>
		<div class="contenido_seccion_oficina_modificacion_titular">
		<script type="text/javascript">
				function enviar_formulario(){ document.form_cambios_datos_titular.submit() }
			</script>
			<form action="oficina_modificacion_datostitular.php?id=<?=$_SESSION["idioma"]?>"  method="post" name="form_cambios_datos_titular" id="form_cambios_datos_titular">

            <div class="arialblack18Titulo" style="width:100%; text-align:center; margin-bottom:20px; margin-top:25px; ">
				<?php
                $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 195');			
                $rowTexto = mysql_fetch_array($resultTexto); echo(utf8_encode($rowTexto['Texto']));?>
            </div>


            <?php
					//Esta variable controlara si el cliente tiene o no modificaciones pendientes
					$tiene_modificaciones_pendientes=0;

					//Comprobamos si el cliente, tiene modificaciones pendientes
					$consulta_cliente=mysql_query("SELECT * FROM DatosModificados WHERE CodigoCliente  = ".$_SESSION["numeroCliente"]);
					mysql_query("SET NAMES 'utf8'");					
					$hay_cliente = mysql_fetch_array($consulta_cliente);

					if($hay_cliente){$tiene_modificaciones_pendientes=1;}


					if($tiene_modificaciones_pendientes==1)
					{
			?>
            	<div style="width:95%; text-align:center; color:#F00"><?=$advertencia_cambios_pendientes?></div>
            <?php	}?>

            <script type="text/javascript">
			function ver_seccion(seccion, llamador){
				var obj = document.getElementById(seccion)
				if (llamador.checked){
				 obj.style.display="block";
				}else {
				 obj.style.display="none";
				}
				activas();
			}
			/*function activas(){
				var obj = document.getElementById("mensaje");
				var obj2 = document.getElementById("botonEnviar");
				if (document.form_cambios_datos_titular.TitularCambiar.checked ||
				document.form_cambios_datos_titular.FacturacionCambiar.checked ||
				document.form_cambios_datos_titular.EnvioCambiar.checked ||
				document.form_cambios_datos_titular.PagadorCambiar.checked ||
				document.form_cambios_datos_titular.MetodoPagoCambiar.checked ||
				document.form_cambios_datos_titular.ObservacionesCambiar.checked){
					obj.style.display="none";
					obj2.style.display="block";
				} else {
				 	obj.style.display="block";
					obj2.style.display="none";
				}

			}*/
			</script>

			<!-- TITULAR ACTUAL -->
			<!--<fieldset style="margin-bottom:10px;">
				<legend class="arialblack14">
					<?php
					$resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 562');
					$rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);
					?>
				</legend>-->
                <?php

				$result = mysql_query("SELECT * FROM DatosRegistrados WHERE CodigoCliente = ".$_SESSION['numeroCliente']." ");
				$row = mysql_fetch_array($result);

				//Si se ha aceptado el formulario, las variables que contienen los valores de cada campo ya tienen valor.
				//Si no es asi, asignaremos los valores con el contenido de la base de datos
				if (!isset($_POST["submit"])){
					$titular_nombre=$row["TitularNombre"];
								$titular_calle=$row["TitularCalle"];
								$titular_numero2=$row["TitularNumero"];
								if ($titular_numero2=='0') {$titular_numero='';} else {$titular_numero=$titular_numero2;};
								$titular_extension=$row["TitularExtension"];
								$titular_aclarador=$row["TitularAclarador"];
								$titular_cp=$row["TitularCP"];
								$titular_poblacion=$row["TitularPoblacion"];;
								$titular_provincia=$row["TitularProvincia"];
								$titular_telefono=$row["TitularTelefono1"];
								$titular_movil=$row["TitularTelefono2"];
								$titular_fax=$row["TitularFax"];
								$titular_email=$row["TitularEmail"];
								$titular_dni=$row["TitularDNI"];

								$factura_nombre=$row["FacturaNombre"];
								$factura_calle=$row["FacturaCalle"];
								$factura_numero2=$row["FacturaNumero"];
								if ($factura_numero2=='0') {$factura_numero='';} else {$factura_numero=$factura_numero2;};
								$factura_extension=$row["FacturaExtension"];
								$factura_aclarador=$row["FacturaAclarador"];
								$factura_cp=$row["FacturaCP"];
								$factura_poblacion=$row["FacturaPoblacion"];
								$factura_provincia=$row["FacturaProvincia"];
								$factura_telefono=$row["FacturaTelefono1"];
								$factura_movil=$row["FacturaTelefono2"];
								$factura_fax=$row["FacturaFax"];
								$factura_email=$row["FacturaEmail"];
								$factura_dni=$row["FacturaDNI"];

								$envio_nombre=$row["EnvioNombre"];
								$envio_calle=$row["EnvioCalle"];
								$envio_numero2=$row["EnvioNumero"];
								if ($envio_numero2=='0') {$envio_numero='';} else {$envio_numero=$envio_numero2;};
								$envio_extension=$row["EnvioExtension"];
								$envio_aclarador=$row["EnvioAclarador"];
								$envio_cp=$row["EnvioCP"];
								$envio_poblacion=$row["EnvioPoblacion"];
								$envio_provincia=$row["EnvioProvincia"];
								$envio_telefono=$row["EnvioTelefono1"];
								$envio_movil=$row["EnvioTelefono2"];
								$envio_fax=$row["EnvioFax"];
								$envio_email=$row["EnvioEmail"];
								$envio_dni=$row["EnvioDNI"];

								$pagador_nombre=$row["PagadorNombre"];
								$pagador_calle=$row["PagadorCalle"];
								$pagador_numero2=$row["PagadorNumero"];
								if ($pagador_numero2=='0') {$pagador_numero='';} else {$pagador_numero=$pagador_numero2;};
								$pagador_extension=$row["PagadorExtension"];
								$pagador_aclarador=$row["PagadorAclarador"];
								$pagador_cp=$row["PagadorCP"];
								$pagador_poblacion=$row["PagadorPoblacion"];
								$pagador_provincia=$row["PagadorProvincia"];
								$pagador_telefono=$row["PagadorTelefono1"];
								$pagador_movil=$row["PagadorTelefono2"];
								$pagador_fax=$row["PagadorFax"];
								$pagador_email=$row["PagadorEmail"];
								$pagador_dni=$row["PagadorDNI"];

								$representante_nombre=$row["RepresentanteEmpresa"];
								$representante_dni=$row["DNIRepresentante"];

								$num_cuenta_banco=substr($row["NumeroCuenta"],0,4);
								$num_cuenta_sucursal=substr($row["NumeroCuenta"],4,4);
								$num_cuenta_digito_control=substr($row["NumeroCuenta"],8,2);
								$num_cuenta_cuenta=substr($row["NumeroCuenta"],10,10);

								//Ahora subdividiremos el numero del iban para escribir las cifras correspondientes en cada uno de los cuadros
								$iban1=substr($row["Iban"],0,4);
								$iban2=substr($row["Iban"],4,4);
								$iban3=substr($row["Iban"],8,4);
								$iban4=substr($row["Iban"],12,4);
								$iban5=substr($row["Iban"],16,4);
								$iban6=substr($row["Iban"],20,4);
								$iban7=substr($row["Iban"],24,4);
								$iban8=substr($row["Iban"],28,4);
								$iban9=substr($row["Iban"],32,2);
								$BancoExtranjero=$row["BancoExtranjero"];

							    $entidad2=$row["BancoDomiciliado"];

								$metodopago=$row["FormaPago"];
								$idioma_factura=$row["IdiomaFactura"];
								$entidad_bancaria=$row["BancoDomiciliado"];
				}

				?>
                <!--<table width="600" align="center" border="0" class="arial12bu">
                  <tr class="arial12Importante">
                    <td colspan="2">
                        <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 119');
                        $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                    </td>
                    <td align="right">
                        <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 118');
                        $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                    </td>
                  </tr>
                  <tr>
                    <td colspan="2"><?php echo(str_replace('"','&quot;',$row['TitularNombre']));?></td>
                    <td align="right"><?php echo($row['DNI']);?></td>
                  </tr>
                  <tr style="height:3px"><td colspan="3"></td></tr>
                  <tr class="arial12Importante">
                    <td width="275">
                         <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 196');
                         $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                    </td>
                    <td width="155">
                        <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 197');
                        $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                    </td>
                    <td width="155" align="right">CUPS</td>
                  </tr>
                  <tr>
                    <td><?php echo($row['CodigoCliente']);?></td>
                    <td><?php echo($row['NumeroPoliza']);?></td>
                    <td align="right"><?php echo($row['CUPS']);?></td>
                  </tr>
                  </table>
            </fieldset>-->

            <!-- PUNTO DE SUMINISTRO -->
			<!--<fieldset style="margin-bottom:20px">
            	<legend class="arialblack14">
				<?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 130');
				$rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                </legend>
                    <table width="600" border="0" class="arial12bu" align="center">
  <tr class="arial12Importante">
    <td width="275">
		<?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 123');
		$rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
    </td>
    <td width="155">
   		<?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 124');
		$rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
   	</td>
    <td width="155" align="right">
    	<?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 125');
		$rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
    </td>
  </tr>
  <tr>
    <td><?php echo(str_replace('"','&quot;',$row['SuministroCalle']));?></td>
    <td><?php echo($row['SuministroNumero']);?></td>
    <td align="right"><?php echo(str_replace('"','&quot;',$row['SuministroExtension']));?></td>
  </tr>
  <tr class="arial12Importante">
    <td>
    	<?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 121');
        $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
    </td>
    <td>
		<?php $resultTexto=mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto=122');
		$rowTexto=mysql_fetch_array($resultTexto);echo($rowTexto['Texto']); ?>
    </td>
    <td></td>
  </tr>
  <tr>
    <td><?php echo(str_replace('"','&quot;',$row['SuministroCiudad']));?></td>
    <td><?php echo($row['SuministroCP']);?></td>
    <td>&nbsp;</td>
  </tr>
  <tr class="arial12Importante">
    <td><?=$oficina_datos_titular21?></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td><?php echo(str_replace('"','&quot;',$row['SuministroExtension2']));?></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
            </fieldset>
            -->
            <div style="margin:auto; text-align:center; margin-bottom:20px;">
                <p style="font-weight:bold">Por favor, seleccione los apartados que desea modificar:
                <br /><span style="font-size:10px; color:#999">(El formulario mostrará las secciones marcadas)</span></p>
                <input type="checkbox" name="TitularCambiar" id="TitularCambiar" onchange="ver_seccion('titular', this)" style="width:auto" />
                <span style="color:#000; font-weight:bold">Titular</span>
                <input type="checkbox" name="FacturacionCambiar" id="FacturacionCambiar" onchange="ver_seccion('facturacion', this)" style="width:auto" />
                <span style="color:#000; font-weight:bold">Facturacion</span>
                <input type="checkbox" name="EnvioCambiar" id="EnvioCambiar" onchange="ver_seccion('envio', this)" style="width:auto" />
                <span style="color:#000; font-weight:bold">Envio</span>
                <input type="checkbox" name="PagadorCambiar" id="PagadorCambiar" onchange="ver_seccion('pagador', this)" style="width:auto" />
                <span style="color:#000; font-weight:bold">Pagador</span>
                <input type="checkbox" name="MetodoPagoCambiar" id="MetodoPagoCambiar" onchange="ver_seccion('metodoPago', this)" style="width:auto" />
                <span style="color:#000; font-weight:bold">Metodo de Pago</span>
                <!--<input type="checkbox" name="PagadorCambiar" id="PagadorCambiar" onchange="ver_seccion('representante', this)" />
                <span style="color:#333; font-weight:bold">Representante</span>-->
                <input type="checkbox" name="bservacioneCambiar" id="ObservacionesCambiar" onchange="ver_seccion('observaciones', this)" style="width:auto" />
                <span style="color:#000; font-weight:bold">Observaciones</span>
            </div>
            <!--<p id="mensaje" style="color:#F00; font-size:12px; font-weight:bold; text-align:center">
            	No ha seleccionado ninguna opcion para modificar.
            </p>-->

            <!-- DATOS DE TITULAR -->
            <div class="box-form" style="border-radius:10px; margin-top:20px">
            <fieldset style="padding:15px; text-align:left; display:none" class="box-4-4" id="titular">
              <legend  class="arialblack14"><?=$oficina_datos_titular3?></legend>
                 <table width="600" align="center" border="0">
                     <tr>
                          <td valign="middle" class="clr-4 bold"><?=$oficina_datos_titular4?>*</td>
                          <td colspan="3">

                          <input name="TitularNombre" value="<?=$titular_nombre?>" type="text" onchange="cambios('cambiotitular');" id="TitularNombre" style="width:100%" />
                          </td>
                      </tr>

                     <tr>
                         <td valign="middle" class="clr-4 bold top-4"><?=$oficina_datos_titular5?>*</td>
                         <td colspan="3">
                         <input name="TitularCalle" value="<?=$titular_calle?>" type="text" id="TitularCalle" onchange="cambios('cambiotitular');" style="width:100%" />
                         </td>
                     </tr>

                     <tr>
                        <td width="200" valign="middle" class="clr-4 bold"><?=$oficina_datos_titular6?>*</td>
                        <td width="150"><input name="TitularNumero" onchange="cambios('cambiotitular');" value="<?=$titular_numero?>" type="text"  id="TitularNumero"  size="5" maxlength="4" /></td>
                        <td width="200" style="text-align:right" valign="middle" class="clr-4 bold">&nbsp;<?=$oficina_datos_titular7?></td>
                        <td width="150" valign="top" style="text-align:right"><input name="TitularExtension" onchange="cambios('cambiotitular');" type="text" id="TitularExtension" value="<?=$titular_extension?>" style="width:100%" />
                        </td>

	                 </tr>

                     <tr>
                        <td valign="middle" class="clr-4 bold"><?=$oficina_datos_titular8?></td>
                        <td colspan="3">
                        <input name="TitularAclarador" value="<?=$titular_aclarador?>" type="text" id="TitularAclarador" onchange="cambios('cambiotitular');" style="width:100%" />
                        </td>
                      </tr>

                     <tr>
						<td valign="middle" class="clr-4 bold"><?=$oficina_datos_titular9?>*</td>
    					<td valign="top"><input name="TitularPoblacion" type="text"  value="<?=$titular_poblacion?>" id="TitularPoblacion" onchange="cambios('cambiotitular');" size="20" maxlength="50" /></td>
    					<td valign="middle" class="clr-4 bold" style="text-align:right">
							<?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 289');
                            $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>*
                        </td>

                        <td valign="top" style="text-align:right">
                        	<input name="TitularCP" type="text"  value="<?=$titular_cp?>"   id="TitularCP" onchange="cambios('cambiotitular');" style="width:100%" />
                            </td>
                    </tr>

                     <tr>
                        <td valign="middle" class="clr-4 bold"><?=$oficina_datos_titular10?>*</td>
                        <td valign="top" colspan="3"><input name="TitularProvincia" type="text"  value="<?=$titular_provincia?>" id="TitularProvincia" onchange="cambios('cambiotitular');" size="20" /></td>
                   </tr>

             	     <tr>
                        <td valign="middle" class="clr-4 bold"><?=$oficina_datos_titular11?></td>
                        <td valign="top" class="clr-4 bold">
                           <input name="TitularTelefono" type="text" value="<?=$titular_telefono?>"  id="TitularTelefono" onchange="cambios('cambiotitular');" size="15" maxlength="15" />
                           </td>
                           <td style="text-align:right" class="clr-4 bold"><?=$oficina_datos_titular12?></td>
                           <td style="text-align:right" class="clr-4 bold">
                          <input name="TitularMovil" value="<?=$titular_movil?>"  type="text" id="TitularMovil" onchange="cambios('cambiotitular');" style="width:100%" />
                          </td>
                      </tr>

                     <tr>
                         <td valign="middle" class="clr-4 bold"><?=$oficina_datos_titular13?></td>
                         <td valign="top" class="clr-4 bold" colspan="3">
                         	<input name="TitularFax" onchange="cambios('cambiotitular');" value="<?=$titular_fax?>"  type="text" id="TitularFax" size="15" maxlength="15" />
                            </td>
                      </tr>

                     <tr>
                            <td  valign="middle" class="clr-4 bold"><?=$oficina_datos_titular14?></td>
                        	<td  valign="top" colspan="3">
                            <input name="TitularEmail" onchange="cambios('cambiotitular');" value="<?=$titular_email?>"  type="text" id="TitularEmail" style="width:100%" /></td>
                      </tr>

					 <tr>
                       <td valign="middle" class="clr-4 bold"><?=$oficina_datos_titular15?>*</td>
                       <td valign="top" class="clr-4 bold" colspan="3"><input name="TitularDNI" onchange="cambios('cambiotitular');" value="<?=$titular_dni?>"  type="text" id="TitularDNI" size="15" maxlength="15"/></td>
                     </tr>
                 </table>

            </fieldset>
             </div>

            <!-- DATOS DE FACTURACION -->
            <div class="box-form" style="border-radius:10px; margin-top:20px">
            <fieldset style="padding:15px; text-align:left; display:none" class="box-4-4" id="facturacion">              
			  <legend  class="arialblack14">Datos de Facturación</legend>
                   <table width="600" align="center" border="0">
                      <tr>
                          <td valign="middle" class="clr-4 bold"><?=$oficina_datos_titular17?></td>
                          <td valign="top" colspan="3"><input name="FacturaNombre" onchange="cambios('cambiofactura');" value="<?=$factura_nombre?>" type="text" id="FacturaNombre" style="width:100%" /></td>
                      </tr>

                      <tr>
                        <td valign="middle" class="clr-4 bold"><?=$oficina_datos_titular18?></td>
                        <td valign="top" colspan="3"><input name="FacturaCalle" onchange="cambios('cambiofactura');" value="<?=$factura_calle?>" type="text" id="FacturaCalle" style="width:100%" /></td>
                     </tr>

                      <tr>
                        <td width="150" valign="middle" class="clr-4 bold"><?=$oficina_datos_titular19?></td>
                        <td width="150" valign="top" class="clr-4 bold"><input name="FacturaNumero" onchange="cambios('cambiofactura');" value="<?=$factura_numero?>" type="text"  id="FacturaNumero"  size="5" maxlength="4" /></td>
                        <td width="150" valign="middle" class="clr-4 bold" style="text-align:right"><?=$oficina_datos_titular20?></td>
                        <td width="150" valign="top" style="text-align:right"><input name="FacturaExtension" onchange="cambios('cambiofactura');" type="text" id="FacturaExtension" value="<?=$factura_extension?>"  style="width:100%" /></td>
	                  </tr>

                      <tr>
                         <td valign="middle" class="clr-4 bold"><?=$oficina_datos_titular21?></td>
                         <td valign="top" colspan="3"><input name="FacturaAclarador" onchange="cambios('cambiofactura');" value="<?=$factura_aclarador?>" type="text" id="FacturaAclarador" style="width:100%" /></td>
                      </tr>

    				  <tr>
                      		<td valign="middle" class="clr-4 bold"><?=$oficina_datos_titular22?></td>
                      		<td valign="top"><input name="FacturaPoblacion" onchange="cambios('cambiofactura');" type="text"  value="<?=$factura_poblacion?>" id="FacturaPoblacion" size="20" maxlength="50" /></td>
                      		<td valign="middle" class="clr-4 bold" style="text-align:right">
							<?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 289');
							$rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                            </td>
                      		<td valign="top" style="text-align:right"><input name="FacturaCP" onchange="cambios('cambiofactura');" type="text"  value="<?=$factura_cp?>"   id="FacturaCP"  style="width:100%" /></td>
                      </tr>

                      <tr>
                          <td valign="middle" class="clr-4 bold"><?=$oficina_datos_titular23?></td>
                          <td valign="top" colspan="3"><input name="FacturaProvincia" onchange="cambios('cambiofactura');" type="text"  value="<?=$factura_provincia?>" id="FacturaProvincia"  size="20" /></td>
                      </tr>

                      <tr>
                            <td valign="middle" class="clr-4 bold"><?=$oficina_datos_titular24?></td>
                        	<td valign="top" class="clr-4 bold">
                            <input name="FacturaTelefono" onchange="cambios('cambiofactura');" type="text" value="<?=$factura_telefono?>"  id="FacturaTelefono" size="15" maxlength="15" />
                            </td>
                            <td class="clr-4 bold" style="text-align:right">
<?=$oficina_datos_titular25?>
                          </td><td style="text-align:right">
                          <input name="FacturaMovil" onchange="cambios('cambiofactura');" value="<?=$factura_movil?>"  type="text" id="FacturaMovil"  style="width:100%" />
                          </td>
                      </tr>

                      <tr>
                         <td valign="middle" class="clr-4 bold"><?=$oficina_datos_titular26?></td>
                         <td valign="top" class="clr-4 bold" colspan="3">
                         <input name="FacturaFax" onchange="cambios('cambiofactura');" value="<?=$factura_fax?>"  type="text" id="FacturaFax" size="15" maxlength="15" />
                         </td>
                      </tr>

                      <tr>
                            <td valign="middle" class="clr-4 bold"><?=$oficina_datos_titular27?></td>
                            <td valign="top" colspan="3"><input name="FacturaEmail" onchange="cambios('cambiofactura');" value="<?=$factura_email?>"  type="text" id="FacturaEmail"  style="width:100%" /></td>
                      </tr>

                      <tr>
                            <td valign="middle" class="clr-4 bold"><?=$oficina_datos_titular28?></td>
                            <td valign="top" class="clr-4 bold"><input name="FacturaDNI" onchange="cambios('cambiofactura');" value="<?=$factura_dni?>"  type="text" id="FacturaDNI" size="15" maxlength="15" /></td>
                      </tr>
                   </table>

            </fieldset>
            </div>

            <!-- DATOS DE ENVIO -->
            <div class="box-form" style="border-radius:10px; margin-top:20px">
            <fieldset style="padding:15px; text-align:left; display:none" class="box-4-4" id="envio">              
			  <legend  class="arialblack14">Datos Envío</legend>
               <table width="600" align="center" border="0">
                     <tr>
                        <td valign="middle" class="clr-4 bold"><?=$oficina_datos_titular17?></td>
                        <td valign="top" colspan="3"><input name="EnvioNombre" onchange="cambios('cambioenvio');" value="<?=$envio_nombre?>" type="text" id="EnvioNombre" style="width:100%" /></td>
                     </tr>

                     <tr>
                        <td valign="middle" class="clr-4 bold"><?=$oficina_datos_titular18?></td>
                        <td valign="top" colspan="3"><input name="EnvioCalle" onchange="cambios('cambioenvio');" value="<?=$envio_calle?>" type="text" id="EnvioCalle" style="width:100%" /></td>
                      </tr>

                     <tr>
                          <td width="150" valign="middle" class="clr-4 bold"><?=$oficina_datos_titular19?></td>
                          <td width="150" valign="top" class="clr-4 bold"><input name="EnvioNumero" onchange="cambios('cambioenvio');" value="<?=$envio_numero?>" type="text"  id="EnvioNumero"  size="5" maxlength="4" /></td>
                          <td width="150" style="text-align:right" valign="middle" class="clr-4 bold">&nbsp;<?=$oficina_datos_titular20?></td>
                          <td width="150" style="text-align:right" valign="top"><input name="EnvioExtension" onchange="cambios('cambioenvio');" type="text" id="EnvioExtension" value="<?=$envio_extension?>" style="width:100%" /></td>
                       </tr>

                     <tr>
                          <td valign="middle" class="clr-4 bold"><?=$oficina_datos_titular21?></td>
                          <td valign="top" colspan="3"><input name="EnvioAclarador" onchange="cambios('cambioenvio');" value="<?=$envio_aclarador?>" type="text" id="EnvioAclarador" style="width:100%" /></td>
                       </tr>

                      <tr>
                            <td valign="middle" class="clr-4 bold"><?=$oficina_datos_titular22?></td>
                            <td valign="top"><input name="EnvioPoblacion" onchange="cambios('cambioenvio');" type="text"  value="<?=$envio_poblacion?>" id="EnvioPoblacion" size="20" maxlength="50" /></td>
                           <td valign="middle" class="clr-4 bold" style="text-align:right">
                           <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 289');
                           $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?></td>
                           <td valign="top"  style="text-align:right">
                           <input name="EnvioCP" onchange="cambios('cambioenvio');" type="text"  value="<?=$envio_cp?>"   id="EnvioCP" style="width:100%" /></td>
                        </tr>

                     <tr>
                            <td valign="middle" class="clr-4 bold"><?=$oficina_datos_titular10?></td>
                            <td valign="top" colspan="3"><input name="EnvioProvincia" onchange="cambios('cambioenvio');" type="text"  value="<?=$envio_provincia?>" id="EnvioProvincia" size="20" maxlength="50" /></td>
                        </tr>

                     <tr>
                          <td valign="middle" class="clr-4 bold"><?=$oficina_datos_titular24?></td>
                          <td valign="top" class="clr-4 bold"><input name="EnvioTelefono" onchange="cambios('cambioenvio');" type="text" value="<?=$envio_telefono?>"  id="EnvioTelefono" size="15" maxlength="15" />
                          </td>
                          <td style="text-align:right" class="clr-4 bold"><?=$oficina_datos_titular25?></td>
                          <td style="text-align:right">  <input name="EnvioMovil" onchange="cambios('cambioenvio');" value="<?=$envio_movil?>"  type="text" id="EnvioMovil"  style="width:100%" />
                          </td>
                       </tr>

                     <tr>
                           <td valign="middle" class="clr-4 bold"><?=$oficina_datos_titular26?></td>
                           <td valign="top" class="clr-4 bold" colspan="3"><input name="EnvioFax" onchange="cambios('cambioenvio');" value="<?=$envio_fax?>"  type="text" id="EnvioFax" size="15" maxlength="15" /></td>
                        </tr>

                     <tr>
                           <td valign="middle" class="clr-4 bold"><?=$oficina_datos_titular27?></td>
                           <td valign="top" colspan="3"><input name="EnvioEmail" onchange="cambios('cambioenvio');" value="<?=$envio_email?>"  type="text" id="EnvioEmail" style="width:100%" /></td>
                        </tr>

                     <tr>
                           <td valign="middle" class="clr-4 bold"><?=$oficina_datos_titular28?></td>
                           <td valign="top" class="clr-4 bold" colspan="3"><input name="EnvioDNI" onchange="cambios('cambioenvio');" value="<?=$envio_dni?>"  type="text" id="EnvioDNI" size="15" maxlength="15" /></td>
                         </tr>
               </table>
           </fieldset>
         </div>

            <!--  DATOS DE PAGADOR  -->
           <div class="box-form" style="border-radius:10px; margin-top:20px">
            <fieldset style="padding:15px; text-align:left; display:none" class="box-4-4" id="pagador">               
   			   <legend  class="arialblack14"><?=$oficina_datos_titular29?></legend>
                <table width="600" align="center" border="0">
                      <tr>
                        <td valign="middle" class="clr-4 bold"><?=$oficina_datos_titular30?></td>
                        <td valign="top" colspan="3"><input name="PagadorNombre" onchange="cambios('cambiopagador');" value="<?=$pagador_nombre?>" type="text" id="PagadorNombre" style="width:100%" /></td>
                      </tr>

                      <tr>
                        <td valign="middle" class="clr-4 bold"><?=$oficina_datos_titular31?></td>
                        <td valign="top" colspan="3"><input name="PagadorCalle" onchange="cambios('cambiopagador');" value="<?=$pagador_calle?>" type="text" id="PagadorCalle"  style="width:100%" /></td>
                      </tr>

                      <tr>
                        <td width="150" valign="middle" class="clr-4 bold"><?=$oficina_datos_titular32?></td>
                        <td width="150" valign="top" class="clr-4 bold"><input name="PagadorNumero" onchange="cambios('cambiopagador');" value="<?=$pagador_numero?>" type="text"  id="PagadorNumero"  size="5" maxlength="4" /></td>
                        <td width="150" valign="middle" class="clr-4 bold" style="text-align:right">&nbsp;<?=$oficina_datos_titular33?></td>
                        <td width="150" valign="top" style="text-align:right"><input name="PagadorExtension" onchange="cambios('cambiopagador');" type="text" id="PagadorExtension" value="<?=$pagador_extension?>" style="width:100%" /></td>
                      </tr>

                       <tr>
                         <td valign="middle" class="clr-4 bold"><?=$oficina_datos_titular34?></td>
                         <td valign="top" colspan="3"><input name="PagadorAclarador" onchange="cambios('cambiopagador');" value="<?=$pagador_aclarador?>" type="text" id="PagadorAclarador" style="width:100%" /></td>
                      </tr>

                      <tr>
                      		<td valign="middle" class="clr-4 bold"><?=$oficina_datos_titular35?></td>
                        	<td valign="top"><input name="PagadorPoblacion" onchange="cambios('cambiopagador');" type="text"  value="<?=$pagador_poblacion?>" id="PagadorPoblacion" size="20" maxlength="50" /></td>
                            <td valign="middle" class="clr-4 bold" style="text-align:right">
                            <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 289');
                            $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                            </td>
                            <td valign="top" style="text-align:right">
                            <input name="PagadorCP" onchange="cambios('cambiopagador');" type="text"  value="<?=$pagador_cp?>"   id="PagadorCP" style="width:100%" />
                            </td>
                     </tr>

					  <tr>
                        <td valign="middle" class="clr-4 bold"><?=$oficina_datos_titular36?></td>
                        <td valign="top" colspan="3"><input name="PagadorProvincia" onchange="cambios('cambiopagador');" type="text"  value="<?=$pagador_provincia?>" id="PagadorProvincia" size="20" maxlength="50" /></td>
                    </tr>

             		  <tr>
                        <td valign="middle" class="clr-4 bold"><?=$oficina_datos_titular37?></td>
                        <td valign="top" class="clr-4 bold"><input name="PagadorTelefono" onchange="cambios('cambiopagador');" type="text" value="<?=$pagador_telefono?>"  id="PagadorTelefono" size="15" maxlength="15" /></td>
						<td style="text-align:right"><?=$oficina_datos_titular38?></td>
                        <td style="text-align:right">
                          <input name="PagadorMovil" onchange="cambios('cambiopagador');" value="<?=$pagador_movil?>"  type="text" id="PagadorMovil" style="width:100%" />
                          </td>
                     </tr>

                      <tr>
                        <td valign="middle" class="clr-4 bold"><?=$oficina_datos_titular39?></td>
                        <td valign="top" class="clr-4 bold" colspan="3"><input name="PagadorFax" onchange="cambios('cambiopagador');" value="<?=$pagador_fax?>"  type="text" id="PagadorFax" size="15" maxlength="15" /></td>
                      </tr>

                      <tr>
                        <td valign="middle" class="clr-4 bold"><?=$oficina_datos_titular40?></td>
                        <td valign="top" colspan="3"><input name="PagadorEmail" onchange="cambios('cambiopagador');" value="<?=$pagador_email?>"  type="text" id="PagadorEmail" style="width:100%" /></td>
                     </tr>

                      <tr>
                   <td valign="middle" class="clr-4 bold"><?=$oficina_datos_titular41?></td>
                   <td valign="top" class="clr-4 bold" colspan="3"><input name="PagadorDNI" onchange="cambios('cambiopagador');" value="<?=$pagador_dni?>"  type="text" id="PagadorDNI" size="15" maxlength="15" /></td>
                </tr>
               </table>
           </fieldset>
           </div>

           <!-- METODO DE PAGO -->
           <div class="box-form"  style="border-radius:10px; margin:auto; margin-top:20px;">
            <fieldset  style="padding:15px; text-align:left; display:none" class="box-4-4" id="metodoPago" >
   			   <legend  class="arialblack14"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 138'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?></legend>

<!--ESPACIO ENTRE EL TITULO Y EL PRIMER CAMPO-->
				    <div class="limpiar"> </div>
					<div class="espacio"> </div>

<!--ESPACIO ENTRE EL TITULO Y EL PRIMER CAMPO-->



                    <table width="500" align="center">

                   <tr>

                      <td align="center"> <div align="left">Método de Pago actual:&nbsp;&nbsp;<input size="42" name="metodopago" id="metodopago" class="textfieldLectura" value="<?=$metodopago?>" readonly="readonly"/>
                          <br/><br/>
          </div></td></tr>

                          <tr>
                        <td width="442" class="arial14b">


<!--La forma de pago ya no se lee de ninguna tabla, se ha introducido como textos en las tablas de idiomas, asi que simplemente mostramos el valor de las variables-->


						  <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 292'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>*&nbsp; &nbsp; &nbsp;
						  <select width="250" style="width: 250px" name="FormaPago" id="FormaPago" onchange="datos_bancarios();">

						    <option selected="selected" value="<?=$forma_pago?>"><?=$forma_pago1?></option>

                          </select>

                         <select  name="EntidadBancaria" id="EntidadBancaria" style="display:none;" readOnly="enabled" >
                           <option value="-1">&nbsp;</option>
<?php

						   $consulta_bancos = mysql_query("SELECT * FROM EntidadesBancarias ORDER BY NombreBanco ASC ");
							while ($registro_banco = mysql_fetch_array($consulta_bancos)){
?>
                           <option value="<?=$registro_banco["NombreBanco"]."+".$registro_banco["CodigoOficial"]?>">
	                           <?= $registro_banco["NombreBanco"]?>
                           </option>
                           <?php }?>
                         </select>
                         </td>
                      </tr>






                    </table>


<div class="limpiar"> </div>
<div class="espacio"> </div>


                    <table width="500" border="0" align="center" cellpadding="0" cellspacing="0" class="arial14">
                      <tr>
                        <td width="450"><fieldset>
                          <legend class="arialblack14">
                          <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 140'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                          </legend>

                          <table width="500" align="center" border="0">

                            <tr align="center">
                                <td width="390" valign="top">


                                <table width="380" border="0" cellpadding="0" cellspacing="0">
                                  <tr>
                                    <td width="50" valign="top"><table width="76" border="0" cellpadding="0" cellspacing="0">
                                      <tr>
                                        <td width="50" align="center" valign="top" class="arial14">
                                  <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 141'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                                  *</td>
                              </tr>
                                    </table>                                    </td>
                                    <td width="4">&nbsp;</td>
                                    <td valign="top"><table width="70" border="0" cellpadding="0" cellspacing="0">

                                      <tr>
                                        <td width="92" align="center" valign="top" class="arial14">
                                  <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 142'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                                  *</td>
                              </tr>
                                    </table>                                    </td>
                                    <td width="120" valign="top"><table width="120" border="0" cellpadding="0" cellspacing="0">

                                      <tr >
                                        <td width="100" align="center" valign="top" class="arial14">
                                  <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 143'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                                  *  </td>
                              <td width="3" >&nbsp;</td>
                                      </tr>
                                    </table>                                    </td>
                                    <td width="97" valign="top"><table width="100" border="0" cellpadding="0" cellspacing="0">

                                      <tr>
                                        <td width="100" align="center" valign="top" class="arial14">
                                  <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 144'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                                  *</td>
                            </tr>
                                    </table>                                    </td>
                            </tr>
                                </table>                                </td>
                            </tr>
                            <tr align="center">
                              <td valign="top" class="arial14"><table width="100%" border="0" cellpadding="0" cellspacing="0" style="width: 90%; margin-right: 50px;">


                                <tr>
                                <td width="58" valign="middle" class="arial14" style="padding-right: 5px;"> <input type="radio" name="group1" id="radioCc" onchange="tipo()" value="cc"> CC</td>
                                  <td width="55" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">

                                    <tr>
                                      <td width="55" align="center" valign="top" class="arial14"> <input name="banco" type="text" class="textfieldCentrado" id="banco" value="<?=$num_cuenta_banco?>" size="6" maxlength="4"  pattern="[0-9]{4}" onchange="cambiar_iban2()" readOnly/></td>
                              </tr>
                                  </table>
                            </td>
                                  <td width="23" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">


                                    <tr>
                                      <td width="4" >&nbsp;</td>
                                      <td width="14" valign="top" class="arial14">&#8212;</td>
                              <td width="5">&nbsp;</td>
                                    </tr>
                                  </table>                                  </td>
                                  <td width="67" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">

                                    <tr>
                                      <td width="67" align="center" valign="top" class="arial14"> <input name="sucursal" type="text" class="textfieldCentrado" id="sucursal" value="<?=$num_cuenta_sucursal?>" size="6" maxlength="4" pattern="[0-9]{4}" onchange="cambiar_iban();" readOnly/></td>

                              </tr>
                                  </table>                                  </td>
                                  <td width="26" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">

                                    <tr>
                                      <td width="6" >&nbsp;</td>
                                      <td width="14" valign="top" class="arial14">&#8212;</td>
                              <td width="6">&nbsp;</td>
                                    </tr>
                                  </table>                                  </td>
                                  <td width="76" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">

                                    <tr>
                                      <td width="76" align="center" valign="top" class="arial14"> <input name="sucursal2" type="text" class="textfieldCentrado" id="sucursal2" value="<?=$num_cuenta_digito_control?>" size="4"  maxlength="2"  pattern="[0-9]{2}" onchange="cambiar_iban();" readOnly/></td>
                              </tr>
                                  </table>                                  </td>
                                  <td width="26" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">

                                    <tr>
                                      <td width="6" >&nbsp;</td>
                                      <td width="14" valign="top" class="arial14">&#8212;</td>
                              <td width="6">&nbsp;</td>
                                    </tr>
                                  </table>                                  </td>
                                  <td width="119" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">

                                    <tr>
                                      <td width="119" align="center" valign="top" class="arial14"> <input name="sucursal3" type="text" class="textfieldCentrado" id="sucursal3" value="<?=$num_cuenta_cuenta?>" size="12" pattern="[0-9]{10}" maxlength="10"  onchange="cambiar_iban();" readOnly /></td>
                            </tr>
                                  </table>                                  </td>
                            </tr>

                              </table>
                                                          </td>
                            </tr>
                            </table>
                            <!--//////////////////////////////////////////////////////////////////////////////////////////////////////////-->

                            <!--IBAN-->
                             <table width="500" align="center" border="0">
                            <tr align="center">
                              <td valign="top" class="arial14"><table width="100%" border="0" cellpadding="0" cellspacing="0">

                                <tbody><tr>
                               <td width="220" valign="middle" class="arial14" style="padding-right: 5px;"> <input type="radio" name="group1" id="radioIban" value="Iban" onchange="tipo()" checked="checked">IBAN</td>
                                  <td width="55" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">

                                    <tbody><tr>
                                      <td width="55" align="center" valign="top" class="arial14"> <input name="iban1" type="text" class="textfieldCentrado" id="iban1" value="<?=$iban1?>" size="3" maxlength="4"  onchange="cambios('cambiodomiciliacion')"></td>
                              </tr>
                                  </tbody></table>
                            </td>
                                  <td width="23" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">


                                    <tbody><tr>


                                    </tr>
                                  </tbody></table>                                  </td>

                                  <td width="55" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">

                                    <tbody><tr>
                                      <td width="55" align="center" valign="top" class="arial14"> <input name="iban2" type="text" class="textfieldCentrado" id="iban2" value="<?=$iban2?>" size="3" maxlength="4" onchange="cambiar_cb2()"></td>
                              </tr>
                                  </tbody></table>
                            </td>
                                  <td width="23" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">


                                    <tbody><tr>


                                    </tr>
                                  </tbody></table>                                  </td>


                                  <td width="67" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">

                                    <tbody><tr>
                                      <td width="67" align="center" valign="top" class="arial14"> <input name="iban3" type="text" class="textfieldCentrado" id="iban3" value="<?=$iban3?>" size="3" maxlength="4" onchange="cambiar_cb();"></td>

                              </tr>
                                  </tbody></table>                                  </td>
                                  <td width="26" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">

                                    <tbody><tr>


                                    </tr>
                                  </tbody></table>                                  </td>
                                  <td width="76" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">

                                    <tbody><tr>
                                      <td width="76" align="center" valign="top" class="arial14"> <input name="iban4" type="text" class="textfieldCentrado" id="iban4" value="<?=$iban4?>" size="3" maxlength="4" onchange="cambiar_cb();"></td>
                              </tr>
                                  </tbody></table>                                  </td>

                                   <td width="26" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">

                                    <tbody><tr>


                                    </tr>
                                  </tbody></table>                                  </td>
                                  <td width="76" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">

                                    <tbody><tr>
                                      <td width="76" align="center" valign="top" class="arial14"> <input name="iban5" type="text" class="textfieldCentrado" id="iban5" value="<?=$iban5?>" size="3" maxlength="4" onchange="cambiar_cb();"></td>
                            </tr>
                                  </tbody></table>

                                   <td width="26" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">

                                    <tbody><tr>


                                    </tr>
                                  </tbody></table>                                  </td>
                                  <td width="76" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">

                                    <tbody><tr>
                                      <td width="76" align="center" valign="top" class="arial14"> <input name="iban6" type="text" class="textfieldCentrado" id="iban6" value="<?=$iban6?>" size="3" maxlength="4"  onchange="cambiar_cb();"></td>
                            </tr>
                                  </tbody></table>
											 <td width="26" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">

                                    <tbody><tr>


                                    </tr>
                                  </tbody></table>                                  </td>
                                  <td width="76" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">

                                    <tbody><tr>

                                      <td width="76" align="center" valign="top" class="arial14"> <input name="iban7" type="text" class="textfieldCentrado"  id="iban7" value="<?=$iban7?>" size="3" maxlength="4" <?=($BancoExtranjero=='')?'readOnly':''?> onchange="cambios('cambiodomiciliacion');"></td>
                            </tr>
                                  </tbody></table>                                  </td>
								    <td width="50" valign="middle" class="arial14" style="padding-right: 5px;padding-left: 5px;">CEE </td>
                                   <td width="76" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">

                                    <tbody><tr>
                                      <td width="76" align="center" valign="top" class="arial14"> <select name="BancoExtranjero" id="BancoExtranjero" style="height:22px" onchange="cee();">
                          <option value="" <?=($BancoExtranjero=='')?'selected="selected"':''?>>
                          No                          </option>
                          <option value="Si" <?=($BancoExtranjero!='')?'selected="selected"':''?>>
                          Si                          </option>
                        </select></td>
                            </tr>
                                  </tbody></table>
                                   <td width="50" valign="middle" class="arial14" style="padding-right: 5px;">*</td>

                            </tr>
                              </tbody></table>
                              <tbody><tr>

                                <td width="50" valign="middle" class="arial14" style="padding-right: 5px;font-size: 11px;"></td>
                            </tr>



                                <td width="50" valign="middle" class="arial14" style="padding-right: 5px;font-size: 11px;">* Debe activarse Si, cuando el IBAN corresponda fuera de cualquier pais de la Comunidad Econ&oacute;mica Europea. Tambi&eacute;n es valido para cualquier pais extranjero, fuera de la Comunidad Econ&oacute;mica Europea.</td>
                            </tr>
                              </tbody></table>
 								</fieldset>

                            <tr>
                      	<td >&nbsp; </td>
                      </tr>
   					  <tr>
                      <td width="650" align="center" valign="top" class="arial14"><div align="left">
                          <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 139'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                                      &nbsp;:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input name="entidad3" type="text" class="textfieldLectura" id="entidad3"  size="42" maxlength="50" value="<?=$entidad2?>" readonly="readonly"/>
          </div></td>
                      </tr>
                      <tr>
                      	<td >&nbsp; </td>
                      </tr>

                          </table>
                        </fieldset>
          </div>

          <!-- OBSERVACIONES -->
          <div class="box-form" style="border-radius:10px; margin-top:20px;">
            <fieldset style="padding:15px; text-align:left; display:none" class="box-4-4" id="observaciones">
             <legend class="arialblack14">				
             <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto =152');
			 $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
             </legend>
             <table width="600" align="center">
             <tr>
				   <td align="center" class="arial12">
                     Escriba aqui cualquier indicacion que debamos tener en cuenta al procesar los cambios.
                   </td>
                </tr>
                <tr>
				   <td align="center" class="arial12">
                     <textarea name="Observaciones" cols="50" rows="10" id="Observaciones" ><?=$Observaciones?></textarea>
                   </td>
                </tr>
             </table>
          </fieldset>
         </div>
         <!--  SECCIONES DE CAMBIO -->
         <fieldset style="display:none">
              <legend class="arialblack14"> Tipo de Solicitudes de Cambio</legend>
                 <table width="350" align="center">
                      <tr>
                      	<td width="25"><input name="cambiotitular" id="cambiotitular" type="checkbox" value="Si" readonly="readonly" onclick="javascript: return false;"/></td>
                      	<td width="313"><div align="left">Cambio de Titular</div></td>
                      </tr>

                      <tr>
                          <td><input name="cambiofactura" id="cambiofactura" type="checkbox" value="Si" readonly="readonly"onclick="javascript: return false;"/></td>
                          <td> <div align="left">Cambio de Datos de Facturación</div></td>
                      </tr>

                      <tr>
                          <td><input name="cambioenvio" id="cambioenvio" type="checkbox" value="Si" readonly="readonly" onclick="javascript: return false;"/></td>
                          <td><div align="left">Cambio de Datos de Envío</div></td>
                      </tr>

                      <tr>
                          <td><input name="cambiopagador" id="cambiopagador" type="checkbox" value="Si" readonly="readonly" onclick="javascript: return false;"/></td>
                          <td> <div align="left">Cambio de Datos de Pagador </div></td>
                      </tr>

                      <tr>
                          <td><input name="cambiodomiciliacion" id="cambiodomiciliacion" type="checkbox" value="Si" readonly="readonly" onclick="javascript: return false;"/></td>
                          <td><div align="left">Cambio Domiciliación Bancaria</div></td>
                      </tr>

                      <tr>
                          <td><input name="cambiorepresentante" id="cambiorepresentante" type="checkbox" value="Si" readonly="readonly" onclick="javascript: return false;" /></td>
                          <td><div align="left">Cambio Representante Empresa<br /></div></td>
                      </tr>
                  </table>
         </fieldset>
        <BR><BR>
         <!-- CONDICIONES Y SUBMIT -->
         <table width="550" border="0" cellpadding="0" cellspacing="0"  style="margin:auto; margin-bottom:20px; text-align:center" id="botonEnviar">
            <tr>
	                <td align="center" class="clr-4 bold">
                    <fieldset style="border-radius:10px; margin-bottom:10px">
                        <table border="0">
                        <tr><td width="100"><img src="../img/oficina/info.png" width="60"/></td>
						<td style="font-size:12px; text-align: justify;">
                        Puede introducir cualquier dato en aquel campo que desee modificar y después activar la opción de "Enviar" que se encuentra al final del formulario.
                        <br><br>
                        Estos cambios deben ser validados por la empresa y recibirá conformidad de dichas modificaciones en un plazo estimado de 48 horas.
                        </td></tr></table>
                    </fieldset>
                    <input name="Condiciones" type="checkbox" value="Yes" id="Condiciones" style="width:auto"/>He leído y Acepto las Condiciones de Modificación<br/>
                </tr>

			<tr>
	            <td align="center" class="clr-4 bold submit">
                    <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 153'); $rowTexto = mysql_fetch_array($resultTexto);?>
                    <!--<input name="submit" type="submit" id="submit" class="button" value="<?php echo($rowTexto['Texto']);?>"> -->
                    <input name="submit" type="submit" id="submit" class="button" value="<?php echo($rowTexto['Texto']);?>" >
                </td>
             </tr>
  		 </table>
        </form>
		</div><!--<div class="contenido_seccion_oficina">-->


        <div class="limpiar"></div>

<!--Por ultimo aparecera el pie de la web-->
<?php
//	include($_SESSION["directorio_raiz"]."includes/pie.php");
	include("../includes/pie.php");


//NOTA: EN ESTA OCASION, EL ERROR SE MOSTRARA DESPUES DE LA CARGA DE LA PAGINA, SI NO POR ALGUN MOTIVO EN OCASIONES DESPUES DE DAR EL ERROR CESA LA CARGA DE LA PAGINA
	if(isset($_POST["submit"]))
	{
//Por ultimo mostraremos el error correspondiente, en el caso de que haya fallado alguna de las verificaciones, o continuaremos con el proceso
		switch($error)
		{
//Proceso OK
			case 0:
//Antes de realizar el alta en la base de datos, deberemos de preparar los datos que no nos da el formulario

//Como en este caso el cliente ya existe, el codigo del cliente no hace falta calcularlo. Lo mismo sucede con el CUPS

				$cod_usuario=$row["CodigoCliente"];
				$cambiosecciontitular="Si";


//La forma de pago envia un indice y no la descripcion de la forma de pago, ahora lo sustituiremos antes de dar el alta
				switch($forma_pago)
				{
					case 1:
						$forma_pago="GESTION BANCARIA";
					break;

					case 2:
						$forma_pago="No domiciliado";
					break;
				}//switch($forma_pago)

//El idioma de facturacion envia un indice y no la descripcion del idioma, ahora lo sustituiremos antes de dar el alta
				switch($idioma_factura)
				{
					case 0:
						$idioma_factura="Castellano";
					break;

					case 1:
						$idioma_factura="Catalán";
					break;

					case 2:
						$idioma_factura="Euskera";
					break;

					case 3:
						$idioma_factura="Gallego";
					break;

					case 4:
						$idioma_factura="Bable (Asturias)";
					break;
				}//switch($idioma_factura)


//Tambien preparamos el numero de la cuenta que sera la concatenacion de todos los campos que lo forman
				$num_cuenta_completo=$num_cuenta_banco.$num_cuenta_sucursal.$num_cuenta_digito_control.$num_cuenta_cuenta;
				$iban_completo=$iban1.$iban2.$iban3.$iban4.$iban5.$iban6.$iban7;

//LOLOLO CONSULTAR DATOS PARA TIPO DE SUMINISTRO Y DATOS DEL SUMINISTRO

//A continuacion asignaremos el valor a las variables, que como no nos han hecho falta hasta ahora no habiamos asignado de la consulta de los datos actuales del cliente. En este caso los datos son los del suministro

				$suministro_poblacion=$row["SuministroCiudad"];
				$suministro_provincia=$row["SuministroProvincia"];
				$suministro_calle=$row["SuministroCalle"];
				$suministro_numero=$row["SuministroNumero"];
				$suministro_extension=$row["SuministroExtension"];
				$suministro_aclarador=$row["SuministroExtension2"];
				$suministro_cp=$row["SuministroCP"];
				$suministro_telefono=$row["SuministroTelefono1"];
				$suministro_movil=$row["SuministroTelefono2"];
				$suministro_fax=$row["SuministroFax"];
				$suministro_email=$row["SuministroEmail"];

				$tarifa_contratada=$row["TarifaContratada"];
				$potencia_contratada=$row["PotenciaContratada"];
				$tipo_tension=$row["TipoTension"];
				$discriminador=$row["Discriminador"];
				$tipo_alquiler=$row["TipoAlquiler"];
				$maximetro=$row["Maximetro"];
				$modo=$row["Modo"];
				$observaciones=$row["Observaciones"];
				$fases=$row["Fases"];
				$reactiva=$row["Reactiva"];

				$p1_activa=$row["P1Activa"];
				$p1_reactiva=$row["P1Reactiva"];
				$p1_maximetro=$row["P1Maximetro"];

				$p2_activa=$row["P2Activa"];
				$p2_reactiva=$row["P2Reactiva"];
				$p2_maximetro=$row["P2Maximetro"];

				$p3_activa=$row["P3Activa"];
				$p3_reactiva=$row["P3Reactiva"];
				$p3_maximetro=$row["P3Maximetro"];

				$p4_activa=$row["P4Activa"];
				$p4_reactiva=$row["P4Reactiva"];
				$p4_maximetro=$row["P4Maximetro"];

				$p5_activa=$row["P5Activa"];
				$p5_reactiva=$row["P5Reactiva"];
				$p5_maximetro=$row["P5Maximetro"];

				$p6_activa=$row["P6Activa"];
				$p6_reactiva=$row["P6Reactiva"];
				$p6_maximetro=$row["P6Maximetro"];

//Las modificaciones en realidad no cambian los datos de la tabla DatosRegistrados, si no que dan un alta en la tabla DatosModificados. Aunque se hace la inserccion completa, algunos campos quedaran en blanco, ya que se cambian desde la seccion de oficina_modificacion_datossuministro

//Ahora dependiendo de si el usuario ya tiene un registro o no en la tabla de DatosModificados, se realizara un alta nueva o una actualizacion del registro

		                    $consulta_bancos = mysql_query("SELECT * FROM EntidadesBancarias ORDER BY NombreBanco ASC ");
							while ($registro_banco = mysql_fetch_array($consulta_bancos))

							{      if ($registro_banco["CodigoOficial"]==$num_cuenta_banco)

											{$nombrebanco= $registro_banco["NombreBanco"];}
											else{}


                            }


						$forma_pago="GESTION BANCARIA";

						$nombrebanco=str_replace("'","\'",$nombrebanco);

					if($tiene_modificaciones_pendientes==0)

					{

						$solicitud_modificacion="INSERT INTO `DatosModificados` (`CodigoCliente`, `Usuario`, `Password`, `DNI`, `SuministroCiudad`, `SuministroProvincia`, `SuministroCalle`, `SuministroNumero`, `SuministroExtension`, `SuministroExtension2`, `SuministroCP`, `SuministroTelefono1`, `SuministroTelefono2`, `SuministroFax`, `SuministroEmail`, `TitularNombre`, `TitularCalle`, `TitularNumero`, `TitularExtension`, `TitularAclarador`, `TitularCP`, `TitularPoblacion`, `TitularProvincia`, `TitularPais`,  `TitularTelefono1`, `TitularTelefono2`, `TitularFax`, `TitularEmail`, `TitularDNI`, `FacturaNombre`, `FacturaCalle`, `FacturaNumero`, `FacturaExtension`, `FacturaAclarador`,`FacturaCP`, `FacturaPoblacion`, `FacturaProvincia`,`FacturaPais`, `FacturaTelefono1`, `FacturaTelefono2`, `FacturaFax`, `FacturaEmail`, `FacturaDNI`, `EnvioNombre`, `EnvioCalle`, `EnvioNumero`, `EnvioExtension`, `EnvioAclarador`,`EnvioCP`, `EnvioPoblacion`, `EnvioProvincia`,`EnvioPais`, `EnvioTelefono1`, `EnvioTelefono2`, `EnvioFax`, `EnvioEmail`, `EnvioDNI`, `PagadorNombre`, `PagadorCalle`, `PagadorNumero`, `PagadorExtension`, `PagadorAclarador`, `PagadorCP`, `PagadorPoblacion`, `PagadorProvincia`, `PagadorPais`,`PagadorTelefono1`, `PagadorTelefono2`, `PagadorFax`, `PagadorEmail`, `PagadorDNI`, `RepresentanteEmpresa`, `DNIRepresentante`, `CUPS`, `FormaPago`, `BancoDomiciliado`, `NumeroCuenta`, `IdiomaFactura`, `TarifaContratada`, `PotenciaContratada`, `TipoTension`, `Discriminador`, `TipoAlquiler`, `Maximetro`, `Modo`, `Observaciones`, `FechaRegistro`, `HoraRegistro`, `Fases`, `Reactiva`, `P1Activa`, `P1Reactiva`, `P1Maximetro`, `P2Activa`, `P2Reactiva`, `P2Maximetro`, `P3Activa`, `P3Reactiva`, `P3Maximetro`, `P4Activa`, `P4Reactiva`, `P4Maximetro`, `P5Activa`, `P5Reactiva`, `P5Maximetro`, `P6Activa`, `P6Reactiva`, `P6Maximetro`, `EstadoBonoSocial`,`Comercializadora`,`Distribuidora`, `CambioSeccionBanco`, `CambioSeccionDatosTecnicos`, `cambiotitular`, `cambiofactura`, `cambioenvio`, `cambiopagador`, `cambiodomiciliacion`, `cambiorepresentante`, `Iban`, `BancoExtranjero`) VALUES ('".$cod_usuario."', '".$_SESSION['usuario']."', '".$_SESSION['password']."', '".$_SESSION['DNI']."', '".$suministro_poblacion."', '".$suministro_provincia."', '".$suministro_calle."', '".$suministro_numero."', '".$suministro_extension."', '".$suministro_aclarador."', '".$suministro_cp."','".$suministro_telefono."', '".$suministro_movil."', '".$suministro_fax."', '".$suministro_email."', '".$titular_nombre."','".$titular_calle."','".$titular_numero."' , '".$titular_extension."', '".$titular_aclarador."', '".$titular_cp."','".$titular_poblacion."', '".$titular_provincia."', '".$titular_pais."', '".$titular_telefono."', '".$titular_movil."', '".$titular_fax."', '".$titular_email."', '".$titular_dni."', '".$factura_nombre."', '".$factura_calle."', '".$factura_numero."', '".$factura_extension."', '".$factura_aclarador."', '".$factura_cp."' , '".$factura_poblacion."', '".$factura_provincia."', '".$factura_pais."', '".$factura_telefono."', '".$factura_movil."', '".$factura_fax."', '".$factura_email."', '".$factura_dni."', '".$envio_nombre."', '".$envio_calle."', '".$envio_numero."', '".$envio_extension."', '".$envio_aclarador."', '".$envio_cp."' , '".$envio_poblacion."', '".$envio_provincia."', '".$envio_pais."', '".$envio_telefono."', '".$envio_movil."', '".$envio_fax."', '".$envio_email."', '".$envio_dni."', '".$pagador_nombre."', '".$pagador_calle."', '".$pagador_numero."', '".$pagador_extension."', '".$pagador_aclarador."', '".$pagador_cp."', '".$pagador_poblacion."', '".$pagador_provincia."', '".$pagador_pais."', '".$pagador_telefono."', '".$pagador_movil."', '".$pagador_fax."', '".$pagador_email."', '".$pagador_dni."', '".$representante_nombre."', '".$representante_dni."', '".$_POST["CUPSFijo"]."', '".$forma_pago."', '".$_POST['entidad3']."', '".$num_cuenta_completo."','".$idioma_factura."','".$tarifa_contratada."','".$potencia_contratada."', '".$tipo_tension."', '".$discriminador."','".$tipo_alquiler."','".$maximetro."', '".$modo."', '".$Observaciones."', '".date("Y-m-d")."', '".date("Y-m-d H:i:s")."','".$fases."', '".$reactiva."', '".$p1_activa."', '".$p1_reactiva."', '".$p1_maximetro."', '".$p2_activa."', '".$p2_reactiva."', '".$p2_maximetro."', '".$p3_activa."', '".$p3_reactiva."', '".$p3_maximetro."', '".$p4_activa."', '".$p4_reactiva."', '".$p4_maximetro."', '".$p5_activa."', '".$p5_reactiva."', '".$p5_maximetro."', '".$p6_activa."', '".$p6_reactiva."', '".$p6_maximetro."', '".$estadobonosocial."', '".$comercializadora."', '".$distribuidora."', '".$cambioseccionbanco."', '".$cambiosecciondatostecnicos."' , '".$_POST['cambiotitular']."' , '".$_POST['cambiofactura']."' , '".$_POST['cambioenvio']."' , '".$_POST['cambiopagador']."' , '".$_POST['cambiodomiciliacion']."' , '".$_POST['cambiorepresentante']."' , '".$iban_completo."' , '".$BancoExtranjero."');";



				}//if($tiene_modificaciones_pendientes==0)

//Si el usuario ya tenia un registro en la base de datos, se realizara una actualizacion del registro
				else
				{

					$solicitud_modificacion="UPDATE `DatosModificados` SET `TitularNombre` = '".$titular_nombre."', `TitularCalle` = '".$titular_calle."',`TitularNumero` = '".$titular_numero."', `TitularExtension` = '".$titular_extension."',`TitularAclarador` = '".$titular_aclarador."', `TitularPoblacion` = '".$titular_poblacion."',`TitularProvincia` = '".$titular_provincia."',`TitularPais` = '".$titular_pais."',`TitularTelefono1` = '".$titular_telefono."', `TitularTelefono2` = '".$titular_movil."',`TitularFax` = '".$titular_fax."',`TitularEmail` = '".$titular_email."', `TitularDNI` = '".$titular_dni."',`FacturaNombre` = '".$factura_nombre."',`FacturaCalle` = '".$factura_calle."',`FacturaNumero` = '".$factura_numero."',`FacturaExtension` = '".$factura_extension."',`FacturaAclarador` = '".$factura_aclarador."',`FacturaPoblacion` = '".$factura_poblacion."',`FacturaProvincia` = '".$factura_provincia."',`FacturaPais` = '".$factura_pais."',`FacturaTelefono1` = '".$factura_telefono."',`FacturaTelefono2` = '".$factura_movil."',`FacturaFax` = '".$factura_fax."',`FacturaEmail` = '".$factura_email."',`FacturaDNI` = '".$factura_dni."',`EnvioNombre` = '".$envio_nombre."',`EnvioCalle` = '".$envio_calle."',`EnvioNumero` = '".$envio_numero."',`EnvioExtension` = '".$envio_extension."',`EnvioAclarador` = '".$envio_aclarador."',`EnvioCP` = '".$envio_cp."',`EnvioPoblacion` = '".$envio_poblacion."',`EnvioProvincia` = '".$envio_provincia."',`EnvioPais` = '".$envio_pais."',`EnvioTelefono1` = '".$envio_telefono."',`EnvioTelefono2` = '".$envio_movil."',`EnvioFax` = '".$envio_fax."',`EnvioEmail` = '".$envio_email."',`EnvioDNI` = '".$envio_dni."',`PagadorNombre` = '".$pagador_nombre."',`PagadorCalle` = '".$pagador_calle."',`PagadorNumero` = '".$pagador_numero."',`PagadorExtension` = '".$pagador_extension."',`PagadorAclarador` = '".$pagador_aclarador."',`PagadorCP` = '".$pagador_cp."',`PagadorPoblacion` = '".$pagador_poblacion."',`PagadorProvincia` = '".$pagador_provincia."',`PagadorPais` = '".$pagador_pais."',`PagadorTelefono1` = '".$pagador_telefono."',`PagadorTelefono2` = '".$pagador_movil."',`PagadorFax` = '".$pagador_fax."',`PagadorEmail` = '".$pagador_email."',`PagadorDNI` = '".$pagador_dni."',`RepresentanteEmpresa` = '".$representante_nombre."',`DNIRepresentante` = '".$representante_dni."',`FormaPago` = '".$forma_pago."',`BancoDomiciliado` = '".$_POST['entidad3']."',`NumeroCuenta` = '".$num_cuenta_completo."',`IdiomaFactura` = '".$idioma_factura."',`Observaciones` = '".$Observaciones."',`FechaRegistro` = '".date("Y-m-d")."',`HoraRegistro` = '".date("Y-m-d H:i:s")."',`cambiotitular` = '".$_POST['cambiotitular']."',`cambiofactura` = '".$_POST['cambiofactura']."',`cambioenvio` = '".$_POST['cambioenvio']."',`cambiopagador` = '".$_POST['cambiopagador']."',`cambiodomiciliacion` = '".$_POST['cambiodomiciliacion']."',`cambiorepresentante` = '".$_POST['cambiorepresentante']."',`Iban` = '".$iban_completo."',`BancoExtranjero` = '".$BancoExtranjero."' WHERE `CodigoCliente` =".$cod_usuario.";";



				}//else($tiene_modificaciones_pendientes==0)

				$seccion_modificaciones="Titular";
				include("../includes/email_modificaciones.php");

//En cualquier caso ejecutamos la sentencia SQL contruida
				$ejecutar_solicitud_modificacion=mysql_query($solicitud_modificacion);

				if($ejecutar_solicitud_modificacion == true){
					MsgBox($modificacion_ok);
					if($error_mail==1 or $error_dni==1 or $error_telefono==1){
					?><br />
				<script type="text/javascript">
					var opciones = "width=650,height=400,scrollbars=NO,left=400,top=300";
					window.open("../includes/datos_importantes.php?mail=<?=$error_mail;?>&dni=<?=$error_dni;?>&telefono=<?=$error_telefono;?>&seccion=<?=$seccion_modificaciones;?>","Importante", opciones);
				</script>
					<?php
				}
				} else {
					MsgBox("Algo fallo! Por favor, contacte con su proveedor");
				}


			break;



//Error por no rellenar los campos obligatorios
			case 1:
				MsgBox($error_rellenar);
			break;

//Error por caracteres no numericos en telefonos o faxes
			case 2:
				MsgBox($error_numeros);
			break;

//Error en un e-mail
			case 3:
				MsgBox($error_email);
			break;

			case 4:
				MsgBox($error_dni);
			break;

//Error por no rellenar la entidad bancaria o el numero de cuenta con forma de pago domiciliación
			case 5:
				MsgBox($error_datos_bancarios);
			break;

//Error en el numero de cuenta bancaria
			case 6:
				MsgBox($error_num_cuenta);
			break;

//Error por rellenar parcialmente los datos de facturacion o los del pagador
			case 7:
				MsgBox($error_datos_incompletos);
			break;

//Error por rellenar parcialmente los datos del representante de la empresa
			case 8:
				MsgBox($error_datos_representante);
			break;

			case 9:
			    MsgBox($error_condiciones);//"No has aceptado las condiciones");
			break;

			case 10:
			    MsgBox("Tiene que elegir al menos una de las categoría que ha modificado");//"No has aceptado las condiciones");
			break;
			case 11:
			    MsgBox("El numero de cuenta no es correcto");//"Error del campo de control");
			break;
			case 12:
			    MsgBox("El iban no es correcto");//"Error del campo de control");
				//header('Location:oficina_modificacion_datostitular.php');
			break;

		}//switch($error)



	}//if (isset($_POST["submit"]))
}//else ($_SESSION['usuario'])

	//En el caso en que se acepte el formulario, como la cuenta bancaria esta compuesta por	el nombre + codigo, habra que subdividirlo para pasarle el paranetro correctamente a la funcion javascript
	$partes_entidad_bancaria=explode("+",$entidad_bancaria);

//Llamamos a la funcion javascript que da valor a los datos bancarios. La funcion recibira los valores escritos en el formulario o los que hay en la base de datos dependidendo de la situacion
	    echo("<script type='application/javascript' language='javascript'>");
		echo("dar_valor_datos_bancarios('".$forma_pago."','".$partes_entidad_bancaria[0]."','".$idioma_factura."');");
		echo("</script>");


?>




    </div><!--<div id="central_oficina"-->
</body>
</html>

