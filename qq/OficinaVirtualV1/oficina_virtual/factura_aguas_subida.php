<?php
session_start();
require('fpdf/WriteHTML.php');
//require_once 'fpdf/barcode/Image/Barcode.php';

class PDF extends PDF_HTML
{
	// Cabecera de página
	function Header()
	{
		$this->Image('../img/oficina/logo_datos.gif',10,8,130);// Logo
		$this->SetFont('Arial','B',15);// Arial bold 15
		$this->Ln(20);// Salto de línea
	}
	
	// Pie de página
	function Footer()
	{
		$this->SetY(-15);// Posición: a 1,5 cm del final
		$this->SetFont('Arial','I',8);// Arial italic 8
		$this->Cell(0,10,' ',0,0,'C');// Número de página
	}
	
	function SeccionTitle($x, $y, $width, $height, $label)
	{
		$this->SetY($y);
		$this->SetX($x);
		$this->SetFont('Arial','B',8);// Arial 12
		$this->SetFillColor(190,190,190);// Color de fondo
		$this->Cell($width,$height,$label,1,1,'C',true);		
		$this->Ln(4); // Salto de línea
	}

	function SeccionBody($x, $y, $width, $text, $height_cont)
	{
		$this->SetY($y+5);
		$this->SetX($x);
		$this->SetFont('Arial','B',8);// Arial 12
		$this->MultiCell($width,$height_cont,$text,1);
		$this->Ln(); // Salto de línea
	}

	function PrintSeccion($x, $y, $width, $height, $title, $text, $height_cont)
	{
		$this->SeccionTitle($x, $y, $width, $height, $title);
		$this->SeccionBody($x, $y, $width, $text, $height_cont);
	}
	
	function CreaCelda($x, $y, $width, $label, $r, $g, $b, $fuente, $especial, $tamano){
		$this->SetY($y);
		$this->SetX($x);
		$this->SetFont($fuente, $especial, $tamano);
		$this->SetFillColor($r,$g,$b); // Color de fondo
		$this->Cell($width,3,$label,0,0,'L',true);
	}
	function CreaCelda2($x, $y, $width, $label, $r, $g, $b){
		$this->SetY($y);
		$this->SetX($x);
		$this->SetFont('Arial','B',7);
		$this->SetFillColor($r,$g,$b); // Color de fondo
		$this->Cell($width,4,$label,0,0,'C',true);
	}
	function CreaCeldaCentrada($x, $y, $width, $label, $r, $g, $b){
		$this->SetY($y);
		$this->SetX($x);
		$this->SetFont('Arial','',7);
		$this->SetFillColor($r,$g,$b); // Color de fondo
		$this->Cell($width,3,$label,0,0,'C',true);
	}
	/*function CreaCeldaNegrita($x, $y, $width, $label, $r, $g, $b){
		$this->SetY($y);
		$this->SetX($x);
		$this->SetFont('Arial','B',9);
		$this->SetFillColor($r,$g,$b);// Color de fondo
		$this->Cell($width,3,$label,0,0,'L',true);
	}
	
	function CreaCeldaA8($x, $y, $width, $label, $r, $g, $b){
		$this->SetY($y);
		$this->SetX($x);
		$this->SetFont('Arial','',8);
		$this->SetFillColor($r,$g,$b);// Color de fondo
		$this->Cell($width,3,$label,0,0,'L',true);
	}
	function CreaCeldaA8Negrita($x, $y, $width, $label, $r, $g, $b){
		$this->SetY($y);
		$this->SetX($x);
		$this->SetFont('Arial','B',8);
		$this->SetFillColor($r,$g,$b);// Color de fondo
		$this->Cell($width,3,$label,0,0,'L',true);
	}
	function CreaCeldaA7Negrita($x, $y, $width, $label, $r, $g, $b){
		$this->SetY($y);
		$this->SetX($x);
		$this->SetFont('Arial','B',7);
		$this->SetFillColor($r,$g,$b);// Color de fondo
		$this->Cell($width,3,$label,0,0,'L',true);
	}*/
	function CreaCeldaAlDer($x, $y, $width, $label, $r, $g, $b){
		$this->SetY($y);
		$this->SetX($x);
		$this->SetFont('Arial','',7);
		$this->SetFillColor($r,$g,$b);// Color de fondo
		$this->Cell($width,3,$label,0,0,'R',true);
	}
	
	function SeccionBodyBG($x, $y, $width, $text, $height_cont,$r,$g,$b)
	{
		$this->SetY($y+5);
		$this->SetX($x);
		$this->SetFont('Times','B',8);// Times 12
		
		$this->MultiCell($width,$height_cont,$text);
		$this->SetFillColor($r,$g,$b);// Color de fondo
		$this->Ln(); // Salto de línea
	}
}

$link = mysql_connect('89.248.100.44', 'aguasbarbastro', 'oscar');
if (!$link) {die('Could not connect: ' . mysql_error());}
$db_selected = mysql_select_db('aguasbarbastro', $link);
if (!$db_selected){die ('Can\'t use foo : ' . mysql_error());}

/*
$link = mysql_connect('82.223.114.64', 'aguasbarbastro', 'oscar');
if (!$link) {die('Could not connect: ' . mysql_error());}
$db_selected = mysql_select_db('aguasbarbastro', $link);
if (!$db_selected){die ('Can\'t use foo : ' . mysql_error());}
*/

//$_SESSION['numeroCliente'] = 1003272;
$fecha=$_GET['Registro_Factura_Fecha'];
$fechafactura = $fecha." 00:00:00";
set_time_limit(0);  // sin limite
$consulta_facturas="SELECT * FROM FacturasImprimir WHERE CodAbonado = ".$_SESSION['numeroCliente']." and Fecha= '".$fechafactura."'";
$resultados_facturas = mysql_query($consulta_facturas);	
	
$pdf=new PDF();

while($registro_factura = mysql_fetch_array($resultados_facturas))
{
   	if($registro_factura["Fecha"]==$fechafactura){
	
/* RECOGIDA DE DATOS */	
//Datos de la factura	
$fecha = explode(' ',$registro_factura["Fecha"]);
$periodo_factura = $registro_factura["periodo"];
$num_factura = $registro_factura["NumFactura"];
$importe_total=number_format($registro_factura["importe"],2,",",".");

// Datos de titular
$nombre_titular = $registro_factura['NombreAbonado'];
$direccion_titular = $registro_factura['Ndomicilio1'];
$direccion_titular2 = $registro_factura['CodPostal1']." - ".$registro_factura['texto-ciudad1'];;
$dni_titular=$registro_factura['Nif'];
$cod_barras=$registro_factura['CodigoBarras'];

// Datos de suministro
$nombre_suministro = $registro_factura['NombreAbonado2'];
$direccion_suministro = $registro_factura['Ndomicilio1'];
$poblacion_suministro = $registro_factura['texto-ciudad1'];
$cups = $registro_factura['CUPS'];
$cnae = $registro_factura['cnae'];
$potencia_contratada=$registro_factura['pckw_ab'];
if($registro_factura['PotContP1']==0){$p1="";} else {$p1=$registro_factura['PotContP1'];}
if($registro_factura['PotContP2']==0){$p2="";} else {$p2=$registro_factura['PotContP2'];}
if($registro_factura['PotContP3']==0){$p3="";} else {$p3=$registro_factura['PotContP3'];}
if($registro_factura['PotContP4']==0){$p4="";} else {$p4=$registro_factura['PotContP4'];}
if($registro_factura['PotContP5']==0){$p5="";} else {$p5=$registro_factura['PotContP5'];}
if($registro_factura['PotContP6']==0){$p6="";} else {$p6=$registro_factura['PotContP6'];}
$tarifa=$registro_factura['nombretarifa'];
if($registro_factura['TensionSuministro']==""){$tension = " ";} else {$tension = $registro_factura['TensionSuministro'];}
if($registro_factura['contadoractiva']==""){$contador=" ";}else{$contador=$registro_factura['contadoractiva'];}
$ref_catastral=$registro_factura['ReferenciaCatastral'];

//Datos de pago
$forma_pago = $registro_factura['FormaPago'];
$banco = $registro_factura['ban2-nom'];
$num_cuenta_banco=substr($registro_factura["numcuenta"],0,4);
$num_cuenta_sucursal=substr($registro_factura["numcuenta"],4,4);
$num_cuenta_digito_control=substr($registro_factura["numcuenta"],8,2);
$num_cuenta_cuenta=substr($registro_factura["numcuenta"],10,10);
$num_cuenta = $num_cuenta_banco.' - '. $num_cuenta_sucursal.' - '.$num_cuenta_digito_control.' - '.substr($num_cuenta_cuenta,1,5).'*****';
if($num_cuenta!="" and $banco!=""){
	$forma_pago="BANCO";}
$base1 = number_format($registro_factura["Base1"],2,",",".");
$iva1 = number_format($registro_factura["Iva1"],2,",",".");
$cuota1 = number_format($registro_factura["Cuota1"],2,",",".");
$base2 = number_format($registro_factura["Base2"],2,",",".");
$iva2 = number_format($registro_factura["Iva2"],2,",",".");
$cuota2 = number_format($registro_factura["Cuota2"],2,",",".");
$total_factura = number_format($registro_factura["Total"],2,",",".");

$plazo=$registro_factura['plazo1'];

//Lecturas
if($registro_factura['LecturaP1Ant']==0){$lecturaP1ant='-';}else{$lecturaP1ant=number_format($registro_factura['LecturaP1Ant'],0,",",".");}
if($registro_factura['LecturaP2Ant']==0){$lecturaP2ant='-';}else{$lecturaP2ant=number_format($registro_factura['LecturaP2Ant'],0,",",".");}
if($registro_factura['LecturaP3Ant']==0){$lecturaP3ant='-';}else{$lecturaP3ant=number_format($registro_factura['LecturaP3Ant'],0,",",".");}
if($registro_factura['LecturaP4Ant']==0){$lecturaP4ant='-';}else{$lecturaP4ant=number_format($registro_factura['LecturaP4Ant'],0,",",".");}
if($registro_factura['LecturaP5Ant']==0){$lecturaP5ant='-';}else{$lecturaP5ant=number_format($registro_factura['LecturaP5Ant'],0,",",".");}
if($registro_factura['LecturaP6Ant']==0){$lecturaP6ant='-';}else{$lecturaP6ant=number_format($registro_factura['LecturaP6Ant'],0,",",".");}

if($registro_factura['LecturaP1Act']==0){$lecturaP1act='-';}else{$lecturaP1act=number_format($registro_factura['LecturaP1Act'],0,",",".");}
if($registro_factura['LecturaP2Act']==0){$lecturaP2act='-';}else{$lecturaP2act=number_format($registro_factura['LecturaP2Act'],0,",",".");}
if($registro_factura['LecturaP3Act']==0){$lecturaP3act='-';}else{$lecturaP3act=number_format($registro_factura['LecturaP3Act'],0,",",".");}
if($registro_factura['LecturaP4Act']==0){$lecturaP4act='-';}else{$lecturaP4act=number_format($registro_factura['LecturaP4Act'],0,",",".");}
if($registro_factura['LecturaP5Act']==0){$lecturaP5act='-';}else{$lecturaP5act=number_format($registro_factura['LecturaP5Act'],0,",",".");}
if($registro_factura['LecturaP6Act']==0){$lecturaP6act='-';}else{$lecturaP6act=number_format($registro_factura['LecturaP6Act'],0,",",".");}

if($registro_factura['LecturaReacP1Ant']==0){$lecturaReacP1ant='-';}else{$lecturaReacP1ant=number_format($registro_factura['LecturaReacP1Ant'],0,",",".");}
if($registro_factura['LecturaReacP2Ant']==0){$lecturaReacP2ant='-';}else{$lecturaReacP2ant=number_format($registro_factura['LecturaReacP2Ant'],0,",",".");}
if($registro_factura['LecturaReacP3Ant']==0){$lecturaReacP3ant='-';}else{$lecturaReacP3ant=number_format($registro_factura['LecturaReacP3Ant'],0,",",".");}
if($registro_factura['LecturaReacP4Ant']==0){$lecturaReacP4ant='-';}else{$lecturaReacP4ant=number_format($registro_factura['LecturaReacP4Ant'],0,",",".");}
if($registro_factura['LecturaReacP5Ant']==0){$lecturaReacP5ant='-';}else{$lecturaReacP5ant=number_format($registro_factura['LecturaReacP5Ant'],0,",",".");}
if($registro_factura['LecturaReacP6Ant']==0){$lecturaReacP6ant='-';}else{$lecturaReacP6ant=number_format($registro_factura['LecturaReacP6Ant'],0,",",".");}

if($registro_factura['LecturaReacP1Act']==0){$lecturaReacP1act='-';}else{$lecturaReacP1act=number_format($registro_factura['LecturaReacP1Act'],0,",",".");}
if($registro_factura['LecturaReacP2Act']==0){$lecturaReacP2act='-';}else{$lecturaReacP2act=number_format($registro_factura['LecturaReacP2Act'],0,",",".");}
if($registro_factura['LecturaReacP3Act']==0){$lecturaReacP3act='-';}else{$lecturaReacP3act=number_format($registro_factura['LecturaReacP3Act'],0,",",".");}
if($registro_factura['LecturaReacP4Act']==0){$lecturaReacP4act='-';}else{$lecturaReacP4act=number_format($registro_factura['LecturaReacP4Act'],0,",",".");}
if($registro_factura['LecturaReacP5Act']==0){$lecturaReacP5act='-';}else{$lecturaReacP5act=number_format($registro_factura['LecturaReacP5Act'],0,",",".");}
if($registro_factura['LecturaReacP6Act']==0){$lecturaReacP6act='-';}else{$lecturaReacP6act=number_format($registro_factura['LecturaReacP6Act'],0,",",".");}

if($registro_factura['LecturaMaxP1']==0){$lecturaMaxP1='-';}else{$lecturaMaxP1=number_format($registro_factura['LecturaMaxP1'],2,",",".");}
if($registro_factura['LecturaMaxP2']==0){$lecturaMaxP2='-';}else{$lecturaMaxP2=number_format($registro_factura['LecturaMaxP2'],2,",",".");}
if($registro_factura['LecturaMaxP3']==0){$lecturaMaxP3='-';}else{$lecturaMaxP3=number_format($registro_factura['LecturaMaxP3'],2,",",".");}
if($registro_factura['LecturaMaxP4']==0){$lecturaMaxP4='-';}else{$lecturaMaxP4=number_format($registro_factura['LecturaMaxP4'],2,",",".");}
if($registro_factura['LecturaMaxP5']==0){$lecturaMaxP5='-';}else{$lecturaMaxP5=number_format($registro_factura['LecturaMaxP5'],2,",",".");}
if($registro_factura['LecturaMaxP6']==0){$lecturaMaxP6='-';}else{$lecturaMaxP6=number_format($registro_factura['LecturaMaxP6'],2,",",".");}


//Calculo de factura
$textoPotP1=$registro_factura['TextoPotP1'];
$textoPotP2=$registro_factura['TextoPotP2'];
$textoPotP3=$registro_factura['TextoPotP3'];
$textoPotP4=$registro_factura['TextoPotP4'];
$textoPotP5=$registro_factura['TextoPotP5'];
$textoPotP6=$registro_factura['TextoPotP6'];

$importePotP1=number_format($registro_factura['ImportePotP1'],2,",",".");
$importePotP2=number_format($registro_factura['ImportePotP2'],2,",",".");
$importePotP3=number_format($registro_factura['ImportePotP3'],2,",",".");
$importePotP4=number_format($registro_factura['ImportePotP4'],2,",",".");
$importePotP5=number_format($registro_factura['ImportePotP5'],2,",",".");
$importePotP6=number_format($registro_factura['ImportePotP6'],2,",",".");

$textoEnerP1=$registro_factura['TextoEnerP1'];
$textoEnerP2=$registro_factura['TextoEnerP2'];
$textoEnerP3=$registro_factura['TextoEnerP3'];
$textoEnerP4=$registro_factura['TextoEnerP4'];
$textoEnerP5=$registro_factura['TextoEnerP5'];
$textoEnerP6=$registro_factura['TextoEnerP6'];

$importeEnerP1=number_format($registro_factura['ImporteEnerP1'],2,",",".");
$importeEnerP2=number_format($registro_factura['ImporteEnerP2'],2,",",".");
$importeEnerP3=number_format($registro_factura['ImporteEnerP3'],2,",",".");
$importeEnerP4=number_format($registro_factura['ImporteEnerP4'],2,",",".");
$importeEnerP5=number_format($registro_factura['ImporteEnerP5'],2,",",".");
$importeEnerP6=number_format($registro_factura['ImporteEnerP6'],2,",",".");

$textoReacP1=$registro_factura['TextoReacP1'];
$textoReacP2=$registro_factura['TextoReacP2'];
$textoReacP3=$registro_factura['TextoReacP3'];
$textoReacP4=$registro_factura['TextoReacP4'];
$textoReacP5=$registro_factura['TextoReacP5'];
$textoReacP6=$registro_factura['TextoReacP6'];

$importeReacP1=number_format($registro_factura['ImporteReacP1'],2,",",".");
$importeReacP2=number_format($registro_factura['ImporteReacP2'],2,",",".");
$importeReacP3=number_format($registro_factura['ImporteReacP3'],2,",",".");
$importeReacP4=number_format($registro_factura['ImporteReacP4'],2,",",".");
$importeReacP5=number_format($registro_factura['ImporteReacP5'],2,",",".");
$importeReacP6=number_format($registro_factura['ImporteReacP6'],2,",",".");

$textoDtoPotP1 = $registro_factura['TextoDtoPotP1'];
$textoDtoPotP2 = $registro_factura['TextoDtoPotP2'];
$textoDtoPotP3 = $registro_factura['TextoDtoPotP3'];
$textoDtoPotP4 = $registro_factura['TextoDtoPotP4'];
$textoDtoPotP5 = $registro_factura['TextoDtoPotP5'];
$textoDtoPotP6 = $registro_factura['TextoDtoPotP6'];

$importeDtoPotP1 = number_format($registro_factura['ImporteDtoPotP1'],2,",",".");
$importeDtoPotP2 = number_format($registro_factura['ImporteDtoPotP2'],2,",",".");
$importeDtoPotP3 = number_format($registro_factura['ImporteDtoPotP3'],2,",",".");
$importeDtoPotP4 = number_format($registro_factura['ImporteDtoPotP4'],2,",",".");
$importeDtoPotP5 = number_format($registro_factura['ImporteDtoPotP5'],2,",",".");
$importeDtoPotP6 = number_format($registro_factura['ImporteDtoPotP6'],2,",",".");


$textoDtoEnerP1 = $registro_factura['TextoDtoEnerP1'];
$textoDtoEnerP2 = $registro_factura['TextoDtoEnerP2'];
$textoDtoEnerP3 = $registro_factura['TextoDtoEnerP3'];
$textoDtoEnerP4 = $registro_factura['TextoDtoEnerP4'];
$textoDtoEnerP5 = $registro_factura['TextoDtoEnerP5'];
$textoDtoEnerP6 = $registro_factura['TextoDtoEnerP6'];

$importeDtoEnerP1 = number_format($registro_factura['ImporteDtoEnerP1'],2,",",".");
$importeDtoEnerP2 = number_format($registro_factura['ImporteDtoEnerP2'],2,",",".");
$importeDtoEnerP3 = number_format($registro_factura['ImporteDtoEnerP3'],2,",",".");
$importeDtoEnerP4 = number_format($registro_factura['ImporteDtoEnerP4'],2,",",".");
$importDtoeEnerP5 = number_format($registro_factura['ImporteDtoEnerP5'],2,",",".");
$importeDtoEnerP6 = number_format($registro_factura['ImporteDtoEnerP6'],2,",",".");

$impuesto=$registro_factura['impuesto'];
$sinimpuesto=$registro_factura['simpuesto'];
$coeficiente=$registro_factura['coeficiente'];
$total_impuesto=number_format($registro_factura['totalimpuesto'],2,",",".");

$alquiler=$registro_factura['talqreac'];

$boe2012texto42011=$registro_factura['BOE2012Texto42011'];
$boe2012texto12012=$registro_factura['BOE2012Texto12012'];
$boe2012textototal=$registro_factura['BOE2012TextoTotal'];
$boe2012consumo42011=$registro_factura['BOE2012Consumo42011'];
$boe2012consumo12012=$registro_factura['BOE2012Consumo12012'];
$boe2012consumototal=$registro_factura['BOE2012ConsumoTotal'];
$boe2012importe42011=$registro_factura['BOE2012Importe42011'];
$boe2012importe12012=$registro_factura['BOE2012Importe12012'];
$boe2012importetotal=$registro_factura['BOE2012ImporteTotal'];
$boe2012textoabril=$registro_factura['BOE2012TextoAbril'];
$boe2012consumoabril=$registro_factura['BOE2012ConsumoAbril'];
$boe2012importeabril=$registro_factura['BOE2012ImporteAbril'];

//Historial consumos
$mes1=$registro_factura['Mes1'];
$mes2=$registro_factura['Mes2'];
$mes3=$registro_factura['Mes3'];
$mes4=$registro_factura['Mes4'];
$mes5=$registro_factura['Mes5'];
$mes6=$registro_factura['Mes6'];
$mes7=$registro_factura['Mes7'];
$mes8=$registro_factura['Mes8'];
$mes9=$registro_factura['Mes9'];
$mes10=$registro_factura['Mes10'];
$mes11=$registro_factura['Mes11'];
$mes12=$registro_factura['Mes12'];

$anio1=$registro_factura['An1'];
$anio2=$registro_factura['An2'];
$anio3=$registro_factura['An3'];
$anio4=$registro_factura['An4'];
$anio5=$registro_factura['An5'];
$anio6=$registro_factura['An6'];
$anio7=$registro_factura['An7'];
$anio8=$registro_factura['An8'];
$anio9=$registro_factura['An9'];
$anio10=$registro_factura['An10'];
$anio11=$registro_factura['An11'];
$anio12=$registro_factura['An12'];

$con1=$registro_factura['Con1'];
$con2=$registro_factura['Con2'];
$con3=$registro_factura['Con3'];
$con4=$registro_factura['Con4'];
$con5=$registro_factura['Con5'];
$con6=$registro_factura['Con6'];
$con7=$registro_factura['Con7'];
$con8=$registro_factura['Con8'];
$con9=$registro_factura['Con9'];
$con10=$registro_factura['Con10'];
$con11=$registro_factura['Con11'];
$con12=$registro_factura['Con12'];
$emisora=$registro_factura['NIFEmisora'];
$referencia=$registro_factura['Referencia'];
$identificacion=$registro_factura['Identi'];

$i=0;
if($mes1=="") {} else {$meses[$i]=$mes1; $i+=1;}
if($mes2=="") {} else {$meses[$i]=$mes2; $i+=1;}
if($mes3=="") {} else {$meses[$i]=$mes3; $i+=1;}
if($mes4=="") {} else {$meses[$i]=$mes4; $i+=1;}
if($mes5=="") {} else {$meses[$i]=$mes5; $i+=1;}
if($mes6=="") {} else {$meses[$i]=$mes6; $i+=1;}
if($mes7=="") {} else {$meses[$i]=$mes7; $i+=1;}
if($mes8=="") {} else {$meses[$i]=$mes8; $i+=1;}
if($mes9=="") {} else {$meses[$i]=$mes9; $i+=1;}
if($mes10=="") {} else {$meses[$i]=$mes10; $i+=1;}
if($mes11=="") {} else {$meses[$i]=$mes11; $i+=1;}
if($mes12=="") {} else {$meses[$i]=$mes12; $i+=1;}

$i=0;
if($anio1=="") {} else {$anios[$i]=$anio1; $i+=1;}
if($anio2=="") {} else {$anios[$i]=$anio2; $i+=1;}
if($anio3=="") {} else {$anios[$i]=$anio3; $i+=1;}
if($anio4=="") {} else {$anios[$i]=$anio4; $i+=1;}
if($anio5=="") {} else {$anios[$i]=$anio5; $i+=1;}
if($anio6=="") {} else {$anios[$i]=$anio6; $i+=1;}
if($anio7=="") {} else {$anios[$i]=$anio7; $i+=1;}
if($anio8=="") {} else {$anios[$i]=$anio8; $i+=1;}
if($anio9=="") {} else {$anios[$i]=$anio9; $i+=1;}
if($anio10=="") {} else {$anios[$i]=$anio10; $i+=1;}
if($anio11=="") {} else {$anios[$i]=$anio11; $i+=1;}
if($anio12=="") {} else {$anios[$i]=$anio12; $i+=1;}

$i=0;
if($con1=="") {} else {$consu[$i]=$con1; $i+=1;}
if($con2=="") {} else {$consu[$i]=$con2; $i+=1;}
if($con3=="") {} else {$consu[$i]=$con3; $i+=1;}
if($con4=="") {} else {$consu[$i]=$con4; $i+=1;}
if($con5=="") {} else {$consu[$i]=$con5; $i+=1;}
if($con6=="") {} else {$consu[$i]=$con6; $i+=1;}
if($con7=="") {} else {$consu[$i]=$con7; $i+=1;}
if($con8=="") {} else {$consu[$i]=$con8; $i+=1;}
if($con9=="") {} else {$consu[$i]=$con9; $i+=1;}
if($con10=="") {} else {$consu[$i]=$con10; $i+=1;}
if($con11=="") {} else {$consu[$i]=$con11; $i+=1;}
if($con12=="") {} else {$consu[$i]=$con12; $i+=1;}


/* CREACION PDF */
/* IMPORTANTE: Los saltos de linea se interpretan y generan igual */
$pdf->SetDisplayMode('real');
$pdf->AddPage();
$pdf->SetFont('Arial');
$pdf->AddFont('A28NN___','','A28NN___.php');

$fecha_partes=explode('-',$fecha[0]);
$meses_nombres=array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");

/* DATOS CLIENTE */
$pdf->SeccionBody(110,53,90,
' '.$nombre_titular.'
 '.$direccion_titular.'
 '.$direccion_titular2.'',5
);

/* INFORMACION DE FACTURA */
$pdf->PrintSeccion(15,57.5,92,5,'Datos de la Factura','',0);

$pdf->SeccionBody(15,59,92,
'Fecha de emision: 
Periodo de Facturacion: 
Num. de Factura:

',5);

$pdf->CreaCelda(15.2,80,15,'Total Factura:',255,255,255,'Arial','B',10);

$pdf->CreaCelda(50.5,65,25,$fecha_partes[2].' de '.$meses_nombres[number_format($fecha_partes[1]-1)].' de '.$fecha_partes[0],255,255,255,'Arial','',7);
$pdf->CreaCelda(50.5,70,25,$periodo_factura,255,255,255,'Arial','',7);
$pdf->CreaCelda(50.5,75,25,$num_factura,255,255,255,'Arial','',7);
$pdf->CreaCelda(50.5,80,25,$importe_total.' euros',255,255,255,'Arial','B',10);

//$pdf->Image('cod_barras.jpg',119,67,75);
$pdf->CreaCelda(112,77,85,$cod_barras,255,255,255,'A28NN___','',13);
$pdf->CreaCelda(122,82,85,$cod_barras,255,255,255,'Arial','',7);
//Image_Barcode::draw($cod_barras, 'code128', 'png');

/* DATOS DE SUMINISTRO */
$pdf->PrintSeccion(15,88,92,5,'Datos del Suministro','',0);

$pdf->SeccionBody(15,90,92,
'',50); 

$pdf->CreaCelda(16,96,25,'Titular:' ,255,255,255,'Arial','B',7);
$pdf->CreaCelda(16,100,25,'Direccion: ',255,255,255,'Arial','B',7);
$pdf->CreaCelda(16,104,25,'Poblacion: ',255,255,255,'Arial','B',7);
$pdf->CreaCelda(16,108,25,'NIF/DNI: ',255,255,255,'Arial','B',7);
$pdf->CreaCelda(16,112,25,'Referencia catastral: ',255,255,255,'Arial','B',7);
$pdf->CreaCelda(16,116,25,'CUPS: ',255,255,255,'Arial','B',7);
$pdf->CreaCelda(16,120,25,'CNAE: ',255,255,255,'Arial','B',7);
$pdf->CreaCelda(16,128,25,'Potencia:' ,255,255,255,'Arial','B',7);
$pdf->CreaCelda(16,132,25,'Tension:',255,255,255,'Arial','B',7);
$pdf->CreaCelda(16,136,25,'Tarifa: ',255,255,255,'Arial','B',7);
$pdf->CreaCelda(16,140,25,'Numero de contador:',255,255,255,'Arial','B',7);

$pdf->CreaCelda(37,96,25,$nombre_titular,255,255,255,'Arial','',7);
$pdf->CreaCelda(37,100,25,$direccion_suministro,255,255,255,'Arial','',7);
$pdf->CreaCelda(37,104,25,$poblacion_suministro,255,255,255,'Arial','',7);
$pdf->CreaCelda(37,108,25,$dni_titular,255,255,255,'Arial','',7);
$pdf->CreaCelda(44,112,25,$ref_catastral,255,255,255,'Arial','',7);
$pdf->CreaCelda(32,116,25,$cups,255,255,255,'Arial','',7);
$pdf->CreaCelda(32,120,15,$cnae,255,255,255,'Arial','',7);
$pdf->CreaCelda(52,120,20,'Codigo Cliente:',255,255,255,'Arial','B',7);
$pdf->CreaCelda(72,120,15,$_SESSION['numeroCliente'],255,255,255,'Arial','',7);
$pdf->CreaCelda(20,122.5,25,'_______________________________________________',255,255,255,'Arial','B',9);
$pdf->CreaCelda(42,128,25,$p1.'   '.$p2.'   '.$p3.'   '.$p4.'   '.$p5.'   '.$p6,255,255,255,'Arial','',7);
$pdf->CreaCelda(42,132,25,$tension,255,255,255,'Arial','',7);
$pdf->CreaCelda(42,136,20,$tarifa,255,255,255,'Arial','',7);
$pdf->CreaCelda(63,136,8,'Discriminador:',255,255,255,'Arial','B',7);
$pdf->CreaCelda(84,136,6,$registro_factura['textodisriminador'],255,255,255,'Arial','',7);
$pdf->CreaCelda(48,140,25,$contador,255,255,255,'Arial','',7);

$posy= 101;
/* CALCULO DE LA FACTURA */
$pdf->PrintSeccion(110,88,90,5,'Calculo de la Factura',
' ',0);
$pdf->SeccionBody(110,90,90,
'',120);

$pdf->CreaCelda(111,97,50,'Termino de Potencia:',210,210,210,'Arial','B',8);
if($importePotP1==0 and $textoPotP1==""){} else {
	$pdf->CreaCelda(111,$posy,80,$textoPotP1,255,255,255,'Arial','',8);
	$pdf->CreaCeldaAlDer(192,$posy,7,$importePotP1,255,255,255); 
	$posy+=3;
}
if($importeDtoPotP1==0 and $textoDtoPotP1==""){} else {
	$pdf->CreaCelda(111,$posy,70,$textoDtoPotP1,255,255,255,'Arial','',8);
	$pdf->CreaCeldaAlDer(192,$posy,6,$importeDtoPotP1,255,255,255);
	$posy+=3;
}

if($importePotP2==0 and $textoPotP2==""){} else {
	$pdf->CreaCelda(111,$posy,80,$textoPotP2,255,255,255,'Arial','',8);
	$pdf->CreaCeldaAlDer(192,$posy,7,$importePotP2,255,255,255);
	$posy+=3;
}
if($importeDtoPotP2==0 and $textoDtoPotP2==""){} else {
	$pdf->CreaCelda(111,$posy,70,$textoDtoPotP2,255,255,255,'Arial','',8);
	$pdf->CreaCeldaAlDer(192,$posy,6,$importeDtoPotP2,255,255,255);
	$posy+=3;
}

if($importePotP3==0 and $textoPotP3==""){} else {
	$pdf->CreaCelda(111,$posy,80,$textoPotP3,255,255,255,'Arial','',8);
	$pdf->CreaCeldaAlDer(192,$posy,7,$importePotP3,255,255,255);
	$posy+=3;
}
if($importeDtoPotP3==0 and $textoDtoPotP3==""){} else {
	$pdf->CreaCelda(111,$posy,70,$textoDtoPotP3,255,255,255,'Arial','',8);
	$pdf->CreaCeldaAlDer(192,$posy,6,$importeDtoPotP3,255,255,255);
	$posy+=3;
}

$posy+=5;

$pdf->CreaCelda(111,$posy,50,'Exceso de Potencia (Maximetros) ',210,210,210,'Arial','B',8);
//$pdf->CreaCelda(105,98,80,'Punta',255,255,255);
//$pdf->CreaCelda(110,101,80,'Llano: 0000000',255,255,255);
//$pdf->CreaCelda(115,104,80,'Valle: 0000000',255,255,255);
$posy=137;

$pdf->CreaCelda(111,$posy,50,'Termino de Energia ',210,210,210,'Arial','B',8);
$posy+=4;

if($importeEnerP1==0 and $textoEnerP1==""){} else {
$pdf->CreaCelda(111,$posy,80,$textoEnerP1,255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(192,$posy,7,$importeEnerP1,255,255,255);
$posy+=3;
}
if($importeDtoEnerP1==0 and $textoDtoEnerP1==""){} else {
$pdf->CreaCelda(111,$posy,70,$textoDtoEnerP1,255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(192,$posy,6,$importeDtoEnerP1,255,255,255); 
$posy+=3;
}

if($importeEnerP2==0 and $textoEnerP2==""){} else {
$pdf->CreaCelda(111,$posy,80,$textoEnerP2,255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(192,$posy,7,$importeEnerP2,255,255,255);
$posy+=3;
}
if($importeDtoEnerP2==0 and $textoDtoEnerP2==""){} else {
$pdf->CreaCelda(111,$posy,70,$textoDtoEnerP2,255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(192,$posy,6,$importeDtoEnerP2,255,255,255); 
$posy+=3;
}

if($importeEnerP3==0 and $textoEnerP3==""){} else {
$pdf->CreaCelda(111,$posy,80,$textoEnerP3,255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(192,$posy,7,$importeEnerP3,255,255,255);
$posy+=3;
}
if($importeDtoEnerP3==0 and $textoDtoEnerP3==""){} else {
$pdf->CreaCelda(111,$posy,70,$textoDtoEnerP3,255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(192,$posy,6,$importeDtoEnerP3,255,255,255); 
$posy+=3;
}

$posy+=10;

$pdf->CreaCelda(111,$posy,50,'Termino de Reactiva ',210,210,210,'Arial','B',8);
$posy+=4;

if($importeReacP1==0 and $textoeacP1==""){} else {
$pdf->CreaCelda(111,$posy,80,$textoReacP1,255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(192,$posy,7,$importeReacP1,255,255,255);
$posy+=3;
}
if($importeReacP2==0 and $textoReacP2==""){} else {
$pdf->CreaCelda(111,$posy,80,$textoReacP2,255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(192,$posy,7,$importeReacP2,255,255,255);
$posy+=3;
}
if($importeReacP3==0 and $textoReacP3==""){} else {
$pdf->CreaCelda(111,$posy,80,$textoReacP3,255,255,255);
$pdf->CreaCeldaAlDer(192,$posy,7,$importeReacP3,255,255,255);
$posy+=3;
}

if ($boe2012consumo42011==0 and $boe2012importe42011==0 and $boe2012texto42011=="") {} else {
	$pdf->CreaCelda(111,185,40,$boe2012texto42011,255,255,255, 'Arial','',7);
	$pdf->CreaCeldaAlDer(192,185,6,$boe2012importe42011,255,255,255);
	$posy+=3;
}

if ($boe2012consumo12012==0 and $boe2012importe12012==0 and $boe2012texto12012=="") {} else {
	$pdf->CreaCelda(111,188,40,$boe2012texto12012,255,255,255, 'Arial','',7);
	$pdf->CreaCeldaAlDer(192,188,6,$boe2012importe12012,255,255,255);
	$posy+=3;
}
/*if ($boe2012consumototal==0 and $boe2012importetotal==0 and $boe2012textototal=="") {} else {
	$pdf->CreaCelda(111,210,80,$boe2012textototal,255,255,255, 'Arial','',8);
	$pdf->CreaCeldaAlDer(192,210,7,$boe2012importetotal,255,255,255);
	$posy+=3;
}*/

$pdf->CreaCelda(111,200,50,'Impuesto Electrico',210,210,210,'Arial','B',8);
$pdf->CreaCelda(111,204,80,$impuesto.' % sobre '.$sinimpuesto.' eur x '.$coeficiente,255,255,255,'Arial','',8);
$pdf->CreaCeldaAlDer(192,204,7,$total_impuesto,255,255,255,'Arial','',8);

if($alquiler==""){} else {
	$pdf->CreaCelda(111,208,40,'Importe Alquiler',255,255,255,'Arial','',8);
	$pdf->CreaCeldaAlDer(192,208,7,number_format($alquiler,2,",","."),255,255,255,'Arial','',8);
}

/* LECTURAS */
$pdf->PrintSeccion(15,148,92,5,'Resumen de Lecturas Tarifas de Acceso',
' ',0);
$pdf->SeccionBody(15,150,92,'',31);

$pdf->CreaCelda2(21,155,34,'Activa',210,210,210);
$pdf->CreaCelda2(55,155,34,'Reactiva',210,210,210);
$pdf->CreaCelda2(87.8,155,19,'Punta',210,210,210);
$pdf->CreaCelda2(21,158,17,'Anterior',210,210,210);
$pdf->CreaCelda2(38,158,17,'Actual',210,210,210);
$pdf->CreaCelda2(55,158,17,'Anterior',210,210,210);
$pdf->CreaCelda2(72,158,17,'Actual',210,210,210);
$pdf->CreaCelda2(87.8,158,19,'Max.',210,210,210);

$pdf->SeccionBody(15,157,6,
'P1
P2
P3
P4
P5
P6',4
);

/* Secciones vacias para crear marco */
$pdf->SeccionBody(21,150,35,' ',31);
$pdf->SeccionBody(56,150,34,' ',31);
$pdf->SeccionBody(90,150,17,' ',31);

 /* P1 */ 
$pdf->CreaCeldaAlDer(25,162.5,10,$lecturaP1ant,255,255,255);
$pdf->CreaCeldaAlDer(43.5,162.5,10,$lecturaP1act,255,255,255);
$pdf->CreaCeldaAlDer(60.5,162.5,10,$lecturaReacP1ant,255,255,255);
$pdf->CreaCeldaAlDer(77.5,162.5,10,$lecturaReacP1act,255,255,255);
$pdf->CreaCeldaAlDer(95,162.5,10,$lecturaMaxP1,255,255,255);

 /* P2 */ 
$pdf->CreaCeldaAlDer(25,166.5,10,$lecturaP2ant,255,255,255);
$pdf->CreaCeldaAlDer(43.5,166.5,10,$lecturaP2act,255,255,255);
$pdf->CreaCeldaAlDer(60.5,166.5,10,$lecturaReacP2ant,255,255,255);
$pdf->CreaCeldaAlDer(77.5,166.5,10,$lecturaReacP2act,255,255,255);
$pdf->CreaCeldaAlDer(95,166.5,10,$lecturaMaxP2,255,255,255);

 /* P3 */ 
$pdf->CreaCeldaAlDer(25,170.5,10,$lecturaP3ant,255,255,255);
$pdf->CreaCeldaAlDer(43.5,170.5,10,$lecturaP3act,255,255,255);
$pdf->CreaCeldaAlDer(60.5,170.5,10,$lecturaReacP3ant,255,255,255);
$pdf->CreaCeldaAlDer(77.5,170.5,10,$lecturaReacP3act,255,255,255);
$pdf->CreaCeldaAlDer(95,170.5,10,$lecturaMaxP3,255,255,255);

 /* P4 */ 
$pdf->CreaCeldaAlDer(25,174.5,10,$lecturaP4ant,255,255,255);
$pdf->CreaCeldaAlDer(43.5,174.5,10,$lecturaP4act,255,255,255);
$pdf->CreaCeldaAlDer(60.5,174.5,10,$lecturaReacP4ant,255,255,255);
$pdf->CreaCeldaAlDer(77.5,174.5,10,$lecturaReacP4act,255,255,255);
$pdf->CreaCeldaAlDer(95,174.5,10,$lecturaMaxP4,255,255,255);

 /* P5 */ 
$pdf->CreaCeldaAlDer(25,178.5,10,$lecturaP5ant,255,255,255);
$pdf->CreaCeldaAlDer(43.5,178.5,10,$lecturaP5act,255,255,255);
$pdf->CreaCeldaAlDer(60.5,178.5,10,$lecturaReacP5ant,255,255,255);
$pdf->CreaCeldaAlDer(77.5,178.5,10,$lecturaReacP5act,255,255,255);
$pdf->CreaCeldaAlDer(95,178.5,10,$lecturaMaxP5,255,255,255);

 /* P6 */ 
$pdf->CreaCeldaAlDer(25,182.5,10,$lecturaP6ant,255,255,255);
$pdf->CreaCeldaAlDer(43.5,182.5,10,$lecturaP6act,255,255,255);
$pdf->CreaCeldaAlDer(60.5,182.5,10,$lecturaReacP6ant,255,255,255);
$pdf->CreaCeldaAlDer(77.5,182.5,10,$lecturaReacP6act,255,255,255);
$pdf->CreaCeldaAlDer(95,182.5,10,$lecturaMaxP6,255,255,255);


/* HISTORIAL DE CONSUMOS */
$pdf->PrintSeccion(15,188,92,5,'Historial de Consumo',
'',0);
$pdf->SeccionBody(15,190,92,''.$meses[5].'
'.$meses[4].'
'.$meses[3].'
'.$meses[2].'
'.$meses[1].'
'.$meses[0].'',4);

/* Columna Año */
$pdf->CreaCelda(52,195.5,10,$anios[5],255,255,255,'Arial','',8);
$pdf->CreaCelda(52,199.5,10,$anios[4],255,255,255,'Arial','',8);
$pdf->CreaCelda(52,203.5,10,$anios[3],255,255,255,'Arial','',8);
$pdf->CreaCelda(52,207.5,10,$anios[2],255,255,255,'Arial','',8);
$pdf->CreaCelda(52,211.5,10,$anios[1],255,255,255,'Arial','',8);
$pdf->CreaCelda(52,215.5,10,$anios[0],255,255,255,'Arial','',8);

$pdf->CreaCelda(72,195.5,10,$consu[5],255,255,255,'Arial','',8);
$pdf->CreaCelda(72,199.5,10,$consu[4],255,255,255,'Arial','',8);
$pdf->CreaCelda(72,203.5,10,$consu[3],255,255,255,'Arial','',8);
$pdf->CreaCelda(72,207.5,10,$consu[2],255,255,255,'Arial','',8);
$pdf->CreaCelda(72,211.5,10,$consu[1],255,255,255,'Arial','',8);
$pdf->CreaCelda(72,215.5,10,$consu[0],255,255,255,'Arial','',8);

$pdf->SeccionBody(15,218,92,
'Forma de pago:
Entidad bancaria:
Numero de Cuenta:
 
',4);

$pdf->CreaCelda(47,223.5,50,$forma_pago,255,255,255,'Arial','',8);
$pdf->CreaCelda(47,227.5,50,$banco,255,255,255,'Arial','',8);
$pdf->CreaCelda(47,231.5,50,$num_cuenta,255,255,255,'Arial','',8);

/* BASE IMPONIBLE... */
$pdf->SeccionBody(110,214,90,'',20);

$pdf->CreaCelda(112,222,11,'BASE IMPONIBLE',255,255,255,'Arial','B',9);
$pdf->CreaCelda(169,222,11,number_format($iva1,2,",","").' %',255,255,255,'Arial','',9);


if ($base1==0 or $iva1==0 or $cuota1==0){
	$pdf->Rect(143,221,22,5,'D');
	$pdf->CreaCelda(148,222,11,$base2,255,255,255,'Arial','B',9);
	$pdf->CreaCelda(169,222,11,number_format($iva2,2,",","").' %',255,255,255,'Arial','',9);
	$pdf->CreaCelda(185,222,11,$cuota2,255,255,255,'Arial','',9);
	$iva18=true;
} else {
	$pdf->Rect(143,221,22,5,'D');
	$pdf->CreaCelda(148,222,11,$base1,255,255,255,'Arial','B',9);
	$pdf->CreaCelda(169,222,11,number_format($iva1,2,",","").' %',255,255,255,'Arial','',9);
	$pdf->CreaCelda(185,222,11,$cuota1,255,255,255,'Arial','',9);
}

if ($base2==0 or $iva2==0 or $cuota2==0){} else {
	if ($iva18==true){} else {
		$pdf->Rect(143,227,22,5,'D');
		$pdf->CreaCelda(148,228,11,$base2,255,255,255,'Arial','B',9);
		$pdf->CreaCelda(169,228,11,number_format($iva2,2,",","").' %',255,255,255,'Arial','',9);
		$pdf->CreaCelda(185,228,11,$cuota2,255,255,255,'Arial','',9);
	}
}

$pdf->CreaCelda(112,235,11,'IMPORTE TOTAL EUROS',255,255,255,'Arial','B',9);
$pdf->CreaCelda(185,235,10,$total_factura,255,255,255,'Arial','B',9);

/* OBSERVACIONES */
$pdf->PrintSeccion(15,242,0,5,'Observaciones',
' ',0);
$pdf->SeccionBody(110,243,90,
$registro_factura['textolibre'].'

',5);

$pdf->PrintSeccion(15,248,20,4.5,' ',' ',0);
$pdf->PrintSeccion(15,252.5,20,4.5,' ',' ',0);
$pdf->PrintSeccion(15,257,20,4.5,' ',' ',0);
$pdf->PrintSeccion(15,261.5,20,4.5,' ',' ',0);
$pdf->CreaCelda(16,249,15,'Emisora',190,190,190,'Times','',8);
$pdf->CreaCelda(16,253.5,15,'Referencia',190,190,190,'Times','',8);
$pdf->CreaCelda(16,258,15,'Identificacion',190,190,190,'Times','',8);
$pdf->CreaCelda(16,262.5,15,'Importe',190,190,190,'Times','',8);

$pdf->CreaCelda(39,249,60,$emisora,255,255,255,'Times','',8);
$pdf->CreaCelda(39,253.5,60,$referencia,255,255,255,'Times','',8);
$pdf->CreaCelda(39,258,60,$identificacion,255,255,255,'Times','',8);
$pdf->CreaCelda(39,262.5,60,$importe_total,255,255,255,'Times','',8);
		}
	}
	$pdf->Output();
	echo "<p align='center' style='margin-top:40px'><strong>La factura solicitada no esta disponible en estos momentos.</strong>";
	echo "<br><br>";
	echo "Por favor, pongase en contacto con nosotros para informanos de este error.<br>";
	echo "Lamentamos las molestias.</p>";
?>