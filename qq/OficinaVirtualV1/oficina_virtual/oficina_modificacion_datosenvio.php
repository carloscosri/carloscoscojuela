<?php
/*
NOTAS:

1- ESTA SECCION TOMA LOS DATOS DE LA TABLA DatosRegistrados

2- ESCRIBE EN LA TABLA DatosModificados

*/

/* NOTA:

 COMO NO SE CIERRA LA CONEXION TRAS CADA CONEXION CON LA BASE DE DATOS, LAS CONSULTAS DEJAN DE FUNCIONAR SI SE ESTABLECE COMO UNICA CONEXION
 LA QUE UTILIZA EL RESTO DE LA WEB, POR LO TANTO HASTA QUE SE HAGA LA MIGRACION COMPLETA DE LOS TEXTOS, COEXISTIRAN LAS DOS CONEXIONES, YA QUE
 SE HA AÑADIDO UNA LLAMADA A LA CONEXION BUENA EN ESTE ARCHIVO.
*/

include("_conexion.php");

//Se incluye el archivo que contiene las verificaciones de datos
include("../includes/verificaciones.php");

//Como tienen textos en comun, para evitar el crecimiento innecesario de la base de datos, se incluye el archivo de textos de las secciones de datos del titular y del alta, ya que la mayoria de los textos son comunes
//include($_SESSION["directorio_raiz"]."idiomas/".$_SESSION["idioma"]."/oficina_datos_titular.php");
//include($_SESSION["directorio_raiz"]."idiomas/".$_SESSION["idioma"]."/oficina_contratacion_alta.php");

//include($_SESSION["directorio_raiz"]."idiomas/".$_SESSION["idioma"]."/oficina_modificacion_datostitular.php");

include("../idiomas/".$_SESSION["idioma"]."/oficina_datos_titular.php");
include("../idiomas/".$_SESSION["idioma"]."/oficina_contratacion_alta.php");

include("../idiomas/".$_SESSION["idioma"]."/oficina_modificacion_datostitular.php");

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<title><?=$titulo_web;?></title>

<!--Se añaden las hojas de estilos genericas que utiliza esta seccion-->
<link href="../css/estilos.css" rel="stylesheet" type="text/css" />
<link href="../css/secciones.css" rel="stylesheet" type="text/css" />
<link href="css/oficina.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_vieja.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_modificacion_titular.css" rel="stylesheet" type="text/css" />

<!--Ahora se añaden las reglas para Internet Explorer. Asi corregiremos las diferencias de visualizacion entre navegadores
Es importante añadirlo despues de las reglas principales, para que se sobreescriban las correcciones pertinentes. En este caso las diferencias
solo son para internet explorer 7-->

<!--[if IE 7]>
	<link href="../css/estilos_ie7.css" rel="stylesheet" type="text/css" />
	<link href="../css/secciones_ie7.css" rel="stylesheet" type="text/css" />    
<![endif]-->

<script language="javascript" type="text/javascript">

</script>

<?php
if (isset($_POST["submit"]))
{
//Recogemos los valores de las variables
	$titular_nombre=$_POST["TitularNombre"];							
	$titular_calle=$_POST["TitularCalle"];								
	$titular_numero=$_POST["TitularNumero"];
	$titular_extension=$_POST["TitularExtension"];	
	$titular_aclarador=$_POST["TitularAclarador"];		
	$titular_poblacion=$_POST["TitularPoblacion"];		
	$titular_provincia=$_POST["TitularProvincia"];
	
	$titular_telefono=$_POST["TitularTelefono"];					
	$titular_movil=$_POST["TitularMovil"];						
	$titular_fax=$_POST["TitularFax"];								
	$titular_email=$_POST["TitularEmail"];	
	$titular_dni=$_POST["TitularDNI"];

	$factura_nombre=$_POST["FacturaNombre"];							
	$factura_calle=$_POST["FacturaCalle"];								
	$factura_numero=$_POST["FacturaNumero"];	
	$factura_extension=$_POST["FacturaExtension"];
	$factura_aclarador=$_POST["FacturaAclarador"];		
	$factura_poblacion=$_POST["FacturaPoblacion"];		
	$factura_provincia=$_POST["FacturaProvincia"];
	
	$factura_telefono=$_POST["FacturaTelefono"];		
	$factura_movil=$_POST["FacturaMovil"];			
	$factura_fax=$_POST["FacturaFax"];	
	$factura_email=$_POST["FacturaEmail"];	
	$factura_dni=$_POST["FacturaDNI"];
		
	$pagador_nombre=$_POST["PagadorNombre"];							
	$pagador_calle=$_POST["PagadorCalle"];								
	$pagador_numero=$_POST["PagadorNumero"];
	$pagador_poblacion=$_POST["PagadorPoblacion"];		
	$pagador_provincia=$_POST["PagadorProvincia"];	
	$pagador_numero=$_POST["PagadorNumero"];			
	$pagador_extension=$_POST["PagadorExtension"];
	$pagador_aclarador=$_POST["PagadorAclarador"];
		
	$pagador_telefono=$_POST["PagadorTelefono"];		
	$pagador_movil=$_POST["PagadorMovil"];			
	$pagador_fax=$_POST["PagadorFax"];						
	$pagador_email=$_POST["PagadorEmail"];
	$pagador_dni=$_POST["PagadorDNI"];	

	$representante_nombre=$_POST["RepresentanteEmpresa"];
	$representante_dni=$_POST["DNIRepresentante"];
	
	$num_cuenta_banco=$_POST["banco"];
	$num_cuenta_sucursal=$_POST["sucursal"];	
	$num_cuenta_digito_control=$_POST["sucursal2"];		
	$num_cuenta_cuenta=$_POST["sucursal3"];		
	
	$observaciones=$_POST["Observaciones"];

//Estos campos son combos y tendrán un tratamiento especial	
	$forma_pago=$_POST["FormaPago"];
	$idioma_factura=$_POST["IdiomaFactura"];	
	$entidad_bancaria=$_POST["EntidadBancaria"];	
	
//Esta variable controlara si se produce algun error en las verificaciones
	$error=0;	
	

?>

</head>
	
<body>
<!--*************************************************Web realizada por ****************************************-->
	<div id="central_oficina">
    
<?php
//Se comprueba que el usuario se haya identificado, si no es asi se da un aviso y redirigimos a la seccion de identificacion
if ($_SESSION['usuario']=="")
{	
	MsgBox($error_identificacion);
	redirigir("oficina_index.php?id=".$_SESSION["idioma"]);
	
}//if ($_SESSION['usuario'])

//Si el usuario se ha identificado la seccion se ejecutara con normalidad
else
{
//Añadimos los fragmentos comunes, la cabecera, el menu, y el submenu
		include("oficina_fragmentos_comunes.php");
?>                    
		<div class="contenido_seccion_oficina_modificacion_titular">

<!--VERSION VIEJA-->


		<form action="oficina_modificacion_datostitular.php?id=<?=$_SESSION["idioma"]?>"  method="post" name="form_cambios_datos_titular" id="form_cambios_datos_titular">
		  <table width="458" border="0" cellspacing="0" cellpadding="0">
            <tr> 
              <td><table width="490" border="0" cellspacing="0" cellpadding="0">
                  <tr align="center"> 
                    <td width="470" height="30" class="arialblack18Titulo"> 
                      <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 195'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>                    </td>
                  </tr>
                  <tr> 
                    <td height="18" valign="middle">&nbsp;</td>
                  </tr>

<?php
//Esta variable controlara si el cliente tiene o no modificaciones pendientes
					$tiene_modificaciones_pendientes=0;

//Comprobamos si el cliente, tiene modificaciones pendientes
					$consulta_cliente=mysql_query("SELECT * FROM DatosModificados WHERE CodigoCliente  = ".$_SESSION["numeroCliente"]); 
					$hay_cliente = mysql_fetch_array($consulta_cliente);
										
					if($hay_cliente)
					{
						$tiene_modificaciones_pendientes=1;
					}//if($hay_cliente)
										
//Si el cliente tiene modificaciones pendientes se mostrara un mensaje					
					if($tiene_modificaciones_pendientes==1)
					{
?>                  
	                  <tr>
    	              	<td valign="middle" class="texto_advertencia"><?=$advertencia_cambios_pendientes?></td>
        	          </tr>
<?php				}//if($tiene_modificaciones_pendientes==1)?>                  

                  <tr> 
                    <td height="94" class="arial14"><fieldset style="border-left-color: #205FA6;
                  									   border-bottom-color: #205FA6;
													   border-top-color: #205FA6;
                                                       border-right-color: #205FA6;">
					<legend class="arialblack14">
					<?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 562'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
					</legend>
                      <table width="500" align="center" border="0">
<?php
							
							$result = mysql_query("SELECT * FROM DatosRegistrados WHERE CodigoCliente = ".$_SESSION['numeroCliente']." ");
							$row = mysql_fetch_array($result);
							
//Si se ha aceptado el formulario, las variables que contienen los valores de cada campo ya tienen valor. Si no es asi, asignaremos los valores con el contenido de la base de datos
							if (!isset($_POST["submit"]))	
							{								
//Si el formulario no se ha aceptado, se mostraran los valores almacenados en la base de datos
								$titular_nombre=$row["TitularNombre"];
								$titular_calle=$row["TitularCalle"];
								$titular_numero=$row["TitularNumero"];
								$titular_extension=$row["TitularExtension"];
								$titular_aclarador=$row["TitularAclarador"];
								$titular_poblacion=$row["TitularPoblacion"];;
								$titular_provincia=$row["TitularProvincia"];
								$titular_telefono=$row["TitularTelefono1"];
								$titular_movil=$row["TitularTelefono2"];
								$titular_fax=$row["TitularFax"];
								$titular_email=$row["TitularEmail"];
								$titular_dni=$row["TitularDNI"];
							
								$factura_nombre=$row["FacturaNombre"];
								$factura_calle=$row["FacturaCalle"];
								$factura_numero=$row["FacturaNumero"];
								$factura_extension=$row["FacturaExtension"];
								$factura_aclarador=$row["FacturaAclarador"];
								$factura_poblacion=$row["FacturaPoblacion"];
								$factura_provincia=$row["FacturaProvincia"];
								$factura_telefono=$row["FacturaTelefono1"];
								$factura_movil=$row["FacturaTelefono2"];
								$factura_fax=$row["FacturaFax"];
								$factura_email=$row["FacturaEmail"];
								$factura_dni=$row["FacturaDNI"];
							
								$pagador_nombre=$row["PagadorNombre"];
								$pagador_calle=$row["PagadorCalle"];
								$pagador_numero=$row["PagadorNumero"];
								$pagador_extension=$row["PagadorExtension"];
								$pagador_aclarador=$row["PagadorAclarador"];
								$pagador_poblacion=$row["PagadorPoblacion"];
								$pagador_provincia=$row["PagadorProvincia"];
								$pagador_telefono=$row["PagadorTelefono1"];
								$pagador_movil=$row["PagadorTelefono2"];
								$pagador_fax=$row["PagadorFax"];
								$pagador_email=$row["PagadorEmail"];
								$pagador_dni=$row["PagadorDNI"];
							
								$representante_nombre=$row["RepresentanteEmpresa"];
								$representante_dni=$row["DNIRepresentante"];
													
//Ahora subdividiremos el numero de la cuenta de banco para escribir las cifras correspondientes en cada uno de los cuadros
								$num_cuenta_banco=substr($row["NumeroCuenta"],0,4);
								$num_cuenta_sucursal=substr($row["NumeroCuenta"],4,4);
								$num_cuenta_digito_control=substr($row["NumeroCuenta"],8,2);
								$num_cuenta_cuenta=substr($row["NumeroCuenta"],10,10);
							
								$forma_pago=$row["FormaPago"];
								$idioma_factura=$row["IdiomaFactura"];	
								$entidad_bancaria=$row["BancoDomiciliado"];	
		
								$observaciones=$row["Observaciones"];
							}//if (!isset($_POST["submit"]))
						
?>
                        <tr> 
                          <td width="461" height="22" class="arial14"> 
                            <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 119'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                          &nbsp;&nbsp; <input name="TitularNombreFijo"   value="<?php echo(str_replace('"','&quot;',$row['TitularNombre']));?>"type="text" class="textfieldLectura" id="TitularNombreFijo" size="50" maxlength="50" /></td>
                        </tr>
                        
<!--CAMPO NUEVO-->
                        <tr> 
                          <td height="22" class="arial14">CUPS&nbsp; <input name="CUPSFijo" value="<?php echo($row['CUPS']);?>"  type="text" class="textfieldLectura" id="CUPSFijo" size="50" maxlength="50" /></td>
                        </tr>                        
<!--CAMPO NUEVO-->                        
                        
                        
                      </table>
                      </fieldset></td>
                  </tr>
                  <tr> 
                    <td height="10"></td>
                  </tr>
                
                  <tr> 
                    <td height="36" class="arial12"><strong> 
                      <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 198'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                      <br />
                      <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 199'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                    </strong></td>
                  </tr>
                  <tr> 
                    <td height="10"></td>
                  </tr>
                 <!--SEPARACION ENTRE APARTADOS-->    
                    <tr class="arial12"> 
                	  <td width="451" height="10" class="arial14"></td>
	                  <td width="9" class="arial14"></td>
                </tr>
<!--SEPARACION ENTRE APARTADOS-->                                    

                <tr> 
                    <td width="451" class="nota_campos_obligatorios"><span class="nota_campos_obligatorios"><?=$nota_campos_obligatorios?></span><br /><br /></td>
                </tr>


<!--APARRADO NUEVO-->

<!--SEPARACION ENTRE APARTADOS-->    
                    <tr class="arial12"> 
                	  <td width="451" height="10" class="arial14"></td>
	                  <td width="9" class="arial14"></td>
                </tr>
<!--SEPARACION ENTRE APARTADOS-->                                    

<!--APARTADO NUEVO-->

                <tr class="arial12"> 
                  <td colspan="2" class="arial14" width="600"><fieldset   >
                    <legend class="arialblack14"><?=$oficina_datos_titular16?></legend>
                    <table width="450" align="center">
                      
                      <tr> 
                        <td width="73" height="5" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                                 
                        </table>                        </td>
                      </tr>
                                      
                      
                      <tr> 
                        <td height="24" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                          
                          <tr>
                            <td width="75" height="20" valign="middle" class="arial14"><?=$oficina_datos_titular17?></td>
                        </tr>
                        </table>                        </td>
                        <td valign="top"><input name="FacturaNombre" value="<?=$factura_nombre?>" type="text" class="textfield" id="FacturaNombre" size="52" maxlength="50" /></td>
                      </tr>
                      <tr> 
    
                     <tr> 
                        <td height="24" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                          
                          <tr>
                            <td width="75" height="20" valign="middle" class="arial14"><?=$oficina_datos_titular18?></td>
                        </tr>
                        </table>                        </td>
                        <td valign="top"><input name="FacturaCalle" value="<?=$factura_calle?>" type="text" class="textfield" id="FacturaCalle" size="52" maxlength="50" /></td>
                      </tr>
                      <tr> 
     
                      <tr>                                           
                        <td height="24" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                          <tr>
                            <td width="75" height="20" valign="middle" class="arial14"><?=$oficina_datos_titular19?></td>
                        </tr>                        
                        
                        </table>                        </td>
         
                        <td valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                          <tr>                                                  
                            <td width="105" height="20" valign="top" class="arial14"><input name="FacturaNumero" value="<?=$factura_numero?>" type="text" class="textfield" id="FacturaNumero"  size="5" maxlength="4" /></td>
                            
                            <td width="75" bordercolor="#FF0000" height="22" valign="middle" class="arial14">&nbsp;&nbsp;<?=$oficina_datos_titular20?>&nbsp;</td>
                            <td width="363" height="22" valign="top"><input name="FacturaExtension" type="text" class="textfield" id="FacturaExtension" value="<?=$factura_extension?>"  size="29" maxlength="50" /></td>                            
                            
							</td>
	                      </tr>
                        </table>                                                                                                               
   
                      <tr> 
                        <td height="24" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                          
                          <tr>
                            <td width="75" height="20" valign="middle" class="arial14"><?=$oficina_datos_titular21?></td>
                        </tr>
                        </table>                        </td>
                        <td valign="top"><input name="FacturaAclarador" value="<?=$factura_aclarador?>" type="text" class="textfield" id="FacturaAclarador" size="52" maxlength="50" /></td>
                      </tr>
                      <tr> 

    
                      <tr>
                        <td height="24" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                          
                          <tr>
                            <td width="73" height="20" valign="middle" class="arial14"><?=$oficina_datos_titular22?></td>
                        </tr>
                        </table>                        </td>
                        <td valign="top"><input name="FacturaPoblacion" type="text"  value="<?=$factura_poblacion?>"  class="textfield" id="FacturaPoblacion" size="52" maxlength="50" /></td>
                      </tr>
                                

						 <tr>
                        <td height="24" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                          
                          <tr>
                            <td width="73" height="20" valign="middle" class="arial14"><?=$oficina_datos_titular23?></td>
                        </tr>
                        </table>                        </td>
                        <td valign="top"><input name="FacturaProvincia" type="text"  value="<?=$factura_provincia?>"  class="textfield" id="FacturaProvincia" size="52" maxlength="50" /></td>
                      </tr>


             
                      <tr>
                        <td height="2" colspan="2"></td>
                      </tr>
                      <tr> 
                        <td height="24" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                          
                          <tr>
                            <td width="75" height="20" valign="middle" class="arial14"><?=$oficina_datos_titular24?></td>
                        </tr>
                        </table>                        </td>
                        <td valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                          
                          <tr>
                            <td width="363" height="20" valign="top" class="arial14"><input name="FacturaTelefono" type="text" class="textfieldCentrado" value="<?=$factura_telefono?>"  id="FacturaTelefono" size="15" maxlength="15" />
&nbsp;&nbsp;

<?=$oficina_datos_titular25?>
                          &nbsp; 
                          <input name="FacturaMovil" value="<?=$factura_movil?>"  type="text" class="textfieldCentrado" id="FacturaMovil" size="15" maxlength="15" /></td>
                      </tr>
                        </table>                        </td>
                      </tr>
                      
                      
                      
                      
                      <tr>
                        <td height="25" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                          
                          <tr>
                            <td width="75" height="21" valign="middle" class="arial14"><?=$oficina_datos_titular26?></td>
                        </tr>
                        </table>                        </td>
                        <td valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                          
                          <tr>
                            <td width="363" height="21" valign="top" class="arial14"><input name="FacturaFax" value="<?=$factura_fax?>"  type="text" class="textfieldCentrado" id="FacturaFax" size="15" maxlength="15" /></td>
                      </tr>
                        </table>                        </td>
                      </tr>
                      
                      
                      
                      
                      <tr> 
                        <td height="24" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                          
                          <tr>
                            <td width="75" height="20" valign="middle" class="arial14"><?=$oficina_datos_titular27?></td>
                        </tr>
                        </table>                        </td>
                        <td valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                          
                          <tr>
                            <td width="363" height="20" valign="top"><input name="FacturaEmail" value="<?=$factura_email?>"  type="text" class="textfield" id="FacturaEmail" size="52" maxlength="50" /></td>
                      </tr>
                        </table>                        </td>
                      </tr>
                      
                      
				<tr>
                   <td height="25" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                          
                          <tr>
                            <td width="75" height="21" valign="middle" class="arial14"><?=$oficina_datos_titular28?></td>
                        </tr>
                        </table>                        </td>
                        <td valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                          
                          <tr>
                            <td width="363" height="21" valign="top" class="arial14"><input name="FacturaDNI" value="<?=$factura_dni?>"  type="text" class="textfieldCentrado" id="FacturaDNI" size="15" maxlength="15" /></td>
                      </tr>
                        </table>                        </td>
                      </tr>
                      
<!--CIERRE DEL APARTADO-->                      
                    </table>
                    </fieldset></td>
                </tr>
<!--CIERRE DEL APARTADO-->                      

<!--APARRADO NUEVO-->


<!--SEPARACION ENTRE APARTADOS-->    
                    <tr class="arial12"> 
                	  <td width="451" height="10" class="arial14"></td>
	                  <td width="9" class="arial14"></td>
                </tr>
<!--SEPARACION ENTRE APARTADOS-->                                    

<!--APARTADO NUEVO-->

            
<!--CIERRE DEL APARTADO-->

<!--APARRADO NUEVO-->

          
   
              
                    
                    
                
                
<!--ESPACIO ENTRE EL TITULO Y EL PRIMER CAMPO-->                   
                    
                    
                    
                   
               
<!--APARTADO NUEVO OBSERVACIONES-->                   
                   
				  <tr><td><fieldset>
                      <legend class="arialblack14"> 
                      <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto =152'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                       </legend>
                    <table width="469" align="center">
                        <tr> 
                          <td width="461" align="center" class="arial12"> <textarea name="Observaciones" cols="55" rows="5" id="Observaciones" class="arial14"><?=$observaciones?></textarea> 
                          </td>
                        </tr>
                      </table>
                      </fieldset></td></tr>
<!--APARTADO NUEVO OBSERVACIONES-->                   

                <tr class="arial12"> 
                  <td height="20" class="arial14">&nbsp;</td>
                </tr>
                                      
				<tr>
	                <td height="30" align="center" class="arial14">
                    <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 153'); $rowTexto = mysql_fetch_array($resultTexto);?>
                    <input name="submit" type="submit" id="submit" value="<?php echo($rowTexto['Texto']);?>"> 
                </tr>
                      
                <tr class="arial12"> 
                  <td height="20" class="arial14">&nbsp;</td>
                </tr>
               
              </table></td>
            </tr>
          </table>
          
          </form>



<!--VERSION VIEJA-->
                	
		</div><!--<div class="contenido_seccion_oficina">-->                        
		
        
        <div class="limpiar"></div>
                        	
<!--Por ultimo aparecera el pie de la web-->
<?php 
//	include($_SESSION["directorio_raiz"]."includes/pie.php");
	include("../includes/pie.php");

//NOTA: EN ESTA OCASION, EL ERROR SE MOSTRARA DESPUES DE LA CARGA DE LA PAGINA, SI NO POR ALGUN MOTIVO EN OCASIONES DESPUES DE DAR EL ERROR CESA LA CARGA DE LA PAGINA
	if(isset($_POST["submit"]))
	{		
//Por ultimo mostraremos el error correspondiente, en el caso de que haya fallado alguna de las verificaciones, o continuaremos con el proceso		
		switch($error)
		{		
//Proceso OK	
			case 0:						
//Antes de realizar el alta en la base de datos, deberemos de preparar los datos que no nos da el formulario

//Como en este caso el cliente ya existe, el codigo del cliente no hace falta calcularlo. Lo mismo sucede con el CUPS
				$cod_usuario=$row["CodigoCliente"];
								
//La forma de pago envia un indice y no la descripcion de la forma de pago, ahora lo sustituiremos antes de dar el alta
				switch($forma_pago)
				{
					case 1:
						$forma_pago="Domiciliacion";
					break;
					
					case 2:
						$forma_pago="No domiciliado";					
					break;					
				}//switch($forma_pago)
												
//El idioma de facturacion envia un indice y no la descripcion del idioma, ahora lo sustituiremos antes de dar el alta
				switch($idioma_factura)
				{
					case 0:
						$idioma_factura="Castellano";
					break;
					
					case 1:
						$idioma_factura="Catalán";					
					break;	

					case 2:
						$idioma_factura="Euskera";					
					break;	

					case 3:
						$idioma_factura="Gallego";					
					break;	

					case 4:
						$idioma_factura="Bable (Asturias)";					
					break;																				
				}//switch($idioma_factura)

								
//Tambien preparamos el numero de la cuenta que sera la concatenacion de todos los campos que lo forman
				$num_cuenta_completo=$num_cuenta_banco.$num_cuenta_sucursal.$num_cuenta_digito_control.$num_cuenta_cuenta;
				
//LOLOLO CONSULTAR DATOS PARA TIPO DE SUMINISTRO Y DATOS DEL SUMINISTRO

//A continuacion asignaremos el valor a las variables, que como no nos han hecho falta hasta ahora no habiamos asignado de la consulta de los datos actuales del cliente. En este caso los datos son los del suministro

				$suministro_poblacion=$row["SuministroCiudad"];
				$suministro_provincia=$row["SuministroProvincia"];				
				$suministro_calle=$row["SuministroCalle"];								
				$suministro_numero=$row["SuministroNumero"];				
				$suministro_extension=$row["SuministroExtension"];								
				$suministro_aclarador=$row["SuministroExtension2"];				
				$suministro_cp=$row["SuministroCP"];				
				$suministro_telefono=$row["SuministroTelefono1"];				
				$suministro_movil=$row["SuministroTelefono2"];								
				$suministro_fax=$row["SuministroFax"];												
				$suministro_email=$row["SuministroEmail"];																
				
				$tarifa_contratada=$row["TarifaContratada"];																
				$potencia_contratada=$row["PotenciaContratada"];																				
				$tipo_tension=$row["TipoTension"];				
				$discriminador=$row["Discriminador"];								
				$tipo_alquiler=$row["TipoAlquiler"];				
				$maximetro=$row["Maximetro"];								
				$modo=$row["Modo"];
				$observaciones=$row["Observaciones"];				
				$fases=$row["Fases"];				
				$reactiva=$row["Reactiva"];
								
				$p1_activa=$row["P1Activa"];				
				$p1_reactiva=$row["P1Reactiva"];				
				$p1_maximetro=$row["P1Maximetro"];			

				$p2_activa=$row["P2Activa"];				
				$p2_reactiva=$row["P2Reactiva"];				
				$p2_maximetro=$row["P2Maximetro"];			
				
				$p3_activa=$row["P3Activa"];				
				$p3_reactiva=$row["P3Reactiva"];				
				$p3_maximetro=$row["P3Maximetro"];			
				
				$p4_activa=$row["P4Activa"];				
				$p4_reactiva=$row["P4Reactiva"];				
				$p4_maximetro=$row["P4Maximetro"];			
				
				$p5_activa=$row["P5Activa"];				
				$p5_reactiva=$row["P5Reactiva"];				
				$p5_maximetro=$row["P5Maximetro"];			
				
				$p6_activa=$row["P6Activa"];				
				$p6_reactiva=$row["P6Reactiva"];				
				$p6_maximetro=$row["P6Maximetro"];																													
								
//Las modificaciones en realidad no cambian los datos de la tabla DatosRegistrados, si no que dan un alta en la tabla DatosModificados. Aunque se hace la inserccion completa, algunos campos quedaran en blanco, ya que se cambian desde la seccion de oficina_modificacion_datossuministro

//Ahora dependiendo de si el usuario ya tiene un registro o no en la tabla de DatosModificados, se realizara un alta nueva o una actualizacion del registro

					if($tiene_modificaciones_pendientes==0)
					{										
						$solicitud_modificacion="INSERT INTO `DatosModificados` (`CodigoCliente`, `Usuario`, `Password`, `DNI`, `SuministroCiudad`, `SuministroProvincia`, `SuministroCalle`, `SuministroNumero`, `SuministroExtension`, `SuministroExtension2`, `SuministroCP`, `SuministroTelefono1`, `SuministroTelefono2`, `SuministroFax`, `SuministroEmail`, `TitularNombre`, `TitularCalle`, `TitularNumero`, `TitularExtension`, `TitularAclarador`, `TitularPoblacion`, `TitularProvincia`, `TitularTelefono1`, `TitularTelefono2`, `TitularFax`, `TitularEmail`, `TitularDNI`, `FacturaNombre`, `FacturaCalle`, `FacturaNumero`, `FacturaExtension`, `FacturaAclarador`, `FacturaPoblacion`, `FacturaProvincia`, `FacturaTelefono1`, `FacturaTelefono2`, `FacturaFax`, `FacturaEmail`, `FacturaDNI`, `PagadorNombre`, `PagadorCalle`, `PagadorNumero`, `PagadorExtension`, `PagadorAclarador`, `PagadorPoblacion`, `PagadorProvincia`, `PagadorTelefono1`, `PagadorTelefono2`, `PagadorFax`, `PagadorEmail`, `PagadorDNI`, `RepresentanteEmpresa`, `DNIRepresentante`, `CUPS`, `FormaPago`, `BancoDomiciliado`, `NumeroCuenta`,
`IdiomaFactura`,`TarifaContratada`, `PotenciaContratada`, `TipoTension`, `Discriminador`, `TipoAlquiler`, `Maximetro`, `Modo`, `Observaciones`, `FechaRegistro`, `HoraRegistro`, `Fases`, `Reactiva`, `P1Activa`, `P1Reactiva`, `P1Maximetro`, `P2Activa`, `P2Reactiva`, `P2Maximetro`, `P3Activa`, `P3Reactiva`, `P3Maximetro`, `P4Activa`, `P4Reactiva`, `P4Maximetro`, `P5Activa`, `P5Reactiva`, `P5Maximetro`, `P6Activa`, `P6Reactiva`, `P6Maximetro`) VALUES ('".$cod_usuario."', '".$_SESSION['usuario']."', '".$_SESSION['password']."', '".$_SESSION['DNI']."', '".$suministro_poblacion."', '".$suministro_provincia."', '".$suministro_calle."', '".$suministro_numero."', '".$suministro_extension."', '".$suministro_aclarador."', '".$suministro_cp."','".$suministro_telefono."', '".$suministro_movil."', '".$suministro_fax."', '".$suministro_email."', '".$titular_nombre."','".$titular_calle."','".$titular_numero."' , '".$titular_extension."', '".$titular_aclarador."', '".$titular_poblacion."', '".$titular_provincia."', '".$titular_telefono."', '".$titular_movil."', '".$titular_fax."', '".$titular_email."', '".$titular_dni."', '".$factura_nombre."', '".$factura_calle."', '".$factura_numero."', '".$factura_extension."', '".$factura_aclarador."', '".$factura_poblacion."', '".$factura_provincia."', '".$factura_telefono."', '".$factura_movil."', '".$factura_fax."', '".$factura_email."', '".$factura_dni."', '".$pagador_nombre."', '".$pagador_calle."', '".$pagador_numero."', '".$pagador_extension."', '".$pagador_aclarador."', '".$pagador_poblacion."', '".$pagador_provincia."', '".$pagador_telefono."', '".$pagador_movil."', '".$pagador_fax."', '".$pagador_email."', '".$pagador_dni."', '".$representante_nombre."', '".$representante_dni."', '".$_POST["CUPSFijo"]."', '".$forma_pago."', '".$partes_entidad_bancaria[0]."', '".$num_cuenta_completo."','".$idioma_factura."','".$tarifa_contratada."','".$potencia_contratada."', '".$tipo_tension."', '".$discriminador."','".$tipo_alquiler."','".$maximetro."', '".$modo."', '".$observaciones."', '".date("Y-m-d")."', '".date("Y-m-d H:i:s")."','".$fases."', '".$reactiva."', '".$p1_activa."', '".$p1_reactiva."', '".$p1_maximetro."', '".$p2_activa."', '".$p2_reactiva."', '".$p2_maximetro."', '".$p3_activa."', '".$p3_reactiva."', '".$p3_maximetro."', '".$p4_activa."', '".$p4_reactiva."', '".$p4_maximetro."', '".$p5_activa."', '".$p5_reactiva."', '".$p5_maximetro."', '".$p6_activa."', '".$p6_reactiva."', '".$p6_maximetro."');";																												

				}//if($tiene_modificaciones_pendientes==0)

//Si el usuario ya tenia un registro en la base de datos, se realizara una actualizacion del registro				
				else				
				{
					$solicitud_modificacion="UPDATE `DatosModificados` SET `TitularNombre` = '".$titular_nombre."', `TitularCalle` = '".$titular_calle."',`TitularNumero` = '".$titular_numero."', `TitularExtension` = '".$titular_extension."',`TitularAclarador` = '".$titular_aclarador."', `TitularPoblacion` = '".$titular_poblacion."',`TitularProvincia` = '".$titular_provincia."',`TitularTelefono1` = '".$titular_telefono."', `TitularTelefono2` = '".$titular_movil."',`TitularFax` = '".$titular_fax."',`TitularEmail` = '".$titular_email."', `TitularDNI` = '".$titular_dni."',`FacturaNombre` = '".$factura_nombre."',`FacturaCalle` = '".$factura_calle."',`FacturaNumero` = '".$factura_numero."',`FacturaExtension` = '".$factura_extension."',`FacturaAclarador` = '".$factura_aclarador."',`FacturaPoblacion` = '".$factura_poblacion."',`FacturaProvincia` = '".$factura_provincia."',`FacturaTelefono1` = '".$factura_telefono."',`FacturaTelefono2` = '".$factura_movil."',`FacturaFax` = '".$factura_fax."',`FacturaEmail` = '".$factura_email."',`FacturaDNI` = '".$factura_dni."',`PagadorNombre` = '".$pagador_nombre."',`PagadorCalle` = '".$pagador_calle."',`PagadorNumero` = '".$pagador_numero."',`PagadorExtension` = '".$pagador_extension."',`PagadorAclarador` = '".$pagador_aclarador."',`PagadorPoblacion` = '".$pagador_poblacion."',`PagadorProvincia` = '".$pagador_provincia."',`PagadorTelefono1` = '".$pagador_telefono."',`PagadorTelefono2` = '".$pagador_movil."',`PagadorFax` = '".$pagador_fax."',`PagadorEmail` = '".$pagador_email."',`PagadorDNI` = '".$pagador_dni."',`RepresentanteEmpresa` = '".$representante_nombre."',`DNIRepresentante` = '".$representante_dni."',`FormaPago` = '".$forma_pago."',`BancoDomiciliado` = '".$partes_entidad_bancaria[0]."',`NumeroCuenta` = '".$num_cuenta_completo."',`IdiomaFactura` = '".$idioma_factura."',`Observaciones` = '".$observaciones."',`FechaRegistro` = '".date("Y-m-d")."',`HoraRegistro` = '".date("Y-m-d H:i:s")."' WHERE `CodigoCliente` =".$cod_usuario.";";
																													
				}//else($tiene_modificaciones_pendientes==0)

//En cualquier caso ejecutamos la sentencia SQL contruida				
				$ejecutar_solicitud_modificacion=mysql_query($solicitud_modificacion);
					
		}//switch($error)
	}//if (isset($_POST["submit"]))
}//else ($_SESSION['usuario'])
}		

?>        
    </div><!--<div id="central_oficina"-->
</body>
</html>

