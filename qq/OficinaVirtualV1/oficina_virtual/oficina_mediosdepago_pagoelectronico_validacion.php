<?php
/*
NOTAS:

1- ESTA SECCION TOMA LOS DATOS DE LA TABLA FacturasPDF

- ESCRIBE EN LA TABLA SolicitudFacturas

*/

/* NOTA:

 COMO NO SE CIERRA LA CONEXION TRAS CADA CONEXION CON LA BASE DE DATOS, LAS CONSULTAS DEJAN DE FUNCIONAR SI SE ESTABLECE COMO UNICA CONEXION
 LA QUE UTILIZA EL RESTO DE LA WEB, POR LO TANTO HASTA QUE SE HAGA LA MIGRACION COMPLETA DE LOS TEXTOS, COEXISTIRAN LAS DOS CONEXIONES, YA QUE
 SE HA AÑADIDO UNA LLAMADA A LA CONEXION BUENA EN ESTE ARCHIVO.
*/

include("_conexion.php");

//Se incluye el archivo que contiene las verificaciones de datos
include("../includes/verificaciones.php");

//Se incluye el archivo con los textos especifico de esta seccion en el idioma oportuno
//include($_SESSION["directorio_raiz"]."idiomas/".$_SESSION["idioma"]."/oficina_informacion_facturas.php");
include("../idiomas/".$_SESSION["idioma"]."/oficina_datos_titular.php");
include("../idiomas/".$_SESSION["idioma"]."/oficina_informacion_facturas.php");

include("../idiomas/".$_SESSION["idioma"]."/oficina_informacion_certificadofacturas.php");

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<title><?=$titulo_web;?></title>

<!--Se añaden las hojas de estilos genericas que utiliza esta seccion-->
<link href="../css/estilos.css" rel="stylesheet" type="text/css" />
<link href="../css/secciones.css" rel="stylesheet" type="text/css" />
<link href="css/oficina.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_vieja.css" rel="stylesheet" type="text/css" />

<link href="css/oficina_informacion_contratos_y_facturas.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_informacion_facturas.css" rel="stylesheet" type="text/css" />

<link href="css/oficina_informacion_contratos_y_facturas.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_informacion_certificadoconsumos.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_informacion_certificadofacturas.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" media="all" href="scripts/calendar/skins/aqua/theme.css" title="Aqua" />


<!-- librería principal del calendario -->
<script type="text/javascript" src="scripts/calendar/calendar.js"></script>

<!-- librería para cargar el lenguaje deseado -->
<script type="text/javascript" src="scripts/calendar/lang/calendar-es.js"></script>

<!-- librería que declara la función Calendar.setup, que ayuda a generar un calendario en unas pocas líneas de código -->
<script type="text/javascript" src="scripts/calendar/calendar-setup.js"></script>




<!--Ahora se añaden las reglas para Internet Explorer. Asi corregiremos las diferencias de visualizacion entre navegadores
Es importante añadirlo despues de las reglas principales, para que se sobreescriban las correcciones pertinentes. En este caso las diferencias
solo son para internet explorer 7-->

<!--[if IE 7]>
	<link href="../css/estilos_ie7.css" rel="stylesheet" type="text/css" />
	<link href="../css/secciones_ie7.css" rel="stylesheet" type="text/css" />    
<![endif]-->

<!--[if IE]>
	<link href="css/tablas_informacionIE.css" rel="stylesheet" type="text/css" />
<![endif]-->

<!--[if IE 7]>
	<link href="css/tablas_informacionIE7.css" rel="stylesheet" type="text/css" />
<![endif]-->

<script language="javascript" type="text/javascript">
function avisa()
{
	alert("OPCION NO DISPONIBLE");
}//function avisa()
//-->
</script>

<?php

//Asignamos los valores por defecto a la fecha de inicio y la fecha de fin, estos cambiaran si el usuario pulsa el boton del formulario habiendo escrito algo
$fecha_inicio="dd-mm-aaaa";
$fecha_fin="dd-mm-aaaa";

if(isset($_POST["Submit"]))
{	
	$fecha_inicio=$_POST["DesdeFecha"];
	$fecha_fin=$_POST["HastaFecha"];
	$observaciones=$_POST["observaciones"];	
	
	$error=comparar_fechas($fecha_inicio,$fecha_fin);		
}//if(isset($_POST["Submit"]))

?>






<?php
if (isset($_POST["submit"]))
{
//Recogemos los valores de las variables
	//Recogemos los valores de las variables
	
	//$titular_nombre=str_replace("'","&quot;",$_POST["TitularNombre"]);
	

	$factura_nombre=$_POST["FacturaNombre"];							
	$factura_calle=$_POST["FacturaCalle"];								
	$factura_numero2=$_POST["FacturaNumero"];
	if ($factura_numero2=='0') {$factura_numero='';} else {$factura_numero=$factura_numero2;};
	
	$factura_extension=$_POST["FacturaExtension"];
	$factura_aclarador=$_POST["FacturaAclarador"];
	$factura_cp=$_POST["FacturaCP"];		
	$factura_poblacion=$_POST["FacturaPoblacion"];		
	$factura_provincia=$_POST["FacturaProvincia"];
	
	$factura_telefono=$_POST["FacturaTelefono"];		
	$factura_movil=$_POST["FacturaMovil"];			
	$factura_fax=$_POST["FacturaFax"];	
	$factura_email=$_POST["FacturaEmail"];	
	$factura_dni=$_POST["FacturaDNI"];
	
}
?>


</head>
	
<body>




<script> 

function addCommas(nStr)
{
	nStr += '';
	x = nStr.split('.');
	x1 = x[0];
	x2 = x.length > 1 ? ',' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + '.' + '$2');
	}
	return x1 + x2;
}



function sumaCheck(num){ var suma=0; 
for(i=1;i<num+1;i++){ 
if(document.getElementById("check"+i).checked) 
suma = suma + parseFloat(document.getElementById("check"+i).value)
suma2 = suma.toFixed(2)
suma3= addCommas(suma2)
} 
document.getElementById("seleccionada").value='S';
document.getElementById("importeseleccionado").value=suma3
return (suma3); 
} 
</script> 





<!--*************************************************Web realizada por ****************************************-->
	<div id="central_oficina">
    
<?php
//Se comprueba que el usuario se haya identificado, si no es asi se da un aviso y redirigimos a la seccion de identificacion
if ($_SESSION['usuario']=="")
{	
	MsgBox($error_identificacion);
	redirigir("oficina_index.php?id=".$_SESSION["idioma"]);
	
}//if ($_SESSION['usuario'])

//Si el usuario se ha identificado la seccion se ejecutara con normalidad
else
{
//Añadimos los fragmentos comunes, la cabecera, el menu, y el submenu
	include("oficina_fragmentos_comunes.php");
?>    
	  <div class="contenido_seccion_oficina_mediospago">                                         
       	
  <form action="oficina_mediosdepago_pagoelectronico_pago.php?id=<?=$_SESSION["idioma"]?>"  method="post" name="form_factura_seleccion" id="form_factura_seleccion">

        <table width="600" border="0" cellspacing="0" cellpadding="0">
		  <?php if (isset($_SESSION['numeroCliente'])){
					 		$result = mysql_query("SELECT * FROM DatosRegistrados WHERE CodigoCliente = ".$_SESSION['numeroCliente']." ");
							$row = mysql_fetch_array($result);
					 }else{
					 		$result = mysql_query("SELECT * FROM DatosRegistrados WHERE CodigoCliente = -1");
							//Para que no tome ningun cliente pero no de ningun error en la pagina
							$row = mysql_fetch_array($result);
					 }
					 
					 
					 
				if (!isset($_POST["submit"]))	
							{								
//Si el formulario no se ha aceptado, se mostraran los valores almacenados en la base de datos
							    $factura_nombre=$row["FacturaNombre"];
								$factura_calle=$row["FacturaCalle"];
								$factura_numero2=$row["FacturaNumero"];
								if ($factura_numero2=='0') {$factura_numero='';} else {$factura_numero=$factura_numero2;};
								$factura_extension=$row["FacturaExtension"];
								$factura_aclarador=$row["FacturaAclarador"];
								$factura_cp=$row["FacturaCP"];
								$factura_poblacion=$row["FacturaPoblacion"];
								$factura_provincia=$row["FacturaProvincia"];
								$factura_telefono=$row["FacturaTelefono1"];
								$factura_movil=$row["FacturaTelefono2"];
								$factura_fax=$row["FacturaFax"];
								$factura_email=$row["FacturaEmail"];
								$factura_dni=$row["FacturaDNI"];
								
								
															
							}
					 ?>
            <tr> 
              <td valign="top"><div align="center">
             
                
                <table width="640" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td height="47"><div align="center"><span class="arialblack18Titulo">Medio Electr&oacute;nico Pago Facturas Pendientes</span></div></td>
                  </tr>
                  <tr>
                    <td height="20"><div align="center"></div></td>
                  </tr>
                 
                  <tr>
                    <td><div align="center">
                      <table width="633" border="0" cellpadding="0" cellspacing="0" >
                        <tr>
                          <td width="633" class="arial14"><fieldset >
                            <legend class="arialblack14">
                              <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 130'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                              </legend>
                            <table width="500" align="center">
                              <tr>
                                <td width="148" height="22" valign="top" class="arial14"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 196'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                                  &nbsp;</td>
                                <td width="57" valign="top" class="arial14"><input name="CodigoCliente" type="text" class="textfieldLecturaNum" id="CodigoCliente" value="<?php echo($row['CodigoCliente']);?>" size="5" maxlength="5"  readonly="readonly" /></td>
                                <td width="20" valign="top" class="arial14">&nbsp;</td>
                                <td width="47" valign="top" class="arial14">CUPS</td>
                                <td width="204" valign="top" class="arial14">&nbsp;&nbsp;&nbsp;
                                  <input name="CUPSFijo" value="<?php echo($row['CUPS']);?>"  type="text" class="textfieldLectura" id="CUPSFijo" size="25" maxlength="25" /></td>
                                </tr>
                              </table>
                            </fieldset></td>
                        </tr>
                      </table>
                    </div>
                    <div align="center"></div></td>
                  </tr>
                  <tr>
                    <td height="25">&nbsp;</td>
                  </tr>
                  <tr>
                    <td><div align="center"><span class="arialblack16Titulo">M&eacute;todo de Pago de las Facturas Seleccionadas</span> <br />
                    </div></td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                  </tr>
                  
<!--CIERRE DEL APARTADO-->
                  <tr>
                    <td height="27"><p align="left"><span class="arialblack14">
                      <? //   =$oficina_datos_titular16?>
                    </span></p>
                      <table width="550" border="0" cellpadding="0" cellspacing="0">
                        <tr class="arial12">
                          <td colspan="2" class="arial14" width="550"><fieldset   >
                            <legend class="arialblack14">Forma de Pago</legend>
                            <p align="center" class="arial12Importanteb">El Sistema de Pago no admite el uso de Tarjetas Virtuales.<br />
                              Es conveniente tener a la vista la Tarjeta de Cr&eacute;dito a trav&eacute;s de la cual va a realizar el Pago</p>
                            <table width="600" border="0" cellspacing="2" cellpadding="0">
                              <tr>
                                <td width="148">Titular de la Tarjeta</td>
                                <td width="33">&nbsp;</td>
                                <td colspan="4"><input name="FacturaProvincia2" type="text"  value="<?=$titular_tarjeta?>"  class="textfield" id="FacturaProvincia2" size="60" maxlength="50" /></td>
                              </tr>
                              <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td width="102">&nbsp;</td>
                                <td width="95">&nbsp;</td>
                                <td width="97">&nbsp;</td>
                                <td width="111">&nbsp;</td>
                              </tr>
                              <tr>
                                <td>N&uacute;mero de Tarjeta</td>
                                <td>&nbsp;</td>
                                <td colspan="4"><div align="left">
                                  <input name="NumeroTarjeta1" type="text"  value="<?=$numero_tarjeta_1?>"  class="textfieldnum" id="NumeroTarjeta1" size="3" maxlength="4" />
                                  <input name="NumeroTarjeta2" type="text"  value="<?=$numero_tarjeta_2?>"  class="textfieldnum" id="NumeroTarjeta2" size="3" maxlength="4" />
                                  <input name="NumeroTarjeta3" type="text"  value="<?=$numero_tarjeta_3?>"  class="textfieldnum" id="NumeroTarjeta3" size="3" maxlength="4" />
                                  <input name="NumeroTarjeta4" type="text"  value="<?=$numero_tarjeta_4?>"  class="textfieldnum" id="NumeroTarjeta4" size="3" maxlength="4" />
                                </div>
                                  <div align="left"></div>
                                  <div align="left"></div>
                                  <div align="left"></div></td>
                              </tr>
                              <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                              </tr>
                              <tr>
                                <td>Fecha de Caducidad</td>
                                <td>&nbsp;</td>
                                <td><div align="center">Mes
                                    <select name="MesCaducidad" id="MesCaducidad">
                                      <option>1</option>
                                      <option>2</option>
                                      <option>3</option>
                                      <option>4</option>
                                      <option>5</option>
                                      <option>6</option>
                                      <option>7</option>
                                      <option>8</option>
                                      <option>9</option>
                                      <option>10</option>
                                      <option>11</option>
                                      <option>12</option>
                                    </select>
                                </div></td>
                                <td><div align="center">A&ntilde;o 
                                  <select name="AnoCaducidad" id="AnoCaducidad">
                                    <option>2011</option>
                                    <option>2012</option>
                                    <option>2013</option>
                                    <option>2014</option>
                                    <option>2015</option>
                                    <option>2016</option>
                                    <option>2017</option>
                                    <option>2018</option>
                                    <option>2019</option>
                                    <option>2020</option>
                                  </select>
                                </div></td>
                                <td><div align="right"></div></td>
                                <td><label for="AnoCaducidad"></label></td>
                              </tr>
                              <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                              </tr>
                              <tr>
                                <td>CCV2</td>
                                <td>&nbsp;</td>
                                <td><input name="ccv2" type="text"  value="<?=$ccv2?>"  class="textfieldnum" id="ccv2" size="2" maxlength="3" /></td>
                                <td colspan="2"><div align="center" class="arial12black">&iquest;Que es el CVV2?</div></td>
                                <td>&nbsp;</td>
                              </tr>
                            </table>
                            <p>&nbsp;</p>
                            <table width="600" border="0" cellspacing="2" cellpadding="0">
                              <tr>
                                <td height="36" colspan="2" class="arialblack14">Condiciones Legales</td>
                              </tr>
                              <tr>
                                <td colspan="2">AVISO LEGAL: Este aviso regula la utilizaci&oacute;n por parte de los usuarios de las p&aacute;ginas web contenidas en este sitio de Internet (en adelante website), propiedad de esta empresa.</td>
                              </tr>
                              <tr>
                                <td colspan="2">&nbsp;</td>
                              </tr>
                              <tr>
                                <td colspan="2"><label for="CondicionesLegales"></label>
                                  <textarea name="CondicionesLegales" cols="145" rows="7" class="arial10Negro" id="CondicionesLegales">El acceso y la navegaci&oacute;n en el website implican el conocimiento y aceptaci&oacute;n de las advertencias legales, las condiciones y los t&eacute;rminos de uso contenidos en el mismo.

Entradas See Tickets S.A. aplica medidas dirigidas a garantizar que la navegaci&oacute;n en el website se realice en condiciones &oacute;ptimas, aunque no garantiza su disponibilidad y continuidad en todo momento. Particularmente, Entradas See Tickets S.A. no se responsabiliza de las ca&iacute;das o interrupciones del servicio de comunicaciones electr&oacute;nicas que haya de servir de soporte a los servicios de la sociedad de la informaci&oacute;n que son ofrecidos a trav&eacute;s del website.

Entradas See Tickets S.A. aplica medidas dirigidas a garantizar la privacidad y la seguridad de las comunicaciones electr&oacute;nicas precisas para la contrataci&oacute;n de los productos y de los servicios que comercializa a trav&eacute;s del presente website o que impliquen la captura de datos personales y cumple con los est&aacute;ndares de seguridad tecnol&oacute;gicamente disponibles en la actualidad (como el sistema &quot;Verisign&quot;). No obstante, Entradas See Tickets S.A. no garantiza la privacidad y la seguridad en la utilizaci&oacute;n del website y, en particular, no garantiza que terceros no autorizados no puedan tener conocimiento acerca de la clase, condiciones, caracter&iacute;sticas y circunstancias del uso que los usuarios realizan del website.

Entradas See Tickets S.A. aplica medidas dirigidas a garantizar la no presencia en el website o en sus contenidos de elementos de car&aacute;cter inform&aacute;tico que puedan producir alteraciones en los equipos para el proceso de informaci&oacute;n de los usuarios o en cualquier tipo de archivo almacenado en los mismos. No obstante, Entradas See Tickets S.A. no garantiza la no introducci&oacute;n en el website o en sus contenidos por parte de los usuarios o de terceros de elementos de car&aacute;cter inform&aacute;tico que produzcan una alteraci&oacute;n de los equipos para el proceso de informaci&oacute;n de los usuarios o de cualquier tipo de archivo almacenado en los mismos.

La informaci&oacute;n que aparece en el website es la vigente en la fecha de su &uacute;ltima actualizaci&oacute;n. Entradas See Tickets S.A. se reserva el derecho a, en cualquier momento, actualizar, modificar o eliminar la informaci&oacute;n de este website, pudiendo limitar o no permitir el acceso al mismo. Entradas See Tickets S.A. declina la responsabilidad frente al uso indebido de los contenidos de su p&aacute;gina web por parte de los usuarios, as&iacute; como frente a informaciones no contenidas en su p&aacute;gina web o que no hayan sido elaboradas por Entradas See Tickets S.A.

Entradas See Tickets S.A. realiza los m&aacute;ximos esfuerzos para evitar cualquier error en los contenidos del website, aunque no garantiza ni se responsabiliza de las consecuencias derivadas de los errores en los contenidos del website proporcionados por terceros.

Entradas See Tickets S.A. no concede ninguna licencia o autorizaci&oacute;n de uso de ninguna clase sobre sus derechos de propiedad industrial e intelectual o sobre cualquier otra propiedad o derecho relacionado con el website o sus contenidos.

                            </textarea></td>
                              </tr>
                              <tr>
                                <td colspan="2">&nbsp;</td>
                              </tr>
                              <tr>
                                <td width="229"><div align="center"><img src="../img/oficina/formaspago.jpg" width="149" height="99" /></div></td>
                                <td width="366" class="arial12Negro">Servicio Garantizado y respaldado por Verisign.<br />
                                  Los Datos facilitados se enviar&aacute;n encriptados a un servidor seguro en el que ser&aacute;n tratados de forma estr&iacute;ctamente confidencial</td>
                              </tr>
                              <tr>
                                <td colspan="2">&nbsp;</td>
                              </tr>
                              <tr>
                                <td colspan="2" class="arial12Negro"><blockquote>
                                  <p>He le&iacute;do y al pulsar la opci&oacute;n de &quot;comprar&quot;, acepto y estoy conforme con las condiciones generales del contrato y la pol&iacute;tica de privacidad y protecci&oacute;n de datos</p>
                                </blockquote></td>
                              </tr>
                              <tr>
                                <td colspan="2" class="arial12Negro">&nbsp;</td>
                              </tr>
                              <tr>
                                <td colspan="2" class="arial12Negro"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr>
                                    <td width="58"><div align="center">
                                      <input type="checkbox" name="recibircomunicaciones" id="recibircomunicaciones" />
                                    </div>
                                      <label for="recibircomunicaciones"></label>
                                      <div align="center"></div></td>
                                    <td width="524">No deseo recibir comunicaciones electr&oacute;nica de car&aacute;cter comercial enviadas por esta empresa</td>
                                  </tr>
                                </table></td>
                              </tr>
                            </table>
                            <p align="right">
                              <input type="submit" name="finalizarcompra" id="finalizarcompra" value="Finalizar Compra" />
                            </p>
                            <p>&nbsp;</p>
                          </fieldset></td>
                        </tr>
                        <!--CIERRE DEL APARTADO-->
                    </table></td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                  </tr>
                </table>
              </div>
           </tr>
             
<!--CIERRE DEL APARTADO-->                      

	      </td>
          </tr>
           </table>
        <!--VERSION VIEJA-->        	
       </form> 	
  </div><!--<div class="contenido_seccion_oficina">-->                        
		
        
        <div class="limpiar"></div>
                        	
<!--Por ultimo aparecera el pie de la web-->
<?php 
	include("../includes/pie.php");


		if(isset($_POST["submit"]))
	{
//Por ultimo mostraremos el error correspondiente, en el caso de que haya fallado alguna de las verificaciones, o continuaremos con el proceso		
		
				$insertar_facturas_seleccionadas="INSERT INTO `PagoRemesas` (`NumFactura1`, `NumFactura2`, `NumFactura3`, `NumFactura4`, `NumFactura5`, `ImporteFactura1`, `ImporteFactura2`, `ImporteFactura3`, `ImporteFactura4`, `ImporteFactura5`, `ImporteTotal`, `FechaPago`) VALUES ('1','2','3','4','5','100,20','57,30','654,20','65,30','25,10','".$importetotal."', '".date("Y-m-d H:i:s")."'); ";
				
				//echo($insertar_facturas_seleccionadas);
				$ejecutar_solicitud_facturas=mysql_query($insertar_facturas_seleccionadas);
				//echo($ejecutar_solicitud_facturas);											
				
				redirigir("oficina_mediosdepago_pagoelectronico_validacion.php?id=".$_SESSION["idioma"]);

										
			
		
	}//if(isset($_POST["Submit"]))	
		



}//else ($_SESSION['usuario'])
?>        
    </div><!--<div id="central_oficina"-->
</body>
</html>
