function objetoAjax(){
	var xmlhttp=false;
	try {
		xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
	} catch (e) {
		try {
		   xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		} catch (E) {
			xmlhttp = false;
  		}
	}

	if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
		xmlhttp = new XMLHttpRequest();
	}
	return xmlhttp;
}

function MostConsulta(inicio, fin, nif){
	divResultado = document.getElementById('resultado');
	ajax=objetoAjax();
	ajax.open("POST", "consulta.php?inicio="+inicio+"&fin="+fin+"&nif="+nif);
	ajax.onreadystatechange=function() {
		if (ajax.readyState==4) {
			divResultado.innerHTML = ajax.responseText
		}
	}
	ajax.send(null)
}

function MostrarConsulta(cliente, dni, titular, direccion, numero, provincia, cups){
	//alert('0');
	divResultado = document.getElementById('resultado');
	ajax=objetoAjax();
	ajax.open("POST", "consultaBusqueda.php?cliente="+cliente+"&dni="+dni+"&titular="+titular+"&direccion="+direccion+"&numero="+numero+"&provincia="+provincia+"&cups="+cups);
	ajax.onreadystatechange=function() {
		if (ajax.readyState==4) {
			divResultado.innerHTML = ajax.responseText
		}
	}
	ajax.send(null);
}

function MostrarConsulta2(cliente, dni, titular, direccion, numero, provincia, cups, agente){
	// alert('0');
	divResultado = document.getElementById('resultadoAgente');
	ajax=objetoAjax();
	ajax.open("POST", "consultaBusquedaAgente.php?cliente="+cliente+"&dni="+dni+"&titular="+titular+"&direccion="+direccion+"&numero="+numero+"&provincia="+provincia+"&cups="+cups+"&agente="+agente);
	ajax.onreadystatechange=function() {
		if (ajax.readyState==4) {
			divResultado.innerHTML = ajax.responseText
		}
	}
	ajax.send(null);
}


function MostrarConsultaAgentes(inicio, fin, nif){
	divResultado = document.getElementById('resultado');
	ajax=objetoAjax();
	ajax.open("POST", "consultaAgentes.php?inicio="+inicio+"&fin="+fin+"&nif="+nif);
	ajax.onreadystatechange=function() {
		if (ajax.readyState==4) {
			divResultado.innerHTML = ajax.responseText
		}
	}
	ajax.send(null)
}