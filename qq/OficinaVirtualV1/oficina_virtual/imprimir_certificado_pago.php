<?php 
session_start();
include("_conexion.php");
include("../idiomas/".$_SESSION["idioma"]."/oficina_informacion_facturas.php");
include("../idiomas/".$_SESSION["idioma"]."/oficina_informacion_certificadofacturas.php");

//Se comprueba que el usuario se haya identificado, si no es asi se da un aviso y redirigimos a la seccion de identificacion
if ($_SESSION['usuario']=="")
{	
	MsgBox($error_identificacion);
	redirigir("oficina_index.php?id=".$_SESSION["idioma"]);
	
}//if ($_SESSION['usuario'])

//Si el usuario se ha identificado la seccion se ejecutara con normalidad
else
{
	$sql = "SELECT * FROM DatosRegistrados WHERE CodigoCliente = ".$_SESSION['numeroCliente']."";
	$result = mysql_query($sql);
	$row = mysql_fetch_array($result);
	
    $consulta_medio_pago_doc="SELECT * FROM RecibosPendientes WHERE FechaPago IS NULL AND NumeroCliente=".$_SESSION['numeroCliente']." AND Seleccionada='S' ORDER BY `FechaFactura` DESC";
			
	$resultados_medio_pago_doc = mysql_query($consulta_medio_pago_doc);
	$num_medio_pago_doc=mysql_num_rows($resultados_medio_pago_doc);
	$n2=$num_medio_pago_doc;
	$importetotalprev=0;
	
	
	
	
if($num_medio_pago_doc != 0)
			{while($registro_medio_pago_doc = mysql_fetch_array($resultados_medio_pago_doc))
				{
					$importetotalprev=$importetotalprev+$registro_medio_pago_doc["ImportePendiente"];
					$titular_tarjeta=$registro_medio_pago_doc["TitularTarjeta"];
					$num_tarjeta_completo=$registro_medio_pago_doc["NumeroTarjeta"];
					
					$pagador_nombre=$registro_medio_pago_doc["PagadorNombre"];
					$pagador_direccion=$registro_medio_pago_doc["PagadorDireccion"];
					$pagador_cp=$registro_medio_pago_doc["PagadorCP"];
					$pagador_poblacion=$registro_medio_pago_doc["PagadorPoblacion"];
					$pagador_telefono=$registro_medio_pago_doc["PagadorTelefono"];
					$pagador_email=$registro_medio_pago_doc["PagadorEmail"];
					
					
					
					
					}
				}


	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?=$titulo_web;?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="css/oficina_vieja.css" rel="stylesheet" type="text/css" />
<link href="css/oficina.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_vieja.css" rel="stylesheet" type="text/css" />

<link href="css/oficina_informacion_contratos_y_facturas.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_informacion_facturas.css" rel="stylesheet" type="text/css" />

<link href="css/oficina_informacion_contratos_y_facturas.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_informacion_certificadoconsumos.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_informacion_certificadofacturas.css" rel="stylesheet" type="text/css" />


<script language="javascript" type="text/javascript">
//<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
//-->

//Esta funcion hace que al cargar la pagina se habra la ventana de dialogo de impresion
function imprimir()
{
	window.print();
}//function dimensiona()

//-->
</script>
</head>

<body onload="javascript:imprimir()">

<!--*************************************************Web realizada por ****************************************-->

<div id="Layer3" border="1" style="position:absolute; left:25px; top:14px; width:637px; height:23px; z-index:1" class="arialblack16Titulo">
  
  
  <table width="631" border="1" cellspacing="0" cellpadding="0" align="center">
  <tr>
    <td width="627" align="center">DOCUMENTO ACREDITATIVO DE PAGO FACTURAS A TRAV&Eacute;S DE WEB&nbsp;</td>
  </tr>
</table>

</div>
<div id="Layer5" style="position:absolute; left:24px; top:58px; width:639px; height:530px; z-index:2" class="arial12Negro"> 

  <div align="center"><span class="arial12black">
    </p>
  Este Documento acredita que la informaci&oacute;n de Pago que hacermos referencia, ha sido realizada correctamente por medio de Tarjeta de Cr&eacute;dito dentro de nuestra Oficina Virtual de la P&aacute;gina Web.</span><br />
  <br />
  </div>
  <div align="left">
    <table width="431" border="1" cellspacing="0" cellpadding="0">
      <tr>
        <td height="20" width="161" class="arial12Negrob pad"><div align="right"><strong>Referencia de Pago:</strong></div></td>
        <td height="20" class="arial10Negro pad"> <div align="right">***********************</div></td>
      </tr>
      <tr>
        <td height="20" class="arial12Negrob pad"><div align="right">Importe Cargado en Tarjeta:</div></td>
        <td height="20" class="arial12Negro pad"><div align="right">
          <?=number_format($importetotalprev,2,',','.')?>&nbsp;&nbsp; <? echo(" ");?>
        </div></td>
      </tr>
      <tr>
        <td height="20" class="arial12Negrob pad"><div align="right"><strong>Titular de la Tarjeta:</strong></div></td>
        <td height="20" class="arial12Negro pad"><div align="right">
          <?=$titular_tarjeta?>
        </div></td>
      </tr>
      <tr class="arial12Gris">
        <td height="20" class="arial12Negrob pad"><div align="right"><strong>N&uacute;mero de Tarjeta</strong></div></td>
        <td height="20" class="arial12Negro pad"><div align="right">
        
        
        <?php
$num_tarjeta_completo[4] = "*";
$num_tarjeta_completo[5] = "*";
$num_tarjeta_completo[6] = "*";
$num_tarjeta_completo[7] = "*";
$num_tarjeta_completo[8] = "*";
$num_tarjeta_completo[9] = "*";
$num_tarjeta_completo[10] = "*";
$num_tarjeta_completo[11] = "*";

?>
        
        
          <?=$num_tarjeta_completo?>
        </div></td>
      </tr>
    </table>
  </div>
<p>&nbsp;</p>
<div align="center">
  <table width="643" border="1" cellpadding="0" cellspacing="0" class="colfondo">
    <tr >
      <td height="20" ><div align="center">Facturas Pagadas</div></td>
      </tr>
  </table>
  
  <br />

</div>
<div align="center"></div>
  <div align="left">
    <table width="552" border="0" cellspacing="0" cellpadding="0">
      
      
      
      <tr>
                    <td>
                      
                      <div align="center">
                        <table width="645" border="1" cellpadding="0" cellspacing="0" >
                          <tr>
                            <td><div >
                              <div class="columnas_fila4_contrato">
                                <div class="color_fondo_cabecera_contrato2 arial12Negro">N&ordm; Factura</div>
                               
                              </div>
                              <!--<div class="columnas_fila3_contrato">-->
                              <div class="columnas_fila4_contrato">
                                <div class="color_fondo_cabecera_contrato2 arial12Negro">Fecha </div>
                               
                              </div>
                              <!--<div class="columnas_fila3_contrato">-->
                              <div class="columnas_fila4_contrato">
                                <div class="color_fondo_cabecera_contrato2 arial12Negro">N&ordm; Recibo </div>
                            
                              </div>
                              <!--<div class="columnas_fila3_contrato">-->
                              <div class="columnas_fila4_contrato">
                                <div class="color_fondo_cabecera_contrato2 arial12Negro">Origen Fact </div>
                            
                              </div>
                              <!--<div class="columnas_fila3_contrato columna_factura_periodo">-->
                              <div class="columnas_fila4_contrato">
                                <div class="color_fondo_cabecera_contrato2 arial12Negro">Tipo Recibo </div>
                              </div>
                              <!--<div class="columnas_fila3_contrato">-->
                              <div class="columnas_fila4_contrato">
                                <div class="color_fondo_cabecera_contrato2 arial12Negro">Es. Impagado </div>
                            
                              </div>
                              <!--<div class="columnas_fila3_contrato">-->
                              <div class="columnas_fila7_contrato">
                                <div class="color_fondo_cabecera_contrato2 arial12Negro">Importe</div>
                               
                              <!--<div class="columnas_fila3_contrato">-->
                            </div>
                              <!--<div class="columnas_fila1_larga_contrato">--></td>
                          </tr>
                        </table>
                        <?php 
		   
		
			$importetotal=0;
			$i=1;
		
			$consulta_medio_pago_seleccion="SELECT * FROM RecibosPendientes WHERE FechaPago IS NULL AND NumeroCliente=".$_SESSION['numeroCliente']." AND Seleccionada='S' ORDER BY `FechaFactura` DESC";
			
			$resultados_medio_pago_seleccion = mysql_query($consulta_medio_pago_seleccion);
			$num_medio_pago_seleccion=mysql_num_rows($resultados_medio_pago_seleccion);
			$n2=$num_medio_pago_seleccion;
			
			//$registro_medio_pago = mysql_fetch_array($resultados_medio_pago);
			//echo($registro_medio_pago);
			//echo("<br/>");


//Si hay facturas se mostrara un resumen de los datos de cada una de ellas			
			if($num_medio_pago_seleccion != 0)
			{
				
				while($registro_medio_pago_seleccion = mysql_fetch_array($resultados_medio_pago_seleccion))
				{
					
		
?>
                        
                        
                        <table width="646" border="1" cellpadding="0" cellspacing="0" >
                          <tr>
                            <td>
                              
                              
                              <div >
                                <div class="columnas_fila4_contrato">
                                  <div class="color_fondo_datos_contrato2 alto_factura_periodo arial12">
                                    <?=$registro_medio_pago_seleccion["NumeroFactura"]?>
                                  </div>
                                </div>
                                <!--<div class="columnas_fila3_contrato">-->
                                <div class="columnas_fila4_contrato">
                                  <div class="color_fondo_datos_contrato2 alto_factura_periodo arial12">
                                    <?=fecha_normal($registro_medio_pago_seleccion["FechaFactura"])?>
                                  </div>
                                </div>
                                <!--<div class="columnas_fila3_contrato">-->
                                <div class="columnas_fila4_contrato"><div class="color_fondo_datos_contrato2 alto_factura_periodo arial12">
                                    <?=$registro_medio_pago_seleccion["NumeroRecibo"]?>
                                  </div>
                                </div>
                                <!--<div class="columnas_fila3_contrato">-->
                                <div class="columnas_fila4_contrato">
      <div class="color_fondo_datos_contrato2 alto_factura_periodo arial12">
                                    <?
						
						switch ($registro_medio_pago_seleccion["TipoFactura"])
						{
						case 'E':						{$tipofactura="Energía";}						break;
						case 'R':						{$tipofactura="Reconexión";}					break;
						case 'D':						{$tipofactura="Derechos";}						break;
						case 'V':						{$tipofactura="Varios";}						break;
						default :						{$tipofactura="------";}      					break;
						}
						echo($tipofactura);
						
						
						?>
                                  </div>
                                </div>
                                <!--<div class="columnas_fila3_contrato columna_factura_periodo">-->
                                <div class="columnas_fila4_contrato">
                  <div class="color_fondo_datos_contrato2 alto_factura_periodo arial12">
                                    <?
						
						switch ($registro_medio_pago_seleccion["TipoRecibo"])
						{
						case 'F':				{$tiporecibo="Facturado";}					break;
						case 'I':				{$tiporecibo="Impagado";}					break;
						default :				{$tiporecibo="------";}     				break;
						}
						echo($tiporecibo);
									
						?>
                                  </div>
                                </div>
                                <!--<div class="columnas_fila3_contrato">-->
                                <div class="columnas_fila4_contrato">
      <div class="color_fondo_datos_contrato2 alto_factura_periodo arial12">
                                    <?
						
						switch ($registro_medio_pago_seleccion["EstadoImpagado"])
						{
						case '1':				{$tipoestadoimpagado="Pendiente";}					break;
						case 'C':				{$tipoestadoimpagado="Carta de Corte";}				break;
						case 'O':				{$tipoestadoimpagado="Cortado";}					break;
						default :				{$tipoestadoimpagado="------";}     				break;
						}
						echo($tipoestadoimpagado);
									
						?>
                                  </div>
                                </div>
                                <!--<div class="columnas_fila3_contrato">-->
                                <div class="columnas_fila5_contrato">
      <div class="color_fondo_datos_contrato2 alto_factura_periodo arial12_3">
       <?=number_format($registro_medio_pago_seleccion["ImportePendiente"],2,',','.')?>
                                    <? echo(" ");?></div>
                                </div>
                                <!--<div class="columnas_fila3_contrato">-->
                              </div>
                              <!--<div class="columnas_fila1_larga_contrato">-->
                            </td>
                          </tr>
                        </table>
                        
                        
                        <?php 
			$importetotal=$importetotal+$registro_medio_pago_seleccion["ImportePendiente"];
			
			if ($i==1) {
			
			
	$remesafacturas1="INSERT INTO `PagoRemesas` (`NumeroCliente`,`PagadorNombre`,`PagadorDireccion`,`PagadorCP`,`PagadorPoblacion`,`PagadorTelefono`,`PagadorEmail`,`NumFactura".$i."`,`ImporteFactura".$i."`,`ImporteTotal`,`FechaPago`)	VALUES ('".$_SESSION['usuario']."','".$pagador_nombre."', '".$pagador_direccion."', '".$pagador_cp."','".$pagador_poblacion."','".$pagador_telefono."','".$pagador_email."','".$registro_medio_pago_seleccion["NumeroFactura"]."','".$registro_medio_pago_seleccion["ImportePendiente"]."','".$importetotalprev."','".date("Y-m-d H:i:s")."')";
	
	$ejecutar_remesafacturas1=mysql_query($remesafacturas1);
	
	$remesaselect="SELECT * from `PagoRemesas` where `FechaPago`='".date("Y-m-d H:i:s")."' AND `NumeroCliente` =".$_SESSION['usuario']."";
	
	$remesa=mysql_query($remesaselect);
	
	//echo($remesafacturas1);
	//echo($remesaselect);
	//echo($remesa);
	$registro_remesa= mysql_fetch_array($remesa);
	//echo($registro_remesa["NumeroRemesa"]);
	
	

	
				} else {
	
	//echo($registro_remesa["NumeroRemesa"]);
	
	
	$remesafacturas2="UPDATE `PagoRemesas` SET `NumFactura".$i."`='".$registro_medio_pago_seleccion["NumeroFactura"]."',`ImporteFactura".$i."`='".$registro_medio_pago_seleccion["ImportePendiente"]."' where `NumeroRemesa`=".$registro_remesa["NumeroRemesa"]." AND `NumeroCliente` =".$_SESSION['usuario']."";
			
	//		echo($remesafacturas2);
	$ejecutar_remesafacturas2=mysql_query($remesafacturas2);
				}

		$facturas="UPDATE `RecibosPendientes` SET `NumeroRemesa`=".$registro_remesa["NumeroRemesa"].",`FechaPago`='".date("Y-m-d H:i:s")."',`HoraPago`='".date("H")."',`MinutoPago`='".date("i")."',`ImportePago`='".$importetotalprev."' where `NumeroFactura`=".$registro_medio_pago_seleccion["NumeroFactura"]." AND `NumeroCliente` =".$_SESSION['usuario']."";
	//echo($facturas);
			
	$ejecutar_facturas=mysql_query($facturas);
				
				//$ejecutar_datospersonales=mysql_query($datospersonalesseleccionadas);
					
	//if(isset($_POST["Submit"]))	
			
			$i=$i+1;

			
}//while($row = mysql_fetch_array($result))				  
   }//if($num_facturas != 0)
		   
//Si no hay facturas en la base de datos mostraremos un mensaje al usuario
		   else
		   {
?>
                        
                      </div>
                      <?php		   
		   }//else($num_facturas != 0)
$i=$i-1;

?>
        </td>
      </tr>
      
      
    </table>
  </div>
  <div align="center"><br />
     <br />
  <br />
  </div>
  <div align="left">
    <table width="427" border="1" cellspacing="0" cellpadding="0">
      <tr>
        <td height="20" colspan="2" class="colfondo"><div align="center">Datos Personales Facilitados</div></td>
       </tr>
      <tr>
        <td width="122" height="20" class="arial12Negrob pad"> <div align="right">Nombre</div></td>
        <td width="299" height="20" class="arial12Negro pad"> 
          <div align="right"><?=$pagador_nombre?></div></td>
       </tr>
   
      <tr>
        <td height="20" class="arial12Negrob pad"><div align="right">Direcci&oacute;n</div></td>
        <td height="20" class="arial12Negro pad"><div align="right"><?=$pagador_direccion?></div></td>
       </tr>
      <tr>
        <td height="20" class="arial12Negrob pad"><div align="right">C&oacute;digo Postal</div></td>
        <td height="20" class="arial12Negro pad"><div align="right"><?=$pagador_cp?></div></td>
       </tr>
      <tr>
        <td height="20" class="arial12Negrob pad"><div align="right">Poblaci&oacute;n</div></td>
        <td height="20" class="arial12Negro pad"><div align="right"><?=$pagador_poblacion?></div></td>
       </tr>
      <tr>
        <td height="20" class="arial12Negrob pad"><div align="right">Tel&eacute;fono</div></td>
        <td height="20" class="arial12Negro pad"><div align="right"><?=$pagador_telefono?></div></td>
       </tr>
      <tr>
        <td height="20" class="arial12Negrob pad"><div align="right">Email</div></td>
        <td height="20" class="arial12Negro pad"><div align="right"><?=$pagador_email?></div></td>
       </tr>
    </table>
  </div>
  <div align="center">	
    <p>&nbsp;</p>
    <p><a href="oficina_mediosdepago_pagoelectronico.php">VOLVER</a><br />
    </p>
  </div>
</div>
</body>
</html>
<?php
}//else ($_SESSION['usuario'])

?>