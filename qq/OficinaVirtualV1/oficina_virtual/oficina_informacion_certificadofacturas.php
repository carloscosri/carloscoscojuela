<?php
/*
NOTAS:

1- ESTA SECCION TOMA LOS DATOS DE LA TABLA FacturasPDF

2- ESCRIBE EN LA TABLA SolicitudCertificadoEconomico

*/

/* NOTA:

 COMO NO SE CIERRA LA CONEXION TRAS CADA CONEXION CON LA BASE DE DATOS, LAS CONSULTAS DEJAN DE FUNCIONAR SI SE ESTABLECE COMO UNICA CONEXION
 LA QUE UTILIZA EL RESTO DE LA WEB, POR LO TANTO HASTA QUE SE HAGA LA MIGRACION COMPLETA DE LOS TEXTOS, COEXISTIRAN LAS DOS CONEXIONES, YA QUE
 SE HA AÑADIDO UNA LLAMADA A LA CONEXION BUENA EN ESTE ARCHIVO.
*/

include("_conexion.php");

//Se incluye el archivo que contiene las verificaciones de datos
include("../includes/verificaciones.php");

//Se incluye el archivo con los textos de las facturas, ya que los textos de los errores de las fechas se encuentran en ese archivo, asi como otros textos que se tienen en comun
//include($_SESSION["directorio_raiz"]."idiomas/".$_SESSION["idioma"]."/oficina_informacion_facturas.php");

//include($_SESSION["directorio_raiz"]."idiomas/".$_SESSION["idioma"]."/oficina_informacion_certificadofacturas.php");

include("../idiomas/".$_SESSION["idioma"]."/oficina_informacion_facturas.php");
include("../idiomas/".$_SESSION["idioma"]."/oficina_datos_titular.php");

include("../idiomas/".$_SESSION["idioma"]."/oficina_informacion_certificadofacturas.php");
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<title><?=$titulo_web;?></title>

<!--Se añaden las hojas de estilos genericas que utiliza esta seccion-->
<link href="../css/estilos.css" rel="stylesheet" type="text/css" />
<link href="../css/secciones.css" rel="stylesheet" type="text/css" />
<link href="css/oficina.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_vieja.css" rel="stylesheet" type="text/css" />

<link href="css/oficina_informacion_contratos_y_facturas.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_informacion_certificadoconsumos.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_informacion_certificadofacturas.css" rel="stylesheet" type="text/css" />


<link rel="stylesheet" type="text/css" media="all" href="scripts/calendar/skins/aqua/theme.css" title="Aqua" />


<!-- librería principal del calendario -->
<script type="text/javascript" src="scripts/calendar/calendar.js"></script>

<!-- librería para cargar el lenguaje deseado -->
<script type="text/javascript" src="scripts/calendar/lang/calendar-es.js"></script>

<!-- librería que declara la función Calendar.setup, que ayuda a generar un calendario en unas pocas líneas de código -->
<script type="text/javascript" src="scripts/calendar/calendar-setup.js"></script>

<script language=javascript type=text/javascript>
function stopRKey(evt) {
var evt = (evt) ? evt : ((event) ? event : null);
var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
if ((evt.keyCode == 13) && (node.type=="text")) {return false;}
}
document.onkeypress = stopRKey;
</script>



<!--Ahora se añaden las reglas para Internet Explorer. Asi corregiremos las diferencias de visualizacion entre navegadores
Es importante añadirlo despues de las reglas principales, para que se sobreescriban las correcciones pertinentes. En este caso las diferencias
solo son para internet explorer 7-->

<!--[if IE 7]>
	<link href="../css/estilos_ie7.css" rel="stylesheet" type="text/css" />
	<link href="../css/secciones_ie7.css" rel="stylesheet" type="text/css" />
<![endif]-->

<!--[if IE]>
	<link href="css/tablas_informacionIE.css" rel="stylesheet" type="text/css" />
<![endif]-->

<!--[if IE 7]>
	<link href="css/tablas_informacionIE7.css" rel="stylesheet" type="text/css" />
<![endif]-->




<?php
//Asignamos los valores por defecto a la fecha de inicio y la fecha de fin, estos cambiaran si el usuario pulsa el boton del formulario habiendo escrito algo
$ano=intval(substr(date("d-m-Y"),6,4));
$ano2=$ano-1;
$fecha_inicio_ver="01-01-".$ano2;
$fecha_fin_ver="31-12-".$ano2;
$fecha_inicio="dd-mm-aaaa";
$fecha_fin="dd-mm-aaaa";



//Si se pulsa el boton ver comprobaremos las fechas escritas por el usuario en el formulario
if(isset($_POST["Ver"]))
{
	$fecha_inicio_ver=$_POST["FechaInicial"];
	$fecha_fin_ver=$_POST["FechaFinal"];

	$error=comparar_fechas($fecha_inicio_ver,$fecha_fin_ver);
}//if(isset($_POST["Submit"]))

//Si se pulsa el boton Enviar del formulario inferior de la seccion comprobaremos las fechas escritas por el usuario en el formulario
if(isset($_POST["Submit"]))
{

	$fecha_inicio=$_POST["DesdeFecha"];
	$fecha_fin=$_POST["HastaFecha"];
	$suministroemail=$_POST["suministroemail"];
	$observaciones=$_POST["observaciones"];

	$error=comparar_fechas($fecha_inicio,$fecha_fin);
}//if(isset($_POST["Submit"]))

?>

</head>

<body >


<!--*************************************************Web realizada por ****************************************-->
	<div id="central_oficina">

<?php
//Se comprueba que el usuario se haya identificado, si no es asi se da un aviso y redirigimos a la seccion de identificacion
if ($_SESSION['usuario']=="")
{
	MsgBox($error_identificacion);
	redirigir("oficina_index.php?id=".$_SESSION["idioma"]);

}//if ($_SESSION['usuario'])

//Si el usuario se ha identificado la seccion se ejecutara con normalidad
else
{
//Añadimos los fragmentos comunes, la cabecera, el menu, y el submenu
	include("oficina_fragmentos_comunes.php");
?>
		<div class="contenido_seccion_oficina_informacion_certificado_consumos">
<!--VERSION VIEJA-->
            <table width="600" border="0" cellspacing="0" cellpadding="0">
		  <?php if (isset($_SESSION['numeroCliente'])){
					 		$result = mysql_query("SELECT * FROM DatosRegistrados WHERE CodigoCliente = ".$_SESSION['numeroCliente']." ");
							mysql_query("SET NAMES 'utf8'");
							$row = mysql_fetch_array($result);
					 }else{
					 		$result = mysql_query("SELECT * FROM DatosRegistrados WHERE CodigoCliente = -1");
							//Para que no tome ningun cliente pero no de ningun error en la pagina
							$row = mysql_fetch_array($result);
					 }
					 ?>


            <tr>
              <td valign="top"><table class="tabla_datos_certificado_consumos" border="0" cellspacing="0" cellpadding="0" align="center">


                <tr>

                  <td height="30" align="center" class="arialblack18Titulo">
                    <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 248'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>                  </td>
			    </tr>
				    <tr>
				   <td class="arial14">&nbsp;</td>
				    </tr>
              <tr>
                  <td class="arial14"><fieldset >
                    <legend class="arialblack14">
                    <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 130'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                    </legend>
                    <table width="tabla_datos_contratos_y_facturas" align="center">
                      <tr>
                        <td height="3">&nbsp;</td>
                      </tr>
                      <tr>
                        <td height="22" valign="top" class="arial14"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 196'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                          &nbsp;</td>
                        <td valign="top" class="arial14"><input name="CodigoCliente" type="text" class="textfieldLecturaNum" id="CodigoCliente" value="<?php echo($row['CodigoCliente']);?>" size="8" maxlength="5"  readonly="readonly" />
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                          <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 118'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                          &nbsp;
                          <input name="SuministroDNI"  type="text" class="textfieldLecturaNum" id="SuministroDNI" value="<?php echo($row['DNI']);?>" size="9" maxlength="15" readonly="readonly" />
                          &nbsp;</td>
                      </tr>
                      <tr>
                        <td height="22" class="arial14">CUPS&nbsp;</td>
                        <td valign="top" class="arial14"><input name="CUPSFijo" value="<?php echo($row['CUPS']);?>"  type="text" class="textfieldLectura" id="CUPSFijo" size="25" maxlength="25" /></td>
                      </tr>
                      <tr>
                        <td height="22" valign="top" class="arial14"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 119'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                          &nbsp;&nbsp;</td>
                        <td valign="top"><input name="SuministroNombre" type="text"  class="textfieldLectura" id="SuministroNombre" value="<?php echo(str_replace('"','&quot;',$row['TitularNombre']));?>" size="50" maxlength="50" readonly="readonly" /></td>
                      </tr>
                      <tr>
                        <td height="22" valign="top" class="arial14"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 123'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                          &nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td valign="top"><input name="SuministroCalle" type="text"  class="textfieldLectura" id="SuministroExtensionNombre3" value="<?php echo(str_replace('"','&quot;',$row['SuministroCalle']));?>" size="50" maxlength="50" readonly="readonly" /></td>
                      </tr>
                      <tr>
                        <td height="22" valign="top" class="arial14"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 124'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                          &nbsp;</td>
                        <td valign="top" class="arial14"><input readonly="readonly"  name="SuministroNumero" type="text" class="textfieldLecturaNum" id="SuministroNumero" value="<?php echo($row['SuministroNumero']);?>" size="5" maxlength="5" />
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                          <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 125'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                          &nbsp;
                        <input name="SuministroExtension" type="text" class="textfieldLectura" id="SuministroExtension" value="<?php echo(str_replace('"','&quot;',$row['SuministroExtension']));?>" size="20" maxlength="20" readonly="readonly" /></td>
                      </tr>
                      <tr>
                        <td height="22" valign="top" class="arial14"><?=$oficina_datos_titular21?></td>
                        <td valign="top"><input name="SuministroAclaradorFijo" type="text" class="textfieldLectura" value="<?php echo(str_replace('"','&quot;',$row['SuministroExtension2']));?>" size="50" maxlength="50" /></td>
                      </tr>
                      <tr>
                        <td height="24" valign="middle" class="arial14"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 122'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?></td>
                        <td valign="top"><input readonly="readonly"  name="SuministroCP2" type="text" class="textfieldLecturaNum" id="SuministroCP3" value="<?php echo($row['SuministroCP']);?>" size="5" maxlength="5" /></td>
                      </tr>


                      <tr>
                        <td height="21" valign="middle" class="arial14"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 121'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?></td>
                        <td valign="top"><input  name="SuministroExtension2" type="text" class="textfieldLectura" id="SuministroExtension3" value="<?php echo(str_replace('"','&quot;',$row['SuministroCiudad']));?>" size="50" maxlength="50" readonly="readonly" /></td>
                      </tr>
                    </table>
                  </fieldset></td>
                </tr>
                <tr align="center">
		          <td class="arialblack16">&nbsp;</td>
                  </tr>
		        <tr align="center">
		          <td align="center" class="arial12">
		            <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 249'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
		            <br />
		            <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 250'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>		            </td>


                  </tr>


		        <tr align="center">
		          <td class="arialblack16">&nbsp;</td>
                  </tr>

		        <tr align="center">


                  <td align="center" valign="middle" class="arial14">
					<form action="oficina_informacion_certificadofacturas.php?id=<?=$_SESSION["idioma"]?>" method="post" name="copias_certificados" id="copias_certificados">

                    <div >


		            <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 208'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
		            &nbsp;
		            <input name="FechaInicial"  id="sel1" value="<?=$fecha_inicio_ver?>"  type="text" class="textfield" size="10" maxlength="10">
		            &nbsp;
                    <input type="button" id="lanzador" value="..." />
<!-- script que define y configura el calendario-->
<script type="text/javascript">
Calendar.setup({
inputField : "sel1", // id del campo de texto
ifFormat : "%d-%m-%Y", // formato de la fecha que se escriba en el campo de texto
button : "lanzador" // el id del botón que lanzará el calendario
});
</script>



                    &nbsp;  &nbsp;  &nbsp;  &nbsp;
		            <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 209'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
		            &nbsp;
		            <input name="FechaFinal" id="sel2" value="<?=$fecha_fin_ver?>"type="text" class="textfield" size="10" maxlength="10">
		            &nbsp;



                        <input type="button" id="lanzador2" value="..." />
<!-- script que define y configura el calendario-->
<script type="text/javascript">
Calendar.setup({
inputField : "sel2", // id del campo de texto
ifFormat : "%d-%m-%Y", // formato de la fecha que se escriba en el campo de texto
button : "lanzador2" // el id del botón que lanzará el calendario
});
</script>
                     &nbsp;&nbsp; &nbsp;&nbsp;

		            <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 245'); $rowTexto = mysql_fetch_array($resultTexto);?>
		            &nbsp;
		            <input type="submit" name="Ver" value="<?php echo($rowTexto['Texto']);?>">
                   </div> </form></td>
                  </tr>
		        </table>
             </td>
		  </tr>
          </table>

          <table width="470" border="0" cellpadding="0" cellspacing="0" class="tablacertiffact">
		    <tr>
		      <td colspan="3" valign="top">
			  <br /><br />


			  <?php
//Si se ha pulsado el boton de ver del formulario, mostraremos los resultados de la consulta realizada
			 	if($error==0 and isset($_POST["Ver"]))
				{
					$fechaIni=fecha_mysql($fecha_inicio_ver);
					$fechaFin=fecha_mysql($fecha_fin_ver);

//LOLOLO
			   		$consulta_certificado_factura = "SELECT * FROM FacturasPDF WHERE NumeroCliente = ".$_SESSION['numeroCliente']." AND FechaFactura >= '".$fechaIni."' AND FechaFactura <= '".$fechaFin."' ORDER BY AnioPeriodo ASC,FechaFactura ASC";

					$resultado_certificado_factura = mysql_query($consulta_certificado_factura);

//Estas variables iran acumulando los valores de los dos tipos de consumos. Las utilizaremos para mostrar los totales
					$total_base=0;
					$total_IVA=0;
					$total_general=0;

								?>

                <?php
					if($registro_certificado_factura = mysql_fetch_array($resultado_certificado_factura))
					{
?>
										<div class="limpiar"></div>

 								<div class="estructura_consumo separacion_certificado_facturas">

                 				<div class="cabecera_periodo_certificado_factura_fecha color_fondo_cabecera_contrato times12Azul4"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 254'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?></div>

	                			<div class="cabecera_periodo_certificado_factura color_fondo_cabecera_contrato times12Azul4"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 252'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?></div>

                                <div class="cabecera_periodo_certificado_factura_periodo color_fondo_cabecera_contrato times12Azul4"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 253'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?></div>

                                   <div class="cabecera_periodo_certificado_factura color_fondo_cabecera_contrato times12Azul4"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 256'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?></div>

                                   <div class="cabecera_periodo_certificado_factura color_fondo_cabecera_contrato times12Azul4"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 257'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?></div>


                                <div class="cabecera_periodo_certificado_factura color_fondo_cabecera_contrato times12Azul4"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 258'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?></div>

                                	</div>


<?php
//Ahora iremos mostrando las facturas del periodo solicitado
							do
							{
								$total_base=$total_base+$registro_certificado_factura["ImporteBase"];
								$total_IVA=$total_IVA+$registro_certificado_factura["ImporteIVA"];
								$total_general=$total_general+$registro_certificado_factura["ImporteFactura"];
?>



                            <div class="limpiar"></div>


                                <div class="fondo_datos_certificado_factura_fecha color_fondo_datos_contrato arial12">
                                    <div class="posicion_datos_periodo_certificado_consumo"><?=fecha_normal($registro_certificado_factura["FechaFactura"])?></div>
                                 </div>

                                <div class="fondo_datos_certificado_factura color_fondo_datos_contrato arial12">
                                    <div class="posicion_datos_periodo_certificado_consumo"><?=$registro_certificado_factura["NumeroFactura"]?></div>
                                 </div>

                                <div class="fondo_datos_certificado_factura_periodo color_fondo_datos_contrato arial12">
                                     <div class="posicion_datos_periodo_certificado_consumo"><?=$registro_certificado_factura["PeriodoTexto"]?></div>
                                 </div>

                                 <div class="fondo_datos_certificado_factura color_fondo_datos_contrato arial12">
                                     <div class="posicion_datos_periodo_certificado_consumo">
                                       <div align="right"><?php echo(number_format($registro_certificado_factura["ImporteBase"],2,",","."));?> </div>
                                     </div>
                                 </div>


                                  <div class="fondo_datos_certificado_factura color_fondo_datos_contrato arial12">
                                     <div class="posicion_datos_periodo_certificado_consumo">
                                       <div align="right"><?php echo(number_format($registro_certificado_factura["ImporteIVA"],2,",","."));?> </div>
                                     </div>
                                 </div>

                                  <div class="fondo_datos_certificado_factura color_fondo_datos_contrato arial12">
                                     <div class="posicion_datos_periodo_certificado_consumo">
                                       <div align="right"><?php echo(number_format($registro_certificado_factura["ImporteFactura"],2,",","."));?> </div>
                                     </div>
                                 </div>


						<!--<div class="estructura_consumo">-->

<?php
						}while($registro_certificado_factura = mysql_fetch_array($resultado_certificado_factura));
?>
					<div class="limpiar"></div><br />


<!--Ahora añadimos dos filas mas, una con los totales de cada tipo de consumo y otra con el boton de imprimir el certificado-->
           			<div class="cabecera_totales_certificado_factura color_fondo_cabecera_contrato times12Azul4"><?=$oficina_informacion_certificadofacturas1?><?=": ".number_format($total_base,2,",",".")?> </div>

           			<div class="cabecera_totales_certificado_factura color_fondo_cabecera_contrato times12Azul4"><?=$oficina_informacion_certificadofacturas2?><?=": ".number_format($total_IVA,2,",",".")?> </div>

           			<div class="cabecera_importe_total_certificado_factura color_fondo_cabecera_contrato times12Azul4"><?=$oficina_informacion_certificadofacturas3?><?=": ".number_format($total_general,2,",",".")?> </div>

                     <div class="limpiar"></div>
                        <br />


          			<div class="cabecera_imprimir_certificado_consumo times12Azul4">
	                    <div class="posicion_boton_imprimir_certificado_consumo">
            	           	 <a href="imprimir_certificado_facturas.php?FechaInicial=<?=$fecha_inicio_ver.$ampersand?>FechaFinal=<?=$fecha_fin_ver.$ampersand?>" target="_blank"><img src="../img/oficina/icono_pdf.gif" alt="<?=$oficina_informacion_certificadofacturas4?>" title="<?=$oficina_informacion_certificadofacturas4?>" border="0" /></a>
                        </div><!--<div class="posicion_boton_imprimir_certificado_consumo">-->

						<div class="posicion_texto_imprimir_certificado_consumo">
                    		<a href="imprimir_certificado_facturas.php?FechaInicial=<?=$fecha_inicio_ver.$ampersand?>FechaFinal=<?=$fecha_fin_ver.$ampersand?>" target="_blank" class="enlace"><?=$oficina_informacion_certificadofacturas4?></a>
                        </div><!--<div class="posicion_boton_imprimir_certificado_consumo">-->
                     </div><!--<div class="cabecera_imprimir_certificado_consumo color_fondo_cabecera_contrato times12Azul4">-->

                     <div class="limpiar"></div>

<?php
				}//if($registro_certificado_factura = mysql_fetch_array($resultado_certificado_factura))

//Cuando no hay consumos en la tabla de certificado de consumos se muestra un mensaje al usuario
				else
				{
?>
					<div class="error_no_registros_consumos"><?=$error_no_facturas?></div>
<?php
                }//if($registro_certificado_factura = mysql_fetch_array($resultado_certificado_factura))
			}//if($error==0 and isset($_POST["Ver"]))
?>


            </td>
            </tr>

            <tr>
		      <td height="18"></td>
	      </tr>
          </table>









              	<table class="tabla_datos_certificado_consumos" width="560" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="560" class="arial12"> </td>
                </tr>
                <tr>
                  <td width="560" class="arial14"><form action="oficina_informacion_certificadofacturas.php?id=<?=$_SESSION["idioma"]?>" method="post" name="copias_certificados" id="copias_certificados"><fieldset>
                    <legend class="arialblack14">
                    <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 263'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                    </legend>

                    <table width="560" border="0">
                      <tr>
                        <td height="22" colspan="4" valign="top" class="arial12"><div align="justify">
                          <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 262'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?><br /><br />
                        </div></td>
                      </tr>

                      <tr>
                          <td width="122" height="24" align="right" valign="middle" class="arial14">
                            <div align="left">
                              <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 243'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                          *&nbsp;&nbsp;&nbsp;&nbsp;</div></td>
                      <td width="146" valign="middle"><input id="sel5" name="DesdeFecha"  type="text" class="textfield" value="<?=$fecha_inicio?>" size="10" maxlength="10" />


                                <input type="button" id="lanzador5" value="..." />
<!-- script que define y configura el calendario-->
<script type="text/javascript">
Calendar.setup({
inputField : "sel5", // id del campo de texto
ifFormat : "%d-%m-%Y", // formato de la fecha que se escriba en el campo de texto
button : "lanzador5" // el id del botón que lanzará el calendario
});
</script>





                      </td>
                      <td width="120" ><div align="left">
                        <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 244'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                        *&nbsp;</div></td>
                      <td width="154" valign="middle"><input id="sel6" name="HastaFecha" type="text" class="textfield" value="<?=$fecha_fin?>" size="10" maxlength="10" />
                        <input type="button" id="lanzador6" value="..." />
                        <!-- script que define y configura el calendario-->
                        <script type="text/javascript">
Calendar.setup({
inputField : "sel6", // id del campo de texto
ifFormat : "%d-%m-%Y", // formato de la fecha que se escriba en el campo de texto
button : "lanzador6" // el id del botón que lanzará el calendario
});
                        </script></td>
                      </tr>
                      <tr>
                          <td height="10" align="right" valign="middle" class="arial14">&nbsp;&nbsp;&nbsp;
                          </td>
                          <td colspan="3" valign="top"></td>
                      </tr>




                    <tr>
                     <td height="113" colspan="4" valign="middle">
                       <?php

					$result = mysql_query("SELECT * FROM DatosRegistrados WHERE CodigoCliente = ".$_SESSION['numeroCliente']." ");
					$row = mysql_fetch_array($result);
					$suministroemail=$row["SuministroEmail"];

					$resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 129'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>&nbsp;&nbsp;
                       <input name="suministroemail" value="<?=$suministroemail?>"  type="text" class="textfield" id="suministroemail" size="70" maxlength="70" /><br /><br />

                       <fieldset>
                         <legend class="arialblack14">

                           <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto =152'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                           </legend>
                         <table class="tabla_formulario_solicitud_certificado_consumo" align="center">
                           <tr> <td align="center" valign="top" class="arial12"><textarea name="observaciones" cols="70" rows="5" id="observaciones" class="arial14"></textarea></td>
                             </tr>
                           </table>
                         </fieldset>

                     </td>
                    </tr>

                    <tr>
					  <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 153'); $rowTexto = mysql_fetch_array($resultTexto);?>

                        <td height="26" colspan="4" align="center" valign="top" class="arial14"><input type="submit" name="Submit" value="<?php echo($rowTexto['Texto']);?>" />
  &nbsp;&nbsp;&nbsp;&nbsp; </td>
                      </tr>
                    </table>
                  </fieldset></form></td>
                </tr>
                <tr>
                  <td class="arial12">&nbsp;</td>
                </tr>
              </table>






















<!--VERSION VIEJA-->

		</div><!--<div class="contenido_seccion_oficina_informacion_certificado_consumos">-->


        <div class="limpiar"></div>

<!--Por ultimo aparecera el pie de la web-->
<?php
	include("../includes/pie.php");

//NOTA: EN ESTA OCASION, EL ERROR SE MOSTRARA DESPUES DE LA CARGA DE LA PAGINA, SI NO POR ALGUN MOTIVO EN OCASIONES DESPUES DE DAR EL ERROR CESA LA CARGA DE LA PAGINA
	if(isset($_POST["Ver"]) or isset($_POST["Submit"]))
	{
//Por ultimo mostraremos el error correspondiente, en el caso de que haya fallado alguna de las verificaciones, o continuaremos con el proceso
		switch($error)
		{
			case 0:
//Si no se ha producido ningun error en las verificaciones de las fechas, en el caso de haber pulsado el boton enviar se dara el alta en la tabla CertificadoConsumo
				if(isset($_POST["Submit"]))
				{

					$suministroemail=$_POST["suministroemail"];

					$insertar_solicitud_certificado_facturas="INSERT INTO `SolicitudCertificadoEconomico` (`NumeroCliente` ,`FechaInicial` ,`FechaFinal` ,`FechaRegistro` ,`HoraRegistro`,`Email`,`Observaciones`) VALUES (
'".$_SESSION['numeroCliente']."', '".fecha_mysql($fecha_inicio)."', '".fecha_mysql($fecha_fin)."', '".date("Y-m-d")."', '".date("Y-m-d H:i:s")."', '".$suministroemail."', '".$observaciones."');";

					$ejecutar_solicitud_certificado_facturas=mysql_query($insertar_solicitud_certificado_facturas);

					MsgBox($solicitud_ok);
					include("../includes/email_solicitud.php");
				}//if(isset($_POST["Submit"]))

			break;
//Error por no rellenar los campos obligatorios
			case 1:
				MsgBox($error_rellenar);
			break;

//Error por escribir la fecha incorrectamente
			case 2:
				MsgBox($error_fecha);
			break;

//Error por fechas escritas posterior a la actual
			case 3:
				MsgBox($error_fecha_posterior);
			break;

//Error por que la fecha de inicio es mayor que la fecha final
			case 4:
				MsgBox($error_fecha_inicio_mayor);
			break;

		}//switch($error)
	}//if(isset($_POST["Submit"]))

}//else ($_SESSION['usuario'])
?>
    </div><!--<div id="central_oficina"-->
</body>
</html>
