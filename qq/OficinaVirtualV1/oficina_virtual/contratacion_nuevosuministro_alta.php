<?php
/* NOTA:

 COMO NO SE CIERRA LA CONEXION TRAS CADA CONEXION CON LA BASE DE DATOS, LAS CONSULTAS DEJAN DE FUNCIONAR SI SE ESTABLECE COMO UNICA CONEXION
 LA QUE UTILIZA EL RESTO DE LA WEB, POR LO TANTO HASTA QUE SE HAGA LA MIGRACION COMPLETA DE LOS TEXTOS, COEXISTIRAN LAS DOS CONEXIONES, YA QUE
 SE HA AÑADIDO UNA LLAMADA A LA CONEXION BUENA EN ESTE ARCHIVO.
 
 2 - ESTA SECCION PERTENECE A LA PARTE DE LA WEB PUBLICA, POR ESO NO SE REQUIERE IDENTIFICACION PARA ACCEDER A ELLA. ESTA EN LA CARPETA DE OFICINA VIRTUAL POR NO TENER QUE HACER NUEVOS COMPONENTES Y CAMBIAR RUTAS.

*/

include("_conexion.php");

//Se incluye el archivo que contiene las verificaciones de datos
include("../includes/verificaciones.php");

//Se incluye el archivo con los textos especifico de esta seccion en el idioma oportuno
//include($_SESSION["directorio_raiz"]."idiomas/".$_SESSION["idioma"]."/oficina_contratacion_alta.php");

//LALALA Como el formulario es similar el de la seccion de datos del titular, añadimos su archivo de textos a la seccion, para no duplicarlos
//include($_SESSION["directorio_raiz"]."idiomas/".$_SESSION["idioma"]."/oficina_datos_titular.php");

//include($_SESSION["directorio_raiz"]."idiomas/".$_SESSION["idioma"]."/oficina_contratacion_alta.php");

include("../idiomas/".$_SESSION["idioma"]."/oficina_datos_titular.php");

include("../idiomas/".$_SESSION["idioma"]."/oficina_contratacion_alta.php");

//LOLOLO
//Esta variable controlara si la contratacion la esta haciendo un usuario sin registrar en la web (1) o un usuario registrado (0), se empleara para determinar el tipo de insert que hace el archivo "comprobaciones_inserccion_contratacion.php"
$_SESSION["contratacion_sin_registro"]=1;

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<title><?=$titulo_web;?></title>

<!--Se añaden las hojas de estilos genericas que utiliza esta seccion-->
<link href="../css/estilos.css" rel="stylesheet" type="text/css" />
<link href="../css/secciones.css" rel="stylesheet" type="text/css" />
<link href="css/oficina.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_vieja.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_contratacion_alta.css" rel="stylesheet" type="text/css" />
<link href="css/contratacion_nuevosuministro_alta.css" rel="stylesheet" type="text/css" />

<!--Ahora se añaden las reglas para Internet Explorer. Asi corregiremos las diferencias de visualizacion entre navegadores
Es importante añadirlo despues de las reglas principales, para que se sobreescriban las correcciones pertinentes. En este caso las diferencias
solo son para internet explorer 7-->

<!--[if IE 7]>
	<link href="../css/estilos_ie7.css" rel="stylesheet" type="text/css" />
	<link href="../css/secciones_ie7.css" rel="stylesheet" type="text/css" />    
<![endif]-->


<!--Añadimos el script que define el comportamiento del apartado Datos Tipo Suministo-->
<script src="scripts/comportamiento_datos_tipo_suministro.js" type="text/javascript"></script>

<!--Añadimos el script que define el comportamiento de las altas de contratacion, tanto de las de oficina virtual como las de los usuarios no registrados-->
<script src="scripts/comportamiento_altas_contratacion.js" type="text/javascript"></script>

<?php
//Las verificaciones de la contratacion son comunes a los usuarios identificados en la oficina virtual y los que no. Lo que sucede es que el el caso de los clientes nuevos existen 3 campos más (CUPS, comercializadora y distribuidora) que hay que verificar

if (isset($_POST["submit"]))
{
	$cups=$_POST["CUPS"];
	$comercializadora=$_POST["Comercializadora"];	
	$distribuidora=$_POST["Distribuidora"];		

//Inicializamos la variable que controla si se ha producido un error o no en los campos del apartado de identificacion	
	$error_identificacion=0;
	
//En primer lugar como todos estos campos son obligatorios comprobamos que se haya escrito en ellos	
	if($cups=="" or $comercializadora=="" or $distribuidora=="")
	{
		$error=1;
	}//if($cups=="" or $comercializadora=="" or $distribuidora=="")
		
//Si se ha escrito en los campos pasaremos a verificar el CUPS	
	else
	{
		if (comprobar_cups($cups)==1)
		{
			$error=9;
		}//if (comprobar_cups($cups))
		
//Si todo es correcto pasaremos ah realizar las mismas comprobaciones que en la solicitud desde la oficina virtual por un usuario ya registrado
		else
		{
			include("scripts/verificaciones_contratacion.php");		
		}//else (comprobar_cups($cups))
		
	}//else($cups=="" or $comercializadora=="" or $distribuidora=="")
			
}//if (isset($_POST["submit"]))
?>


</head>
	
<body>
<!--*************************************************Web realizada por Óscar Rodríguez Calero****************************************-->	
	<div id="central_oficina">    
<?php
//Capturaremos la variable que nos indica desde que enlace se ha llegado a la seccion. De esta manera mostraremos un titulo u otro y se producirán los demás cambios de comportamiento

//0->10 Kw/h
//1->10 a 15 Kw/h
//2-> >15 Kw/h

if($_GET["s"]!="")
{
	$_SESSION["seccion_origen_contratacion"]=$_GET["s"];
}//if($_GET["s"]!="")

?>                    
		<div>
<?php
			//include($_SESSION["directorio_raiz"]."includes/cabecera.php");
			include("../includes/cabecera.php");            
?>
		</div>                               

		<div class="posicion_menu">
<?php        
			//include($_SESSION["directorio_raiz"]."includes/menu.php");
			include("../includes/menu.php");
?>			            
		</div><!--<div class="posicion_menu">-->
           
		<div class="posicion_submenu">
<?php
			//include($_SESSION["directorio_raiz"]."includes/submenu.php");
			include("../includes/submenu.php");
?>
        </div> <!--<div class="posicion_menu">-->                 

		<div class="contenido_seccion_oficina_contratacion_alta_sin_registrar"> 
			<div class="titulo_seccion_oficina_contratacion_alta arialblack18Titulo">
<?php            
//Dependiendo desde que enlace se haya accedido mostraremos un titulo u otro.
//Además las tarifas a contratar que se muestran seran diferentes, se almacenaran en array_tarifas_disponibles
//También variaran las potencias normalizadas disponibles, se almacenaran los identificadores de las potencias a mostrar en array_tarifas en la segunda posicion

				switch($_SESSION["seccion_origen_contratacion"])	
				{
//10 kw/h				
					case 0:
						echo($oficina_contratacion_alta1);
						$array_tarifas_disponibles[0]=20;									
						$array_tarifas_disponibles[1]=21;															
					break;

//10-15 kw/h					
					case 1:
						echo($oficina_contratacion_alta2);																											
						$array_tarifas_disponibles[0]=210;									
						$array_tarifas_disponibles[1]=211;															
					break;
									
//>15 kw/h. EN ESTE CASO TAMBIEN SE ESTABLECERAN OTROS VALORES POR DEFECTO

					case 2:
						echo($oficina_contratacion_alta3);									
						$array_tarifas_disponibles[0]=30;									
						
/*NOTA: DE MOMENTO NO SE TIENEN EN CUENTA ESTAS TARIFAS						
						$array_tarifas_disponibles[1]=31;																																											
						$array_tarifas_disponibles[2]=61;
*/																		
					break;

//LOLOLO POSIBLE CAMBIO FUNCIOMIENTO BONOSOCIAL
					case 4:
						echo($oficina_contratacion_alta10);
						$array_tarifas_disponibles[0]=10;			
					break;
				}//switch($_SESSION["seccion_origen_contratacion"])	
?>                
            </div><!--<div class="titulo_seccion_oficina_contratacion_alta arialblack18Titulo">-->
            
            <div class="texto_seccion_oficina_contratacion_alta arial12">
				<?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 670'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
            </div>
            
			<div class="nota_campos_obligatorios"><?=$nota_campos_obligatorios?></div>
                     
<!--VERSION VIEJA-->          

			<form action="contratacion_nuevosuministro_alta.php?id=<?=$_SESSION["idioma"].$ampersand?>s=<?=$_SESSION["seccion_origen_contratacion"]?>" method="post" name="form_alta" id="form_alta" onsubmit="habilitar_campos();">

<?php
//Ahora añadimos los campos que diferencian el formulario al que accede un usuario nuevo y al que accede un usuario con la identificacion de la oficina virtual. Al ser usuario nuevo deberá de facilitar el CUPS, las empresas comercializadora y distribuidora a la que pertenece actualmente
?>

				<div class="posicion_campos_nuevo_cliente">
					<fieldset>
	    		        <legend class="arialblack14"><?=$oficina_contratacion_alta13?></legend>
                			
                            <div class="posicion_textos_formulario_alta arial14">
                            	<div class="texto_alta_formulario">CUPS*</div>
								<div class="texto_alta_formulario"><?=$oficina_contratacion_alta14?>*</div>
								<div class="texto_alta_formulario"><?=$oficina_contratacion_alta15?>*</div>
                            </div><!--<div class="texto_formulario_alta">-->
							  
                             <div class="posicion_campos_formulario_alta">
	                             <div class="campo_alta_formulario"><input name="CUPS" value="<?=$_POST["CUPS"]?>" type="text" class="textfield" id="CUPS" size="45" maxlength="22" /></div>
                                 
							  <div class="campo_alta_formulario"><input name="Comercializadora" value="<?=$_POST["Comercializadora"]?>" type="text" class="textfield" id="Comercializadora" size="45" maxlength="100" /></div>
                             
							 <div class="campo_alta_formulario"><input name="Distribuidora" value="<?=$_POST["Distribuidora"]?>" type="text" class="textfield" id="Distribuidora" size="45" maxlength="100" /></div>
							  </div><!--<div class="campos_formulario_alta">-->

							<div class="nota_identificacion_obligatoria"><?=$oficina_contratacion_alta16?></div                              
                            
					></fieldset>
		        </div><!--<div class="posicion_datos_suministro">-->          
            
<?php
				include("scripts/formulario_contratacion.php");
?>			
	      
			</form>
                                                                    
<!--VERSION VIEJA-->

            <div class="volver_seccion_oficina_contratacion_alta"><a href="contratacion_nuevosuministro.php?id=<?=$_SESSION["idioma"]?>" class="enlace"><?=$oficina_contratacion_alta6?></a></div>                    	
        	
		</div><!--<div class="contenido_seccion_oficina_contratacion_alta_sin_registrar">-->                        
		        
        <div class="limpiar"></div>
        
<!--Por ultimo aparecera el pie de la web-->
<?php 
//	include($_SESSION["directorio_raiz"]."includes/pie.php");
	include("../includes/pie.php");

	include("scripts/comprobaciones_inserccion_contratacion.php");	        
?>        
    </div><!--<div id="central_oficina"-->
</body>
</html>