<?php
/*
NOTAS:

1- ESTA SECCION TOMA LOS DATOS DE LA TABLA FacturasPDF

- ESCRIBE EN LA TABLA SolicitudFacturas

*/

/* NOTA:

 COMO NO SE CIERRA LA CONEXION TRAS CADA CONEXION CON LA BASE DE DATOS, LAS CONSULTAS DEJAN DE FUNCIONAR SI SE ESTABLECE COMO UNICA CONEXION
 LA QUE UTILIZA EL RESTO DE LA WEB, POR LO TANTO HASTA QUE SE HAGA LA MIGRACION COMPLETA DE LOS TEXTOS, COEXISTIRAN LAS DOS CONEXIONES, YA QUE
 SE HA AÑADIDO UNA LLAMADA A LA CONEXION BUENA EN ESTE ARCHIVO.
*/

include("_conexion.php");

//Se incluye el archivo que contiene las verificaciones de datos
include("../includes/verificaciones.php");

//Se incluye el archivo con los textos especifico de esta seccion en el idioma oportuno
//include($_SESSION["directorio_raiz"]."idiomas/".$_SESSION["idioma"]."/oficina_informacion_facturas.php");
include("../idiomas/".$_SESSION["idioma"]."/oficina_datos_titular.php");
include("../idiomas/".$_SESSION["idioma"]."/oficina_informacion_facturas.php");

include("../idiomas/".$_SESSION["idioma"]."/oficina_informacion_certificadofacturas.php");

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<title><?=$titulo_web;?></title>

<!--Se añaden las hojas de estilos genericas que utiliza esta seccion-->
<link href="../css/estilos.css" rel="stylesheet" type="text/css" />
<link href="../css/secciones.css" rel="stylesheet" type="text/css" />
<link href="css/oficina.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_vieja.css" rel="stylesheet" type="text/css" />

<link href="css/oficina_informacion_contratos_y_facturas.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_informacion_facturas.css" rel="stylesheet" type="text/css" />

<link href="css/oficina_informacion_contratos_y_facturas.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_informacion_certificadoconsumos.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_informacion_certificadofacturas.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" media="all" href="scripts/calendar/skins/aqua/theme.css" title="Aqua" />


<!-- librería principal del calendario -->
<script type="text/javascript" src="scripts/calendar/calendar.js"></script>

<!-- librería para cargar el lenguaje deseado -->
<script type="text/javascript" src="scripts/calendar/lang/calendar-es.js"></script>

<!-- librería que declara la función Calendar.setup, que ayuda a generar un calendario en unas pocas líneas de código -->
<script type="text/javascript" src="scripts/calendar/calendar-setup.js"></script>




<!--Ahora se añaden las reglas para Internet Explorer. Asi corregiremos las diferencias de visualizacion entre navegadores
Es importante añadirlo despues de las reglas principales, para que se sobreescriban las correcciones pertinentes. En este caso las diferencias
solo son para internet explorer 7-->

<!--[if IE 7]>
	<link href="../css/estilos_ie7.css" rel="stylesheet" type="text/css" />
	<link href="../css/secciones_ie7.css" rel="stylesheet" type="text/css" />    
<![endif]-->

<!--[if IE]>
	<link href="css/tablas_informacionIE.css" rel="stylesheet" type="text/css" />
<![endif]-->

<!--[if IE 7]>
	<link href="css/tablas_informacionIE7.css" rel="stylesheet" type="text/css" />
<![endif]-->

<script language="javascript" type="text/javascript">
function avisa()
{
	alert("OPCION NO DISPONIBLE");
}//function avisa()
//-->
</script>

<?php

//Asignamos los valores por defecto a la fecha de inicio y la fecha de fin, estos cambiaran si el usuario pulsa el boton del formulario habiendo escrito algo
$fecha_inicio="dd-mm-aaaa";
$fecha_fin="dd-mm-aaaa";

if(isset($_POST["Submit"]))
{	
	$fecha_inicio=$_POST["DesdeFecha"];
	$fecha_fin=$_POST["HastaFecha"];
	$observaciones=$_POST["observaciones"];	
	
	$error=comparar_fechas($fecha_inicio,$fecha_fin);		
}//if(isset($_POST["Submit"]))

?>

</head>
	
<body>
<!--*************************************************Web realizada por ****************************************-->
	<div id="central_oficina">
    
<?php
//Se comprueba que el usuario se haya identificado, si no es asi se da un aviso y redirigimos a la seccion de identificacion
if ($_SESSION['usuario']=="")
{	
	MsgBox($error_identificacion);
	redirigir("oficina_index.php?id=".$_SESSION["idioma"]);
	
}//if ($_SESSION['usuario'])

//Si el usuario se ha identificado la seccion se ejecutara con normalidad
else
{
//Añadimos los fragmentos comunes, la cabecera, el menu, y el submenu
	include("oficina_fragmentos_comunes.php");
?>
<div class="contenido_seccion_oficina_consultadeuda">                                         

        <table border="0" cellspacing="0" cellpadding="0">
                <tr>  
                  <td height="30" align="center" class="arialblack18Titulo"> Consulta de Deuda</td>
			    </tr> 
				    <tr> 
				   <td class="arial14">&nbsp;</td>
				    </tr> 
                 
                <!--<tr align="center"> 
                  <td align="center">
					<p>Este m&oacute;dulo le permite realizar el Pago de Facturas Pendientes por medio de una Tarjeta de Cr&eacute;dito, sin necesidada de desplazarse a nuestra Oficina o a cualquier Entidad Bancaria.<br />
					  <br />
				    Le detallamos el contenido de todas las facturas que est&aacute;n pendientes. Debe seleccionar aquellas que desee pagar y las facturas seleccionadas se reflejar&aacute;n en la parte inferior obteniendo el importe total que va a ser cargado en Tarjeta de Cr&eacute;dito.</p>
					<p>Si el pago se realiza porque ya le hemos efectuado un Corte de Energ&iacute;a en su domicilio, debe de pagar no solamente la factura que tiene asignado el Corte, sino la de Reconexi&oacute;n a que hace Referencia y todas las que correspondan a fechas anteriores. De no ser as&iacute; no podremos efectuarle la reposici&oacute;n de energ&iacute;a en su Punto de Suministro.</p></td>
                </tr>
                <tr align="center"> 
                  <td height="50" class="arialblack16">&nbsp;</td>
                </tr>
                  <tr align="center"> 
                  <td class="arialblack16Titulo">Relación de Facturas Pendientes de Pago</td>
                </tr>-->
                  <tr align="center">
                    <td class="arialblack16Titulo">&nbsp;</td>
                  </tr>
                  <tr align="center">
                    <td><span class="arial12"><span style="font-size:10px">Relaci&oacute;n de todas las facturas que tienen pendientes de pago en ese Punto de Suministro. Pueden ser canceladas accediendo al apartado de Medio Electr&oacute;nico de Pago</span></span></td>
                  </tr>
                
              </table>
<?php 
		   
		
			$importetotal=0;
			$consulta_medio_pago="SELECT * FROM RecibosPendientes WHERE  NumeroCliente=".$_SESSION['numeroCliente']." ORDER BY `FechaFactura` DESC";
			//echo($consulta_medio_pago);
			//echo("<br/>");
			$resultados_medio_pago = mysql_query($consulta_medio_pago);
			//echo($resultados_medio_pago);			
			//echo("<br/>");
			$num_medio_pago=mysql_num_rows($resultados_medio_pago);
			//echo($num_medio_pago);
			//echo("<br/>");
			//$registro_medio_pago = mysql_fetch_array($resultados_medio_pago);
			//echo($registro_medio_pago);
			//echo("<br/>");


//Si hay facturas se mostrara un resumen de los datos de cada una de ellas			
			if($num_medio_pago != 0)
			{
				
				while($registro_medio_pago = mysql_fetch_array($resultados_medio_pago))
				{
					
			//$consulta_medio_pago="SELECT * FROM FacturasPDF WHERE (NumeroCliente=".$_SESSION['numeroCliente']." AND FechaFactura = '".$registro_medio_pago["FechaFactura"]."')";
			//$resultados_medio_pago = mysql_query($consulta_medio_pago);			
			//$num_medio_pago=mysql_num_rows($resultados_medio_pago);
			//$registro_medio_pago = mysql_fetch_array($resultados_medio_pago);		
?>
                 <br />
                <div>
			  <div class="columnas_fila4_contrato">
                 <div class="color_fondo_cabecera_contrato times12Azul4">Nº Factura</div>
                 <div class="color_fondo_datos_contrato alto_factura_periodo arial12"><?=$registro_medio_pago["NumeroFactura"]?></div>                          
              </div><!--<div class="columnas_fila3_contrato">-->
			  <div class="columnas_fila4_contrato">
                   <div class="color_fondo_cabecera_contrato times12Azul4">Fecha	</div>
                   <div class="color_fondo_datos_contrato alto_factura_periodo arial12"><?=fecha_normal($registro_medio_pago["FechaFactura"])?></div>                                           
              </div><!--<div class="columnas_fila3_contrato">-->

			  <div class="columnas_fila4_contrato">
                  <div class="color_fondo_cabecera_contrato times12Azul4">N&ordm; Recibo	</div>
                  <div class="color_fondo_datos_contrato alto_factura_periodo arial12"><?=$registro_medio_pago["NumeroRecibo"]?></div>                                                
              </div><!--<div class="columnas_fila3_contrato">-->

			  <div class="columnas_fila4_contrato">
                  <div class="color_fondo_cabecera_contrato times12Azul4">Origen Fact	</div>
                  <div class="color_fondo_datos_contrato alto_factura_periodo arial12"><?php
						
					switch ($registro_medio_pago["TipoFactura"])
						{
						case 'E':						{$tipofactura="Energía";}						break;
						case 'R':						{$tipofactura="Reconexión";}					break;
						case 'D':						{$tipofactura="Derechos";}						break;
						case 'V':						{$tipofactura="Varios";}						break;
						default :						{$tipofactura="------";}      					break;
						}
						echo($tipofactura);
						
						
						?></div>
                  </div><!--<div class="columnas_fila3_contrato columna_factura_periodo">-->                    

			  <div class="columnas_fila4_contrato">
                    	<div class="color_fondo_cabecera_contrato times12Azul4">Tipo Recibo	</div>
                       	<div class="color_fondo_datos_contrato alto_factura_periodo arial12">
						
						<?php
						
						switch ($registro_medio_pago["TipoRecibo"])
						{
						case 'F':				{$tiporecibo="Facturado";}					break;
						case 'I':				{$tiporecibo="Impagado";}					break;
						default :				{$tiporecibo="------";}     				break;
						}
						echo($tiporecibo);
									
						?>
			  </div>                                                                     
                  </div><!--<div class="columnas_fila3_contrato">-->                    

			  <div class="columnas_fila4_contrato">
                    	<div class="color_fondo_cabecera_contrato times12Azul4">Est Impagado	</div>
                       	<div class="color_fondo_datos_contrato alto_factura_periodo arial12">
						
						<?php
						
						switch ($registro_medio_pago["EstadoImpagado"])
						{
						case '1':				{$tipoestadoimpagado="Pendiente";}					break;
						case 'C':				{$tipoestadoimpagado="Carta de Corte";}				break;
						case 'O':				{$tipoestadoimpagado="Cortado";}					break;
						default :				{$tipoestadoimpagado="------";}     				break;
						}
						echo($tipoestadoimpagado);
									
						?>
              </div>                                                
                  </div><!--<div class="columnas_fila3_contrato">-->                    

					<div class="columnas_fila5_contrato">
                    	<div class="color_fondo_cabecera_contrato times12Azul4">Importe</div>
                        <div class="color_fondo_datos_contrato alto_factura_periodo arial12"><?=$registro_medio_pago["ImportePendiente"]?><?php echo(" ");?></div>                                                                         
                    </div><!--<div class="columnas_fila3_contrato">-->                                        

		</div><!--<div class="columnas_fila1_larga_contrato">--> 

               <div class="limpieza"></div>
                    
            <div class="columnas_fila6_contrato">
                    	<div class="color_fondo_cabecera_contrato times12Azul4">Nombre Titular	</div>
                       	<div class="color_fondo_datos_contrato arial12">
						
						<?php echo($registro_medio_pago['NombreCliente']);?>
						
						</div>                                         
                </div><!--<div class="columnas_consumos_informacion_factura">-->
                    
		  <div class="columnas_fila6_contrato">
                    	<div class="color_fondo_cabecera_contrato times12Azul4">Direcci&oacute;n Punto Suministro</div>
                       	<div class="color_fondo_datos_contrato arial12">
						
						<?php echo($registro_medio_pago['DireccionCliente']);?>
                        
	</div>                                         
                </div><!--<div class="columnas_consumos_informacion_factura">-->

		  <div class="columnas_fila6_contrato">
                    	<div class="color_fondo_cabecera_contrato times12Azul4">Poblaci&oacute;n Punto Suministro</div>
                       	<div class="color_fondo_datos_contrato arial12">
						
					    <?php echo($registro_medio_pago['PoblacionCliente']);?>
                        
    	</div>                                         
                </div><!--<div class="columnas_consumos_informacion_factura">-->
                    
      <div class="limpieza"></div>
      <br />
    
	  <?php 
		$importetotal=$importetotal+$registro_medio_pago["ImportePendiente"];
		}//while($row = mysql_fetch_array($result))	
	 ?>
		<table width="642" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td width="284">&nbsp;</td>
                <td width="178" class="arial14b">Importe total a pagar </td>
                <td width="180"><div class="columnas_fila5_contrato">
                          
                <div class="color_fondo_cabecera_contrato times12Azul4">
                   <?php echo(number_format($importetotal,2,',','.'));?>
                   <? echo(" &euro;");?></div>
                 </div>
                </td>
             </tr>
         </table>
		 <?php		
		   } else {
		 ?>
               <br />
                  <fieldset style="text-align:center; border-radius:10px;">
                    <div class="error_no_registros">
                    No hay facturas impagadas referentes a este cliente en la base de datos 
                    </div>
                </fieldset>
                  <?php		   
		   }//else($num_facturas != 0)
		?>
               		</tr>                                                
            	</table>
	
		    </td>
          </tr>
        </table>
	   <br /><br />
      </div>
<!--<div class="contenido_seccion_oficina">-->                        
		
        
      <div class="limpiar"></div>
                        	
<!--Por ultimo aparecera el pie de la web-->
<?php 
	include("../includes/pie.php");

//NOTA: EN ESTA OCASION, EL ERROR SE MOSTRARA DESPUES DE LA CARGA DE LA PAGINA, SI NO POR ALGUN MOTIVO EN OCASIONES DESPUES DE DAR EL ERROR CESA LA CARGA DE LA PAGINA
	if(isset($_POST["Submit"]))
	{
//Por ultimo mostraremos el error correspondiente, en el caso de que haya fallado alguna de las verificaciones, o continuaremos con el proceso		
		switch($error)
		{		
//Proceso OK	
			case 0:						
//Antes de realizar el alta en la base de datos, deberemos de preparar los datos que no nos da el formulario
			
				$insertar_solicitud_factura="INSERT INTO `SolicitudFacturas` (`NumeroCliente` ,`FechaInicial` ,`FechaFinal` ,`FechaRegistro` ,
`HoraRegistro` ,`Observaciones`)VALUES ('".$_SESSION['numeroCliente']."', '".fecha_mysql($fecha_inicio)."', '".fecha_mysql($fecha_fin)."', '".date("Y-m-d")."', '".date("Y-m-d H:i:s")."', '".$observaciones."')";
								
				$ejecutar_solicitud_facturas=mysql_query($insertar_solicitud_factura);
															
				MsgBox($solicitud_ok);
														
			break;
				
//Error por no rellenar los campos obligatorios
			case 1:
				MsgBox($error_rellenar);
			break;								
			
//Error por escribir la fecha incorrectamente
			case 2:
				MsgBox($error_fecha);
			break;									
		
//Error por fechas escritas posterior a la actual
			case 3:
				MsgBox($error_fecha_posterior);
			break;									

//Error por que la fecha de inicio es mayor que la fecha final
			case 4:
				MsgBox($error_fecha_inicio_mayor);
			break;											
			
		}//switch($error)
	}//if(isset($_POST["Submit"]))	
}//else ($_SESSION['usuario'])
?>        
    </div><!--<div id="central_oficina"-->
</body>
</html>
