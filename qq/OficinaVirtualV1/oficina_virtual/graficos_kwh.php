<?php
session_start();

//Asignamos los valores por defecto a la fecha de inicio y la fecha de fin, estos cambiaran si el usuario pulsa el boton del formulario habiendo escrito algo
$ano = intval(substr(date("d-m-Y"),6,4));
$fecha_inicio_ver = "01-01-".$ano;
$fecha_fin_ver = "31-12-".$ano;
$fecha_inicio = "dd-mm-aaaa";
$fecha_fin = "dd-mm-aaaa";
$fechaInputIni = $ano."-01-01";
$fechaInputFin = $ano."-12-31";
include("_conexion.php");
include("../idiomas/".$_SESSION["idioma"]."/oficina_presentacion.php");

if (isset($_POST["Ver"])){
	include('datos/GetImportesFechas.php'); 
	include('datos/GetConsumosFechas.php'); 
} else {
	include('datos/GetImportes.php'); 
	include('datos/GetConsumos.php'); 
}

?>

<?php
//Se comprueba que el usuario se haya identificado, si no es asi se da un aviso y redirigimos a la seccion de identificacion
if ($_SESSION['usuario']=="")
{	
	MsgBox($error_identificacion);
	redirigir("graficos_index.php");
	
}
else
{
		//include("oficina_fragmentos_comunes.php");
?>     
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<style>

</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?=$titulo_web;?></title>

<!--Se a�aden las hojas de estilos genericas que utiliza esta seccion-->
<link href="../css/estilos.css" rel="stylesheet" type="text/css" />
<link href="../css/secciones.css" rel="stylesheet" type="text/css" />
<link href="../oficina_virtual/css/oficina.css" rel="stylesheet" type="text/css" />
<link href="../oficina_virtual/css/oficina_vieja.css" rel="stylesheet" type="text/css" />
<script src="external/jquery-1.8.0.min.js" type="text/javascript"></script>
<script src="external/jquery-ui-1.8.23.custom.min.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" media="all" href="scripts/calendar/skins/aqua/theme.css" title="Aqua" />
<script type="text/javascript" src="scripts/calendar/calendar.js"></script>

<!-- librer�a para cargar el lenguaje deseado -->
<script type="text/javascript" src="scripts/calendar/lang/calendar-es.js"></script>

<!-- librer�a que declara la funci�n Calendar.setup, que ayuda a generar un calendario en unas pocas l�neas de c�digo -->
<script type="text/javascript" src="scripts/calendar/calendar-setup.js"></script>

<script id="scriptInit" type="text/javascript">
		$(document).ready(function () {
			document.getElementById('inputConsumos');

			var column = 0;
			var line = 0;
			colorear('barras');
			
			$('#inputConsumos').click(function(){
				ocultargrafico('GraficoLine');
				vergrafico('GraficoColumn');
				colorear('barras');
				original('lineal');
			});
			
			
			$('#inputLineal').click(function(){
				ocultargrafico('GraficoColumn');
				vergrafico('GraficoLine');
				colorear('lineal');
				original('barras');
			});
			
		});
	</script>
    
     <script type="text/javascript" src="https://www.google.com/jsapi"></script>
      <script type="text/javascript">
	 $(function () {
	 <?php include('datos/GraficokwhColumn.php'); ?>
	 <?php //if($ie!=1){include('datos/GraficoConsumosLine.php');} else {  } ?>
	 });
	</script>
 <style>
	 input{height:inherit}
	 </style>
<!--[if IE 7]>
	<link href="../css/estilos_ie7.css" rel="stylesheet" type="text/css" />
	<link href="../css/secciones_ie7.css" rel="stylesheet" type="text/css" />    
<![endif]-->
<script>
         function vergrafico(tipo){
            document.getElementById(tipo).style.display = 'block';
         }
         function ocultargrafico(tipo){
            document.getElementById(tipo).style.display = 'none';
         }
          function colorear(tipo){
            document.getElementById(tipo).style.backgroundColor = '#ECC634';
         }
         function original(tipo){
            document.getElementById(tipo).style.backgroundColor = '#CDCDCD';
         }
		 
        </script>
        <script src="js/highcharts.js"></script>
        <script src="js/highcharts-3d.js"></script>
        <script src="js/modules/exporting.js"></script>
        <script src="js/modules/data.js"></script>
        <script src="js/modules/drilldown.js"></script>
</head>
	
<body>
	<div id="central_oficina">
    
<?php

//A�adimos los fragmentos comunes, la cabecera, el menu, y el submenu
		include("oficina_fragmentos_comunes.php");
?>            
       
<div class="contenido_seccion_oficina_datos">
    <div class="contenido_ajustado">
	<!--
	<div class="titulo" style="padding-top: 15px; margin-left: 480px; font-size: 20px; margin-bottom: -15px;">Gr&aacute;fico de Consumo/Coste<br /><br /></div>
	-->
    <div style="text-align:center">
    <?php // if($curvas==0){ ?>
    <div id="titulosForm" style="margin-bottom: 5px;"> <r>Fechas:</r><r style="margin-left:430px;">Tipo de gr&aacute;fico:</r>  </div>
    	<form action="graficos_kwh.php?id=<?=$_SESSION["idioma"]?>" method="post" name="copias_certificados" id="copias_certificados" style="margin-bottom:20px">
		   &nbsp; 
             <?php if($_POST['FechaInicial']==""){
			  $fechaInputIni = $fechaInputIni;
			  }else{
				 $fechaInputIni = $_POST['FechaInicial'];
				  };?>
                    <?php if($_POST['FechaFinal']==""){
			  $fechaInputFin = $fechaInputFin;
			  }else{
				 $fechaInputFin = $_POST['FechaFinal'];
				  };?>
		  <input name="FechaInicial"  id="sel1"  type="text" class="textfield" size="10" maxlength="10" onchange="comprobarFechas()" style="box-shadow: 0px 0px;border-radius: 4px 0px 0px 4px;margin-top: -5px;vertical-align: middle;  padding-left: 5px;  border: 1px solid #555;  height: 20px;" value="<?=$fechaInputIni?>"> 
		                
           <input type="button" id="lanzador" style="height: 24px;border: 1px #555 solid;  padding: 3px;box-shadow: 0px 0px;  margin-left: -6px;  border-radius: 0px 4px 4px 0px;" value="..." />
                <!-- script que define y configura el calendario-->
                <script type="text/javascript">
                Calendar.setup({
                inputField : "sel1", // id del campo de texto
                ifFormat : "%Y-%m-%d", // formato de la fecha que se escriba en el campo de texto
                button : "lanzador" // el id del bot�n que lanzar� el calendario
                });
                </script>
                            
                   
		   <input name="FechaFinal" id="sel2" type="text" class="textfield" size="10" maxlength="10" onchange="comprobarFechas()" style="box-shadow: 0px 0px;border-radius: 4px 0px 0px 4px;margin-top: -5px;vertical-align: middle;  padding-left: 5px;  border: 1px solid #555;  height: 20px;" value="<?=$fechaInputFin?>">
          <input type="button" id="lanzador2" style="height: 24px;border: 1px #555 solid;  padding: 3px;box-shadow: 0px 0px;  margin-left: -6px;  border-radius: 0px 4px 4px 0px;" value="..." />
<!-- script que define y configura el calendario-->
<script type="text/javascript">
Calendar.setup({
inputField : "sel2", // id del campo de texto
ifFormat : "%Y-%m-%d", // formato de la fecha que se escriba en el campo de texto
button : "lanzador2" // el id del bot�n que lanzar� el calendario
});
</script>
                    
		            <input type="submit" name="Ver" value="Mostrar" id="Ver" style="box-shadow: 0px 0px;background-color:#FFF;height: 24px;margin-right: 30px;">
       
           
   
	
    <div id="barras" style="width:30px;height:30px;float:right;background-color: #CDCDCD;border-radius: 5px 5px 5px 5px;border: 1px solid #999;margin-right: 135px;"><img src="img/barras.png" id="inputConsumos" title="Gr&aacute;fico de barras"  style="margin-top: 3px;" /></div>
    <!--<input type="checkbox" id="inputLineal" /> <strong style="text-align:center; text-decoration:none; text-transform:capitalize">Lineal</strong>
    <input type="checkbox" id="inputPie" style="visibility:hidden" /> <strong style="visibility:hidden; text-decoration:none; text-transform:capitalize">Sectores</strong>-->

                  </form>
                  
                  <div id="GraficoColumn" style="width: 710px;float: left; height: 450px; display:block;margin-left: 20px;"></div>
    <div id="GraficoLine" style="width:710px;float: left; height: 450px; display:none;margin-left: 20px;"></div> 
    <div id="Grafico3D" style="width:710px;float: left; height: 450px; display:none;margin-left: 20px;"></div> 
    <?php // } else { ?>
	<?php //} ?>
   
    <div class="limpiar"></div>
    
    
    
    </div>
</div>
                	
		</div><!--<div class="contenido_seccion_oficina">-->                        
		
        
        <div class="limpiar"></div>
                        	
<!--Por ultimo aparecera el pie de la web-->
<?php 
//	include($_SESSION["directorio_raiz"]."includes/pie.php");


}//else ($_SESSION['usuario'])
		
?>        
    
	<?php
    include("../includes/pie.php");
    ?>
    </div><!--<div id="central_oficina"-->
    </body>
</html>

