<?php
/*
NOTAS:

1- ESTA SECCION TOMA LOS DATOS DE LA TABLA CertificadoConsumo

2- ESCRIBE EN LA TABLA SolicitudCertificadoConsumo

*/

/* NOTA:

 COMO NO SE CIERRA LA CONEXION TRAS CADA CONEXION CON LA BASE DE DATOS, LAS CONSULTAS DEJAN DE FUNCIONAR SI SE ESTABLECE COMO UNICA CONEXION
 LA QUE UTILIZA EL RESTO DE LA WEB, POR LO TANTO HASTA QUE SE HAGA LA MIGRACION COMPLETA DE LOS TEXTOS, COEXISTIRAN LAS DOS CONEXIONES, YA QUE
 SE HA AÑADIDO UNA LLAMADA A LA CONEXION BUENA EN ESTE ARCHIVO.
*/

include("_conexion.php");

//Se incluye el archivo que contiene las verificaciones de datos
include("../includes/verificaciones.php");

//Se incluye el archivo con los textos de las facturas, ya que los textos de los errores de las fechas se encuentran en ese archivo, asi como otros textos que se tienen en comun
//include($_SESSION["directorio_raiz"]."idiomas/".$_SESSION["idioma"]."/oficina_informacion_facturas.php");

//include($_SESSION["directorio_raiz"]."idiomas/".$_SESSION["idioma"]."/oficina_informacion_certificadoconsumos.php");

include("../idiomas/".$_SESSION["idioma"]."/oficina_informacion_facturas.php");
include("../idiomas/".$_SESSION["idioma"]."/oficina_datos_titular.php");

include("../idiomas/".$_SESSION["idioma"]."/oficina_informacion_certificadoconsumos.php");
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<title><?=$titulo_web;?></title>

<!--Se añaden las hojas de estilos genericas que utiliza esta seccion-->
<link href="../css/estilos.css" rel="stylesheet" type="text/css" />
<link href="../css/secciones.css" rel="stylesheet" type="text/css" />
<link href="css/oficina.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_vieja.css" rel="stylesheet" type="text/css" />

<link href="css/oficina_informacion_contratos_y_facturas.css" rel="stylesheet" type="text/css" />

<link href="css/oficina_informacion_certificadoconsumos.css" rel="stylesheet" type="text/css" />

<link rel="stylesheet" type="text/css" media="all" href="scripts/calendar/skins/aqua/theme.css" title="Aqua" />

<script language=javascript type=text/javascript>
function stopRKey(evt) {
var evt = (evt) ? evt : ((event) ? event : null);
var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
if ((evt.keyCode == 13) && (node.type=="text")) {return false;}
}
document.onkeypress = stopRKey;
</script>
<!-- librería principal del calendario -->
<script type="text/javascript" src="scripts/calendar/calendar.js"></script>

<!-- librería para cargar el lenguaje deseado -->
<script type="text/javascript" src="scripts/calendar/lang/calendar-es.js"></script>

<!-- librería que declara la función Calendar.setup, que ayuda a generar un calendario en unas pocas líneas de código -->
<script type="text/javascript" src="scripts/calendar/calendar-setup.js"></script>



<?php
//Asignamos los valores por defecto a la fecha de inicio y la fecha de fin, estos cambiaran si el usuario pulsa el boton del formulario habiendo escrito algo
$ano=intval(substr(date("d-m-Y"),6,4));
$ano2=$ano-1;
$fecha_inicio_ver="01-01-".$ano2;
$fecha_fin_ver="31-12-".$ano2;
$fecha_inicio="dd-mm-aaaa";
$fecha_fin="dd-mm-aaaa";

//Si se pulsa el boton ver comprobaremos las fechas escritas por el usuario en el formulario
if(isset($_POST["Ver"]))
{
	$fecha_inicio_ver=$_POST["FechaInicial"];
	$fecha_fin_ver=$_POST["FechaFinal"];

	$error=comparar_fechas($fecha_inicio_ver,$fecha_fin_ver);
}//if(isset($_POST["Submit"]))

//Si se pulsa el boton Enviar del formulario inferior de la seccion comprobaremos las fechas escritas por el usuario en el formulario
if(isset($_POST["Submit"]))
{

	$fecha_inicio=$_POST["DesdeFecha"];
	$fecha_fin=$_POST["HastaFecha"];
	$observaciones=$_POST["observaciones"];
	$suministroemail=$_POST["suministroemail"];
	$error=comparar_fechas($fecha_inicio,$fecha_fin);
}//if(isset($_POST["Submit"]))

?>

</head>

<body>
<!--*************************************************Web realizada por ****************************************-->
	<div id="central_oficina">

<?php
//Se comprueba que el usuario se haya identificado, si no es asi se da un aviso y redirigimos a la seccion de identificacion
if ($_SESSION['usuario']=="")
{
	MsgBox($error_identificacion);
	redirigir("oficina_index.php?id=".$_SESSION["idioma"]);

}//if ($_SESSION['usuario'])

//Si el usuario se ha identificado la seccion se ejecutara con normalidad
else
{include("oficina_fragmentos_comunes.php");
?>

<div class="contenido_seccion_oficina_informacion_certificado_consumos">
<!--VERSION VIEJA-->
            <table width="650" border="0" cellspacing="0" cellpadding="0">
		  <?php if (isset($_SESSION['numeroCliente'])){
					 		$result = mysql_query("SELECT * FROM DatosRegistrados WHERE CodigoCliente = ".$_SESSION['numeroCliente']." ");
							mysql_query("SET NAMES 'utf8'");
							$row = mysql_fetch_array($result);
					 }else{
					 		$result = mysql_query("SELECT * FROM DatosRegistrados WHERE CodigoCliente = -1");
							//Para que no tome ningun cliente pero no de ningun error en la pagina
							$row = mysql_fetch_array($result);
					 }
					 ?>
            <tr>
              <td ><table class="tabla_datos_certificado_consumos" border="0" cellspacing="0" cellpadding="0">


                <tr>

                  <td height="30" align="center" class="arialblack18Titulo">
                    <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 205'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>                  </td>
			    </tr>
				    <tr>
				   <td class="arial14">&nbsp;</td>
				    </tr>
                <tr>
                  <td class="arial14"><fieldset >
                    <legend class="arialblack14">
                    <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 130'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                    </legend>
                    <table width="tabla_datos_contratos_y_facturas" align="center">
                      <tr>
                        <td height="3">&nbsp;</td>
                      </tr>
                      <tr>
                        <td height="22" valign="top" class="arial14"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 196'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                          &nbsp;</td>
                        <td valign="top" class="arial14"><input name="CodigoCliente" type="text" class="textfieldLecturaNum" id="CodigoCliente" value="<?php echo($row['CodigoCliente']);?>" size="8" maxlength="5"  readonly="readonly" />
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                          <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 118'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                          &nbsp;
                          <input name="SuministroDNI"  type="text" class="textfieldLecturaNum" id="SuministroDNI" value="<?php echo($row['DNI']);?>" size="9" maxlength="15" readonly="readonly" />
                          &nbsp;</td>
                      </tr>
                      <tr>
                        <td height="22" class="arial14">CUPS&nbsp;</td>
                        <td valign="top" class="arial14"><input name="CUPSFijo" value="<?php echo($row['CUPS']);?>"  type="text" class="textfieldLectura" id="CUPSFijo" size="25" maxlength="25" /></td>
                      </tr>
                      <tr>
                        <td height="22" valign="top" class="arial14"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 119'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                          &nbsp;&nbsp;</td>
                        <td valign="top"><input name="SuministroNombre" type="text"  class="textfieldLectura" id="SuministroNombre" value="<?php echo(str_replace('"','&quot;',$row['TitularNombre']));?>" size="50" maxlength="50" readonly="readonly" /></td>
                      </tr>
                      <tr>
                        <td height="22" valign="top" class="arial14"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 123'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                          &nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td valign="top"><input name="SuministroCalle" type="text"  class="textfieldLectura" id="SuministroExtensionNombre3" value="<?php echo(str_replace('"','&quot;',$row['SuministroCalle']));?>" size="50" maxlength="50" readonly="readonly" /></td>
                      </tr>
                      <tr>
                        <td height="22" valign="top" class="arial14"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 124'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                          &nbsp;</td>
                        <td valign="top" class="arial14"><input readonly="readonly"  name="SuministroNumero" type="text" class="textfieldLecturaNum" id="SuministroNumero" value="<?php echo($row['SuministroNumero']);?>" size="5" maxlength="5" />
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                          <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 125'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                          &nbsp;
                        <input name="SuministroExtension" type="text" class="textfieldLectura" id="SuministroExtension" value="<?php echo(str_replace('"','&quot;',$row['SuministroExtension']));?>" size="17" maxlength="20" readonly="readonly" /></td>
                      </tr>
                      <tr>
                        <td height="22" valign="top" class="arial14"><?=$oficina_datos_titular21?></td>
                        <td valign="top"><input name="SuministroAclaradorFijo" type="text" class="textfieldLectura" value="<?php echo(str_replace('"','&quot;',$row['SuministroExtension2']));?>" size="50" maxlength="50" /></td>
                      </tr>
                      <tr>
                        <td height="24" valign="middle" class="arial14"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 122'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?></td>
                        <td valign="top"><input readonly="readonly"  name="SuministroCP2" type="text" class="textfieldLecturaNum" id="SuministroCP3" value="<?php echo($row['SuministroCP']);?>" size="5" maxlength="5" /></td>
                      </tr>


                      <tr>
                        <td height="21" valign="middle" class="arial14"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 121'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?></td>
                        <td valign="top"><input  name="SuministroExtension2" type="text" class="textfieldLectura" id="SuministroExtension3" value="<?php echo(str_replace('"','&quot;',$row['SuministroCiudad']));?>" size="50" maxlength="50" readonly="readonly" /></td>
                      </tr>
                    </table>
                  </fieldset></td>
                </tr>
                <tr align="center">
		          <td class="arialblack16">&nbsp;</td>
                  </tr>
		        <tr align="center">
		          <td align="center" class="arial12">
		            <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 206'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
		            <br />
		            <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 207'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>		            </td>
                  </tr>


		        <tr align="center">
		          <td class="arialblack16">&nbsp;</td>
                  </tr>

		        <tr align="center">
		          <td align="center" valign="middle" class="arial14">
					<form action="oficina_informacion_certificadoconsumos.php?id=<?=$_SESSION["idioma"]?>" method="post" name="copias_certificados" id="copias_certificados">

		            <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 208'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
		            &nbsp;
		            <input name="FechaInicial"  id="sel1" value="<?=$fecha_inicio_ver?>"  type="text" class="textfield" size="10" maxlength="10">
		            &nbsp;

                       <input type="button" id="lanzador" value="..." />
<!-- script que define y configura el calendario-->
<script type="text/javascript">
Calendar.setup({
inputField : "sel1", // id del campo de texto
ifFormat : "%d-%m-%Y", // formato de la fecha que se escriba en el campo de texto
button : "lanzador" // el id del botón que lanzará el calendario
});
</script>

                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;


		            <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 209'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
		            &nbsp;
		            <input id="sel2" name="FechaFinal" value="<?=$fecha_fin_ver?>"type="text" class="textfield" size="10" maxlength="10">
		            &nbsp;

                          <input type="button" id="lanzador2" value="..." />
<!-- script que define y configura el calendario-->
<script type="text/javascript">
Calendar.setup({
inputField : "sel2", // id del campo de texto
ifFormat : "%d-%m-%Y", // formato de la fecha que se escriba en el campo de texto
button : "lanzador2" // el id del botón que lanzará el calendario
});
</script>
                    &nbsp;&nbsp;&nbsp;&nbsp;


		            <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 245'); $rowTexto = mysql_fetch_array($resultTexto);?>
		            &nbsp;
		            <input type="submit" name="Ver" value="<?php echo($rowTexto['Texto']);?>">
                    </td>
                  </tr>
		        </table>
            </td>
		  </tr>
		  <tr><td valign="top">

          <table class="tabla_datos_certificado_consumosb" width="622" border="0" cellpadding="0" cellspacing="0">
		    <tr>
		      <td width="622" colspan="3" valign="top">
			  <br /><br />

			  <?php
//Si se ha pulsado el boton de ver del formulario, mostraremos los resultados de la consulta realizada
			 	if($error==0 and isset($_POST["Ver"]))
				{
					$fechaIni=fecha_mysql($fecha_inicio_ver);
					$fechaFin=fecha_mysql($fecha_fin_ver);

			   		$consulta_consumo = "SELECT * FROM CertificadoConsumo WHERE NumeroCliente = ".$_SESSION['numeroCliente']." AND FechaFactura >= '".$fechaIni."' AND FechaFactura <= '".$fechaFin."' ORDER BY Anio ASC,Bimestre ASC,FechaFactura ASC";

					$resultado_consumos = mysql_query($consulta_consumo);

//Estas variables iran acumulando los valores de los dos tipos de consumos. Las utilizaremos para mostrar los totales
					$total_activa=0;
					$total_reactiva=0;

					if($registro_consumo = mysql_fetch_array($resultado_consumos))
					{
?>
						<div class="estructura_consumo3">

                			<div class="cabecera_periodo_certificado_consumo color_fondo_cabecera_contrato times12Azul4"><?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 253'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?></div>
                            <div class="fondo_cabecera_periodos_tarifarios_certificado_consumo_fecha color_fondo_cabecera_contrato times12Azul4">FechaFact.</div>
                			<div class="fondo_cabecera_periodos_tarifarios_certificado_consumo_tipo color_fondo_cabecera_contrato times12Azul4">Tipo</div>

   	                        <div class="fondo_cabecera_periodos_tarifarios_certificado_consumo color_fondo_cabecera_contrato times12Azul4">
                              <div align="center">
                                <?=$oficina_informacion_facturas2?>
                              </div>
                            </div>
   	                        <div class="fondo_cabecera_periodos_tarifarios_certificado_consumo color_fondo_cabecera_contrato times12Azul4">
   	                          <div align="center">
   	                            <?=$oficina_informacion_facturas3?>
                              </div>
   	                        </div>
   	                        <div class="fondo_cabecera_periodos_tarifarios_certificado_consumo color_fondo_cabecera_contrato times12Azul4">
   	                          <div align="center">
   	                            <?=$oficina_informacion_facturas4?>
                              </div>
   	                        </div>
   	                        <div class="fondo_cabecera_periodos_tarifarios_certificado_consumo color_fondo_cabecera_contrato times12Azul4">
   	                          <div align="center">P4</div>
   	                        </div>
   	                        <div class="fondo_cabecera_periodos_tarifarios_certificado_consumo color_fondo_cabecera_contrato times12Azul4">
   	                          <div align="center">P5</div>
   	                        </div>
   	                        <div class="fondo_cabecera_periodos_tarifarios_certificado_consumo color_fondo_cabecera_contrato times12Azul4">
   	                          <div align="center">P6
                              </div>
   	                        </div>
                            <div class="limpiar"></div>

<?php 						do
							{

								$total_activa=$total_activa+$registro_consumo["TotalActiva"];
								$total_reactiva=$total_reactiva+$registro_consumo["TotalReactiva"];;
 ?>
                                  <div class="fondo_datos_periodo_certificado_consumo color_fondo_datos_contrato arial12">
                                    <div class="posicion_datos_periodo_certificado_consumo"><?=$registro_consumo["PeriodoTexto"]?></div>

                                 </div>

                                  <div class="fondo_datos_periodo_certificado_consumo_fecha color_fondo_datos_contrato arial12">
                                    <div class="posicion_datos_periodo_certificado_consumo"><?=$registro_consumo["FechaFactura"]?></div>

                                 </div>

                                  <div class="fondo_datos_periodo_certificado_consumo_tipo color_fondo_datos_contrato arial12">
                                    <div class="posicion_datos_periodo_certificado_consumo"><?=$oficina_informacion_facturas7?></div>
                                    <div class="posicion_datos_periodo_certificado_consumo"><?=$oficina_informacion_facturas8?></div>
                                 </div>

                                <div class="datos_periodos_tarifarios_certificado_consumo color_fondo_datos_contrato arial12">
                                    <div class="posicion_datos_periodo_certificado_consumo">
									  <div align="right">
									    <? if (number_format($registro_consumo["P1Activa"],0,",",".")==0){echo("-");}else{echo(number_format($registro_consumo["P1Activa"],0,",","."));}?>
								      </div>
                                    </div>

                                    <div class="posicion_datos_periodo_certificado_consumo">
									  <div align="right">
									    <? if (number_format($registro_consumo["P1Reactiva"],0,",",".")==0){echo("-");}else{echo(number_format($registro_consumo["P1Reactiva"],0,",","."));}?>

								      </div>
                                  </div>
                                 </div>

                                <div class="datos_periodos_tarifarios_certificado_consumo color_fondo_datos_contrato arial12">
                                    <div class="posicion_datos_periodo_certificado_consumo">
									  <div align="right">
									    <? if (number_format($registro_consumo["P2Activa"],0,",",".")==0){echo("-");}else{echo(number_format($registro_consumo["P2Activa"],0,",","."));}?>
								      </div>
                                    </div>
                                    <div class="posicion_datos_periodo_certificado_consumo">
									  <div align="right">
									    <? if (number_format($registro_consumo["P2Reactiva"],0,",",".")==0){echo("-");}else{echo(number_format($registro_consumo["P2Reactiva"],0,",","."));}?>
								      </div>
                                    </div>
                                 </div>

                                <div class="datos_periodos_tarifarios_certificado_consumo color_fondo_datos_contrato arial12">
                                    <div class="posicion_datos_periodo_certificado_consumo">
									  <div align="right">
									    <? if (number_format($registro_consumo["P3Activa"],0,",",".")==0){echo("-");}else{echo(number_format($registro_consumo["P3Activa"],0,",","."));}?>
								      </div>
                                    </div>
                                    <div class="posicion_datos_periodo_certificado_consumo">
									  <div align="right">
									    <? if (number_format($registro_consumo["P3Reactiva"],0,",",".")==0){echo("-");}else{echo(number_format($registro_consumo["P3Reactiva"],0,",","."));}?>
								      </div>
                                    </div>
                                 </div>

                                <div class="datos_periodos_tarifarios_certificado_consumo color_fondo_datos_contrato arial12">
                                    <div class="posicion_datos_periodo_certificado_consumo">
									  <div align="right">
									    <? if (number_format($registro_consumo["P4Activa"],0,",",".")==0){echo("-");}else{echo(number_format($registro_consumo["P4Activa"],0,",","."));}?>
								      </div>
                                    </div>
                                    <div class="posicion_datos_periodo_certificado_consumo">
									  <div align="right">
									    <? if (number_format($registro_consumo["P4Reactiva"],0,",",".")==0){echo("-");}else{echo(number_format($registro_consumo["P4Reactiva"],0,",","."));}?>
								      </div>
                                    </div>
                                 </div>

                                <div class="datos_periodos_tarifarios_certificado_consumo color_fondo_datos_contrato arial12">
                                    <div class="posicion_datos_periodo_certificado_consumo">
									  <div align="right">
									    <? if (number_format($registro_consumo["P5Activa"],0,",",".")==0){echo("-");}else{echo(number_format($registro_consumo["P5Activa"],0,",","."));}?>
								      </div>
                                    </div>
                                    <div class="posicion_datos_periodo_certificado_consumo">
									  <div align="right">
									    <? if (number_format($registro_consumo["P5Reactiva"],0,",",".")==0){echo("-");}else{echo(number_format($registro_consumo["P5Reactiva"],0,",","."));}?>
								      </div>
                                    </div>
                                 </div>

                                <div class="datos_periodos_tarifarios_certificado_consumo color_fondo_datos_contrato arial12">
                                    <div class="posicion_datos_periodo_certificado_consumo">
									  <div align="right">
									    <? if (number_format($registro_consumo["P6Activa"],0,",",".")==0){echo("-");}else{echo(number_format($registro_consumo["P6Activa"],0,",","."));}?>
								      </div>
                                    </div>
                                    <div class="posicion_datos_periodo_certificado_consumo">
									  <div align="right">
									    <? if (number_format($registro_consumo["P6Reactiva"],0,",",".")==0){echo("-");}else{echo(number_format($registro_consumo["P6Reactiva"],0,",","."));}?>
								      </div>
                                    </div>
                                 </div>
                                 <div class="limpiar"></div>

<?php
						}while($registro_consumo = mysql_fetch_array($resultado_consumos));
?>
					<div class="limpiar"></div>

<!--Ahora añadimos dos filas mas, una con los totales de cada tipo de consumo y otra con el boton de imprimir el certificado-->
           			<div class="cabecera_totales_certificado_consumo color_fondo_cabecera_contrato times12Azul4">
                    	<div class="posicion_totales_certificado_consumo"><?=$oficina_informacion_certificadoconsumos2.": ".number_format($total_activa,0,",",".")?> kWh</div>
                     </div><!--<div class="cabecera_totales_certificado_consumo color_fondo_cabecera_contrato times12Azul4">-->

           			<div class="cabecera_totales_certificado_consumo color_fondo_cabecera_contrato times12Azul4">
						<div class="posicion_totales_certificado_consumo"><?=$oficina_informacion_certificadoconsumos3.": ".number_format($total_reactiva,0,",",".")?> kWh</div>
                    </div><!--<div class="cabecera_totales_certificado_consumo color_fondo_cabecera_contrato times12Azul4">-->

                     <div class="limpiar"></div>


          			<div class="cabecera_imprimir_certificado_consumo times12Azul4">
	                    <div class="posicion_boton_imprimir_certificado_consumo">
            	           	 <a href="imprimir_certificado_consumos.php?FechaInicial=<?=$fecha_inicio_ver.$ampersand?>FechaFinal=<?=$fecha_fin_ver.$ampersand?>" target="_blank"><img src="../img/oficina/icono_pdf.gif" alt="<?=$oficina_informacion_certificadoconsumos4?>" title="<?=$oficina_informacion_certificadoconsumos4?>" border="0" /></a>
                        </div><!--<div class="posicion_boton_imprimir_certificado_consumo">-->

						<div class="posicion_texto_imprimir_certificado_consumo">
                    		<a href="imprimir_certificado_consumos.php?FechaInicial=<?=$fecha_inicio_ver.$ampersand?>FechaFinal=<?=$fecha_fin_ver.$ampersand?>" target="_blank" class="enlace"><?=$oficina_informacion_certificadoconsumos4?></a>
                        </div><!--<div class="posicion_boton_imprimir_certificado_consumo">-->
                     </div><!--<div class="cabecera_imprimir_certificado_consumo color_fondo_cabecera_contrato times12Azul4">-->

                     <div class="limpiar"></div>

		           </div><!--<div class="estructura_consumo">-->

<?php
				}//if($registro_consumo = mysql_fetch_array($resultado_consumos))

//Cuando no hay consumos en la tabla de certificado de consumos se muestra un mensaje al usuario
				else
				{
?>
					<div class="error_no_registros_consumos"><?=$error_no_consumos?></div>
<?php
                }//if($registro_consumo = mysql_fetch_array($resultado_consumos))
			}//if($error==0 and isset($_POST["Ver"]))
?>
		    <tr>
		      <td height="18"></td>
	      </tr>




          <table class="tabla_datos_certificado_consumos" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td class="arial12"> </td>
                </tr>
                <tr>
                  <td class="arial14"><form action="oficina_informacion_certificadoconsumos.php?id=<?=$_SESSION["idioma"]?>" method="post" name="copias_certificados" id="copias_certificados"><fieldset>
                    <legend class="arialblack14">
                    <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 263'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                    </legend>

                    <table width="560" border="0">
                      <tr>
                        <td height="22" colspan="4" valign="top" class="arial12"><div align="justify">
                          <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 262'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?><br /><br />
                        </div></td>
                      </tr>

                      <tr>
                          <td width="122" height="24" align="right" valign="middle" class="arial14">
                            <div align="left">
                              <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 243'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                          *&nbsp;&nbsp;&nbsp;&nbsp;</div></td>
                      <td width="146" valign="middle"><input id="sel7" name="DesdeFecha"  type="text" class="textfield" value="<?=$fecha_inicio?>" size="10" maxlength="10" />


                                <input type="button" id="lanzador7" value="..." />
<!-- script que define y configura el calendario-->
<script type="text/javascript">
Calendar.setup({
inputField : "sel7", // id del campo de texto
ifFormat : "%d-%m-%Y", // formato de la fecha que se escriba en el campo de texto
button : "lanzador7" // el id del botón que lanzará el calendario
});
</script>





                      </td>
                      <td width="120" ><div align="left">
                        <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 244'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                        *&nbsp;</div></td>
                      <td width="154" valign="middle"><input id="sel8" name="HastaFecha" type="text" class="textfield" value="<?=$fecha_fin?>" size="10" maxlength="10" />
                        <input type="button" id="lanzador8" value="..." />
                        <!-- script que define y configura el calendario-->
                        <script type="text/javascript">
Calendar.setup({
inputField : "sel8", // id del campo de texto
ifFormat : "%d-%m-%Y", // formato de la fecha que se escriba en el campo de texto
button : "lanzador8" // el id del botón que lanzará el calendario
});
                        </script></td>
                      </tr>
                      <tr>
                          <td height="10" align="right" valign="middle" class="arial14">&nbsp;&nbsp;&nbsp;
                          </td>
                          <td colspan="3" valign="top"></td>
                      </tr>




                    <tr>
                     <td height="113" colspan="4" valign="middle">
                       <?php

					$result = mysql_query("SELECT * FROM DatosRegistrados WHERE CodigoCliente = ".$_SESSION['numeroCliente']." ");
					$row = mysql_fetch_array($result);
					$suministroemail=$row["SuministroEmail"];

					$resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 129'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>&nbsp;&nbsp;
                       <input name="suministroemail" value="<?=$suministroemail?>"  type="text" class="textfield" id="suministroemail" size="70" maxlength="70" /><br /><br />

                       <fieldset>
                         <legend class="arialblack14">

                           <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto =152'); $rowTexto = mysql_fetch_array($resultTexto); echo($rowTexto['Texto']);?>
                           </legend>
                         <table class="tabla_formulario_solicitud_certificado_consumo" align="center">
                           <tr> <td align="center" valign="top" class="arial12"><textarea name="observaciones" cols="70" rows="5" id="observaciones" class="arial14"></textarea></td>
                             </tr>
                           </table>
                         </fieldset>

                     </td>
                    </tr>

                    <tr>
					  <?php $resultTexto = mysql_query('SELECT * FROM '.$_SESSION['tablaIdioma'].' WHERE idTexto = 153'); $rowTexto = mysql_fetch_array($resultTexto);?>

                        <td height="26" colspan="4" align="center" valign="top" class="arial14"><input type="submit" name="Submit" value="<?php echo($rowTexto['Texto']);?>"/>
  &nbsp;&nbsp;&nbsp;&nbsp; </td>
                      </tr>
                    </table>
                  </fieldset></form></td>
                </tr>
                <tr>
                  <td class="arial12">&nbsp;</td>
                </tr>
              </table>
            </tr>
        </table>

<!--VERSION VIEJA-->

		</div><!--<div class="contenido_seccion_oficina_informacion_certificado_consumos">-->


<div class="limpiar"></div>

<!--Por ultimo aparecera el pie de la web-->
<?php
	include("../includes/pie.php");

//NOTA: EN ESTA OCASION, EL ERROR SE MOSTRARA DESPUES DE LA CARGA DE LA PAGINA, SI NO POR ALGUN MOTIVO EN OCASIONES DESPUES DE DAR EL ERROR CESA LA CARGA DE LA PAGINA
	if(isset($_POST["Ver"]) or isset($_POST["Submit"]))
	{
//Por ultimo mostraremos el error correspondiente, en el caso de que haya fallado alguna de las verificaciones, o continuaremos con el proceso
		switch($error)
		{
			case 0:
//Si no se ha producido ningun error en las verificaciones de las fechas, en el caso de haber pulsado el boton enviar se dara el alta en la tabla CertificadoConsumo
				if(isset($_POST["Submit"]))
				{

//LOLOLO
					$suministroemail=$_POST["suministroemail"];
					$insertar_solicitud_certificado_consumos="INSERT INTO `SolicitudCertificadoConsumo` (`NumeroCliente` ,`FechaInicial` ,`FechaFinal` ,`FechaRegistro` ,`HoraRegistro` ,`Email` ,`Observaciones`) VALUES (
'".$_SESSION['numeroCliente']."', '".fecha_mysql($fecha_inicio)."', '".fecha_mysql($fecha_fin)."', '".date("Y-m-d")."', '".date("Y-m-d H:i:s")."', '".$suministroemail."', '".$observaciones."');";

					$ejecutar_solicitud_certificado_consumos=mysql_query($insertar_solicitud_certificado_consumos);

					MsgBox($solicitud_ok);
					include("../includes/email_solicitud.php");
				}//if(isset($_POST["Submit"]))

			break;
//Error por no rellenar los campos obligatorios
			case 1:
				MsgBox($error_rellenar);
			break;

//Error por escribir la fecha incorrectamente
			case 2:
				MsgBox($error_fecha);
			break;

//Error por fechas escritas posterior a la actual
			case 3:
				MsgBox($error_fecha_posterior);
			break;

//Error por que la fecha de inicio es mayor que la fecha final
			case 4:
				MsgBox($error_fecha_inicio_mayor);
			break;

		}//switch($error)
	}//if(isset($_POST["Submit"]))

}//else ($_SESSION['usuario'])
?>
    </div><!--<div id="central_oficina"-->
</body>
</html>
