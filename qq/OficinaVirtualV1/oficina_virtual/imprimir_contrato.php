<?php
/*
NOTAS:
1- ESTA SECCION TOMA LOS DATOS DE LA TABLA ContratosPDF
2- LA MAQUETACION DE LOS DATOS SE HA TENIDO QUE HACER CON TABLAS, YA QUE CON CAPAS ASIGNABA VALORES DIFERENCTES EN LA WEB, EN LA IMPRESION EN PAPEL Y EN LA IMPRESION PDF.
*/

///Añadimos el archivo que contiene el inicio de la sesion y las variables de sesion.  Es importante añadir
//este archivo antes de las cabeceras del documento o dara error
include("../includes/sesion.php");

//Añadimos la libreria de conexion a base de datos, que a su vez contiene las referencias a todos los archivos necesarios
include("../includes/conexion.php");

//Se comprueba que el usuario se haya identificado, si no es asi se da un aviso y redirigimos a la seccion de identificacion
if ($_SESSION['usuario']=="")
{
	MsgBox($error_identificacion);
	redirigir("oficina_index.php?id=".$_SESSION["idioma"]);
}//if ($_SESSION['usuario'])

//Si el usuario se ha identificado la seccion se ejecutara con normalidad
else
{
//Se incluye el archivo con los textos especifico de esta seccion en el idioma oportuno
//include($_SESSION["directorio_raiz"]."idiomas/".$_SESSION["idioma"]."/imprimir_contrato.php");
	include("../idiomas/".$_SESSION["idioma"]."/imprimir_contrato.php");

//Recogemos los parametros que nos diran el contrato que se ha de leer. El contrato queda identificado con el numero del cliente y la fecha de alta
	$numero_cliente=$_SESSION['numeroCliente'];
	$fecha_alta=$_GET["fe"];

//Realizamos a consulta de los datos del titular
	$rs_cliente = seleccion_condicional ("DatosRegistrados", "CodigoCliente = '".$_SESSION['numeroCliente']."'","CodigoCliente",0,$ruta_sevidor_bd,$usuario_bd,$contrasena_bd,$nombre_bd);
	$registro_cliente= mysql_fetch_array($rs_cliente);

//Realizamos la consulta de los datos del contrato
	$rs_contrato = seleccion_condicional ("ContratosPDF", "NumeroCliente = '".$_SESSION['numeroCliente']."' AND FechaAlta='".$fecha_alta."'","FechaAlta",0,$ruta_sevidor_bd,$usuario_bd,$contrasena_bd,$nombre_bd);
	$registro_contrato = mysql_fetch_array($rs_contrato);

//Tambien consultamos los precios de las tarifas
	$rs_precios= seleccion_condicional ("TarifasDetalles", "Tarifa = '".$registro_cliente["TarifaContratadaAcceso"]."'","Tarifa",0,$ruta_sevidor_bd,$usuario_bd,$contrasena_bd,$nombre_bd);
	$registro_precios = mysql_fetch_array($rs_precios);

//Y los precios de la energia reactiva
	$rs_precios_reactiva= seleccion ("TarifasReactiva","PrecioReactiva1",0,$ruta_sevidor_bd,$usuario_bd,$contrasena_bd,$nombre_bd);
	$registro_precios_reactiva = mysql_fetch_array($rs_precios_reactiva);

/*
+++++++ COMPLEMENTOS +++++++
*/
$texto_complementos="";

$fecha_alta= explode("-",$registro_cliente["ComercializadoraFechaAlta"]);

$excepciones = seleccion_condicional ("TarifasMlExcepciones", "Cliente = ".(int)$registro_cliente["CodigoCliente"]." and Año=".(int)$fecha_alta[0]." and Mes = ".(int)$fecha_alta[1]." and TarifaML =".(int)$registro_cliente['TarifaContratadaML']."","Cliente",0,$ruta_sevidor_bd,$usuario_bd,$contrasena_bd,$nombre_bd);

//$excepciones = seleccion_condicional ("TarifasMlExcepciones", "Cliente = 1005112 and Año=2012 and Mes = 02 and TarifaML =23","Cliente",0,$ruta_sevidor_bd,$usuario_bd,$contrasena_bd,$nombre_bd);
$registro_excepciones= mysql_fetch_array($excepciones);

if ($registro_excepciones){
	if ($registro_excepciones["DescuentoPotencia"] != 0) {

		$texto_complementos.= $registro_conceptos["DescuentoPotencia"]."% ";
		$texto_complementos.= "Descuento en Potencia Facturada. <br>";
	} else {
		$texto_complementos.= "";
	}

	if ($registro_excepciones["DescuentoEnergia"] != 0) {
		$texto_complementos.= $registro_excepciones["DescuentoEnergia"]."% ";
		$texto_complementos.= "Descuento en Energia Facturada <br>";
	} else {
		$texto_complementos.= "";
	}

	if ($registro_excepciones["DtoFactura"] != "") {
		 echo "Descuento Factura: ".$registro_excepciones["DtoFactura"];
		if ($registro_excepciones["DtoFactura"] == "F"){
			$texto_complementos.= "Descuento aplicable en factura. <br>";
		} else {
			switch($registro_excepciones["DtoFactura"] ){
				case "T":
					$texto_complementos.= "Descuento a realizar en factura de abono trimestral. <br>";
					break;
				case "S":
					$texto_complementos.= "Descuento a realizar en factura de abono semestral. <br>";
					break;
				case "A":
					$texto_complementos.= "Descuento a realizar en factura de abono anual. <br>";
					break;
				default:
					$texto_complementos.= "Descuento a realizar en factura de abono. <br>";
					break;
			}
		}
	} else {
		$texto_complementos.= "";
	}

} else {


	$rs_conceptos = seleccion_condicional("tarifasmlconceptos", "Año=".(int)$fecha_alta[0]." and Mes = ".(int)$fecha_alta[1]." and TarifaML =".(int)$registro_cliente['TarifaContratadaML']."","Mes",0,$ruta_sevidor_bd,$usuario_bd,$contrasena_bd,$nombre_bd);
	$registro_conceptos = mysql_fetch_array($rs_conceptos);

	if ($registro_conceptos["DescuentoPotencia"] != 0) {
		$texto_complementos.= $registro_conceptos["DescuentoPotencia"]."% ";
		$texto_complementos.= "Descuento en Potencia Facturada <br>";
	} else {
		$texto_complementos.= "";
	}

	if ($registro_conceptos["DescuentoEnergia"] != 0) {
		$texto_complementos.= $registro_conceptos["DescuentoEnergia"]."% ";
		$texto_complementos.= "Descuento en Energia Facturada <br>";
	} else {
		$texto_complementos.= "";
	}

	if ($registro_conceptos["DtoFactura"] != "") {
		if ($registro_excepciones["DtoFactura"] == "F"){
			$texto_complementos.= "Descuento aplicable en factura. <br>";
		} else {
			switch($registro_excepciones["DtoFactura"] ){
				case "T":
					$texto_complementos.= "Descuento a realizar en factura de abono trimestral. <br>";
					break;
				case "S":
					$texto_complementos.= "Descuento a realizar en factura de abono semestral. <br>";
					break;
				case "A":
					$texto_complementos.= "Descuento a realizar en factura de abono anual. <br>";
					break;
				default:
					$texto_complementos.= "Descuento a realizar en factura de abono. <br>";
					break;
			}
		}
	} else {
		$texto_complementos.= "";
	}
}



/*
+++++++ FIN COMPLEMENTOS +++++++
*/

//Para mostrar por separado los distintos digitos de la cuenta bancaria los separamos
	$num_cuenta_banco=substr($registro_cliente["NumeroCuenta"],0,4);
	$num_cuenta_sucursal=substr($registro_cliente["NumeroCuenta"],4,4);
	$num_cuenta_digito_control=substr($registro_cliente["NumeroCuenta"],8,2);
	$num_cuenta_cuenta=substr($registro_cliente["NumeroCuenta"],10,10);

//Para mostrar por separado los distintos digitos del iban
	$num_iban1=substr($registro_cliente["Iban"],0,4);
	$num_iban2=substr($registro_cliente["Iban"],4,4);
	$num_iban3=substr($registro_cliente["Iban"],8,4);
	$num_iban4=substr($registro_cliente["Iban"],12,4);
	$num_iban5=substr($registro_cliente["Iban"],16,4);
	$num_iban6=substr($registro_cliente["Iban"],20,4);
	$num_iban7=substr($registro_cliente["Iban"],24,4);
//Tambien capturamos la fecha de alta de contratacion y la pasamos a "formato texto"

	$dia_actual=substr($registro_contrato["FechaAlta"],8,2);
	$mes_actual=substr($registro_contrato["FechaAlta"],5,2);
	$ano_actual=substr($registro_contrato["FechaAlta"],0,4);

//Si el primer carácter del dia o del mes es 0, lo suprimiremos
	if(substr($dia_actual,0,1)==0)
	{
		$dia_actual=str_replace("0","",$dia_actual);
	}//if(substr($dia_actual,0,1)==0)

	if(substr($mes_actual,0,1)==0)
	{
		$mes_actual=str_replace("0","",$mes_actual);
	}//if(substr($mes_actual,0,1)==0)

//Por último el hay que pasar el mes al nombre del mes en vez de formato numerico
	switch($mes_actual)
	{
		case 1:
			$mes_actual_texto=$enero;
		break;

		case 2:
			$mes_actual_texto=$febrero;
		break;

		case 3:
			$mes_actual_texto=$marzo;
		break;

		case 4:
			$mes_actual_texto=$abril;
		break;

		case 5:
			$mes_actual_texto=$mayo;
		break;

		case 6:
			$mes_actual_texto=$junio;
		break;

		case 7:
			$mes_actual_texto=$julio;
		break;

		case 8:
			$mes_actual_texto=$agosto;
		break;

		case 9:
			$mes_actual_texto=$septiembre;
		break;

		case 10:
			$mes_actual_texto=$octubre;
		break;

		case 11:
			$mes_actual_texto=$noviembre;
		break;

		case 12:
			$mes_actual_texto=$diciembre;
		break;
	}//switch($mes_actual)

?>

<STYLE>
H1.SaltoDePagina
{
PAGE-BREAK-AFTER: always
}
</STYLE>

<!--**************************************SECCION NUEVA INTEGRAMENTE DESARROLLADA POR OSCAR RODRIGUEZ CALERO**********************-->
<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
<!--<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' />-->
<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />
<title><?=$titulo_web;?></title>

<link href="../css/estilos.css" rel="stylesheet" type="text/css" />
<link href="../css/secciones.css" rel="stylesheet" type="text/css" />

<!--Añadimos los estilos especificos de esta seccion-->
<link href="css/imprimir_contrato.css" rel="stylesheet" type="text/css" />

<!--La visualizacion de la impresion del contrato es completamente distinta para IE, por lo tanto le asignaremos su propia hoja de estilos-->
<!--[if IE]>
	<link href="css/imprimir_contratoIE.css" rel="stylesheet" type="text/css" />
<![endif]-->

<script language="javascript" type="text/javascript">
//<!--
//Esta funcion hace que al cargar la pagina se habra la ventana de dialogo de impresion
function imprimir()
{
	window.print();
}//function dimensiona()

//-->
</script>
</head>

<body onLoad="javascript:imprimir()" style="background-image:none; background-color:#FFF">
    <div class="dimensiones_contrato">

    	<div class="margenes_impresion_contrato">

			<div><img src="../img/oficina/logo_datos.gif" alt="<?=$nombre_empresa_completo?>" title="<?=$nombre_empresa_completo?>" width="385" /></div>

    		<div class="titulo_impresion_contrato"><?=$titulo1?></div>
<!--*****************************************************************HOJA 1**************************************************************-->
<!--************************************************************************************************************************************-->

<!--*****************************************************************HOJA 1-TABLA1******************************************************-->
<!--************************************************************************************************************************************-->
		    <div class="titulo_apartado_impresion_contrato"><?=$datos_cliente?></div>

            <table border="1" cellpadding="0" cellspacing="0" class="tabla_impresion_contrato">
            	<tr>
                	<td class="celda_larga_contrato borde_contrato texto_contrato" colspan="2"><strong><?=$nombre_razon_social?>:</strong> <?=$registro_cliente["TitularNombre"]?></td>
                	<td class="celda_corta borde_contrato texto_contrato" colspan="2"><strong><?=$cif_nif?>:</strong> <?=$registro_cliente["DNI"]?></td>
                </tr>

              	<tr>
                	<td class="celda_larga borde_contrato texto_contrato" colspan="2"><strong><?=$direccion?>:</strong> <?=$registro_cliente["TitularCalle"]?></td>
                	<td class="celda_corta borde_contrato texto_contrato"><strong><?=$num?>:</strong> <?=$registro_cliente["TitularNumero"]?></td>

                </tr>

              	<tr>
                	<td class="celda_mitad borde_contrato texto_contrato"><strong><?=$cp_poblacion?>:</strong> <?=$registro_cliente["TitularCP"]."   ".$registro_cliente["TitularPoblacion"]?></td>
                	<td class="celda_mitad borde_contrato texto_contrato" colspan="2"><strong><?=$provincia_pais?>:</strong> <?=$registro_cliente["TitularProvincia"]."   ".$registro_cliente["TitularPais"]?></td>

                </tr>

              	<tr>

                	<td class="celda_tercio borde_contrato texto_contrato"><strong><?=$telefono?>:</strong> <?=$registro_cliente["TitularTelefono1"]?></td>
                	<td class="celda_tercio borde_contrato texto_contrato"><strong><?=$telefono?> 2:</strong> <?=$registro_cliente["TitularTelefono2"]?></td>
                	<td class="ultima_celda_tercio borde_contrato texto_contrato"><strong><?=$fax?>:</strong> <?=$registro_cliente["TitularFax"]?></td>

                </tr>

              	<tr>
                   	<td class="celda_completa borde_contrato texto_contrato" colspan="3"><strong><?=$email?>:</strong> <?=$registro_cliente["TitularEmail"]?></td>
                </tr>
            </table>

<!--*****************************************************************(FIN)HOJA 1-TABLA1*************************************************-->
<!--************************************************************************************************************************************-->

<!--*****************************************************************HOJA 1-TABLA2******************************************************-->
<!--************************************************************************************************************************************-->

         	<div class="titulo_apartado_impresion_contrato"><?=$datos_contacto?></div>

			<table border="1" cellpadding="0" cellspacing="0" class="tabla_impresion_contrato">
            	<tr>
                   	<td class="celda_completa borde_contrato texto_contrato" colspan="3"><strong><?=$persona_contacto?>:</strong> <?=$registro_cliente["FacturaNombre"]?></td>
                </tr>

              	<tr>
                	<td class="celda_larga borde_contrato texto_contrato" colspan="2"><strong><?=$direccion?>:</strong> <?=$registro_cliente["FacturaCalle"]?></td>
                	<td class="celda_corta borde_contrato texto_contrato"><strong><?=$num?>:</strong> <?=$registro_cliente["FacturaNumero"]?></td>

                </tr>

              	<tr>
                	<td class="celda_mitad borde_contrato texto_contrato"><strong><?=$cp_poblacion?>:</strong> <?=$registro_cliente["FacturaCP"]."   ".$registro_cliente["FacturaPoblacion"]?></td>
                	<td class="celda_mitad borde_contrato texto_contrato" colspan="2"><strong><?=$provincia_pais?>:</strong> <?=$registro_cliente["FacturaProvincia"]."   ".$registro_cliente["FacturaPais"]?></td>

                </tr>

              	<tr>

                	<td class="celda_tercio borde_contrato texto_contrato"><strong><?=$telefono?>:</strong> <?=$registro_cliente["FacturaTelefono1"]?></td>
                	<td class="celda_tercio borde_contrato texto_contrato"><strong><?=$telefono?> 2:</strong> <?=$registro_cliente["FacturaTelefono2"]?></td>
                	<td class="ultima_celda_tercio borde_contrato texto_contrato"><strong><?=$fax?>:</strong> <?=$registro_cliente["FacturaFax"]?></td>

                </tr>

              	<tr>
                   	<td class="celda_completa borde_contrato texto_contrato"  colspan="3"><strong><?=$email?>:</strong> <?=$registro_cliente["FacturaEmail"]?></td>
                </tr>
            </table>

<!--*****************************************************************(FIN)HOJA 1-TABLA2*************************************************-->
<!--************************************************************************************************************************************-->

    	<div class="titulo_impresion_contrato"><?=$titulo2?></div>

<!--*****************************************************************HOJA 1-TABLA3******************************************************-->
<!--************************************************************************************************************************************-->

		    <div class="titulo_apartado_impresion_contrato"><?=$datos_suministro?></div>

			<table border="1" cellpadding="0" cellspacing="0" class="tabla_impresion_contrato">
            	<tr>
                	<td class="celda_larga borde_contrato texto_contrato" colspan="3"><strong><?=$direccion?>:</strong> <?=$registro_cliente["SuministroCalle"]?></td>
                	<td class="celda_corta borde_contrato texto_contrato"><strong><?=$num?>:</strong> <?=$registro_cliente["SuministroNumero"]?></td>
                </tr>

              	<tr>
                	<td class="celda_mitad borde_contrato texto_contrato" colspan="2"><strong><?=$cp_poblacion?>:</strong> <?=$registro_cliente["SuministroCP"]."   ".$registro_cliente["SuministroCiudad"]?></td>
                	<td class="celda_mitad borde_contrato texto_contrato" colspan="2"><strong><?=$provincia_pais?>:</strong> <?=$registro_cliente["SuministroProvincia"]."   ".$registro_cliente["SuministroPais"]?></td>

                </tr>

              	<tr>
                	<td class="celda_cuarto_1 borde_contrato texto_contrato"><strong>CUPS:</strong> <?=$registro_cliente["CUPS"]?></td>
                	<td class="celda_cuarto_1 borde_contrato texto_contrato"><strong><?=$ref_catastral?>:</strong> <?=$registro_cliente["SuministroReferenciaCatastral"]?></td>
                	<td class="celda_cuarto_2 borde_contrato texto_contrato"><strong>CNAE:</strong> <?=$registro_cliente["SuministroCNAE"]?></td>
                    <td class="ultimo_celda_cuarto borde_contrato texto_contrato"><strong><?=$contrato_num?>:</strong> <?=$numero_cliente?></td>

                </tr>

                <tr>
                	<td class="celda_larga_contrato borde_contrato texto_contrato" colspan="2"><strong><?=$empresa_distribuidora?></strong> <?=$registro_cliente["EmpresaDistribuidora"]?></td>
                	<td class="celda_corta borde_contrato texto_contrato" colspan="2"><strong><?=$tension_nominal?></strong> <?=$registro_cliente["TipoTension"]?></td>
                </tr>

            </table>

<!--*****************************************************************(FIN)HOJA 1-TABLA3*************************************************-->
<!--************************************************************************************************************************************-->

<!--*****************************************************************HOJA 1-TABLA4******************************************************-->
<!--************************************************************************************************************************************-->

   		    <div class="titulo_apartado_impresion_contrato"><?=$tarifas_acceso?></div>

<!--LALALA Precio potencia o precio energia!!!!??-->
			<table border="1" cellpadding="0" cellspacing="0" class="tabla_impresion_contrato">

              	<tr>
                	<td class="celda_quinto borde_contrato texto_contrato"><strong><?=$tarifa?></strong> <?=$registro_contrato["Tarifa"]?></td>
                	<td class="celda_quinto_grande borde_contrato texto_contrato"><strong><?=$potencia_contratada?></strong> <?


					if( $registro_cliente["PotenciaContratada"]==0)  {echo("-");} else {echo($registro_cliente["PotenciaContratada"]);}?>
					</td>
                	<td class="celda_quinto borde_contrato texto_contrato"><strong><?=$punta?>:</strong> <?=$registro_precios["PotenciaP1"]?></td>
                    <td class="celda_quinto borde_contrato texto_contrato"><strong><?=$llana?>:</strong> <?
                    if( $registro_precios["PotenciaP2"]==0)  {echo("-");} else {echo($registro_precios["PotenciaP2"]);}
					?></td>
                    <td class="celda_quinto borde_contrato texto_contrato"><strong><?=$valle?>:</strong> <?
                    if( $registro_precios["PotenciaP3"]==0)  {echo("-");} else {echo($registro_precios["PotenciaP3"]);} ?>
                    </td>
                </tr>

            </table>

<!--*****************************************************************(FIN)HOJA 1-TABLA4*************************************************-->
<!--************************************************************************************************************************************-->

<!--*****************************************************************HOJA 1-TABLA5******************************************************-->
<!--************************************************************************************************************************************-->
  		    <div class="titulo_apartado_impresion_contrato"><?=$producto_contratado?></div>

            <table border="1" cellpadding="0" cellspacing="0" class="tabla_impresion_contrato">

              	<tr>
                	<td class="celda_completa borde_contrato texto_contrato"><strong><?=$producto?></strong> <?=$registro_cliente["ComercializadoraProducto"]?></td>
                </tr>

            </table>

<!--*****************************************************************(FIN)HOJA 1-TABLA5*************************************************-->
<!--************************************************************************************************************************************-->

<!--*****************************************************************HOJA 1-TABLA6******************************************************-->
<!--************************************************************************************************************************************-->

  		    <div class="titulo_apartado_impresion_contrato"><?=$datos_pagador?></div>


			<table border="1" cellpadding="0" cellspacing="0" class="tabla_impresion_contrato">

              	<tr>
                	<td class="celda_larga borde_contrato texto_contrato" colspan="3"><strong><?=$titular_cuenta?></strong> <?=$registro_cliente["PagadorNombre"]?></td>
                	<td class="celda_corta borde_contrato texto_contrato"><strong><?=$nif?>:</strong> <?=$registro_cliente["PagadorDNI"]?></td>

                </tr>

              <tr>
                	<td class="celda_mitad borde_contrato texto_contrato" colspan="2"><strong>Domiciliaci&oacute;n Bancaria:</strong> <?=$num_cuenta_banco."-".$num_cuenta_sucursal."-".$num_cuenta_digito_control."-".$num_cuenta_cuenta?></td>
                	<td class="celda_mitad borde_contrato texto_contrato" colspan="2"><strong>IBAN: </strong> <?=$num_iban1." ".$num_iban2." ".$num_iban3." ".$num_iban4." ".$num_iban5." ".$num_iban6." ".$num_iban7?></td>

                </tr>

            </table>

<!--*****************************************************************(FIN)HOJA 1-TABLA5*************************************************-->
<!--************************************************************************************************************************************-->

            <div class="texto_impresion_contrato">
            	<!--<?=$explicacion_contrato1?><?=$nombre_empresa_completo?><?=$explicacion_contrato2?>
				<?=$nombre_empresa_completo?><?=$explicacion_contrato3?>-->
                <strong>AUTORIZACION A LA DISTRIBUIDORA</strong><br>
                Autorizo expresamente a <?=$nombre_empresa_completo?> para que act&uacute;e como sustituto de esta sociedad a la hora de contratar el acceso a las redes con la empresa distribuidora, de tal forma que la posici&oacute;n de <?=$nombre_empresa_completo?> en el contrato de acceso suscrito sea, a todos los efectos, la de esta Sociedad a la que represento.<br>
                Y para que quede constancia de ello, firmo el presente escrito.
                <br><br>
                <strong>AUTORIZACION DE DOMICILIACION BANCARIA:</strong><br>
                Por la presente autorizo a <?=$nombre_empresa_completo?> para que procedan a efectuar el cargo de sus facturas correspondientes al suministro de Energ&iacute;a El&eacute;ctrica, en la domiciliacion bancaria arriba indicada.

            </div><!--<div class="texto_impresion_contrato">-->

            <div class="fecha_contratacion_impresion_contrato"><?php
  // $rowTexto = //mysql_fetch_array($resultTexto);$rowTexto[1]);
  $Monate = array('Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');

  //$Monate = array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
  $mes = $Monate[date("m")-1];


  echo("<br />");
  echo($poblacion_empresa.", ");
  echo(date("d "));
  echo("de ");
  echo($mes);
  echo(" del ");
  echo(date("Y"));
   echo(".");
    ?></div>

            <div class="firma_cliente_impresion_contrato"><?=$el_cliente?></div>
            <div class="firma_empresa_impresion_contrato"><?=$nombre_empresa?></div>
			<div class="limpiar"></div>
            <div class="fdo_cliente_impresion_contrato">
				<?=$fdo?>. <?=$registro_cliente["TitularNombre"]?>
            </div>
            <div class="fdo_empresa_impresion_contrato">
				&nbsp;&nbsp;&nbsp;<img src="../img/oficina/firma.jpg" height="90"><br />
				<?=$fdo?>. <?=$nombre_empresa?>
            </div>
			<div class="limpiar"></div>

        </div><!--<div class="margenes_impresion_contrato">-->
     </div><!--<div class="dimensiones_contrato">-->

<!--*****************************************************************(FIN)HOJA 1********************************************************-->
<!--************************************************************************************************************************************-->
<!--*****************************************************************HOJA 2**************************************************************-->
<!--************************************************************************************************************************************-->
	<div class="dimensiones_contrato">
    	<div class="margenes_impresion_contrato">
    		<div><img src="../img/oficina/logo_datos.gif" alt="<?=$nombre_empresa_completo?>" title="<?=$nombre_empresa_completo?>" width="427" /></div>

	    	<div class="titulo_impresion_contrato"><?=$anexo_precios?></div>
<!--*****************************************************************HOJA 2-TABLA1******************************************************-->
<!--************************************************************************************************************************************-->
		    <div class="titulo_apartado_impresion_contrato" style="margin-top:10px;"><?=$datos_cliente?></div>

			<table border="1" cellpadding="0" cellspacing="0" class="tabla_impresion_contrato"  style="width:98%">
              	<tr>
                	<td class="celda_larga_contrato borde_contrato texto_contrato"><strong><?=$nombre_razon_social?>:</strong> <?=$registro_cliente["TitularNombre"]?></td>
                	<td class="celda_corta borde_contrato texto_contrato"><strong><?=$cif_nif?>:</strong> <?=$registro_cliente["DNI"]?></td>
                </tr>
			</table>

<!--*****************************************************************(FIN)HOJA 2-TABLA1*************************************************-->
<!--************************************************************************************************************************************-->

<!--*****************************************************************HOJA 2-TABLA2******************************************************-->
<!--************************************************************************************************************************************-->

          	<div class="titulo_apartado_impresion_contrato" style="margin-top:10px;"><?=$datos_suministro_sin_omitir?></div>
			<table border="1" cellpadding="0" cellspacing="0" class="tabla_impresion_contrato">
              	<tr>
                	<td class="celda_larga_contrato borde_contrato texto_contrato" colspan="2"><strong><?=$empresa_distribuidora?></strong> <?=$registro_cliente["EmpresaDistribuidora"]?></td>
                	<td class="celda_corta borde_contrato texto_contrato" colspan="2"><strong><?=$tension_nominal?></strong> <?=$registro_cliente["TipoTension"]?></td>
                </tr>
              	<tr>
                	<td class="celda_cuarto_1 borde_contrato texto_contrato"><strong>CUPS:</strong> <?=$registro_cliente["CUPS"]?></td>
                	<td class="celda_cuarto_1 borde_contrato texto_contrato"><strong><?=$ref_catastral?>:</strong> <?=$registro_cliente["SuministroReferenciaCatastral"]?></td>
                	<td class="celda_cuarto_2 borde_contrato texto_contrato"><strong>CNAE:</strong> <?=$registro_cliente["SuministroCNAE"]?></td>
                	<td class="ultimo_celda_cuarto borde_contrato texto_contrato"><strong><?=$contrato_num?>:</strong> <?=$numero_cliente?></td>
                </tr>
			</table>

<!--*****************************************************************(FIN)HOJA 2-TABLA2*************************************************-->
<!--************************************************************************************************************************************-->

<!--*****************************************************************HOJA 2-TABLA3******************************************************-->
<!--************************************************************************************************************************************-->
            <div class="titulo_pequeno_anexo_precios"><?=$termino_potencia?></div>



                <table border="1" cellpadding="0" cellspacing="0" class="tabla_impresion_contrato" style="width:98%">
                    <tr>
                        <td class="celda_cuarto_larga_anexo_precios borde_contrato texto_contrato"><br /></td>
                        <td class="celda_cuarto_anexo_precios borde_contrato texto_contrato" style="width:55px"><strong><?=$punta?></strong></td>
                        <td class="celda_cuarto_anexo_precios borde_contrato texto_contrato" style="width:55px"><strong><?=$llana?></strong></td>
                        <td class="celda_cuarto_anexo_precios borde_contrato texto_contrato" style="width:55px"><strong><?=$valle?></strong></td>
                      <td class="celda_cuarto_anexo_precios borde_contrato texto_contrato" style="width:25px"><strong>P4</strong></td>
                      <td class="celda_cuarto_anexo_precios borde_contrato texto_contrato" style="width:25px"><strong>P5</strong></td>
                      <td class="celda_cuarto_anexo_precios borde_contrato texto_contrato" style="width:25px"><strong>P6</strong></td>
                    </tr>

                    <tr>
                        <td class="celda_cuarto_larga_anexo_precios borde_contrato texto_contrato">
                        <strong><?=$potencia_contratada?></strong></td>
                        <td class="celda_cuarto_anexo_precios borde_contrato texto_contrato" style="width:55px">
                        <?php
                        if ($registro_contrato["Tarifa"] != '2.0A' and $registro_contrato["Tarifa"] != '2.1A') {?>
						<?=$registro_cliente["PotenciaContratada"]?>
                        <?php } ?>
                        </td>
                        <td class="celda_cuarto_anexo_precios borde_contrato texto_contrato" style="width:55px">
						<?php
                        if ($registro_contrato["Tarifa"] == '2.0A' or $registro_contrato["Tarifa"] == '2.1A'
						or $registro_contrato["Tarifa"] == '3.0A' or $registro_contrato["Tarifa"] == '3.1A') {?>
						<?=$registro_cliente["PotenciaContratada"]?>
                        <?php } ?></td>
                        <td class="celda_cuarto_anexo_precios borde_contrato texto_contrato" style="width:55px" >
						<?php
                        if ($registro_contrato["Tarifa"] == '2.0DHA' or $registro_contrato["Tarifa"] == '2.1DHA'
						or $registro_contrato["Tarifa"] == '3.0A' or $registro_contrato["Tarifa"] == '3.1A') {?>
						<?=$registro_cliente["PotenciaContratada"]?>
                        <?php } ?>
						</td>
                        <td class="celda_cuarto_anexo_precios borde_contrato texto_contrato"  style="width:25px">
						<?php
                        if ($registro_contrato["Tarifa"] == '3.1A' or $registro_contrato["Tarifa"] == '3.1') {?>
						<?=$registro_cliente["PotenciaContratada"]?>
                        <?php } else { echo '';} ?>
						</td>
                        <td class="celda_cuarto_anexo_precios borde_contrato texto_contrato"  style="width:25px">
						<?php
                        if ($registro_contrato["Tarifa"] == '3.1A' or $registro_contrato["Tarifa"] == '3.1') {?>
						<?=$registro_cliente["PotenciaContratada"]?>
                        <?php } else { echo '';} ?>
						</td>
                        <td class="celda_cuarto_anexo_precios borde_contrato texto_contrato"  style="width:25px">
						<?php
                        if ($registro_contrato["Tarifa"] == '3.1A' or $registro_contrato["Tarifa"] == '3.1') {?>
						<?=$registro_cliente["PotenciaContratada"]?>
                        <?php } else { echo '';} ?>
						</td>
                    </tr>

                    <tr>
                        <td class="celda_cuarto_larga_anexo_precios borde_contrato texto_contrato"><strong><?=$precio_por_periodo?>
                      </strong></td>
                        <td class="celda_cuarto_anexo_precios borde_contrato texto_contrato" style="width:55px">

						<?php	if( $registro_precios["PotenciaP1"]==0)  {echo("-");} else {echo($registro_precios["PotenciaP1"]);}	?></td>
                        <td class="celda_cuarto_anexo_precios borde_contrato texto_contrato" style="width:55px">
						<?php	if( $registro_precios["PotenciaP2"]==0)  {echo("-");} else {echo($registro_precios["PotenciaP2"]);}	?></td>
                        <td class="celda_cuarto_anexo_precios borde_contrato texto_contrato" style="width:55px">
						<?php	if( $registro_precios["PotenciaP3"]==0)  {echo("-");} else {echo($registro_precios["PotenciaP3"]);} ?></td>
                        <td class="celda_cuarto_anexo_precios borde_contrato texto_contrato" style="width:25px">
						<?php
						if 	($registro_contrato["Tarifa"] == '3.1' or $registro_contrato["Tarifa"] == '3.1A'){
							if( $registro_precios["PotenciaP4"]==0)  {
								echo("-");
							} else {
								echo($registro_precios["PotenciaP4"]);
							}
						} else {
							echo '-';
						}
						?>
                        </td>
                        <td class="celda_cuarto_anexo_precios borde_contrato texto_contrato" style="width:25px">
						<?php
						if 	($registro_contrato["Tarifa"] == '3.1' or $registro_contrato["Tarifa"] == '3.1A'){
							if( $registro_precios["PotenciaP5"]==0)  {
								echo("-");
							} else {
								echo($registro_precios["PotenciaP5"]);
							}
						} else {
							echo '-';
						}
						?>
                        </td>
                        <td class="celda_cuarto_anexo_precios borde_contrato texto_contrato" style="width:25px">
						<?php
						if 	($registro_contrato["Tarifa"] == '3.1' or $registro_contrato["Tarifa"] == '3.1A'){
							if( $registro_precios["PotenciaP6"]==0)  {
								echo("-");
							} else {
								echo($registro_precios["PotenciaP6"]);
							}
						} else {
							echo '-';
						}
						?>
                        </td>
                    </tr>
                </table>



           	<div class="nota_anexo_impresion_contrato" style="margin-top:0px;"><?=$explicacion_anexo_precios1?></div>

<!--*****************************************************************(FIN)HOJA 2-TABLA3*************************************************-->
<!--************************************************************************************************************************************-->

<!--*****************************************************************HOJA 2-TABLA4******************************************************-->
<!--************************************************************************************************************************************-->

            <div class="titulo_apartado_impresion_contrato" style="margin-top:10px;"><?=$precio_energia?></div>



                <table border="1" cellpadding="0" cellspacing="0" class="tabla_impresion_contrato" style="width:98%">
                    <tr>
                        <td class="celda_cuarto_larga_anexo_precios borde_contrato texto_contrato"><br /></td>
                        <td class="celda_cuarto_anexo_precios borde_contrato texto_contrato" style="width:55px"><strong><?=$punta?></strong></td>
                        <td class="celda_cuarto_anexo_precios borde_contrato texto_contrato" style="width:55px"><strong><?=$llana?></strong></td>
                        <td class="celda_cuarto_anexo_precios borde_contrato texto_contrato" style="width:55px"><strong><?=$valle?></strong></td>
                        <td class="celda_cuarto_anexo_precios borde_contrato texto_contrato" style="width:25px"><strong>P4</strong></td>
                        <td class="celda_cuarto_anexo_precios borde_contrato texto_contrato" style="width:25px"><strong>P5</strong></td>
                        <td class="celda_cuarto_anexo_precios borde_contrato texto_contrato" style="width:25px"><strong>P6</strong></td>
                    </tr>

                    <!--<tr>
                        <td class="celda_cuarto_larga_anexo_precios borde_contrato texto_contrato"><strong><?=$potencia_contratada?></strong></td>
                        <td class="celda_cuarto_anexo_precios borde_contrato texto_contrato"><?php

                        if ($registro_contrato["Tarifa"] != '2.0A' and $registro_contrato["Tarifa"] != '2.1A') {?>
						<?=$registro_cliente["PotenciaContratada"]?>
                        <?php } ?>
                        </td>
                        <td class="celda_cuarto_anexo_precios borde_contrato texto_contrato">
						<?php
                        if ($registro_contrato["Tarifa"] == '2.0A' or $registro_contrato["Tarifa"] == '2.1A'
						or $registro_contrato["Tarifa"] == '3.0A' or $registro_contrato["Tarifa"] == '3.1A') {?>
						<?=$registro_cliente["PotenciaContratada"]?>
                        <?php } ?></td>
                        <td class="celda_cuarto_anexo_precios borde_contrato texto_contrato" >
						<?php
                        if ($registro_contrato["Tarifa"] == '2.0DHA' or $registro_contrato["Tarifa"] == '2.1DHA'
						or $registro_contrato["Tarifa"] == '3.0A' or $registro_contrato["Tarifa"] == '3.1A') {?>
						<?=$registro_cliente["PotenciaContratada"]?>
                        <?php } ?></td>
                    </tr>  -->

                    <tr>
                        <td class="celda_cuarto_larga_anexo_precios borde_contrato texto_contrato"><strong><?=$precio_por_periodo?> /kW</strong></td>
                        <td class="celda_cuarto_anexo_precios borde_contrato texto_contrato" style="width:55px">

						<?php if( $registro_precios["EnergiaP1"]==0)  {echo("-");} else {echo($registro_precios["EnergiaP1"]);} ?></td>
                        <td class="celda_cuarto_anexo_precios borde_contrato texto_contrato" style="width:55px">
						<?php if( $registro_precios["EnergiaP2"]==0)  {echo("-");} else {echo($registro_precios["EnergiaP2"]);} ?></td>
                        <td class="celda_cuarto_anexo_precios borde_contrato texto_contrato" style="width:55px">
						<?php if( $registro_precios["EnergiaP3"]==0)  {echo("-");} else {echo($registro_precios["EnergiaP3"]);} ?></td>

                        <td class="celda_cuarto_anexo_precios borde_contrato texto_contrato" style="width:25px">
						<?php
						if 	($registro_contrato["Tarifa"] == '3.1' or $registro_contrato["Tarifa"] == '3.1A'){
							if( $registro_precios["EnergiaP4"]==0)  {
								echo("-");
							} else {
								echo($registro_precios["EnergiaP4"]);
							}
						} else {
							echo '-';
						}
						?>
                        </td>

                        <td class="celda_cuarto_anexo_precios borde_contrato texto_contrato"  style="width:25px">
						<?php
						if 	($registro_contrato["Tarifa"] == '3.1' or $registro_contrato["Tarifa"] == '3.1A'){
							if( $registro_precios["EnergiaP5"]==0)  {
								echo("-");
							} else {
								echo($registro_precios["EnergiaP5"]);
							}
						} else {
							echo '-';
						}
						?>
                        </td>

                        <td class="celda_cuarto_anexo_precios borde_contrato texto_contrato"  style="width:25px">
						<?php
						if 	($registro_contrato["Tarifa"] == '3.1' or $registro_contrato["Tarifa"] == '3.1A'){
							if( $registro_precios["EnergiaP6"]==0)  {
								echo("-");
							} else {
								echo($registro_precios["EnergiaP6"]);
							}
						} else {
							echo '-';
						}
						?>
                        </td>
                    </tr>
                </table>



           	<div class="nota_anexo_impresion_contrato" style="margin-top:0px;"><?=$explicacion_anexo_precios2?></div>

<!--*****************************************************************(FIN)HOJA 2-TABLA4*************************************************-->
<!--************************************************************************************************************************************-->

<!--*****************************************************************HOJA 2-TABLA5******************************************************-->
<!--************************************************************************************************************************************-->
            <div class="titulo_pequeno_anexo_precios"><?=$calendario_periodos?></div>



                <table border="1" cellpadding="0" cellspacing="0" class="tabla_impresion_contrato" style="width:98%">
                    <tr>
                        <td class="celda_mitad_anexo_precios borde_contrato texto_contrato" colspan="3"><strong><?=$invierno?></strong></td>
                        <td class="celda_mitad_anexo_precios borde_contrato texto_contrato" colspan="3"><strong><?=$verano?></strong></td>
                    </tr>

                    <tr>
                        <td class="celda_tercio_anexo_precios borde_contrato texto_contrato"><strong><?=$punta?></strong></td>
                        <td class="celda_tercio_anexo_precios borde_contrato texto_contrato"><strong><?=$llana?></strong></td>
                        <td class="celda_tercio_grande_anexo_precios borde_contrato texto_contrato"><strong><?=$valle?></strong></td>

                        <td class="celda_tercio_anexo_precios borde_contrato texto_contrato"><strong><?=$punta?></strong></td>
                        <td class="celda_tercio_anexo_precios borde_contrato texto_contrato"><strong><?=$llana?></strong></td>
                        <td class="celda_tercio_grande_anexo_precios borde_contrato texto_contrato"><strong><?=$valle?></strong></td>
                    </tr>
                    <tr>
                        <td class="celda_tercio_anexo_precios borde_contrato texto_contrato"><strong>
                        <?php switch($registro_contrato["Tarifa"]){
						 case '2.0A':
						 	echo '---';
							break;
						  case '2.1A':
						  	echo '---';
							break;
						  case '2.0DHA':
						  	echo '12-22';
							break;
						  case '2.1DHA':
						  	echo '12-22';
							break;
						  case '3.0A':
						  	echo '18-22';
							break;
						  case '3.1A':
						  	echo '17-23';
							break;
						  default:
						  	echo '---';
							break;
						}
						?>
                        </strong></td>
                        <td class="celda_tercio_anexo_precios borde_contrato texto_contrato"><strong>
                        <?php switch($registro_contrato["Tarifa"]){
						 case '2.0A':
						 	echo '0-24';
							break;
						  case '2.1A':
						  	echo '0-24';
							break;
						  case '2.0DHA':
						  	echo '---';
							break;
						  case '2.1DHA':
						  	echo '---';
							break;
						  case '3.0A':
						  	echo '8-18/22-24';
							break;
						  case '3.1A':
						  	echo '08-17/23-24';
							break;
						  default:
						  	echo '---';
							break;
						}
						?>
                        </strong></td>
                        <td class="celda_tercio_grande_anexo_precios borde_contrato texto_contrato"><strong>
                        <?php switch($registro_contrato["Tarifa"]){
						 case '2.0A':
						 	echo '---';
							break;
						  case '2.1A':
						  	echo '---';
							break;
						  case '2.0DHA':
						  	echo '0-12/22-24';
							break;
						  case '2.1DHA':
						  	echo '00-12/22-24';
							break;
						  case '3.0A':
						  	echo '0-8';
							break;
						  case '3.1A':
						  	echo '0-8';
							break;
						  default:
						  	echo '---';
							break;
						}
						?>
                        </strong></td>

                        <td class="celda_tercio_anexo_precios borde_contrato texto_contrato"><strong>
                        <?php switch($registro_contrato["Tarifa"]){
						 case '2.0A':
						 	echo '---';
							break;
						  case '2.1A':
						  	echo '---';
							break;
						  case '2.0DHA':
						  	echo '13-23';
							break;
						  case '2.1DHA':
						  	echo '13-23';
							break;
						  case '3.0A':
						  	echo '11-15';
							break;
						  case '3.1A':
						  	echo '10-16';
							break;
						  default:
						  	echo '---';
							break;
						}
						?>
                        </strong></td>
                        <td class="celda_tercio_anexo_precios borde_contrato texto_contrato"><strong>
                        <?php switch($registro_contrato["Tarifa"]){
						 case '2.0A':
						 	echo '0-24';
							break;
						  case '2.1A':
						  	echo '0-24';
							break;
						  case '2.0DHA':
						  	echo '---';
							break;
						  case '2.1DHA':
						  	echo '---';
							break;
						  case '3.0A':
						  	echo '8-11/15-24';
							break;
						  case '3.1A':
						  	echo '09-10/16-01';
							break;
						  default:
						  	echo '---';
							break;
						}
						?>
                        </strong></td>
                        <td class="celda_tercio_grande_anexo_precios borde_contrato texto_contrato"><strong>
                        <?php switch($registro_contrato["Tarifa"]){
						 case '2.0A':
						 	echo '---';
							break;
						  case '2.1A':
						  	echo '---';
							break;
						  case '2.0DHA':
						  	echo '00-13/23-24';
							break;
						  case '2.1DHA':
						  	echo '00-13/23-24';
							break;
						  case '3.0A':
						  	echo '0-8';
							break;
						  case '3.1A':
						  	echo '01-09';
							break;
						  default:
						  	echo '---';
							break;
						}
						?>
                        </strong></td>
                    </tr>
                    <?php
					if ($registro_contrato["Tarifa"] == '3.1' or $registro_contrato["Tarifa"] == '3.1A' or
						$registro_contrato["Tarifa"] == '3.0' or $registro_contrato["Tarifa"] == '3.0A'){
					?>
						<tr>
                        	<td class="celda_tercio_anexo_precios borde_contrato texto_contrato"><strong>---</strong></td>
                            <td class="celda_tercio_anexo_precios borde_contrato texto_contrato"><strong>18-24</strong></td>
                            <td class="celda_tercio_anexo_precios borde_contrato texto_contrato" style="width:79px"><strong>0-18</strong></td>
                            <td class="celda_tercio_anexo_precios borde_contrato texto_contrato"><strong>---</strong></td>
                            <td class="celda_tercio_anexo_precios borde_contrato texto_contrato"><strong>18-24</strong></td>
                            <td class="celda_tercio_anexo_precios borde_contrato texto_contrato"><strong>0-18</strong></td>
                        </tr>
					<?php
					}
					?>
                </table>



           	<div class="nota_anexo_impresion_contrato" style="margin-top:0px;"><?=$explicacion_anexo_precios3?></div>
<!--*****************************************************************(FIN)HOJA 2-TABLA5*************************************************-->
<!--************************************************************************************************************************************-->

<!--*****************************************************************HOJA 2-TABLA6******************************************************-->
<!--************************************************************************************************************************************-->
<?php if ($registro_contrato["Tarifa"] == '3.0A'){ ?>
            <div class="titulo_apartado_impresion_contrato"><?=strtoupper($energia_reactiva)?></div>

<?php

//Estas variables almacenaran los valores de las cabeceras de la tabla de precios de energia reactiva. Se hace este modo ya que al llevar el caracter "<" no valida, ya que se interpreta como la apertura de una etiqueta HTML
	$cabecera_eenergia_reactiva1="Cos &#60;0,95<br />a cos = 0,90";
	$cabecera_eenergia_reactiva2="Cos &#60;0,90<br />a cos = 0,85";
	$cabecera_eenergia_reactiva3="Cos &#60;0,85<br />a cos = 0,80";
	$cabecera_eenergia_reactiva4="Cos &#60;0,80";
?>


                <table border="1" cellpadding="0" cellspacing="0" class="tabla_impresion_contrato" style="width:98%">
                    <tr>
                        <td class="celda_quinto_grande_anexo_precios cabecera_energia_reactiva_anexo_impresion_contrato borde_contrato texto_contrato" align="center"><strong><?=$energia_reactiva?></strong></td>
                        <td class="celda_quinto_anexo_precios borde_contrato texto_contrato"><strong><?=$cabecera_eenergia_reactiva1?></strong></td>
                        <td class="celda_quinto_anexo_precios borde_contrato texto_contrato"><strong><?=$cabecera_eenergia_reactiva2?></strong></td>
                        <td class="celda_quinto_anexo_precios borde_contrato texto_contrato"><strong><?=$cabecera_eenergia_reactiva3?></strong></td>
                        <td class="celda_quinto_anexo_precios cabecera_energia_reactiva_anexo_impresion_contrato borde_contrato texto_contrato"><strong><?=$cabecera_eenergia_reactiva4?></strong></td>
                    </tr>

                    <tr>
                        <td class="celda_quinto_grande_anexo_precios borde_contrato texto_contrato"><strong>/kVArh</strong></td>
                        <td class="celda_quinto_anexo_precios borde_contrato texto_contrato"><strong><?=$registro_precios_reactiva["PrecioReactiva1"]?></strong></td>
                        <td class="celda_quinto_anexo_precios borde_contrato texto_contrato"><strong><?=$registro_precios_reactiva["PrecioReactiva2"]?></strong></td>

                        <td class="celda_quinto_anexo_precios borde_contrato texto_contrato"><strong><?=$registro_precios_reactiva["PrecioReactiva3"]?></strong></td>
                        <td class="celda_quinto_anexo_precios borde_contrato texto_contrato"><strong><?=$registro_precios_reactiva["PrecioReactiva4"]?></strong></td>
                    </tr>

                </table>




           	<div class="nota_anexo_impresion_contrato" style="margin-top:0px;"><?=$explicacion_anexo_precios4?></div>
            <?php } ?>
<!--*****************************************************************(FIN)HOJA 2-TABLA6*************************************************-->
<!--************************************************************************************************************************************-->

<!--*****************************************************************HOJA 2-TABLA7******************************************************-->
<!--************************************************************************************************************************************-->

            <div class="titulo_apartado_impresion_contrato"><?=$complementos?></div>


            <div class="tabla_complementos_anexo_impresion_contrato">
	            <table border="1" cellpadding="0" cellspacing="0" class="tabla_impresion_contrato" style="width:98%">
    	            <tr>
                    	<td class="tabla_complementos_anexo_impresion_contrato borde_contrato texto_contrato">
                        <?=$texto_complementos;?>
                        </td>
                   	</tr>
                </table>

            </div><!--<div class="tabla_complementos_anexo_impresion_contrato">-->

<!--*****************************************************************(FIN)HOJA 2-TABLA7*************************************************-->
<!--************************************************************************************************************************************-->

<!--*****************************************************************HOJA 2-TABLA8******************************************************-->
<!--************************************************************************************************************************************-->

            <div class="titulo_apartado_impresion_contrato" style="margin-top:10px;"><?=$equipos_medida?></div>

                <div class="posicion_equipos_medida_anexo_impresion_contrato">

	            <table border="1" cellpadding="0" cellspacing="0" class="tabla_impresion_contrato" style="width:98%">
    	            <tr>
                    	<td class="celda_mitad_tabla_complementos_anexo_impresion_contrato borde_contrato texto_contrato"><strong><?=$alquiler_equipo?></strong></td>
                    	<td class="celda_mitad_tabla_complementos_anexo_impresion_contrato borde_contrato texto_contrato"><strong><?=$euros_mes?>:</strong>



						<?php if( $registro_cliente["ImporteAlquiler"]==0)  {echo("-");} else {echo($registro_cliente["ImporteAlquiler"]);} ?>
						</td>
                   	</tr>

    	            <!--<tr>
                    	<td class="celda_mitad_tabla_complementos_anexo_impresion_contrato borde_contrato texto_contrato"><strong><?=$otros_alquileres?></strong></td>
                    	<td class="celda_mitad_tabla_complementos_anexo_impresion_contrato borde_contrato texto_contrato"><strong><?=$euros_mes?>:</strong>
                        <?php if( $registro_cliente["ImporteAlquiler"]==0)  {echo("-");} else {echo($registro_cliente["ImporteAlquiler"]);} ?>
                        </td>
                   	</tr>-->

                </table>

	             </div><!--<div class="posicion_equipos_medida_anexo_impresion_contrato">-->

           	<div class="nota_anexo_impresion_contrato"><?=$explicacion_anexo_precios5?></div>
<!--*****************************************************************(FIN)HOJA 2-TABLA8*************************************************-->
<!--************************************************************************************************************************************-->

			<div class="texto_impresion_contrato"><?=$explicacion_anexo_precios6?></div><!--<div class="texto_impresion_contrato">-->

            <div class="fecha_contratacion_impresion_contrato">




			<?php
 // $rowTexto = //mysql_fetch_array($resultTexto);$rowTexto[1]);
$Monate = array('Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');



//$Monate = array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
  $mes = $Monate[date("m")-1];


  echo("<br />");
  echo($poblacion_empresa.", ");
  echo(date("d "));
  echo("de ");
  echo($mes);
  echo(" del ");
  echo(date("Y"));
   echo(".");
    ?></div>

            <div class="firma_cliente_impresion_contrato"><?=$el_cliente?></div>
            <div class="firma_empresa_impresion_contrato"><?=$nombre_empresa?></div>
			<div class="limpiar"></div>
            <div class="fdo_cliente_impresion_contrato">
				<?=$fdo?>. <?=$registro_cliente["TitularNombre"]?>
            </div>
            <div class="fdo_empresa_impresion_contrato">
				&nbsp;&nbsp;&nbsp;<img src="../img/oficina/firma.jpg" height="90"><br />
				<?=$fdo?>. <?=$nombre_empresa?>
            </div>
			<div class="limpiar"></div>

    	</div><!--<div class="margenes_impresion_contrato">-->

	</div><!--<div class="dimensiones_contrato">-->
<!--*****************************************************************(FIN)HOJA 2********************************************************-->
<!--************************************************************************************************************************************-->

<!--*****************************************************************HOJA 2**************************************************************-->
<!--************************************************************************************************************************************-->
	<div class="dimensiones_contrato">
    	<div class="margenes_impresion_contrato">
    		<?php /*?><img src="fpdf/condicionesContrato1.png" style="border:none">
            <img src="fpdf/condicionesContrato2.png" style="border:none"><?php */?>


		  <div class="limpiar"></div>
    	</div><!--<div class="margenes_impresion_contrato">-->
	</div><!--<div class="dimensiones_contrato">-->
<!--*****************************************************************(FIN)HOJA 2********************************************************-->
<!--************************************************************************************************************************************-->

<!--*****************************************************************HOJA 3*************************************************************-->
<!--************************************************************************************************************************************--><!--<div class="dimensiones_contrato">-->

<!--*****************************************************************(FIN)HOJA 3********************************************************-->
<!--************************************************************************************************************************************-->

<!--*****************************************************************HOJA 4*************************************************************-->
<!--************************************************************************************************************************************-->

<!--NOTA: ESTA ULTIMA PARTE ES EXCLUSIVA DE CADA CLIENTE, POR LO TANTO SE MOSTRARÁ EN EL IDIOMA EN EL QUE LO FACILITE-->
	 <!--<div class="dimensiones_contrato">-->
<!--*****************************************************************(FIN)HOJA 4********************************************************-->
<!--************************************************************************************************************************************-->

</body>
</html>
<?php
}//else($_SESSION['usuario'])
?>