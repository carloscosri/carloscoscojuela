<?php
$error_rellenar="You should fill in all obligatory fields.";
$error_fecha="The date has been written in an incorrect way. It should use the following format: 18-03-2009.";
$error_hora="The time and minutes should be 2  numerical characters between 00 and 23 and between 00-59 respectively.";
$error_telefonos="Fields Phone, Mobile and Fax only can contain numerical characters.";
$error_mail="You has not written the and-mail correctly.";
$incidencia_ok="Your incidence has been sent.";
$error_fecha_posterior="Written dates can not be subsequent to the current date.";
?>