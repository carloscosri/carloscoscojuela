<?php
$error_rellenar="You should fill in all obligatory fields.";
$error_fecha="The date has been written in an incorrect way. It should use the following format: 18-03-2009.";
$error_fecha_posterior="Written dates can not be subsequent to the current date.";
$error_fecha_inicio_mayor="Start date can not be greater than the final date.";
$solicitud_ok="His request has been sent succesfully";
$oficina_informacion_facturas1="ACTIVE CONSUMPTIONS";
$oficina_informacion_facturas2="P1 Tip";
$oficina_informacion_facturas3="P2 Flat";
$oficina_informacion_facturas4="P3 Valley";
$oficina_informacion_facturas5="REACTIVE CONSUMPTIONS";
$oficina_informacion_facturas6="AMOUNTS";
$oficina_informacion_facturas7="Active";
$oficina_informacion_facturas8="Reactive";
$oficina_informacion_facturas9="Maximeter";
$oficina_informacion_facturas10="Discounts";
$oficina_informacion_facturas11="Charges";
$oficina_informacion_facturas12="Base";
$oficina_informacion_facturas13="VAT";
$oficina_informacion_facturas14="TOTAL:";
$oficina_informacion_facturas15="Print";
$error_no_facturas="Currently does not be invoices related to this client in our database.";
?>