<?php
$error_rellenar="You should write the date of the reading and a meter number.";
$error_fecha="The date has been written in an incorrect way. It should use the following format: 18-03-2009.";
$error_lectura="You should write at least a reading.";
$lectura_ok="Your reading has been sent.";
$oficina_lecturas1="Reading of only one Pricing Period";
$oficina_lecturas2="Readings with Several Pricing Periods";
$oficina_lecturas3="Period";
$oficina_lecturas4="P1 Tip";
$oficina_lecturas5="P2 Flat";
$oficina_lecturas6="P3 Valley";
$oficina_lecturas7="Active";
$oficina_lecturas8="Reactive";
$oficina_lecturas9="Maximeter";
$error_varias_lecturas="If you has written readings in the section 'Reading of an Alone Pricing Period' you can´t write readings in the section 'Readings with Several Pricing Periods'.";
$error_numero_lectura="You has not correctly written some reading. They should be at the most a numbers with 8 entire and 3 decimals.";
$error_fecha_posterior="Written dates can not be subsequent to the current date.";
?>