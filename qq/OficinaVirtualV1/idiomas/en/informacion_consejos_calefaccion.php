<?php
$informacion_consejos_calefaccion1="Advices to Save Energy - Heating";
$informacion_consejos_calefaccion2="Electric heating. Saving energy with nocturnal night storage heaters and fare 2,0:";
$informacion_consejos_calefaccion3="Storage heaters are devices able to store heat energy during a period of time, to free it later slowly, with ajustable flow. They are elements suitable for environmental heating because they use hours at night to store the heat, making the best use of  the advantages of the Nocturnal Fare. The user of this fare, obtains a discount of 53% in the consumed price of energy during nocturnal hours, and they have a surcharge of 3% in the price of the consumed time kilowatt during the day.";
$informacion_consejos_calefaccion4="Types of accumulators:";
$informacion_consejos_calefaccion5="There are two types of accumulators, whose difference is the way of yielding heat:<br /><br />
<strong>Static Accumulators:</strong> In this type of devices, the heat is yielded mostly for radiation, through the metallic wrapper, and for a natural convention, thanks to the air that can flow through some existing channels in the casing and in core of the accumulator. To do this, it has an air entry in the low part of the device and an exit for the upper part. The outlet air is regulated through a floodgate.<br /><br />
<strong>Dynamic Accumulators:</strong> There are similar to the static ones. It yields the heat for radiation too, through the metallic casing, but the convection is forced through a turbine that makes to flow the air by existing conduits or channels in the core of the accumulator.";
$informacion_consejos_calefaccion6="<strong>1.  Accumulator block: </strong>The core accumulator is constituted by plates of heat-resisting material (magnesita) that should has a specific high heat and the maximum density, because the capacity to store heat depends on those two parameters. The maximum temperature that is reached at the end of the load period in the core of the accumulator is 600 to 700 ºC.<br /><br />
<strong>2. Heater Resistors: </strong>The elements hearters (resistances) are inserted in cavities or channels situated inside the core accumulator, that way they heat all the mass of the refractory uniformly.<br /><br />
<strong>3. Thermal insulation:</strong> The block accumulator is surrounded with thermical insulator to keep the heat accumulate and to limit the superficial temperature of furniture of the device.<br /><br />
<strong>4. Device of security:</strong> In general accumulators have a thermal limiter or of a security thermostat, to disconnect the device if for some reason, an inadequate evaluation of the temperature is produced  (for example, a malfunction in the system of regulation of the system of load).<br /><br />
<strong>5. Thermostat:</strong> There are two of them, one for modify the load and another for the heat issuance.";
$informacion_consejos_calefaccion7="ADVANTAGES:";
$informacion_consejos_calefaccion8="It can be installed in buildings of new construction and in houses former.";
$informacion_consejos_calefaccion9="There are no circuits of water, eliminating the leaks possibility.";
$informacion_consejos_calefaccion10="It doesn´t needs a fuel deposit.";
$informacion_consejos_calefaccion11="Simple installation, without making improvements .";
$informacion_consejos_calefaccion12="Ecological, then they do not issue smokes and they use the cleanest energy that is known in the point of consumption: the Electricity.";
$informacion_consejos_calefaccion13="Total security because not having fuels, there is no risk of accidental explosion.";
$informacion_consejos_calefaccion14="Practically nonexistent maintenance.";
$informacion_consejos_calefaccion15="They give heating during 24 h. at day.";
$informacion_consejos_calefaccion16="General costs are reduced of the invoicing, when moving the consumptions of the refrigerator, washers, dishwasher, water heaters, lighting, etc, Nocturnal Fare, as has been pointed out previously in the description of the device.";
$informacion_consejos_calefaccion17="It has advanced design aesthetic semblance. ";
$informacion_consejos_calefaccion18="They are maximum automatizables.";
$informacion_consejos_calefaccion19="Electric heating via cable radiant with systems of accumulation (base + support, nocturnal fare 2,0):";
$informacion_consejos_calefaccion20="The via cable radiant electric heating is a not visible heating system, that incorporates two differentiated parts about hourly use and complementary by its operation: base heating and back-up heating.";
$informacion_consejos_calefaccion21="Base heating:";
$informacion_consejos_calefaccion22="This heater system installed in the floor is connected exclusively at night, when the nocturnal fare has a reduction of 53% in the price of the kWh.
The base heating installed in the floor, accumulates heat during the cheap rate period, that restitutes to the ambient without spending energy during other hours.The amount of accumulated heat in the floor is determined by the outside temperature, because it has a probe which detects the temperature.<br /><br />
Because the internal temperature is increased in some degrees because of free heat contributions generated by people, illumination, sanitary hot water, appliances, heatstroke during the day, etc, and the amount of these contributions is unpredictable and not detectable by the probe of the throttle situated in the exterior and that it doesn´t works during day-time hours, it is essential to have a back-up heater system complementary that allows an accurate regulation of the wished comfort level in every room.";
$informacion_consejos_calefaccion23="Back-up heating:";
$informacion_consejos_calefaccion24="This independent heater system, of reduced potency because just works compensating the variations of the internal temperature that the base system can´t detect or correct, it is complementary of the base heating by accumulation. It consists of heater cables integrated in the floor, over the cables of the base system, connected to the electric power network through an automatic ambient thermostat in every room or premise.<br /><br />
Its function is to keep during 24 at day the precise temperature wished by the user in every room.
Another option for the back-up heating is the installation of electric convectors to complete the heat contribution until the temperature previously selected in each room.";
$informacion_consejos_calefaccion25="Use Recommendations:";
$informacion_consejos_calefaccion26="The adjustment of the wished temperatures through thermostats situated in different locations is very important to achieve the best comfort of your heating system.<br /><br />
If your house is of recent construction, the humidity of materials can cause a consumption between 20 and 30% higher than the normal in the first operation season, independent of which is the heating system that you have.";
$informacion_consejos_calefaccion27="ADVANTAGES:";
$informacion_consejos_calefaccion28="Clean: Movements of the air are not produced and, as a result, of the dust.";
$informacion_consejos_calefaccion29="Healthy: It doesn´t consume oxygen, doesn´t parch the air and it doesn´t reheats excessively.";
$informacion_consejos_calefaccion30="Controlled cost: The individualized installation by rooms allows the control of the expense according to needs, schedules and uses. Moreover makes good use of free heat contributions (sun, lights, people).";
$informacion_consejos_calefaccion31="Invisible and quiet: There isn´t machinery nor mechanical elements in sight.";
$informacion_consejos_calefaccion32="Safe: Out of reach of people and electrically protected according to the Low Voltage Electrotechnical Regulation, and because it hasn´t fuels there is no risk of accidental explosion.";
$informacion_consejos_calefaccion33="Moreover: Without auxiliary works, fuel deposits, maintenance, advanced payments of energy and, it is the system with lower investmented cost.";
$informacion_consejos_calefaccion34="Practical Advices:";
$informacion_consejos_calefaccion35="The temperature adjustment in each room through the thermostat is very important to control the consumption of your heating system. Setting a temperature of 20ºC instead of 21ºC can mean an energy saving of 10%.";
$informacion_consejos_calefaccion34="Practical Advices:";
$informacion_consejos_calefaccion35="The adjustment of the temperature in each room through the thermostat is very important to control the consumption of your heating system. Setting a temperature of 20ºC instead of 21ºC can mean an energy saving of 10%.";
$informacion_consejos_calefaccion36="When you goes out home just for some hours, set the position of the thermostat in 15º, that is the one equivalent to some models 'economical' position.";
$informacion_consejos_calefaccion37="You should reduce the temperature of the rooms which you don´t occupy during long periods of time.";
$informacion_consejos_calefaccion38="You should turn off the heating at night and turn it on in the morning after ventilating the house and close windows.";
$informacion_consejos_calefaccion39="To ventilate rooms 10 minutes are enough. An excessive renewal can represent between about 30 and 40% of the total consumption.";
$informacion_consejos_calefaccion40="To avoid excessive heat losses, at night close blinds and curtains.";
$informacion_consejos_calefaccion41="You should use thermostatic valves in radiators and programer thermostats, they are easy to install and are amortized quickly (suppose an energy saving between 8 and 133%).";
$informacion_consejos_calefaccion42="At the beggining of every season of cold it is convenient to purge the air inland of radiators to facilitate the heat transmission from the hot water to the exterior.";
$informacion_consejos_calefaccion43="You shouldn´t cover or place next to the radiator objects.";
$informacion_consejos_calefaccion44="Take care and keep properly your heating equipment, it can supposes you a saving of until 15% of energy.";
$informacion_consejos_calefaccion45="It is necessary the counseling of an installer for the initial regulation and starting up of the heating base.";
?>