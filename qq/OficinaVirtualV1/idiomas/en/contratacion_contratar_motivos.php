<?php
$contratacion_contratar_motivos1="Reasons to contract with ";
$contratacion_contratar_motivos2="Our philosophy is to give the maximum in everything that we are able to: quality of provision, attention, counseling, inspections, security, innovation...";
$contratacion_contratar_motivos4="You will have at your disposal a great group of technicians and professionals always prepared to help you.";
$contratacion_contratar_motivos3="We warrant you all the experience of a company which has supplied electrical energy during years.";
$contratacion_contratar_motivos5="Our offices, stores and technicians are located in our city and, if necessary, are near from you to help you.";
$contratacion_contratar_motivos6="Benefits that the company generates are kept and invested,the most part of it,  in our city.";
$contratacion_contratar_motivos7="We have a commitment with you and with the earth, because an energetic saving is not only good for saving money, also it is good to environment.";
?>