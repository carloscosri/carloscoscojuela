<?php
$titulo_web="AUDINFOR SYSTEM - Oficina Virtual";
$pie_imagen="AUDINFOR SYSTEM S.L.";
$pie="Website developed by Audinfor System S.L.";
$w3c_html="W3C HTML Validation";
$w3c_css="W3C CSS Validation";
$menu1="Company";
$menu2="Offers and Contracting";
$menu3="Customer´s Information";
$menu4="Online Office";
$menu5="Sige Energy Portal";
$datos_empresa="AUDINFOR SYSTEM, SL | C/ PANAMA 2 LOCAL 2 ZARAGOZA Tel. 976312018";
$menu3_2="Customer´s<br />Information";
$menu_idiomas1="Change language to Spanish";
$menu_idiomas2="Change language to Catalan";
$menu_idiomas3="Change language to Basque";
$menu_idiomas4="Change language to Galician";
$menu_idiomas5="Changing language into English";
$idioma_activo="Active language";
$error_identificacion="This seccion only can be accessed by identified users.";
$nota_pdf1="* This documentation is in PDF format, to visualize these files, needs to have installed the program Adobe Acrobat Reader, if not it has can download this program (free) from here:";
$nota_pdf2="Download Adobe Acrobat Reader";
$poblacion_empresa="ZARAGOZA";
$banner_sigeenergia="Access to Website";
$nota_campos_obligatorios="ATTENTION: Only special fields with * are compulsory.";
$tlf_averias="";
?>