<?php
$titulo_potencias_menos_10="Fares of contracted power until 10 kW";
$titulo_potencias_mas_10="Fares of contracted power higher than 10 Kw and lower than 15 Kw";
$titulo_potencias_alta_tension="Prices for High Tension Contracts";
$termino_potencia="Potency Term";
$termino_energia="Term of Energy ";
$euro_kw_mes="Euros / Kw / Month";
$euro_kwh="Euros / kWh";
$volver="Return";
$nota_potencias_menos_10="NOTE: Showed prices will could be modified during the validity period of the contract or of its extra times according to sections 4 and 6 of general conditions of the Contract of Provision.";
$nota_potencias_mas_10="";
$nota_potencias_alta_tension="In order to apply fares of the type 6.1, an individual study will be done and a customised offer will be done for every client.";
$tarifas_alta_tension1="If you need to contract a Provision in Average/High Tension, he should fill ";
$tarifas_alta_tension2="the form that is pointed out in the corresponding section.";
$tarifas_alta_tension3="Later we will contact you to inform you aboout our prices and conditions.";
$titulo_potencias_mas_15="Prices for Power Contracts Greater than 15 KW";
?>