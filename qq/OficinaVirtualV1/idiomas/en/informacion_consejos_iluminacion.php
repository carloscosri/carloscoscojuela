<?php
$informacion_consejos_iluminacion1="Advices to Save Energy - Illumination";
$informacion_consejos_iluminacion2="From the beginning of the existence, the humans had striven to find systems to substitute solar light in the dark hours or in places where it was scarce.<br /><br />
Result of this interest has been a series of elements appearance that, from the discovery of the fire until the different modalities of electric lamps acquaintances nowadays, has gone marking phases in the development of artificial light sources.<br /><br />
Currently, the investigation continues moving forward towards the achievement of a lamp  which has a light quality the nearest possible to solar light.<br /><br />
In the market we can find several kinds of suitable lamps to domestic use in which varies, among others factors, its duration, the consumption and the quality of the light which it emits.";
$informacion_consejos_iluminacion3="Lightning Rod";
$informacion_consejos_iluminacion4="Halogenous Lamps";
$informacion_consejos_iluminacion5="Fluorescent Lamps";
$informacion_consejos_iluminacion6="Low-energy Lamps";
$informacion_consejos_iluminacion7="Comparative Chart";
$informacion_consejos_iluminacion8="Effectiveness of the Different Kinds of Lamps";
$informacion_consejos_iluminacion10="They are popular light bulbs whose light is achieved through a filament that is heated with conduct of the electricity.";
$informacion_consejos_iluminacion11="ADVANTAGES:";
$informacion_consejos_iluminacion12="These kind of lamps emit a light of great amount, which consequently they reproduce very well colors.";
$informacion_consejos_iluminacion13="It are the cheapest ones between all artificial light sources.";
$informacion_consejos_iluminacion14="They offer great flexibility because of the huge models and potencies ranges.";
$informacion_consejos_iluminacion15="DISADVANTAGES:";
$informacion_consejos_iluminacion16="They consume more energy that other in comparison with the amount of light that contribute.";
$informacion_consejos_iluminacion17="In comparison with other kind of lamps, its duration is more limited.";
$informacion_consejos_iluminacion18="In comparison with traditional light bulbs, halogen light is more luminous and white, also contributes several individual solutions of illumination, and of course with a great visual comfort, obtaining more modern and attractive ambients. They exist with or without reflector, depends on we want to concentrate the light or not.";
$informacion_consejos_iluminacion19="They offer a whiter and brilliant light, for that reason allows a perfect discrimination of colors.";
$informacion_consejos_iluminacion20="For its qualities give more possibilities on indoors decoration.";
$informacion_consejos_iluminacion21="Its effectiveness and duration is higher than normal incandescent lamps.";
$informacion_consejos_iluminacion22="Its light quality remains unchanging over all the life of the lamp.";
$informacion_consejos_iluminacion23="They have an high power consumption.";
$informacion_consejos_iluminacion24="The types of little potency demand a transformer that, in determined models, brings already installed.";
$informacion_consejos_iluminacion25="The light issuance is very concentrated, for that reason it compels to install a screen the lamp in order to avoid that it is seen directly.";
$informacion_consejos_iluminacion26="In this case, light is not produced by the warming of a filament but is produced by an electric shock in a maintained arch in a gas or ionized steam.<br /><br />
Fluorescent lamps are advisable as general lighting of universal and economic application, and because exists different tonalities of the light, you can choose the most suitable one for your application, as in bathrooms, kitchens, offices, aisles, lumber rooms or garages.";
$informacion_consejos_iluminacion27="They offer, depending on the range, a good light quality and natural reproduction of colors.";
$informacion_consejos_iluminacion28="Its energy consumption is very reduced and profitable because, to equal amount of light, it consumes the fifth part that incandescent lamps and its duration is very long, from eight to ten times more than these.";
$informacion_consejos_iluminacion29="As opposed to popular opinion, do not consume more in the switch on, for that reason they should switch off when they aren´t been used.";
$informacion_consejos_iluminacion30="Its light quality remains unchanging over all the life of the lamp.";
$informacion_consejos_iluminacion31="Its dimensions and designs limit its uses because doesn´t exist a more extensive offer than the incandescent ones.";
$informacion_consejos_iluminacion32="They are energetic saving lamps (Fluorescent compact electronics) that, in addition to contributing an environmental light quality anywhere, both hinterland and exterior, they are basic for its low consumption in those places where lighting are needed with long periods of switched on. They are ideal for outer spaces and such as a  lighting of security.<br /><br />
This efficient lamp family, are something more that merely economic. They can be used almost on a general way, exactly the same as traditional light bulbs.";
$informacion_consejos_iluminacion33="They consume five times fewer than the incandescent ones.";
$informacion_consejos_iluminacion34="They have a service life 6 great times, approximately eight years, estimating four daily hours of switched on.";
$informacion_consejos_iluminacion35="Thanks to the energy saving, its use contributes to preserv the environment.";
$informacion_consejos_iluminacion36="They possess the same bulbholder that traditional light bulbs, for that reason, joined to his light quality and comfort, becames them useful for any application.";
$informacion_consejos_iluminacion37="Traditional";
$informacion_consejos_iluminacion38="In the following table you can see the effectiveness, the average duration and the possibility of distinguishing colors between different kinds of the most utilized lamps in houses.";
$informacion_consejos_iluminacion39="Type of Lamp";
$informacion_consejos_iluminacion40="Effectiveness Index";
$informacion_consejos_iluminacion41="Duration Average(h)";
$informacion_consejos_iluminacion42="Possibility of Distinguishing Colors";
$informacion_consejos_iluminacion43="Incandescent";
$informacion_consejos_iluminacion44="Excellent";
$informacion_consejos_iluminacion45="Halogenous";
$informacion_consejos_iluminacion46="Fluorescent";
$informacion_consejos_iluminacion47="Good";
$informacion_consejos_iluminacion48="Fluorescent Extra";
$informacion_consejos_iluminacion49="Very Good";
$informacion_consejos_iluminacion50="Practical Advices:";
$informacion_consejos_iluminacion51="If you can, always make good use of the natural lighting.";
$informacion_consejos_iluminacion52="You should use at home clear colors for walls and ceilings to make a better use of the natural illumination and to reduce the artificial one.";
$informacion_consejos_iluminacion53="You should always turn off lights in the rooms which is not using.";
$informacion_consejos_iluminacion54="Minimize the ornamental illumination in gardens and other outer zones.";
$informacion_consejos_iluminacion55="You should clean often lamps and screens, thus it will increase the brightness without increasing the potency.";
$informacion_consejos_iluminacion56="You shold adapt the illumination to your needs and we have to consider that with the located illumination will achieve, in addition to saving energy, more comfortable ambients.";
$informacion_consejos_iluminacion57="You should use low-energy light bulbs instead of incandescent light bulbs, last 6 times more and it will save until 80% of energy.";
$informacion_consejos_iluminacion58="You sholud use electronic lamps because its life is larger, support a bigger number of switched on and turning off endure, and they consume fewer than conventional low-energy lamps. You will know to distinguish them for his weight: whereas the conventional ones weigh around the 400 grs. electronics weigh about 100 grs.";
$informacion_consejos_iluminacion59="You sholud install electronic luminous intensity regulator (not of rostato).";
$informacion_consejos_iluminacion60="You should use fluorescent tubes where you need more light during many hours, as in the kitchen.";
$informacion_consejos_iluminacion61="You sholud use presence detectors in order to lights work automatically in those places little habitables ones such as lobbies, garages or public areas.";
$volver="Back";
?>