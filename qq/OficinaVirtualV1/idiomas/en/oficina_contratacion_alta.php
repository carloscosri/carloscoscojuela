<?php
$oficina_contratacion_alta6="Return";
$oficina_contratacion_alta1="Contracts until 10 kWh with or without hourly discriminacion <br />(Fares 2.0.to and 2.0 DHA)";
$oficina_contratacion_alta2="Contracts from 10 to 15 kWh with or without hourly discriminacion <br />(Fares 2.1.to and 2.1 DHA)";
$oficina_contratacion_alta3="Contracts from 15 kWh <br />(Fares 3.0.To and 3.1.To and 6.x)";
$oficina_contratacion_alta4="Details of the Payer (If it´s different than holder details)";
$forma_pago1="Direct debit payment";
$forma_pago2="Undomiciled";
$idioma_facturacion1="Spanish";
$idioma_facturacion2="Catalan";
$idioma_facturacion3="Basque";
$idioma_facturacion4="Galician";
$idioma_facturacion5="Bable (Asturias)";
$periodo_tarifario1="P1-Tip";
$periodo_tarifario2="P2-Flat";
$periodo_tarifario3="P3-Valley";
$oficina_contratacion_alta5="Pricing periods";
$equipos_medida1="Company rent";
$equipos_medida2="Client property";
$oficina_contratacion_alta7="Active";
$oficina_contratacion_alta8="Reactive";
$oficina_contratacion_alta9="Maximeter";
$error_rellenar="You must fill in marked as compulsory fields.";
$error_numeros="All phone numbers, fax, written zip codes should contain only carácteres numerical.";
$error_email="You has written some e-mail wrong";
$error_dni="You have written some ID wrong.";
$error_datos_bancarios="You have selected Direct debit payment as a payment method should select a Banking Entity and fill in the account number correctly.";
$error_num_cuenta="The bank account number should be complete and only can contain numerical characters.";
$error_datos_incompletos="If invoicing details or the payer details are different from account holder, you should fill in fields that are compulsory for the account holder: Name, Street, Number, Town, Province and ID.";
$error_datos_representante="If you wish to facilitate details of the company\'s agent should fill in all the information.";
$alta_ok="The registration has been carried out succesfully.";
$opcion_periodo_tarifario1="P2-Flat";
$opcion_periodo_tarifario2="P1 Tip and P3 Valley";
$opcion_periodo_tarifario3="From P1 to P3";
$opcion_periodo_tarifario4="From P1 to P6";
$oficina_contratacion_alta10="Social Contract <br />(Lower than 3 kWh).";
$estado_bono_social1="Retired";
$estado_bono_social2="Jobless";
$estado_bono_social3="Handicapped man";
$estado_bono_social4="Large Family";
$oficina_contratacion_alta11="Legal Situation to Take in to the Social Contract";
$oficina_contratacion_alta12="Power Normalized to Hire";
$oficina_contratacion_alta13="Identification";
$oficina_contratacion_alta14="Trading Company";
$oficina_contratacion_alta15="Distributor";
$error_cups="The CUPS has not written correctly.";
$oficina_contratacion_alta16="ATTENTION: This identification only is compulsory for those clients that already have contract in force of power supply with any other trading company company.";
$casilla_cliente="If you are a current customer check the box";
$error_condiciones="You must agree to the Terms of Amendment. Check the appropriate box";
$condiciones_modificacion="Changing Conditions";
?>