<?php
$informacion_consejos_termo1="Advices to Save Energy - Flask";
$informacion_consejos_termo2="Electric flask by nocturnal accumulation:";
$informacion_consejos_termo3="To have hot water is part of the minimum comfort. Its operation is simple: the cold water from general network, enters from the low part of the flask and it is heated inside the  device through an electrical resistance, which works in night time schedule, until an approximate temperature of 60ºC, this value can be changed through the thermostat. To achieve its objective should have the following conditions:";
$informacion_consejos_termo4="1. The water always must be available.<br />
2. The amount of water should be enough to cover demands of the house.<br />
3. The water must be supplied at wished temperature.<br />
4. The energy consumption should carry out at night, when the electricity results a 53% cheaper.";
$informacion_consejos_termo5="Advices to Purchase an Electric Flask:";
$informacion_consejos_termo6="There are in market a wide range of electronic flasks. In order to achieve the maximum output, we sholud choose properly the device that we wished to acquire. In order to do this, it is necessary that we consider the following aspects:";
$informacion_consejos_termo7="<strong>The amount of water necessary.</strong> It is necessary to choose a flask whis is able to satisfy the water demand according to the number of family members, the sanitary facilities and the individual habits.<br /><br />
<strong>The water composition. </strong>It is very important that the  boiler is suitable according to water composition of the area, because if it is very aggressive or it contains mineral salt excess can erode the boiler walls. The boiler or accumulator container can be of enameled steel veneer, or galvanized or veneer steel, if water of the city is very aggressive, it is convenient that the device is provided by an anode magnesium. This element prevents the corrosion of the inside walls of the boiler.<br /><br />
<strong>Type and espesor of the isolation.</strong> Can be interesting to highlight that the consumed energy in order to heat and keep the same amount of water to the same temperature will vary according to the quality of the isolation.";
$informacion_consejos_termo8="Installation:";
$informacion_consejos_termo9="The location of the device. It can be installed anywhere: attics, under kitchen sinks, on false ceilings, etc... They don´t need ventilation or gases chimneys or exits. Only should take the precaution of leaving accessible the part of the flask in whose hinterland has to carry out some maintenance or repair operation.
If use points of the hot water are distant, it is preferable to use two or more devices, to avoid heat losses in the pipes.<br /><br />
When the flask installation is carried out in bathrooms, will have to respect the Low Voltage electrotechnical Regulation. Electric flask should be out of the ban volume, in order to avoiding that the water splashes inside the connections box of the device.<br /><br />
It should used a plug base for the electrical connection of the flask of 16A, with ground connection. It's recommended to install a bipolar cut switch, which allows the easy disconnection of the flask, at the time of using the bathroom or shower.<br /><br />";
$informacion_consejos_termo10="Use Recommendations:";
$informacion_consejos_termo11="When your electric flask has been installed flask it is recommended to follow some simple indications, when using, that will provide you a better service quality as well as will help you to preserve your equipment.
To achieve this, we offer you some practical advices that you must respect when you uses your electric flask.";
$informacion_consejos_termo12="If the hot water which the flask accumulates during the night results insufficient for all the day, you can choose one or both of the two following solutions:<br /><br />
- Increasing the warming temperature of the flask activating its thermostat. User´s manual shows where is located and how to use it.<br /><br />
- Adjusting the programer in order to the flask also can connect during the day, but just during the minimum time  necessary to cover hot water needs.<br /><br />
Checking that the temperature of the hot water doesn´t exceed 60ºC, that is suitable for the security of the people and to make larger the device´s life. That way we achieve to prevent the corrosion and to decrease the calcification in 50%.<br /><br />
Eliminating deposit´s inlaies, as well as security valve ones, when it is observed that the hot water volume flow has considerably decreased.";
$informacion_consejos_termo13="Maintenance:";
$informacion_consejos_termo14="It it is convenient to consult the user´s manual so that to achieve the maximum duration of the equipment with a minimum maintenance.<br /><br />
Demanding have at hand the guarantee of the flask. Its vigilance period covers all repairs that can be produced occasionally.<br /><br />
If the water is mixed, first, you should leave to flow  the cold water and later mix with hot water, until reaching the wished temperature.
";
$informacion_consejos_termo15="Estimated Energy´s Consumption:";
$informacion_consejos_termo16="Task";
$informacion_consejos_termo17="Have a shower (30 litres to 40ºC)";
$informacion_consejos_termo18="Have a bathroom (90 litres to 40ºC)";
$informacion_consejos_termo19="To wash the dishes (15 litres to 40ºC)";
$informacion_consejos_termo20="Practical Advices";
$informacion_consejos_termo21="During summer, when it is not necessary that the water flow so hot, you should reduce the temperature activating the thermostat.";
$informacion_consejos_termo22="In prolonged absences, higher than three or four days, disconnect the device. If the absence is short, it is preferable to reduce the temperature of the thermostat and to keep connected the flask.";
$informacion_consejos_termo23="If you install temperature regulators with thermostat you will save between 4% and 6% of energy in the shower.";
$informacion_consejos_termo24="The recommended temperature to enjoy a nice sensation in the personal cleanliness is from 30º to 35º.";
$informacion_consejos_termo25="Keeping taps in good condition in order to they dont´t drip, close them completely after its use and you should repair them if it has leaks of water. A tap that drips can spend 170 litres of water at month.";
$informacion_consejos_termo26="You have to consider that the third part of the energy that is consumed in a bathroom, is consumed in the shower.";
$informacion_consejos_termo27="You shouldn´t use more water hot of the necessary one. For example, you shouldn´t let the water flow while you are washing hands or teeth.";
$informacion_consejos_termo28="You should buy low-energy shower headrests, it allows a comfortable cleanliness spending the 50% of water and energy.";
$informacion_consejos_termo29="You can place in the taps reducers of flow rate.";
?>