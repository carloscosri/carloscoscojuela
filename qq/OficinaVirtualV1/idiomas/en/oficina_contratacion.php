<?php
$submenu_contratacion_oficina1="Fares to contract:";
$submenu_contratacion_oficina2="- Social Contract (Lower than 3 kWh).";
$submenu_contratacion_oficina3="- Contracts until 10 kWh with or without hourly discriminacion (Fares 2.0.to and 2.0 DHA).";
$submenu_contratacion_oficina4="- Contracts from 10 to 15 kWh with or without hourly discriminacion (Fares 2.1.to and 2.1 DHA).";
$submenu_contratacion_oficina5="- Contracts from 15 kWh (Fares 3.0.To and 3.1.To and 6.x).";
$oficina_contratacion1="Administrative requirements to register a new provision point:";
$oficina_contratacion2="NOTE: For any further information on fares to be contracted and type of provision, you should consult directly in our branches.";
$oficina_contratacion3="Terms and Conditions for the Social Contract:";
$oficina_contratacion4="The Social Contract is only fof aplicacion to individuals whose provision pair is destined to use in
normal residence and that belongs to some next associations:<br /><br />
Individuals with a contracted power lower than 3kW in their normal residence. Social Contract will be applied 
 automatically.<br /><br />
Clients with minimum age of 60 years that perceive a minimum pension.<br /><br />
Clients of more than 60 years that perceive non contributory pensions of retirement and disability, as well as
recipient of pensions of the extinguished Old age Statutory Insurance and disability.<br /><br />
Large families.<br /><br />
Families with all his members in situation of unemployment.<br /><br />";
$oficina_contratacion5="Land Registry Reference Number.";
$oficina_contratacion6="CUPS (Identifier Code of your Provision Point) in the supposition that you come from another Trading company.";
$oficina_contratacion7="This application of new provision will have merely informative character.In the next few days our company will contact you to present you an economic customised offer, via email or postal mail.<br /><br />
Once accepted the above-mentioned budget will be when we prune to finish the contract of electrical energy provision with our company in the point of requested provision and conditions.<br />
So that you can receive this information should fill in the form corresponding to the power that wishes to contract, according to groups that below are broken down.";
$oficina_contratacion8="Property or rental contract title.";
$oficina_contratacion9="Covered bond of habitability.";
?>