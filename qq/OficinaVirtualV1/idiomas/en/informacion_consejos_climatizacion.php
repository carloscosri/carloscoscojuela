<?php
$informacion_consejos_climatizacion1="Advices to Save Energy - Air-conditioning";
$informacion_consejos_climatizacion2="The air-conditioning is the treatment process of the air that allows control the temperature, the humidity, the movement and the cleanliness of the air in the hinterland of a local. The values of these parameters influences directly on the comfort, and they vary with the period of the year.<br /><br />
The Heat pump is the only system that covers all needs of cold in summer and of heat in winter in a house, whereupon we obtain a considerable saving in the initial investment, because we don´t need to buy devices for cold and devices for heat.<br /><br />
We should keep in mind the fact that to apart of this advantage it has an high output, because it makes good use of the existing energy on the air ambient.";
$informacion_consejos_climatizacion3="This system allows us:";
$informacion_consejos_climatizacion4="1. Controlling the temperature.<br />
2. Controlling the movement of air.<br />
3. Eliminating impurities of the air.";
$informacion_consejos_climatizacion5="The Heat pump is clean, and it doesn´t produce smokes or smells because there is not makes any type of combustion, also the possibility of the air's renewal according to the type of device.";
$informacion_consejos_climatizacion7="When installing an air-conditioning system should seek the counseling of a specialized installer.<br /><br />
To obtain the maximum comfort is essential that the calculation of the necessary frigories / calories is carried out with enough reliability. To do this, the installer will keep in mind the house´s characteristics: orientation, constructive details, heat sources, etc. Distribution system of the air should be suitable in order to heat and cold could be distributed uniformly.In order to ventilate rooms is necessary that the air-conditioning system has an outside air intake.";
$informacion_consejos_climatizacion9="Air-conditioning systems for Heat pump, are controlled through a centralized command. Its use is simple and easy, just it is necessary to select:";
$informacion_consejos_climatizacion8="Recommendations of use:";
$informacion_consejos_climatizacion10="- The wished temperature from the thermostat.<br />
- The use way: cold, heat or ventilation.<br />
- The ventilator speed.";
$informacion_consejos_climatizacion11="There are some models that take built in a hourly programer of starting up.";
$informacion_consejos_climatizacion12="Maintenance:";
$informacion_consejos_climatizacion13="The internal unit that cools or heats the air, has a filter, that serves to clean the air of the house. The only precaution that it is necessary to have, is keep up clean, following the manufacturer's indications.";
$informacion_consejos_climatizacion14="Practical Advices:";
$informacion_consejos_climatizacion15="Recommended comfort temperatures are 25º in summer and between 18 and 20º in winter.";
$informacion_consejos_climatizacion16="The adjustment of the temperature is very important to control the consumption, for example: setting a temperature in winter of 20º instead of 21º can mean an energy saving of 10%; in summer, on the other hand, for every degree of temperature  under 25º, you will be increasing the consumption.";
$informacion_consejos_climatizacion17="You can reduce the warming of your house in summer placing awnings, closing blinds and curtains.";
$informacion_consejos_climatizacion18="In summer it is convenient to ventilate the house when the air of the street is colder: night and early hours in the morning.";
$informacion_consejos_climatizacion19="Another way of avoiding internal space warming is to paint roofs and exterior walls with clear colors to reflect the solar radiation.";
$informacion_consejos_climatizacion20="You should place the climatizer in a place where there aren´t sun light and where has good air circulation.";
?>