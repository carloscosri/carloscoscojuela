<?php
$titulo1="SPECIAL CONDITIONS OF THE SUPPLIES OF ELECTRICAL ENERGY CONTRACT ";
$datos_cliente="CUSTOMER´S DETAILS";
$nombre_razon_social="Name/Registered Name";
$cif_nif="CIF / ID number";
$direccion="Address";
$num="No";
$cp_poblacion="ZIP CODE / Town";
$provincia_pais="Province / Country";
$telefono="Phone";
$fax="Fax";
$email="E-Mail";
$datos_contacto="CONTACT DETAILS (bill mailing, other communications... only Omit if it differs of the customer's details)";
$persona_contacto="Contact person";
$titulo2="TECHNICAL-ECONOMICS CONDITIONS";
$datos_suministro="DETAILS OF THE POINT OF PROVISION (only Omit if it differs of the client's details)";
$ref_catastral="Cadastral Ref.";
$contrato_num="Contract No";
$empresa_distribuidora="Distributor company:";
$tension_nominal="Nominal Tension (V):";
$tarifas_acceso="ACCESS RATES (Defined in R.D. 1164/2001 of 26 October 2001)";
$tarifa="Fare:";
$potencia_contratada="Contracted Power (kW):";
$punta="Tip";
$llana="Unvarnished";
$valle="Valley";
$producto_contratado="CONTRACTED PRODUCT";
$producto="Product:";
$datos_pagador="PAYER´S DETAILS";
$titular_cuenta="Account holder:";
$nif="ID number";
$entidad_financiera="Financial Institution";
$sucursal="Branch office";
$digito_control="CD";
$num_cuenta="Account No";
$explicacion_contrato1="These conditions of the Provision Contract, together with Generals that are found on the back of the current document, as the price annexe that is attached, have been read by the client who, being in agreement with the content of the same the subscribe it.<br /><br />

For this purpose, authorizes expressly to ";
$explicacion_contrato2=", to that contracts with the distributor access company to third parties to the network (ATR), in the indicated ends to the General Condition 1a., to end and effect according to which ";
$explicacion_contrato3="pass to be his new supplier.<br /><br />

The client all the same declares that reflected details and information in the current conditions are more complete and verídicas authorizing to the company to carry out checkouts of the same.<br /><br />
";
$a="";
$de="of";
$enero="January";
$febrero="February";
$marzo="March";
$abril="April";
$mayo="imprimir_contrato";
$junio="June";
$julio="July";
$agosto="August";
$septiembre="September";
$octubre="October";
$noviembre="November";
$diciembre="December";
$el_cliente="THE CUSTOMER";
$fdo="Signed";
$anexo_precios="PRICE ANNEXE";
$datos_suministro_sin_omitir="DETAILS OF THE POINT OF PROVISION";
$termino_potencia="POWER TERMS";
$precio_por_periodo="Price for period /kW/month";
$explicacion_anexo_precios1="It is applied to the invoicing price and conditions of the potency end of access rate 3.0A in force.";
$precio_energia="PRICE OF ENERGY";
$explicacion_anexo_precios2="The price of energy adds the price of the access rate 3.0A in force, to apply on the consumed energy in the hourly period corresponding.";
$calendario_periodos="PERIOD APPLICATION CALENDAR";
$invierno="Winter";
$verano="Summer";
$explicacion_anexo_precios3="Currently established in the second point 3 of the 2ND Annexe Order ITC 2794/2007. The winter timetable change to summer and vice versa will coincide with the date of the official rate of time.";
$energia_reactiva="Reactive Energy";
$explicacion_anexo_precios4="It is applied to invoicing prices and conditions of the reactive energy established by the access rate 3.0A in force (currently R.D. 1164/2001). It will be applied to all pricing periods, except to the period valley, provided that the reactive power consumption exceeds 33% of the consumption of the active energy during the considered invoicing period.";
$complementos="COMPLEMENTS";
$equipos_medida="EQUIPMENTS OF MEASURE";
$alquiler_equipo="Rent of the equipment";
$euros_mes="/month";
$otros_alquileres="Other rents";
$explicacion_anexo_precios5="Subject price to modifications according to the willingness that follows of application.";
$explicacion_anexo_precios6="Invoicing of the access rates according to regimentation in force in every moment.<br />
Each of the prices included in this annexe bring built in corresponding taxes except I.And and I.V.A.";
?>