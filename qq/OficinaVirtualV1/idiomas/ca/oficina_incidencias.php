<?php
$error_rellenar
="Han d\'emplenar-se els camps marcats com a obligatoris.";
$error_fecha="La data s\'ha escrit de forma incorrecta. Ha d\'utilitzar el següent format: 18-03-2009.";
$error_hora="L\'hora i els minuts han de ser 2 caràcters numèrics entre 00 i 23 i entre 00-59 respectivament.";
$error_telefonos="Els camps Teléfonom, Mòbil i Fax sol poden contenir caràcters numèrics.";
$error_mail="No ha escrit l\'i-correu electrònic correctament.";
$incidencia_ok="La seva incidència ha estat enviada.";
$error_fecha_posterior="Les dates escrites no poden ser posteriors a la data actual.";
?>