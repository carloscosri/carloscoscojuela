<?php
$informacion_consejos_termo1="Consells per Estalviar Energia - Escalfador";
$informacion_consejos_termo2="Escalfador elèctric per acumulació nocturna:";
$informacion_consejos_termo3="El disposar d'aigua calenta forma part del mínim confort. El seu funcionament és senzill: l'aigua freda de la xarxa general, entra per la part inferior de lescalfador i és escalfada a l'interior de l'aparell per mitjà d'una resistència elèctrica, que funciona en horari nocturn, fins a una temperatura aproximada de 60ºC, valor que es pot regular mitjançant termòstat. Per aconseguir el seu objectiu ha de reunir les següents condicions:";
$informacion_consejos_termo4="1. L'aigua ha destar disponible en qualsevol moment.<br />
2. La quantitat d'aigua ha de ser suficient per cobrir la demanda de l'habitatge.<br />
3. L'aigua ha de ser subministrada a la temperatura desitjada.<br />
4. El consum energètic ha de realitzar-se durant la nit, quan l'electricitat resulta un 53% més barata.";
$informacion_consejos_termo5="Consells per a la Compra de lEscalfador Elèctric:";
$informacion_consejos_termo6="Existeixen en el mercat una àmplia gamma descalfadors electrònics. Amb lobjecte d'obtenir el màxim rendiment, s'ha de seleccionar adequadament l'aparell que es vol adquirir. Per a això cal prestar atenció als següents aspectes:";
$informacion_consejos_termo7="<strong>La quantitat d'aigua necessària.</strong> S'ha d'escollir un escalfador que sigui capaç d'atendre la demanda d'aigua segons el nombre de membres de la família, de les instal·lacions sanitàries i dels hàbits individuals.<br /><br />
<strong>La natura de l'aigua.</strong> És molt important que lescalfador sigui l'adequat al tipus d'aigua de la zona, ja que si aquesta és molt agressiva o conté excés de sals minerals pot corroir les seves parets. Lescalfador o recipient acumulador pot ser de xapa d'acer esmaltat, d'acer galvanitzat o esmaltat, i l'aigua de la ciutat és molt agressiva, és convenient que l'aparell estigui proveït d'un ánodo magnesio. Aquest element prevé la corrosió de les parets interiors del calderín.<br /><br />
<strong>Tipus i gruix de l'aïllament.</strong> És interessant ressaltar que l'energia consumida per escalfar i mantenir una mateixa quantitat d'aigua a una mateixa temperatura variarà segons la qualitat de l'aïllament.<br /><br />";
$informacion_consejos_termo8="Instal·lació:";
$informacion_consejos_termo9="L'emplaçament de l'aparell. Pot instal·lar-se a qualsevol lloc: altells, sota de les aigüeres, sobre falsos sostres, etc... No necessiten ni ventilació ni xemeneies ni sortida de gasos. Únicament haurà de prendre's la precaució de deixar accessible la part de lescalfador, a linterior de la qual shagi de realitzar-se alguna operació de manteniment o reparació.
Si els punts d'utilització de l'aigua calenta estan llunyans, és preferible utilitzar dos o més aparells, per evitar pèrdues de calor en les canonades.<br /><br />
Quan la instal·lació de lescalfador es realitzi en banys, s'haurà de respectar el més indicat en el Reglament Electrotècnic de Baixa Tensió. El termos elèctric haurà d'estar fora del volum de prohibició, amb lobjecte d'evitar que l'aigua esquitxi l'interior de la caixa de connexions de l'aparell.<br /><br />
Per a la connexió elèctrica de lescalfador haurà d'utilitzar-se una base d'endoll de 16 A, amb presa de terra. És aconsellable instal·lar un interruptor de tall bipolar, que permet la fàcil desconnexió del termos, al moment d'utilitzar el bany o dutxa.";
$informacion_consejos_termo10="Recomanacions d'ús:";
$informacion_consejos_termo11="Una vegada instal·lat lescalfador elèctric és aconsellable seguir algunes indicacions senzilles, a l'hora d'utilitzar-ho, que li proporcionaran una millor qualitat de servei a la vegada que ajudaran a la conservació del seu equip.
Per tal fi, li oferim a continuació una sèrie de consells pràctics que vostè haurà de tenir en compte quan usi el seu escalfador elèctric.";
$informacion_consejos_termo12="Si l'aigua calenta que acumula el termos durant la nit resulta insuficient per a tot el dia, es pot optar per una, o per ambdues, de les dues solucions següents:<br /><br />
- Augmentar la temperatura d'escalfament de lescalfador accionant el seu termòstat. El manual d'instruccions mostra on està ubicat i com manejar-ho.<br /><br />
- Ajustar el programador perquè lescalfador pugui connectar-se també de dia, però només durant el mínim temps que calgui per cobrir les necessitats d'aigua calenta.<br /><br />
Comprovar que la temperatura de l'aigua calenta no sobrepassi els 60ºC, que és l'adequada per a la seguretat de les persones i per a una més gran durada de l'aparell. D'aquesta manera s'aconsegueix prevenir la corrosió i disminuir la calcificació en un 50%.<br /><br />
Eliminar les incrustacions del dipòsit, així com de la vàlvula de seguretat, quan s'observi que el cabal d'aigua calenta ha disminuït considerablement.";
$informacion_consejos_termo13="Manteniment:";
$informacion_consejos_termo14="És convenient consultar el manual d'instruccions perquè amb un mínim de manteniment aconseguir la màxima durada de l'equip.<br /><br />
Exigir tenir a mà la garantia de lescalfador. El període de vigilància de la mateixa cobreix la reparació de les avaries que, ocasionalment, puguin produir-se.<br /><br />
Si es barreja l'aigua, deixar sortir primer l'aigua freda i després barrejar amb aigua calenta, fins a assolir la temperatura desitjada.";
$informacion_consejos_termo15="Consum Aproximat d'energia: ";
$informacion_consejos_termo16="Tasca";
$informacion_consejos_termo17="Una dutxa (30 litres a 40ºC)";
$informacion_consejos_termo18="Un bany (90 litres a 40ºC)";
$informacion_consejos_termo19="Un rentat a mà de vaixella (15 litres a 40ºC)";
$informacion_consejos_termo20="Consells Pràctics";
$informacion_consejos_termo21="Durant els mesos d'estiu, quan no cal que l'aigua surti tan calent, reduïu la temperatura accionant el termòstat.";
$informacion_consejos_termo22="En absències prolongades, superiors a tres o quatre dies, desconnecteu l'aparell. Si l'absència és curta, és preferible reduir la temperatura del termòstat i deixar connectat lescalfador.";
$informacion_consejos_termo23="Estalviï en la dutxa entre un 4% i un 6% d'energia amb els reguladors de temperatura amb termòstat.";
$informacion_consejos_termo24="La temperatura aconsellada per gaudir d'una agradable sensació en el lavabo personal és de 30è a 35è.";
$informacion_consejos_termo25="Manteniu les aixetes en bon estat perquè no degotin, tanqui-les completament després del seu ús i repari de seguida les que presentin fugues d'aigua. Una aixeta que goteja pot gastar al mes 170 litres d'aigua.";
$informacion_consejos_termo26="Tingui en compte que en una dutxa es consumeix la tercera part de l'energia que es consumeix en un bany.";
$informacion_consejos_termo27="No utilitzi més aigua calenta de la necessària. Per exemple, no deixi l'aixeta oberta mentre s'ensabona o es renta les dents.";
$informacion_consejos_termo28="Adquiriu capçals de dutxa de baix consum, permeten un lavabo còmode gastant la meitat d'aigua i d'energia.";
$informacion_consejos_termo29="Pot col·locar a les  aixetes reductors de cabal (airejadors).";
?>