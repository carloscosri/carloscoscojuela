<?php
/*
NOTAS:

1- SE COMPRUEBAN LOS DATOS DEL LOGIN Y SE LE DA VALOR A LAS VARIABLES DE SESION EN VALIDA_LOGIN.PHP

2- EL OLVIDO DE LA CONTRASEÑA LLAMA A UNA SECCION CON UN FORMULARIO olvido_password.php

*/

//NOTA: NO SE PUEDE INCLUIR EN EL ARCHIVO DE CONEXION, PORQUE ENTONCES LE DARIAMOS EL VALOR A LA VARIABLE DE SESION Y SE PERDERIA, DEBIDO A LAS 
//DIFERENTES RUTAS RELATIVAS DEPENDIOENDO DE SI SE LLAMA DESDE LA WEB O LA OFICINA VIRTUAL


///Añadimos el archivo que contiene el inicio de la sesion y las variables de sesion.  Es importante añadir
//este archivo antes de las cabeceras del documento o dara error
include("../includes/sesion.php");

//Una vez con la sesion iniciada, se puede dar valor a la variable de sesion que indica si nos encontramos en la oficina virtual
$_SESSION["oficina_virtual"]=1;

//Añadimos la libreria de conexion a base de datos, que a su vez contiene las referencias a todos los archivos necesarios
include("../includes/conexion.php");

//Se incluye el archivo con los textos especifico de esta seccion en el idioma oportuno
//include($_SESSION["directorio_raiz"]."idiomas/".$_SESSION["idioma"]."/oficina_virtual_login.php");
include("../idiomas/".$_SESSION["idioma"]."/oficina_virtual_login.php");

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<title><?=$titulo_web;?></title>
<link rel="apple-touch-icon" sizes="57x57" href="/apple-touch-icon-57x57.png">
<link rel="apple-touch-icon" sizes="114x114" href="/apple-touch-icon-114x114.png">
<link rel="apple-touch-icon" sizes="72x72" href="/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="144x144" href="/apple-touch-icon-144x144.png">
<link rel="apple-touch-icon" sizes="60x60" href="/apple-touch-icon-60x60.png">
<link rel="apple-touch-icon" sizes="120x120" href="/apple-touch-icon-120x120.png">
<link rel="apple-touch-icon" sizes="76x76" href="/apple-touch-icon-76x76.png">
<link rel="apple-touch-icon" sizes="152x152" href="/apple-touch-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon-180x180.png">
<meta name="apple-mobile-web-app-title" content="Multienergia | Ahorra Luz y Gas">
<link rel="icon" type="image/png" href="/favicon-192x192.png" sizes="192x192">
<link rel="icon" type="image/png" href="/favicon-160x160.png" sizes="160x160">
<link rel="icon" type="image/png" href="/favicon-96x96.png" sizes="96x96">
<link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
<link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
<meta name="msapplication-TileColor" content="#ff0000">
<meta name="msapplication-TileImage" content="/mstile-144x144.png">
<meta name="application-name" content="Multienergia | Ahorra Luz y Gas">
<!--Se añaden las hojas de estilos genericas que utiliza esta seccion-->
<link href="../css/estilos.css" rel="stylesheet" type="text/css" />
<link href="../css/secciones.css" rel="stylesheet" type="text/css" />
<link href="css/oficina.css" rel="stylesheet" type="text/css" />
<link href="css/oficina_login.css" rel="stylesheet" type="text/css" />

<!--Ahora se añaden las reglas para Internet Explorer. Asi corregiremos las diferencias de visualizacion entre navegadores
Es importante añadirlo despues de las reglas principales, para que se sobreescriban las correcciones pertinentes. En este caso las diferencias
solo son para internet explorer 7-->

<!--[if IE 8]>
	<link href="../css/secciones_ie8.css" rel="stylesheet" type="text/css" />    
<![endif]-->

<!--[if IE 7]>
	<link href="../css/estilos_ie7.css" rel="stylesheet" type="text/css" />
	<link href="../css/secciones_ie7.css" rel="stylesheet" type="text/css" />    
<![endif]-->

</head>
	
<body>
<!--*************************************************Web realizada por ****************************************-->
	<div id="central_login_oficina">
<!--Añadimos los fragmentos comunes, la cabecera, el menu, y el submenu-->    
		<div>
<?php
        	//include($_SESSION["directorio_raiz"]."includes/cabecera.php");
			include("../includes/cabecera.php");            
?>
        </div>                     
          
   		<div class="posicion_menu">
        
<?php        
		 	//include($_SESSION["directorio_raiz"]."includes/menu.php");
			include("../includes/menu.php");
?>			

        
        <div class="contenido_seccion_oficina_index">
       

	      <div class="posicion_login">
       <div id="tituloLogin">
              			<img src="../images/titulo_oficina.png" alt="TituloOficina"/>
                 	</div>
        	<form id="login" name="login" action="oficina_valida_login.php?id=<?=$_SESSION["idioma"]?>" method="post">
            
	            <div class="componente_login"><?=$oficina_virtual_login1?></div>
                
              <div class="componente_login">	                    
                <input id="usuario" name="usuario" type="text" size="20" maxlength="15">
              </div>
                
                <div class="componente_login"><?=$oficina_virtual_login2?></div>
                 
                 <div class="componente_login">
	                 <input id="password" name="password" type="password" size="20" maxlength="15">
                </div>                
                                
                <div class="boton_login">  
                	<input type="submit" name="btn_aceptar" value="<?=$oficina_virtual_login6?>" />
                </div>
                
                <div class="olvido_contrasena_login">

					<a href="oficina_recordar_contrasena.php?id=<?=$_SESSION["idioma"]?>" class="enlace"><?=$oficina_virtual_login3?></a>
                </div>                
                                                
        </form>
		</div><!--<div class="posicion_login">-->
        <div class="banner_sigeenergia">
        
    <div class="banner_sigeenergia_imagen" style="float: left; margin-bottom: 10px;">
<?php
//La ruta de la imagen si se accede desde la oficina virtual
		if($_SESSION['oficina_virtual']!=1)
		{
?>
				<a href="http://www.multienergia.es" target="_blank"><img src="img/comun/banner_multienergia.jpg" border="0" alt="volver a la web corporativa" title="volver a la web corporativa" /></a>
<?php
		}//if($_SESSION['oficina_virtual']!=1)
		else
		{	
?>
			<a href="http://www.multienergia.es" target="_blank"><img src="../img/comun/banner_multienergia.jpg" border="0" alt="volver a la web corporativa" title="volver a la web corporativa" /></a>
<?php	
		}//else($_SESSION['oficina_virtual']!=1)
?>
	</div><!--<div class="banner_sigeenergia_imagen">-->
    
	<div>
    	<a href="http://www.multienergia.es" target="_blank" class="banner_sigeenergia_texto"><?=$banner_sigeenergia?><br />www.multienergia.es</a>
	</div>
        
</div><!--<div class="banner_sigeenergia">-->            
        </div><!--<div class="posicion_menu">-->
      
        
        <div class="limpiar"> </div>
        
        <div class="enlace_no_registrado">
       	  <fieldset>
          <br />
Para <strong>optimizar</strong> el visionado de la Oficina Virtual, se recomienda utilizar los exploradores:<br />
<a href="http://windows.microsoft.com/es-ES/internet-explorer/products/ie/home" target="_blank" class="enlace_nota_pdf"><img src="../img/oficina/ie.png" width="24" height="23" border="0" align="absmiddle" /> Internet Explorer 10 o superior</a>, o <a href="http://www.mozilla.org/es-ES/firefox/new/" target="_blank" class="enlace"><img src="../img/oficina/mf.png" width="22" height="21" border="0" align="absmiddle" /> Mozilla Firefox 30 o superior.</a>
<br />
<br />
          </fieldset>
           </div>
        
		</div><!--<div class="contenido_seccion">-->                        
                	
<!--Por ultimo aparecera el pie de la web-->
	<?php include("../includes/pie.php");?>        
        
        
    </div><!--<div id="central_login_oficina"-->
</body>
</html>
