<?php
$error_rellenar="Ha d\'escriure la data de la lectura i un nombre de comptador.";
$error_fecha="La data s\'ha escrit de forma incorrecta. Ha d\'utilitzar el següent format: 18-03-2009.";
$error_lectura="Ha d\'escriure almenys una lectura.";
$lectura_ok="La seva lectura ha estat enviada.";
$oficina_lecturas1="Lectura d'un sol Període Tarifari";
$oficina_lecturas2="Lectures amb Diversos Períodes Tarifaris";
$oficina_lecturas3="Període";
$oficina_lecturas4="P1 Punta";
$oficina_lecturas5="P2 Pla";
$oficina_lecturas6="P3 Vall";
$oficina_lecturas7="Activa";
$oficina_lecturas8="Reactiva";
$oficina_lecturas9="Maxímetro";
$error_varias_lecturas="Si ha escrit lectures en l'apartat 'Lectura d'un Sol Període Tarifari' no s'han d'escriure lectures en l'apartat 'Lectures amb Diversos Períodes Tarifaris'.";
$error_numero_lectura="No ha escrit correctament alguna lectura. Han de ser com a màxim uns nombres amb 8 sencers i 3 decimals.";
$error_fecha_posterior="Les dates escrites no poden ser posteriors a la data actual.";
?>