<?php
$contratacion_contratar_motivos1="Motius per contractar amb ";
$contratacion_contratar_motivos2="La nostra filosofia és donar el màxim en tot el que està al nostre abast: qualitat de subministrament, atenció, assessorament, inspeccions, seguretat, innovació...";
$contratacion_contratar_motivos4="Tindrà a la seva disposició a un gran equip de tècnics i professionals disposats a atendrel en qualsevol moment.";
$contratacion_contratar_motivos3="Li garantim tota la nostra experiència d'una empresa que durant anys ha subministrat energia elèctrica.";
$contratacion_contratar_motivos5="Les nostres oficines, magatzems i tècnics es troben a la nostra ciutat i, en cas de necessitat, estem a prop de vostè per atendrel.";
$contratacion_contratar_motivos6="Els beneficis que genera l'empresa es queden i inverteixen, de forma majoritària, en la nostra ciutat.";
$contratacion_contratar_motivos7="Tenim un compromís amb vostè i amb el món que ens envolta, ja que un estalvi energètic no és només bo per a la seva butxaca, sinó també per a la natura. ";
?>