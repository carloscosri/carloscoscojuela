<?php
$informacion_seguridad1="Consells per a la Seguretat de la seva Llar";
$informacion_seguridad2="Seguretat i Protecció de la seva instal·lació elèctrica";
$informacion_seguridad3="Qualsevol habitatge ha de tenir la Taula de Control i Protecció. Si el seu habitatge és de nova construcció, no es preocupi, ja ho té instal·lat. Però si és antiga, asseguri's de què disposa d'un, ja que és important per a la seguretat de la seva llar i els que viuen en ell.<br /><br />Les proteccions incloses en la Taula de control i Protecció són les següents:";
$informacion_seguridad4="1. Interruptor de Control de Potència (ICP)";
$informacion_seguridad5="És un interruptor automàtic que determina la potència simultània disponible d'acord amb el Contracte d'energia elèctrica.L'ICP desconnectarà la seva instal·lació quan la suma de la potència dels aparells elèctrics connectats a la vegada, superi la potència contractada.D'aquesta forma s'eviten riscos innecessaris a la seva instal·lació.";
$informacion_seguridad6="2. Interruptor General de Tall (IGC)";
$informacion_seguridad7="Aquest interruptor permet la desconnexió total de la seva instal·lació en cas necessari. També realitza la funció de protecció de la seva instal·lació, de manera que mai no es sobrepassi la potència màxima admissible de la seva instal·lació.";
$informacion_seguridad8="3. Interruptor Diferencial (ID)";
$informacion_seguridad9="És l'element de protecció més important de la Taula de control i Protecció, ja que protegeix les persones de qualsevol accident elèctric. L'interruptor diferencial desconnecta la seva instal·lació quan detecta una derivació en algun aparell electrodomèstic o en algun punt de la installació.En habitatges, aquests interruptors diferencials, han de ser d'alta sensibilitat (30 mA.)";
$informacion_seguridad10="4. Petits interruptors magnetotèrmics (PIA)";
$informacion_seguridad11="Qualsevol instal·lació elèctrica està formada per diversos circuits (enllumenat, força, cuina, etc...) i cadascun d'aquests circuits han d' estar protegits per sobrecàrregues i curtcircuits per un interruptor magnetotèrmic, d'acord amb la capacitat de cadascun dels circuits interiors.";
$informacion_seguridad12=" li recomana:";
$informacion_seguridad13="Si ha de realitzar un nou contracte de subministrament, modifiqueu o substitueixi la seva Taula de Control i Protecció d'acord amb la legislació vigent per assegurar la seva instal·lació domèstica davant riscos elèctrics. Així vetllarà per la seva seguretat i la dels seus familiars.<br /><br />Instal·li un interruptor Diferencial de 30 mA. De suficient sensibilitat per evitar talls en el subministrament de la seva instal·lació (en cas d'un funcionament inadequat d'alguns dels electrodomèstics)";
$informacion_seguridad14="Legislació";
$informacion_seguridad15="Les normes i condicions que han de reunir les instal·lacions elèctriques de Baixa Tensió van ser definides pel Ministeri d'Indústria i Energia i publicat al Reial decret 842/2002, i estan reunides en el ";
$informacion_seguridad16="Reglament Electrotècnic de Baixa Tensió";
?>