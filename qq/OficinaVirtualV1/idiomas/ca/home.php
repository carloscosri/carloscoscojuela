<?php
$home1="Comercialització d'Energia Elèctrica";
$home2="Secció Empresa";
$home3="Secció Contraració";
$home4="Secció Informació al client";
$home5="Secció Informació al client";
$home6="Portal Sige Energia";
$home7="Aprèn que ens dediquem, on trobar-nos, quins serveis podem oferir-te i, si el vols posa't, en contacte amb nosaltres.";
$home8="Informa't de les possibles fórmules de contractació, les nostres tarifes, etc.";
$home9="Aquí trobaràs qualsevol tipus de informació de tu interès. Fins i tot uns bons consells perquè estalviïs energia!";
$home10="Des de l'oficina virtual, els nostres clients poden accedir de forma privada a totes les seves dades des de qualsevol equip informàtic.";
$home11="Accedeix al nou portal www.sigeenergia.com, on es centra tota la informació del sector energètic.";
?>