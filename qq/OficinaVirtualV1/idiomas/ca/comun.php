<?php
$titulo_web="AUDINFOR SYSTEM - Oficina Virtual";
$pie_imagen="AUDINFOR SYSTEM S.L.";
$pie="Lloc Web desenvolupat per Audinfor System S.L.";
$w3c_html="Validació W3C HTML";
$w3c_css="Validació W3C CSS";
$menu1="Empresa";
$menu2="Ofertes i Contractació";
$menu3="Informació al Client";
$menu4="Oficina Virtual";
$menu5="Portal Sige Energia";
$datos_empresa="AUDINFOR SYSTEM, SL | C/ PANAMA 2 LOCAL 2 ZARAGOZA Tel. 976312018";
$menu3_2="Informació<br />al Client";
$menu_idiomas1="Canviar idioma a Espanyol";
$menu_idiomas2="Canviar idioma a Catalan";
$menu_idiomas3="Canviar idioma a Euskera";
$menu_idiomas4="Canviar idioma a Gallego";
$menu_idiomas5="Canviar idioma a Anglès";
$idioma_activo="Idioma actiu";
$error_identificacion="Aquesta seccion sol és per a usuaris identificats.";
$nota_pdf1="* Aquesta documentació està en format PDF, per visualitzar aquests arxius, necessita tenir instal·lat el programa Adobe Acrobat Reader, si no ho té pot baixar aquest programa (gratuït) des d'aquí:";
$nota_pdf2="Baixar Adobe Acrobat Reader";
$poblacion_empresa="ZARAGOZA";
$banner_sigeenergia="Accés al Portal";
$nota_campos_obligatorios="ATENCIÓ: Només els camps assenyalats amb * són obligatoris.";
$tlf_averias="";
?>