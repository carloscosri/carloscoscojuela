<?php
$contratacion_legislacion3="ORDRE ITC 3860/2007";
$contratacion_legislacion4=", de 28 de desembre, per la qual es revisen les tarifes elèctriques a partir de l'1 de gener de 2008.";
$contratacion_legislacion5="REIAL DECRET 385/2002";
$contratacion_legislacion6=", de 26 d'abril, pel qual es modifica el Reial decret 2018/1997, de 26 de desembre, pel qual s'aprova el reglament de punts de mesura dels consums i trànsits d'energia elèctrica. (BOR 14-05-02).";
$contratacion_legislacion7="REIAL DECRET 1955/2000";
$contratacion_legislacion8=", d'1 de desembre, pel qual es regulen les activitats de transport, distribució, comercialització, subministrament i procediments d'autorització d'instal·lacions d'energia elèctrica. (BOE 27-12-00).";
$contratacion_legislacion9="REIAL DECRET-LLEI 6/2000";
$contratacion_legislacion10=", de 23 de juny, de Mesures Urgents d'Intensificació de la Competència en Mercats de Béns i Serveis. (BOE 24-06-00).";
$contratacion_legislacion11="LLEI 54/1997";
$contratacion_legislacion12=" , de 27 de novembre, del Sector Elèctric. (BOE 28-11-97).";
$contratacion_legislacion1="Legislació Elèctrica Espanyola";
$contratacion_legislacion2="En aquesta secció trobarà diversos enllaços als documents corresponents a la legislació elèctrica espanyola.";
$contratacion_legislacion13="Ministeri d'Indústria, Turisme i Comerç";
?>