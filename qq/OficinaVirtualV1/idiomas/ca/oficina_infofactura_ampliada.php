<?php
$oficina_infofactura_ampliada1="Anagrama de l´empresa comercialitzadora i Dades Identificatives de la Factura.</br></br> Direcció d´enviament de Factura";
$oficina_infofactura_ampliada2="Dades del Titular i punt de subministrament. Informació de Tarifes i Potència Contractada.";
$oficina_infofactura_ampliada4="Informació de Lectures Anteriors, Actuals i Consums efectuats dins dels Períodes de Dates indicats.";
$oficina_infofactura_ampliada5="Desglossament detallat dels conceptes facturats. ";
$oficina_infofactura_ampliada6="Impost elèctric, I.V.A. i total.";
$oficina_infofactura_ampliada3="Informació addicional";
?>