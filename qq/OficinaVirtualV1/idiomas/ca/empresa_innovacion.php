<?php
$empresa_innovacion1="Innovació";
$empresa_innovacion2="té establert un Pla d'Innovació que comprèn aspectes tan importants com són:";
$empresa_innovacion3="- Pla d'Infraestructures en la Xarxa de Distribució.<br />
- Pla d'Instal·lació de Nous Aparells de Mesura (Comptadors).<br />
- Pla d'Innovació TIC per a Gestions de Clients.";
$empresa_innovacion4="Pla d'Infraestructures en la Xarxa de Distribució.";
$empresa_innovacion5="Estem en plena fase de millores en La nostra Xarxa de Distribució, en
Línies de Connexió de servei, Centres de Transformació, Transformadors,
Circuits...<br /><br />
La renovació i substitució dels materials que constitueixen nostra
Xarxa de Distribució, és un fet de constant activitat, per tal
de reduir al mínim les avaries i garantir un millor servei feia
els nostres clients.";
$empresa_innovacion6="Pla d'Instal·lació de Nous Aparells de Mesura (Comptadors).";
$empresa_innovacion7="D'acord amb la Normativa Legal sobre els Aparells de Mesura (Comptadors),
s'estan realitzant la substitució de tots aquells de tecnologia analògica
pels nous de tecnologia digital. Aquests aparells porten integrats tots els requisits tècnics actuals i futurs.<br /><br />
Això comporta que puguin estar totalment preparats per a les vinents
Normatives Legals de Telemesura i Telegestió, que facilitessin els processos de gestionar tota la problemàtica del client en forma remota a través de Teleprocés.";
$empresa_innovacion8="Pla d'Innovació TIC per a Gestions de Clients.";
$empresa_innovacion9="El Pla d'Innovació TIC (Noves Tecnologies) que estem implementant,
tracta d'oferir als seus clients una gestió directa a través de la nostra nova pàgina web, facilitant totes les necessitats que es plantegin en relació amb la nostra empresa, directament des del seu domicili i sense necessitat de desplaçar-se a les nostres oficines.<br /><br />
Podrà realitzar gestions com:";
$empresa_innovacion10="- Contractar un nou Punt de Subministrament.<br />
- Qualsevol canvi en el seu contracte (Tarifes, Potències, Domicilis, Compte Bancari...).<br />
- Imprimir còpies de factures que ja han estat emeses.<br />
- Informació i Certificats relatius als seus consums en kWh i/o imports facturats.<br />
- Comunicar Lectures de Comptadors sinó ha estat possible prendre-la.<br />
- Comunicar qualsevol Avaria o Incidència.";
$empresa_innovacion11="És a dir, qualsevol necessitat de gestió es podrà realitzar a través de l'Oficina de la nostra Pàgina Web.";
?>