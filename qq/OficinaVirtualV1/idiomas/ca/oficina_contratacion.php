<?php
$oficina_contratacion1="Requeriments Administratius per donar d'alta un nou punt de subministrament:";
$submenu_contratacion_oficina1="Tarifes a contractar:";
$submenu_contratacion_oficina2="- Bonos Social (Inferior a 3 kWh).";
$submenu_contratacion_oficina3="- Contractes fins a 10 kWh amb o sense discriminació horària (Tarifes 2.0.A i 2.0 DHA).";
$submenu_contratacion_oficina4="- Contractes de 10 a 15 kWh amb o sense discriminació horària (Tarifes 2.1.A i 2.1 DHA).";
$submenu_contratacion_oficina5="- Contractes a partir de 15 kWh (Tarifes 3.0.A i 3.1.A i 6.x).";
$oficina_contratacion2="NOTA: Per a qualsevol informació addicional sobre tarifes a contractar i tipus de subministrament, han de consultar directament a les nostres oficines.";
$oficina_contratacion3="Condicions per al Bono Social:";
$oficina_contratacion4="El Bono Social saplicarà a personas físicas el subministrament de les quals es destini a lús en
residència habitual i que pertanyi a algun dels següents col·lectius:<br /><br />
Persones físiques amb una potència contractada inferior a 3kW a la residència habitual. Se li aplicarà
el Bono Social Automàticament.<br /><br />
Clients amb edat mínima de 60 anys que percebin una pensió mínima.<br /><br />
Clients de més de 60 anys que percebin pensions no contributives de jubilació i invalidesa, així com
beneficiaris de pensions de l'extingida Assegurança Obligatòria de Vellesa i invalidesa.<br /><br />
Famílies nombroses.<br /><br />
Famílies amb tots els seus membres en situació d'atur.<br /><br />";
$oficina_contratacion5="Referència Cadastral.";
$oficina_contratacion6="CUPS (Codi Identificador del seu Punt de Subministrament) en cas que provingui d'una altra Comercialitzadora.";
$oficina_contratacion7="Aquesta sol·licitud de nou subministrament tindrà caràcter merament informatiu. La nostra empresa es posarà en contacte amb vostè durant els propers dies per presentar-li una oferta econòmica personalitzada, a través d&acute;adreça electrònica o correu ordinari.<br /><br />Una vegada acceptat aquest pressupost serà quan podem finalitzar amb la nostra empresa el contracte de subministrament d&acute;energia elèctrica en el punt de subministrament i condicions sol·licitades.<br />Perquè pugui rebre aquesta informació haurà d&acute;emplenar el formulari corresponent a la potència que vulgui contractar, segons els grups que a continuació es desglossen.";
$oficina_contratacion8="Títol de propietat o contracte de lloguer.";
$oficina_contratacion9="Cèdula d'habitabilitat.";
?>