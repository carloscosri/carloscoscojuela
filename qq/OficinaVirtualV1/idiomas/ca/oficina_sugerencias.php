<?php
$error_telefono="El telèfon i el Fax han de contenir caràcters numèrics.";
$error_email="No ha escrit l\'adreça de correu electrònica correctament.";
$error_rellenar="Per realitzar el suggeriment almenys ha d\'emplenar els camps obligatoris: nom, telèfon i observacions.";
$sugerencia_ok="El seu suggeriment ha estat enviada.";
?>