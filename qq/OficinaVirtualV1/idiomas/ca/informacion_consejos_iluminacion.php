<?php
$informacion_consejos_iluminacion1="Consells per Estalviar Energia - Il·luminació";
$informacion_consejos_iluminacion2="Des del principi de l'existència, l'home s'ha esforçat per trobar sistemes que substitueixin la llum solar en les hores de foscor o en llocs on escassejava.<br /><br />
Fruit d'aquesta inquietud ha estat l'aparició d'una sèrie d'elements que, des del descobriment del foc fins a les diferents modalitats de llums elèctriques conegudes avui dia, han anat marcant etapes en el desenvolupament de fonts de llum artificial.<br /><br />
Actualment, la investigació continua avançant cap a la consecució de llums que facilitin una qualitat de llum el més pròxima possible a la solar.<br /><br />
En el mercat es poden trobar diversos tipus de llums adequades a l'ús domèstic en què varia, entre altres factors, la seva durada, el consum i la qualitat de la llum que emeten.";
$informacion_consejos_iluminacion3="Llums incandescents";
$informacion_consejos_iluminacion4="Llums Halògenes";
$informacion_consejos_iluminacion5="Llums Fluorescents";
$informacion_consejos_iluminacion6="Llums de Baix Consum";
$informacion_consejos_iluminacion7="Quadre Comparatiu";
$informacion_consejos_iluminacion8="Eficàcia dels Diferents Tipus de Llums";
$informacion_consejos_iluminacion10="Són les populars bombetes la llum de les quals saconsegueix per mitjà d'un filament que s'escalfa amb el pas de l'electricitat.";
$informacion_consejos_iluminacion11="AVANTATGES:";
$informacion_consejos_iluminacion12="Aquestes llums emeten una llum de gran quantitat, amb la qual cosa reprodueixen molt bé els colors.";
$informacion_consejos_iluminacion13="Són les més barates entre totes les fonts de llum artificial.";
$informacion_consejos_iluminacion14="Ofereixen més gran flexibilitat a causa de l'enorme gamma de models i potències.";
$informacion_consejos_iluminacion15="INCONVENIENTS:";
$informacion_consejos_iluminacion16="Consumeixen més energia que unes altres en relació amb la quantitat de llum que aporten.";
$informacion_consejos_iluminacion17="Pel que fa a unes altres, la seva durada és més limitada.";
$informacion_consejos_iluminacion18="Davant les bombetes tradicionals, la llum halògena és més lluminosa i blanca, a més aporta nombroses solucions individuals d'il·luminació, i per descomptat amb un més gran confort visual, obtenint ambients més moderns i atractius. Existeixen amb o sense reflector, segons es vulgui concentrar la llum o no.";
$informacion_consejos_iluminacion19="Ofereixen una llum més blanca i brillant, cosa que fa que permetin una perfecta discriminació dels colors.";
$informacion_consejos_iluminacion20="Per les seves qualitats donen més joc en la decoració d'interiors.";
$informacion_consejos_iluminacion21="La seva eficàcia i durada és superior a les llums incandescents normals.";
$informacion_consejos_iluminacion22="La seva qualitat de llum roman inalterable durant tota la vida de la llum.";
$informacion_consejos_iluminacion23="Tenen un elevat consum d'energia.";
$informacion_consejos_iluminacion24="Els tipus de poca potència exigeixen un transformador que, en determinats models, vénen ja incorporat.";
$informacion_consejos_iluminacion25="Lemissió de la llum és molt concentrada, això obliga a apantallar la làmpada perquè no es vegi directament.";
$informacion_consejos_iluminacion26="En aquest cas, la llum no està produïda per l'escalfament d'un filament sinó per una baixada elèctrica en arc mantinguda en un gas o vapor ionitzat.<br /><br />
Les llums fluorescents són aconsellables com enllumenat general d'aplicació universal i econòmica, i atès que existeixen en diferents tonalitats de la llum, es pot escollir la més adequada per a la seva aplicació, com en banys, cuines, despatxos, passadissos, trasters o garatges.";
$informacion_consejos_iluminacion27="Ofereixen, depenent de la gamma, una bona qualitat de llum i reproducció natural dels colors.";
$informacion_consejos_iluminacion28="El seu consum energètic és molt reduït i rendible perquè, a igual quantitat de llum, consumeixen la cinquena part que les llums incandescents i la seva durada és molt llarga, de vuit a deu vegades més que aquestes.";
$informacion_consejos_iluminacion29="Davant l'opinió popular, no consumeixen més en l'arrencada de l'encès, per la qual cosa han d'apagar-se quan no vagin a utilitzar-se.";
$informacion_consejos_iluminacion30="La seva qualitat de llum roman inalterable durant tota la vida de la llum.";
$informacion_consejos_iluminacion31="Les seves dimensions i disseny limiten la seva utilització al no existir una oferta tan extensa com en els llums incandescents.";
$informacion_consejos_iluminacion32="Són les llums d'estalvi energètic (Fluorescents compactes electròniques) que, a més d'aportar una qualitat de llum ambiental a qualsevol lloc, tant interior com exterior, són fonamentals pel seu baix consum en aquells llocs on es necessitin un enllumenat amb llargs períodes d'encès. Ideals per a espais exteriors i com a enllumenat de seguretat.<br /><br />
Aquesta família de llums eficients, són alguna cosa més que merament econòmiques. Poden utilitzar-se gairebé de manera general, exactament igual que les tradicionals bombetes.";
$informacion_consejos_iluminacion33="Consumeixen cinc vegades menys que les incandescents.";
$informacion_consejos_iluminacion34="Tenen una vida útil 6 vegades més grans, aproximadament vuit anys, estimant quatre hores diàries d'encès.";
$informacion_consejos_iluminacion35="Gràcies a l'estalvi d'energia, la seva utilització contribueix a la preservació del medi ambient.";
$informacion_consejos_iluminacion36="Posseeixen el mateix casquet que les bombetes tradicionals el que, unit a la seva qualitat i confort de llum, les fa útils per a qualsevol aplicació.";
$informacion_consejos_iluminacion37="Tradicionals";
$informacion_consejos_iluminacion38="A la taula següent podem veure leficàcia, la duració mitjana i la possibilitat de diferenciar colors entre els diferents tipus de làmpades més utilitzades a les vivendes.";
$informacion_consejos_iluminacion39="Tipus de Llum";
$informacion_consejos_iluminacion40="Índex d'Eficàcia";
$informacion_consejos_iluminacion41="Durada Mitja (h)";
$informacion_consejos_iluminacion42="Posibilidad de Distinguir Colores";
$informacion_consejos_iluminacion43="Incandescents";
$informacion_consejos_iluminacion44="Excel·lent";
$informacion_consejos_iluminacion45="Halògenes";
$informacion_consejos_iluminacion46="Fluorescents";
$informacion_consejos_iluminacion47="Bona";
$informacion_consejos_iluminacion48="Fluorescent Extra";
$informacion_consejos_iluminacion49="Molt Bona";
$informacion_consejos_iluminacion50="Consells pràctics:";
$informacion_consejos_iluminacion51="Sempre que pugui, aprofiti la llum natural.";
$informacion_consejos_iluminacion52="Utilitzi la seva llar colors clars per a parets i sostres per aprofitar millor la il·luminació natural i reduir l'artificial.";
$informacion_consejos_iluminacion53="Apagui sempre les llums en les habitacions que no estigui utilitzant.";
$informacion_consejos_iluminacion54="Minimitzi la il·luminació ornamental en jardins i unes altres zones exteriors.";
$informacion_consejos_iluminacion55="Netegi sovint les llums i les pantalles, així augmentarà la lluminositat sense augmentar la potència.";
$informacion_consejos_iluminacion56="Adapti la il·luminació a les seves necessitats i tingui en compte que amb la il·luminació localitzada aconseguirà, a més d'estalviar energia, ambients més confortables.";
$informacion_consejos_iluminacion57="Utilitzi bombetes de baix consum en comptes de les bombetes incandescents, duren 6 vegades més i estalviarà fins a un 80% d'energia.";
$informacion_consejos_iluminacion58="Utilitzi llums electròniques ja que duren més, aguanten més encesos i apagats, i consumeixen menys que les llums de baix consum convencional. Sabrà distingir-les pel seu pes: mentre que les convencionals pesen al voltant dels 400 grs. les electròniques pesen uns 100 grs.";
$informacion_consejos_iluminacion59="Colloqui reguladors d'intensitat lluminosa de tipus electrònic (no de rostato).";
$informacion_consejos_iluminacion60="Utilitzi tubs fluorescents on necessiti més llum durant moltes hores, com per exemple la cuina.";
$informacion_consejos_iluminacion61="Utilitzi detectors de presència perquè les llums funcionin automàticament en aquells llocs pocs habitables com vestíbuls, garatges o zones comunes.";
$volver="Tornar";
?>