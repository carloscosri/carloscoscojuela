<?php
$informacion_consejos_vitroceramica1="Consells per Estalviar Energia - Vitroceràmica";
$informacion_consejos_vitroceramica2="Cuina elèctrica: encimera vitroceràmica";
$informacion_consejos_vitroceramica3="És una cuina en què les zones de cocció han estat serigrafiades sobre una superfície plana vitroceràmica amb un elevat grau de resistència al pes, als cops i a les diferències de temperatura. La calor s'obté d'unes resistències elèctriques situades sota les zones de cocció i, en alguns casos, de l'acció combinada d'aquestes resistències i unes llums halògenes.<br /><br />
Depenent dels models i fabricants, poden tenir 2, 4 o 5 punts de cocció. Existeixen models que tenen una zona lateral que funciona a baixa potència (150 W) i que s'utilitza per mantenir temperats els aliments. La incorporació de calefactors de mitja lluna en una de les zones de cocció, permet la utilització de recipients allargats.
Per les seves característiques exteriors és la cuina més neta i segura, de més fàcil utilització i més precisa.<br /><br />
La situació dels comandaments poden ser les següents:";
$informacion_consejos_vitroceramica4="1. <strong>A lencimera.</strong> Resulta molt útil, quan es tracta de substitucions en renovació de cuina com a tal aparell, o altres similars.<br /><br />
2. <strong>En panell d'independència.</strong> Permet col·locar els comandaments en una determinada situació, per exemple, sota la visera del moble, cosa que els protegeix de la brutícia.<br /><br />
3. <strong>Incorporats al forn.</strong> Els comandaments incorporats al forn tenen l'avantatge d'oferir una superfície de treball més gran.";
$informacion_consejos_vitroceramica5="Tipus de vitroceràmica (en funció dels elements calefactors):";
$informacion_consejos_vitroceramica6="Des de l'aparició de les Encimeres Vitroceràmiques, els elements calefactors utilitzats s'han desenvolupat i diversificat fortament. Existeixen tres grans grups pel que fa a les tecnologies treballadores: resistències radiants, halògens i d'Inducció.<br /><br />
Les potències màximes més usuals dels punts de cocció són 1200, 1700 i 2100 W. Els actuals sistemes permeten regular la potència de cocció a valors inferiors als indicats segons la posició del comandament.<br /><br />
Els punts de cocció amb halògens presenten avantatges reals, com ara: ";
$informacion_consejos_vitroceramica7="- Emissió de calor i llum instantània.<br />
- Rapidesa d'escalfament.<br />
- Menor dependència de la qualitat del recipient.<br />
- Flexibilitat i rapidesa de reacció al comandament de la placa.<br />
- Més seguretat contra els errors de manipulació al veures immediatament si es connecta.";
$informacion_consejos_vitroceramica8="Recomanacions d'ús";
$informacion_consejos_vitroceramica9="Llegiu atentament les instruccions del manual del fabricant de la cuina. Aquí coneixerà quina és la posició del comandament regulador de calor més adequada al tipus de cocció, i la quantitat d'aliments a cuinar.<br /><br />
Utilitzeu estris de cuina amb fons difusor pla i llis i un diàmetre igual o superior al de la zona de cocció. Si no disposa d'ells, hi ha una gran varietat de models i marques a escollir entre els quals han estat especialment dissenyats per ser utilitzats en vitroceràmiques. En canvi es desaconsella el lloc de treball de recipients d'alumini o cassoles de fang.<br /><br />
Les cuines elèctriques vitroceràmiques incorporen, normalment una senyalització de cada zona de cocció, que s'il·lumina quan la temperatura de la mateixa és més gran de 50 º C i roman així, una vegada s'ha desconnectat, fins que la temperatura baixa d'aquest límit.<br /><br />
Existeixen models que automàticament desconnecten les plaques en retirar els recipients, amb la qual cosa no existeix consum encara que romangui connectada.";
$informacion_consejos_vitroceramica10="Manteniment:";
$informacion_consejos_vitroceramica11="La cuina elèctrica vitroceràmica no precisa pràcticament manteniment. És aconsellable netejar la superfície després de cada ús, una vegada apagats els focus de calor, amb un drap o paper de cuina perquè recuperi brillantor. Si han quedat residus sòlids adherits sobre la superfície, es recomana arrencar-los amb una rasqueta abans que es refredin i, després, esbandir i assecar la placa.<br /><br />
Exigiu i teniu a mà la garantia del Fabricant de la Cuina. Durant el seu període de vigència cobreix la reparació de les avaries que ocasionalment poden produir-se.";
$informacion_consejos_vitroceramica12="AVANTATGES:";
$informacion_consejos_vitroceramica13="Les encimeres vitroceràmiques pel seu disseny y acabat totalment nou i atractiu, i per les seves característiques de visualització, flexibilitat i rapidesa, són un producte altament competitiu y avantatjós davant altres tipus de cuines.";
$informacion_consejos_vitroceramica14="Velocitat, precisió i visualització: Aconsegueixen una velocitat d'escalfament comparable a qualsevol altre sistema i energia. El seu temps de resposta és pràcticament immediat, visualitzant-se les plaques en funcionament.";
$informacion_consejos_vitroceramica15="Estètica: amb línies elegants i depurades i superfícies totalment llises, aconsegueixen una elegància inaccessible per qualsevol altre tipus dencimeres.";
$informacion_consejos_vitroceramica16="Facilitat d'utilització i neteja: Les cassoles, paelles i recipients en general rellisquen per la seva superfície sense bascular, per la qual cosa resulta molt fàcil la seva manipulació, i la bolcada és però difícil que en altres tipus de cuines, cuya superfície no sigui completament llisa. En no disposar de ranures ni zones de difícil accés, o unes altres zones que requereixen una neteja intensiva, aquesta resulta més fàcil. Per efectuar la neteja normal es recomana que la placa estigui freda o temperada.";
$informacion_consejos_vitroceramica17="Solidesa i resistència: la vitroceràmica és un material molt robust.";
$informacion_consejos_vitroceramica18="Seguretat: A la seguretat mateixa de la utilització de l'energia elèctrica, s'afegeix que la calor es concentra en les zones de cocció, sense perills de cremades si es toca la resta de la superfície. En no haver combustible, no existeix el risc d'explosió fortuïta.";
$informacion_consejos_vitroceramica19="Cuina elèctrica: Encimera vitroceràmica per inducció:";
$informacion_consejos_vitroceramica20="La cuina vitroceràmica per inducció és freda, la més neta, la de més fàcil utilització, la més segura, la més moderna i la més ràpida.<br /><br />
La cuina elèctrica vitroceràmica per inducció és llisa, sense cremadors, i la seva superfície és un vidre ceràmic, amb alt grau de resistència al pes i als cops. La seva precisió en cada temperatura de cocció és exacta, a més no s'escalfa la placa.<br /><br />
En aquestes cuines la calor es genera en el mateix recipient de cuinar a partir d'un camp magnètic creat per un element que està situat baix la superfície vitroceràmica, per la qual cosa no hi ha pèrdues de calor.";
$informacion_consejos_vitroceramica21="Recomanacions d'ús i manteniment:";
$informacion_consejos_vitroceramica22="Iniciar la cocció amb la numeració més alta de calor de comandament, per baixar posteriorment a la posició desitjada.<br /><br />
Si només es vol calor suau, usar numeracions baixes fins a aconseguir exactament la calor desitjada.<br /><br />
En situar-se sobre la posició 0 del comandament, es deixa de subministrar calor instantàniament (l'ebullició s'atura a l'acte).<br /><br />
Si no hi ha recipient sobre la zona de cocció, aquesta no subministra calor encara que està connectada.<br /><br />
Per a la neteja les encimeres vitroceràmiques per inducció, n'hi haurà prou amb usar un drap humit (de paper, reixeta, spontex, o similar). Però si cal, poden utilitzar-se detergents adequats que existeixin en el mercat, esbandint i assecant bé després del seu ús per recuperar la seva brillantor.<br /><br />
S'obtindrà un notable estalvi d'energia usant els recipients tapats o mitjà tapat sempre que sigui possible.";
$informacion_consejos_vitroceramica23="Consum aproximat d'energia";
$informacion_consejos_vitroceramica24="Tasca";
$informacion_consejos_vitroceramica25="Gas (kWh)";
$informacion_consejos_vitroceramica26="Electricitat (kWh)";
$informacion_consejos_vitroceramica27="Filet planxa";
$informacion_consejos_vitroceramica28="Jueves cuites en cassola d'acer (4 persones)";
$informacion_consejos_vitroceramica29="Arròs (4 persones)";
$informacion_consejos_vitroceramica30="Recipients apropiats per a aquest tipus de cuina:";
$informacion_consejos_vitroceramica31="Han d'utilitzar-se recipients de material ferromagnètic, fons pla, llis i el més gruixut possible. Aquesta propietat i fàcilment comprovable amb un imant que haurà de ser atret pel recipient. Per tant poden utilitzar-se estris d'acer esmaltat, de ferro fos o d'acer inoxidable que continguin algun material ferromagnètic.<br /><br />
Els recipients amb fons tipus sandvitx, amb tres o més capes que continguin algun material ferromagnètic, són utilitzables en la majoria dels casos.<br /><br />
Encara que cal una quantitat mínima de massa ferromagnètica per poder funcionar, no deixar estris metàl·lics sobre la zona de cocció. En cap cas s'han d'utilitzar estris d'alumini o de fang, ja que no funcionaria la placa.";
$informacion_consejos_vitroceramica32="AVANTATGES:";
$informacion_consejos_vitroceramica33="Subministrament d'energia: L'electricitat es troba instal·lada en totes les llars, per la qual cosa no cal preocupar-se d'anar a cercar l'energia amb què cuinar o emmagatzemada.";
$informacion_consejos_vitroceramica34="Velocitat, pressió i visualització: Aconsegueixen una velocitat d'escalfament comparable a qualsevol altre sistema d'energia. El seu temps de resposta és immediat, visualitzant-se les plaques en funcionament mitjançant un pilot censor de temperatura.";
$informacion_consejos_vitroceramica35="Estètica: Amb línies elegants i depurades, i superfícies totalment llises, aconsegueixen una elegància inabastable per qualsevol tipus dencimera.";
$informacion_consejos_vitroceramica36="Facilitat d'utilització i neteja: Les cassoles, paelles i recipients en general rellisquen per la seva superfície sense bascular, per la qual cosa resulta molt fàcil la seva manipulació, i la bolcada és més difícil que en altres tipus de cuines cuya neteja intensiva i atès que la placa roman freda o amb una lleu calor transmesa pel mateix recipient, la seva neteja es limita a passar un drap humit. Solidesa i resistència: La vitroceràmica és un material molt robust.";
$informacion_consejos_vitroceramica37="Seguretat: A la seguretat mateixa de la utilització de l'energia elèctrica, s'afegeix que les zones de cocció no tenen perill de cremades si es toquen en retirar el recipient, ja que lencimera roman freda.";
$informacion_consejos_vitroceramica38="Consells Pràctics";
$informacion_consejos_vitroceramica39="Procureu cuinar amb poca aigua i amb els recipients tapats o mig tapats.";
$informacion_consejos_vitroceramica40="L'ús de l'olla a pressió és molt aconsellable, pot estalviar fins a un 50% d'energia.";
$informacion_consejos_vitroceramica41="Tenint en compte que les zones de cocció, una vegada desconnectades mantenen l'ebullició i la calor dels aliments de 5 a 7 minuts més (llevat de les d'inducció), apagui-les una mica abans d'acabar la cocció.";
$informacion_consejos_vitroceramica42="A l'hora de cuinar tingui en compte que el fons dels recipients ha de ser lleugerament superior a la zona de cocció per aprofitar al màxim la calor.";
$informacion_consejos_vitroceramica43="És recomanable la utilització de parament amb fons gruixut difusor ja que s'aconsegueix una temperatura més homogènia en tot el recipient.";
$informacion_consejos_vitroceramica44="Amb les cuines vitroceràmiques per inducció l'aprofitament energètic és total: detecta si hi ha o no hi ha recipient sobre la seva superfície, actuant només en el primer cas.";
?>