<?php
$informacion_consejos_climatizacion1="Consells per Estalviar Energia - Climatització";
$informacion_consejos_climatizacion2="La climatització és el procés de tractament de l'aire que permet controlar la temperatura, la humitat, el moviment i la neteja de l'aire a l'interior d'un local. Els valors d'aquests paràmetres influeixen molt directament sobre el confort, i varien amb l'època de l'any.<br /><br />
La Bomba de Calor és l'únic sistema que cobreix totes les necessitats de fred a l'estiu i de calor a l'hivern en un habitatge, amb la qual cosa obtenim un estalvi considerable en la inversió inicial, en no haver de comprar aparells per a fred i aparells per a calor.<br /><br />
Cal tenir en compte que a aquest avantatge s'uneix la del seu elevat rendiment, ja que aprofita l'energia existent en l'aire ambient.";
$informacion_consejos_climatizacion3="Aquest sistema ens permet:";
$informacion_consejos_climatizacion4="1. Controlar la temperatura.<br />
2. Controlar el moviment de l'aire.<br />
3. Eliminar les impureses de l'aire.";
$informacion_consejos_climatizacion5="La Bomba de Calor és neta, i no produeix fums ni olors en existir cap tipus de combustió, tenint a més, la possibilitat de la renovació de l'aire segons el tipus d'aparell.";
$informacion_consejos_climatizacion6="Abans de comprar un sistema de climatització:";
$informacion_consejos_climatizacion7="A l'hora d'instal·lar un sistema de climatització ha de buscar  l'assessorament d'un instal·lador especialitzat.<br /><br />
Per obtenir el màxim confort és imprescindible que el càlcul de les frigorías / calories necessàries es realitzi amb fiabilitat suficient. Per fer-ho, l'instal·lador tindrà en compte les característiques de l'habitatge: orientació, detalls constructius, fonts de calor, etc.El sistema de distribució de l'aire ha de ser l'adequat perquè la calor i el fred es reparteixin uniformement.Per ventilar les habitacions cal que el sistema de climatització tingui una presa d'aire exterior.";
$informacion_consejos_climatizacion9="Els sistemes de climatització per Bomba de Calor, es controlen mitjançant un comandament centralitzat. El seu ús és simple i fàcil, només cal seleccionar:";
$informacion_consejos_climatizacion8="Recomanacions d'ús:";
$informacion_consejos_climatizacion10="- La temperatura desitjada des del termòstat.<br />
- El mode d'utilització: fred, calor o ventilació.<br />
- La velocitat de ventilador.";
$informacion_consejos_climatizacion11="Existeixen alguns models que porten incorporat un programador horari de posada en marxa.";
$informacion_consejos_climatizacion12="Manteniment:";
$informacion_consejos_climatizacion13="La unitat interior que refreda o escalfa l'aire, disposa d'un filtre, que serveix per netejar l'aire de l'habitatge. L'única precaució que cal tenir, és mantenir-ho net, seguint les indicacions del fabricant.";
$informacion_consejos_climatizacion14="Consells pràctics:";
$informacion_consejos_climatizacion15="Les temperatures recomanades de confort són 25è a l'estiu i entre 18 i 20è a l'hivern.";
$informacion_consejos_climatizacion16="L'ajust de la temperatura és molt important per controlar el consum, per exemple: fixar una temperatura a l'hivern de 20è en comptes de 21è pot significar un estalvi d'energia del 10%; a l'estiu, per contra, per cada grau de temperatura que es fixi per sota dels 25è, estarà consumint de més.";
$informacion_consejos_climatizacion17="Pot reduir l'escalfament del seu habitatge a l'estiu col·locant tendals, tancant persianes i corrent les cortines.";
$informacion_consejos_climatizacion18="A l'estiu és convenient ventilar la casa quan l'aire del carrer sigui més fresc: nit i primeres hores del matí.";
$informacion_consejos_climatizacion19="Una altra manera d'evitar l'escalfament d'espais interiors és pintar sostres i parets exteriors amb colors clars per reflectir la radiació solar.";
$informacion_consejos_climatizacion20="Col·loqueu el climatitzador en un lloc on no doni sol i hi hagi bona circulació d'aire.";
?>