<?php
$contratacion_contratar_potencia1="Selecció de la Potència a Contractar";
$contratacion_contratar_potencia2="La potència de contracte d'electricitat depèn de la quantitat i les característiques de l'equipament de la seva llar: segons què i quants equips tingui, la potència varia significativament.<br /><br />

Contractar una potència inadequada a les seves necessitats no és res convenient. Si contracta més potencia de què necessita, no només està malgastant diners, sinó que també està perjudicant el medi ambient gastant més energia de la necessària. I si, per contra, contracta menys, es poden produir molestos talls en el subministrament provocat per l'ICP (Interruptor de Control de Potència).
";
$contratacion_contratar_potencia3="El càlcul de la potència suposa que la utilització dels equips de més gran potència no es realitza de forma simultània. És una potència de contracte mínim recomanada partint d'una tarifa 2.0 general.<br /><br />
Si disposes de:";
$contratacion_contratar_potencia4="- Termòstat elèctric instantani.<br />
- Radiadors elèctrics.<br />
- Calefacció acumulació.<br />
- Bomba de calor.<br />
- Aire condicionat.";
$contratacion_contratar_potencia5="T'aconsellem que et posis en contacte amb nosaltres al telèfon d'atenció al client 972 59 46 91 o a les nostres oficines perquè estudiem el teu cas més exhaustivament.";
?>