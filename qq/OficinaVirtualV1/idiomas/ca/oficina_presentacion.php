<?php
$oficina_presentacion1="Benvingut a l'oficina virtual de ";
$oficina_presentacion2="Li recordem que, per <strong>optimitzar</strong> la visualització de l'Oficina Virtual,<br />es recomana utilitzar els exploradors:<br />";
$presentacionTexto1="La contrasenya introduïda està associada a diversos punts de subministrament. <br>
Heu de seleccionar que punt de subministrament vol accedir";
$resumenFacturasIntro="Selecciona el rang de dates de les factures que desitgi descarregar";
$cerrar="Trancar";
$resumenFacturas="Resumen Factures";
$fechaInicio="Data inici:";
$fechaFin="Data fin:";
$cliente="Client";
$numfactura="Num. Facture";
$fecha="Data";
$importe="Import";
$numResumenFacturas="Num. Facture";
$totalfacturas="Total factures: ";
?>