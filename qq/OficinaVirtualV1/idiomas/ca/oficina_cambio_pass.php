<?php
$oficina_cambio_pass1="Introdueixi la seva contrasenya";
$oficina_cambio_pass2="Introdueixi nova contrasenya";
$oficina_cambio_pass3="Confirmi la seva nova contrasenya";
$error_rellenar="Ha d\'emplenar totes les dades.";
$error_pass_invalida="La clau d\'accés no és correcta.";
$pass_no_coincide="La nova clau d\'accés i la confirmació no coincideixen.";
$mensaje_actualizar_pass="S\'ha actualitzat la seva clau d\'accés.";
?>