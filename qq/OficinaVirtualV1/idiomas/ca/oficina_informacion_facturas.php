<?php
$error_rellenar="Han d\'emplenar-se els camps marcats com a obligatoris.";
$error_fecha="La data s\'ha escrit de forma incorrecta. Ha d\'utilitzar el següent format: 18-03-2009.";
$error_fecha_posterior="Les dates escrites no poden ser posteriors a la data actual.";
$error_fecha_inicio_mayor="La data d\'inici no pot ser més gran que la data final.";
$solicitud_ok="La seva sol·licitud ha estat enviada amb èxit.";
$oficina_informacion_facturas1="CONSUMS ACTIUS";
$oficina_informacion_facturas2="P1 Punta";
$oficina_informacion_facturas3="P2 Pla";
$oficina_informacion_facturas4="P3 Vall";
$oficina_informacion_facturas5="CONSUMS REACTIUS";
$oficina_informacion_facturas6="IMPORTS";
$oficina_informacion_facturas7="Activa";
$oficina_informacion_facturas8="Reactiva";
$oficina_informacion_facturas9="Maxímetre";
$oficina_informacion_facturas10="Descomptes";
$oficina_informacion_facturas11="Càrrecs";
$oficina_informacion_facturas12="Base";
$oficina_informacion_facturas13="IVA";
$oficina_informacion_facturas14="TOTAL:";
$oficina_informacion_facturas15="Imprimir";
$error_no_facturas="Actualment no hi ha factures relacionades amb aquest client en la nostra base de dades.";
?>