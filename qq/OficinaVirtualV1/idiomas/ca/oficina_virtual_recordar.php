<?php
$oficina_virtual_recordar1="Per la seva pròpia seguretat la informació de la clau d'accés oblidada, només serà proporcionada a l'usuari en el telèfon o direcció adreça electrònica que ens proporciona.";
$oficina_virtual_recordar2="Usuari";
$oficina_virtual_recordar3="Telèfon";
$oficina_virtual_recordar4="E-mail";
$oficina_virtual_recordar5="Acceptar";
$oficina_virtual_recordar6="Tornar";
$error_recordar_vacio="Per facilitar-li la seva clau ha d\'escriure el seu nombre d\'usuari i almenys un de les dades de contacte.";
$error_no_usuario="No existeix aquest usuari.";
$error_telefono="El telèfon sol pot contenir caràcters numèrics.";
$error_mail="No ha escrit l\'adreça electrònica correctament.";
$recordar_ok="Ens pondrémos en contácto amb vostè per facilitar-li la seva clau.";
?>