<?php
$informacion_consejos_calefaccion1="Consells per Estalviar Energia - Calefacció";
$informacion_consejos_calefaccion2="Calefacció elèctrica. Estalviar energia amb acumuladors de calor i tarifa nocturna 2.0:";
$informacion_consejos_calefaccion3="Els acumuladors de calor són aparells capaços d'emmagatzemar energia calorífica durant un període de temps, per alliberar-la després lentament, amb flux controlable. Són elements idonis per a calefacció ambiental atès que utilitzen les hores de la nit per emmagatzemar la calor, aprofitant al màxim els avantatges de la Tarifa Nocturna. L'usuari d'aquesta tarifa, obté un descompte d'un 53% en el preu de l'energia consumit durant les hores, nocturnes, i tenen un recàrrec del 3% en el preu del quilowatt hora consumida durant el dia.";
$informacion_consejos_calefaccion4="Tipus d'acumuladors:";
$informacion_consejos_calefaccion5="Existeixen dos tipus d'acumuladors, la diferència de les quals és la forma de cedir calor:<br /><br />
<strong>Acumuladors estàtics:</strong> En aquest tipus d'aparells, la calor es cedeix principalment per radiació, a través de l'envoltant metàl·lica, i per una convenció natural, gràcies a l'aire que pot circular a través d'uns canals existents en la carcassa i en nucli de l'acumulador. A aquest efecte, disposen d'una entrada d'aire a la part inferior de l'aparell i una sortida per la part superior. L'aire de sortida es regula mitjançant una comporta.<br /><br />
<strong>Acumuladors dinàmics:</strong> Els acumuladors dinàmics són similars als estàtics. També cedeix la calor per radiació, a través de l'envoltant metàl·lica, però la convecció és forçada mitjançant una turbina que fa circular l'aire pels conductes o canals existents en el nucli de l'acumulador.";
$informacion_consejos_calefaccion6="<strong>1.  Bloc acumulador:</strong> El nucli acumulador està constituït per plaques de material refractari (magnesita) que han de presentar una calor específica elevada i la màxima densitat, ja que la capacitat per emmagatzemar calor depèn d'aquests dos paràmetres. La temperatura màxima que s'assoleix al final del període de càrrega en el nucli de l'acumulador és de 600 a 700 º C.<br /><br />
<strong>2. Resistències calefactores:</strong> Els elements calefactors (resistències) s'insereixen en cavitats o canals situats a l'interior del nucli acumulador, de manera que escalfen tota la massa del refractorio uniformement.<br /><br />
<strong>3. Aïllament tèrmic:</strong> El bloc acumulador està envoltat amb aïllant tèrmic per conservar la calor acumuleu i limitar la temperatura superficial del moble de l'aparell.<br /><br />
<strong>4. Dispositiu de seguretat:</strong> Generalment els acumuladors disposen d'un limitador tèrmic o d'un termòstat de seguretat, per desconnectar l'aparell si, per qualsevol motiu, es produeix una avaluació inadequada de la temperatura (per exemple, una disfunció en el sistema de regulació del sistema de càrrega).<br /><br />
<strong>5. Termòstat:</strong> Nhi  dos, un per regular la càrrega i un altre per a l'emissió de calor.";
$informacion_consejos_calefaccion7="AVANTATGES:";
$informacion_consejos_calefaccion8="Es poden instal·lar tant en edificis de nova construcció com en cases antigues.";
$informacion_consejos_calefaccion9="No existeixen circuits d'aigua, eliminant la possibilitat de fugues i degotejos.";
$informacion_consejos_calefaccion10="No necessiten dipòsit de combustible.";
$informacion_consejos_calefaccion11="Instal·lació senzilla, sense obres.";
$informacion_consejos_calefaccion12="Ecològics, ja que no emeten fums i utilitzen l'energia més neta que es coneix en el punt de consum: l'Electricitat.";
$informacion_consejos_calefaccion13="Total seguretat i en no haver combustibles, no existeix risc d'explosió fortuïta.";
$informacion_consejos_calefaccion14="Manteniment pràcticament inexistent.";
$informacion_consejos_calefaccion15="Proporcionen calefacció les 24 h. del dia.";
$informacion_consejos_calefaccion16="Es redueixen els costos generals de la facturació, en desplaçar el consum de frigorífics, rentadores, rentaplats, escalfadors d'aigua, enllumenat, etc., a horari de Tarifa Nocturna, com s'ha indicat anteriorment en la descripció de l'aparell.";
$informacion_consejos_calefaccion17="La seva aparença estètica és de disseny avançat.";
$informacion_consejos_calefaccion18="Automatitzables al màxim.";
$informacion_consejos_calefaccion19="Calefacció elèctrica per cable radiant amb sistemes d'acumulació (base + suport, tarifa nocturna 2.0):";
$informacion_consejos_calefaccion20="La calefacció elèctrica per cable radiant és un sistema de calefacció no visible, que integra dues parts diferenciades pel que fa a l'ús d'horari i complementàries pel seu funcionament: calefacció de base i calefacció de suport.";
$informacion_consejos_calefaccion21="Calefacció de base:";
$informacion_consejos_calefaccion22="Aquest sistema calefactor instal·lat a terra queda connectat exclusivament durant el període de la nit, quan la tarifa nocturna té una reducció del 53% en el preu del kWh.
La calefacció de base instal·lada a terra, acumula calor durant el període de tarifa reduïda, que restitueix a l'ambient sense gastar energia durant el període de les hores restants.La quantitat de calor acumulat a terra ve determinada per la temperatura exterior, ja que disposa d'una sonda que detecta la temperatura.<br /><br />
Atès que la temperatura interior s'incrementa en alguns graus a causa de les aportacions de calor gratuïta generada per les persones, il·luminació, aigua calenta sanitària, electrodomèstics, insolació durant el dia, etc., que la quantia d'aquestes aportacions és imprevisible i no detectable per la sonda de la reguladora situada a l'exterior i que aquest no funciona durant les hores diürnes, és imprescindible comptar amb un sistema calefactor de suport complementari que permeti una regulació exacta del nivell de confort desitjat en cada estada.";
$informacion_consejos_calefaccion23="Calefacció de suport:";
$informacion_consejos_calefaccion24="Aquest sistema calefactor independent, de potència reduïda atès que només intervé compensant les variacions de la temperatura interior que el sistema de base no pot detectar ni corregir, és complementari de la calefacció de base per acumulació. Consisteix en cables calefactors integrats a terra, per damunt dels cables del sistema de base, connectats a la xarxa elèctrica a través d'un termòstat automàtic d'ambient en cada habitació o dependència.<br /><br />
La seva funció és mantenir durant les 24 hores al dia la temperatura exacta desitjada per l'usuari en cada habitació.
Una altra opció per a la calefacció de suport és la instal·lació de convectors elèctrics per completar l'aportació de calor fins a la temperatura prèviament seleccionada en cada habitació.";
$informacion_consejos_calefaccion25="Recomanacions d'ús:";
$informacion_consejos_calefaccion26="L'ajust de les temperatures volgut mitjançant els termòstats situats en les diferents dependències és molt important per aconseguir el més gran confort del seu sistema de calefacció.<br /><br />
Si el seu habitatge és de recent construcció, la mateixa humitat dels materials pot provocar un consum entre un 20 i un 30% més gran del que és normal en la primera temporada de funcionament, independentment del sistema de calefacció de què disposi.";
$informacion_consejos_calefaccion27="AVANTATGES:";
$informacion_consejos_calefaccion28="Net: No es produeixen moviments de l'aire i, per consegüent, de la pols.";
$informacion_consejos_calefaccion29="Saludable: No consumeix oxigen, no reseca laire i no rescalfa en excés.";
$informacion_consejos_calefaccion30="Cost controlat: La instal·lació individualitzada per dependències permet el control de la despesa d'acord amb les necessitats, horaris i usos. A més aprofita les aportacions gratuïtes de calor (sol, llums, persones).";
$informacion_consejos_calefaccion31="Invisible i silenciós: No hi ha maquinària ni elements mecànics a la vista.";
$informacion_consejos_calefaccion32="Assegurança: Fora de l'abast de les persones i protegit elèctricament d'acord amb el Reglament Electrotècnic de Baixa Tensió, i en no haver combustibles no existeix risc d'explosió fortuïta.";
$informacion_consejos_calefaccion33="I a més: Sense obres auxiliars, dipòsits de combustibles, manteniment, pagaments avançats d'energia i, és el sistema de menor cost d'inversió.";
$informacion_consejos_calefaccion34="Consells pràctics:";
$informacion_consejos_calefaccion35="L'ajust de la temperatura en cada habitació mitjançant el termòstat és molt important per controlar el consum del seu sistema de calefacció. Fixar una temperatura de 20ºC en comptes de 21ºC pot significar un estalvi d'energia del 10%.";
$informacion_consejos_calefaccion34="Consells pràctics:";
$informacion_consejos_calefaccion35="L'ajust de la temperatura en cada habitació mitjançant el termòstat és molt important per controlar el consum del seu sistema de calefacció. Fixar una temperatura de 20ºC en comptes de 21ºC pot significar un estalvi d'energia del 10%.";
$informacion_consejos_calefaccion36="Quan surti de la seva llar només per unes hores, fixeu la posició del termòstat en 15è, que és l'equivalent a la posició 'economia' d'alguns models.";
$informacion_consejos_calefaccion37="Redueixi la temperatura de les estades que no ocupin durant períodes de temps llargs.";
$informacion_consejos_calefaccion38="Apagui la calefacció a la nit i encengui-la al matí despues de ventilar la casa i tancar les finestres.";
$informacion_consejos_calefaccion39="Per ventilar les habitacions 10 minuts són suficients. Una renovació excessiva pot representar entre un 30 i 40% del consum total.";
$informacion_consejos_calefaccion40="Per evitar excessives pèrdues de calor, tancament a la nit persianes i cortines.";
$informacion_consejos_calefaccion41="Utilitzi vàlvules termostàtiques en radiadors i termòstats programadors, són fàcils de col·locar i s'amortitzen ràpidament (suposen un estalvi d'energia entre un 8 i 133%).";
$informacion_consejos_calefaccion42="A l'inici de cada temporada de fred és convenient purgar l'aire de l'interior dels radiadors per facilitar la transmissió de calor des de l'aigua calenta a l'exterior.";
$informacion_consejos_calefaccion43="No cobreixi ni col·loqui al costat del radiador cap objecte.";
$informacion_consejos_calefaccion44="Cuidi i mantingui adequadament el seu equip de calefacció, pot suposar-li un estalvi de fins al 15% d'energia.";
$informacion_consejos_calefaccion45="Cal l'assessorament d'un instal·lador per a la regulació inicial i posada en marxa de la calefacció base.";
?>