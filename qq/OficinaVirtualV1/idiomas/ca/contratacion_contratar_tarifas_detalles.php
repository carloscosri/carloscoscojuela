<?php
$titulo_potencias_menos_10="Tarifes de potència contractada fins a 10 kW";
$titulo_potencias_mas_10="Tarifes de potència contractada superior a 10 Kw i inferior a 15 Kw";
$titulo_potencias_alta_tension="Preus per a Contractes d'Alta Tensió";
$termino_potencia="Terme de Potència ";
$termino_energia="Terme d'Energia ";
$euro_kw_mes="Euros / Kw / Mes";
$euro_kwh="Euros / kWh";
$volver="Tornar";
$nota_potencias_menos_10="NOTA: Els preus reflectits podran ser modificats durant el període de vigència del contracte o de les seves pròrrogues segons els apartats 4 i 6 de les condicions generals del Contracte de Subministrament.";
$nota_potencias_mas_10="";
$nota_potencias_alta_tension="Per aplicar les tarifes del tipus 6.1, es farà un estudi individual i es farà una oferta personalitzada per a cada client.";
$tarifas_alta_tension1="Si necessita contractar un Subministrament en Mitjana/Alta Tensió, haurà d'emplenar ";
$tarifas_alta_tension2="la sol·licitud que s'indica en l'apartat corresponent.";
$tarifas_alta_tension3="Posteriorment ens posarem en contacte amb vostè per informar-li a a prop dels nostres preus i condicions.";
$titulo_potencias_mas_15="Tarifes de potència contractada superior a 15 Kw";
?>