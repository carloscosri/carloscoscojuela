<?php
$contratacion_ofertas1="Sistema per consultar Preus i Ofertes dirigides a Particulars i Empreses que vulguin contractar el Subministrament Elèctric amb ";
$contratacion_ofertas2="Energia ";
$contratacion_ofertas3="Inferior o igual a 10 kW.<br />Tarifes d'Accés 2.0A i 2.0DHA.";
$contratacion_ofertas4="Superior a 10 kW i inferior a 15 kW.<br />Tarifes d'Accés 2.1A i 2.1DHA.";
$contratacion_ofertas5="Superior a 15 kW.<br />Tarifa d'accés 3.0A.";
$contratacion_ofertas6="Si la seva potència contractada és<br />superior a 50 kW o el seu consum anual<br />és superior a 100.000 kWh.";
$contratacion_ofertas7="Per a determinats nivells de consum, es pot sol·licitar una oferta personalitzada. Això ho pot realitzar emplenant el següent ";
$contratacion_ofertas10="model de sol·licitud d'oferta en format PDF editable";
$contratacion_ofertas11="i enviar-ho per fax o ";
$contratacion_ofertas12="e-mail";
$contratacion_ofertas8="formulari";
$contratacion_ofertas9=", o bé emplenant el ";
?>