<?php
$titulo_potencias_menos_10="Kontratatutako potentzien tarifak ere 10 kW";
$titulo_potencias_mas_10="Kontratatutako potentzia nagusien tarifak a 10 kW";
$titulo_potencias_alta_tension="Goi-tentsioaren Kontratuentzat prezioak";
$termino_potencia="Hitza de Sustatzen du ";
$termino_energia="Energiaren hitza ";
$euro_kw_mes="Euroak / Kw / Hilabetea";
$euro_kwh="Euros / kWh";
$volver="Itzuli";
$nota_potencias_menos_10="OHARRA: Islatutako prezioak aldatuak izan ahal izango dira kontratuaren indarraldiaren denboraldian zehar edo hilaren 4tik 6ra bere luzapenen Horniduraren Kontratuaren baldintza orokorren.";
$nota_potencias_mas_10="";
$nota_potencias_alta_tension="6.1. motaren tarifak ezartzeko, gizabanako ikerketa bat egingo da eta bezero bakoitzarentzat pertsonalizatutako eskaintza bat egingo da.";
$tarifas_alta_tension1="Hornidura bat Batezbestekoan alokatu behar badu/Goi-tentsioa, bete behar izango du ";
$tarifas_alta_tension2="dagokion atalean adierazten den eskaera.";
$tarifas_alta_tension3="Ondoren harremanetan jarriko gara zurekin informatzeko a inguru gure prezioen eta baldintzak.";
$titulo_potencias_mas_15="Potentzia Handiagoen Kontratuentzat prezioak de 15 KW";
?>