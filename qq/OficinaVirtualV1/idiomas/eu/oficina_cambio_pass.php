<?php
$oficina_cambio_pass1="Haren pasahitza sar ezan";
$oficina_cambio_pass2="Pasahitz berria sar ezan";
$oficina_cambio_pass3="Haren pasahitz berria baiezta ezan";
$error_rellenar="Datu guztiak bete behar ditu.";
$error_pass_invalida="Pasahitza ez da zuzena.";
$pass_no_coincide="Pasahitz berria eta baieztapena ez bat datoz.";
$mensaje_actualizar_pass="Bere pasahitza eguneratu da.";
?>