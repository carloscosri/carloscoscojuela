<?php
$contratacion_contratar_motivos1="Alokatzeko arrazoiak con ";
$contratacion_contratar_motivos2="Gure filosofia que guztian maximoa eman da gure eskura dago: horniduraren kalitatea, arreta, aholkularitza, ikuskapenak, segurtasuna, berrikuntza...";
$contratacion_contratar_motivos4="Bere xedapenera talde handi bati teknikarien izango du eta arreta jartzera prestatutako profesionalak uneoro.";
$contratacion_contratar_motivos3="Enpresa baten gure esperientzia guztia bermatzen diogu urteetan zehar energia elektrikoa eman du.";
$contratacion_contratar_motivos5="Gure bulegoak, biltegiak eta teknikariak gure hirian daude eta, behar izanez gero, arreta jartzeko zure inguru gaude.";
$contratacion_contratar_motivos6="Enpresa sortzen duen onurak geratzen dira eta inbertitzen dute, era gehiengoa duenaren, gure hirian.";
$contratacion_contratar_motivos7="Zurekin konpromiso bat daukagu eta inguratzen digun munduarekin, porque aurrezte energetiko bat ez da bakarrik bere patrikarentzat ona, izaerarako ere.";
?>