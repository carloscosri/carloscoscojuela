<?php
$titulo1="BEHE-TENTSIOKO ENERGIA ELEKTRIKOAREN HORNIDUREN KONTRATUAREN baldintza PARTIKULARRAK";
$datos_cliente="BEZEROAREN DATUAK";
$nombre_razon_social="Izena/Izen Soziala";
$cif_nif="CIF / IFZa";
$direccion="Helbidea";
$num="Zenbakia";
$cp_poblacion="PK / Herria";
$provincia_pais="Probintzia / Herrialdea";
$telefono="Telefonoa";
$fax="Faxa";
$email="Eta-Maila";
$datos_contacto="KONTAKTUAREN DATUAK (envio faktura, beste komunikazio batzuk... Omitir bakarrik bezeroaren datuen badesberdina da)";
$persona_contacto="Kontakturako pertsona";
$titulo2="BALDINTZA TEKNIKOA-EKONOMIKOAK";
$datos_suministro="HORNIDURAREN PUNTUAREN DATUAK(Omitir bakarrik bezeroaren datuen badesberdina da)";
$ref_catastral="Katastral Erre.";
$contrato_num="Zen. kontratua";
$empresa_distribuidora="Enpresa Banatzailea:";
$tension_nominal="Tentsio Nominala (V):";
$tarifas_acceso="SARBIDEAREN TARIFAK(2001eko urriaren 26ko 1164/2001 EDean Definitutakoak)";
$tarifa="Tarifa:";
$potencia_contratada="Sustatzen du Kontratatutakoa (kW):";
$punta="Punta";
$llana="Laua";
$valle="Ibarra";
$producto_contratado="KONTRATATUTAKO PRODUKTUA";
$producto="Produktua:";
$datos_pagador="ORDAINTZAILEAREN DATUAK";
$titular_cuenta="Kontuaren titulua:";
$nif="IFZa";
$entidad_financiera="Erakunde finantzarioa";
$sucursal="Sukurtsala";
$digito_control="DK";
$num_cuenta="Kontuaren Zen.";
$explicacion_contrato1="Horniduraren Kontratuaren baldintza hauek, Orokorrekin batera dokumentu honen atzealdean daudena, horrela
bezala prezioen eranskina se erantsia, irakurriak izan dira bezeroak zein, ados egonez beraien edukiarekin
las subscribe.<br /><br />

Horretarako, bereziki baimena ematen du a ";
$explicacion_contrato2=", a alokatzen duena sarbidearen enpresa banatzailearekin sarera hirugarrenei (ATR), hitz egokietan Baldintza Orokorrera 1a., helburu eta efektura de ";
$explicacion_contrato3="baimena bere berri hornitzailea izatera.<br /><br />

Bezeroak era berean adierazten du datuak eta baldintza han zeudenetan islatutako informazioa osoak dira eta egiazkoek enpresa beraien frogapenak egitera baimena ematen.<br /><br />";
$a="ean";
$de="ko";
$enero="Urtarrila";
$febrero="Otsaila";
$marzo="Martxoa";
$abril="Apirila";
$mayo="Maiatza";
$junio="Ekaina";
$julio="Uztaila";
$agosto="Abuztua";
$septiembre="Iraila";
$octubre="Urria";
$noviembre="Azaroa";
$diciembre="Abendua";
$el_cliente="BEZEROA";
$fdo="Sinatuta";
$anexo_precios="PREZIOEN ERANSKINA";
$datos_suministro_sin_omitir="HORNIDURAREN PUNTUAREN DATUAK";
$termino_potencia="HITZA DE SUSTATZEN DU";
$precio_por_periodo="aldiak prezioa kWh";
$explicacion_anexo_precios1="Hitzaren fakturazioaren preziora eta baldintzetara ezartzen da de sustatzen du sarbidearen tarifaren 3.0A indarreko.";
$precio_energia="ENERGIAREN PREZIOA";
$explicacion_anexo_precios2="Energiaren prezioak sarbidearen tarifaren prezioa barne hartzen du 3.0A indarreko, aplikatu beharrekoa energiaren inguruan ordutegi aldian kontsumitutakoa dagokiona.";
$calendario_periodos="ALDIEN APLIKAZIOAREN EGUTEGIA";
$invierno="Negua";
$verano="Uda";
$explicacion_anexo_precios3="Bigarren puntuan gaur egun ezarrita 3 II. Eranskinaren ITCaren Agindua 2794/2007. Udara neguko ordutegiaren aldaketa eta alderantziz bat etorriko da orduaren aldaketa ofizialaren datarekin.";
$energia_reactiva="Energia Erreaktiboa";
$explicacion_anexo_precios4="Ezartzen dira zuei fakturazioaren prezioak eta baldintzak ezarritako energia erreaktiboaren sarbidearen tarifak 3.0A indarreko (gaur egun 1164/2001 EDa). Aldi guztiei ezarriko zaie tarifa-zerrendak, izan ezik ibar aldira, energia kontsumo erreaktiboak aldian zehar energia aktiboaren kontsumoaren% 33 pasatzen duenean gogoan hartutako fakturazioaren.";
$complementos="OSAGARRIAK";
$equipos_medida="NEURRIAREN TALDEAK";
$alquiler_equipo="Taldearen alokairua";
$euros_mes="/hilabetea";
$otros_alquileres="Beste alokairu batzuk";
$explicacion_anexo_precios5="Jarraitzen duen subjektu prezioa aldaketetara xedapenaren arabera aplikazioaren.";
$explicacion_anexo_precios6="Indarreko araudiaren arabera sarbidearen tarifen fakturazioa beti.<br />
Prezioetako bakoitza barne eranskin honetan dagozkion zergak barne hartuta dakarte izan ezik I.Eta eta I.V.A.";
?>