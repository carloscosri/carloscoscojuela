<?php
$error_rellenar="Behar dituzte bete arlo nabariak bezala nahitaezkoak.";
$error_fecha="Data gaizki idatzi da. Hurrengo formatuak enplegatu behar du: 18-03-2009.";
$error_hora="Ordua eta minutuek izan behar dute 2 carácteres 00 eta 23 arte zenbakizkoak eta arte 00-59 hurrenez hurren.";
$error_telefonos="Arloak Teléfonom, Mugikorra eta Faxa bakarrikak izan ahal dute carácteres zenbakizkoak.";
$error_mail="Ez du idatzi el eta-maila zuzenki.";
$incidencia_ok="Bere intzidentzia ordezkaria izan da.";
$error_fecha_posterior="Idatzitako datek ondorengoak gaur egungo datara. izan ezin dituzte.";
?>