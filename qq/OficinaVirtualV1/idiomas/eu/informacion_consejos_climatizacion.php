<?php
$informacion_consejos_climatizacion1="Energia Aurrezteko kontseiluak - Klimatizazioa";
$informacion_consejos_climatizacion2="Tenperatura, hezetasuna, mugimendua eta garbiketa kontrolatzea uzten duen klimatizazioa airearen tratamenduaren prozesua da barnekoan airearen tokiko baten. Parametro hauen balioek konfortaren inguruko oso zuzenean eragina dute, eta aldatzen dute urtearen garaiarekin.<br /><br />
Udan hotzaren behar guztiak estaltzen ditu Bero-ponpa sistema bakarra da eta neguan beroaren etxebizitza batean, beraz aurrezki handi bat hasierako inbertsioan, lortzen dugu al ez hotzarentzat aparatuak erosi behar izan eta beroarentzat aparatuak.<br /><br />
Kontuan izan behar da abantaila honetara batzen da la bere etekin altuaren, dagoen energia airean aprobetxatzen duelako girotzen du.";
$informacion_consejos_climatizacion3="Sistema honek uzten digu:";
$informacion_consejos_climatizacion4="1. Tenperatura kontrolatu.<br />
2. Airearen mugimendua kontrolatu.<br />
3. Airearen lohikeriak ezabatu.";
$informacion_consejos_climatizacion5="Bero-ponpa garbia da, eta ez du produzitzen kerik edo errekuntzarik egotean usainak, gainera izanez, aparatu motaren arabera airearen berritzearen aukera.";
$informacion_consejos_climatizacion6="Klimatizazioaren sistema bat erosi baino lehen:";
$informacion_consejos_climatizacion6="Before buying an air-conditioning system:";
$informacion_consejos_climatizacion7="Klimatizazioaren sistema bat instalatzeko orduan espezializatutako instalatzaile baten aholkularitza bilatu behar du.<br /><br />
Konfort maximoa lortzeko nahitaezkoa da kalkulua de las frigorías / beharrezko kaloriek fidagarritasun nahikoarekin egin da. Horretarako, instalatzailea etxebizitzaren ezaugarriak kontuan izango du: orientazioa, xehetasun konstruktiboak, bero-iturriak, etc.El sistema airearen banaketaren egokitutakoa izan behar da para beroa eta hotza gelak aireztatu banan uniformemente.Para klimatizazioaren sistemak kanpo airearen hartze bat eduki ohi duen. beharrezkoa da";
$informacion_consejos_climatizacion9="Klimatizazioaren sistemak Bero-ponpak, zentralizatutako aginte baten bidez kontrolatzen dira. Bere erabilera sinplea eta erraza da, bakarrik aukeratu behar da:";
$informacion_consejos_climatizacion8="Erabileraren gomendioak:";
$informacion_consejos_climatizacion10="- Termostatotik nahi izandako tenperatura.<br />
- Erabileraren modua: hotza, bero edo aireztapena.<br />
- Haizagailuaren abiadura.";
$informacion_consejos_climatizacion11="Martxan jartzearen ordutegi programatzaile bat barne hartuta daramaten eredu batzuk daude.";
$informacion_consejos_climatizacion12="Mantentzea:";
$informacion_consejos_climatizacion13="Hozten duen barneko unitatea edo airea berotzen du, etxebizitzaren airea garbitzeko balio duen, iragazki bat. Izan behar den kontu bakarra, garbia mantendu da, fabrikatzailearen argibideak jarraituz.";
$informacion_consejos_climatizacion14="Kontseilu Praktikoak:";
$informacion_consejos_climatizacion15="Konfortaren gomendatutako tenperaturak dira 25 udan eta neguan 18 eta 20 arte.";
$informacion_consejos_climatizacion16="Tenperaturaren doikuntza kontsumoa kontrolatzeko oso garrantzitsua da, esate baterako: neguan tenperatura bat ezarri de 20 ordez 21 %10eko energiaren aurrezki bat esan nahi izan ahal du; udan, aitzitik, tenperaturaren maila bakoitzak ezar dena azpitik 25ean, egongo da kontsumitzen de gehiago.";
$informacion_consejos_climatizacion17="Udan bere etxebizitzaren beroketa eguzki-oihalak jartzen gutxitu ahal du, pertsianak itxiz eta korrika gortinak.";
$informacion_consejos_climatizacion18="Udan aireztatzea egoki da etxea kalearen airea freskoa izan dadinean: gaua eta goizeko lehen orduak.";
$informacion_consejos_climatizacion19="Barneko guneen beroketa saihesteko beste era bat eguzki-erradiazioa islatzeko kolore argiekin sabai eta paretak kanpoak pintatu da.";
$informacion_consejos_climatizacion20="Eta han leku batean aire-girogailua jartzen du ez berari eguzkiaren eta airearen zirkulazio ona egon dadin.";
?>