<?php
$empresa_innovacion1="Berrikuntza";
$empresa_innovacion2="berrikuntzaren Plan bat ezarrita dauka alderdien abarka hain garrantzitsuak bezala";
$empresa_innovacion3="- Banaketa-sarean Azpiegituren plana.<br />
- Neurgailu Berrien Instalazioaren plana (Kontadoreak).<br />
- Bezeroen Kudeaketentzat IKT Berrikuntzaren plana.";
$empresa_innovacion4="Banaketa-sarean Azpiegituren plana.";
$empresa_innovacion5="Gure Banaketa-sarean hobekuntzen fase betean gaude, en
Oldartutakoaren lerroak, Eraldaketaren Zentroak, Transformatzaileak,
Zirkuituak...<br /><br />
Gurea osatzen gaituzten materialen berritzea eta ordezkapena
Banaketa-sarea, jarduera konstantearen gertaera bat da, helburuarekin
minimoari matxurak gutxitzeko eta hobeto bat bermatu zerbitzuak egiten zuen gure bezeroak.";
$empresa_innovacion6="Neurgailu Berrien Instalazioaren plana (Kontadoreak).";
$empresa_innovacion7="Neurgailuen inguruko Legezko Araudiaren arabera (Kontadoreak),
guztien ordezkapena egiten ari dira teknologia analogikoaren haiek
teknologia digitalaren berriek. Aparatu hauek baldintza guztiak integratuta daramate gaur egungo eta etorkizuneko teknikariak.<br /><br />
Honek darama datozenentzat erabat prestatutak egon ahal dituztela
Teleneurri eta Telekudeaketaren Legezko araudiak, erraztuko zezaten urruneko eran bezeroaren problematika guztia kudeatzeko prozesuak Teleprozesaketaren bidez.";
$empresa_innovacion8="Bezeroen Kudeaketentzat IKT Berrikuntzaren plana.";
$empresa_innovacion9="IKT Berrikuntzaren Plana (Teknologia Berriak) inplementatzen ari garena,
bere bezeroei kudeaketa zuzeneko bat eskaini saiatzen da gure web orri berriaren bidez, plantea diren behar guztiak erraztuz dagokionez gure enpresari, zuzenean bere bizilekutik eta mugitzeko beharrik gabe gure bulegoei.<br /><br />
Kudeaketak egin ahal izango ditu bezala:";
$empresa_innovacion10="- Horniduraren Puntu berri bat alokatu.<br />
- Edozein aldaketa bere kontratuan (Tarifak, Potentziak, Bizilekuak, Banku-kontua...).<br />
- Fakturen kopiak inprimatu jada igorriak izan dira.<br />
- Informazioa eta Ziurtagiriak kWhean bere kontsumoei buruzko edota fakturatutako zenbatekoak.<br />
- Kontadoreen Irakurketak komunikatu baizik hartu posiblea izan da.<br />
- Edozein Matxura komunikatu edo Intzidentzia.";
$empresa_innovacion11="Hau da, kudeaketaren edozein behar gure Orrialdean Bulego Birtualaren bidez egin ahal izango da Weba.";
?>