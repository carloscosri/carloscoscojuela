<?php
$titulo_web="AUDINFOR SYSTEM - Oficina Virtual";
$pie_imagen="AUDINFOR SYSTEM S.L.";
$pie="Garatutako web Gunea por Audinfor System S.L.";
$w3c_html="Baliozkotzea W3C HTML";
$w3c_css="Baliozkotzea W3C CSS";
$menu1="Enpresa";
$menu2="Eskaintzak eta Kontratazioa";
$menu3="Informazioa Bezeroari";
$menu4="Bulego Birtuala";
$menu5="Ataria Sige Energia";
$datos_empresa="AUDINFOR SYSTEM, SL | C/ PANAMA 2 LOCAL 2 ZARAGOZA Tel. 976312018";
$menu3_2="Informazioa<br />Bezeroari";
$menu_idiomas1="Espainierara hizkuntza aldatzea";
$menu_idiomas2="Hizkuntza aldatu a Catalan";
$menu_idiomas3="Euskarara hizkuntza aldatu";
$menu_idiomas4="Galizierara hizkuntza aldatzea";
$menu_idiomas5="Ingelesera hizkuntza aldatzea";
$idioma_activo="Hizkuntza aktiboa";
$error_identificacion="Hau seccion bakarrik da erabiltzaile identifikatuentzat.";
$nota_pdf1="*Dokumentazioa Esta dago formatu PDF-an, artxibo hauek ikusteko, programa Adobe Acrobat Reader instalatua izatea behar du, ez badu programa hau deskargatu ahal du (doakoa) hemendik:";
$nota_pdf2="Acrobat Reader Adobea deskargatu";
$poblacion_empresa="ZARAGOZA";
$banner_sigeenergia="Sarbidea Atarira";
$nota_campos_obligatorios="ARRETA: Bakarrik arlo garrantzitsuak con * nahitaezkoak dira.";
$tlf_averias="";
?>