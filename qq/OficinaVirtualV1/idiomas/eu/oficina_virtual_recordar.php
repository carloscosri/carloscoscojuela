<?php
$oficina_virtual_recordar1="Bere segurtasun beregatik ahaztutako pasahitzaren informazioa, bakarrik telefonoan erabiltzaileari emana izango zaio edo helbidea eta-ematen digun maila.";
$oficina_virtual_recordar2="Erabiltzailea";
$oficina_virtual_recordar3="Telefonoa";
$oficina_virtual_recordar4="E-mail";
$oficina_virtual_recordar5="Onartu";
$oficina_virtual_recordar6="Itzuli";
$error_recordar_vacio="Erabiltzailearen bere kopurua idatzi behar du eta behintzat kontaktuaren datuetako bat";
$error_no_usuario="Ez dago erabiltzaile hori.";
$error_telefono="Telefono bakarrikak izan ahal du carácteres zenbakizkoak.";
$error_mail="Ez du idatzi el eta-maila zuzenki.";
$recordar_ok="Guri pondrémos en contácto zurekin bere kodea errazteko.";
?>