<?php
$informacion_consejos_vitroceramica1="Energia Aurrezteko kontseiluak - Bitrozeramika";
$informacion_consejos_vitroceramica2="Elektrikoa janaria prestatzen du: gaineko bitrozeramika";
$informacion_consejos_vitroceramica3="Sukalde bat da. Honen bortxaren tokiak izan dira serigrafiadas azalera lau baten inguru erresistentzia maila altu batekin bitrozeramika pisura, kolpeetara eta diferentzietara tenperaturaren. Beroa lortzen da kokatutako erresistentzia elektriko batzuen bortxaren tokien azpian eta, kasu batzuetan, erresistentzia horien konbinatutako ekintzaren eta lanpara halogeno batzuk.<br /><br />
Ereduen arabera eta fabrikatzaileak, izan ahal dute 2, 4 edo bortxaren 5 puntu. Alboko toki bat duten ereduak daude bajara funtzionatzen duena sustatzen du (150 W) eta epeldutakoak mantentzeko erabiltzen dena elikagaiak. Ilargi batezbestekoaren berogailuen eranstea bortxaren tokietako batean, luzatutako ontzien erabilera uzten du.
Bere ezaugarriak direla medio kanpoak erabilera erraz gehiagotako garbi eta segurua sukaldea da eta zehatza.<br /><br />
Aginteen egoerak hurrengoak izan ahal dituzte: ";
$informacion_consejos_vitroceramica4="1. <strong>Gainekoan. </strong> Oso erabilgarriak Izaten da, berritzean ordezkapenei buruz denean de halako aparatu bezala janaria prestatzen du, edo beste antzeko batzuk.<br /><br />
2. <strong>Independentziaren panelean.</strong> erabakitako egoera batean aginteak jartzea Uzten du, esate baterako, altzariaren biseraren azpian, eta hau lo babesten du zikinkeriaren.<br /><br />
3. <strong>Labera barne hartutakoak.</strong> labera barne hartutako aginteak lan handiagoaren azalera bat eskaintzeko abantaila dute.";
$informacion_consejos_vitroceramica5="Bitrozeramika motak (elementuen arabera berogailuak):";
$informacion_consejos_vitroceramica6="Gaineko Bitrozeramiken agerpenetik, berogailu elementuak erabilitakoak garatu dira eta tinko dibertsifikatuta. Enplegatuak teknologiei dagokienez hiru talde handi daude: erresistentzia distiratsuak, halogenoak eta Indukzioaren.<br /><br />
Puntuen gehiago ohizko potentzia maximoak bortxaren 1200, 1700 eta 2100 dira W. gaur egungo sistemek beheko balioetara bortxaren potentzia erregularra egokiei uzten diete agintearen jarreraren arabera.<br /><br />
Bortxaren puntuak halogenoekin benetako abantailak aurkezten dituzte, hala nola: ";
$informacion_consejos_vitroceramica7="- Bero eta argiaren jaulkipena argazkia.<br />
- Beroketaren azkartasuna.<br />
- Ontziaren kalitatearen menpekotasun adingabea.<br />
- Plakaren agintera erreakzioaren malgutasuna eta azkartasuna.<br />
- Manipulazioaren akatsen aurka segurtasun handiagoa berehala ikustean konektatzen bada.";
$informacion_consejos_vitroceramica8="Erabileraren gomendioak";
$informacion_consejos_vitroceramica9="Lea adeitasunez sukaldearen fabrikatzailearen eskuliburuan izandako argibideak. Han ezagutuko du cual bortxa motari egokitua beroaren aginte erregulatzailearen jarrera da, eta elikagaien kopurua janaria prestatzera.<br /><br />
Funts barreiatzaile lauarekin sukaldeko tresnak erabili ohi ditu eta lisoa eta diametro berdin bat edo baino gehiago bortxaren tokia. Bestela du haiek, eredu eta marken barietate handi bat dago aukeratzera arte lo bereziki diseinatutak izan direna bitrozeramiketan erabilitakoak izateko. Ordea aluminiozko ontzien lanbidea ez da aholkatzen edo lokatzaren lapikoak.<br /><br />
Sukalde elektrikoak bitrozeramikek barne hartzen dute, normalean bortxaren toki bakoitzeko seinaleztapen bat, argiztatzen dena beraren tenperatura handiagoa denean de 50 º C eta horrela, behin jarraitzen du deskonektatu da, muga honen tenperatura baxuraino.<br /><br />
Automatikoki ontziak kentzean plakak deskonektatzen dituzten ereduak daude, beraz ez dago kontsumorik nahiz eta jarraitzen duen konektatutakoa.";
$informacion_consejos_vitroceramica10="Mantentzea:";
$informacion_consejos_vitroceramica11="Ia bitrozeramika ez zehatza sukalde elektrikoa mantentzea. Erabilera bakoitzaren ondoren azalera garbitzea gomendagarri da da, beroaren fokuak itzali ondoren, oihal batekin edo papera de janaria prestatzen du distira berreskuratzen du. Azaleraren inguruan atxikitako hondakin sendoak geratu badira, hasi gomendatzen arraspa batekin baino lehen hoz direna eta, gero, enjuagar eta plaka lehortu.<br /><br />
Eskura Sukaldearen Fabrikatzailearen bermea exijitzen du eta eduki ohi du. Indarraldiaren bere aldian zehar matxuren konponketa estaltzen du batzuetan gertatu ahal dute.";
$informacion_consejos_vitroceramica12="ABANTAILAK:";
$informacion_consejos_vitroceramica13="Bere diseinuagatik gaineko bitrozeramikak eta bukatutako erabat berria eta erakargarria, eta bistaratze, malgutasun eta azkartasunaren bere ezaugarriak direla medio, produktu oso lehiakor bat dira eta beste mota batzuen aurrean onuragarria sukaldeak.";
$informacion_consejos_vitroceramica14="Abiadura, zehaztasuna eta bistaratzea: Beste sistema bati beroketa konparagarriaren abiadura bat lortzenek diote eta energia. Bere erantzun-denbora ia berehalako da, jardunean plakak ikusiz.";
$informacion_consejos_vitroceramica15="Estetika: Lerro dotoreekin eta garbitutakoekin eta erabat azalera lisoek, beste mota batek dotorezia helezin bat lortzen dute gainekoak.";
$informacion_consejos_vitroceramica16="Erabilera eta garbiketaren erraztasuna: Eltze, zartaginak eta ontziak oro har resbalan bere azaleragatik gabe bascular, eta horregatik oso erraza izaten da bere manipulazioa, eta iraulketa da baina zaila beste mota batzuetan sukaldeak, eta bere azalera guztiz leuna ez dadin izan. Al ez artekak izan edo sarbide zailaren tokiak, edo garbiketa trinko bat, hau behar duten beste toki batzuk erraza izaten da. Plaka hotza egon dadila garbiketa normala egiteko gomendatzen da edo epeldutakoa.";
$informacion_consejos_vitroceramica17="Sendotasuna eta erresistentzia: Bitrozeramika material oso gotor bat da.";
$informacion_consejos_vitroceramica18="Segurtasuna: Energia elektrikoaren erabileraren segurtasun berera, beroa bortxaren tokietan, kontzentratzen dela gehitzen da erreduren arriskurik gabe gainerakoa ukitzen bada azalera. Al ez erregaia egon, ez dago ustekabeko leherketaren arriskua.";
$informacion_consejos_vitroceramica19="Elektrikoa janaria prestatzen du: indukzioak gaineko bitrozeramika:";
$informacion_consejos_vitroceramica20="Sukaldea indukzioak bitrozeramika hotza, garbia, da la erabilera erraz gehiagotako, segurua, modernoa eta azkarra.<br /><br />
Sukalde elektrikoa indukzioak bitrozeramika leuna, erretzaile gabe, da eta bere azalera beira zeramiko bat da, erresistentzia maila altuarekin pisura eta kolpeetara. Bortxaren tenperatura bakoitzean bere zehaztasuna zehatza da, gainera plaka ez da berotzen.<br /><br />
Sukalde hauetan beroa ontzi berean sortzen da janaria prestatzeko elementu batek sortutako eremu magnetiko batetik aurrera kokatuta dago azaleraren azpian bitrozeramika, eta horregatik ez dago beroaren galerak.";
$informacion_consejos_vitroceramica21="Erabilera eta mantentzearen gomendioak:";
$informacion_consejos_vitroceramica22="Bortxa hasi alta gehiago zenbakikuntzarekin agintearen beroaren, ondoren jaisteko nahi izandako jarrerara.<br /><br />
Bakarrik beroa nahi dio leuna, zehazki lortzera zenbakikuntza baxuak erabili nahi izandako beroa.<br /><br />
Kokatzean jarreraren inguruan 0 agintearen, bat-batean beroa emateko uzten da (irakitea ekitaldian atxilotzen da).<br /><br />
Bestela bortxaren tokiaren inguruko ontzia dago, honek beroa ez ematen du nahiz eta konektatuta dago.<br /><br />
Gaineko bitrozeramiken garbiketarako indukzioak, oihal heze bat erabiltzearekin nahikoa izango da (paperen, saretoa, spontex, edo antzekoa). Baina beharrezkoa bada, merkatuan, izango daitezen egokitutako garbikaria erabili ahal dute enjuagando eta ondo lehortzen bere distira berreskuratzeko bere erabileraren ondoren.<br /><br />
Energiaren aurrezki nabarmen bat lortuko da estalitako ontziak erabiltzen edo estalitako baliabidea posiblea izan dadinean.";
$informacion_consejos_vitroceramica23="Energiaren gutxi gorabeherako kontsumoa";
$informacion_consejos_vitroceramica24="Jarduera";
$informacion_consejos_vitroceramica25="Gasa (kWh)";
$informacion_consejos_vitroceramica26="Elektrizitatea (kWh)";
$informacion_consejos_vitroceramica27="Lisakinaren xerra";
$informacion_consejos_vitroceramica28="Altzairuzko lapikoan egosita juduak (4 pertsona)";
$informacion_consejos_vitroceramica29="Arroza (4 pertsona)";
$informacion_consejos_vitroceramica30="Sukalde mota honentzat egokitutako ontziak:";
$informacion_consejos_vitroceramica31="Material ferromagnetikoaren ontziak, funts laua, lisoak erabili behar dute eta lo lodia posiblea. Jabetza hau eta erraz frogagarria erakarria izan behar izango den iman batekin ontziak. Beraz altzairuzko tresnak erabili ahal dituzte esmaltado, burdinezkoa urtuta edo material ferromagnetikoren bat izan dezaten altzairuzko herdoilgaitza.<br /><br />
Mota funtsarekin ontziak sandwicha, con hiru edo material ferromagnetikoren bat izan dezaten geruza gehiago, kasuen gehiengoan baliagarriak dira.<br /><br />
Nahiz eta masa ferromagnetikoaren kopuru minimo bat beharrezkoa den funtzionatu ahal izateko, ez bortxaren tokiaren inguruan tresna metalikoak utzi. Inola ere tresnak aluminiozko erabili behar dira edo lokatzaren, ez funtzionatuko";
$informacion_consejos_vitroceramica32="ABANTAILAK:";
$informacion_consejos_vitroceramica33="Energiaren hornidura: Elektrizitatea etxe guztietan eskatutakoa dago, eta horregatik ez da kezkatu beharrezkoa joateko energia bilatzera con janaria prestatu edo gordetakoa.";
$informacion_consejos_vitroceramica34="Abiadura, presioa eta bistaratzea: Energiaren beste sistema bati beroketa konparagarriaren abiadura bat lortzen diote. Bere erantzun-denbora berehalakoa da, jardunean plakak ikusiz pilotu baten bidez censor tenperaturaren.";
$informacion_consejos_vitroceramica35="Estetika: Lerro dotoreekin eta garbitutakoekin, eta erabat azalera lisoek, edozein gaineko motak dotorezia helezin bat lortzen dute.";
$informacion_consejos_vitroceramica36="Erabilera eta garbiketaren erraztasuna: Eltze, zartaginak eta ontziak oro har resbalan bere azaleragatik gabe bascular, eta horregatik oso erraza izaten da bere manipulazioa, eta iraulketa zaila da beste mota batzuetan sukaldeak eta bere garbiketa trinkoa eta plakak hotza jarraitzen duelako edo transmititutako bero arin batekin ontzi berak, bere garbiketa oihal heze bat pasatzera mugatzen da. Sendotasuna eta erresistentzia: Bitrozeramika material oso gotor bat da.";
$informacion_consejos_vitroceramica37="Segurtasuna: Energia elektrikoaren erabileraren segurtasun berera, gehitzen da bortxaren tokiek ez dutela erreduren arriskua ontzia kentzean ukitzen badira, gainekoak hotza jarraitzen duelako.";
$informacion_consejos_vitroceramica38="Kontseilu Praktikoak";
$informacion_consejos_vitroceramica39="Janaria prestatzea eskuratzen du ur gutxirekin eta estalitako ontziekin edo estalitako baliabidea.";
$informacion_consejos_vitroceramica40="Presio-eltzearen erabilera oso gomendagarria da, energiaren % 50eraino. aurreztu ahal du";
$informacion_consejos_vitroceramica41="Kontuan hartuta bortxaren tokiak, deskonektatu ondoren irakitea mantentzen dute eta 5 eta 7 minutu artean elikagaien beroa gehiago (izan ezik indukzioa), itzal bat bortxa bukatu baino lehen gutxi.";
$informacion_consejos_vitroceramica42="Janaria prestatzeko orduan ontzien funtsa tokia baino gehiago arinki izan behar dela kontuan izan ohi du bortxaren beroa ahalik eta gehien aprobetxatzeko.";
$informacion_consejos_vitroceramica43="Horniduraren erabilera gomendagarria da funts lodi barreiatzailearekin ba tenperatura.";
$informacion_consejos_vitroceramica44="Bitrozeramika sukaldeekin indukzioak ustiapen energetikoa guztizkoa da: aurkitzen du si dago edo ez dago bere azaleraren inguruko ontzia, jokatzen bakarrik lehen kasuan.";
?>