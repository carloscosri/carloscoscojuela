<?php
$oficina_contratacion1="Horniduraren puntu berri bat alta emateko eskakizun Administratiboak:";
$submenu_contratacion_oficina1="Alokatzeko tarifak:";
$submenu_contratacion_oficina2="- Bonu Soziala (3 Kw Baino gutxiago/ordua).";
$submenu_contratacion_oficina3="- Kontratuak ere 10 Kw/ordua con edo gabe discriminacion ordukoa (2.0. Tarifak.A eta 2.0 DHA).";
$submenu_contratacion_oficina4="- Kontratuak 10etik 15era Kw/ordua con edo gabe discriminacion ordukoa (2.1. Tarifak.A eta 2.1 DHA).";
$submenu_contratacion_oficina5="- Kontratuak aurrera 15 Kw/ordua (3.0. Tarifak.A eta 3.1.A eta 6.x).";
$oficina_contratacion2="OHARRA: Tarifen inguruan edozein informazio gehigarrirentzat alokatzera eta hornidura mota, zuzenean gure bulegoetan kontsultatu behar dute.";
$oficina_contratacion3="Bonu Sozialerako baldintzak:";
$oficina_contratacion4="Bonu Soziala bakarrik da de aplicacion pertsona fisikoei eta bere hornidurak parea zuzen da erabilera en
ohiko etxebizitza eta izango dadina a hurrengo taldeetako bat:<br /><br />
Pertsona fisikoak con batek bere ohiko etxebizitzan 3kW baino gutxiago kontratatutakoa sustatzen du. Se ezarriko dio
Bonu Soziala Automatikoki.<br /><br />
Pentsio minimo bat jasotzen duten 60 urteko adin minimoarekin bezeroak.<br /><br />
Erretiroagatiko kotizatu gabeko pentsioak eta baliaezintasuna jasotzen dituzten 60 urte baino gehiagoko bezeroak, halaber
ostatuen onuradunak iraungitako Seguruaren Zahartzaro eta baliaezintasunaren Derrigorrezkoa.<br /><br />
Familia ugariak.<br /><br />
Guztiekin familiak langabezia egoeran bere kideak.";
$oficina_contratacion5="Katastral Erreferentzia.";
$oficina_contratacion6="CUPS (Horniduraren bere Puntuaren Identifikatzailearen Kodea) beste Komertzializatzaile baten etor ari den egoeran.";
$oficina_contratacion7="Eskaera hau berriro ere hornidurak soilik izaera informatzailea izango du. Gure enpresa zurekin harremanetan jarriko da datozen egunetan pertsonalizatutako eskaintza ekonomiko bat aurkezteko, posta-elektroniko edo posta arruntaren bidez.<br /><br />
Esandako aurrekontua onartu ondoren izango da kontratua gure enpresarekin amaitzea mozten dugunean energia elektrikoaren horniduraren hornidura eta baldintzen puntuan eskatutakoak.<br />
Informazio hau jaso ahal duen alokatu nahi duen potentziari dagokion inprimakia bete behar izango du, taldeen arabera ondoren banakatzen dira.";
$oficina_contratacion8="Alokairuaren kontratu edo jabetzaren titulua.";
$oficina_contratacion9="Bizigarritasunaren zedula.";
?>