<?php
$error_rellenar="Behar dituzte bete arlo nabariak bezala nahitaezkoak.";
$error_fecha="Data gaizki idatzi da. Hurrengo formatuak enplegatu behar du: 18-03-2009.";
$error_fecha_posterior="Idatzitako datek ondorengoak gaur egungo datara. izan ezin dituzte.";
$error_fecha_inicio_mayor="Hasieraren data data baino handiagoa izan ezin da bukaera.";
$solicitud_ok="Bere eskaera arrakastaz ordezkaria izan da.";
$oficina_informacion_facturas1="KONTSUMO AKTIBOA";
$oficina_informacion_facturas2="P1 Punta";
$oficina_informacion_facturas3="P2 Laua";
$oficina_informacion_facturas4="P3 Ibarra";
$oficina_informacion_facturas5="KONTSUMO ERREAKTIBOA";
$oficina_informacion_facturas6="ZENBATEKOAK";
$oficina_informacion_facturas7="Activa";
$oficina_informacion_facturas8="Erreaktiboa";
$oficina_informacion_facturas9="Maxímetro";
$oficina_informacion_facturas10="Deskontuak";
$oficina_informacion_facturas11="Karguak";
$oficina_informacion_facturas12="Oinarria";
$oficina_informacion_facturas13="BEZa";
$oficina_informacion_facturas14="GUZTIZKOA:";
$oficina_informacion_facturas15="Inprimatzea";
$error_no_facturas="Gaur egun ez dago bezero honekin lotutako fakturak gure datu-basean.";
?>