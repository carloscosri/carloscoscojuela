<?php
$contratacion_legislacion3="ORDENA 3860/2007 ITCa";
$contratacion_legislacion4=", abenduaren 28ko, horren bidez 2008ko urtarrilaren 1etik aurrera tarifa elektrikoak gainbegiratzen dira.";
$contratacion_legislacion5="ERREGE-DEKRETUA 385/2002";
$contratacion_legislacion6=", Honen bidez, apirilaren 26ko abenduaren 26ko 2018/1997 Errege-Dekretua aldatzen da. Honen bidez, energia elektrikoaren kontsumo eta zirkulazioen neurriaren puntuen araudia onesten da. (BOR 02-05-14).";
$contratacion_legislacion7="ERREGE-DEKRETUA 1955/2000";
$contratacion_legislacion8=", Honen bidez, abenduaren 1eko garraioaren jarduerak arautzen dira, energia elektrikoaren instalazioen baimenaren banaketa, merkaturatzea, hornidura eta prozedurak. (27. BOE-12-00).";
$contratacion_legislacion9="ERREGE-DEKRETUA-LEGEA 6/2000";
$contratacion_legislacion10=", Premiazko Neurrien ekainaren 23ko Eskuduntzaren Areagotzearen Ondasun eta Zerbitzuen Merkatuetan. (24. BOE-06-00).";
$contratacion_legislacion11="LEGEA 54/1997";
$contratacion_legislacion12=", Sektore Elektrikoaren azaroaren 27ko. (28. BOE-11-97).";
$contratacion_legislacion1="Espainiako legeria Elektrikoa";
$contratacion_legislacion2="Sail honetan esteka batzuk dokumentuetara Espainiako legeria elektrikoari dagozkion aurkituko du.";
$contratacion_legislacion13="Industria, Turismo Eta Merkataritza Ministerioa";
?>