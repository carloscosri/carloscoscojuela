<?php
$contratacion_contratar_potencia1="Aukeraketa de Sustatzen du Alokatzera";
$contratacion_contratar_potencia2="Hornikuntzaren kopuruaren eta ezaugarrien araberakoa da elektrizitatearen kontratuaren potentzia bere etxearen: arabera zer eta zenbat talde eduki ohi du, potentziak esanguratsuki aldatzen du.<br /><br />
Bat alokatzeak desegokia bere beharretara sustatzen du ez da ezer egokia. Si esleipena gehiago sustatzen du eta honen behar du, ez bakarrik dirua alferrik galtzen ari da, baizik eta ere beharrezkoaren energia gehiago gastatuz ingurumena kalte egiten ari da. Eta si, aitzitik, esleipena gutxiago, desatseginak produzitu ahal dira ICPak eragindako horniduran mozten duzu (Potentzia-kontrolatzeko etengailua).";
$contratacion_contratar_potencia3="Handiagoaren taldeen erabilerak sustatzen duela potentziaren kalkulua da ez da egiten aldi berean. Bat da sustatzen du gomendatutako kontratu minimoaren tarifa batean oinarriturik 2.0 orokor.<br /><br />
Baduzu:";
$contratacion_contratar_potencia4="- Berehalako termo elektrikoa.<br />
- Erradiadore elektrikoak.<br />
- Metaketaren berogailua.<br />
- Bero-ponpa.<br />
- Aire girotua.";
$contratacion_contratar_potencia5="Harremanetan jartzela aholkatzen dizugu gurekin bezeroentzako arretaren telefonoan 972 59 46 91 edo gure bulegoetan zure kasua ikasten dugu gehiago exhaustivamente.";
?>