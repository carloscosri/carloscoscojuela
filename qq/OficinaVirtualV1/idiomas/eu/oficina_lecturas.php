<?php
$error_rellenar="Irakurketaren datak eta kontadorearen kopuru batek idatzi behar du.";
$error_fecha="Data gaizki idatzi da. Hurrengo formatuak enplegatu behar du: 18-03-2009.";
$error_lectura="Idatzi behar du behintzat irakurketa bat.";
$lectura_ok="Bere irakurketa ordezkaria izan da.";
$oficina_lecturas1="Aldi Bakarrik baten irakurketa Tarifa-zerrenda";
$oficina_lecturas2="Aldi Batzuekin irakurketak Tarifa-zerrendak";
$oficina_lecturas3="Aldia";
$oficina_lecturas4="P1 Punta";
$oficina_lecturas5="P2 Laua";
$oficina_lecturas6="P3 Ibarra";
$oficina_lecturas7="Aktiboa";
$oficina_lecturas8="Erreaktiboa";
$oficina_lecturas9="Maxímetro";
$error_varias_lecturas="Aldi Bakarrik baten Irakurketa atalean 'irakurketak Tarifa-zerrendak' idatzi badu Aldi Batzuekin Irakurketa atalean 'irakurketak ez idatzi dira behar Tarifa-zerrendak'.";
$error_numero_lectura="Ez du idatzi zuzenki irakurketa bat. Izan behar dute 8 osorekin kopuru bat gehienez eta 3 decimales.";
$error_fecha_posterior="Idatzitako datek ondorengoak gaur egungo datara. izan ezin dituzte.";
?>