<?php
$contratacion_contratar_requisitos1="Kontrataziorako Baldintzak";
$contratacion_contratar_requisitos2="Etxebizitza edo tokikoa zu okupatzen du edo lana emango du, elektrizitate instalazio bat du bakarrik konektatu falta sare orokorrera para arte zerbitzuan. Konexioa egin baino lehen, ziurta de hurrengo dokumentazioa duena:";
$contratacion_contratar_requisitos3="Ordezkaritzaren buletina hedatutako Industriaren baimena emandako Argiketari Instalatzaile batek.";
$contratacion_contratar_requisitos4="Bermatzeko bere etxebizitzaren elektrizitate instalazioa reune segurtasunaren berme nahikoa, indarreko legeriak exijitzen du babes batzuk bere etxebizitzan instala direla eta baimena emandako Instalatzaile batek Buletin bat, konfirmatua hedatzen duena estatu zuzena eta instalazioaren funtzionamendua.
Eraikuntza berriaren etxebizitza baten kasuan, esandako Buletina se eta honi higiezinen enpresa enpresak erraztuko dit zu etxebizitza eskuratu zuen.";
$contratacion_contratar_requisitos5="Etxebizitza lehen lana dutena izan bazen eman ahal izango duen beste batetik jabea, hau dagokion Buletina izan behar izango luke beraren antzinatasunak 20 urteak ez gaindi ditzanean. Dena den, bere segurtasun beregatik eta la bere senideen, aholkatzen diogu harremanetan jarri baimena emandako Instalatzaile batekin bere instalazioa gainbegiratzen du eta egokitzen du, bereziki Koadroa Aginte eta Babesaren segurtasun egokien elementuak izango ditzan edozein arrisku elektrikoaren aurrean bere instalazioa ziurtatzen duena.
Si potentzia zu alokatu nahi du maximoa baino gehiago da aurreko jabearen Buletinean agertzen duen onargarria, baimena emandako Instalatzaile baten bisita eskatzen duen beharrezkoa ere da bere instalazioa egokitzen du eta Buletin berri bat hedatzen dio.";
$contratacion_contratar_requisitos6="Udaleko lizentzia.";
$contratacion_contratar_requisitos7="Energia elektrikoaren horniduretarako beharrezkoa da objektu bikaina izatera joaten diren eraikuntzetan erabilera edo bere erabileran aldaketaren.";
$contratacion_contratar_requisitos8="Etxebizitzaren alokairuaren kontratu edo jabetzaren titulua edo tokikoa.";
$contratacion_contratar_requisitos9="Nortasun Agiri nazionala.";
$contratacion_contratar_requisitos10="Bere banku- kontu korrontearen kopurua edo aurrezki-libreta.";
$contratacion_contratar_requisitos11="Bere erosotasunarentzat, aholkatzen diogu elektrizitatearen ordainagiriak bere Bankuan helbideratzen ditu edo Aurrezki-kutxa. Jakina da, ordainketa ordainagiri baten ez du bere adostasuna eta akats posible bat gertatzekotan, zenbatekoa berehala dirua itzulita izango zaio.";
$contratacion_contratar_requisitos12="Eraikinaren katastral erreferentzia.";
$contratacion_contratar_requisitos13="Azken legezko aldaketen arabera, horniduraren kontratua egiteko nahitaezkoa da, eraikinaren katastral erreferentzia eman. Katastroaren Zuzendaritza Nagusiaren arabera 20 izaeratako kode hau ondasun higiezinen identifikatzaile ofiziala eta derrigorrezkoa da eta egokitua da beraz eraikin guztiak erreferentzia bakar bat dauka katastrala. Kode hau salerosketa berrien eskrituran dago edo ondasun higiezin zergaren ordainagirian (I.B.I.).";
?>