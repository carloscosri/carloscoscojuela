<?php
$informacion_seguridad1="Bere Etxearen Segurtasunerako kontseiluak";
$informacion_seguridad2="Bere elektrizitate instalazioaren segurtasuna eta Babesa";
$informacion_seguridad3="Etxebizitza guztiak Aginte eta Babesaren Koadroa izan behar du. Bere etxebizitza eraikuntza berriaren bada, ez da kezka, jada lo eskatutakoa dauka. Baina zaharra bada, ziurta de bat duena, bere etxearen segurtasunerako garrantzitsua delako eta lo bertan.<br /><br />Babesak bizi direna aginte eta Babesaren Koadroan barne hurrengoak dira:";
$informacion_seguridad4="1. Potentzia-kontrolatzeko etengailua (ICPa)";
$informacion_seguridad5="Energiaren Kontratuaren arabera aldibereko potentzia eskuragarria zehazten duen etengailu automatiko bat da eléctrica.El ICPak bere instalazioa deskonektatuko du, aldi berean konektatutako aparatu elektrikoen potentziaren batura potentzia contratada.De gaindi dezan era hau ez beharrezko arriskuak bere instalazioan. saihesten dira";
$informacion_seguridad6="2. Etengailu Orokorra de Mozten du (IGC)";
$informacion_seguridad7="Etengailu honek behar izanez gero bere instalazioaren guztizko deskonexioa uzten du. Bere instalazioaren babesaren funtzioa ere egiten du, beraz inoiz ez se bere instalazioaren maximo onargarria potentziaren gainbaimena.";
$informacion_seguridad8="3. Etengailu Bereizgarria (ID)";
$informacion_seguridad9="Aginte eta Babesaren Koadroaren babes garrantzitsuaren elementua da, ba edozein istripu elektrikoren pertsonak babesten du. Etengailu bereizgarriak bere instalazioa deskonektatzen du eratorpen bat tresna elektrikoren batean aurkitzen duenean edo punturen batean de la instalación.En etxebizitzek, etengailu bereizgarri hauek, sentsibilitate altaren izan behar dute (30 mA.)";
$informacion_seguridad10="4. Etengailu txiki magnetotermikoak (PIA)";
$informacion_seguridad11="Elektrizitate instalazio guztia zirkuitu batzuek osatutakoa dago (argiztapenak, indarrak, janaria prestatzen du, eta abar..) eta zirkuitu hauen bakoitzak behar dute egoteko babestuta gainkargek eta etengailu magnetotermiko batek kortozirkuitoak, zirkuitu bakoitzaren gaitasunaren arabera barnekoak.";
$informacion_seguridad12=" gomendatzen dio:";
$informacion_seguridad13="Horniduraren kontratu berri bat egin behar badu, aldatzen du edo indarreko legeriaren arabera Aginte eta Babesaren bere Koadroa ordezka dezan arrisku elektrikoen aurrean bere etxeko instalazioa ziurtatzeko. Horrela beilatuko du bere segurtasunagatik eta la bere senideen.<br /><br />30 mAtako etengailu Bereizgarri bat Instalatzen du. Sentsibilitate nahikoaren saihesteko bere instalazioaren horniduran mozten duzu (funtzionamendu desegoki bat gertatzekotan de etxetresna elektrikoetako bat)";
$informacion_seguridad14="Legeria";
$informacion_seguridad15="Behe-tentsioaren elektrizitate instalazioak bildu behar dituzten arauak eta baldintzak definituak izan ziren Energia eta Industriaren Ministerioak eta 842/2002 Errege-Dekretuan argitaratutakoa, eta bilduta daude en el ";
$informacion_seguridad16="Behe-tentsioaren Elektroteknikari araudia";
?>