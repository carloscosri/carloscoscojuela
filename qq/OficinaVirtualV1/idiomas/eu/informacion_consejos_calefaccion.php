<?php
$informacion_consejos_calefaccion1="Energia Aurrezteko kontseiluak - Berogailua";
$informacion_consejos_calefaccion2="Berogailu elektrikoa. Metagailuekin energia aurreztu bero eta tarifaren gaueko 2.0:";
$informacion_consejos_calefaccion3="Beroaren metagailuak aldi batean zehar bero-energia gordetzeko aparatu trebatuak dira denboraren, gero askatzeko astiro, fluxuarekin controlable. Ingurune berogailuarentzat elementu egokiak dira gauaren orduak beroa Tarifa honen erabiltzaileak, kontsumitutako energiaren prezioan % 53aren deskontu bat lortzen du orduetan zehar, gauekoak, eta ordu kilowattaren prezioan % 3ko gainordain bat dute kontsumitutakoa egunean zehar.";
$informacion_consejos_calefaccion4="Metagailu motak:";
$informacion_consejos_calefaccion5="Bi metagailu mota, eta bere diferentzia daude beroa esteko era da:<br /><br />
<strong>Metagailu estatikoak:</strong> aparatu mota honetan, beroa gehien bat esten da erradiazioak, inguratzailearen bidez metalikoa, eta hitzarmen natural batek, mesedea zirkulatu ahal duen airera karkasan dauden kanal batzuen bidez eta metagailuaren nukleoan. Horretarako, aparatuaren beheko zatian airearen sarrera bat dute eta goialdeak irteera bat. Irteeraren airea konporta baten bidez arautzen da.<br /><br />
<strong>Metagailu dinamikoak:</strong> metagailu dinamikoak antzekoak dira estatikoei. Erradiazioak beroa ere esten du, inguratzailearen bidez metalikoa, baina la convección zirkularra dauden hodi edo kanalengatik aireak egiten duen turbina baten bidez bortxatua da metagailuaren nukleoan.";
$informacion_consejos_calefaccion6="<strong>1.  Metagailuaren blokea:</strong> metagailu nukleoa material erregogorraren plakek osatutakoa dago (magnesita) bero espezifiko altu bat aurkeztu behar dutena eta dentsitate maximoa, porque horien araberakoa da beroa gordetzeko gaitasuna bi parametro. Nukleoan kargaren denboraldiaren amaieran lortzen den tenperatura maximoa metagailuaren 600etik 700era da º C.<br /><br />
<strong>2. Berogailuen erresistentziak:</strong> berogailu elementuak (erresistentziak) barrunbeetan sartzen dira edo metagailu nukleoaren barnekoan kokatutako kanalak, beraz masa guztia berotzen dute del refractorio uniformeki.<br /><br />
<strong>3. Isolamendu termikoa:</strong> metagailu blokea beroa mantentzeko isolatzaile termikoarekin inguratutakoa dago meta eta aparatuaren altzariaren azaleko tenperatura mugatu.<br /><br />
<strong>4. Segurtasunaren gailua:</strong> Eskuarki metagailuek bat dute limitador termikoa edo segurtasunaren termostato baten, aparatua deskonektatzeko si, edozein arrazoik, tenperaturaren ebaluazio desegoki bat gertatzen da (esate baterako, disfuntzio bat karga sistemaren arautzearen sisteman).<br /><br />
</strong>5. Termostatoa:</strong> bi, bat Dago karga erregularrarentzat eta beroaren jaulkipenerako beste bat.";
$informacion_consejos_calefaccion7="ABANTAILAK:";
$informacion_consejos_calefaccion8="Instalatu ahal dira bai eraikuntza berriaren eraikinetan bezala etxe zaharretan.";
$informacion_consejos_calefaccion9="Ez daude uraren zirkuituak, fuga eta tanta-erortzeen aukera ezabatuz.";
$informacion_consejos_calefaccion10="Ez dute behar erregaiaren gordailua.";
$informacion_consejos_calefaccion11="Instalazio arrunta, lanik gabe.";
$informacion_consejos_calefaccion12="Ekologikoak, ba ez dute igortzen kerik eta kontsumoaren puntuan ezagutzen den energia garbia erabiltzen dute: Elektrizitatea.";
$informacion_consejos_calefaccion13="Guztizko segurtasuna eta al ez erregaiak egon, ez dago ustekabeko leherketaren arriskua.";
$informacion_consejos_calefaccion14="Ia existitzen ez den mantentzea.";
$informacion_consejos_calefaccion15="Berogailua 24etan. ematen dute egunaren.";
$informacion_consejos_calefaccion16="Fakturazioaren kostu orokorrak gutxitzen direnez, hozkailuen kontsumoa mugitzean, garbigailuak, plater-garbigailua, ur-berogailuak, argiztapena, eta abar, Tarifako ordutegira Gauekoa, aparatuaren deskribapenean lehen adierazi.";
$informacion_consejos_calefaccion17="Bere itxura estetika da aurreratutako diseinuaren.";
$informacion_consejos_calefaccion18="Automatizables ahalik eta gehien.";
$informacion_consejos_calefaccion19="Berogailu elektrikoa kable bidez metaketaren sistemekin distiratsua (oinarria + laguntza, gaueko tarifa 2.0):";
$informacion_consejos_calefaccion20="Berogailu elektrikoa kable bidez distiratsua berogailuaren sistema bat da ez agerikoa, ordutegiaren erabilerari dagokienez bi zati ezberdin integratzen ditu eta bere funtzionamenduagatik osagarriak: laguntzaren oinarri eta berogailuaren berogailua.";
$informacion_consejos_calefaccion21="Oinarriaren berogailua:";
$informacion_consejos_calefaccion22="Zoruan eskatutako berogailua sistema hau gauaren aldian zehar esklusiboki konektatutakoa geratzen da, gaueko tarifak kWharen prezioan % 53ko murriztapen bat daukanean.
Zoruan eskatutako oinarriaren berogailuak, denboraldian zehar beroa metatzen du gutxitutako tarifaren, itzultzen duena a kopuru orduen restantes.La denboraldian zehar energia gastatu gabe girotzen du zoruan metatutako beroaren kanpo tenperaturak erabakitakoa etortzen da, zunda bat duelako tenperatura aurkitzen duena.<br /><br />
Barneko tenperatura ekarpenak dela eta maila batzuetan areagotzen delako sortuta doako beroaren pertsonek, argiztapena, ur bero sanitarioa, etxetresna elektrikoak, egunean zehar intsolazioa, eta abar, ekarpen hauen kopurua aurretik jakin ezin dena dena eta ez detectable zundak atzerrian kokatutako erregulatzailearen eta honek ez funtzionatzen duena eguneko orduetan zehar,. egonaldi bakoitzean nahi izandako konfortaren mailaren uzten duen laguntza osagarriaren berogailu sistema bat izan nahitaezkoa da arautze zehatz bat.";
$informacion_consejos_calefaccion23="Laguntzaren berogailua:";
$informacion_consejos_calefaccion24="Sistema hau berogailu independentea, de sustatzen du gutxitutakoa puestoque bakarrik oinarriaren sistemak aurkitu ezin duen barneko tenperaturaren aldaketak konpentsatzen esku hartzen nau edo zuzendu, metaketak oinarriaren berogailuaren osagarria da. Zoruan integratutakoak berogailuen kableak da, oinarriaren sistemaren kableen gainetik, sare elektrikora konektatutakoak termostato automatiko baten bidez de gela bakoitzean girotzen du edo menpekotasuna.<br /><br />
Bere funtzioa da mantendu zehar 24etan egunean erabiltzaileak nahi izandako tenperatura zehatza gela bakoitzean.
Laguntzaren berogailurako beste aukera bat instalazioa da de convectores tenperaturaraino beroaren ekarpena osatzeko elektrikoak gela bakoitzean aldez aurretik aukeratuta.";
$informacion_consejos_calefaccion25="Erabileraren gomendioak:";
$informacion_consejos_calefaccion26="Nahi izandako tenperaturen doikuntza bulego desberdinetan kokatutako termostatoen bidez berogailuaren bere sistemaren konfort handiena lortzeko oso garrantzitsua da.<br /><br />
Bere etxebizitza eraikuntza berriaren, hezetasun bera bada materialen kontsumo bat eragin ahal du arte bat 20 eta % 30a handiagoa de funtzionamenduaren lehen denboraldian normala, independentea de cual berogailuaren sistema izan dadin de ezartzen duena.";
$informacion_consejos_calefaccion27="ABANTAILAK:";
$informacion_consejos_calefaccion28="Garbia: Airearen mugimenduak ez dira gertatzen eta, ondorioz, hautsaren.";
$informacion_consejos_calefaccion29="Osasungarria: Ez du kontsumitzen oxigenorik, ez erabat lehorra airea edo gehiegi. gehiegi berotzen du";
$informacion_consejos_calefaccion30="Kontrolatutako kostua: Bulegoek indibidualizatutako instalazioak beharren arabera gastuaren kontrola uzten du, ordutegiak eta erabilerak. Gainera beroaren doako ekarpenak aprobetxatzen du (eguzkia, argiak, pertsonak).";
$informacion_consejos_calefaccion31="Ikusezina eta isila: Ez dago makineriarik edo elementu mekanikoak begien bistan.";
$informacion_consejos_calefaccion32="Segurua: Pertsonen helmenaz kanpo eta Araudiaren arabera elektrikoki babestuta Behe-tentsioaren Elektroteknikaria, eta al ez erregaiak egon ez dago ustekabeko leherketaren arriskua.";
$informacion_consejos_calefaccion33="Eta gainera: Lanik gabe laguntzaileak, erregaien gordailuak, mantentzea, aurreratutako ordainketak energiaren eta, inbertsioaren kostu adingabearen sistema da.";
$informacion_consejos_calefaccion34="Kontseilu praktikoak:";
$informacion_consejos_calefaccion35="Gela bakoitzean tenperaturaren doikuntza termostatoaren bidez berogailuaren bere sistemaren kontsumoa kontrolatzeko oso garrantzitsua da. Tenperatura bat ezarri de 20ºC ordez 21ºC %10eko energiaren aurrezki bat esan nahi izan ahal du.";
$informacion_consejos_calefaccion34="Kontseilu Praktikoak:";
$informacion_consejos_calefaccion35="Gela bakoitzean tenperaturaren doikuntza termostatoaren bidez berogailuaren bere sistemaren kontsumoa kontrolatzeko oso garrantzitsua da. Tenperatura bat ezarri de 20ºC ordez 21ºC %10eko energiaren aurrezki bat esan nahi izan ahal du.";
$informacion_consejos_calefaccion36="Bere etxetik ateratzen denean bakarrik ordu batzuek, termostatoaren jarrera ezartzen du en 15, eredu batzuen ekonomia 'jarrerara' baliokidea dena.";
$informacion_consejos_calefaccion37="Ez hartzen duten denbora luzeen aldietan zehar egonaldien tenperatura gutxitzen du.";
$informacion_consejos_calefaccion38="Gauak berogailua itzaltzen du eta piz goizean despues etxea aireztatzeko eta leihoak itxi.";
$informacion_consejos_calefaccion39="10. gelek minutuak aireztatzeko nahikoak dira. Gehiegizko berritze batek ordezkatu ahal du arte bat 30 eta guztizko kontsumoaren % 40.";
$informacion_consejos_calefaccion40="Beroaren gehiegizko galerak saihesteko, ixten du pertsiana gauak eta gortinak.";
$informacion_consejos_calefaccion41="Termostatiko balbulak erabil ditzan erradiadoreetan eta programatzaile termostatoak, errazak dira jarri izatera eta arin amortizatzen dira (energiaren aurrezki bat uste dute zortzi eta bat %133aren artean).";
$informacion_consejos_calefaccion42="Hotzaren denboraldi bakoitzeko hasieran purgatzea egoki da erradiadoreen barnekoaren airea ur berotik beroaren transmisioa errazteko kanpora.Hotzaren ";
$informacion_consejos_calefaccion43="Ez du estaltzen eta ez du eta ez erradiadorearen ondoan objekturik.";
$informacion_consejos_calefaccion44="Zain dezan eta egokiro bere taldea mantentzen du berogailuaren, energiaren % 15a arteko aurrezki bat izan ahal da.";
$informacion_consejos_calefaccion45="Instalatzaile baten aholkularitza beharrezkoa da hasierako arautzearentzat eta oinarri berogailuaren martxan jartzea.";
?>