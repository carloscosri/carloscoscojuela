<?php
$informacion_consejos_termo1="Energia Aurrezteko kontseiluak - Termoa";
$informacion_consejos_termo2="Gaueko metaketak termo elektrikoa:";
$informacion_consejos_termo3="Ur beroak zatiaren era izatea konfort minimoaren. Bere funtzionamendua arrunta da: sare orokorraren ur hotza, sartzen da termoaren beheko zatiak eta aparatuaren barnekoan berotua da erresistentzia elektriko baten bidez, gaueko ordutegian, gutxi gorabeherako tenperatura bateraino funtzionatzen duena de 60ºC, termostatoaren bidez erregularra ahal dion balioa. Bere xedea";
$informacion_consejos_termo4="1. Urak uneoro eskuragarria egoteko behar du.<br />
2. Uraren kopurua etxebizitzaren eskaera estaltzeko nahikoa izan behar da.<br />
3. Ura nahi izandako tenperaturara eman behar da.<br />
4. Kontsumo energetikoak gauez, egin behar du elektrizitatea % 53a izaten denean merkea.";
$informacion_consejos_termo5="Termo Elektrikoaren Erosketarako Kontseiluak:";
$informacion_consejos_termo6="Merkatuan sorta zabal bat daude termo elektronikoen. Etekin maximoa lortzeko asmoz, eskuratu nahi den egokiro aparatua aukeratu behar da. Horretarako hurrengo alderdietara arreta utzi beharrezkoa da:";
$informacion_consejos_termo7="<strong>Beharrezko uraren kopurua.</strong> familiakideen kopuruaren arabera uraren eskaera arreta jarri ahal izan dadin termo bat aukeratu behar da, instalazio sanitarioen eta gizabanako ohituren.<br /><br />
<strong>Uraren izaera.</strong> Oso garrantzitsua Da el calderín ur motari egokitutakoa izan dadin tokiaren, porque hau oso oldarkorra bada edo gatz mineralaren gehiegikeria du ahal du corroer bere paretak. El calderín edo metagailuaren ontzia altzairuzko txaparen izan ahal da esmaltado, altzairuzko galbanizatua edo esmaltado, eta hiriaren ura oso oldarkorra da, aparatua hornitua egon dadin egokia da de bat ánodo magnesioa. Elementu honek barneko pareten korrosioa aurreikusten du del calderín.<br /><br />
<strong>Isolamendua mota eta lodiera.</strong> nabarmentzea interesgarri Da berotzeko eta mantentzeko kontsumitutako energia tenperatura bera batera uraren kopuru bera batek isolamenduaren kalitatearen arabera aldatuko du.";
$informacion_consejos_termo8="Instalazioa:";
$informacion_consejos_termo9="Aparatuaren kokapena. Edonon instalatu ahal du: ganbarak, harrasken azpian, sabai aizunen inguruan, eta abar.. Ez dute behar aireztapena edo tximiniak edo gasen irteera. Bakarrik erabilerraza uzteko kontua eta bere barnekoan termoaren zatiak hartu behar izango du konponketa edo mantentzearen operazio bat egin behar izango dezan.
Ur beroaren erabileraren puntuak urrunak badaude, erabiltzea hobe da bi edo aparatu gehiago, tutuetan beroaren galerak saihesteko.<br /><br />
Termoaren instalazioak bainugeletan, egin denean Behe-tentsioaren Elektroteknikari Araudian adierazitakoa errespetatu behar izango da. Termo elektrikoak debekuaren bolumenaz kanpo egon behar izango du, saihesteko asmoz urak aparatuaren konexioen kutxaren barnekoa zipriztintzen duena.<br /><br />
Termoaren konexio elektrikoarentzat entxufe-oin bat erabili behar izango du de 16 A, con hartzen du lurraren. Termoaren deskonexio erraza uzten duen, mozketako etengailu bipolar bat instalatzea gomendagarri, komuna edo dutxa erabiltzeko unean.";
$informacion_consejos_termo10="Erabileraren gomendioak:";
$informacion_consejos_termo11="Bere termo elektrikoa instalatu ondoren egitea gomendagarri da argibide arrunt batzuk, erabiltzeko orduan, bat hobeto emango diotena bere taldearen kontserbaziora. lagunduko duten aldi berean zerbitzuaren kalitatea
Hori dela eta, ondoren kontseilu praktiko batzuk eskaintzen diogu zu kontuan izan behar izango du bere termo elektrikoa erabili ohi duenean.";
$informacion_consejos_termo12="Si termoa gauez metatzen duen ur beroa egun guztiarentzat eskasa izaten da, bat erabaki ahal da, edo hurrengo bi konponbideen biek:<br /><br />
- Beroketaren tenperatura handitu termoaren bere termostatoa eragiten. Argibideen eskuliburuak erakusten du non kokatuta dago eta bezala maneiatu.<br /><br />
- Programatzailea egokitu termoak egun-argiz ere konektatu ahal du, baina bakarrik denbora minimoan zehar ur beroaren beharrak estaltzeko beharrezkoa izan dadina.<br /><br />
Egiaztatu ur beroaren tenperatura ez pertsonen segurtasunerako egokitutakoa den gainbaimena 60ºCean, eta aparatuaren iraupen handiago batentzat. Honela korrosioa aurreikusi lortzen da eta %50ean kaltzifikazioa gutxitu.<br /><br />
Gordailuaren inkrustazioak ezabatu, segurtasunaren balbularen halaber, begira denean ur beroaren emariak urritu duena nabarmen.";
$informacion_consejos_termo13="Mantentzea:";
$informacion_consejos_termo14="Argibideen eskuliburua kontsultatzea egoki da da para con taldearen iraupen maximoa lortu mantentzea gutxienez.<br /><br />
Exijitu bermea eskura izan termoaren. Beraren zaintzaren aldiak matxuren konponketa estaltzen du, batzuetan, gertatu ahal dute.<br /><br />
Si se nahastea ura, utzi lehena atera ur hotza eta gero nahasi ur beroarekin, nahi izandako tenperatura lortzera.";
$informacion_consejos_termo15="Energiaren gutxi Gorabeherako Kontsumoa:";
$informacion_consejos_termo16="Jarduera";
$informacion_consejos_termo17="Dutxa bat (30 litro a 40ºC)";
$informacion_consejos_termo18="Komun bat (90 litro a 40ºC)";
$informacion_consejos_termo19="Ontziteriaren eskura garbiketa bat (15 litro a 40ºC)";
$informacion_consejos_termo20="Kontseilu Praktikoak";
$informacion_consejos_termo21="Udaren hilabeteetan, osterantzean ura hain beroa ateratzen den beharrezkoa da, termostatoa eragiten tenperatura gutxitzen du.";
$informacion_consejos_termo22="Eza luzeetan, nagusiak a hiru edo lau egunek, aparatua deskonektatzen du. Si gabezia escorta, gutxitzea hobe da tenperatura del termostatoa eta konektatutakoa utzi termoa.";
$informacion_consejos_termo23="% 4aren bat arte dutxan aurrezten du eta energiaren % 6a tenperaturaren erregulatzaileekin termostatoarekin.";
$informacion_consejos_termo24="Gozatzeko aholkatutako tenperatura bainugela pertsonalean sentipen atsegin baten 30etik 35era.";
$informacion_consejos_termo25="Txorrotak estatu onean mantentzen ditu para ez goteen, itx bere erabileraren ondoren guztiz eta berehala zuzentzen du lo uraren fugak aurkezten dituztena. Txorrota bat gotea hilean 170 litro gastatu ahal du uraren.";
$informacion_consejos_termo26="Dutxa batean energiaren herena kontsumitzen dela kontuan izan ohi du komun batean kontsumitzen dena.";
$informacion_consejos_termo27="Ez du erabili ohi beharrezkoaren ur bero gehiago. Adibidez, txorrota irekia ez dezan utz xaboitzen den bitartean edo hortzak garbitzen ditu.";
$informacion_consejos_termo28="Dutxaren burkoak eskuratzen ditu de kontsumoaren azpian, energia eta uraren erdia gastatuz bainugela eroso bat uzten dute.";
$informacion_consejos_termo29="Emariaren txorrota erreduktoreetan jarri ahal du (aireadores).";
?>