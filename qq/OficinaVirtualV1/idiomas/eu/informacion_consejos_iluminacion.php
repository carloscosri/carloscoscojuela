<?php
$informacion_consejos_iluminacion1="Energia Aurrezteko Kontseiluak - Argiztapena";
$informacion_consejos_iluminacion2="Iluntasunaren orduetan eguzkiaren argia ordezka dezaten existentziaren hasieratik, gizona sistemak aurkitzeagatik lehiatu da edo lekuetan eta han escaseaba.<br /><br />
Kezka honen fruitua elementu batzuen agerpena izan da, modalitate desberdinetaraino suaren aurkikuntzatik gaur egun ezagututako lanpara elektrikoen, iturrien garapenean etapak markatzen joan dira argi artifizialaren.<br /><br />
Gaur egun, ikerketa jarraitzen du aurreratzen argiaren kalitate bat hurbila errazten duten lanparen lorpenerantz lo posiblea.<br /><br />
Merkatuan lanpara mota batzuk aurkitu ahal dira etxekoa ohizko egokitutakoak. Hauen aldatzen du, besteak beste faktoreak, bere iraupena, kontsumoa eta igortzen duten argiaren kalitatea.";
$informacion_consejos_iluminacion3="Lanpara Goriak";
$informacion_consejos_iluminacion4="Lanpara Halogenoak";
$informacion_consejos_iluminacion5="Lanpara Fluoreszenteak";
$informacion_consejos_iluminacion6="Lanparak de Kontsumoaren Azpian";
$informacion_consejos_iluminacion7="Koadro Konparatiboa";
$informacion_consejos_iluminacion8="Lanpara Mota Desberdinen Eraginkortasuna";
$informacion_consejos_iluminacion10="Eta bere argia bonbila popularrak dira berotzen den harizpi baten bidez lortzen da elektrizitatearen pausoarekin.";
$informacion_consejos_iluminacion11="ABANTAILAK:";
$informacion_consejos_iluminacion12="Lanpara hauek kopuru handiaren argi bat igortzen dute, beraz oso ondo koloreak erreproduzitzen dituzte.";
$informacion_consejos_iluminacion13="Argi artifizialen iturri guztien arte merkeak dira.";
$informacion_consejos_iluminacion14="Eredu eta potentzien sekulako sorta dela eta malgutasun handiagoa eskaintzen dute.";
$informacion_consejos_iluminacion15="ERAGOZPENAK:";
$informacion_consejos_iluminacion16="Energia gehiago kontsumitzen dute ematen duten argiaren kopuruari dagokionez beste batzuk.";
$informacion_consejos_iluminacion17="Beste batzuei dagokionez, bere iraupena mugatua da.";
$informacion_consejos_iluminacion18="Bonbila tradizionalen aurrean, argi halogenoa argitsu eta zuria da, gainera argiztapenaren gizabanako konponbide ugariak ematen du, eta ikusizko konfort handiago batekin jakina da, lortuz moderno eta erakargarriak girotzen dituzu. Daude con edo islagailurik gabe, arabera argia kontzentratu nahi dio edo ez.";
$informacion_consejos_iluminacion19="Eskaintzen dute gehiago zuria argi bat eta brillantea, egiten duen koloreen bereizketa perfektu bat utz dezaten.";
$informacion_consejos_iluminacion20="Bere gaitasunengatik joko gehiago barnealdeen dekorazioan. ematen dute";
$informacion_consejos_iluminacion21="Bere eraginkortasun eta iraupena lanpara gori normalak baino gehiago da.";
$informacion_consejos_iluminacion22="Argiaren bere kalitateak lanpararen bizitza guztian zehar aldaezina jarraitzen du.";
$informacion_consejos_iluminacion23="Energia kontsumo altu bat dute.";
$informacion_consejos_iluminacion24="Motak gutxik sustatzen du transformatzaile bat exijitzen dute, erabakitako ereduetan, datoz jada barne hartutakoa.";
$informacion_consejos_iluminacion25="Argiaren jaulkipena, eta hau oso kontzentratua da derrigortzen du a apantallar lanpara ez da ikus zuzenean.";
$informacion_consejos_iluminacion26="Kasu honetan, argia ez dago harizpi baten beroketak produzitutakoa deskarga elektriko batek baizik gas batean mantendutako arkuan edo lurruna ionizado.<br /><br />
Lanpara fluoreszenteak aplikazio unibertsal eta ekonomikoaren argiztapen orokor bezala gomendagarriak dira, eta argiaren tonalitate desberdinetan, daudelako bere aplikazioarentzat egokitua aukeratu ahal da, bezala komunetan, sukaldeak, bulegoak, korridoreak, trastelekuak edo garajeak.";
$informacion_consejos_iluminacion27="Eskaintzen dute, sortaren arabera, koloreetan jaioa argi eta erreprodukzioaren kalitate on bat.";
$informacion_consejos_iluminacion28="Bere kontsumo energetikoa oso gutxitua da eta errentagarria zeren eta, argiaren kopuru berdinari, bosgarren zatia kontsumitzen dute lanpara goria eta bere iraupena oso luzea da, de zortzi hamar aldizetara baino gehiago hauek.";
$informacion_consejos_iluminacion29="Iritzi popularraren aurrean, ez dute kontsumitzen gehiago piztuaren hasieran, eta horregatik osterantzean itzali behar dute joaten dira erabiltzera.";
$informacion_consejos_iluminacion30="Argiaren bere kalitateak lanpararen bizitza guztian zehar aldaezina jarraitzen du.";
$informacion_consejos_iluminacion31="Bere dimentsio eta diseinuak bere lanbidea mugatzen dute al ez eskaintza hain luze bat egon bezala gorietan.";
$informacion_consejos_iluminacion32="Aurrezte energetikoaren lanparak dira (elektronika Fluoreszente trinkoak), edonon girotze-argiaren kalitate bat emateaz gain, bai barnekoa kanpo bezala, funtsezkoak dira por bere leku haietan kontsumoaren azpian eta han piztuaren denboralde luzeekin argiztapen bat behar diote. Kanpo guneentzat idealak eta segurtasunaren argiztapen bezala.<br /><br />
Lanpara eraginkorren familia hau, zerbait dira baino gehiago soilik ekonomikoak. Era orokorraren ia, erabili ahal dute bonbila tradizional bezala zehazki.";
$informacion_consejos_iluminacion33="Bost aldiz kontsumitzen dituzte gutxiago goriak.";
$informacion_consejos_iluminacion34="Balio-bizitza bat dute 6 aldiz handiago, gutxi gorabehera zortzi urte, piztuaren eguneroko lau ordu estimatzen.";
$informacion_consejos_iluminacion35="Energiaren aurrezkiari esker, bere erabilerak ingurumenaren zaintzara. laguntzen du";
$informacion_consejos_iluminacion36="Zorro bera dute bonbila tradizionalak que, bere kalitate eta konfortari lotutakoa argiaren, edozein aplikaziorentzat erabilgarriek egiten ditu.";
$informacion_consejos_iluminacion37="Tradizionalak";
$informacion_consejos_iluminacion38="Hurrengo taulan eraginkortasuna, batezbesteko iraupena eta aukera ikusi ahal dugu lanpara desberdin moten arte koloreak antzemateko etxebizitzan erabiliak.";
$informacion_consejos_iluminacion39="Lanpara Mota";
$informacion_consejos_iluminacion40="Eraginkortasunaren Indizea";
$informacion_consejos_iluminacion41="Batezbestekoaren iraupena (h)";
$informacion_consejos_iluminacion42="Koloreak Antzemateko Aukera";
$informacion_consejos_iluminacion43="Goriak";
$informacion_consejos_iluminacion44="Bikaina";
$informacion_consejos_iluminacion45="Halogenoak";
$informacion_consejos_iluminacion46="Fluoreszenteak";
$informacion_consejos_iluminacion47="Ona";
$informacion_consejos_iluminacion48="Estra Fluoreszenteak";
$informacion_consejos_iluminacion49="Oso Ona";
$informacion_consejos_iluminacion50="Kontseilu praktikoak:";
$informacion_consejos_iluminacion51="Ahal duenean, argi naturala aprobetxatzen du.";
$informacion_consejos_iluminacion52="Bere etxean kolore argiak erabili ohi du argiztapen naturala hobeto aprobetxatzeko pareta eta sabaientzat eta artifiziala gutxitu.";
$informacion_consejos_iluminacion53="Beti argiak itzaltzen du ez egon dadin geletan erabiltzen.";
$informacion_consejos_iluminacion54="Argiztapen apaingarria lorategietan txikiagotzen du eta beste toki batzuk kanpoak.";
$informacion_consejos_iluminacion55="Maiz lanparak garbitzen du eta pantailak, horrela potentzia handitu gabe argitasuna handituko da.";
$informacion_consejos_iluminacion56="Argiztapena bere beharretara egokitzen du eta kontuan izan ohi du aurkitutako argiztapenarekin lortuko du, energia aurrezteaz gain, erosoak girotzen dituzu.";
$informacion_consejos_iluminacion57="Bonbila gorien ordez kontsumo txikiko bonbillak erabili ohi ditu, 6 aldiz irauten dituzte gehiago eta energiaren % 80raino. aurreztuko du";
$informacion_consejos_iluminacion58="Lanpara elektronikoak erabili ohi ditu porque durán gehiago, itzalitakoak eta piztuen kopuru handiago bat jasaten dute, eta kontsumitzen dute gutxiago lanparak de kontsumo konbentzionalen azpian. Antzematea jakingo du bere pisuagatik: konbentzionalen bitartean pisatzen dute inguruan 400 grsa. elektronikek batzuk pisatzen dituzte 100 grs.";
$informacion_consejos_iluminacion59="Colóque mota elektronikoaren intentsitate argitsuaren erregulatzaileak (ez de rostato).";
$informacion_consejos_iluminacion60="Hodi fluoreszenteak eta han erabili ohi ditu ordu askotan zehar argi gehiago behar du, bezala sukaldean.";
$informacion_consejos_iluminacion61="Presentziaren detektagailuak erabili ohi ditu argiek bizigarri gutxi funtzionatzen dituzte automatikoki leku haietan atondo bezala, garaje edo guztientzako eremuak.";
$volver="Itzuli";
?>