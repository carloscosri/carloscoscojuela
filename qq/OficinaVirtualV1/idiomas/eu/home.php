<?php
$home1="Energia Elektrikoaren merkaturatzea";
$home2="Enpresa saila";
$home3="Kontraanoaren saila";
$home4="Bezeroari Informazioaren saila";
$home5="Bulegoaren saila Birtuala";
$home6="Ataria Sige Energia";
$home7="Zertan lan egiten dugun, eta han ikasten du aurkitu, zer zerbitzu eskaini ahal dugu eta, nahi baduzu, jar gure kontaktuan.";
$home8="Informa posibleen kontratazioaren formulak,gure tarifak, eta abar";
$home9="Hemen mota guztia aurkituko duzu zure informazioa interesa. Kontseilu on batzuk ere energia aurrezten duzu!";
$home10="Bulego birtualetik, gure bezeroak sarbide izan ahal dute era pribatuaren bere datu guztiei edozein ekipamendu informatikotatik.";
$home11="Atari www.sigeenergia.com berriari sarbide du, eta han sektore energetikoaren informazio guztia erdiratzen da.";
?>