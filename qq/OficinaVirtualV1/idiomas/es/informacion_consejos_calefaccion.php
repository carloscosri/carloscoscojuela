<?php
$informacion_consejos_calefaccion1="Consejos para Ahorrar Energía - Calefacción";
$informacion_consejos_calefaccion2="Calefacción eléctrica. Ahorrar energía con acumuladores de calor y tarifa nocturna 2.0:";
$informacion_consejos_calefaccion3="Los acumuladores de calor son aparatos capaces de almacenar energía calorífica durante un periodo de tiempo, para liberarla después lentamente, con flujo controlable. Son elementos idóneos para calefacción ambiental puesto que utilizan las horas de la noche para almacenar el calor, aprovechando al máximo las ventajas de la Tarifa Nocturna. El usuario de esta tarifa, obtiene un descuento de un 53 % en el precio de la energía consumida durante las horas, nocturnas, y tienen un recargo del 3% en el precio del kilovatio hora consumido durante el día.";
$informacion_consejos_calefaccion4="Tipos de acumuladores:";
$informacion_consejos_calefaccion5="Existen dos tipos de acumuladores, cuya diferencia es la forma de ceder calor:<br /><br />
<strong>Acumuladores estáticos:</strong> En este tipo de aparatos, el calor se cede principalmente por radiación, a través de la envolvente metálica, y por un convención natural, merced al aire que puede circular a través de unos canales existentes en la carcasa y en núcleo del acumulador. A tal fin, disponen de una entrada de aire en la parte inferior del aparato y una salida por la parte superior. El aire de salida se regula mediante una compuerta.<br /><br />
<strong>Acumuladores dinámicos:</strong> Los acumuladores dinámicos son similares a los estáticos. También cede el calor por radiación, a través de la envolvente metálica, pero la convección es forzada mediante una turbina que hace circular el aire por los conductos o canales existentes en el núcleo del acumulador.";
$informacion_consejos_calefaccion6="<strong>1.  Bloque acumulador:</strong> El núcleo acumulador está constituido por placas de material refractario (magnesita) que deben presentar un calor específico elevado y la máxima densidad, ya que la capacidad para almacenar calor depende de esos dos parámetros. La temperatura máxima que se alcanza al final del período de carga en el núcleo del acumulador es de 600 a 700 º C.<br /><br />
<strong>2. Resistencias calefactores:</strong> Los elementos calefactores (resistencias) se insertan en cavidades o canales situados en el interior del núcleo acumulador, de forma que calientan toda la masa del refractorio uniformemente.<br /><br />
<strong>3. Aislamiento térmico:</strong> El bloque acumulador está rodeado con aislante térmico para conservar el calor acumulad y limitar la temperatura superficial del mueble del aparato.<br /><br />
<strong>4. Dispositivo de seguridad:</strong> Generalmente los acumuladores disponen de un limitador térmico o de un termostato de seguridad, para desconectar el aparato si, por cualquier motivo, se produce una evaluación inadecuada de la temperatura (por ejemplo, una disfunción en el sistema de regulación del sistema de carga).<strong></strong>
<strong>5. Termostato:</strong> Hay dos, uno para regular la carga y otro para la emisión de calor.";
$informacion_consejos_calefaccion7="VENTAJAS:";
$informacion_consejos_calefaccion8="Se pueden instalar tanto en edificios de nueva construcción como en casas antiguas.";
$informacion_consejos_calefaccion9="No existen circuitos de agua, eliminando la posibilidad de fugas y goteos.";
$informacion_consejos_calefaccion10="No necesitan depósito de combustible.";
$informacion_consejos_calefaccion11="Instalación sencilla, sin obras.";
$informacion_consejos_calefaccion12="Ecológicos, pues no emiten humos y utilizan la energía más limpia que se conoce en el punto de consumo: la Electricidad.";
$informacion_consejos_calefaccion13="Total seguridad y al no haber combustibles, no existe riesgo de explosión fortuita.";
$informacion_consejos_calefaccion14="Mantenimiento prácticamente inexistente.";
$informacion_consejos_calefaccion15="Proporcionan calefacción las 24 h. del día.";
$informacion_consejos_calefaccion16="Se reducen los costos generales de la facturación, al desplazar el consumo de frigoríficos, lavadoras, lavavajillas, calentadores de agua, alumbrado, etc., a horario de Tarifa Nocturna, como se ha indicado anteriormente en la descripción del aparato.";
$informacion_consejos_calefaccion17="Su apariencia estética es de diseño avanzado.";
$informacion_consejos_calefaccion18="Automatizables al máximo.";
$informacion_consejos_calefaccion19="Calefacción eléctrica por cable radiante con sistemas de acumulación (base + apoyo, tarifa nocturna 2.0):";
$informacion_consejos_calefaccion20="La calefacción eléctrica por cable radiante es un sistema de calefacción no visible, que integra dos partes diferenciadas en cuanto al uso de horario y complementarias por su funcionamiento: calefacción de base y calefacción de apoyo.";
$informacion_consejos_calefaccion21="Calefacción de base:";
$informacion_consejos_calefaccion22="Éste sistema calefactor instalado en el suelo queda conectado exclusivamente durante el periodo de la noche, cuando la tarifa nocturna tiene una reducción del 53 % en el precio del kWh.
La calefacción de base instalada en el suelo, acumula calor durante el período de tarifa reducida, que restituye al ambiente sin gastar energía durante el período de las horas restantes.La cantidad de calor acumulado en el suelo viene determinada por la temperatura exterior, ya que dispone de una sonda que detecta la temperatura.<br /><br />
Dado que la temperatura interior se incrementa en algunos grados debido a las aportaciones de calor gratuito generado por las personas, iluminación, agua caliente sanitaria, electrodomésticos, insolación durante el día, etc., que la cuantía de estas aportaciones es imprevisible y no detectable por la sonda del regulador situada en el exterior y que éste no funciona durante las horas diurnas, es imprescindible contar con un sistema calefactor de apoyo complementario que permita una regulación exacta del nivel de confort deseado en cada estancia.";
$informacion_consejos_calefaccion23="Calefacción de apoyo:";
$informacion_consejos_calefaccion24="Éste sistema calefactor independiente, de potencia reducida puesto que sólo interviene compensando las variaciones de la temperatura interior que el sistema de base no puede detectar ni corregir, es complementario de la calefacción de base por acumulación. Consiste en cables calefactores integrados en el suelo, por encima de los cables del sistema de base, conectados a la red eléctrica a través de un termostato automático de ambiente en cada habitación o dependencia.<br /><br />
Su función es mantener durante las 24 horas al día la temperatura exacta deseada por el usuario en cada habitación.
Otra opción para la calefacción de apoyo es la instalación de convectores eléctricos para completar la aportación de calor hasta la temperatura previamente seleccionada en cada habitación.";
$informacion_consejos_calefaccion25="Recomendaciones de uso:";
$informacion_consejos_calefaccion26="El ajuste de las temperaturas deseadas mediante los termostatos situados en las distintas dependencias es muy importante para conseguir el mayor confort de su sistema de calefacción.<br /><br />
Si su vivienda es de reciente construcción, la propia humedad de los materiales puede provocar un consumo entre un 20 y un 30% mayor de lo normal en la primera temporada de funcionamiento, independiente de cual sea el sistema de calefacción de que disponga.";
$informacion_consejos_calefaccion27="VENTAJAS:";
$informacion_consejos_calefaccion28="Limpio: No se producen movimientos del aire y, por consiguiente, del polvo.";
$informacion_consejos_calefaccion29="Saludable: No consume oxígeno, no reseca el aire ni lo recalienta en exceso.";
$informacion_consejos_calefaccion30="Costo controlado: La instalación individualizada por dependencias permite el control del gasto de acuerdo con las necesidades, horarios y usos. Además aprovecha las aportaciones gratuitas de calor (sol, luces, personas).";
$informacion_consejos_calefaccion31="Invisible y silencioso: No hay maquinaria ni elementos mecánicos a la vista.";
$informacion_consejos_calefaccion32="Seguro: Fuera del alcance de las personas y protegido eléctricamente de acuerdo con el Reglamento Electrotécnico de Baja Tensión, y al no haber combustibles no existe riesgo de explosión fortuita.";
$informacion_consejos_calefaccion33="Y además: Sin obras auxiliares, depósitos de combustibles, mantenimiento, pagos adelantados de energía y, es el sistema de menor coste de inversión.";
$informacion_consejos_calefaccion34="Consejos prácticos:";
$informacion_consejos_calefaccion35="El ajuste de la temperatura en cada habitación mediante el termostato es muy importante para controlar el consumo de su sistema de calefacción. Fijar una temperatura de 20ºC en lugar de 21ºC puede significar un ahorro de energía del 10%.";
$informacion_consejos_calefaccion34="Consejos prácticos:";
$informacion_consejos_calefaccion35="El ajuste de la temperatura en cada habitación mediante el termostato es muy importante para controlar el consumo de su sistema de calefacción. Fijar una temperatura de 20ºC en lugar de 21ºC puede significar un ahorro de energía del 10%.";
$informacion_consejos_calefaccion36="Cuando salga de su hogar sólo por unas horas, fije la posición del termostato en 15º, que es el equivalente a la posición 'economía' de algunos modelos.";
$informacion_consejos_calefaccion37="Reduzca la temperatura de las estancias que no ocupen durante periodos de tiempo largos.";
$informacion_consejos_calefaccion38="Apague la calefacción por la noche y enciendala por la mañana despues de ventilar la casa y cerrar las ventanas.";
$informacion_consejos_calefaccion39="Para ventilar las habitaciones 10 minutos son suficientes. Una renovación excesiva puede representar entre un 30 y 40 % del consumo total.";
$informacion_consejos_calefaccion40="Para evitar excesivas pérdidas de calor, cierre por la noche persianas y cortinas.";
$informacion_consejos_calefaccion41="Utilice válvulas termostáticas en radiadores y termostatos programadores, son fáciles de colocar y se amortizan rápidamente (suponen un ahorro de energía entre un 8 y 133 %).";
$informacion_consejos_calefaccion42="Al inicio de cada temporada de frío es conveniente purgar el aire del interior de los radiadores para facilitar la transmisión de calor desde el agua caliente al exterior.";
$informacion_consejos_calefaccion43="No cubra ni coloque al lado del radiador ningún objeto.";
$informacion_consejos_calefaccion44="Cuide y mantenga adecuadamente su equipo de calefacción, puede suponerle un ahorro de hasta el 15% de energía.";
$informacion_consejos_calefaccion45="Es necesario el asesoramiento de un instalador para la regulación inicial y puesta en marcha de la calefacción base.";
?>