<?php
$informacion_consejos_vitroceramica1="Consejos para Ahorrar Energía - Vitrocerámica";
$informacion_consejos_vitroceramica2="Cocina eléctrica: encimera vitrocerámica";
$informacion_consejos_vitroceramica3="Es una cocina en la que las zonas de cocción han sido serigrafiadas sobre una superficie plana vitrocerámica con un elevado grado de resistencia al peso, a los golpes y a las diferencias de temperatura. El calor se obtiene de unas resistencias eléctricas situadas bajo las zonas de cocción y, en algunos casos, de la acción combinada de esas resistencias y unas lámparas halógenas.<br /><br />
Dependiendo de los modelos y fabricantes, pueden tener 2, 4 ó 5 puntos de cocción. Existen modelos que tienen una zona lateral que funciona a baja potencia (150 W) y que se utiliza para mantener templados los alimentos. La incorporación de calefactores de media luna en una de las zonas de cocción, permite la utilización de recipientes alargados.
Por sus características exteriores es la cocina más limpia y segura, de más fácil utilización y más precisa.<br /><br />
La situación de los mandos pueden ser las siguientes: ";
$informacion_consejos_vitroceramica4="1.   <strong>En la encimera.</strong> Resulta muy útiles, cuando se trata de sustituciones en renovación de cocina como tal aparato, u otras similares.<br /><br />
2. <strong>En panel de independencia.</strong> Permite colocar los mandos en una determinada situación, por ejemplo, bajo la visera del mueble, lo que los protege de la suciedad.<br /><br />
3. <strong>Incorporados al horno.</strong> Los mandos incorporados al horno tienen la ventaja de ofrecer una superficie de trabajo mayor.";
$informacion_consejos_vitroceramica5="Tipos de vitrocerámica (en función de los elementos calefactores):";
$informacion_consejos_vitroceramica6="Desde la aparición de las Encimeras Vitrocerámicas, los elementos calefactores utilizados se han desarrollado y diversificado fuertemente. Existen tres grandes grupos en cuanto a las tecnologías empleadas: resistencias radiantes, halógenos y de Inducción.<br /><br />
Las potencias máximas más usuales de los puntos de cocción son 1200, 1700 y 2100 W. Los actuales sistemas permiten regular la potencia de cocción a valores inferiores a los indicados según la posición del mando.<br /><br />
Los puntos de cocción con halógenos presentan ventajas reales, tales como: ";
$informacion_consejos_vitroceramica7="- Emisión de calor y luz instantánea.<br />
- Rapidez de calentamiento.<br />
- Menor dependencia de la calidad del recipiente.<br />
- Flexibilidad y rapidez de reacción al mando de la placa.<br />
- Mayor seguridad contra los errores de manipulación al verse de inmediato si se conecta.";
$informacion_consejos_vitroceramica8="Recomendaciones de uso";
$informacion_consejos_vitroceramica9="Lea atentamente las instrucciones contenidas en el manual del fabricante de la cocina. Ahí conocerá cual es la posición del mando regulador de calor más adecuada al tipo de cocción, y la cantidad de alimentos a cocinar.<br /><br />
Utilice utensilios de cocina con fondo difusor plano y liso y un diámetro igual o superior al de la zona de cocción. Si no dispone de ellos, hay una gran variedad de modelos y marcas a elegir entre los que han sido especialmente diseñados para ser utilizados en vitrocerámicas. En cambio se desaconseja el empleo de recipientes de aluminio o cazuelas de barro.<br /><br />
Las cocinas eléctricas vitrocerámicas incorporan, normalmente una señalización de cada zona de cocción, que se ilumina cuando la temperatura de la misma es mayor de 50 º C y permanece así, una vez se ha desconectado, hasta que la temperatura baja de este límite.<br /><br />
Existen modelos que automáticamente desconectan las placas al retirar los recipientes, con lo cual no existe consumo aunque permanezca conectada.";
$informacion_consejos_vitroceramica10="Mantenimiento:";
$informacion_consejos_vitroceramica11="La cocina eléctrica vitrocerámica no precisa prácticamente mantenimiento. Es aconsejable limpiar la superficie después de cada uso, una vez apagados los focos de calor, con un paño o papel de cocina para que recupere brillo. Si han quedado residuos sólidos adheridos sobre la superficie, se recomienda arrancarlos con una rasqueta antes de que se enfríen y, luego, enjuagar y secar la placa.<br /><br />
Exija y tenga a mano la garantía del Fabricante de la Cocina. Durante su periodo de vigencia cubre la reparación de las averías que ocasionalmente pueden producirse.";
$informacion_consejos_vitroceramica12="VENTAJAS:";
$informacion_consejos_vitroceramica13="Las encimeras vitrocerámicas por su diseño y acabado totalmente novedoso y atractivo, y por sus características de visualización, flexibilidad y rapidez, son un producto altamente competitivo y ventajoso frente a otros tipos de cocinas.";
$informacion_consejos_vitroceramica14="Velocidad, precisión y visualización: Consiguen una velocidad de calentamiento comparable a cualquier otro sistema y energía. Su tiempo de respuesta es prácticamente inmediato, visualizándose las placas en funcionamiento.";
$informacion_consejos_vitroceramica15="Estética: Con líneas elegantes y depuradas y superficies totalmente lisas, consiguen una elegancia inalcanzable por cualquier otro tipo de encimeras.";
$informacion_consejos_vitroceramica16="Facilidad de utilización y limpieza: Las cacerolas, sartenes y recipientes en general resbalan por su superficie sin bascular, por lo que resulta muy fácil su manipulación, y el vuelco es mas difícil que en otros tipos de cocinas, cuya superficie no sea completamente lisa. Al no disponer de ranuras ni zonas de difícil acceso, u otras zonas que requieren una limpieza intensiva, ésta resulta más fácil. Para efectuar la limpieza normal se recomienda que la placa esté fría o templada.";
$informacion_consejos_vitroceramica17="Solidez y resistencia: La vitrocerámica es un material muy robusto.";
$informacion_consejos_vitroceramica18="Seguridad: A la seguridad propia de la utilización de la energía eléctrica, se añade que el calor se concentra en las zonas de cocción, sin peligros de quemaduras si se toca el resto de la superficie. Al no haber combustible, no existe el riesgo de explosión fortuita.";
$informacion_consejos_vitroceramica19="Cocina eléctrica: encimera vitrocerámica por inducción:";
$informacion_consejos_vitroceramica20="La cocina vitrocerámica por inducción es fría, la más limpia, la de más fácil utilización, la más segura, la más moderna y la más rápida.<br /><br />
La cocina eléctrica vitrocerámica por inducción es lisa, sin quemadores, y su superficie es un vidrio cerámico, con alto grado de resistencia al peso y a los golpes. Su precisión en cada temperatura de cocción es exacta, además no se calienta la placa.<br /><br />
En estas cocinas el calor se genera en el propio recipiente de cocinar a partir de un campo magnético creado por un elemento que está situado bajo la superficie vitrocerámica, por lo que no hay pérdidas de calor.";
$informacion_consejos_vitroceramica21="Recomendaciones de uso y mantenimiento:";
$informacion_consejos_vitroceramica22="Iniciar la cocción con la numeración más alta de calor de mando, para descender posteriormente a la posición deseada.<br /><br />
Si solo se desea calor suave, usar numeraciones bajas hasta conseguir exactamente el calor deseado.<br /><br />
Al situarse sobre la posición 0 del mando, se deja de suministrar calor instantáneamente (la ebullición se detiene en el acto).<br /><br />
Si no hay recipiente sobre la zona de cocción, ésta no suministra calor aunque está conectada.<br /><br />
Para la limpieza de las encimeras vitrocerámicas por inducción, bastará con usar un paño húmedo (de papel, rejilla, spontex, o similar). Pero si es necesario, pueden usarse detergente adecuados que existan en el mercado, enjuagando y secando bien después de su uso para recuperar su brillo.<br /><br />
Se obtendrá un notable ahorro de energía usando los recipientes tapados o medio tapados siempre que sea posible.";
$informacion_consejos_vitroceramica23="Consumo aproximado de energía";
$informacion_consejos_vitroceramica24="Tarea";
$informacion_consejos_vitroceramica25="Gas (kWh)";
$informacion_consejos_vitroceramica26="Electricidad (kWh)";
$informacion_consejos_vitroceramica27="Filete plancha";
$informacion_consejos_vitroceramica28="Judías cocidas en cazuela de acero (4 personas)";
$informacion_consejos_vitroceramica29="Arroz (4 personas)";
$informacion_consejos_vitroceramica30="Recipientes apropiados para este tipo de cocina:";
$informacion_consejos_vitroceramica31="Deben utilizarse recipientes de material ferromagnético, fondo plano, liso y lo más grueso posible. Esta propiedad e fácilmente comprobable con un imán que deberá ser atraído por el recipiente. Por lo tanto pueden utilizarse utensilios de acero esmaltado, de hierro fundido o de acero inoxidable que contengan algún material ferromagnético.<br /><br />
Los recipientes con fondo tipo sándwich, con tres o más capas que contengan algún material ferromagnético, son utilizables en la mayoría de los casos.<br /><br />
Aunque es necesaria una cantidad mínima de masa ferromagnética para poder funcionar, no dejar utensilios metálicos sobre la zona de cocción. En ningún caso se deben utilizar utensilios de aluminio o de barro, ya que no funcionaría la placa.";
$informacion_consejos_vitroceramica32="VENTAJAS:";
$informacion_consejos_vitroceramica33="Suministro de energía: La electricidad se encuentra instalada en todos los hogares, por lo que no es necesario preocuparse de ir a buscar la energía con que cocinar o almacenada.";
$informacion_consejos_vitroceramica34="Velocidad, presión y visualización: Consiguen una velocidad de calentamiento comparable a cualquier otro sistema de energía. Su tiempo de respuesta es inmediato, visualizándose las placas en funcionamiento mediante un piloto censor de temperatura.";
$informacion_consejos_vitroceramica35="Estética: Con líneas elegantes y depuradas, y superficies totalmente lisas, consiguen una elegancia inalcanzable por cualquier tipo de encimeras.";
$informacion_consejos_vitroceramica36="Facilidad de utilización y limpieza: Las cacerolas, sartenes y recipientes en general resbalan por su superficie sin bascular, por lo que resulta muy fácil su manipulación, y el vuelco es más difícil que en otros tipos de cocinas cuya limpieza intensiva y dado que la placa permanece fría o con un leve calor transmitido por el propio recipiente, su limpieza se limita a pasar un paño húmedo. Solidez y resistencia: La vitrocerámica es un material muy robusto.";
$informacion_consejos_vitroceramica37="Seguridad: A la seguridad propia de la utilización de la energía eléctrica, se añade que las zonas de cocción no tienen peligro de quemaduras si se tocan al retirar el recipiente, ya que la encimera permanece fría.";
$informacion_consejos_vitroceramica38="Consejos Prácticos";
$informacion_consejos_vitroceramica39="Procure cocinar con poca agua y con los recipientes tapados o medio tapados.";
$informacion_consejos_vitroceramica40="El uso de la olla a presión es muy aconsejable, puede ahorrar hasta un 50 % de energía.";
$informacion_consejos_vitroceramica41="Teniendo en cuenta que las zonas de cocción, una vez desconectadas mantienen la ebullición y el calor de los alimentos de 5 a 7 minutos más (excepto las de inducción), apáguelas un poco antes de acabar la cocción.";
$informacion_consejos_vitroceramica42="A la hora de cocinar tenga en cuenta que el fondo de los recipientes debe ser ligeramente superior a la zona de cocción para aprovechar al máximo el calor.";
$informacion_consejos_vitroceramica43="Es recomendable la utilización de menaje con fondo grueso difusor pues se consigue una temperatura más homogénea en todo el recipiente.";
$informacion_consejos_vitroceramica44="Con las cocinas vitrocerámicas por inducción el aprovechamiento energético es total: detecta si hay o no hay recipiente sobre su superficie, actuando sólo en el primer caso.";
?>