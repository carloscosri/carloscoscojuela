<?php
$contratacion_contratar_opcionescontratar1="Contratar con ";
$contratacion_contratar_opcionescontratar2="Para contratar el Servicio de Suministro Eléctrico con nosotros estaremos encantados de atenderle en ";
$contratacion_contratar_opcionescontratar3="nuestras oficinas";
$contratacion_contratar_opcionescontratar4="Para agilizar la solicitud del servicio le aconsejamos que previamente revise si dispone de toda la ";
$contratacion_contratar_opcionescontratar5="documentación necesaria";
$contratacion_contratar_opcionescontratar6="Si desconoce qué potencia se adecua más a sus necesidades informese sobre ";
$contratacion_contratar_opcionescontratar7="factores influyentes para seleccionar una potencia";
$contratacion_contratar_opcionescontratar8="Para cualquier duda o consulta, le atenderemos en nuestro teléfono de  Atención al Cliente 981 50 58 30 o puede ";
$contratacion_contratar_opcionescontratar9="enviarnos un e-mail";
$contratacion_contratar_opcionescontratar10="Cumplimentar Impreso de Solicitud de Suministro";
?>