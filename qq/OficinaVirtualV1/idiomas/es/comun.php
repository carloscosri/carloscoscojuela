<?php
$titulo_web="AUDINFOR SYSTEM - Oficina Virtual";
$pie_imagen="AUDINFOR SYSTEM S.L.";
$pie="Sitio Web desarrollado por Audinfor System S.L.";
$w3c_html="Validación W3C HTML";
$w3c_css="Validación W3C CSS";
$menu1="Empresa";
$menu2="Ofertas y Contratación";
$menu3="Información al Cliente";
$menu4="Oficina Virtual";
$menu5="Portal Sige Energía";
$datos_empresa="AUDINFOR SYSTEM, SL | C/ PANAMA 2 LOCAL 2 ZARAGOZA Tel. 976312018";
$menu3_2="Información<br />al Cliente";
$menu_idiomas1="Cambiar idioma a Español";
$menu_idiomas2="Cambiar idioma a Catalan";
$menu_idiomas3="Cambiar idioma a Euskera";
$menu_idiomas4="Cambiar idioma a Gallego";
$menu_idiomas5="Cambiar idioma a Inglés";
$idioma_activo="Idioma activo";
$error_identificacion="Esta seccion solo es para usuarios identificados.";
$nota_pdf1="* Esta documentación está en formato PDF, para visualizar estos archivos, necesita tener instalado el programa Adobe Acrobat Reader, si no lo tiene puede descargar este programa (gratuito) desde aquí:";
$nota_pdf2="Descargar Adobe Acrobat Reader";
$poblacion_empresa="ZARAGOZA";
$banner_sigeenergia="Acceso al Portal";
$nota_campos_obligatorios="ATENCIÓN: Solamente los campos señalados con * son obligatorios.";
$tlf_averias="";
?>