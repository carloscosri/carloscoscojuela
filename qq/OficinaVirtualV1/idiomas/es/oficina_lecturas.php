<?php
$error_rellenar="Debe escribir la fecha de la lectura y un número de contador.";
$error_fecha="La fecha se ha escrito de forma incorrecta. Debe emplear el siguiente formato: 18-03-2009.";
$error_lectura="Debe escribir al menos una lectura.";
$lectura_ok="Su lectura ha sido enviada.";
$oficina_lecturas1="Lectura de un Solo Periodo Tarifario";
$oficina_lecturas2="Lecturas con Varios Periodos Tarifarios";
$oficina_lecturas3="Periodo";
$oficina_lecturas4="P1 Punta";
$oficina_lecturas5="P2 Llano";
$oficina_lecturas6="P3 Valle";
$oficina_lecturas7="Activa";
$oficina_lecturas8="Reactiva";
$oficina_lecturas9="Maxímetro";
$error_varias_lecturas="Si ha escrito lecturas en el apartado Lectura de un 'Solo Periodo Tarifario' no se deben escribir lecturas en el apartado 'Lecturas con Varios Periodos Tarifarios'.";
$error_numero_lectura="No ha escrito correctamente alguna lectura. Deben ser como máximo un números con 8 enteros y 3 decimales.";
$error_fecha_posterior="Las fechas escritas no pueden ser posteriores a la fecha actual.";
?>