<?php
$oficina_contratacion1="Requerimientos Administrativos para dar de alta un nuevo punto de suministro:";
$submenu_contratacion_oficina2="- Bono Social (Inferior a 3 kWh).";
$submenu_contratacion_oficina3="- Contratos hasta 10 kWh con o sin discriminacion horaria (Tarifas 2.0.A y 2.0 DHA).";
$submenu_contratacion_oficina4="- Contratos de 10 a 15 kWh con o sin discriminacion horaria (Tarifas 2.1.A y 2.1 DHA).";
$submenu_contratacion_oficina5="- Contratos a partir de 15 kWh (Tarifas 3.0.A y 3.1.A y 6.x).";
$submenu_contratacion_oficina1="Tarifas a contratar:";
$oficina_contratacion2="NOTA: Para cualquier información adicional sobre tarifas a contratar y tipo de suministro, deben consultar directamente en nuestras oficinas.";
$oficina_contratacion3="Condiciones para el Bono Social:";
$oficina_contratacion4="El Bono Social es solo de aplicacion a personas físicas cuyo suministro se destine par el uso en
vivienda habitual y que pertenezca a alguno de los siguientes colectivos:<br /><br />
Personas físicas con una potencia contratada inferior a 3kW en su vivienda habitual. Se le aplicará
el Bono Social Automáticamente.<br /><br />
Clientes con edad mínima de 60 años que perciban una pensión mínima.<br /><br />
Clientes de más de 60 años que perciban pensiones no contributivas de jubilación e invalidez, así como
beneficiarios de pensiones del extinguido Seguro Obligatorio de Vejez e invalidez.<br /><br />
Familias numerosas.<br /><br />
Familias con todos sus miembros en situación de desempleo.";
$oficina_contratacion5="Referencia Catastral.";
$oficina_contratacion6="CUPS (Código Identificador de su Punto de Suministro) en el supuesto que provenga de otra Comercializadora.";
$oficina_contratacion7="Esta solicitud de nuevo suministro tendrá carácter meramente informativo. Nuestra empresa se pondrá en contacto con usted en los próximos días para presentarle una oferta económica personalizada, a través de e-mail o correo ordinario.<br /><br />
Una vez aceptado dicho presupuesto será cuando podamos finalizar con nuestra empresa el contrato de suministro de energía eléctrica en el punto de suministro y condiciones solicitadas.<br />
Para que pueda recibir esta información deberá rellenar el formulario correspondiente a la potencia que desee contratar, según los grupos que a continuación se desglosan.";
$oficina_contratacion8="Título de propiedad o contrato de alquiler.";
$oficina_contratacion9="Cédula de habitabilidad.";
?>