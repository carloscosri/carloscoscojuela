<?php
$informacion_consejos_climatizacion1="Consejos para Ahorrar Energía - Climatización";
$informacion_consejos_climatizacion2="La climatización es el proceso de tratamiento del aire que permite controlar la temperatura, la humedad, el movimiento y la limpieza del aire en el interior de un local. Los valores de estos parámetros influyen muy directamente sobre el confort, y varían con la época del año.<br /><br />
La Bomba de Calor es el único sistema que cubre todas las necesidades de frío en verano y de calor en invierno en una vivienda, con lo cual obtenemos un ahorro considerable en la inversión inicial, al no tener que comprar aparatos para frío y aparatos para calor.<br /><br />
Hay que tener en cuenta que a esta ventaja se une la de su elevado rendimiento, ya que aprovecha la energía existente en el aire ambiente.";
$informacion_consejos_climatizacion3="Este sistema nos permite:";
$informacion_consejos_climatizacion4="1. Controlar la temperatura.<br />
2. Controlar el movimiento del aire.<br />
3. Eliminar las impurezas del aire.";
$informacion_consejos_climatizacion5="La Bomba de Calor es limpia, y no produce humos ni olores al existir ningún tipo de combustión, teniendo además, la posibilidad de la renovación del aire según el tipo de aparato.";
$informacion_consejos_climatizacion6="Antes de comprar un sistema de climatización:";
$informacion_consejos_climatizacion7="A la hora de instalar un sistema de climatización debe buscarse el asesoramiento de un instalador especializado.<br /><br />
Para obtener el máximo confort es imprescindible que el cálculo de las frigorías / calorías necesarias se realice con fiabilidad suficiente. Para ello, el instalador tendrá en cuenta las características de la vivienda: orientación, detalles constructivos, fuentes de calor, etc.El sistema de distribución del aire debe ser el adecuado para que el calor y el frío se repartan uniformemente.Para ventilar las habitaciones es necesario que el sistema de climatización tenga una toma de aire exterior.";
$informacion_consejos_climatizacion9="Los sistemas de climatización por Bomba de Calor, se controlan mediante un mando centralizado. Su uso es simple y fácil, sólo hay que seleccionar:";
$informacion_consejos_climatizacion8="Recomendaciones de uso:";
$informacion_consejos_climatizacion10="- La temperatura deseada desde el termostato.<br />
- El modo de utilización: frío, calor o ventilación.<br />
- La velocidad de ventilador.";
$informacion_consejos_climatizacion11="Existen algunos modelos que llevan incorporado un programador horario de puesta en marcha.";
$informacion_consejos_climatizacion12="Mantenimiento:";
$informacion_consejos_climatizacion13="La unidad interior que enfría o calienta el aire, dispone de un filtro, que sirve para limpiar el aire de la vivienda. La única precaución que hay que tener, es mantenerlo limpio, siguiendo las indicaciones del fabricante.";
$informacion_consejos_climatizacion14="Consejos prácticos:";
$informacion_consejos_climatizacion15="Las temperaturas recomendadas de confort son 25º en verano y entre 18 y 20º en invierno.";
$informacion_consejos_climatizacion16="El ajuste de la temperatura es muy importante para controlar el consumo, por ejemplo: fijar una temperatura en invierno de 20º en lugar de 21º puede significar un ahorro de energía del 10%; en verano, por el contrario, por cada grado de temperatura que se fije por debajo de los 25º, estará consumiendo de más.";
$informacion_consejos_climatizacion17="Puede reducir el calentamiento de su vivienda en verano colocando toldos, cerrando persianas y corriendo las cortinas.";
$informacion_consejos_climatizacion18="En verano es conveniente ventilar la casa cuando el aire de la calle sea más fresco: noche y primeras horas de la mañana.";
$informacion_consejos_climatizacion19="Otra manera de evitar el calentamiento de espacios interiores es pintar techos y paredes exteriores con colores claros para reflejar la radiación solar.";
$informacion_consejos_climatizacion20="Coloque el climatizador en un lugar donde no le de el sol y haya buena circulación de aire.";
?>