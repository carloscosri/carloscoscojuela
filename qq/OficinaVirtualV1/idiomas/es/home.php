<?php
$home1="Comercialización de Energía Eléctrica";
$home2="Sección Empresa";
$home3="Sección Contraración";
$home4="Sección Información al cliente";
$home5="Sección Oficina Virtual";
$home6="Portal Sige Energía";
$home7="Aprende a qué nos dedicamos, donde encontrarnos, qué servicios podemos ofrecerte y, si lo deseas, ponte en contacto con nosotros.";
$home8="Infórmate de las posibles fórmulas de contratación, nuestras tarifas, etc.";
$home9="Aquí encontrarás todo tipo de información de tu interés. ¡Incluso unos buenos consejos para que ahorres energía!";
$home10="Desde la oficina virtual, nuestros clientes pueden acceder de forma privada a todos sus datos desde cualquier equipo informático.";
$home11="Accede al nuevo portal www.sigeenergia.com, donde se centra toda la información del sector energético.";
?>