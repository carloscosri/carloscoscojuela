<?php
$oficina_infofactura_ampliada1="Anagrama de la empresa Comercializadora y Información de la Factura </br></br> Fecha de la factura, Periodo de Facturación y Número de Factura.";
$oficina_infofactura_ampliada2="Resumen de la factura con importes y Datos de dirección de envio de factura.";
$oficina_infofactura_ampliada4="Detalle de todos los Datos del Contrato </br></br> Nombre, NIF, Nº de cliente, Dirección, Potencia, Datos Bancarios...";
$oficina_infofactura_ampliada5="Gráfico sobre Importe de la factura desglosado en: Costes de producción,Impuestos aplicados y Costes regulados ";
$oficina_infofactura_ampliada6="Historial de Consumos y Observaciones </br></br> Detalla el historial de consumos efectuados los últimos 14 meses y las observaciones a tener en cuenta.";
$oficina_infofactura_ampliada3="Información del consumo eléctrico: Lecturas Anteriores, Actuales y Consumos efectuados dentro de los Periodos de Fechas indicados.";
?>