<?php
$error_rellenar="Deben rellenarse los campos marcados como obligatorios.";
$error_fecha="La fecha se ha escrito de forma incorrecta. Debe emplear el siguiente formato: 18-03-2009.";
$error_hora="La hora y los minutos deben ser 2 carácteres numéricos entre 00 y 23 y entre 00-59 respectivamente.";
$error_telefonos="Los campos Teléfonom, Móvil y Fax solo pueden contener carácteres numéricos.";
$error_mail="No ha escrito el e-mail correctamente.";
$incidencia_ok="Su incidencia ha sido enviada.";
$error_fecha_posterior="Las fechas escritas no pueden ser posteriores a la fecha actual.";
?>