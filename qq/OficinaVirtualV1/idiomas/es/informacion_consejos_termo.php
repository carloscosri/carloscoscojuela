<?php
$informacion_consejos_termo1="Consejos para Ahorrar Energía - Termo";
$informacion_consejos_termo2="Termo eléctrico por acumulación nocturna:";
$informacion_consejos_termo3="El disponer de agua caliente forma parte del mínimo confort. Su funcionamiento es sencillo: el agua fría de la red general, entra por la parte inferior del termo y es calentada en el interior del aparato por medio de una resistencia eléctrica, que funciona en horario nocturno, hasta una temperatura aproximada de 60ºC, valor que se puede regular mediante termostato. Para lograr su objetivo debe reunir las siguientes condiciones:";
$informacion_consejos_termo4="1. El agua debe de estar disponible en todo momento.<br />
2. La cantidad de agua debe ser suficiente para cubrir la demanda de la vivienda.<br />
3. El agua tiene que ser suministrada a la temperatura deseada.<br />
4. El consumo energético debe realizarse durante la noche, cuando la electricidad resulta un 53% más barata.";
$informacion_consejos_termo5="Consejos para la Compra del Termo Eléctrico:";
$informacion_consejos_termo6="Existen en el mercado una amplia gama de termos electrónicos. Con objeto de obtener el máximo rendimiento, se debe seleccionar adecuadamente el aparato que se desea adquirir. Para ello es necesario prestar atención a los siguientes aspectos:";
$informacion_consejos_termo7="<strong>La cantidad de agua necesaria.</strong> Se debe elegir un termo que sea capaz de atender la demanda de agua según el número de miembros de la familia, de las instalaciones sanitarias y de los hábitos individuales.<br /><br />
<strong>La naturaleza del agua.</strong> Es muy importante que el calderín sea el adecuado al tipo de agua de la zona, ya que si ésta es muy agresiva o contiene exceso de sales minerales puede corroer sus paredes. El calderín o recipiente acumulador puede ser de chapa de acero esmaltado, de acero galvanizado o esmaltado, si el agua de la ciudad es muy agresiva, es conveniente que el aparato esté provisto de un ánodo magnesio. Este elemento previene la corrosión de las paredes interiores del calderín.<br /><br />
<strong>Tipo y espesor del aislamiento.</strong> Es interesante resaltar que la energía consumida para calentar y mantener una misma cantidad de agua a una misma temperatura variará según la calidad del aislamiento.";
$informacion_consejos_termo8="Instalación:";
$informacion_consejos_termo9="El emplazamiento del aparato. Puede instalarse en cualquier lugar: altillos, debajo de los fregaderos, sobre falsos techos, etc... No necesitan ni ventilación ni chimeneas ni salida de gases. Únicamente deberá tomarse la precaución de dejar accesible la parte del termo en cuyo interior haya de realizarse alguna operación de mantenimiento o reparación.
Si los puntos de utilización del agua caliente están lejanos, es preferible utilizar dos o más aparatos, para evitar pérdidas de calor en las tuberías.<br /><br />
Cuando la instalación del termo se realice en cuartos de baño, se deberá respetar lo indicado en el Reglamento Electrotécnico de Baja Tensión. El termo eléctrico deberá estar fuera del volumen de prohibición, con objeto de evitar que el agua salpique el interior de la caja de conexiones del aparato.<br /><br />
Para la conexión eléctrica del termo deberá utilizarse una base de enchufe de 16 A, con toma de tierra. Es aconsejable instalar un interruptor de corte bipolar, que permite la fácil desconexión del termo, en el momento de utilizar el baño o ducha.";
$informacion_consejos_termo10="Recomendaciones de uso:";
$informacion_consejos_termo11="Una vez instalado su termo eléctrico es aconsejable seguir algunas indicaciones sencillas, a la hora de utilizarlo, que le proporcionarán una mejor calidad de servicio a la vez que ayudarán a la conservación de su equipo.
Con tal fin, le ofrecemos a continuación una serie de consejos prácticos que usted deberá tener en cuenta cuando use su termo eléctrico.";
$informacion_consejos_termo12="Si el agua caliente que acumula el termo durante la noche resulta insuficiente para todo el día, se puede optar por una, o por ambas, de las dos soluciones siguientes:<br /><br />
- Aumentar la temperatura de calentamiento del termo accionando su termostato. El manual de instrucciones muestra dónde está ubicado y como manejarlo.<br /><br />    
- Ajustar el programador para que el termo pueda conectarse también de día, pero sólo durante el mínimo tiempo que sea necesario para cubrir las necesidades de agua caliente.<br /><br />           
Comprobar que la temperatura del agua caliente no sobrepase los 60ºC, que es la adecuada para la seguridad de las personas y para una mayor duración del aparato. De esta forma se logra prevenir la corrosión y disminuir la calcificación en un 50 %.<br /><br />
Eliminar las incrustaciones del depósito, así como de la válvula de seguridad, cuando se observe que el caudal de agua caliente ha disminuido considerablemente.
";
$informacion_consejos_termo13="Mantenimiento:";
$informacion_consejos_termo14="Es conveniente consultar el manual de instrucciones para que con un mínimo de mantenimiento conseguir la máxima duración del equipo.<br /><br />
Exigir tener a mano la garantía del termo. El periodo de vigilancia de la misma cubre la reparación de las averías que, ocasionalmente, puedan producirse.<br /><br />
Si se mezcla el agua, dejar salir primero el agua fría y después mezclar con agua caliente, hasta alcanzar la temperatura deseada.";
$informacion_consejos_termo15="Consumo Aproximado de Energía:";
$informacion_consejos_termo16="Tarea";
$informacion_consejos_termo17="Una ducha (30 litros a 40ºC)";
$informacion_consejos_termo18="Un baño (90 litros a 40ºC)";
$informacion_consejos_termo19="Un lavado a mano de vajilla (15 litros a 40ºC)";
$informacion_consejos_termo20="Consejos Prácticos";
$informacion_consejos_termo21="En los meses de verano, cuando no es necesario que el agua salga tan caliente, reduzca la temperatura accionando el termostato.";
$informacion_consejos_termo22="En ausencias prolongadas, superiores a tres o cuatro días, desconecte el aparato. Si la ausencia escorta, es preferible reducir la temperatura del termostato y dejar conectado el termo.";
$informacion_consejos_termo23="Ahorre en la ducha entre un 4% y un 6% de energía con los reguladores de temperatura con termostato.";
$informacion_consejos_termo24="La temperatura aconsejada para disfrutar de una agradable sensación en el aseo personal es de 30º a 35º.";
$informacion_consejos_termo25="Mantenga los grifos en buen estado para que no goteen, ciérrelos completamente después de su uso y repare en seguida los que presenten fugas de agua. Un grifo que gotea puede gastar al mes 170 litros de agua.";
$informacion_consejos_termo26="Tenga en cuenta que en una ducha se consume la tercera parte de la energía que se consume en un baño.";
$informacion_consejos_termo27="No utilice más agua caliente de la necesaria. Por ejemplo, no deje el grifo abierto mientras se enjabona o se lava los dientes.";
$informacion_consejos_termo28="Adquiera cabezales de ducha de bajo consumo, permiten un aseo cómodo gastando la mitad de agua y de energía.";
$informacion_consejos_termo29="Puede colocar en los grifos reductores de caudal (aireadores).";
?>