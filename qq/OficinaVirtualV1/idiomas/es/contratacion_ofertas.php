<?php
$contratacion_ofertas1="Sistema para consultar Precios y Ofertas dirigidas a Particulares y Empresas que deseen contratar el Suministro Eléctrico con ";
$contratacion_ofertas2="Energía ";
$contratacion_ofertas3="Inferior o igual a 10 kW.<br />Tarifas de Acceso 2.0A y 2.0DHA.";
$contratacion_ofertas4="Superior a 10 kW e inferior a 15 kW.<br />Tarifas de Acceso 2.1A y 2.1DHA.";
$contratacion_ofertas5="Superior a 15 kW.<br />Tarifa de acceso 3.0A.";
$contratacion_ofertas6="Si su potencia contratada es<br />superior a 50 kW o su consumo anual<br />es superior a 100.000 kWh.";
$contratacion_ofertas7="Para determinados niveles de consumo, se puede solicitar una oferta personalizada. Esto lo puede realizar rellenando el siguiente ";
$contratacion_ofertas10="modelo de solicitud de oferta en formato PDF editable";
$contratacion_ofertas11="y enviarlo por fax o ";
$contratacion_ofertas12="e-mail";
$contratacion_ofertas8="formulario";
$contratacion_ofertas9=", o bien rellenando el ";
?>