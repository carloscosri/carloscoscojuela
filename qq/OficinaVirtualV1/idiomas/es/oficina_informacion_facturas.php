<?php
$error_rellenar="Deben rellenarse los campos marcados como obligatorios.";
$error_fecha="La fecha se ha escrito de forma incorrecta. Debe emplear el siguiente formato: 18-03-2009.";
$error_fecha_posterior="Las fechas escritas no pueden ser posteriores a la fecha actual.";
$error_fecha_inicio_mayor="La fecha de inicio no puede ser mayor que la fecha final.";
$solicitud_ok="Su solicitud ha sido enviada con éxito.";
$oficina_informacion_facturas1="CONSUMOS ACTIVA";
$oficina_informacion_facturas2="P1 Punta";
$oficina_informacion_facturas3="P2 Llano";
$oficina_informacion_facturas4="P3 Valle";
$oficina_informacion_facturas5="CONSUMOS REACTIVA";
$oficina_informacion_facturas6="IMPORTES";
$oficina_informacion_facturas7="Activa";
$oficina_informacion_facturas8="Reactiva";
$oficina_informacion_facturas9="Maxímetro";
$oficina_informacion_facturas10="Descuentos";
$oficina_informacion_facturas11="Cargos";
$oficina_informacion_facturas12="Base";
$oficina_informacion_facturas13="IVA";
$oficina_informacion_facturas14="TOTAL:";
$oficina_informacion_facturas15="Imprimir";
$error_no_facturas="Actualmente no hay facturas relacionadas con este cliente en nuestra base de datos.";
?>