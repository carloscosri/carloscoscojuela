<?php
$informacion_consejos_iluminacion1="Consejos para Ahorrar Energía - Iluminación";
$informacion_consejos_iluminacion2="Desde el principio de la existencia, el hombre se ha esforzado por encontrar sistemas que sustituyan la luz solar en las horas de oscuridad o en lugares donde escaseaba.<br /><br />
Fruto de esta inquietud ha sido la aparición de una serie de elementos que, desde el descubrimiento del fuego hasta las distintas modalidades de lámparas eléctricas conocidas hoy día, han ido marcando etapas en el desarrollo de fuentes de luz artificial.<br /><br />
Actualmente, la investigación continúa avanzando hacia la consecución de lámparas que faciliten una calidad de luz lo más cercana posible a la solar.<br /><br />
En el mercado se pueden encontrar diversos tipos de lámparas adecuadas al uso doméstico en las que varía, entre otros factores, su duración, el consumo y la calidad de la luz que emiten.";
$informacion_consejos_iluminacion3="Lámparas incandescentes";
$informacion_consejos_iluminacion4="Lámparas halógenas";
$informacion_consejos_iluminacion5="Lámparas fluorescentes";
$informacion_consejos_iluminacion6="Lámparas de bajo consumo";
$informacion_consejos_iluminacion7="Cuadro comparativo";
$informacion_consejos_iluminacion8="Eficacia de los Distintos Tipos de Lámparas";
$informacion_consejos_iluminacion10="Son las populares bombillas cuya luz se consigue por medio de un filamento que se calienta con el paso de la electricidad.";
$informacion_consejos_iluminacion11="VENTAJAS:";
$informacion_consejos_iluminacion12="Estas lámparas emiten una luz de gran cantidad, con lo que reproducen muy bien los colores.";
$informacion_consejos_iluminacion13="Son las más baratas entre todas las fuentes de luz artificiales.";
$informacion_consejos_iluminacion14="Ofrecen mayor flexibilidad debido a la enorme gama de modelos y potencias.";
$informacion_consejos_iluminacion15="INCONVENIENTES:";
$informacion_consejos_iluminacion16="Consumen más energía que otras en relación con la cantidad de luz que aportan.";
$informacion_consejos_iluminacion17="Respecto a otras, su duración es más limitada.";
$informacion_consejos_iluminacion18="Frente a las bombillas tradicionales, la luz halógena es más luminosa y blanca, además aporta numerosas soluciones individuales de iluminación, y por supuesto con un mayor confort visual, obteniendo ambientes más modernos y atractivos. Existen con o sin reflector, según se quiera concentrar la luz o no.";
$informacion_consejos_iluminacion19="Ofrecen una luz más blanca y brillante, lo que hace que permitan una perfecta discriminación de los colores.";
$informacion_consejos_iluminacion20="Por sus cualidades dan más juego en la decoración de interiores.";
$informacion_consejos_iluminacion21="Su eficacia y duración es superior a las lámparas incandescente normales.";
$informacion_consejos_iluminacion22="Su calidad de luz permanece inalterable a lo largo de toda la vida de la lámpara.";
$informacion_consejos_iluminacion23="Tienen un elevado consumo de energía.";
$informacion_consejos_iluminacion24="Los tipos de poca potencia exigen un transformador que, en determinados modelos, vienen ya incorporado.";
$informacion_consejos_iluminacion25="La emisión de luz es muy concentrada, lo que obliga a apantallar la lámpara para que no se vea directamente.";
$informacion_consejos_iluminacion26="En este caso, la luz no está producida por el calentamiento de un filamento sino por una descarga eléctrica en arco mantenida en un gas o vapor ionizado.<br /><br />
La lámparas fluorescentes son aconsejables como alumbrado general de aplicación universal y económico, y puesto que existen en diferentes tonalidades de la luz, se puede elegir la más adecuada para su aplicación, como en baños, cocinas, despachos, pasillos, trasteros o garajes.";
$informacion_consejos_iluminacion27="Ofrecen, dependiendo de la gama, una buena calidad de luz y reproducción natural de los colores.";
$informacion_consejos_iluminacion28="Su consumo energético es muy reducido y rentable porque, a igual cantidad de luz, consumen la quinta parte que las lámparas incandescente y su duración es muy larga, de ocho a diez veces más que éstas.";
$informacion_consejos_iluminacion29="Frente a la opinión popular, no consumen más en el arranque del encendido, por lo que deben apagarse cuando no vayan a utilizarse.";
$informacion_consejos_iluminacion30="Su calidad de luz permanece inalterable a lo largo de toda la vida de la lámpara.";
$informacion_consejos_iluminacion31="Sus dimensiones y diseños limitan su empleo al no existir una oferta tan extensa como en las incandescentes.";
$informacion_consejos_iluminacion32="Son las lámparas de ahorro energético (Fluorescentes compactas electrónicas) que, además de aportar una calidad de luz ambiental en cualquier lugar, tanto interior como exterior, son fundamentales por su bajo consumo en aquellos lugares donde se necesiten un alumbrado con largos períodos de encendido. Ideales para espacios exteriores y como alumbrado de seguridad.<br /><br />
Ésta familia de lámparas eficientes, son algo más que meramente económicas. Pueden utilizarse casi de forma general, exactamente igual que las tradicionales bombillas.";
$informacion_consejos_iluminacion33="Consumen cinco veces menos que las incandescentes.";
$informacion_consejos_iluminacion34="Tienen una vida útil 6 veces mayor, aproximadamente ocho años, estimando cuatro horas diarias de encendido.";
$informacion_consejos_iluminacion35="Gracias al ahorro de energía, su utilización contribuye a la preservación del medio ambiente.";
$informacion_consejos_iluminacion36="Poseen el mismo casquillo que las bombillas tradicionales lo que, unido a su calidad y confort de luz, las hace útiles para cualquier aplicación.";
$informacion_consejos_iluminacion37="Tradicionales";
$informacion_consejos_iluminacion38="En la siguiente tabla podemos ver la eficacia, la duración media y la posibilidad de distinguir colores entre los distintos tipos de lámparas más utilizadas en la vivienda.";
$informacion_consejos_iluminacion39="Tipo de Lámpara";
$informacion_consejos_iluminacion40="Índice de Eficacia";
$informacion_consejos_iluminacion41="Duración Media (h)";
$informacion_consejos_iluminacion42="Posibilidad de Distinguir Colores";
$informacion_consejos_iluminacion43="Incandescentes";
$informacion_consejos_iluminacion44="Excelente";
$informacion_consejos_iluminacion45="Halógenas";
$informacion_consejos_iluminacion46="Fluorescentes";
$informacion_consejos_iluminacion47="Buena";
$informacion_consejos_iluminacion48="Fluorescentes Extra";
$informacion_consejos_iluminacion49="Muy Buena";
$informacion_consejos_iluminacion50="Consejos prácticos:";
$informacion_consejos_iluminacion51="Siempre que pueda, aproveche la luz natural.";
$informacion_consejos_iluminacion52="Utilice en su hogar colores claros para paredes y techos para aprovechar mejor la iluminación natural y reducir la artificial.";
$informacion_consejos_iluminacion53="Apague siempre las luces en las habitaciones que no esté utilizando.";
$informacion_consejos_iluminacion54="Minimice la iluminación ornamental en jardines y otras zonas exteriores.";
$informacion_consejos_iluminacion55="Limpie a menudo las lámparas y las pantallas, así aumentará la luminosidad sin aumentar la potencia.";
$informacion_consejos_iluminacion56="Adapte la iluminación a sus necesidades y tenga en cuenta que con la iluminación localizada conseguirá, además de ahorrar energía, ambientes más confortables.";
$informacion_consejos_iluminacion57="Utilice bombillas de bajo consumo en vez de las bombillas incandescentes, duran 6 veces más y ahorrará hasta un 80% de energía.";
$informacion_consejos_iluminacion58="Use lámparas electrónicas ya que durán más, aguantan un mayor número de encendidos y apagados, y consumen menos que las lámparas de bajo consumo convencionales. Sabrá distinguirlas por su peso: mientras que las convencionales pesan en torno a los 400 grs. las electrónicas pesan unos 100 grs.";
$informacion_consejos_iluminacion59="Colóque reguladores de intensidad luminosa de tipo electrónico (no de rostato).";
$informacion_consejos_iluminacion60="Utilice tubos fluorescentes donde necesite más luz durante muchas horas, como en la cocina.";
$informacion_consejos_iluminacion61="Use detectores de presencia para que las luces funcionen automáticamente en aquellos lugares poco habitables como vestíbulos, garajes o zonas comunes.";
$volver="Volver";
?>