<?php
$informacion_seguridad1="Consejos para la Seguridad de su Hogar";
$informacion_seguridad2="Seguridad y Protección de su instalación eléctrica";
$informacion_seguridad3="Toda vivienda debe tener el Cuadro de Mando y Protección. Si su vivienda es de nueva construcción, no se preocupe, ya lo tiene instalado. Pero si es antigua, asegúrese de que dispone de uno, ya que es importante para la seguridad de su hogar y los que viven en él.<br /><br />Las protecciones incluidas en el Cuadro de mando y Protección son las siguientes:";
$informacion_seguridad4="1. Interruptor de Control de Potencia (ICP)";
$informacion_seguridad5="Es un interruptor automático que determina la potencia simultánea disponible de acuerdo con el Contrato de energía eléctrica.El ICP desconectará su instalación cuando la suma de la potencia de los aparatos eléctricos conectados a la vez, supere la potencia contratada.De esta forma se evitan riesgos innecesarios en su instalación.";
$informacion_seguridad6="2. Interruptor General de Corte (IGC)";
$informacion_seguridad7="Este interruptor permite la desconexión total de su instalación en caso necesario. También realiza la función de protección de su instalación, de forma que nunca se sobrepase la potencia máxima admisible de su instalación.";
$informacion_seguridad8="3. Interruptor Diferencial (ID)";
$informacion_seguridad9="Es el elemento de protección más importante del Cuadro de mando y Protección, pues protege a las personas de cualquier accidente eléctrico. El interruptor diferencial desconecta su instalación cuando detecta una derivación en algún aparato electrodoméstico o en algún punto de la instalación.En viviendas, estos interruptores diferenciales, deben ser de alta sensibilidad (30 mA.)";
$informacion_seguridad10="4. Pequeños interruptores magnetotérmicos (PIA)";
$informacion_seguridad11="Toda instalación eléctrica está formada por varios circuitos (alumbrado, fuerza, cocina, etc...) y cada uno de estos circuitos deben de estar protegidos por sobrecargas y cortocircuitos por un interruptor magnetotérmico, de acuerdo con la capacidad de cada uno de los circuitos interiores.";
$informacion_seguridad12=" le recomienda:";
$informacion_seguridad13="Si tiene que realizar un nuevo contrato de suministro, modifique o sustituya su Cuadro de Mando y Protección de acuerdo con la legislación vigente para asegurar su instalación doméstica ante riesgos eléctricos. Así velará por su seguridad y la de sus familiares.<br /><br />Instale un interruptor Diferencial de 30 mA. De suficiente sensibilidad para evitar cortes en el suministro de su instalación (en caso de un funcionamiento inadecuado de alguno de los electrodomésticos)";
$informacion_seguridad14="Legislación";
$informacion_seguridad15="Las normas y condiciones que deben reunir las instalaciones eléctricas de Baja Tensión fueron definidas por el Ministerio de Industria y Energía y publicado en el Real Decreto 842/2002, y están reunidas en el ";
$informacion_seguridad16="Reglamento Electrotécnico de Baja Tensión";
?>