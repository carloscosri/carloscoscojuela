<?php
$contratacion_legislacion3="ÓRDEN ITC 3860/2007";
$contratacion_legislacion4=", de 28 de diciembre, por la que se revisan las tarifas eléctricas a partir del 1 de enero de 2008.";
$contratacion_legislacion5="REAL DECRETO 385/2002";
$contratacion_legislacion6=", de 26 de abril, por el que se modifica el Real Decreto 2018/1997, de 26 de diciembre, por el que se aprueba el reglamento de puntos de medida de los consumos y tránsitos de energía eléctrica. (BOR 14-05-02).";
$contratacion_legislacion7="REAL DECRETO 1955/2000";
$contratacion_legislacion8=", de 1 de diciembre, por el que se regulan las actividades de transporte, distribución, comercialización, suministro y procedimientos de autorización de instalaciones de energía eléctrica. (BOE 27-12-00).";
$contratacion_legislacion9="REAL DECRETO-LEY 6/2000";
$contratacion_legislacion10=", de 23 de junio, de Medidas Urgentes de Intensificación de la Competencia en Mercados de Bienes y Servicios. (BOE 24-06-00).";
$contratacion_legislacion11="LEY 54/1997";
$contratacion_legislacion12=", de 27 de noviembre, del Sector Eléctrico. (BOE 28-11-97).";
$contratacion_legislacion1="Legislación Eléctrica Española";
$contratacion_legislacion2="En esta sección encontrará diversos enlaces a los documentos correspondientes a la legislación eléctrica española.";
$contratacion_legislacion13="Ministerio de Industria, Turismo y Comercio";
?>