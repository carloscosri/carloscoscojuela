<?php
$oficina_presentacion1="Bienvenido a la oficina virtual de ";
$oficina_presentacion2="Le recordamos que, para <strong>optimizar</strong> el visionado de la Oficina Virtual,<br />se recomienda utilizar los exploradores:<br />";
$presentacionTexto1="La contrase&ntilde;a introducida esta asociada a varios Puntos de Suministro.<br>
Debe seleccionar a que Punto de Suministro desea acceder";
$resumenFacturasIntro="Selecciona el rango de fechas de las facturas que desee descargar";
$cerrar="Cerrar";
$resumenFacturas="Resumen Facturas";
$fechaInicio="Fecha Inicio:";
$fechaFin="Fecha Fin:";
$cliente="Cliente";
$numfactura="Num. Factura";
$fecha="Fecha";
$importe="Importe";
$numResumenFacturas="Num. Factura";
$totalfacturas="Total Facturas: ";
?>