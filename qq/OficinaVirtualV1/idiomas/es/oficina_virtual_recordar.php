<?php
$oficina_virtual_recordar1="Por su propia seguridad la información de la contraseña olvidada, solamente será proporcionada al usuario en la dirección e-mail que nos proporciona. Si su contraseña corresponde a un usuario multipunto, debe ponerse en contacto telefónico con nosotros, para proporcionarle las correspondientes contraseñas.";
$oficina_virtual_recordar2="Usuario";
$oficina_virtual_recordar3="Teléfono";
$oficina_virtual_recordar4="E-mail";
$oficina_virtual_recordar5="Aceptar";
$oficina_virtual_recordar6="Volver";
$error_recordar_vacio="Para facilitarle su clave debe escribir su número de usuario y al menos uno de los datos de contacto.";
$error_no_usuario="No existe ese usuario.";
$error_telefono="El teléfono solo puede contener carácteres numéricos.";
$error_mail="No ha escrito el e-mail correctamente.";
$recordar_ok="Nos pondrémos en contácto con usted para facilitarle su clave.";
?>