<?php
$contratacion_contratar_potencia1="Selección de la Potencia a Contratar";
$contratacion_contratar_potencia2="La potencia de contrato de electricidad depende de la cantidad y las características del equipamiento de su hogar: según qué y cuántos equipos tenga, la potencia varía significativamente.<br /><br />
Contratar una potencia inadecuada a sus necesidades no es nada conveniente. Si contrata más potencia de la que necesita, no sólo está derrochando dinero, sino que también está perjudicando el medio ambiente gastando más energía de la necesaria. Y si, por el contrario, contrata menos, se pueden producir molestos cortes en el suministro provocados por el ICP (Interruptor de Control de Potencia).";
$contratacion_contratar_potencia3="El cálculo de la potencia supone que la utilización de los equipos de mayor potencia no se realiza de forma simultánea. Es una potencia de contrato mínima recomendada en base a una tarifa 2.0 general.<br /><br />
Si dispones de:";
$contratacion_contratar_potencia4="- Termo eléctrico instantáneo.<br />
- Radiadores eléctricos.<br />
- Calefacción acumulación.<br />
- Bomba de calor.<br />
- Aire acondicionado.";
$contratacion_contratar_potencia5="Te aconsejamos que te pongas en contacto con nosotros en el teléfono de atención al cliente 972 59 46 91 ó en nuestras oficinas para que estudiemos tu caso más exhaustivamente.";
?>