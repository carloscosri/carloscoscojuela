<?php
$oficina_cambio_pass1="Introduzca su contraseña";
$oficina_cambio_pass2="Introduzca nueva contraseña";
$oficina_cambio_pass3="Confirme su nueva contraseña";
$error_rellenar="Debe rellenar todos los datos.";
$error_pass_invalida="La contraseña no es correcta.";
$pass_no_coincide="La nueva contraseña y la confirmación no coinciden.";
$mensaje_actualizar_pass="Se ha actualizado su contraseña.";
?>