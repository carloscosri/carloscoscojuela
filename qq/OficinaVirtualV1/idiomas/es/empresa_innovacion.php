<?php
$empresa_innovacion1="Innovación";
$empresa_innovacion2="tiene establecido un Plan de Innovación que abarca aspectos tan importantes como son:
";
$empresa_innovacion3="- Plan de Infraestructuras en la Red  de Distribución.<br />
- Plan de Instalación de Nuevos Aparatos de Medida  (Contadores). <br />
- Plan de Innovación TIC para Gestiones de Clientes.";
$empresa_innovacion4="Plan de Infraestructuras en la Red  de Distribución.";
$empresa_innovacion5="Estamos en plena fase de mejoras en Nuestra Red de Distribución, en
Líneas de Acometida, Centros de Transformación, Transformadores,
Circuitos...<br /><br />
La renovación y sustitución de los materiales que constituyen nuestra
Red de Distribución, es un hecho de constante  actividad, con el fin
de reducir al mínimo las averías y garantizar un mejor servicio hacía
nuestros clientes.";
$empresa_innovacion6="Plan de Instalación de Nuevos Aparatos de Medida  (Contadores).";
$empresa_innovacion7="De acuerdo con la Normativa Legal sobre los Aparatos de Medida (Contadores), 
se están realizando la sustitución de todos aquellos de tecnología analógica
por los nuevos de tecnología digital. Estos aparatos llevan integrados todos los requisitos técnicos actuales y futuros.<br /><br />
Ello conlleva que puedan estar totalmente preparados para las próximas 
Normativas Legales de TeleMedida y TeleGestión, que facilitaran los procesos de gestionar toda la problemática del cliente en forma remota a través de TeleProceso.
";
$empresa_innovacion8="Plan de Innovación TIC para Gestiones de Clientes.";
$empresa_innovacion9="El Plan de Innovación TIC (Nuevas Tecnologías)  que estamos implementando, 
trata de ofrecer a sus clientes una gestión directa a través de nuestra nueva página web, facilitando todas las necesidades que se planteen en relación con nuestra empresa, directamente desde su domicilio y sin necesidad de desplazarse a nuestras oficinas.<br /><br />
Podrá realizar gestiones como:
";
$empresa_innovacion10="- Contratar un nuevo Punto de Suministro.<br />
- Cualquier cambio en su contrato (Tarifas, Potencias, Domicilios, Cuenta Bancaria...).<br />
- Imprimir copias de facturas que ya han sido emitidas.<br />
- Información y Certificados relativos a sus consumos en kWh y/o importes facturados.<br />
- Comunicar Lecturas de Contadores sino ha sido posible tomarla.<br />
- Comunicar cualquier Avería o Incidencia.";
$empresa_innovacion11="Es decir, cualquier necesidad de gestión se podrá realizar a través de la Oficina Virtual en nuestra Pagina Web.";
?>