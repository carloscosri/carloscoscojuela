<?php
$contratacion_contratar_motivos1="Motivos para contratar con ";
$contratacion_contratar_motivos2="Nuestra filosofía es dar el máximo en todo lo que está a nuestro alcance: calidad de suministro, atención, asesoramiento, inspecciones, seguridad, innovación...";
$contratacion_contratar_motivos4="Tendrá a su disposición a un gran equipo de técnicos y profesionales dispuestos a atenderle en todo momento.";
$contratacion_contratar_motivos3="Le garantizamos toda nuestra experiencia de una empresa que durante años ha suministrado energía eléctrica.";
$contratacion_contratar_motivos5="Nuestras oficinas, almacenes y técnicos se encuentran en nuestra ciudad y, en caso de necesidad, estamos cerca de usted para atenderle.";
$contratacion_contratar_motivos6="Los beneficios que genera la empresa se quedan e invierten, de forma mayoritaria, en nuestra ciudad.";
$contratacion_contratar_motivos7="Tenemos un compromiso con usted y con el mundo que nos rodea, ya que un ahorro energético no es sólo bueno para su bolsillo, sino también para la naturaleza.";
?>