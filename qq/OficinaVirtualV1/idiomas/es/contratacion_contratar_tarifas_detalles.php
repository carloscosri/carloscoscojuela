<?php
$titulo_potencias_menos_10="Tarifas de potencias contratadas hasta 10 kW";
$titulo_potencias_mas_10="Tarifas de potencias contratadas superiores a 10 Kw e inferior a 15Kw";
$titulo_potencias_alta_tension="Precios para Contratos de Alta Tensión";
$termino_potencia="Término de Potencia ";
$termino_energia="Término de Energía ";
$euro_kw_mes="Euros / Kw / Mes";
$euro_kwh="Euros / kWh";
$volver="Volver";
$nota_potencias_menos_10="NOTA: Los precios reflejados podrán ser modificados durante el período de vigencia del contrato o de sus prórrogas según los apartados 4 y 6 de las condiciones generales del Contrato de Suministro.";
$nota_potencias_mas_10="";
$nota_potencias_alta_tension="Si su contrato es de Alta tensión, le haremos una oferta personalizada en función de su consumo. Para ello puede contactar con nosotros directamente en el teléfono 981 50 58 30 o puede enviarnos su última factura de electricidad por correo electrónico <a href='mailto:info@fucinos.es' class='enlace'>info@fucinos.es</a>";
$tarifas_alta_tension1="Si necesita contratar un Suministro en Media/Alta Tensión, deberá cumplimentar ";
$tarifas_alta_tension2="la solicitud que se indica en el apartado correspondiente.";
$tarifas_alta_tension3="Posteriormente nos pondremos en contacto con usted para informarle a cerca de nuestros precios y condiciones.";
$titulo_potencias_mas_15="Tarifas para potencias contratadas superiores de 15 kw";
?>