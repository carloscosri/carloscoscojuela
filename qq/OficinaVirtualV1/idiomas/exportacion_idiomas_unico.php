<?php
//Añadimos el archivo que contiene el inicio de la sesion y las variables de sesion.  Es importante añadir
//este archivo antes de las cabeceras del documento o dara error
include("../includes/sesion.php");	

//Una vez con la sesion iniciada, se puede dar valor a la variable de sesion que indica si nos encontramos en la oficina virtual
$_SESSION["oficina_virtual"]=0;

//Añadimos la libreria de conexion a base de datos, que a su vez contiene las referencias a todos los archivos necesarios.
//include($_SESSION["directorio_raiz"]."includes/conexion.php");
include("../includes/conexion.php");
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>EXPORTACION DE IDIOMAS</title>
</head>

<body>
<?php
//Esta funcion recibe como parametro un texto y devuelve una cadena con una serie de sustituciones de caracteres, para evitar que se den 
//problemas al escribir en HTML y PHP
function sustituir_caracteres_problematicos($texto)
{
//IMPORTANTE:!!!!!!!!!PUEDE QUE CUANDO SE AÑADAN NUEVOS TEXTOS APAREZCAN NUEVOS CARACTERES QUE CAUSEN PROBLEMAS, POR LO TANTO SE DEBERA DE AMPLIAR ESTA SERIE DE VERIFICACIONES
//Se buscaran las comillas dobles y se reemplazaran por comillas simples
	$cadena_a_remplazar='"';
	$cadena_de_sustitucion="'";			
	$texto_modificado=ereg_replace($cadena_a_remplazar,$cadena_de_sustitucion,$texto);				
	
	return $texto_modificado;
			
}//function sustituir_caracteres_problematicos($texto)	
	
//Consultamos en la tabla de idiomas, cuantos idiomas DISTINTOS hay disponibles en la web
$rs_idiomas = seleccion_unica("idiomas","idioma","idioma",0,$ruta_sevidor_bd,$usuario_bd,$contrasena_bd,$nombre_bd);
$registro_idioma = mysql_fetch_array($rs_idiomas);

//Ana Mª Gil 03/11/2010 ********Generamos el selecctor de idioma********

?>
<form name="idiomas" action="exportacion_idiomas_unico.php">
Seleccionar idioma: <select name="idi" id="idi">
<option value=""></option>
          <?php
do{
?>
		    <option value="<?=$registro_idioma["idioma"]?>"><?=$registro_idioma["idioma"]?></option>		    
	      
<?php
}while ($registro_idioma = mysql_fetch_array($rs_idiomas));
?> </select>
<input type="submit" />
</form>
<?php
// ********Fin selector idioma*********
	
// Ana Mª Gil 03/11!2010 ********** resocgemos el idioma seleccionado******


if ($_GET["idi"]!="")
{
	

			
//Esta variable almacenara el idioma en el que nos encontramos en cada momento		
		//$idioma_actual=$registro_idioma["idioma"];
             $idioma_actual=$_GET["idi"];
		echo("--------------Comienza la exportacion del idioma: ".$idioma_actual."--------------<br /><br />");
				
//Comprobamos si existe la carpeta del idioma
		if(file_exists($idioma_actual))
		{
//Abrimos el directorio
			$directorio = opendir($idioma_actual); 

//Recorremos el contenido del directorio y eliminamos cada archivo
			while($nombre_archivo = readdir($directorio))
			{ 
//Se omiten los archivos "." y ".."
				if($nombre_archivo!="." and $nombre_archivo!="..")
				{			
					$ruta_archivo=$idioma_actual."/".$nombre_archivo;										
					unlink($ruta_archivo);
				}//if($nombre_archivo!="." and $nombre_archivo!="..")				
			}//while($nombre_archivo = readdir($directorio)) 

//Cerramos el directorio, y lo eliminamos
			closedir($directorio); 
			rmdir($idioma_actual); 
		}//if(file_exists($idioma_actual))
		
//Como lo hemos eliminado el directorio, existiera o no existiera previamente, lo creamos
		mkdir($idioma_actual);

//Seleccionamos de la base de datos todos los registros del idioma que nos ocupa ordenados por seccion
		$rs_textos = seleccion_condicional("idiomas","idioma='".$idioma_actual."'","seccion",0,$ruta_sevidor_bd,$usuario_bd,$contrasena_bd,$nombre_bd);		
		
//Esta variable almacenara la ultima seccion con la que se ha trarado. Comienza teniendo el mismo valor que la seccion inicial del recodset
		$seccion_anterior="";
//Esta variable contendra el cuerpo del texto de los archivos de idiomas que se vayan creando, comienza con la etiqueta de apertura de PHP, si no el primer archivo que se cree no la tendría.
		$cuerpo="<?php\n";		
//Esta variable controlara el numero de textos totales, la emplearemos para controlar cuando terminar el bucle		
		$num_textos_totales=mysql_num_rows($rs_textos);
//Esta variable controlara el numero de textos que se han incluido en el archivo de texto
		$num_textos_incluidos=0;		
										
//Ahora se creara un archivo por cada seccion existente en la base de datos, de esta forma se optimiza, ya que todas las paginas no contienen
//todos los textos de la web, solo los que les corresponden		
//		while($registro_texto = mysql_fetch_array($rs_textos))

		while($num_textos_incluidos<=$num_textos_totales)
		{
//Posicionamos el recordet en el siguiente registro		
			$registro_texto = mysql_fetch_array($rs_textos);
										
//Si se ha cambiado de seccion crearemos el archivo con el contenido del cuerpo. Tambien en el caso de ser el ultimo registro, ya que si no
//saldria del bucle sin crear el ultimo archivo
			if($seccion_anterior!=$registro_texto["seccion"] or $num_textos_incluidos==$num_textos_totales)
			{
//Si el cambio de seccion no ha sido provocado por el primer cambio de seccion
				if($seccion_anterior!="")
				{									
//Esta variable almacenara la ruta completa del archivo de idioma que se va a crear																
					$archivo_creado=$idioma_actual."/".$seccion_anterior.".php";				
//Cerramos el codigo php
					$cuerpo=$cuerpo."?>";
										
//Esta variable almacenara la ruta completa del nuevo archivo a crear. Estara en una carpeta llamada como la abreviatura del idioma (creada anteriormente) y el nombre del archivo se llamara igual que la variable seccion de la base de datos con la extension .php					
					$archivo_creado=$idioma_actual."/".$seccion_anterior.".php";						
//Abrimos o creamos segun proceda, el archivo y colocamos el cursor al final del mismo
					$gestor_archivo = fopen($archivo_creado, "a+");
//Añadimos el texto que se ha ido almacenando en la variable cuerpo		
					fwrite($gestor_archivo,$cuerpo);
//Por ultimo se cierra el archivo	
					fclose($gestor_archivo);
					
					echo("Creado archivo: idiomas/".$archivo_creado."<br />");
					
				}//if($seccion_anterior!="")
							
				$cuerpo="<?php\n";				
//Asignamos el nuevo valor a la seccion anterior para saber cuando vuelva a cambiar
				$seccion_anterior=$registro_texto["seccion"];										
			}//if($seccion_anterior!=$registro_texto["seccion"])
			
//Añadimos el texto del registro actual, se haya cambiado de seccion o no
			$texto_corregido=sustituir_caracteres_problematicos($registro_texto["texto"]);
			$cuerpo=$cuerpo.$registro_texto["nombre_variable"]."=\"".$texto_corregido."\";\n";																								
						
//1º-DAR CONTENIDO AL CUERPO
						
//2º -COMPROBACION DEL CAMBIO DE SECCION AL FINAL DEL BUCLE

//3º- SI SE HA CAMBIADO SE CREA EL ARCHIVO, SI NO CONTINUA LA EJECUCION NORMAL DEL BUCLE
//Incrementamos el numero de registros incluidos
			$num_textos_incluidos++;			
		}//while($registro_texto = mysql_fetch_array($rs_textos))
		
//Liberamos de memoria el recordeset!!!!!!!
	
		echo("<br />--------------Finalizada la exportacion del idioma:  ".$idioma_actual."--------------<br /><br />");
			   			
	}
	
?>

</body>
</html>
