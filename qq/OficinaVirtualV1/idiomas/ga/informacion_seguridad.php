<?php
$informacion_seguridad1="Consellos para a Seguridade do seu Fogar";
$informacion_seguridad2="Seguridade e Protección da súa instalación eléctrica";
$informacion_seguridad3="Toda vivenda debe ter o Cadro de Mando e Protección. Se a súa vivenda é de nova construción, non se preocupe, xa teno instalado. Pero se é antiga, asegurese de que dispón dun, xa que é importante para a seguridade do seu fogar e os que viven nel.<br /><br />As proteccións incluídas no Cadro de mando e Protección son as seguintes:";
$informacion_seguridad4="1. Interruptor de Control de Potencia (ICP)";
$informacion_seguridad5="É un interruptor automático que determina a potencia simultánea dispoñible de acordo co Contrato de enerxía eléctrica.El ICP dará de baixa a súa instalación cando a suma da potencia dos aparellos eléctricos conectados a un tempo, supere a potencia contratada.De esta forma evítanse riscos innecesarios na súa instalación.";
$informacion_seguridad6="2. Interruptor Xeral de Corte (IGC)";
$informacion_seguridad7="Este interruptor permite a desconexión total da súa instalación en caso necesario. Tamén realiza a función de protección da súa instalación, de xeito que nunca se sobrepase a potencia máxima admisible da súa instalación.";
$informacion_seguridad8="3. Interruptor Diferencial (ID)";
$informacion_seguridad9="É o elemento de protección máis importante do Cadro de mando e Protección, pois protexe ás persoas de calquera accidente eléctrico. O interruptor diferencial dá de baixa a súa instalación cando detecta unha derivación nalgún aparello electrodoméstico ou nalgún punto da instalació n.Envivendas, estes interruptores diferenciais, deben ser de alta sensibilidade (30 mA.)";
$informacion_seguridad10="4. Pequenos interruptores magnetotérmicos (PIA)";
$informacion_seguridad11="Toda instalación eléctrica está formada por varios circuítos (iluminación, forza, cociña, etc...) e cada un destes circuítos deben de estar protexidos por sobrecargas e curtocircuítos por un interruptor magnetotérmico, de acordo coa capacidade de cada un dos circuítos interiores.";
$informacion_seguridad12=" recoméndalle:";
$informacion_seguridad13="Se ten que realizar un novo contrato de subministración, modifique ou substitúa o seu Cadro de Mando e Protección de acordo coa lexislación vixente para asegurar a súa instalación doméstica ante riscos eléctricos. Así velará pola súa seguridade e a dos seus familiares.<br /><br />Instale un interruptor Diferencial de 30 mA. De suficiente sensibilidade para evitar cortes na subministración da súa instalación (en caso dun funcionamento desaxeitado dalgún dos electrodomésticos)";
$informacion_seguridad14="Lexislación";
$informacion_seguridad15="As normas e condicións que deben reunir as instalacións eléctricas de Baixa Tensión foron definidas polo Ministerio de Industria e Enerxía e publicado no Real decreto 842/2002, e están reunidas no ";
$informacion_seguridad16="Regulamento Electrotécnico de Baixa Tensión";
?>