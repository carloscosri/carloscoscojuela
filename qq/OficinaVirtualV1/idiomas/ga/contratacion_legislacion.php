<?php
$contratacion_legislacion3="ORDE ITC 3860/2007";
$contratacion_legislacion4=", do 28 de decembro, por que se revisan as tarifas eléctricas a partir do 1 de xaneiro de 2008.";
$contratacion_legislacion5="REAL DECRETO 385/2002";
$contratacion_legislacion6=", do 26 de abril, por que se modifica o Real decreto 2018/1997, do 26 de decembro, por que se aproba o regulamento de puntos de medida dos consumos e tránsitos de enerxía eléctrica. (BOR 14-05-02).";
$contratacion_legislacion7="REAL DECRETO 1955/2000";
$contratacion_legislacion8=", do 1 de decembro, por que se regulan as actividades de transporte, distribución, comercialización, subministración e procedementos de autorización de instalacións de enerxía eléctrica. (BOE 27-12-00).";
$contratacion_legislacion9="REAL DECRETO-LEI 6/2000";
$contratacion_legislacion10=", do 23 de xuño, de Medidas Urxentes de Intensificación da Competencia en Mercados de Bens e Servizos. (BOE 24-06-00).";
$contratacion_legislacion11="LEI 54/1997";
$contratacion_legislacion12=", do 27 de novembro, do Sector Eléctrico. (BOE 28-11-97).";
$contratacion_legislacion1="Lexislación Eléctrica Española";
$contratacion_legislacion2="Nesta sección encontrará diversas ligazóns aos documentos correspondentes á lexislación eléctrica española.";
$contratacion_legislacion13="Ministerio de Industria, Turismo e Comercio";
?>