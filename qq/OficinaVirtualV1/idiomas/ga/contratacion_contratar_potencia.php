<?php
$contratacion_contratar_potencia1="Selección da Potencia a Contratar";
$contratacion_contratar_potencia2="A potencia de contrato de electricidade depende da cantidade e as características do equipamento do seu fogar: segundo que e cantos equipos teña, a potencia varía significativamente.<br /><br />
Contratar unha potencia desaxeitada ás súas necesidades non é nada conveniente. Se contrata máis potencia de que necesita, non só está malgastando diñeiro, senón que tamén está prexudicando o medio ambiente gastando máis enerxía da necesaria. E se, pola contra, contrata menos, pódense producir molestos cortes na subministración provocada polo ICP (Interruptor de Control de Potencia).";
$contratacion_contratar_potencia3="O cálculo da potencia supón que a utilización dos equipos de maior potencia non se realiza de forma simultánea. É unha potencia de contrato mínima recomendada de acordo cunha tarifa 2.0 xeral.<br /><br />
Se dispós de:";
$contratacion_contratar_potencia4="- Termo eléctrico instantáneo.<br />
- Radiadores eléctricos.<br />
- Calefacción acumulación.<br />
- Bomba de calor.<br />
- Aire acondicionado.
";
$contratacion_contratar_potencia5="Aconsellámosche que che poñas en contacto con nós no teléfono de atención ao cliente 972 59 46 91 ou nas nosas oficinas para que estudemos o teu caso máis exhaustivamente.";
?>