<?php
$titulo_potencias_menos_10="Tarifas de potencias contratadas ata 10 kW";
$titulo_potencias_mas_10="Tarifas de potencias contratadas superiores a 10 kW";
$titulo_potencias_alta_tension="Prezos para Contratos de Alta Tensión";
$termino_potencia="Termo de Potencia ";
$termino_energia="Termo de Enerxía ";
$euro_kw_mes="Euros / Kw / Mes";
$euro_kwh="Euros / kWh";
$volver="Volver";
$nota_potencias_menos_10="NOTA: Os prezos reflectidos poderán ser modificados durante o período de vixencia do contrato ou das súas prórrogas segundo os apartados 4 e 6 das condicións xerais do Contrato de Subministración.";
$nota_potencias_mas_10="";
$nota_potencias_alta_tension="Para aplicar as tarifas do tipo 6.1, farase un estudo individual e farase unha oferta personalizada para cada cliente.";
$tarifas_alta_tension1="Se necesita contratar unha Subministración en Media/Alta Tensión, deberá cubrir ";
$tarifas_alta_tension2="a solicitude que se indica no apartado correspondente.";
$tarifas_alta_tension3="Posteriormente porémosnos en contacto con vostede para informarlle a preto dos nosos prezos e condicións.";
$titulo_potencias_mas_15="Prezos para Contratos de Potencias Maiores de 15 KW";
?>