<?php
$error_rellenar="Deben reencherse os campos marcados como obrigatorios.";
$error_fecha="A data escribiuse de forma incorrecta. Debe empregar o seguinte formato: 18-03-2009.";
$error_fecha_posterior="As datas escritas non poden ser posteriores á data actual.";
$error_fecha_inicio_mayor="A data de inicio non pode ser maior que a data final.";
$solicitud_ok="A súa solicitude foi enviada con éxito";
$oficina_informacion_facturas1="CONSUMOS ACTIVOS";
$oficina_informacion_facturas2="P1 Punta";
$oficina_informacion_facturas3="P2 Llano";
$oficina_informacion_facturas4="P3 Val";
$oficina_informacion_facturas5="CONSUMOS REACTIVOS";
$oficina_informacion_facturas6="IMPORTES";
$oficina_informacion_facturas7="Active";
$oficina_informacion_facturas8="Reactiva";
$oficina_informacion_facturas9="Maxímetro";
$oficina_informacion_facturas10="Descontos";
$oficina_informacion_facturas11="Cargos";
$oficina_informacion_facturas12="Base";
$oficina_informacion_facturas13="IVA";
$oficina_informacion_facturas14="TOTAL:";
$oficina_informacion_facturas15="Imprimir";
$error_no_facturas="Actualmente non hai facturas relacionadas con este cliente na nosa base de datos.";
?>