<?php
$oficina_cambio_pass1="Introduza ou seu contraseña";
$oficina_cambio_pass2="Introduza nova contraseña";
$oficina_cambio_pass3="Confirme a súa nova contraseña";
$error_rellenar="Debe reencher todos os datos.";
$error_pass_invalida="O contrasinal non é correcta.";
$pass_no_coincide="O novo contrasinal e a confirmación non coinciden.";
$mensaje_actualizar_pass="Actualizouse o seu contrasinal.";
?>