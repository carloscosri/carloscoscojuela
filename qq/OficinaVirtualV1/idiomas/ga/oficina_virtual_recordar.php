<?php
$oficina_virtual_recordar1="Pola súa propia seguridade a información do contrasinal esquecido, soamente será proporcionada ao usuario no teléfono ou dirección e-correo electrónico que proporciónanos.";
$oficina_virtual_recordar2="Usuario";
$oficina_virtual_recordar3="Teléfono";
$oficina_virtual_recordar4="E-mail";
$oficina_virtual_recordar5="Aceptar";
$oficina_virtual_recordar6="Volver";
$error_recordar_vacio="Para facilitarlle a súa clave debe escribir o seu número de usuario e polo menos un dos datos de contacto.";
$error_no_usuario="Non existe ese usuario.";
$error_telefono="O teléfono só pode conter carácteres numéricos.";
$error_mail="Non escribiu o e-correo electrónico correctamente.";
$recordar_ok="Nos pondrémos en contácto con vostede para facilitarlle a súa clave.";
?>