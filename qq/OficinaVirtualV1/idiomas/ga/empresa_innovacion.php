<?php
$empresa_innovacion1="Innovación";
$empresa_innovacion3="- Plan de Infraestruturas na Rede de Distribución.<br />
- Plan de Instalación de Novos Aparellos de Medida (Contadores).<br />
- Plan de Innovación TIC para Xestións de Clientes.";
$empresa_innovacion2="ten establecido un Plan de Innovación que abarca aspectos tan importantes como son:";
$empresa_innovacion4="Plan de Infraestruturas na Rede de Distribución.";
$empresa_innovacion5="Estamos en plena fase de melloras na nosa Rede de Distribución, en
Liñas de Acometida, Centros de Transformación, Transformadores,
Circuítos...<br /><br />
A renovación e substitución dos materiais que constitúen nosa
Rede de Distribución, é un feito de constante actividade, co fin
de reducir ao mínimo as avarías e garantir un mellor servizo facía
os nosos clientes.";
$empresa_innovacion6="Plan de Instalación de Novos Aparellos de Medida (Contadores).";
$empresa_innovacion7="De acordo coa Normativa Legal sobre os Aparellos de Medida (Contadores),
éstánse realizando a substitución de todos aqueles de tecnoloxía analóxica
polos novos de tecnoloxía dixital. Estes aparellos levan integrados todos os requisitos técnicos actuais e futuros.<br /><br />
Iso comporta que poidan estar totalmente preparados para as próximas
Normativas Legais de Telemedida e Telexestión, que facilitasen os procesos de xestionar toda a problemática do cliente en forma remota a través de Teleproceso.";
$empresa_innovacion8="Plan de Innovación TIC para Xestións de Clientes.";
$empresa_innovacion9="O Plan de Innovación TIC (Novas Tecnoloxías) que estamos implementando,
trata de ofrecer aos seus clientes unha xestión directa a través da nosa nova páxina web, facilitando todas as necesidades que se propoñan en relación coa nosa empresa, directamente desde o seu domicilio e sen necesidade de desprazarse ás nosas oficinas.<br /><br />
Poderá realizar xestións como:";
$empresa_innovacion10="- Contratar un novo Punto de Subministración.<br />
- Calquera cambio no seu contrato (Tarifas, Potencias, Domicilios, Conta Bancaria...).<br />
- Imprimir copias de facturas que xa foron emitidas.<br />
- Información e Certificados relativos aos seus consumos en kWh e / ou importes facturados.<br />
- Comunicar Lecturas de Contadores senón foi posible tomara.<br />
- Comunicar calquera Avaría ou Incidencia.";
$empresa_innovacion11="É dicir, calquera necesidade de xestión poderase realizar a través da Oficina Virtual en nosa Paxina Web.";
?>