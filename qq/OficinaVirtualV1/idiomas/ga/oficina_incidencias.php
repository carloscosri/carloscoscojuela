<?php
$error_rellenar="Deben reencherse os campos marcados como obrigatorios.";
$error_fecha="A data escribiuse de forma incorrecta. Debe empregar o seguinte formato: 18-03-2009.";
$error_hora="A hora e os minutos deben ser 2 carácteres numéricos entre 00 e 23 e entre 00-59 respectivamente.";
$error_telefonos="Os campos Teléfonom, Móbil e Fax só poden conter carácteres numéricos.";
$error_mail="Non escribiu o e-correo electrónico correctamente.";
$incidencia_ok="A súa incidencia foi enviada.";
$error_fecha_posterior="As datas escritas non poden ser posteriores á data actual.";
?>