<?php
$oficina_infofactura_ampliada1="Logotipo de Comercializadora e datos identificativos da factura.";
$oficina_infofactura_ampliada2="Datos do titular da subministración e características técnicas do mesmo.";
$oficina_infofactura_ampliada3="Datos de lecturas e consumos.";
$oficina_infofactura_ampliada4="Desagregación detallada dos conceptos facturados. ";
$oficina_infofactura_ampliada5="Datos de pagamento e información para o cliente.";
$oficina_infofactura_ampliada6="Mais informacion";
?>