<?php
$informacion_consejos_termo1="Consellos para Aforrar Enerxía - Termo";
$informacion_consejos_termo2="Termo eléctrico por acumulación nocturna:";
$informacion_consejos_termo3=" O dispor de auga quente forma parte do mínimo confort. O seu funcionamento é sinxelo: a auga fría da rede xeral, entra pola parte inferior do termo e é quentada no interior do aparello por medio dunha resistencia eléctrica, que funciona en horario nocturno, ata unha temperatura aproximada de 60ºC, valor que se pode regular mediante termóstato. Para lograr o seu obxectivo debe reunir as seguintes condicións:";
$informacion_consejos_termo4="1. A auga debe de estar dispoñible en todo momento.<br />
2. A cantidade de auga debe ser suficiente para cubrir a demanda da vivenda.<br />
3. A auga ten que ser fornecida á temperatura desexada.<br />
4. O consumo enerxético debe realizarse durante a noite, cando a electricidade resulta un 53% máis barata.";
$informacion_consejos_termo5="Consellos para a Compra do Termo Eléctrico:";
$informacion_consejos_termo6="Existen no mercado unha ampla gama de termos electrónicos. Con obxecto de obter o máximo rendemento, débese seleccionar axeitadamente o aparello que se desexa adquirir. Para iso cómpre prestar atención aos seguintes aspectos:";
$informacion_consejos_termo7="<strong>A cantidade de auga necesaria.</strong> débese elixir un termo que sexa capaz de atender a demanda de auga segundo o número de membros da familia, das instalacións sanitarias e dos hábitos individuais.<br /><br />
<strong>A natureza da auga.</strong> É moi importante que o calderín sexa o axeitado ao tipo de auga da zona, xa que se esta é moi agresiva ou contén exceso de sales minerais pode corroer as súas paredes. O calderín ou recipiente acumulador pode ser de chapa de aceiro esmaltado, de aceiro galvanizado ou esmaltado, e a auga da cidade é moi agresiva, é conveniente que o aparello estea provisto dun ánodo magnesio. Este elemento prevén a corrosión das paredes interiores do calderín.<br /><br />
<strong>Tipo e grosor do illamento. </strong>É interesante resaltar que a enerxía consumida para quentar e manter unha mesma cantidade de auga a unha mesma temperatura variará segundo a calidade do illamento.";
$informacion_consejos_termo8="Instalación:";
$informacion_consejos_termo9="O emprazamento do aparello. Pode instalarse en calquera lugar: alazadeiras, debaixo dos vertedoiros, sobre falsos teitos, etc... Non necesitan nin ventilación nin chemineas nin saída de gases. Unicamente deberá tomarse a precaución de deixar accesible a parte do termo en cuxo interior teña que realizarse algunha operación de mantemento ou reparación.
Se os puntos de utilización da auga quente están afastados, é preferible utilizar dous ou máis aparellos, para evitar perdas de calor nas tubaxes.<br /><br />
Cando a instalación do termo realícese en cuartos de baño, deberase respectar o indicado no Regulamento Electrotécnico de Baixa Tensión. O termo eléctrico deberá estar fóra do volume de prohibición, con obxecto de evitar que a auga salpique o interior da caixa de conexións do aparello.<br /><br />
Para a conexión eléctrica do termo deberá utilizarse unha base de enchufe de 16 A, con toma de terra. É aconsellable instalar un interruptor de corte bipolar, que permite a fácil desconexión do termo, no momento de utilizar o baño ou ducha.";
$informacion_consejos_termo10="Recomendacións de uso:";
$informacion_consejos_termo11="Unha vez instalado o seu termo eléctrico é aconsellable seguir algunhas indicacións sinxelas, á hora de utilizaro, que lle proporcionarán unha mellor calidade de servizo a un tempo que axudarán á conservación do seu equipo.
Con tal fin, ofrecémoslle a continuación unha serie de consellos prácticos que vostede deberá ter en conta cando use o seu termo eléctrico.";
$informacion_consejos_termo12="Se a auga quente que acumula o termo durante a noite resulta insuficiente para todo o día, pódese optar por unha, ou por ambas as dúas, das dúas solucións seguintes:<br /><br />
- Aumentar a temperatura de quentamento do termo accionando o seu termóstato. O manual de instrucións mostra onde está situado e como manexaro.<br /><br />
- Axustar o programador para que o termo poida conectarse tamén de día, pero só durante o mínimo tempo que cumpra para cubrir as necesidades de auga quente.<br /><br />
Comprobar que a temperatura da auga quente non sobrepase os 60ºC, que é a axeitada para a seguridade das persoas e para unha maior duración do aparello. Desta forma lógrase previr a corrosión e diminuír a calcificación nun 50%.<br /><br />
Eliminar as incrustacións do depósito, así como da válvula de seguridade, cando se observe que o caudal de auga quente diminuíu considerablemente.";
$informacion_consejos_termo13="Mantemento:";
$informacion_consejos_termo14="É conveniente consultar o manual de instrucións para que cun mínimo de mantemento conseguir a máxima duración do equipo.<br /><br />
Esixir ter a man a garantía do termo. O período de vixilancia da mesma cobre a reparación das avarías que, ocasionalmente, poidan producirse.<br /><br />
Se se mestura a auga, deixar saír primeiro a auga fría e despois mesturar con auga quente, ata acadar a temperatura desexada.";
$informacion_consejos_termo15="Consumo Aproximado de Enerxía:";
$informacion_consejos_termo16="Tarefa";
$informacion_consejos_termo17="Unha ducha (30 litros a 40ºC)";
$informacion_consejos_termo18="Un baño (90 litros a 40ºC)";
$informacion_consejos_termo19="Un lavado a man de vaixela (15 litros a 40ºC)";
$informacion_consejos_termo20="Consellos Prácticos";
$informacion_consejos_termo21="Nos meses de verán, cando non cómpre que a auga saia tan quente, reduza a temperatura accionando o termóstato.";
$informacion_consejos_termo22="En ausencias prolongadas, superiores a tres ou catro días, dea de baixa o aparello. Se a ausencia escorta, é preferible reducir a temperatura do termóstato e deixar conectado o termo.";
$informacion_consejos_termo23="Aforre na ducha entre un 4% e un 6% de enerxía cos reguladores de temperatura con termóstato.";
$informacion_consejos_termo24="A temperatura aconsellada para gozar dunha agradable sensación no aseo persoal é de 30º a 35º.";
$informacion_consejos_termo25="Manteña as billas en bo estado para que non pinguen, pecheos completamente despois do seu uso e repare en seguida os que presenten fugas de auga. Unha billa que pinga pode gastar ao mes 170 litros de auga.";
$informacion_consejos_termo26="Teña en conta que nunha ducha se consume a terceira parte da enerxía que se consume nun baño.";
$informacion_consejos_termo27="Non utilice máis auga quente da necesaria. Por exemplo, non deixe a billa aberta mentres enxabóase ou lávase os dentes.";
$informacion_consejos_termo28="Adquira cabezales de ducha de baixo consumo, permiten un aseo cómodo gastando a metade de auga e de enerxía.";
$informacion_consejos_termo29="Pode colocar nas billas redutoras de caudal (aireadores).";
?>