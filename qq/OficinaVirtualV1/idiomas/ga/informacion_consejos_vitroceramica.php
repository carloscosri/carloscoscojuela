<?php
$informacion_consejos_vitroceramica1="Consellos para Aforrar Enerxía - Vitrocerámica";
$informacion_consejos_vitroceramica2="Cociña eléctrica: encimera vitrocerámica";
$informacion_consejos_vitroceramica3="É unha cociña en que as zonas de cocción foron serigrafiadas sobre unha superficie chaira vitrocerámica cun elevado grao de resistencia ao peso, aos golpes e ás diferenzas de temperatura. A calor óbténse dunhas resistencias eléctricas situadas baixo as zonas de cocción e, nalgúns casos, da acción combinada desas resistencias e unhas lámpadas halóxenas.<br /><br />
Dependendo dos modelos e fabricantes, poden ter 2, 4 ou 5 puntos de cocción. Existen modelos que teñen unha zona lateral que funciona a baixa potencia (150 W) e que se utiliza para manter mornos os alimentos. A incorporación de calefactores de media lúa nunha das zonas de cocción, permite a utilización de recipientes alongados.
Polos seus característicos exteriores é a cociña máis limpa e segura, de máis fácil utilización e máis precisa.<br /><br />
A situación dos mandos poden ser as seguintes: ";
$informacion_consejos_vitroceramica4="1. <strong>Na encimera. </strong>Resultas moi útiles, cando se trata de substitucións en renovación de cociña como tal aparello, ou outras similares.<br /><br />
2. <strong>En panel de independencia. </strong>Permite colocar os mandos nunha determinada situación, por exemplo, baixo a viseira do moble, o que os protexe da suciedad.<br /><br />
3. <strong>Incorporados ao forno.</strong> Os mandos incorporados ao forno teñen a vantaxe de ofrecer unha superficie de traballo maior.";
$informacion_consejos_vitroceramica5="Tipos de vitrocerámica (en función dos elementos calefactores):";
$informacion_consejos_vitroceramica6="Desde a aparición das Encimeras Vitrocerámicas, os elementos calefactores utilizados desenvolvéronse e diversificado fortemente. Existen tres grandes grupos no referente ás tecnoloxías empregadas: resistencias radiantes, halóxenos e de Indución.<br /><br />
As potencias máximas máis usuais dos puntos de cocción son 1200, 1700 e 2100 W. Os actuais sistemas permiten regular a potencia de cocción a valores inferiores aos indicados segundo a posición do mando.<br /><br />
Os puntos de cocción con halóxenos presentan vantaxes reais, tales como: ";
$informacion_consejos_vitroceramica7="- Emisión de calor e luz instantánea.<br />
- Rapidez de quentamento.<br />
- Menor dependencia da calidade do recipiente.<br />
- Flexibilidade e rapidez de reacción ao mando da placa.<br />
- Maior seguridade contra os erros de manipulación ao verse decontado se se conecta.";
$informacion_consejos_vitroceramica8="Recomendacións de uso";
$informacion_consejos_vitroceramica9="Lea atentamente as instrucións contidas no manual do fabricante da cociña. Aí coñecerá cal é a posición do mando regulador de calor máis axeitada ao tipo de cocción, e a cantidade de alimentos a cociñar.<br /><br />
Utilice utensilios de cociña con fondo difusor plano e liso e un diámetro igual ou superior ao da zona de cocción. Se non dispón deles, hai unha gran variedade de modelos e marcas que hai que elixir entre os que foron especialmente deseñados para ser utilizados en vitrocerámicas. En cambio desaconséllase o emprego de recipientes de aluminio ou cazuelas de barro.<br /><br />
As cociñas eléctricas vitrocerámicas incorporan, normalmente unha sinalización de cada zona de cocción, que se ilumina cando a temperatura da mesma é maior de 50 º C e permanece así, unha vez deu de baixase, ata que a temperatura baixa deste límite.<br /><br />
Existen modelos que automaticamente dan de baixa as placas ao retirar os recipientes, co cal non existe consumo aínda que permaneza conectada.";
$informacion_consejos_vitroceramica10="Mantemento:";
$informacion_consejos_vitroceramica11="A cociña eléctrica vitrocerámica non precisa practicamente mantemento. É aconsellable limpar a superficie despois de cada uso, unha vez apagados os focos de calor, cun pano ou papel de cociña para que recupere brillo. Se quedaron residuos sólidos adheridos sobre a superficie, recoméndase arrincaros cunha rasqueta antes de que se enfríen e, logo, enxaugar e secar a placa.<br /><br />
Esixa e teña a man a garantía do Fabricante da Cociña. Durante o seu período de vixencia cobre a reparación das avarías que ocasionalmente poden producirse.";
$informacion_consejos_vitroceramica12="VANTAXES:";
$informacion_consejos_vitroceramica13="As encimeras vitrocerámicas polo seu deseño e acabado totalmente novo e atractivo, e polas súas características de visualización, flexibilidade e rapidez, son un produto altamente competitivo e vantaxoso fronte a outros tipos de cociñas.";
$informacion_consejos_vitroceramica14="Velocidade, precisión e visualización: Conseguen unha velocidade de quentamento comparable a calquera outro sistema e enerxía. O seu tempo de resposta é practicamente inmediato, visualizandose as placas en funcionamento.";
$informacion_consejos_vitroceramica15="Estética: Con liñas elegantes e depuradas e superficies totalmente lisas, conseguen unha elegancia inalcanzable por calquera outro tipo de encimeras.";
$informacion_consejos_vitroceramica16="Facilidade de utilización e limpeza: As tarteiras, sartenes e recipientes en xeral esvaran pola súa superficie sen bascular, polo que resulta moi fácil a súa manipulación, e a volta é mas difícil que noutros tipos de cociñas, cuxa superficie non sexa completamente lisa. Ao non dispor de regañas nin zonas de difícil acceso, ou outras zonas que requiren unha limpeza intensiva, esta resulta máis fácil. Para efectuar a limpeza normal recoméndase que a placa estea fría ou morna.";
$informacion_consejos_vitroceramica17="Solidez e resistencia: A vitrocerámica é un material moi robusto.";
$informacion_consejos_vitroceramica18="Seguridade: Á seguridade propia da utilización da enerxía eléctrica, engádese que a calor se concentra nas zonas de cocción, sen perigos de queimaduras se se toca o resto da superficie. Ao non haber combustible, non existe o risco de explosión fortuíta.";
$informacion_consejos_vitroceramica19="Cociña eléctrica: encimera vitrocerámica por indución:";
$informacion_consejos_vitroceramica20="A cociña vitrocerámica por indución é fría, a máis limpa, a de máis fácil utilización, a máis segura, a máis moderna e a máis rápida.<br /><br />
A cociña eléctrica vitrocerámica por indución é lisa, sen queimadores, e a súa superficie é un vidro cerámico, con alto grao de resistencia ao peso e aos golpes. A súa precisión en cada temperatura de cocción é exacta, ademais non se quenta a placa.<br /><br />
Nestas cociñas a calor xérase no propio recipiente de cociñar a partir dun campo magnético creado por un elemento que está situado baixo a superficie vitrocerámica, polo que non hai perdas de calor.";
$informacion_consejos_vitroceramica21="Erabilera eta mantentzearen gomendioak:";
$informacion_consejos_vitroceramica22="Iniciar a cocción coa numeración máis alta de calor de mando, para descender posteriormente á posición desexada.<br /><br />
Se só deséxase calor suave, usar numeracións baixas ata conseguir exactamente a calor desexada.<br /><br />
Ao situarse sobre a posición 0 do mando, deíxase de subministrar calor instantaneamente (a ebulición déténse ao momento).<br /><br />
Se non hai recipiente sobre a zona de cocción, esta non subministra calor aínda que está conectada.<br /><br />
Para a limpeza das encimeras vitrocerámicas por indución, abondará con usar un pano húmido (de papel, reixa, spontex, ou similar). Pero se cómpre, poden usarse deterxente axeitado que existan no mercado, enxaugando e secando ben despois do seu uso para recuperar o seu brillo.<br /><br />
Obterase un notable aforro de enerxía usando os recipientes tapados ou medio tapado sempre que sexa posible.";
$informacion_consejos_vitroceramica23="Consumo aproximado de enerxía";
$informacion_consejos_vitroceramica24="Tarefa";
$informacion_consejos_vitroceramica25="Gas (kWh)";
$informacion_consejos_vitroceramica26="Electricidade (kWh)";
$informacion_consejos_vitroceramica27="Filete prancha";
$informacion_consejos_vitroceramica28="Xudías cocidas en cazuela de aceiro (4 persoas)";
$informacion_consejos_vitroceramica29="Arroz (4 persoas)";
$informacion_consejos_vitroceramica30="Recipientes apropiados para este tipo de cociña:";
$informacion_consejos_vitroceramica31="Deben utilizarse recipientes de material ferromagnético, fondo plano, liso e o máis groso posible. Esta propiedade e facilmente comprobable cun imán que deberá ser atraído polo recipiente. Polo tanto poden utilizarse utensilios de aceiro esmaltado, de ferro fundido ou de aceiro inoxidable que conteñan algún material ferromagnético.<br /><br />
Os recipientes con fondo tipo sándwich, con tres ou máis capas que conteñan algún material ferromagnético, son utilizables na maioría dos casos.<br /><br />
Aínda que é necesaria unha cantidade mínima de masa ferromagnética para poder funcionar, non deixar utensilios metálicos sobre a zona de cocción. En ningún caso débense utilizar utensilios de aluminio ou de barro, xa que non funcionaría a placa.";
$informacion_consejos_vitroceramica32="VANTAXES:";
$informacion_consejos_vitroceramica33="Subministración de enerxía: A electricidade encóntrase instalada en todos os fogares, polo que non cómpre preocuparse de ir buscar a enerxía con que cociñar ou almacenada.";
$informacion_consejos_vitroceramica34="Velocidade, presión e visualización: Conseguen unha velocidade de quentamento comparable a calquera outro sistema de enerxía. O seu tempo de resposta é inmediato, visualizandose as placas en funcionamento mediante un piloto censor de temperatura.";
$informacion_consejos_vitroceramica35="Estética: Con liñas elegantes e depuradas, e superficies totalmente lisas, conseguen unha elegancia inalcanzable por calquera tipo de encimeras.";
$informacion_consejos_vitroceramica36="Facilidade de utilización e limpeza: As tarteiras, sartenes e recipientes en xeral esvaran pola súa superficie sen bascular, polo que resulta moi fácil a súa manipulación, e a volta é máis difícil que noutros tipos de cociñas cuxa limpeza intensiva e xa que a placa permanece fría ou cunha leve calor transmitida polo propio recipiente, a súa limpeza limítase a pasar un pano húmido. Solidez e resistencia: A vitrocerámica é un material moi robusto.";
$informacion_consejos_vitroceramica37="Seguridade: Á seguridade propia da utilización da enerxía eléctrica, engádese que as zonas de cocción non teñen perigo de queimaduras se se tocan ao retirar o recipiente, xa que a encimera permanece fría.";
$informacion_consejos_vitroceramica38="Consellos Prácticos";
$informacion_consejos_vitroceramica39="Procure cociñar con pouca auga e cos recipientes tapados ou medio tapado.";
$informacion_consejos_vitroceramica40="O uso da olla a presión é moi aconsellable, pode aforrar ata un 50% de enerxía.";
$informacion_consejos_vitroceramica41="Tendo en conta que as zonas de cocción, unha vez desconectadas manteñen a ebulición e a calor dos alimentos de 5 a 7 minutos máis (agás as de indución), apageas un pouco antes de acabar a cocción.";
$informacion_consejos_vitroceramica42="Á hora de cociñar teña en conta que o fondo dos recipientes debe ser lixeiramente superior á zona de cocción para aproveitar ao máximo a calor.";
$informacion_consejos_vitroceramica43="É recomendable a utilización de menaxe con fondo groso difusor pois conséguese unha temperatura máis homoxénea en todo o recipiente.";
$informacion_consejos_vitroceramica44="Coas cociñas vitrocerámicas por indución o aproveitamento enerxético é total: detecta se hai ou non hai recipiente sobre a súa superficie, actuando só no primeiro caso.";
?>