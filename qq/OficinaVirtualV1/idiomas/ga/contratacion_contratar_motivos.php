<?php
$contratacion_contratar_motivos1="Motivos para contratar con ";
$contratacion_contratar_motivos2="A nosa filosofía é dar o máximo en todo o que observa o noso alcance: calidade de subministración, atención, asesoramento, inspeccións, seguridade, innovación...";
$contratacion_contratar_motivos4="Terá á súa disposición a un gran equipo de técnicos e profesionais dispostos a atenderlle en todo momento.";
$contratacion_contratar_motivos3="Garantímoslle toda nosa a experiencia dunha empresa que durante anos subministrou enerxía eléctrica.";
$contratacion_contratar_motivos5="As nosas oficinas, almacéns e técnicos encóntranse na nosa cidade e, en caso de necesidade, estamos preto de vostede para atenderlle.";
$contratacion_contratar_motivos6="Os beneficios que xera a empresa quédanse e invisten, de forma maioritaria, na nosa cidade.";
$contratacion_contratar_motivos7="Temos un compromiso con vostede e co mundo que nos rodea, xa que un aforro enerxético non é só bo para o seu peto, senón tamén para a natureza.";
?>