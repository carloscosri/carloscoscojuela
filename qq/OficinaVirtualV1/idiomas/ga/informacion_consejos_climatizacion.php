<?php
$informacion_consejos_climatizacion1="Consellos para Aforrar Enerxía - Climatización";
$informacion_consejos_climatizacion2="A climatización é o proceso de tratamento do aire que permite controlar a temperatura, a humidade, o movemento e a limpeza do aire no interior dun local. Os valores destes parámetros inflúen moi directamente sobre o confort, e varían coa época do ano.<br /><br />
A Bomba de Calor é o único sistema que cobre todas as necesidades de frío en verán e de calor en inverno nunha vivenda, co cal obtemos un aforro considerable no investimento inicial, ao non ter que comprar aparellos para frío e aparellos para calor.<br /><br />
Hai que ter en conta que a esta vantaxe se une a do seu elevado rendemento, xa que aproveita a enerxía existente no aire ambiente.";
$informacion_consejos_climatizacion3="Este sistema permítenos:";
$informacion_consejos_climatizacion4="1. Controlar a temperatura.<br />
2. Controlar o movemento do aire.<br />
3. Eliminar as impurezas do aire.";
$informacion_consejos_climatizacion5="A Bomba de Calor é limpa, e non produce fumes nin cheiros ao existir ningún tipo de combustión, tendo ademais, a posibilidade da renovación do aire segundo o tipo de aparello.";
$informacion_consejos_climatizacion6="Antes de comprar un sistema de climatización:";
$informacion_consejos_climatizacion7="Á hora de instalar un sistema de climatización debe buscarse o asesoramento dun instalador especializado.<br /><br />
Para obter o máximo confort é imprescindible que o cálculo das frigorías / calorías necesarias realícese con fiabilidade suficiente. Para iso, o instalador terá en conta as características da vivenda: orientación, detalles construtivos, fontes de calor, etc.El sistema de distribución do aire debe ser o axeitado para que a calor e o frío repártanse uniformemente.Para ventilar os cuartos cómpre que o sistema de climatización teña unha toma de aire exterior.";
$informacion_consejos_climatizacion9="Os sistemas de climatización por Bomba de Calor, contrólanse mediante un mando centralizado. O seu uso é simple e fácil, só hai que seleccionar:";
$informacion_consejos_climatizacion8="Recomendacións de uso:";
$informacion_consejos_climatizacion10="- A temperatura desexada desde o termóstato.<br />
- O xeito de utilización: frío, calor ou ventilación.<br />
- A velocidade de ventilador.";
$informacion_consejos_climatizacion11="Existen algúns modelos que levan incorporado un programador horario de posta en marcha.";
$informacion_consejos_climatizacion12="Mantemento:";
$informacion_consejos_climatizacion13="A unidade interior que enfría ou quenta o aire, dispón dun filtro, que serve para limpar o aire da vivenda. A única precaución que hai que ter, é mantero limpo, seguindo as indicacións do fabricante.";
$informacion_consejos_climatizacion14="Consellos Prácticos:";
$informacion_consejos_climatizacion15="As temperaturas recomendadas de confort son 25º en verán e entre 18 e 20º en inverno.";
$informacion_consejos_climatizacion16="O axuste da temperatura é moi importante para controlar o consumo, por exemplo: fixar unha temperatura en inverno de 20º en lugar de 21º pode significar un aforro de enerxía do 10%; en verán, pola contra, por cada grao de temperatura que se fixe por debaixo dos 25º, estará consumindo de máis.";
$informacion_consejos_climatizacion17="Pode reducir o quentamento da súa vivenda en verán colocando toldos, pechando persianas e correndo as cortinas.";
$informacion_consejos_climatizacion18="En verán é conveniente ventilar a casa cando o aire de cálea sexa máis fresco: noite e primeiras horas da mañá.";
$informacion_consejos_climatizacion19="Outra maneira de evitar o quentamento de espazos interiores é pintar teitos e paredes exteriores con cores claras para reflectir a radiación solar.";
$informacion_consejos_climatizacion20="Coloque o climatizador nun lugar onde non lle do sol e haxa boa circulación de aire.";
?>