<?php
$submenu_contratacion_oficina1="Tarifas a contratar:";
$submenu_contratacion_oficina2="- Bono Social (Inferior a 3 kWh).
";
$submenu_contratacion_oficina3="- Contratos ata 10 kWh con ou sen discriminacion horaria (Tarifas 2.0.A e 2.0 DHA).
";
$submenu_contratacion_oficina4="- Contratos de 10 a 15 kWh con ou sen discriminacion horaria (Tarifas 2.1.A e 2.1 DHA).";
$submenu_contratacion_oficina5="- Contratos a partir de 15 kWh (Tarifas 3.0.A e 3.1.A e 6.x).";
$oficina_contratacion1="Requirimentos Administrativos para dar de alta un novo punto de subministración:";
$oficina_contratacion2="NOTA: Para calquera información adicional sobre tarifas a contratar e tipo de subministración, deben consultar directamente nas nosas oficinas.";
$oficina_contratacion3="Condicións para o Bono Social:";
$oficina_contratacion4="O Bono Social é só de aplicacion a persoas físicas cuxo subministración se destine par o uso en
vivenda habitual e que pertenza a algún dos seguintes colectivos:<br /><br />
Persoas físicas cunha potencia contratada inferior a 3kW na súa vivenda habitual. Se lle aplicará
o Bono Social Automáticamente.<br /><br />
Clientes con idade mínima de 60 anos que perciban unha pensión mínima.<br /><br />
Clientes de máis de 60 anos que perciban pensións non contributivas de xubilación e invalidez, así como
beneficiarios de pensións do extinguido Seguro Obrigatorio de Vellez e invalidez.<br /><br />
Familias numerosas.<br /><br />
Familias con todos os seus membros en situación de desemprego.";
$oficina_contratacion5="Referencia Catastral.";
$oficina_contratacion6="CUPS (Código Identificador do seu Punto de Subministración) no suposto de que proveña doutra Comercializadora.";
$oficina_contratacion7="Esta solicitude de novo subministro terá carácter meramente informativo. A nosa empresa porase en contacto con vostede nos próximos días para presentarlle unha oferta económica personalizada, a través de e-correo electrónico ou correo ordinario.<br /><br />
Unha vez aceptado o devandito orzamento será cando podamos finalizar coa nosa empresa o contrato de subministración de enerxía eléctrica no punto de subministración e condicións solicitadas.<br />
Para que poida recibir esta información deberá encher o formulario correspondente á potencia que desexe contratar, segundo os grupos que a continuación analízanse.";
$oficina_contratacion8="Título de propiedade ou contrato de aluguer.";
$oficina_contratacion9="Cédula de habitabilidade.";
?>