<?php
$contratacion_ofertas1="Sistema para consultar Prezos e Ofertas dirixidas a Particulares e Empresas que desexen contratar a Subministración Eléctrica con ";
$contratacion_ofertas2="Enerxía ";
$contratacion_ofertas3="Inferior ou igual a 10 kW.<br />Tarifas de Acceso 2.0A e 2.0DHA.";
$contratacion_ofertas4="Superior a 10 kW e inferior a 15 kW.<br />Tarifas de Acceso 2.1A e 2.1DHA.";
$contratacion_ofertas5="Superior a 15 kW.<br />Tarifa 3.0A.";
$contratacion_ofertas6="Se a súa potencia contratada é<br />
superior a 50 kW ou o seu consumo anual<br />é superior a 100.000 kWh.";
$contratacion_ofertas7="Para determinados niveis de consumo, pódese solicitar unha oferta personalizada. Isto pódeo realizar enchendo o seguinte ";
$contratacion_ofertas10="modelo de solicitude de oferta en formato PDF editable";
$contratacion_ofertas11="e enviaro por fax ou ";
$contratacion_ofertas12="e-mail";
$contratacion_ofertas8="formulario";
$contratacion_ofertas9=", ou ben enchendo o ";
?>