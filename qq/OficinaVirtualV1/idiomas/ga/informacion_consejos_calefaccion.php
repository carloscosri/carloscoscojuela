<?php
$informacion_consejos_calefaccion1="Consellos para Aforrar Enerxía - Calefacción";
$informacion_consejos_calefaccion2="Calefacción eléctrica. Aforrar enerxía con acumuladores de calor e tarifa nocturna 2.0:";
$informacion_consejos_calefaccion3="Os acumuladores de calor son aparellos capaces de almacenar enerxía calorífica durante un período de tempo, para liberara despois lentamente, con fluxo controlable. Son elementos idóneos para calefacción ambiental tendo en conta que utilizan as horas da noite para almacenar a calor, aproveitando ao máximo as vantaxes da Tarifa Nocturna. O usuario desta tarifa, obtén un desconto dun 53% no prezo da enerxía consumida durante as horas, nocturnas, e teñen unha recarga do 3% no prezo do quilovatio hora consumida durante o día.";
$informacion_consejos_calefaccion4="Tipos de acumuladores:";
$informacion_consejos_calefaccion5="Existen dous tipos de acumuladores, cuxa diferenza é a forma de ceder calor:<br /><br />
<strong>Acumuladores estáticos:</strong> Neste tipo de aparellos, a calor cédese principalmente por radiación, a través da envolvente metálica, e por unha convención natural, mercé ao aire que pode circular a través dunhas canles existentes na carcasa e en núcleo do acumulador. Para tal fin, dispón dunha entrada de aire na parte inferior do aparello e unha saída pola parte superior. O aire de saída regúlase mediante unha comporta.<br /><br />
<strong>Acumuladores dinámicos:</strong> Os acumuladores dinámicos son similares aos estáticos. Tamén cede a calor por radiación, a través da envolvente metálica, pero a convección é forzada mediante unha turbina que fai circular o aire polos condutos ou canles existentes no núcleo do acumulador.";
$informacion_consejos_calefaccion6="<strong>1.  Bloque acumulador:</strong> O núcleo acumulador está constituído por placas de material refractario (magnesita) que deben presentar unha calor específica elevada e a máxima densidade, xa que a capacidade para almacenar calor depende deses dous parámetros. A temperatura máxima que se acada ao final do período de carga no núcleo do acumulador é de 600 a 700 º C.<br /><br />
<strong>2. Resistencias calefactoras:</strong> Os elementos calefactores (resistencias) insírense en cavidades ou canles situadas no interior do núcleo acumulador, de xeito que quentan toda a masa do refractorio uniformemente.<br /><br />
<strong>3. Illamento térmico:</strong> O bloque acumulador está rodeado con illante térmico para conservar a calor acumulade e limitar a temperatura superficial do moble do aparello.<br /><br />
<strong>4. Dispositivo de seguridade:</strong> Xeralmente os acumuladores dispón dun limitador térmico ou dun termóstato de seguridade, para dar de baixa o aparello se, por calquera motivo, prodúcese unha avaliación desaxeitada da temperatura (por exemplo, unha disfunción no sistema de regulación do sistema de carga).<br /><br />
<strong>5. Termóstato: </strong>Hai dous, un para regular a carga e outro para a emisión de calor.";
$informacion_consejos_calefaccion7="VANTAXES:";
$informacion_consejos_calefaccion8="Pódense instalar tanto en edificios de nova construción como en casas antigas.";
$informacion_consejos_calefaccion9="Non existen circuítos de auga, eliminando a posibilidade de fugas e goteos.";
$informacion_consejos_calefaccion10="Non necesitan depósito de combustible.";
$informacion_consejos_calefaccion11="Instalación sinxela, sen obras.";
$informacion_consejos_calefaccion12="Ecolóxicos, pois non emiten fumes e utilizan a enerxía máis limpa que se coñece no punto de consumo: a Electricidade.";
$informacion_consejos_calefaccion13="Total seguridade e ao non haber combustibles, non existe risco de explosión fortuíta.";
$informacion_consejos_calefaccion14="Mantemento practicamente inexistente.";
$informacion_consejos_calefaccion15="Proporcionan calefacción as 24 h. do día.";
$informacion_consejos_calefaccion16="Redúcense os custos xerais da facturación, ao desprazar o consumo de frigoríficos, lavadoras, lavalouza, quentadores de auga, iluminación, etc., a horario de Tarifa Nocturna, como se indicou anteriormente na descrición do aparello.";
$informacion_consejos_calefaccion17="A súa aparencia estética é de deseño avanzado.";
$informacion_consejos_calefaccion18="Automatizables ao máximo.";
$informacion_consejos_calefaccion19="Calefacción eléctrica por cable radiante con sistemas de acumulación (base + apoio, tarifa nocturna 2.0):";
$informacion_consejos_calefaccion20="A calefacción eléctrica por cable radiante é un sistema de calefacción non visible, que integra dúas partes diferenciadas no referente ao uso de horario e complementarias polo seu funcionamento: calefacción de base e calefacción de apoio.";
$informacion_consejos_calefaccion21="Calefacción de base:";
$informacion_consejos_calefaccion22="Este sistema calefactor instalado no chan queda conectado exclusivamente durante o período da noite, cando a tarifa nocturna ten unha redución do 53% no prezo do kWh.
A calefacción de base instalada no chan, acumula calor durante o período de tarifa reducida, que restitúe ao ambiente sen gastar enerxía durante o período das horas restantes.Lacantidade de calor acumulada no chan vén determinada pola temperatura exterior, xa que dispón dunha sonda que detecta a temperatura.<br /><br />
Xa que a temperatura interior se incrementa nalgúns graos debido ás achegas de calor gratuíta xerada polas persoas, iluminación, auga quente sanitaria, electrodomésticos, insolación durante o día, etc., que a contía destas achegas é imprevisible e non detectable pola sonda do regulador situada no exterior e que este non funciona durante as horas diúrnas, é imprescindible contar cun sistema calefactor de apoio complementario que permita unha regulación exacta do nivel de confort desexado en cada estancia.";
$informacion_consejos_calefaccion23="Calefacción de apoio:";
$informacion_consejos_calefaccion24="Este sistema calefactor independente, de potencia reducida tendo en conta que só intervén compensando as variacións da temperatura interior que o sistema de base non pode detectar nin corrixir, é complementario da calefacción de base por acumulación. Consiste en cables calefactores integrados no chan, por riba dos cables do sistema de base, conectados á rede eléctrica a través dun termóstato automático de ambiente en cada cuarto ou dependencia.<br /><br />
A súa función é manter durante as 24 horas ao día a temperatura exacta desexada polo usuario en cada cuarto.
Outra opción para a calefacción de apoio é a instalación de convectores eléctricos para completar a achega de calor ata a temperatura previamente seleccionada en cada cuarto.";
$informacion_consejos_calefaccion25="Recomendacións de uso:";
$informacion_consejos_calefaccion26="O axuste das temperaturas desexadas mediante os termóstatos situados nas distintas dependencias é moi importantes para conseguir o maior confort do seu sistema de calefacción.<br /><br />
Se a súa vivenda é de recente construción, a propia humidade dos materiais pode provocar un consumo entre un 20 e un 30% maior do normal na primeira tempada de funcionamento, independente de cal sexa o sistema de calefacción de que dispoña.";
$informacion_consejos_calefaccion27="VANTAXES:";
$informacion_consejos_calefaccion28="Limpo: Non se producen movementos do aire e, por conseguinte, do po.";
$informacion_consejos_calefaccion29="Saudable: Non consume osíxeno, non reseca o aire nin o requenta en exceso.";
$informacion_consejos_calefaccion30="Custo controlado: A instalación individualizada por dependencias permite o control do gasto de acordo coas necesidades, horarios e usos. Ademais aproveita as achegas gratuítas de calor (sol, luces, persoas).";
$informacion_consejos_calefaccion31="Invisible e silencioso: Non hai maquinaria nin elementos mecánicos á vista.";
$informacion_consejos_calefaccion32="Seguro: Fóra do alcance das persoas e protexido electricamente de acordo co Regulamento Electrotécnico de Baixa Tensión, e ao non haber combustibles non existe risco de explosión fortuíta.";
$informacion_consejos_calefaccion33="E ademais: Sen obras auxiliares, depósitos de combustibles, mantemento, pagamentos adiantados de enerxía e, é o sistema de menor custo de investimento.";
$informacion_consejos_calefaccion34="Consellos prácticos:";
$informacion_consejos_calefaccion35="O axuste da temperatura en cada cuarto mediante o termóstato é moi importante para controlar o consumo do seu sistema de calefacción. Fixar unha temperatura de 20ºC en lugar de 21ºC pode significar un aforro de enerxía do 10%.";
$informacion_consejos_calefaccion34="Consellos Prácticos:";
$informacion_consejos_calefaccion35="O axuste da temperatura en cada cuarto mediante o termóstato é moi importante para controlar o consumo do seu sistema de calefacción. Fixar unha temperatura de 20ºC en lugar de 21ºC pode significar un aforro de enerxía do 10%.";
$informacion_consejos_calefaccion36="Cando saia do seu fogar só por unhas horas, fixe a posición do termóstato en 15º, que é o equivalente á posición 'economía' dalgúns modelos.";
$informacion_consejos_calefaccion37="Reduza a temperatura das estancias que non ocupen durante períodos de tempo longo.";
$informacion_consejos_calefaccion38="Apage a calefacción pola noite e enciendala pola mañá despues de ventilar a casa e pechar as ventás.";
$informacion_consejos_calefaccion39="Para ventilar os cuartos 10 minutos son suficientes. Unha renovación excesiva pode representar entre un 30 e 40% do consumo total.";
$informacion_consejos_calefaccion40="Para evitar excesivas perdas de calor, peche pola noite persianas e cortinas.";
$informacion_consejos_calefaccion41="Utilice válvulas termostáticas en radiadores e termóstatos programadores, son doados de colocar e amortízanse rapidamente (supón un aforro de enerxía entre un 8 e 133%).";
$informacion_consejos_calefaccion42="Ao inicio de cada tempada de frío é conveniente purgar o aire do interior dos radiadores para facilitar a transmisión de calor desde a auga quente ao exterior.";
$informacion_consejos_calefaccion43="Non cubra nin coloque ao lado do radiador ningún obxecto.";
$informacion_consejos_calefaccion44="Coide e manteña axeitadamente o seu equipo de calefacción, pode suporlle un aforro de ata o 15% de enerxía.";
$informacion_consejos_calefaccion45="Cómpre o asesoramento dun instalador para a regulación inicial e posta en marcha da calefacción base.";
?>