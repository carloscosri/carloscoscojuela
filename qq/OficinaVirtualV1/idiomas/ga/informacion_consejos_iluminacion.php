<?php
$informacion_consejos_iluminacion1="Consellos para Aforrar Enerxía - Iluminación";
$informacion_consejos_iluminacion2="Desde o principio da existencia, o home hase esforzado por encontrar sistemas que substitúan a luz solar nas horas de escuridade ou en lugares onde escaseaba.<br /><br />
Froito desta inquietude foi a aparición dunha serie de elementos que, desde o descubrimento do lume ata as distintas modalidades de lámpadas eléctricas coñecidas hoxe en día, foron marcando etapas no desenvolvemento de fontes de luz artificial.<br /><br />
Actualmente, a investigación continúa avanzando cara á consecución de lámpadas que faciliten unha calidade de luz o máis próxima posible á solar.<br /><br />
No mercado pódense encontrar diversos tipos de lámpadas axeitadas ao uso doméstico en que varía, entre outros factores, a súa duración, o consumo e a calidade da luz que emiten.";
$informacion_consejos_iluminacion3="Lámpadas Incandescentes";
$informacion_consejos_iluminacion4="Lámpadas Halóxenas";
$informacion_consejos_iluminacion5="Lámpadas Fluorescentes";
$informacion_consejos_iluminacion6="Lámpadas de Baixo Consumo";
$informacion_consejos_iluminacion7="Cadro Comparativo";
$informacion_consejos_iluminacion8="Eficacia dos Distintos Tipos de Lámpadas";
$informacion_consejos_iluminacion10="Son as populares bombillas cuxa luz se consegue por medio dun filamento que se quenta co paso da electricidade.";
$informacion_consejos_iluminacion11="VANTAXES:";
$informacion_consejos_iluminacion12="Estas lámpadas emiten unha luz de gran cantidade, co que reproducen moi ben as cores.";
$informacion_consejos_iluminacion13="Son as máis baratas entre todas as fontes de luz artificiais.";
$informacion_consejos_iluminacion14="Ofrecen maior flexibilidade debido á enorme gama de modelos e potencias.";
$informacion_consejos_iluminacion15="INCONVENIENTES:";
$informacion_consejos_iluminacion16="Consumen máis enerxía que outras en relación coa cantidade de luz que achegan.";
$informacion_consejos_iluminacion17="Con respecto a outras, a súa duración é máis limitada.";
$informacion_consejos_iluminacion18="Fronte ás bombillas tradicionais, a luz halóxena é máis luminosa e branca, ademais achega numerosas solucións individuais de iluminación, e por suposto cun maior confort visual, obtendo ambientes máis modernos e atractivos. Existen con ou sen reflector, segundo se quiera concentrar a luz ou non.";
$informacion_consejos_iluminacion19="Ofrecen unha luz máis branca e brillante, o que fai que permitan unha perfecta discriminación das cores.";
$informacion_consejos_iluminacion20="Polas súas calidades dan máis xogo na decoración de interiores.";
$informacion_consejos_iluminacion21="A súa eficacia e duración é superior ás lámpadas incandescentes normais.";
$informacion_consejos_iluminacion22="A súa calidade de luz permanece inalterable ao longo de toda a vida da lámpada.";
$informacion_consejos_iluminacion23="Teñen un elevado consumo de enerxía.";
$informacion_consejos_iluminacion24="Os tipos de pouca potencia esixen un transformador que, en determinados modelos, veñen xa incorporado.";
$informacion_consejos_iluminacion25="A emisión de luz é moi concentrada, o que obriga a apantallar a lámpada para que non se vexa directamente.";
$informacion_consejos_iluminacion26="Neste caso, a luz non está producida polo quentamento dun filamento senón por unha descarga eléctrica en arco mantida nun gas ou vapor ionizado.<br /><br />
As lámpadas fluorescentes son aconsellables como alumeado xeral de aplicación universal e económica, e tendo en conta que existen en diferentes tonalidades da luz, pódese elixir a máis axeitada para a súa aplicación, como en baños, cociñas, despachos, corredores, rochos ou garaxes.";
$informacion_consejos_iluminacion27="Ofrecen, dependendo da gama, unha boa calidade de luz e reprodución natural das cores.";
$informacion_consejos_iluminacion28="O seu consumo enerxético é moi reducido e rendible porque, a igual cantidade de luz, consumen a quinta parte que as lámpadas incandescentes e a súa duración é moi longa, de oito a dez veces máis que estas.";
$informacion_consejos_iluminacion29="Fronte á opinión popular, non consumen máis no arranque do aceso, polo que deben apagarse cando non vaian utilizarse.";
$informacion_consejos_iluminacion30="A súa calidade de luz permanece inalterable ao longo de toda a vida da lámpada.";
$informacion_consejos_iluminacion31="Bere dimentsio eta diseinuak bere lanbidea mugatzen dute al ez eskaintza hain luze bat egon bezala gorietan.";
$informacion_consejos_iluminacion32="Son as lámpadas de aforro enerxético (Fluorescentes compactas electrónicas) que, ademais de achegar unha calidade de luz ambiental en calquera lugar, tanto interior como exterior, son fundamentais polo seu baixo consumo naqueles lugares onde se necesiten unha iluminación con longos períodos de aceso. Ideais para espazos exteriores e como alumeado de seguridade.<br /><br />
Esta familia de lámpadas eficientes, son algo máis que meramente económicas. Poden utilizarse case de forma xeral, exactamente igual que as tradicionais bombillas.";
$informacion_consejos_iluminacion33="Consumen cinco veces menos que as incandescentes.";
$informacion_consejos_iluminacion34="Teñen unha vida útil 6 veces maiores, aproximadamente oito anos, estimando catro horas diarias de aceso.";
$informacion_consejos_iluminacion35="Grazas ao aforro de enerxía, a súa utilización contribúe á preservación do medio ambiente.";
$informacion_consejos_iluminacion36="Posúen o mesmo casco que as bombillas tradicionais o que, unido á súa calidade e confort de luz, faias útiles para calquera aplicación.";
$informacion_consejos_iluminacion37="Tradicionais";
$informacion_consejos_iluminacion38="Na seguinte táboa podemos ver a eficacia, a duración media e a posibilidade de distinguir cores entre os distintos tipos de lámpadas máis utilizadas na vivenda.";
$informacion_consejos_iluminacion39="Tipo de Lámpada";
$informacion_consejos_iluminacion40="Índice de Eficacia";
$informacion_consejos_iluminacion41="Duración Media (h)";
$informacion_consejos_iluminacion42="Posibilidade de Distinguir Cores";
$informacion_consejos_iluminacion43="Incandescentes";
$informacion_consejos_iluminacion44="Excelente";
$informacion_consejos_iluminacion45="Halóxenas";
$informacion_consejos_iluminacion46="Fluorescentes";
$informacion_consejos_iluminacion47="Boa";
$informacion_consejos_iluminacion48="Fluorescente Extra";
$informacion_consejos_iluminacion49="Moi Boa";
$informacion_consejos_iluminacion50="Consellos prácticos:";
$informacion_consejos_iluminacion51="Sempre que poida, aproveite a luz natural.";
$informacion_consejos_iluminacion52="Utilice no seu fogar cores claros para paredes e teitos para aproveitar mellor a iluminación natural e reducir a artificial.";
$informacion_consejos_iluminacion53="Apage sempre as luces nos cuartos que non estea utilizando.";
$informacion_consejos_iluminacion54="Minimice a iluminación ornamental en xardíns e outras zonas exteriores.";
$informacion_consejos_iluminacion55="Limpe a miúdo as lámpadas e as pantallas, así aumentará a luminosidad sen aumentar a potencia.";
$informacion_consejos_iluminacion56="Adapte a iluminación ás súas necesidades e teña en conta que coa iluminación localizada conseguirá, ademais de aforrar enerxía, ambientes máis confortables.";
$informacion_consejos_iluminacion57="Utilice bombillas de baixo consumo en vez das bombillas incandescentes, duran 6 veces máis e aforrará ata un 80% de enerxía.";
$informacion_consejos_iluminacion58="Use lámpadas electrónicas xa que durán máis, aguantan un maior número de acesos e apagados, e consumen menos que as lámpadas de baixo consumo convencionais. Saberá distinguiras polo seu peso: mentres que as convencionais pesan arredor dos 400 grs. as electrónicas pesan uns 100 grs.";
$informacion_consejos_iluminacion59="Colóque reguladores de intensidade luminosa de tipo electrónico (non de rostato).";
$informacion_consejos_iluminacion60="Utilice tubos fluorescentes onde necesite máis luz durante moitas horas, como na cociña.";
$informacion_consejos_iluminacion61="Use detectores de presenza para que as luces funcionen automaticamente naqueles lugares pouco habitables como vestíbulos, garaxes ou zonas comúns.";
$volver="Volver";
?>