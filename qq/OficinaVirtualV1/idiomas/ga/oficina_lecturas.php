<?php
$error_rellenar="Debe escribir a data da lectura e un número de contador.";
$error_fecha="A data escribiuse de forma incorrecta. Debe empregar o seguinte formato: 18-03-2009.";
$error_lectura="Debe escribir polo menos unha lectura.
";
$lectura_ok="A súa lectura foi enviada.";
$oficina_lecturas1="Lectura dun Só Período Tarifario";
$oficina_lecturas2="Lecturas con Varios Períodos Tarifarios";
$oficina_lecturas3="Período";
$oficina_lecturas4="P1 Punta";
$oficina_lecturas5="P2 Llano";
$oficina_lecturas6="P3 Val";
$oficina_lecturas7="Activa";
$oficina_lecturas8="Reactiva";
$oficina_lecturas9="Maxímetro";
$error_varias_lecturas="Se escribiu lecturas no apartado 'Lectura dun Só Período Tarifario' non lle deben escribir lecturas no apartado 'Lecturas con Varios Períodos Tarifarios'.";
$error_numero_lectura="Non escribiu correctamente algunha lectura. Deben ser como máximo uns números con 8 enteiros e 3 decimais.";
$error_fecha_posterior="As datas escritas non poden ser posteriores á data actual.";
?>