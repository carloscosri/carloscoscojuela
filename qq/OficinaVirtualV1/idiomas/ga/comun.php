<?php
$titulo_web="AUDINFOR SYSTEM - Oficina Virtual";
$pie_imagen="AUDINFOR SYSTEM S.L.";
$pie="Sitio Web desenvolvido por Audinfor System S.L.";
$w3c_html="Validación W3C HTML";
$w3c_css="Validación W3C CSS";
$menu1="Empresa";
$menu2="Ofertas e Contratación";
$menu3="Información ao Cliente";
$menu4="Oficina Virtual";
$menu5="Portal Sige Enerxía";
$datos_empresa="AUDINFOR SYSTEM, SL | C/ PANAMA 2 LOCAL 2 ZARAGOZA Tel. 976312018";
$menu3_2="Información<bt />ao Cliente";
$menu_idiomas1="Cambiar idioma a Español";
$menu_idiomas2="Cambiar idioma a Catalan";
$menu_idiomas3="Cambiar idioma a Éuscaro";
$menu_idiomas4="Cambiar idioma a Gallego";
$menu_idiomas5="Cambiar idioma a Inglés";
$idioma_activo="Idioma activo";
$error_identificacion="Esta seccion só é para usuarios identificados.";
$nota_pdf1="* Esta documentación está en formato PDF, para visualizar estes arquivos, necesita ter instalado o programa Adobe Acrobat Reader, se non o ten pode descargar este programa (gratuíto) desde aquí:";
$nota_pdf2="Descargar Adobe Acrobat Reader";
$poblacion_empresa="ZARAGOZA";
$banner_sigeenergia="Acceso ao Portal";
$nota_campos_obligatorios="ATENCIÓN: Soamente os campos sinalados con * son obrigatorios.";
$tlf_averias="";
?>