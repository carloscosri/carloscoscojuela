<?php
$contratacion_contratar_requisitos1="Requisitos para a contratación";
$contratacion_contratar_requisitos2="A vivenda ou local que vostede ocupa ou vai ocupar, dispón dunha instalación eléctrica que só falta conectarse á rede xeral para que entre en servizo. Antes de realizar a conexión, asegurese de que dispón da seguinte documentación:";
$contratacion_contratar_requisitos3="Boletín da Delegación de Industria estendida por un Electricista Instalador autorizado.";
$contratacion_contratar_requisitos4="Para garantir que a instalación eléctrica da súa vivenda reune a suficiente garantía de seguridade, a lexislación vixente esixe que se instalen unha serie de proteccións na súa vivenda e que un Instalador autorizado extenda un Boletín, confirmando o correcto estado e funcionamento da instalación.
No caso dunha vivenda de nova construción, o devandito Boletín se facilitarao a empresa inmobiliaria a que vostede adquiriu a vivenda.";
$contratacion_contratar_requisitos5="Se a vivenda foi anteriormente ocupada por outro propietario, esta debería dispor do correspondente Boletín que o poderá achegar sempre que a antigüidade do mesmo non supere os 20 anos. Porén, pola súa propia seguridade e a dos seus familiares, aconsellámoslle que contacte cun Instalador autorizado para que revise e adecue a súa instalación, especialmente o Cadro de Mando e Protección para que dispoña dos elementos de seguridade pertinente que asegure a súa instalación ante calquera risco eléctrico.
Se a potencia que vostede desexa contratar é superior á máxima admisible que figura no Boletín do propietario anterior, é tamén necesario que requira a visita dun Instalador autorizado para que adecue a súa instalación e exténdalle un novo Boletín.";
$contratacion_contratar_requisitos6="Licenza Municipal.";
$contratacion_contratar_requisitos7="É necesaria para as subministracións de enerxía eléctrica en edificacións que vaian ser obxecto de primeira utilización ou de modificación no seu uso.";
$contratacion_contratar_requisitos8="Título de propiedade ou contrato de aluguer da vivenda ou local.";
$contratacion_contratar_requisitos9="Documento Nacional de Identidade.";
$contratacion_contratar_requisitos10="Número da súa conta corrente bancaria ou caderno de aforro.";
$contratacion_contratar_requisitos11="Para a súa comodidade, aconsellámoslle que os recibos de electricidade os domicilie no seu Banco ou Caixa de Aforros. Por suposto, o aboamento dun recibo non entraña a súa conformidade e en caso dun posible erro, o importe seralle reembolsado inmediatamente.";
$contratacion_contratar_requisitos12="Referencia catastral do inmoble.";
$contratacion_contratar_requisitos13="De acordo coas últimas modificacións legais, é imprescindible para facer o contrato de subministración, proporcionar a referencia catastral do inmoble. Segundo a Dirección Xeral do Catastro este código de 20 carácteres é o identificador oficial e obrigatorio dos bens inmobles e é asignado de xeito que todo inmoble ten unha única referencia catastral. Este código encóntrase na escritura de compravendas recentes ou no recibo de imposto bens inmobles (I.B.I.).";
?>