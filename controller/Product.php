<?php
require_once 'lib/Controller.php';
require_once 'model/LevelModel.php';

class Product extends Controller{

    function __construct()
    {
        parent::__construct('Product');
    }

    public function index()
    {
        $rows = $this->model->getAll();
        $this->view->render($rows);
    }

    public function insert()
    {
        $row = $_POST;
        $this->model->insert($row);    
        header('Location: ' . Config::URL . $_SESSION['lang'] . '/product/index');
    }

    public function add($error="")
    {
        $this->view->add($error);
    }

    public function delete($id)
    {
        $this->model->delete($id);    
        header('Location: ' . Config::URL . $_SESSION['lang'] . '/product/index');
    }

    public function edit($id, $error="")
    {
        $row = $this->model->get($id);
        $this->view->edit($row, $error);
    }

    public function update()
    {
        $row = $_POST; 
        $data = $this->model->update($row);
        echo json_encode($data);
        //header('Location: ' . Config::URL . $_SESSION['lang'] . '/product/index');
    }
    
    public function ajaxUpdateProduct()
    {
        $row = $_POST;
        $idProd = $row['idProd'];
        $valor = $row['valor'];
        $campo = $row['campo'];

        $data = $this->model->updateProduct($idProd, $valor, $campo);
        echo json_encode($data);
    }

    public function ajaxPageData($pageNumber)
    {
        $data = $this->model->getPage($pageNumber);
        echo json_encode($data);
    }

    public function ajaxNumPages()
    {
        $data = $this->model->getNumPages();
        echo json_encode($data);
    }

    public function ajaxDelete($number)
    {
        $data = $this->model->delReg($number);
        echo json_encode($data);
    }

    public function ajaxInsert()
    {
        $row = $_POST;
        $option = $this->model->insReg($row);
        echo json_encode($option);
    }
    
    public function ajaxAddOrderList()
    {
        $row=$_POST;
        $_SESSION["orderList"][$row['codigo']]=$row;
        echo json_encode($_SESSION["orderList"]);
    }

    public function ajaxGetDataFilter()
    {
        $data = $this->model->findName($_GET['filter']);
        echo json_encode($data);
    }
    
    public function ajaxNuevoPedido() {
        $row=$_POST;
        $pedido['id'] = $row['id'];
        $pedido['nombre'] = $row['nombre'];
        $pedido['precio'] = $row['precio'];
        $pedido['cantidad'] = $row['cantidad'];

        if ($pedido['id'] != "") {
            $_SESSION['listaPedido'][$pedido['id']] = $pedido;
        }
        echo json_encode($_SESSION['listaPedido']);
    }
    
}