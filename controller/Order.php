<?php
require_once 'lib/Controller.php';

class Order extends Controller
{

    public function __construct()
    {
        parent::__construct('Order');
        $this->selectIdUsuario();
    }
    
    public function __toString(){
        return 'ORDER';
    }   
    
    public function index()
    {
        $pedidos = $this->model->showOrdersUser();
        $this->view->render($pedidos);
    }
    
    public function updateList()
    {
        $idProd = $_POST['id'];
        $cantidad = $_POST['cantidad'];
        $_SESSION['listaPedido'][$idProd]['cantidad'] = $cantidad;
        echo json_encode(true);
    }
    
    public function ajaxDeleteOrder()
    {
        $idProduct = $_POST['id'];
        unset($_SESSION['listaPedido'][$idProduct]);
        echo json_encode(true);
        
    }
    public function ajaxAddOrder()
    {       
        $option = $this->model->addOrder();
        //$this->model->addOrder();
        $opcion = $this->model->addOrderProducts($option);
        echo json_encode($opcion);
    }
    
    public function searchDetailsOrder()
    {
        $idOrder = $_POST['id'];
        $rows = $this->model->selectDetailsOrder($idOrder);
        echo json_encode($rows);
    }

    public function selectIdUsuario()
    {
        $nameUser = $_SESSION['user'];
        $_SESSION['idUsuario'] = $this->model->selectIdUsuario($nameUser)[0]['id'];
        //var_dump($_SESSION['idUsuario']);
    }

}