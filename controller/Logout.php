<?php
require_once 'lib/Controller.php';
//require_once 'model/LevelModel.php';

class Logout extends Controller{

    function __construct()
    {
        parent::__construct('Logout');
    }
   
    public function index()
    {
        $this->view->render();
    }
        
    public function comprobar(){
        $row = $_POST;
        //var_dump($row['confirm']);
        
        if ($row['confirm'] == 'si'){
            unset($_SESSION);
            session_destroy();
        }
        header('Location: ' . Config::URL);
        
    }
}