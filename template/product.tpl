{include file="template/header.tpl" title="encabezado"}
<div id="content">
    <h2>{$language->translate('product_list')}</h2>
    
    <p><a href="{$url}{$lang}/product/add">{$language->translate('new_product')}</a></p>
    <p><label>{$language->translate('filter')}</label><input id="filter" class="cell" type="text"></p>
    <table>
        <thead>
        <tr>
            <th>Id</th>
            <th>{$language->translate('code')}</th>
            <th>{$language->translate('name')}</th>
            <th>{$language->translate('price')}</th>
            <th>{$language->translate('existence')}</th>
            <th>{$language->translate('operations')}</th>
        </tr>
        </thead>
        
        <tfoot>
        {if ($rol == 3)}                          
        <tr>
            <td class="cell"><!--input id="id" class="cell" type="text"--></td>
            <td class="cell"><input id="codigo" class="cell" type="text"></td>
            <td class="cell"><input id="nombre" class="cell" type="text"></td>
            <td class="cell"><input id="precio" class="cell" type="text"></td>
            <td class="cell"><input id="existencia" class="cell" type="text"></td>
            <td id="new"><button class="ins">Nuevo</button></td>
        </tr>
        {/if}  
        </tfoot>        
        
        <tbody id="tbodyList">
        </tbody>
    </table>
    
    <div id="buttons"></div>
    

    
    <p><a href="{$url}{$lang}/product/add">{$language->translate('new_product')}</a></p>

 </div>
{include file="template/footer.tpl" title="footer"}