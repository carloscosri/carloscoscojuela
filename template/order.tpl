{include file="template/header.tpl" title="encabezado"}
<div id="content">
    <br>
    <h2>Carrito</h2>
    <table>
        <tr>           
            <th>Nombre</th>
            <th>Precio</th>
            <th>Unidades</th>
            <th>Total</th>           
            <th>Operaciones</th>
        </tr>
        <tbody>
        {foreach $listaPedido as $row}
            <tr id="ped{$row['id']}" class="listadoPedidos">

                <td>{$row['nombre']}</td>
                <td>{$row['precio']} €</td>
                <td><input type="number" name="quantity" class="cantidad" precio="{$row['precio']}" ide="{$row['id']}" min="1" max="20" value="{$row['cantidad']}"></td>
                <td id="total{{$row['id']}}">{$row['cantidad']*$row['precio']} €</td>
                <td><button class="eliminarPedido" ide="{$row['id']}">Eliminar</button></td>
            </tr>            
        {/foreach}
        </tbody>
        <tfoot>
            <tr>
                <td><button href="#" id="botonPedido">REALIZAR PEDIDO</button></td>
            </tr>
        </tfoot>
    </table>
        <br>
     <h2>Pedidos</h2>
    <table>
        <thead>
            <th>Codigo de pedido</th>
            <th>Fecha de realizacion</th>
            <th>Estado del servicio</th>                               
            {if $idRole ge 3}
                <th>Usuario</th>
                {/if}
        </thead>
        <tbody>
        {foreach $pedidosHechos as $row}
            <tr ide="{$row['id']}" class="listaPedidosRealizados">
                <td>{$row['id']}</td>
                <td>{$row['fechaPedido']}</td>
                <td>{if $row['estado'] eq 0}Pedido{else}Realizado{/if}</td>
               {if $idRole ge 3}
                   <td>{$row['idUsuario']}</td>
                   {/if}
            </tr>          
        {/foreach}
        </tbody>      
    </table>    
</div>
{include file="template/footer.tpl" title="footer"}