{include file="template/header.tpl" title="encabezado"}
<div id="content">
    <br>
    <h2>Edicion de Productos</h2>
    
    <form action="{$url}{$lang}/product/update" method="post">
        <label>id</label><input type="text" name="id" value="{$row.id}"><br>
        <label>{$language->translate('code')}</label><input type="text" name="codigo" value="{$row.codigo}"><br>
        <label>{$language->translate('name')}</label><input type="text" name="nombre" value="{$row.nombre}"><br>
        <label>{$language->translate('price')}</label><input type="text" name="precio" value="{$row.precio}"><br>
        <label>{$language->translate('existence')}</label><input type="text" name="existencia" value="{$row.existencia}"><br>
        <label></label><input type="submit" value="Enviar">
    </form>

 </div>
{include file="template/footer.tpl" title="footer"}